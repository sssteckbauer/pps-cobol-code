000000**************************************************************/   02381440
000001*  PROGRAM: PPEM338                                          */   02381440
000002*  RELEASE: ___1440______ SERVICE REQUEST(S): ____80238____  */   02381440
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___11/08/02__  */   02381440
000004*  DESCRIPTION:                                              */   02381440
000005*  - HCRA TERMINATION DATE IMPLIED MAINTENANCE.              */   02381440
000009**************************************************************/   02381440
000090                                                                  PPEM338
000100 IDENTIFICATION DIVISION.                                         PPEM338
000200 PROGRAM-ID. PPEM338.                                             PPEM338
000300 AUTHOR. UCOP.                                                    PPEM338
000400 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEM338
000500 DATE-WRITTEN.  OCTOBER 2002.                                     PPEM338
000600 DATE-COMPILED.                                                   PPEM338
000700                                                                  PPEM338
000800******************************************************************PPEM338
000900**    THIS ROUTINE PERFORMS IMPLIED MAINTENANCE FOR HCRA DATA   **PPEM338
001000**    DURING MONTHLY PERIODIC MAINTENANCE.                      **PPEM338
001031**                                                              **PPEM338
001090**    IF THE HCRA TERMINATION DATE IS PRIOR TO THE FIRST DAY    **PPEM338
001091**    OF THE MONTH BEING STRTED, INITIALIZE THE G BALANCE.      **PPEM338
001100******************************************************************PPEM338
001200                                                                  PPEM338
001300 ENVIRONMENT DIVISION.                                            PPEM338
001400 CONFIGURATION SECTION.                                           PPEM338
001500 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEM338
001600 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEM338
001700 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEM338
001800                                                                  PPEM338
001900 INPUT-OUTPUT SECTION.                                            PPEM338
002000 FILE-CONTROL.                                                    PPEM338
002100 DATA DIVISION.                                                   PPEM338
002200 FILE SECTION.                                                    PPEM338
002300     EJECT                                                        PPEM338
002400*----------------------------------------------------------------*PPEM338
002500 WORKING-STORAGE SECTION.                                         PPEM338
002600*----------------------------------------------------------------*PPEM338
002700 01  A-STND-PROG-ID            PIC X(15) VALUE                    PPEM338
002800     'PPEM338 /110802'.                                           PPEM338
002900                                                                  PPEM338
003000 01  A-STND-MSG-PARMS                 REDEFINES                   PPEM338
003100     A-STND-PROG-ID.                                              PPEM338
003200     05  FILLER                       PIC X(03).                  PPEM338
003300     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEM338
003400     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEM338
003500     05  FILLER                       PIC X(08).                  PPEM338
003600                                                                  PPEM338
003700 01  ELEMENTS-USED.                                               PPEM338
003800     05  E6000                 PIC 9(04)  VALUE 6000.             PPEM338
004000                                                                  PPEM338
004400 01  MISC-WORK-AREAS.                                             PPEM338
004500     05  FIRST-TIME-SW          PIC X(01)  VALUE LOW-VALUES.      PPEM338
004600         88  THIS-IS-THE-FIRST-TIME        VALUE LOW-VALUES.      PPEM338
004700         88  NOT-THE-FIRST-TIME            VALUE 'Y'.             PPEM338
004730     05  GTN-WORK-AREA.                                           PPEM338
004793         10  DSA-BALANCE-TYPES COMP-3.                            PPEM338
004794             15  DSA-GTN-P      PIC S9(4) VALUE +001.             PPEM338
004795             15  DSA-SUSP-P     PIC S9(4) VALUE +002.             PPEM338
004796             15  DSA-QTD-P      PIC S9(4) VALUE +003.             PPEM338
004797             15  DSA-DECL-P     PIC S9(4) VALUE +004.             PPEM338
004798             15  DSA-YTD-P      PIC S9(4) VALUE +005.             PPEM338
004799             15  DSA-ETD-P      PIC S9(4) VALUE +006.             PPEM338
004800             15  DSA-FTD-P      PIC S9(4) VALUE +007.             PPEM338
004801             15  DSA-USER-P     PIC S9(4) VALUE +008.             PPEM338
004802         10  GTN-NUMBERS         COMP.                            02381440
004803             15  HCRA-GTN-NO    PIC S9(4) VALUE +338.             02381440
004848     EJECT                                                        PPEM338
004900 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEM338
005000     EJECT                                                        PPEM338
005300 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEM338
005400     EJECT                                                        PPEM338
005500 01  INSTALLATION-DEPENDT-CONSTATNS.               COPY CPWSXIDC. PPEM338
005600     EJECT                                                        PPEM338
005700 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEM338
005800     EJECT                                                        PPEM338
005900 01  XMST-MESSAGE-SEVERITY-TABLE.                  COPY CPWSXMST. PPEM338
006000     EJECT                                                        PPEM338
006100 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. PPEM338
006200     EJECT                                                        PPEM338
006400******************************************************************PPEM338
006500**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEM338
006600**--------------------------------------------------------------**PPEM338
006700** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEM338
006800******************************************************************PPEM338
006900                                                                  PPEM338
007000 01  ONLINE-SIGNALS                      EXTERNAL. COPY CPWSONLI. PPEM338
007200     EJECT                                                        PPEM338
007300 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEM338
007500     EJECT                                                        PPEM338
007600 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEM338
007800     EJECT                                                        PPEM338
007900 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEM338
008100     EJECT                                                        PPEM338
008200 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEM338
008300     EJECT                                                        PPEM338
008400 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEM338
008500     EJECT                                                        PPEM338
008600 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEM338
008700     EJECT                                                        PPEM338
008800 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEM338
009000     EJECT                                                        PPEM338
009100 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEM338
009300     EJECT                                                        PPEM338
009400 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEM338
009600     EJECT                                                        PPEM338
009700 01  COMM-TAB                            EXTERNAL. COPY CPOTXCMF. PPEM338
009900     EJECT                                                        PPEM338
010000 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEM338
010200     EJECT                                                        PPEM338
010300 01  DEDUCTION-SEGMENT-ARRAY             EXTERNAL. COPY CPWSEDSA. PPEM338
010310     EJECT                                                        PPEM338
010320 01  BENEFIT-SEGMENT-ARRAY               EXTERNAL. COPY CPWSEBSA. PPEM338
010330     EJECT                                                        PPEM338
010400******************************************************************PPEM338
010500**  DATA BASE AREAS                                             **PPEM338
010600******************************************************************PPEM338
010700 01  BEN-ROW                             EXTERNAL. COPY CPWSRBEN. PPEM338
010800     EJECT                                                        PPEM338
011900******************************************************************PPEM338
012000**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEM338
012100******************************************************************PPEM338
012200*----------------------------------------------------------------*PPEM338
012300 LINKAGE SECTION.                                                 PPEM338
012400*----------------------------------------------------------------*PPEM338
012600******************************************************************PPEM338
012700**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEM338
012800**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEM338
012900******************************************************************PPEM338
013000                                                                  PPEM338
013100 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEM338
013300     EJECT                                                        PPEM338
013400******************************************************************PPEM338
013500**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEM338
013600**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEM338
013700******************************************************************PPEM338
013900 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEM338
014100     EJECT                                                        PPEM338
014200******************************************************************PPEM338
014300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEM338
014400******************************************************************PPEM338
014500 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEM338
014600                          KMTA-MESSAGE-TABLE-ARRAY.               PPEM338
014700                                                                  PPEM338
014800*----------------------------------------------------------------*PPEM338
014900 0000-DRIVER.                                                     PPEM338
015000*----------------------------------------------------------------*PPEM338
015100******************************************************************PPEM338
015200**  PERFORM THE INITIALIZATION ROUTINE IF FIRST TIME            **PPEM338
015300******************************************************************PPEM338
015400                                                                  PPEM338
015500     IF  THIS-IS-THE-FIRST-TIME                                   PPEM338
015600         PERFORM 0100-INITIALIZE                                  PPEM338
015700     END-IF.                                                      PPEM338
015800                                                                  PPEM338
015900******************************************************************PPEM338
016000**  PERFORM THE MAINLINE ROUTINE                                **PPEM338
016100******************************************************************PPEM338
016200                                                                  PPEM338
016300     PERFORM 1000-MAINLINE-ROUTINE.                               PPEM338
016400                                                                  PPEM338
016500     EXIT PROGRAM.                                                PPEM338
016600                                                                  PPEM338
016700     EJECT                                                        PPEM338
016800*----------------------------------------------------------------*PPEM338
016900 0100-INITIALIZE                 SECTION.                         PPEM338
017000*----------------------------------------------------------------*PPEM338
017100******************************************************************PPEM338
017200**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEM338
017300******************************************************************PPEM338
017400                                                                  PPEM338
017500     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEM338
017600                                                                  PPEM338
017700*                                               *-------------*   PPEM338
017800                                                 COPY CPPDXWHC.   PPEM338
017900*                                               *-------------*   PPEM338
018000******************************************************************PPEM338
018100**   SET FIRST CALL SWITCH OFF                                  **PPEM338
018200******************************************************************PPEM338
018300                                                                  PPEM338
018400     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEM338
018600     EJECT                                                        PPEM338
018700*----------------------------------------------------------------*PPEM338
018800 1000-MAINLINE-ROUTINE           SECTION.                         PPEM338
018900*----------------------------------------------------------------*PPEM338
019000                                                                  PPEM338
019010******************************************************************PPEM338
019020**   IF THE HCRA TERMINATION DATE IS IN A PRIOR MONTH TO THE    **PPEM338
019021**   MONTH BEING STARTED, ZERO OUT THE G BALANCE.               **PPEM338
019030******************************************************************PPEM338
019100     IF  HCRA-TERM-DATE >  XDC3-LOW-ISO-DATE                      PPEM338
019110     AND HCRA-TERM-DATE <  XDTS-ISO-FIRST-OF-MONTH-DATE           PPEM338
019200         MOVE ZERO TO EDSA-BALAMT (HCRA-GTN-NO, DSA-GTN-P)        PPEM338
019300                                                                  PPEM338
019400         MOVE E6000 TO DL-FIELD                                   PPEM338
019500         PERFORM 9050-AUDITING-RESPONSIBILITIES                   PPEM338
019510     END-IF.                                                      PPEM338
019640                                                                  PPEM338
019650*----------------------------------------------------------------*PPEM338
019700 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEM338
019800*----------------------------------------------------------------*PPEM338
019900                                                                  PPEM338
020000*                                                 *-------------* PPEM338
020100                                                   COPY CPPDXDEC. PPEM338
020200*                                                 *-------------* PPEM338
020300                                                                  PPEM338
021100******************************************************************PPEM338
021200**   E N D  S O U R C E   ----  PPEM338 ----                    **PPEM338
021300******************************************************************PPEM338
