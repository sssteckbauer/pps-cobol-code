000100**************************************************************/   30871465
000200*  PROGRAM: PPCTLAFU                                         */   30871465
000300*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000400*  NAME:___SRS___________ CREATION DATE:      ___02/11/03__  */   30871465
000500*  DESCRIPTION:                                              */   30871465
000600*  LEAVE ACCRUAL LAF TABLE UPDATE MODULE                     */   30871465
000700**************************************************************/   30871465
000800 IDENTIFICATION DIVISION.                                         PPCTLAFU
000900 PROGRAM-ID. PPCTLAFU.                                            PPCTLAFU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTLAFU
001100 DATE-COMPILED.                                                   PPCTLAFU
001200 ENVIRONMENT DIVISION.                                            PPCTLAFU
001300 DATA DIVISION.                                                   PPCTLAFU
001400 WORKING-STORAGE SECTION.                                         PPCTLAFU
001500                                                                  PPCTLAFU
001600 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTLAFU
001700 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTLAFU
001800                                                                  PPCTLAFU
001900     EXEC SQL                                                     PPCTLAFU
002000        INCLUDE SQLCA                                             PPCTLAFU
002100     END-EXEC.                                                    PPCTLAFU
002200                                                                  PPCTLAFU
002300 LINKAGE SECTION.                                                 PPCTLAFU
002400                                                                  PPCTLAFU
002500 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTLAFU
002600 01  LEAVE-ACCRUAL-LAF-TABLE-DATA.                                PPCTLAFU
002700     EXEC SQL                                                     PPCTLAFU
002800        INCLUDE PPPVZLAF                                          PPCTLAFU
002900     END-EXEC.                                                    PPCTLAFU
003000                                                                  PPCTLAFU
003100 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE            PPCTLAFU
003200                          LEAVE-ACCRUAL-LAF-TABLE-DATA.           PPCTLAFU
003300                                                                  PPCTLAFU
003400 0000-MAIN.                                                       PPCTLAFU
003500                                                                  PPCTLAFU
003600     EXEC SQL                                                     PPCTLAFU
003700        INCLUDE CPPDXE99                                          PPCTLAFU
003800     END-EXEC.                                                    PPCTLAFU
003900     MOVE 'PPCTLAFU' TO XWHC-DISPLAY-MODULE.                      PPCTLAFU
004000     COPY CPPDXWHC.                                               PPCTLAFU
004100     MOVE SPACE TO KCTU-STATUS.                                   PPCTLAFU
004200     IF KCTU-ACTION-ADD                                           PPCTLAFU
004300        PERFORM 2000-INSERT-LAF                                   PPCTLAFU
004400     ELSE                                                         PPCTLAFU
004500       IF KCTU-ACTION-CHANGE                                      PPCTLAFU
004600          PERFORM 2100-UPDATE-LAF                                 PPCTLAFU
004700       ELSE                                                       PPCTLAFU
004800         IF KCTU-ACTION-DELETE                                    PPCTLAFU
004900            PERFORM 2200-DELETE-LAF                               PPCTLAFU
005000         ELSE                                                     PPCTLAFU
005100            SET KCTU-INVALID-ACTION TO TRUE                       PPCTLAFU
005200         END-IF                                                   PPCTLAFU
005300       END-IF                                                     PPCTLAFU
005400     END-IF.                                                      PPCTLAFU
005500                                                                  PPCTLAFU
005600 0100-END.                                                        PPCTLAFU
005700                                                                  PPCTLAFU
005800     GOBACK.                                                      PPCTLAFU
005900                                                                  PPCTLAFU
006000 2000-INSERT-LAF.                                                 PPCTLAFU
006100                                                                  PPCTLAFU
006200     MOVE '2000-INSRT-LAF' TO DB2MSG-TAG.                         PPCTLAFU
006300     EXEC SQL                                                     PPCTLAFU
006400         INSERT INTO PPPVZLAF_LAF                                 PPCTLAFU
006500              VALUES (:LAF-TITLE-TYPE                             PPCTLAFU
006600                     ,:LAF-TITLE-UNIT-CD                          PPCTLAFU
006700                     ,:LAF-AREP-CODE                              PPCTLAFU
006800                     ,:LAF-SHC                                    PPCTLAFU
006900                     ,:LAF-DUC                                    PPCTLAFU
007000                     ,:LAF-EFFECTIVE-DATE                         PPCTLAFU
007100                     ,:LAF-FND-GROUP-CODE                         PPCTLAFU
007200                     ,:LAF-UTIL-FACTR-VAC                         PPCTLAFU
007300                     ,:LAF-UTIL-FACTR-SKL                         PPCTLAFU
007400                     ,:LAF-UTIL-FACTR-PTO                         PPCTLAFU
007500                     ,:LAF-LEAVE-RES-FAU                          PPCTLAFU
007600                     ,:LAF-LAST-ACTION                            PPCTLAFU
007700                     ,:LAF-LAST-ACTION-DT                         PPCTLAFU
007800                     )                                            PPCTLAFU
007900     END-EXEC.                                                    PPCTLAFU
008000     IF SQLCODE NOT = ZERO                                        PPCTLAFU
008100        SET KCTU-INSERT-ERROR TO TRUE                             PPCTLAFU
008200     END-IF.                                                      PPCTLAFU
008300                                                                  PPCTLAFU
008400 2100-UPDATE-LAF.                                                 PPCTLAFU
008500                                                                  PPCTLAFU
008600     MOVE '2100-UPD-LAF ' TO DB2MSG-TAG.                          PPCTLAFU
008700     EXEC SQL                                                     PPCTLAFU
008800         UPDATE PPPVZLAF_LAF                                      PPCTLAFU
008900            SET LAF_TITLE_TYPE        = :LAF-TITLE-TYPE           PPCTLAFU
009000               ,LAF_TITLE_UNIT_CD     = :LAF-TITLE-UNIT-CD        PPCTLAFU
009100               ,LAF_AREP_CODE         = :LAF-AREP-CODE            PPCTLAFU
009200               ,LAF_SHC               = :LAF-SHC                  PPCTLAFU
009300               ,LAF_DUC               = :LAF-DUC                  PPCTLAFU
009400               ,LAF_EFFECTIVE_DATE    = :LAF-EFFECTIVE-DATE       PPCTLAFU
009500               ,LAF_FND_GROUP_CODE    = :LAF-FND-GROUP-CODE       PPCTLAFU
009600               ,LAF_UTIL_FACTR_VAC    = :LAF-UTIL-FACTR-VAC       PPCTLAFU
009700               ,LAF_UTIL_FACTR_SKL    = :LAF-UTIL-FACTR-SKL       PPCTLAFU
009800               ,LAF_UTIL_FACTR_PTO    = :LAF-UTIL-FACTR-PTO       PPCTLAFU
009900               ,LAF_LEAVE_RES_FAU     = :LAF-LEAVE-RES-FAU        PPCTLAFU
010000               ,LAF_LAST_ACTION       = :LAF-LAST-ACTION          PPCTLAFU
010100               ,LAF_LAST_ACTION_DT    = :LAF-LAST-ACTION-DT       PPCTLAFU
010200          WHERE LAF_TITLE_TYPE        = :LAF-TITLE-TYPE           PPCTLAFU
010300            AND LAF_TITLE_UNIT_CD     = :LAF-TITLE-UNIT-CD        PPCTLAFU
010400            AND LAF_AREP_CODE         = :LAF-AREP-CODE            PPCTLAFU
010500            AND LAF_SHC               = :LAF-SHC                  PPCTLAFU
010600            AND LAF_DUC               = :LAF-DUC                  PPCTLAFU
010700            AND LAF_EFFECTIVE_DATE    = :LAF-EFFECTIVE-DATE       PPCTLAFU
010800            AND LAF_FND_GROUP_CODE    = :LAF-FND-GROUP-CODE       PPCTLAFU
010900     END-EXEC.                                                    PPCTLAFU
011000     IF SQLCODE NOT = ZERO                                        PPCTLAFU
011100        SET KCTU-UPDATE-ERROR TO TRUE                             PPCTLAFU
011200     END-IF.                                                      PPCTLAFU
011300                                                                  PPCTLAFU
011400 2200-DELETE-LAF.                                                 PPCTLAFU
011500                                                                  PPCTLAFU
011600     MOVE '2200-DLTE-LAF ' TO DB2MSG-TAG.                         PPCTLAFU
011700     EXEC SQL                                                     PPCTLAFU
011800         DELETE FROM PPPVZLAF_LAF                                 PPCTLAFU
011900          WHERE LAF_TITLE_TYPE        = :LAF-TITLE-TYPE           PPCTLAFU
012000            AND LAF_TITLE_UNIT_CD     = :LAF-TITLE-UNIT-CD        PPCTLAFU
012100            AND LAF_AREP_CODE         = :LAF-AREP-CODE            PPCTLAFU
012200            AND LAF_SHC               = :LAF-SHC                  PPCTLAFU
012300            AND LAF_DUC               = :LAF-DUC                  PPCTLAFU
012400            AND LAF_EFFECTIVE_DATE    = :LAF-EFFECTIVE-DATE       PPCTLAFU
012500            AND LAF_FND_GROUP_CODE    = :LAF-FND-GROUP-CODE       PPCTLAFU
012600     END-EXEC.                                                    PPCTLAFU
012700     IF SQLCODE NOT = ZERO                                        PPCTLAFU
012800        SET KCTU-DELETE-ERROR TO TRUE                             PPCTLAFU
012900     END-IF.                                                      PPCTLAFU
013000                                                                  PPCTLAFU
013100*999999-SQL-ERROR.                                                PPCTLAFU
013200     EXEC SQL                                                     PPCTLAFU
013300        INCLUDE CPPDXP99                                          PPCTLAFU
013400     END-EXEC.                                                    PPCTLAFU
013410     EVALUATE TRUE                                                PPCTLAFU
013420       WHEN (KCTU-ACTION-ADD)                                     PPCTLAFU
013430         SET KCTU-INSERT-ERROR TO TRUE                            PPCTLAFU
013440       WHEN (KCTU-ACTION-CHANGE)                                  PPCTLAFU
013450         SET KCTU-UPDATE-ERROR TO TRUE                            PPCTLAFU
013460       WHEN (KCTU-ACTION-DELETE)                                  PPCTLAFU
013470         SET KCTU-DELETE-ERROR TO TRUE                            PPCTLAFU
013480     END-EVALUATE.                                                PPCTLAFU
013500     GO TO 0100-END.                                              PPCTLAFU
