000100**************************************************************/   30871401
000200*  PROGRAM: PPCTT06                                          */   30871401
000300*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000400*  NAME:___SRS___________ CREATION DATE:      ___03/20/02__  */   30871401
000500*  DESCRIPTION:                                              */   30871401
000600*  PPPDET TABLE TRANSACTION ASSEMBLY MODULE                  */   30871401
000700**************************************************************/   30871401
000800 IDENTIFICATION DIVISION.                                         PPCTT06
000900 PROGRAM-ID. PPCTT06.                                             PPCTT06
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT06
001100 DATE-COMPILED.                                                   PPCTT06
001200 DATA DIVISION.                                                   PPCTT06
001300 FILE SECTION.                                                    PPCTT06
001400 WORKING-STORAGE SECTION.                                         PPCTT06
001500                                                                  PPCTT06
001600 01  WA-WORK-VARIABLES.                                           PPCTT06
001700     05  WA-READ-ERROR.                                           PPCTT06
001800         10 FILLER                     PIC X(37) VALUE            PPCTT06
001900                 'PPCTLTRD ERROR ON TRANS CALL: RETURN='.         PPCTT06
002000         10 WA-RTN-CODE                PIC XX.                    PPCTT06
002100         10 FILLER                     PIC X(07) VALUE            PPCTT06
002200                 ' COUNT='.                                       PPCTT06
002300         10 WA-WORK-Z5                 PIC ZZ,ZZ9.                PPCTT06
002400 01  WB-PROGRAM-CONSTANTS.                                        PPCTT06
002500     05  WB-TABLE-NO                   PIC X(2) VALUE '06'.       PPCTT06
002600 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT06
002700     05  WC-ERR-IX                     PIC S9(04) VALUE ZERO.     PPCTT06
002800     05  WC-SET-CNT                    PIC S9(04) VALUE ZERO.     PPCTT06
002900     05  WC-TALLY                      PIC S9(04) VALUE ZERO.     PPCTT06
003000     05  IX1                           PIC S9(04) VALUE ZERO.     PPCTT06
003100     05  IX2                           PIC S9(04) VALUE ZERO.     PPCTT06
003200     05  IX3                           PIC S9(04) VALUE ZERO.     PPCTT06
003300 01  WF-PROGRAM-FLAGS.                                            PPCTT06
003400     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT06
003500         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT06
003600         88  WF-ACCEPT-SET                      VALUES 00 THRU 04.PPCTT06
003700         88  WF-REJECT-SET                      VALUES 05 THRU 99.PPCTT06
003800 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT06
003900     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT06
004000     05  MCT020                     PIC X(05) VALUE 'CT020'.      PPCTT06
004100     05  MCT021                     PIC X(05) VALUE 'CT021'.      PPCTT06
004200     05  MCT022                     PIC X(05) VALUE 'CT022'.      PPCTT06
004300     05  MCT023                     PIC X(05) VALUE 'CT023'.      PPCTT06
004400     05  M01059                     PIC X(05) VALUE '01059'.      PPCTT06
004500     05  M01914                     PIC X(05) VALUE '01914'.      PPCTT06
004600 01  WS-SET-WORK-AREA.                                            PPCTT06
004700     05  WS-VALID-NEXT-SEQ-NOS      PIC X(06) VALUE LOW-VALUES.   PPCTT06
004800     05  WS-SET-END-FLAG            PIC X(01) VALUE 'N'.          PPCTT06
004900         88  WS-SET-END-NOT-OK                VALUE 'N'.          PPCTT06
005000         88  WS-SET-END-OK                    VALUE 'Y'.          PPCTT06
005100     05  WS-SET-STATUS-FLAG         PIC X(01) VALUE 'N'.          PPCTT06
005200         88  WS-SET-OK                        VALUE 'Y'.          PPCTT06
005300         88  WS-SET-NOT-OK                    VALUE 'N'.          PPCTT06
005400     05  WS-SET-KEY.                                              PPCTT06
005500         10  WS-SET-ACTION          PIC X(01).                    PPCTT06
005600             88  WS-ADD-SET                   VALUE 'A'.          PPCTT06
005700             88  WS-CHANGE-SET                VALUE 'C'.          PPCTT06
005800             88  WS-DELETE-SET                VALUE 'D'.          PPCTT06
005900         10  WS-SET-CARD-TYPE       PIC X(02).                    PPCTT06
006000         10  WS-SET-ELEM-NO         PIC X(04).                    PPCTT06
006100     05  WS-HOLD-KEY                PIC X(07) VALUE LOW-VALUES.   PPCTT06
006200 01  DATA-ELEMENT-TABLE-TRANS.                                    PPCTT06
006300     05  DETT-ACTION-CODE           PIC X(01).                    PPCTT06
006400         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT06
006500         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT06
006600         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT06
006700         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT06
006800     05  FILLER                     PIC X(02).                    PPCTT06
006900     05  DETT-TRAN-SEQ-NO           PIC X(01).                    PPCTT06
007000         88  DETT-TRAN-SEQ-NO-VALID     VALUES '1' '2' '3'        PPCTT06
007100                                               '4' '5' '6'.       PPCTT06
007200     05  DETT-CARD-TYPE             PIC XX.                       PPCTT06
007300     05  DETT-ELEM-NO               PIC XXXX.                     PPCTT06
007400     05  DETT-DATA-1.                                             PPCTT06
007500         10 DETT-CARD-START-POS     PIC XX.                       PPCTT06
007600         10 DETT-DATA-TYPE          PIC X.                        PPCTT06
007700         10 DETT-EXT-LENGTH         PIC XX.                       PPCTT06
007800         10 DETT-DEC-PLACE          PIC X.                        PPCTT06
007900         10 DETT-LEGAL-TO-UPD       PIC X.                        PPCTT06
008000         10 DETT-ELEMENT-NAME       PIC X(15).                    PPCTT06
008100         10 DETT-ST-KEY-POS         PIC X.                        PPCTT06
008200         10 DETT-ST-MANDATORY       PIC X.                        PPCTT06
008300         10 DETT-ST-MSG-ROUTE       PIC X.                        PPCTT06
008400         10 DETT-ST-NOTPYRL         PIC X.                        PPCTT06
008500         10 DETT-DATE-EDIT          PIC X.                        PPCTT06
008600         10 DETT-TBLEDT-RTN         PIC XXX.                      PPCTT06
008700         10 DETT-MAIN-BAL-TYPE      PIC X.                        PPCTT06
008800         10 FILLER                  PIC X(39).                    PPCTT06
008900     05  DETT-DATA-2 REDEFINES DETT-DATA-1.                       PPCTT06
009000         10 DETT-DB2-TABLE-ID       PIC X(6).                     PPCTT06
009100         10 FILLER                  PIC X(64).                    PPCTT06
009200     05  DETT-DATA-3 REDEFINES DETT-DATA-1.                       PPCTT06
009300         10 DETT-TRAN-3-CONTD       PIC X.                        PPCTT06
009400         10 DETT-VAL-RNG-EDTS-GRP.                                PPCTT06
009500            15 DETT-VAL-RNG-EDTS OCCURS 69  PIC X.                PPCTT06
009600     05  DETT-DATA-4 REDEFINES DETT-DATA-1.                       PPCTT06
009700         10 DETT-CON-E-RTNS-GRP.                                  PPCTT06
009800            15 DETT-CON-E-RTNS-X OCCURS 10.                       PPCTT06
009900               20 DETT-CON-E-RTNS   PIC 999.                      PPCTT06
010000         10 FILLER                  PIC X(40).                    PPCTT06
010100     05  DETT-DATA-5 REDEFINES DETT-DATA-1.                       PPCTT06
010200         10 DETT-GTN-E-RTNS-GRP.                                  PPCTT06
010300            15 DETT-GTN-E-RTNS-X OCCURS 5.                        PPCTT06
010400               20 DETT-GTN-E-RTNS   PIC 999.                      PPCTT06
010500         10 FILLER                  PIC X(55).                    PPCTT06
010600     05  DETT-DATA-6 REDEFINES DETT-DATA-1.                       PPCTT06
010700         10 DETT-TRAN-6-CONTD       PIC X.                        PPCTT06
010800         10 DETT-IMP-E-RTNS-GRP.                                  PPCTT06
010900            15 DETT-IMP-E-RTNS-X OCCURS 23.                       PPCTT06
011000               20 DETT-IMP-E-RTNS   PIC 999.                      PPCTT06
011100                                                                  PPCTT06
011200 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT06
011300 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT06
011400 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT06
011500 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT06
011600 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT06
011700 01  DATA-ELEMENT-TABLE-INPUT.         COPY CPCTDETI.             PPCTT06
011800 01  DATA-ELEMENT-TABLE-DATA.                                     PPCTT06
011900     EXEC SQL                                                     PPCTT06
012000         INCLUDE PPPVZDET                                         PPCTT06
012100     END-EXEC.                                                    PPCTT06
012200                                                                  PPCTT06
012300 LINKAGE SECTION.                                                 PPCTT06
012400                                                                  PPCTT06
012500 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT06
012600                                                                  PPCTT06
012700 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE.           PPCTT06
012800                                                                  PPCTT06
012900 0000-MAIN SECTION.                                               PPCTT06
013000                                                                  PPCTT06
013100     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT06
013200     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT06
013300       UNTIL CSRI-TABLE-TRANS-END.                                PPCTT06
013400     PERFORM 3000-FINISH-PREVIOUS-SET.                            PPCTT06
013500                                                                  PPCTT06
013600 0099-END.                                                        PPCTT06
013700                                                                  PPCTT06
013800     GOBACK.                                                      PPCTT06
013900                                                                  PPCTT06
014000 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT06
014100                                                                  PPCTT06
014200     MOVE 'PPCTT06'             TO XWHC-DISPLAY-MODULE.           PPCTT06
014300     COPY CPPDXWHC.                                               PPCTT06
014400     SET CTTI-NORMAL            TO TRUE.                          PPCTT06
014500     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTT06
014600     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTT06
014700                                   CTRI-TRANSACTION               PPCTT06
014800                                   CTRI-TRANSACTION-DISPO         PPCTT06
014900                                   CTRI-TEXT-LINE                 PPCTT06
015000                                   CTRI-MESSAGE-NUMBER.           PPCTT06
015100     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTT06
015200                                   CTTI-ERROR-SEVERITY.           PPCTT06
015300     MOVE WB-TABLE-NO           TO CSRI-CALL-TYPE-NUM.            PPCTT06
015400     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT06
015500                                                                  PPCTT06
015600 1099-INITIALIZATION-EXIT.                                        PPCTT06
015700     EXIT.                                                        PPCTT06
015800                                                                  PPCTT06
015900 2000-PROCESS-TRANSACTIONS SECTION.                               PPCTT06
016000                                                                  PPCTT06
016100     IF WS-SET-KEY NOT = WS-HOLD-KEY                              PPCTT06
016200        PERFORM 3000-FINISH-PREVIOUS-SET                          PPCTT06
016300        PERFORM 2100-BEGIN-NEW-SET                                PPCTT06
016400     END-IF.                                                      PPCTT06
016500     MOVE DATA-ELEMENT-TABLE-TRANS TO CTRI-TRANSACTION.           PPCTT06
016600     MOVE DETT-ACTION-CODE TO KCTE-ACTION.                        PPCTT06
016700     ADD 1 TO WC-SET-CNT.                                         PPCTT06
016800     IF DETT-ACTION-CODE = 'D'                                    PPCTT06
016900        SET WS-SET-END-OK TO TRUE                                 PPCTT06
017000        PERFORM 9100-CALL-PPCTLRPT                                PPCTT06
017100        PERFORM 9300-FETCH-NEXT-TRANSACTION                       PPCTT06
017200        GO TO 2099-EXIT                                           PPCTT06
017300     END-IF.                                                      PPCTT06
017400     IF DETT-TRAN-SEQ-NO-VALID                                    PPCTT06
017500        MOVE ZERO TO WC-TALLY                                     PPCTT06
017600        INSPECT WS-VALID-NEXT-SEQ-NOS TALLYING WC-TALLY           PPCTT06
017700            FOR ALL DETT-TRAN-SEQ-NO                              PPCTT06
017800        IF WC-TALLY = ZERO                                        PPCTT06
017900            MOVE MCT021 TO CTRI-MESSAGE-NUMBER                    PPCTT06
018000            PERFORM 9100-CALL-PPCTLRPT                            PPCTT06
018100            SET WS-SET-NOT-OK TO TRUE                             PPCTT06
018200        ELSE                                                      PPCTT06
018300            PERFORM 2200-MOVE-TRANSACTION-DATA                    PPCTT06
018400        END-IF                                                    PPCTT06
018500        EVALUATE TRUE                                             PPCTT06
018600          WHEN WS-ADD-SET                                         PPCTT06
018700            EVALUATE DETT-TRAN-SEQ-NO                             PPCTT06
018800              WHEN '1'                                            PPCTT06
018900                  MOVE '23456 ' TO WS-VALID-NEXT-SEQ-NOS          PPCTT06
019000                  SET WS-SET-END-OK TO TRUE                       PPCTT06
019100              WHEN '2'                                            PPCTT06
019200                  MOVE '3456  ' TO WS-VALID-NEXT-SEQ-NOS          PPCTT06
019300                  SET WS-SET-END-OK TO TRUE                       PPCTT06
019400              WHEN '3'                                            PPCTT06
019500                  MOVE '3456  ' TO WS-VALID-NEXT-SEQ-NOS          PPCTT06
019600                  SET WS-SET-END-OK TO TRUE                       PPCTT06
019700              WHEN '4'                                            PPCTT06
019800                  MOVE '56    ' TO WS-VALID-NEXT-SEQ-NOS          PPCTT06
019900                  SET WS-SET-END-OK TO TRUE                       PPCTT06
020000              WHEN '5'                                            PPCTT06
020100                  MOVE '6     ' TO WS-VALID-NEXT-SEQ-NOS          PPCTT06
020200                  SET WS-SET-END-OK TO TRUE                       PPCTT06
020300              WHEN '6'                                            PPCTT06
020400                  MOVE '6     ' TO WS-VALID-NEXT-SEQ-NOS          PPCTT06
020500                  SET WS-SET-END-OK TO TRUE                       PPCTT06
020600            END-EVALUATE                                          PPCTT06
020700          WHEN WS-CHANGE-SET                                      PPCTT06
020800            SET WS-SET-END-OK TO TRUE                             PPCTT06
020900        END-EVALUATE                                              PPCTT06
021000        IF CTRI-TRANSACTION NOT = SPACES                          PPCTT06
021100            PERFORM 9100-CALL-PPCTLRPT                            PPCTT06
021200        END-IF                                                    PPCTT06
021300     ELSE                                                         PPCTT06
021400        MOVE MCT020 TO CTRI-MESSAGE-NUMBER                        PPCTT06
021500        PERFORM 9100-CALL-PPCTLRPT                                PPCTT06
021600        SET WS-SET-NOT-OK TO TRUE                                 PPCTT06
021700     END-IF.                                                      PPCTT06
021800                                                                  PPCTT06
021900     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT06
022000                                                                  PPCTT06
022100 2099-EXIT.                                                       PPCTT06
022200     EXIT.                                                        PPCTT06
022300                                                                  PPCTT06
022400 2100-BEGIN-NEW-SET SECTION.                                      PPCTT06
022500                                                                  PPCTT06
022600     MOVE WS-SET-KEY TO WS-HOLD-KEY.                              PPCTT06
022700     SET WS-SET-OK TO TRUE.                                       PPCTT06
022800     SET WS-SET-END-NOT-OK TO TRUE.                               PPCTT06
022900     MOVE ZERO TO WC-SET-CNT.                                     PPCTT06
023000     MOVE SPACES TO DATA-ELEMENT-TABLE-INPUT.                     PPCTT06
023100     MOVE DETT-CARD-TYPE TO DETI-CARD-TYPE.                       PPCTT06
023200     MOVE DETT-ELEM-NO   TO DETI-ELEM-NO-X.                       PPCTT06
023300     EVALUATE TRUE                                                PPCTT06
023400       WHEN WS-ADD-SET                                            PPCTT06
023500           MOVE '1     ' TO WS-VALID-NEXT-SEQ-NOS                 PPCTT06
023600       WHEN WS-CHANGE-SET                                         PPCTT06
023700           MOVE '123456' TO WS-VALID-NEXT-SEQ-NOS                 PPCTT06
023800     END-EVALUATE.                                                PPCTT06
023900                                                                  PPCTT06
024000 2199-EXIT.                                                       PPCTT06
024100     EXIT.                                                        PPCTT06
024200                                                                  PPCTT06
024300 2200-MOVE-TRANSACTION-DATA SECTION.                              PPCTT06
024400                                                                  PPCTT06
024500     EVALUATE DETT-TRAN-SEQ-NO                                    PPCTT06
024600      WHEN '1'                                                    PPCTT06
024700        IF DETT-CARD-START-POS  NOT = SPACES                      PPCTT06
024800           MOVE DETT-CARD-START-POS    TO DETI-CARD-START-POS-X   PPCTT06
024900        END-IF                                                    PPCTT06
025000        IF DETT-DATA-TYPE       NOT = SPACES                      PPCTT06
025100           MOVE DETT-DATA-TYPE         TO DETI-DATA-TYPE          PPCTT06
025200        END-IF                                                    PPCTT06
025300        IF DETT-EXT-LENGTH      NOT = SPACES                      PPCTT06
025400           MOVE DETT-EXT-LENGTH        TO DETI-EXT-LENGTH-X       PPCTT06
025500        END-IF                                                    PPCTT06
025600        IF DETT-DEC-PLACE       NOT = SPACES                      PPCTT06
025700           MOVE DETT-DEC-PLACE         TO DETI-DEC-PLACE-X        PPCTT06
025800        END-IF                                                    PPCTT06
025900        IF DETT-LEGAL-TO-UPD    NOT = SPACES                      PPCTT06
026000           MOVE DETT-LEGAL-TO-UPD      TO DETI-LEGAL-TO-UPD       PPCTT06
026100        END-IF                                                    PPCTT06
026200        IF DETT-ELEMENT-NAME    NOT = SPACES                      PPCTT06
026300           MOVE DETT-ELEMENT-NAME      TO DETI-ELEMENT-NAME       PPCTT06
026400        END-IF                                                    PPCTT06
026500        IF DETT-ST-KEY-POS      NOT = SPACES                      PPCTT06
026600           MOVE DETT-ST-KEY-POS        TO DETI-ST-KEY-POS-X       PPCTT06
026700        END-IF                                                    PPCTT06
026800        IF DETT-ST-MANDATORY    NOT = SPACES                      PPCTT06
026900           MOVE DETT-ST-MANDATORY      TO DETI-ST-MANDATORY       PPCTT06
027000        END-IF                                                    PPCTT06
027100        IF DETT-ST-MSG-ROUTE    NOT = SPACES                      PPCTT06
027200           MOVE DETT-ST-MSG-ROUTE      TO DETI-ST-MSG-ROUTE       PPCTT06
027300        END-IF                                                    PPCTT06
027400        IF DETT-ST-NOTPYRL      NOT = SPACES                      PPCTT06
027500           MOVE DETT-ST-NOTPYRL        TO DETI-ST-NOTPYRL         PPCTT06
027600        END-IF                                                    PPCTT06
027700        IF DETT-DATE-EDIT       NOT = SPACES                      PPCTT06
027800           MOVE DETT-DATE-EDIT         TO DETI-DATE-EDIT          PPCTT06
027900        END-IF                                                    PPCTT06
028000        IF DETT-TBLEDT-RTN      NOT = SPACES                      PPCTT06
028100           MOVE DETT-TBLEDT-RTN        TO DETI-TBLEDT-RTN-X       PPCTT06
028200        END-IF                                                    PPCTT06
028300        IF DETT-MAIN-BAL-TYPE   NOT = SPACES                      PPCTT06
028400           MOVE DETT-MAIN-BAL-TYPE     TO DETI-MAIN-BAL-TYPE      PPCTT06
028500        END-IF                                                    PPCTT06
028600      WHEN '2'                                                    PPCTT06
028700        IF DETT-DB2-TABLE-ID    NOT = SPACES                      PPCTT06
028800           MOVE DETT-DB2-TABLE-ID      TO DETI-DB2-TABLE-ID       PPCTT06
028900        END-IF                                                    PPCTT06
029000      WHEN '3'                                                    PPCTT06
029100        IF DETT-TRAN-3-CONTD        = SPACE                       PPCTT06
029200           MOVE DETT-VAL-RNG-EDTS-GRP  TO DETI-3-DATA             PPCTT06
029300        ELSE                                                      PPCTT06
029400           PERFORM VARYING IX1 FROM 69 BY -1                      PPCTT06
029500             UNTIL IX1 = ZERO                                     PPCTT06
029600                OR DETT-VAL-RNG-EDTS (IX1) NOT = SPACE            PPCTT06
029700           END-PERFORM                                            PPCTT06
029800           PERFORM VARYING IX2 FROM 93 BY -1                      PPCTT06
029900             UNTIL IX2 = ZERO                                     PPCTT06
030000                OR DETI-VAL-RNG-EDTS (IX2) NOT = SPACE            PPCTT06
030100           END-PERFORM                                            PPCTT06
030200           IF IX1 + IX2 > 93                                      PPCTT06
030300              MOVE M01059 TO CTRI-MESSAGE-NUMBER                  PPCTT06
030400              PERFORM 9100-CALL-PPCTLRPT                          PPCTT06
030500              SET WS-SET-NOT-OK TO TRUE                           PPCTT06
030600           ELSE                                                   PPCTT06
030700              PERFORM VARYING IX3 FROM +1 BY +1                   PPCTT06
030800                UNTIL IX3 > IX1                                   PPCTT06
030900               ADD +1 TO IX2                                      PPCTT06
031000               MOVE DETT-VAL-RNG-EDTS (IX3)                       PPCTT06
031100                 TO DETI-VAL-RNG-EDTS (IX2)                       PPCTT06
031200              END-PERFORM                                         PPCTT06
031300           END-IF                                                 PPCTT06
031400        END-IF                                                    PPCTT06
031500      WHEN '4'                                                    PPCTT06
031600        IF DETT-CON-E-RTNS-GRP  NOT = SPACES                      PPCTT06
031700           MOVE DETT-CON-E-RTNS-GRP    TO DETI-4-DATA             PPCTT06
031800        END-IF                                                    PPCTT06
031900      WHEN '5'                                                    PPCTT06
032000        IF DETT-GTN-E-RTNS-GRP  NOT = SPACES                      PPCTT06
032100           MOVE DETT-CON-E-RTNS-GRP    TO DETI-5-DATA             PPCTT06
032200        END-IF                                                    PPCTT06
032300      WHEN '6'                                                    PPCTT06
032400        IF DETT-TRAN-6-CONTD        = SPACE                       PPCTT06
032500           MOVE DETT-IMP-E-RTNS-GRP    TO DETI-6-DATA             PPCTT06
032600        ELSE                                                      PPCTT06
032700           PERFORM VARYING IX1 FROM +1 BY +1                      PPCTT06
032800             UNTIL IX1 = 23                                       PPCTT06
032900                OR DETT-IMP-E-RTNS-X (IX1) = SPACES               PPCTT06
033000           END-PERFORM                                            PPCTT06
033100           PERFORM VARYING IX2 FROM +1 BY +1                      PPCTT06
033200             UNTIL IX2 = 24                                       PPCTT06
033300                OR DETI-IMP-E-RTNS-X (IX2) = SPACES               PPCTT06
033400           END-PERFORM                                            PPCTT06
033500           IF IX1 + IX2 > 24                                      PPCTT06
033600              MOVE M01914 TO CTRI-MESSAGE-NUMBER                  PPCTT06
033700              PERFORM 9100-CALL-PPCTLRPT                          PPCTT06
033800              SET WS-SET-NOT-OK TO TRUE                           PPCTT06
033900           ELSE                                                   PPCTT06
034000              PERFORM VARYING IX3 FROM +1 BY +1                   PPCTT06
034100                UNTIL IX3 > IX1                                   PPCTT06
034200               MOVE DETT-IMP-E-RTNS-X (IX3)                       PPCTT06
034300                 TO DETI-IMP-E-RTNS-X (IX2)                       PPCTT06
034400               ADD +1 TO IX2                                      PPCTT06
034500              END-PERFORM                                         PPCTT06
034600           END-IF                                                 PPCTT06
034700        END-IF                                                    PPCTT06
034800     END-EVALUATE.                                                PPCTT06
034900                                                                  PPCTT06
035000 2299-EXIT.                                                       PPCTT06
035100     EXIT.                                                        PPCTT06
035200                                                                  PPCTT06
035300 3000-FINISH-PREVIOUS-SET SECTION.                                PPCTT06
035400                                                                  PPCTT06
035500     IF WS-HOLD-KEY NOT = LOW-VALUES                              PPCTT06
035600       IF WS-SET-END-NOT-OK                                       PPCTT06
035700          MOVE MCT023 TO CTRI-MESSAGE-NUMBER                      PPCTT06
035800          PERFORM 9100-CALL-PPCTLRPT                              PPCTT06
035900          SET WS-SET-NOT-OK TO TRUE                               PPCTT06
036000       END-IF                                                     PPCTT06
036100       IF WS-SET-OK                                               PPCTT06
036200          SET WF-NO-ERRORS TO TRUE                                PPCTT06
036300          MOVE CTTI-ACTION-DATE TO KCTE-LAST-ACTION-DT            PPCTT06
036400          CALL 'PPCTDETE' USING CONTROL-TABLE-EDIT-INTERFACE      PPCTT06
036500                                DATA-ELEMENT-TABLE-INPUT          PPCTT06
036600                                DATA-ELEMENT-TABLE-DATA           PPCTT06
036700          IF KCTE-ERR > ZERO                                      PPCTT06
036800             PERFORM 3100-PRINT-ERROR                             PPCTT06
036900               VARYING WC-ERR-IX FROM 1 BY 1                      PPCTT06
037000                 UNTIL WC-ERR-IX > KCTE-ERR                       PPCTT06
037100             IF WF-REJECT-SET OR KCTE-STATUS-NOT-COMPLETED        PPCTT06
037200                MOVE MCT022 TO CTRI-MESSAGE-NUMBER                PPCTT06
037300                SET WS-SET-NOT-OK TO TRUE                         PPCTT06
037400             END-IF                                               PPCTT06
037500          END-IF                                                  PPCTT06
037600       END-IF                                                     PPCTT06
037700       IF WS-SET-OK                                               PPCTT06
037800          MOVE 'SET ACCEPTED' TO CTRI-TEXT-DISPO                  PPCTT06
037900          ADD WC-SET-CNT TO CTTI-TRANS-ACCEPTED                   PPCTT06
038000          PERFORM 3200-UPDATE-DB2-TABLE                           PPCTT06
038100       ELSE                                                       PPCTT06
038200          MOVE 'SET REJECTED' TO CTRI-TEXT-DISPO                  PPCTT06
038300          ADD WC-SET-CNT TO CTTI-TRANS-REJECTED                   PPCTT06
038400       END-IF                                                     PPCTT06
038500       PERFORM 9100-CALL-PPCTLRPT                                 PPCTT06
038600       MOVE 2 TO CTRI-SPACING-OVERRIDE                            PPCTT06
038700     END-IF.                                                      PPCTT06
038800                                                                  PPCTT06
038900 3099-EXIT.                                                       PPCTT06
039000     EXIT.                                                        PPCTT06
039100                                                                  PPCTT06
039200 3100-PRINT-ERROR SECTION.                                        PPCTT06
039300                                                                  PPCTT06
039400     MOVE KCTE-ERR-NO (WC-ERR-IX) TO CTRI-MESSAGE-NUMBER.         PPCTT06
039500     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT06
039600     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT06
039700         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT06
039800     END-IF.                                                      PPCTT06
039900                                                                  PPCTT06
040000 3199-EXIT.                                                       PPCTT06
040100     EXIT.                                                        PPCTT06
040200                                                                  PPCTT06
040300 3200-UPDATE-DB2-TABLE SECTION.                                   PPCTT06
040400                                                                  PPCTT06
040500     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT06
040600     CALL 'PPCTDETU' USING CONTROL-TABLE-UPDT-INTERFACE,          PPCTT06
040700                           DATA-ELEMENT-TABLE-DATA.               PPCTT06
040800     IF KCTU-STATUS-OK                                            PPCTT06
040900        EVALUATE TRUE                                             PPCTT06
041000          WHEN KCTU-ACTION-ADD                                    PPCTT06
041100            ADD 1 TO CTTI-ENTRIES-ADDED                           PPCTT06
041200          WHEN KCTU-ACTION-CHANGE                                 PPCTT06
041300            ADD 1 TO CTTI-ENTRIES-UPDATED                         PPCTT06
041400          WHEN KCTU-ACTION-DELETE                                 PPCTT06
041500            ADD 1 TO CTTI-ENTRIES-DELETED                         PPCTT06
041600          WHEN OTHER                                              PPCTT06
041700            GO TO 3300-BAD-UPDATE-CALL                            PPCTT06
041800        END-EVALUATE                                              PPCTT06
041900     ELSE                                                         PPCTT06
042000        GO TO 3300-BAD-UPDATE-CALL                                PPCTT06
042100     END-IF.                                                      PPCTT06
042200                                                                  PPCTT06
042300 3299-EXIT.                                                       PPCTT06
042400     EXIT.                                                        PPCTT06
042500                                                                  PPCTT06
042600 3300-BAD-UPDATE-CALL SECTION.                                    PPCTT06
042700                                                                  PPCTT06
042800     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT06
042900     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT06
043000     STRING 'PPCTDETU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT06
043100            '" STATUS WAS "'               KCTU-STATUS            PPCTT06
043200            '" COUNT='                     WA-WORK-Z5             PPCTT06
043300            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT06
043400     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT06
043500     GO TO 9999-ABEND.                                            PPCTT06
043600                                                                  PPCTT06
043700 3399-EXIT.                                                       PPCTT06
043800     EXIT.                                                        PPCTT06
043900                                                                  PPCTT06
044000 9100-CALL-PPCTLRPT SECTION.                                      PPCTT06
044100                                                                  PPCTT06
044200     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT06
044300     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT06
044400         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT06
044500     END-IF.                                                      PPCTT06
044600     IF CTRI-RUN-ABORTED                                          PPCTT06
044700        GO TO 9999-ABEND                                          PPCTT06
044800     END-IF.                                                      PPCTT06
044900                                                                  PPCTT06
045000 9199-EXIT.                                                       PPCTT06
045100     EXIT.                                                        PPCTT06
045200                                                                  PPCTT06
045300 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT06
045400                                                                  PPCTT06
045500     PERFORM WITH TEST AFTER                                      PPCTT06
045600       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT06
045700          OR CSRI-TRANSACTION(1:1) NOT = '*'                      PPCTT06
045800      CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE        PPCTT06
045900      IF CSRI-NORMAL                                              PPCTT06
046000         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT06
046100         IF CSRI-TRANSACTION(1:1) = '*'                           PPCTT06
046200            MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION             PPCTT06
046300            MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO     PPCTT06
046400            PERFORM 9100-CALL-PPCTLRPT                            PPCTT06
046500            ADD 1 TO CTTI-TRANS-ACCEPTED                          PPCTT06
046600         ELSE                                                     PPCTT06
046700            MOVE CSRI-TRANSACTION TO DATA-ELEMENT-TABLE-TRANS     PPCTT06
046800            MOVE DETT-ACTION-CODE TO WS-SET-ACTION                PPCTT06
046900            MOVE DETT-CARD-TYPE   TO WS-SET-CARD-TYPE             PPCTT06
047000            MOVE DETT-ELEM-NO     TO WS-SET-ELEM-NO               PPCTT06
047100         END-IF                                                   PPCTT06
047200      ELSE                                                        PPCTT06
047300        IF NOT CSRI-TABLE-TRANS-END                               PPCTT06
047400           MOVE MCT001              TO CTRI-MESSAGE-NUMBER        PPCTT06
047500           MOVE CSRI-RETURN-CODE    TO WA-RTN-CODE                PPCTT06
047600           MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                PPCTT06
047700           MOVE WA-READ-ERROR       TO CTRI-TEXT-LINE             PPCTT06
047800           PERFORM 9100-CALL-PPCTLRPT                             PPCTT06
047900           GO TO 9999-ABEND                                       PPCTT06
048000        END-IF                                                    PPCTT06
048100      END-IF                                                      PPCTT06
048200     END-PERFORM.                                                 PPCTT06
048300                                                                  PPCTT06
048400 9399-EXIT.                                                       PPCTT06
048500     EXIT.                                                        PPCTT06
048600                                                                  PPCTT06
048700 9999-ABEND SECTION.                                              PPCTT06
048800                                                                  PPCTT06
048900     SET CTTI-ABORT TO TRUE.                                      PPCTT06
049000     GOBACK.                                                      PPCTT06
049100*************************END OF SOURCE CODE***********************PPCTT06
