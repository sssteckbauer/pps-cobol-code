000100**************************************************************/   14420448
000200*  PROGRAM:  PPBENXVA                                        */   14420448
000300*  RELEASE # ___0448___   SERVICE REQUEST NO(S) __1442______ */   14420448
000400*  NAME ___M. SANO_____   MODIFICATION DATE ____02/08/90_____*/   14420448
000500*  DESCRIPTION                                               */   14420448
000600*      - DELETED PPBUTUTL INTERFACE SINCE PPBENRET READS     */   14420448
000700*        CONTROL FILE DIRECTLY TO VERIFY BRT KEYS AND        */   14420448
000800*        RETREIVE BRT RATES.                                 */   14420448
000900**************************************************************/   14420448
000100**************************************************************/   30930413
000200*  PROGRAM: PPBENXVA                                         */   30930413
000300*  RELEASE: ____0413____  SERVICE REQUEST(S): ____3093____   */   30930413
000400*  NAME:____JIM WILLIAMS__CREATION DATE:      __05/23/89__   */   30930413
000500*  DESCRIPTION:                                              */   30930413
000600*  - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II. */   30930413
000700**************************************************************/   30930413
000800**************************************************************/   14240278
000900*  PROGRAM:  PPBENXVA                                        */   14240278
001000*  REL#:  0278   REF: NONE  SERVICE REQUESTS:   ____1424____ */   14240278
001100*  NAME ___BMB_________   MODIFICATION DATE ____01/22/87_____*/   14240278
001200*  DESCRIPTION                                               */   14240278
001300*      THIS CODE WAS REMOVED FROM USER40 AND PLACED IN       */   14240278
001400*   THIS STAND ALONE CALLED MODULE.                          */   14240278
001500*                                                            */   14240278
001600**************************************************************/   14240278
001700 IDENTIFICATION DIVISION.                                         PPBENXVA
001800 PROGRAM-ID. PPBENXVA.                                            PPBENXVA
001900*SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
002000*OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
002100 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENXVA
002200 AUTHOR.                                                          PPBENXVA
002300******************************************************************PPBENXVA
002400******************************************************************PPBENXVA
002500*NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE*PPBENXVA
002600******************************************************************PPBENXVA
002700******************************************************************PPBENXVA
002800* CONSULTANT SERVICES PROVIDED BY:                               *PPBENXVA
002900*                 BRUCE M. BRISCOE                               *PPBENXVA
003000*                 CREATIVELY APPLIED TECHNOLOGIES, INC.          *PPBENXVA
003100*                 7548 BRAIDBURN AVE.                            *PPBENXVA
003200*                 NEWARK, CA 94560                               *PPBENXVA
003300*                 (415) 791-7636                                 *PPBENXVA
003400******************************************************************PPBENXVA
003500******************************************************************PPBENXVA
003600*NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE*PPBENXVA
003700******************************************************************PPBENXVA
003800******************************************************************PPBENXVA
003900 DATE-WRITTEN.                                                    PPBENXVA
004000 DATE-COMPILED.                                                   PPBENXVA
004100*REMARKS.                                                         30930413
004200******************************************************************PPBENXVA
004300*                                                                *PPBENXVA
004400*  THIS MODULE PERFORMS VISION ALTERNATE RATE AND PREMIUM        *PPBENXVA
004500* CALCULATIONS.                                                  *PPBENXVA
004600*                                                                *PPBENXVA
004700*  RECOMMENDED PRACTICE: CALLING PROGRAM SHOULD CALL THIS        *PPBENXVA
004800* MODULE TO 'INITIALIZE' DURING CALLING PROGRAM INITIALIZATION.  *PPBENXVA
004900*                                                                *PPBENXVA
005000*                                                                *PPBENXVA
005100******************************************************************PPBENXVA
005200 ENVIRONMENT DIVISION.                                            PPBENXVA
005300 CONFIGURATION SECTION.                                           PPBENXVA
005400 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
005500 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
005600 SPECIAL-NAMES.                                                   PPBENXVA
005700 INPUT-OUTPUT SECTION.                                            PPBENXVA
005800 FILE-CONTROL.                                                    PPBENXVA
005900     SKIP3                                                        PPBENXVA
006000******************************************************************PPBENXVA
006100*                                                                *PPBENXVA
006200*                         DATA DIVISION                          *PPBENXVA
006300*                                                                *PPBENXVA
006400******************************************************************PPBENXVA
006500 DATA DIVISION.                                                   PPBENXVA
006600 FILE SECTION.                                                    PPBENXVA
006700     SKIP1                                                        PPBENXVA
006800     EJECT                                                        PPBENXVA
006900******************************************************************PPBENXVA
007000*                                                                *PPBENXVA
007100*                    WORKING-STORAGE SECTION                     *PPBENXVA
007200*                                                                *PPBENXVA
007300******************************************************************PPBENXVA
007400 WORKING-STORAGE SECTION.                                         PPBENXVA
007500     SKIP1                                                        PPBENXVA
007600 01  WS-ID                        PIC X(37)                       PPBENXVA
007700     VALUE 'PPBENXVA WORKING-STORAGE BEGINS HERE'.                PPBENXVA
007800     SKIP3                                                        PPBENXVA
007900 01  COVERAGE-SUBSCRIPT              PIC 9(04)  COMP.             PPBENXVA
008000     SKIP3                                                        PPBENXVA
008100*    *******************************************                  PPBENXVA
008200*    * USED TO GET PREMIUM AMOUNT BY PLAN CODE                    PPBENXVA
008300*    *******************************************                  PPBENXVA
008400 01  FLAGS-AND-SWITCHES.                                          PPBENXVA
008500     05  PROGRAM-STATUS-FLAG         PIC X(01)   VALUE '0'.       PPBENXVA
008600         88  PROGRAM-STATUS-NORMAL               VALUE '0'.       PPBENXVA
008700         88  PROGRAM-STATUS-EXITING              VALUE '1'.       PPBENXVA
008800     05  FOUND-RATE-SW               PIC X(01)   VALUE 'N'.       PPBENXVA
008900         88  FOUND-RATE                          VALUE 'Y'.       PPBENXVA
009000     SKIP1                                                        PPBENXVA
009100 01  HOLD-VALUES-AREA.                                            PPBENXVA
009200*    *************************************************************PPBENXVA
009300*    * THE FOLLOWING IS USED TO HOLD DATA                        *PPBENXVA
009400*    *************************************************************PPBENXVA
009500     05  WS-HOLD-93                  PIC 9(03).                   PPBENXVA
009600     05  WS-NUM-SEGS                 PIC 9(02).                   PPBENXVA
009700     SKIP1                                                        PPBENXVA
009800 01  EDIT-VALUES-AREA.                                            PPBENXVA
009900*    *************************************************************PPBENXVA
010000*    * THE FOLLOWING IS USED TO EDIT DATA                        *PPBENXVA
010100*    *************************************************************PPBENXVA
010200     05  WS-EDIT-X3                  PIC X(03).                   PPBENXVA
010300     SKIP1                                                        PPBENXVA
010400 01  CONSTANT-VALUES.                                             PPBENXVA
010500*    *************************************************************PPBENXVA
010600*    * THE FOLLOWING SHOULD BE THE SAME VALUE AS FOR THE OCCURS ABPPBENXVA
010700*    *************************************************************PPBENXVA
010800     05  CONSTANT-BENEFIT-TYPE       PIC X(01)   VALUE 'A'.       PPBENXVA
010900     05  CONSTANT-NORMAL             PIC X(01)   VALUE '0'.       PPBENXVA
011000     05  CONSTANT-ABORT              PIC X(01)   VALUE '1'.       PPBENXVA
011100     05  CONSTANT-EXIT-PROGRAM       PIC X(01)   VALUE '1'.       PPBENXVA
011200     05  CONSTANT-PREMIUM-CALC       PIC X(01)   VALUE '3'.       PPBENXVA
012200 01  WS-DEFAULT-BRSC          PIC X(05) VALUE '00   '.            14420448
011300     EJECT                                                        PPBENXVA
011400 01  PPBENRET-INTERFACE.          COPY 'CPLNKRET'.                PPBENXVA
011500     EJECT                                                        PPBENXVA
011600 01  XUTF-UTILITY-FUNCTIONS.                                      30930413
011700                                  COPY 'CPWSXUTF'.                PPBENXVA
011800     EJECT                                                        PPBENXVA
011900 01  XBRT-BENEFITS-RATES-RECORD.                                  30930413
012000                                  COPY 'CPWSXBRT'.                PPBENXVA
012100     EJECT                                                        PPBENXVA
012200 01  XCFK-CONTROL-FILE-KEYS.                                      30930413
012300                                  COPY 'CPWSXCFK'.                PPBENXVA
012400     SKIP3                                                        PPBENXVA
012500 01  FILLER                       PIC X(37)                       PPBENXVA
012600     VALUE 'PPBENXVA WORKING-STORAGE ENDS HERE'.                  PPBENXVA
012700     EJECT                                                        PPBENXVA
012800******************************************************************PPBENXVA
012900*                                                                *PPBENXVA
013000*                        LINKAGE SECTION                         *PPBENXVA
013100*                                                                *PPBENXVA
013200******************************************************************PPBENXVA
013300 LINKAGE SECTION.                                                 PPBENXVA
013400     SKIP1                                                        PPBENXVA
013500 01  PPBENXVA-INTERFACE.          COPY 'CPLNKXVA'.                PPBENXVA
013600     EJECT                                                        PPBENXVA
014700*01  PPBUTUTL-INTERFACE.                                          14420448
014800*****                             COPY 'CPWSBUTI'.                14420448
013900     EJECT                                                        PPBENXVA
014000 01  CTL-INTERFACE.                                               30930413
014100                                  COPY 'CPWSXCIF'.                PPBENXVA
014200     SKIP3                                                        PPBENXVA
014300 01  CTL-SEGMENT-TABLE.                                           30930413
014400                                  COPY 'CPWSXCST'.                PPBENXVA
014500     EJECT                                                        PPBENXVA
014600******************************************************************PPBENXVA
014700*                                                                *PPBENXVA
014800*                       PROCEDURE DIVISION                       *PPBENXVA
014900*                                                                *PPBENXVA
015000******************************************************************PPBENXVA
015100     SKIP1                                                        PPBENXVA
015200 PROCEDURE DIVISION USING PPBENXVA-INTERFACE                      PPBENXVA
016300*****                     PPBUTUTL-INTERFACE                      14420448
015400                          CTL-INTERFACE                           PPBENXVA
015500                          CTL-SEGMENT-TABLE.                      PPBENXVA
015600     SKIP1                                                        PPBENXVA
015700 A000-MAINLINE SECTION.                                           PPBENXVA
015800     SKIP2                                                        PPBENXVA
015900     MOVE CONSTANT-NORMAL TO PROGRAM-STATUS-FLAG.                 PPBENXVA
016000     SKIP1                                                        PPBENXVA
016100     PERFORM B010-INITIALIZE                                      PPBENXVA
016200        THRU B019-INITIALIZE-EXIT.                                PPBENXVA
016300     SKIP1                                                        PPBENXVA
016400     IF PROGRAM-STATUS-NORMAL                                     PPBENXVA
016500     THEN                                                         PPBENXVA
016600        IF KXVA-ACTION-RATE-RETRIEVAL OR                          PPBENXVA
016700           KXVA-ACTION-PREMIUM-CALC                               PPBENXVA
016800        THEN                                                      PPBENXVA
016900            PERFORM C010-GET-RATE                                 PPBENXVA
017000               THRU C019-GET-RATE-EXIT                            PPBENXVA
017100            IF PROGRAM-STATUS-NORMAL    AND                       PPBENXVA
017200               KXVA-ACTION-PREMIUM-CALC AND                       PPBENXVA
017300               KRET-SEGMENT-RETRIEVED                             PPBENXVA
017400            THEN                                                  PPBENXVA
017500                PERFORM D010-CALCULATE                            PPBENXVA
017600                   THRU D019-CALCULATE-EXIT                       PPBENXVA
017700            ELSE                                                  PPBENXVA
017800                NEXT SENTENCE.                                    PPBENXVA
017900     SKIP3                                                        PPBENXVA
018000 A009-GOBACK.                                                     PPBENXVA
018100     GOBACK.                                                      PPBENXVA
018200     EJECT                                                        PPBENXVA
018300 B000-INITIALIZATION SECTION.                                     PPBENXVA
018400     SKIP2                                                        PPBENXVA
018500 B010-INITIALIZE.                                                 PPBENXVA
018600     SKIP1                                                        PPBENXVA
018700     MOVE CONSTANT-NORMAL TO KXVA-RETURN-STATUS-FLAG.             PPBENXVA
018800*                                                                 PPBENXVA
018900******************************************************************PPBENXVA
019000*      SET NUMERIC RETURN FIELDS TO ZERO                       *  PPBENXVA
019100******************************************************************PPBENXVA
019200*                                                                 PPBENXVA
019300     MOVE ZERO TO KXVA-AMOUNT                                     PPBENXVA
019400                  KXVA-CONTRIB-AMOUNT.                            PPBENXVA
019500     SKIP1                                                        PPBENXVA
019600*                                                                 PPBENXVA
019700******************************************************************PPBENXVA
019800*      SET UP FIELDS FOR PPBENRET CALL                         *  PPBENXVA
019900******************************************************************PPBENXVA
020000*                                                                 PPBENXVA
020100     MOVE CONSTANT-PREMIUM-CALC TO KRET-ACTION-FLAG.              PPBENXVA
020200     SKIP1                                                        PPBENXVA
020300     IF KXVA-ACTION-RATE-RETRIEVAL OR                             PPBENXVA
020400        KXVA-ACTION-PREMIUM-CALC                                  PPBENXVA
020500         PERFORM B030-VALIDATE-LOOKUP-ARGS                        PPBENXVA
020600            THRU B039-VALIDATE-LOOKUP-ARGS-EXIT                   PPBENXVA
020700     ELSE                                                         PPBENXVA
020800         NEXT SENTENCE.                                           PPBENXVA
020900     SKIP3                                                        PPBENXVA
021000 B019-INITIALIZE-EXIT.                                            PPBENXVA
021100     EXIT.                                                        PPBENXVA
021200     SKIP3                                                        PPBENXVA
021300 B030-VALIDATE-LOOKUP-ARGS.                                       PPBENXVA
021400     SKIP1                                                        PPBENXVA
021500     MOVE '0' TO KXVA-NOT-ENROLLED-IN-XVA-FLAG.                   PPBENXVA
021600     SKIP1                                                        PPBENXVA
021700     IF KXVA-VALID-PLAN-CODE                                      PPBENXVA
021800         NEXT SENTENCE                                            PPBENXVA
021900     ELSE                                                         PPBENXVA
022000         MOVE '1' TO KXVA-NOT-ENROLLED-IN-XVA-FLAG.               PPBENXVA
022100     SKIP1                                                        PPBENXVA
022200     IF KXVA-INVALID-SYSTEM-CODE                                  PPBENXVA
022300         MOVE '1' TO KXVA-NOT-ENROLLED-IN-XVA-FLAG                PPBENXVA
022400     ELSE                                                         PPBENXVA
022500         NEXT SENTENCE.                                           PPBENXVA
022600     SKIP1                                                        PPBENXVA
022700     IF KXVA-INVALID-ALT-HISTORY                                  PPBENXVA
022800         MOVE '1' TO KXVA-NOT-ENROLLED-IN-XVA-FLAG                PPBENXVA
022900     ELSE                                                         PPBENXVA
023000         NEXT SENTENCE.                                           PPBENXVA
023100     SKIP1                                                        PPBENXVA
023200     IF KXVA-NOT-ENROLLED-IN-XVA                                  PPBENXVA
023300         MOVE CONSTANT-ABORT TO PROGRAM-STATUS-FLAG.              PPBENXVA
023400     SKIP3                                                        PPBENXVA
023500 B039-VALIDATE-LOOKUP-ARGS-EXIT.                                  PPBENXVA
023600     EXIT.                                                        PPBENXVA
023700     EJECT                                                        PPBENXVA
023800 C000-RATE-RETRIEVAL SECTION.                                     PPBENXVA
023900     SKIP2                                                        PPBENXVA
024000 C010-GET-RATE.                                                   PPBENXVA
024100     SKIP1                                                        PPBENXVA
024200     PERFORM R010-CALL-PPBENRET                                   PPBENXVA
024300        THRU R019-CALL-PPBENRET-EXIT.                             PPBENXVA
024400     SKIP1                                                        PPBENXVA
024500     IF KRET-SEGMENT-RETRIEVED                                    PPBENXVA
024600         MOVE KRET-SEGMENT TO XBRT-BENEFITS-RATES-RECORD          PPBENXVA
024700*                                                                 PPBENXVA
024800*        THE 6TH OCCURANCE CONTAINS THE ALT VISION DATA           PPBENXVA
024900*                                                                 PPBENXVA
025000         MOVE XBRT-RETIRE-RATE (6) TO KXVA-AMOUNT                 PPBENXVA
025100     ELSE                                                         PPBENXVA
025200         MOVE CONSTANT-ABORT TO KXVA-RETURN-STATUS-FLAG.          PPBENXVA
025300     SKIP3                                                        PPBENXVA
025400 C019-GET-RATE-EXIT.                                              PPBENXVA
025500     EXIT.                                                        PPBENXVA
025600     EJECT                                                        PPBENXVA
025700 D000-XVA-CALC SECTION.                                           PPBENXVA
025800     SKIP2                                                        PPBENXVA
025900 D010-CALCULATE.                                                  PPBENXVA
026000     SKIP1                                                        PPBENXVA
026100     MOVE KXVA-AMOUNT TO KXVA-CONTRIB-AMOUNT.                     PPBENXVA
026200     SKIP3                                                        PPBENXVA
026300 D019-CALCULATE-EXIT.                                             PPBENXVA
026400     EXIT.                                                        PPBENXVA
026500     EJECT                                                        PPBENXVA
026600 R000-SUB-PROGRAM-CALLS SECTION.                                  PPBENXVA
026700     SKIP2                                                        PPBENXVA
026800 R010-CALL-PPBENRET.                                              PPBENXVA
026900     SKIP1                                                        PPBENXVA
028000     MOVE WS-DEFAULT-BRSC TO KRET-BRSC.                           14420448
027000     CALL 'PPBENRET' USING PPBENRET-INTERFACE                     PPBENXVA
028200*****                      PPBUTUTL-INTERFACE                     14420448
027200                           CTL-INTERFACE                          PPBENXVA
027300                           CTL-SEGMENT-TABLE.                     PPBENXVA
027400     SKIP3                                                        PPBENXVA
027500 R019-CALL-PPBENRET-EXIT.                                         PPBENXVA
027600     EXIT.                                                        PPBENXVA
