000100**************************************************************/   36060628
000200*  PROGRAM: PPFNAUTL                                         */   36060628
000300*  RELEASE: ____0628____  SERVICE REQUEST(S): ____3606____   */   36060628
000400*  NAME:__G. STEINITZ_____CREATION DATE:      __01/10/92__   */   36060628
000500*  DESCRIPTION:                                              */   36060628
000600*  - READS ALL FNA ROWS FROM EDB FOR AN EMPLOYEE AND PUTS    */   36060628
000700*    THEM INTO FINAID-ARRAY                                  */   36060628
000800*                                                            */   36060628
000900*  - USES PPFNAUTL-EMPLOYEE-ID AS INPUT.                     */   36060628
001000*                                                            */   36060628
001100*  - SETS PPFNAUTL-ERROR-SW AND PPFNAUTL-ROW-COUNT.          */   36060628
001200**************************************************************/   36060628
001300     SKIP2                                                        PPFNAUTL
001400 IDENTIFICATION DIVISION.                                         PPFNAUTL
001500     SKIP1                                                        PPFNAUTL
001600 PROGRAM-ID. PPFNAUTL.                                            PPFNAUTL
001700     SKIP1                                                        PPFNAUTL
001800 AUTHOR. UCOP.                                                    PPFNAUTL
001900     SKIP1                                                        PPFNAUTL
002000 DATE-WRITTEN. APRIL 1991.                                        PPFNAUTL
002100     SKIP1                                                        PPFNAUTL
002200 DATE-COMPILED.                                                   PPFNAUTL
002300     SKIP3                                                        PPFNAUTL
002400 ENVIRONMENT DIVISION.                                            PPFNAUTL
002500     SKIP2                                                        PPFNAUTL
002600 CONFIGURATION SECTION.                                           PPFNAUTL
002700     SKIP1                                                        PPFNAUTL
002800 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPFNAUTL
002900     SKIP1                                                        PPFNAUTL
003000 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPFNAUTL
003100      EJECT                                                       PPFNAUTL
003200*                D A T A  D I V I S I O N                         PPFNAUTL
003300*                                                                 PPFNAUTL
003400 DATA DIVISION.                                                   PPFNAUTL
003500     SKIP2                                                        PPFNAUTL
003600 WORKING-STORAGE SECTION.                                         PPFNAUTL
003700     SKIP1                                                        PPFNAUTL
003800 01  FILLER.                                                      PPFNAUTL
003900     05  FNA-SUB                 PIC S9(04)   COMP   VALUE ZERO.  PPFNAUTL
004000     05  HERE-BEFORE-SW          PIC  X(01)          VALUE SPACE. PPFNAUTL
004100         88  HERE-BEFORE                             VALUE 'Y'.   PPFNAUTL
004200     05  FNA-ROW-OPEN-SW         PIC  X(01)          VALUE SPACE. PPFNAUTL
004300         88  FNA-ROW-OPEN                            VALUE 'Y'.   PPFNAUTL
004400     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPFNAUTL
004500         88  FIRST-TIME                VALUE 'Y'.                 PPFNAUTL
004600         88  NOT-FIRST-TIME            VALUE 'N'.                 PPFNAUTL
004700     05  A-STND-PROG-ID          PIC  X(08)   VALUE 'PPFNAUTL'.   PPFNAUTL
004800     05  WS-EMPLOYEE-ID          PIC  X(09)          VALUE SPACE. PPFNAUTL
004900     SKIP1                                                        PPFNAUTL
005000 01  PPDB2MSG-INTERFACE.                                          PPFNAUTL
005100                                 COPY CPLNKDB2.                   PPFNAUTL
005200     SKIP1                                                        PPFNAUTL
005300 01  PAYROLL-CONSTANTS.                                           PPFNAUTL
005400                                 COPY CPWSXIC2.                   PPFNAUTL
005500 01  ONLINE-SIGNALS EXTERNAL.                                     PPFNAUTL
005600                                    COPY CPWSONLI.                PPFNAUTL
005700     EJECT                                                        PPFNAUTL
005800 01  XWHC-COMPILE-WORK-AREA.                                      PPFNAUTL
005900                                    COPY CPWSXWHC.                PPFNAUTL
006000     SKIP2                                                        PPFNAUTL
006100******************************************************************PPFNAUTL
006200*          SQL - WORKING STORAGE                                 *PPFNAUTL
006300******************************************************************PPFNAUTL
006400 01  FNA-ROW-DATA.                                                PPFNAUTL
006500     EXEC SQL                                                     PPFNAUTL
006600          INCLUDE PPPVFNA1                                        PPFNAUTL
006700     END-EXEC.                                                    PPFNAUTL
006800     SKIP1                                                        PPFNAUTL
006900     EXEC SQL                                                     PPFNAUTL
007000          INCLUDE SQLCA                                           PPFNAUTL
007100     END-EXEC.                                                    PPFNAUTL
007200     SKIP1                                                        PPFNAUTL
007300******************************************************************PPFNAUTL
007400*                                                                *PPFNAUTL
007500*                SQL- SELECTS                                    *PPFNAUTL
007600*                                                                *PPFNAUTL
007700******************************************************************PPFNAUTL
007800*                                                                *PPFNAUTL
007900*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPFNAUTL
008000*                                                                *PPFNAUTL
008100*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPFNAUTL
008200*                        MEMBER NAMES                            *PPFNAUTL
008300*                                                                *PPFNAUTL
008400*  PPPVFNA1_FNA          PPPVFNA1                                *PPFNAUTL
008500*                                                                *PPFNAUTL
008600******************************************************************PPFNAUTL
008700     SKIP1                                                        PPFNAUTL
008800     EXEC SQL                                                     PPFNAUTL
008900         DECLARE FNA_ROW CURSOR FOR                               PPFNAUTL
009000             SELECT * FROM PPPVFNA1_FNA                           PPFNAUTL
009100             WHERE FNA_EMPLOYEE_ID = :WS-EMPLOYEE-ID              PPFNAUTL
009200             ORDER BY FNA_FISCAL_YEAR                             PPFNAUTL
009300     END-EXEC.                                                    PPFNAUTL
009400     SKIP2                                                        PPFNAUTL
009500******************************************************************PPFNAUTL
009600*                L I N K A G E   S E C T I O N                   *PPFNAUTL
009700******************************************************************PPFNAUTL
009800*                                                                 PPFNAUTL
009900 LINKAGE SECTION.                                                 PPFNAUTL
010000*                                                                 PPFNAUTL
010100 01  PPFNAUTL-INTERFACE.            COPY CPLNKFNA.                PPFNAUTL
010200*                                                                 PPFNAUTL
010300 01  FINAID-ARRAY.                                                PPFNAUTL
010400                                 COPY CPWSEFAR.                   PPFNAUTL
010500      EJECT                                                       PPFNAUTL
010600*                                                                 PPFNAUTL
010700******************************************************************PPFNAUTL
010800*            P R O C E D U R E  D I V I S I O N                  *PPFNAUTL
010900******************************************************************PPFNAUTL
011000*                                                                 PPFNAUTL
011100 PROCEDURE DIVISION USING PPFNAUTL-INTERFACE                      PPFNAUTL
011200                          FINAID-ARRAY.                           PPFNAUTL
011300     SKIP1                                                        PPFNAUTL
011400******************************************************************PPFNAUTL
011500*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPFNAUTL
011600*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPFNAUTL
011700******************************************************************PPFNAUTL
011800     EXEC SQL                                                     PPFNAUTL
011900          INCLUDE CPPDXE99                                        PPFNAUTL
012000     END-EXEC.                                                    PPFNAUTL
012100     SKIP1                                                        PPFNAUTL
012200 MAINLINE-100 SECTION.                                            PPFNAUTL
012300     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPFNAUTL
012400     MOVE PPFNAUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                 PPFNAUTL
012500     IF FIRST-TIME                                                PPFNAUTL
012600         SET NOT-FIRST-TIME TO TRUE                               PPFNAUTL
012700     IF  NOT ENVIRON-IS-CICS                                      PPFNAUTL
012800         PERFORM 7900-FIRST-TIME                                  PPFNAUTL
012900     END-IF                                                       PPFNAUTL
013000     END-IF.                                                      PPFNAUTL
013100     INITIALIZE PPFNAUTL-ROW-COUNT.                               PPFNAUTL
013200     INITIALIZE FINAID-ARRAY.                                     PPFNAUTL
013300     SKIP1                                                        PPFNAUTL
013310     INITIALIZE FNA-SUB.                                          PPFNAUTL
013400     PERFORM SQL-OPEN-FNA.                                        PPFNAUTL
013500     PERFORM SQL-FETCH-FNA.                                       PPFNAUTL
013600     IF SQLCODE = ZERO                                            PPFNAUTL
013700         PERFORM VARYING FNA-SUB FROM 1 BY 1                      PPFNAUTL
013800                         UNTIL SQLCODE = +100                     PPFNAUTL
013900                         OR FNA-SUB > IDC-MAX-NO-FNA              PPFNAUTL
014000             MOVE FNA-FISCAL-YEAR TO EFAR-FISCAL-YEAR (FNA-SUB)   PPFNAUTL
014100             MOVE FNA-RA-FED-FYTD TO EFAR-RA-FED-FYTD (FNA-SUB)   PPFNAUTL
014200             MOVE FNA-TA-FED-FYTD TO EFAR-TA-FED-FYTD (FNA-SUB)   PPFNAUTL
014300             MOVE FNA-OTH-FED-FYTD TO EFAR-OTH-FED-FYTD (FNA-SUB) PPFNAUTL
014400             MOVE FNA-RA-NONFD-FYTD TO EFAR-RA-NONFD-FYTD         PPFNAUTL
014500                     (FNA-SUB)                                    PPFNAUTL
014600             MOVE FNA-TA-NONFD-FYTD TO EFAR-TA-NONFD-FYTD         PPFNAUTL
014700                     (FNA-SUB)                                    PPFNAUTL
014800             MOVE FNA-OTH-NONFD-FYTD TO EFAR-OTH-NONFD-FYTD       PPFNAUTL
014900                     (FNA-SUB)                                    PPFNAUTL
015000             MOVE FNA-ADC-CODE TO EFAR-ADC-CODE                   PPFNAUTL
015100                     (FNA-SUB)                                    PPFNAUTL
015200             PERFORM SQL-FETCH-FNA                                PPFNAUTL
015300         END-PERFORM                                              PPFNAUTL
015400     END-IF.                                                      PPFNAUTL
015500     SKIP1                                                        PPFNAUTL
015600****   IF MORE FNA ENTRIES ON TABLE THAN ALLOWED, SET ERROR       PPFNAUTL
015700     IF FNA-SUB > IDC-MAX-NO-FNA AND SQLCODE NOT = +100           PPFNAUTL
015800         SET PPFNAUTL-ERROR TO TRUE                               PPFNAUTL
015900     END-IF.                                                      PPFNAUTL
016000     SKIP1                                                        PPFNAUTL
016100     PERFORM SQL-CLOSE-FNA.                                       PPFNAUTL
016200     IF FNA-SUB > ZERO                                            PPFNAUTL
016300         COMPUTE PPFNAUTL-ROW-COUNT = FNA-SUB - 1.                PPFNAUTL
016400                                                                  PPFNAUTL
016500 MAINLINE-999.                                                    PPFNAUTL
016600     GOBACK.                                                      PPFNAUTL
016700     SKIP3                                                        PPFNAUTL
016800 7900-FIRST-TIME SECTION.                                         PPFNAUTL
016900     SKIP1                                                        PPFNAUTL
017000     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPFNAUTL
017100     SKIP1                                                        PPFNAUTL
017200                                    COPY CPPDXWHC.                PPFNAUTL
017300******************************************************************PPFNAUTL
017400*  PROCEDURE SQL (OPEN/SELECT/CLOSE) FOR FNA VIEW                *PPFNAUTL
017500******************************************************************PPFNAUTL
017600                                                                  PPFNAUTL
017700 SQL-OPEN-FNA      SECTION.                                       PPFNAUTL
017800     IF NOT FNA-ROW-OPEN                                          PPFNAUTL
017900         MOVE 'OPEN FNA CURSOR' TO DB2MSG-TAG                     PPFNAUTL
018000         EXEC SQL                                                 PPFNAUTL
018100             OPEN FNA_ROW                                         PPFNAUTL
018200         END-EXEC                                                 PPFNAUTL
018300         SET FNA-ROW-OPEN TO TRUE.                                PPFNAUTL
018400                                                                  PPFNAUTL
018500 SQL-FETCH-FNA      SECTION.                                      PPFNAUTL
018600     MOVE 'FETCH FNA ROW' TO DB2MSG-TAG.                          PPFNAUTL
018700     EXEC SQL                                                     PPFNAUTL
018800         FETCH FNA_ROW INTO :FNA-ROW-DATA                         PPFNAUTL
018900     END-EXEC.                                                    PPFNAUTL
019000                                                                  PPFNAUTL
019100 SQL-CLOSE-FNA      SECTION.                                      PPFNAUTL
019200     IF FNA-ROW-OPEN                                              PPFNAUTL
019300         MOVE 'CLOSE FNA CURSOR' TO DB2MSG-TAG                    PPFNAUTL
019400         EXEC SQL                                                 PPFNAUTL
019500             CLOSE FNA_ROW                                        PPFNAUTL
019600         END-EXEC                                                 PPFNAUTL
019700         MOVE SPACE TO FNA-ROW-OPEN-SW.                           PPFNAUTL
019800*999999-SQL-ERROR                                                 PPFNAUTL
019900     COPY CPPDXP99.                                               PPFNAUTL
020000     SKIP1                                                        PPFNAUTL
020100     SET PPFNAUTL-ERROR TO TRUE.                                  PPFNAUTL
020200     IF NOT HERE-BEFORE                                           PPFNAUTL
020300         SET HERE-BEFORE TO TRUE                                  PPFNAUTL
020400         PERFORM SQL-CLOSE-FNA.                                   PPFNAUTL
020500     GOBACK.                                                      PPFNAUTL
