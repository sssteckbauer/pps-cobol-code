000100*==========================================================%      UCSD0102
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0102
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD0102     =%      UCSD0102
000400*==========================================================%      UCSD0102
000500*==========================================================%      UCSD0102
000600*=    PROGRAM: PPCTT27                                    =%      UCSD0102
000700*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1465   =%      UCSD0102
000800*=    NAME: G. CHIU         MODIFICATION DATE: 04/10/04   =%      UCSD0102
000900*=                                                        =%      UCSD0102
001000*=    DESCRIPTION                                         =%      UCSD0102
001100*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA -         =%      UCSD0102
001200*=    FUND CONTAINS SIX CHARACTERS.                       =%      UCSD0102
001300*=                                                        =%      UCSD0102
001400*==========================================================%      UCSD0102
000100**************************************************************/   30871465
000200*  PROGRAM: PPCTT27                                          */   30871465
000300*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000400*  NAME:___SRS___________ CREATION DATE:      ___02/11/03__  */   30871465
000500*  DESCRIPTION:                                              */   30871465
000600*  PPPESP TABLE TRANSACTION ASSEMBLY MODULE                  */   30871465
000700**************************************************************/   30871465
000800 IDENTIFICATION DIVISION.                                         PPCTT27
000900 PROGRAM-ID. PPCTT27.                                             PPCTT27
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT27
001100 DATE-COMPILED.                                                   PPCTT27
001200 DATA DIVISION.                                                   PPCTT27
001300 FILE SECTION.                                                    PPCTT27
001400 WORKING-STORAGE SECTION.                                         PPCTT27
001500                                                                  PPCTT27
001600 01  WA-WORK-VARIABLES.                                           PPCTT27
001700     05  WA-READ-ERROR.                                           PPCTT27
001800         10 FILLER                     PIC X(37) VALUE            PPCTT27
001900                 'PPCTLTRD ERROR ON TRANS CALL: RETURN='.         PPCTT27
002000         10 WA-RTN-CODE                PIC XX.                    PPCTT27
002100         10 FILLER                     PIC X(07) VALUE            PPCTT27
002200                 ' COUNT='.                                       PPCTT27
002300         10 WA-WORK-Z5                 PIC ZZ,ZZ9.                PPCTT27
002400 01  WB-PROGRAM-CONSTANTS.                                        PPCTT27
002500     05  WB-TABLE-NO                   PIC X(2) VALUE '27'.       PPCTT27
002600 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT27
002700     05  WC-ERR-IX                     PIC S9(04) VALUE ZERO.     PPCTT27
002800     05  WC-SET-CNT                    PIC S9(04) VALUE ZERO.     PPCTT27
002900     05  WC-TALLY                      PIC S9(04) VALUE ZERO.     PPCTT27
003000 01  WF-PROGRAM-FLAGS.                                            PPCTT27
003100     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT27
003200         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT27
003300         88  WF-ACCEPT-SET                      VALUES 00 THRU 04.PPCTT27
003400         88  WF-REJECT-SET                      VALUES 05 THRU 99.PPCTT27
003500 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT27
003600     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT27
003700     05  MCT020                     PIC X(05) VALUE 'CT020'.      PPCTT27
003800     05  MCT021                     PIC X(05) VALUE 'CT021'.      PPCTT27
003900     05  MCT022                     PIC X(05) VALUE 'CT022'.      PPCTT27
004000     05  MCT023                     PIC X(05) VALUE 'CT023'.      PPCTT27
004100 01  WS-SET-WORK-AREA.                                            PPCTT27
004200     05  WS-VALID-NEXT-SEQ-NOS      PIC X(04) VALUE LOW-VALUES.   PPCTT27
004300     05  WS-SET-END-FLAG            PIC X(01) VALUE 'N'.          PPCTT27
004400         88  WS-SET-END-NOT-OK                VALUE 'N'.          PPCTT27
004500         88  WS-SET-END-OK                    VALUE 'Y'.          PPCTT27
004600     05  WS-SET-STATUS-FLAG         PIC X(01) VALUE 'N'.          PPCTT27
004700         88  WS-SET-OK                        VALUE 'Y'.          PPCTT27
004800         88  WS-SET-NOT-OK                    VALUE 'N'.          PPCTT27
004900     05  WS-SET-KEY.                                              PPCTT27
005000         10  WS-SET-ACTION          PIC X(01).                    PPCTT27
005100             88  WS-ADD-SET                   VALUE 'A'.          PPCTT27
005200             88  WS-CHANGE-SET                VALUE 'C'.          PPCTT27
005300             88  WS-DELETE-SET                VALUE 'D'.          PPCTT27
005400         10  WS-SET-LOC-CODE        PIC X(01).                    PPCTT27
005500         10  WS-SET-RECORD-TYPE     PIC X(01).                    PPCTT27
005600         10  WS-SET-ACCT-OR-FUND-1  PIC X(06).                    PPCTT27
005700         10  WS-SET-ACCT-OR-FUND-2  PIC X(06).                    PPCTT27
005800     05  WS-HOLD-KEY                PIC X(15) VALUE LOW-VALUES.   PPCTT27
005900 01  EMPL-SUPPORT-ESP-TABLE-TRANS.                                PPCTT27
006000     05  ESPT-ACTION-CODE           PIC X(01).                    PPCTT27
006100         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT27
006200         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT27
006300         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT27
006400         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT27
006500     05  FILLER                     PIC X(02).                    PPCTT27
006600     05  ESPT-LOC-CODE              PIC X(01).                    PPCTT27
006700     05  ESPT-RECORD-TYPE           PIC X(01).                    PPCTT27
006800         88  ESPT-RECORD-TYPE-VALID     VALUES '1' '2' '3' '4'.   PPCTT27
006900     05  FILLER                     PIC X(04).                    PPCTT27
007000     05  ESPT-DATA-2.                                             PPCTT27
007100         10 ESPT-DATA-2-ACCOUNT-1   PIC X(06).                    PPCTT27
007200         10 ESPT-DATA-2-ACCOUNT-2   PIC X(06).                    PPCTT27
007300         10 FILLER                  PIC X(08).                    PPCTT27
007400     05  ESPT-DATA-3 REDEFINES ESPT-DATA-2.                       PPCTT27
008900*****    10 ESPT-DATA-3-FUND-1      PIC X(05).                    UCSD0102
009000*****    10 FILLER                  PIC X(01).                    UCSD0102
009100         10 ESPT-DATA-3-FUND-1      PIC X(06).                    UCSD0102
009200*****    10 ESPT-DATA-3-FUND-2      PIC X(05).                    UCSD0102
009300*****    10 FILLER                  PIC X(09).                    UCSD0102
009400         10 ESPT-DATA-3-FUND-2      PIC X(06).                    UCSD0102
009500         10 FILLER                  PIC X(08).                    UCSD0102
007900     05  ESPT-DATA-4 REDEFINES ESPT-DATA-2.                       PPCTT27
009700         10 ESPT-DATA-4-ACCOUNT-1   PIC X(05).                    PPCTT27
009800*****    10 ESPT-DATA-4-FUND-1      PIC X(05).                    UCSD0102
009900*****    10 FILLER                  PIC X(09).                    UCSD0102
010000         10 ESPT-DATA-4-FUND-1      PIC X(06).                    UCSD0102
010100         10 FILLER                  PIC X(08).                    UCSD0102
008300     05  ESPT-RATE.                                               PPCTT27
008400         10 ESPT-RATE-9             PIC 9(05).                    PPCTT27
008500     05  FILLER                     PIC X(46).                    PPCTT27
008600                                                                  PPCTT27
008700 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT27
008800 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT27
008900 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT27
009000 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT27
009100 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT27
009200 01  EMPL-SUPPORT-ESP-TABLE-INPUT.     COPY CPCTESPI.             PPCTT27
009300 01  EMPL-SUPPORT-ESP-TABLE-DATA.                                 PPCTT27
009400     EXEC SQL                                                     PPCTT27
009500         INCLUDE PPPVZESP                                         PPCTT27
009600     END-EXEC.                                                    PPCTT27
009700                                                                  PPCTT27
009800 LINKAGE SECTION.                                                 PPCTT27
009900                                                                  PPCTT27
010000 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT27
010100                                                                  PPCTT27
010200 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE.           PPCTT27
010300                                                                  PPCTT27
010400 0000-MAIN SECTION.                                               PPCTT27
010500                                                                  PPCTT27
010600     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT27
010700     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT27
010800       UNTIL CSRI-TABLE-TRANS-END.                                PPCTT27
010900     PERFORM 3000-FINISH-PREVIOUS-SET.                            PPCTT27
011000                                                                  PPCTT27
011100 0099-END.                                                        PPCTT27
011200                                                                  PPCTT27
011300     GOBACK.                                                      PPCTT27
011400                                                                  PPCTT27
011500 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT27
011600                                                                  PPCTT27
011700     MOVE 'PPCTT27'             TO XWHC-DISPLAY-MODULE.           PPCTT27
011800     COPY CPPDXWHC.                                               PPCTT27
011900     SET CTTI-NORMAL            TO TRUE.                          PPCTT27
012000     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTT27
012100     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTT27
012200                                   CTRI-TRANSACTION               PPCTT27
012300                                   CTRI-TRANSACTION-DISPO         PPCTT27
012400                                   CTRI-TEXT-LINE                 PPCTT27
012500                                   CTRI-MESSAGE-NUMBER.           PPCTT27
012600     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTT27
012700                                   CTTI-ERROR-SEVERITY.           PPCTT27
012800     MOVE WB-TABLE-NO           TO CSRI-CALL-TYPE-NUM.            PPCTT27
012900     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT27
013000                                                                  PPCTT27
013100 1099-INITIALIZATION-EXIT.                                        PPCTT27
013200     EXIT.                                                        PPCTT27
013300                                                                  PPCTT27
013400 2000-PROCESS-TRANSACTIONS SECTION.                               PPCTT27
013500                                                                  PPCTT27
013600     IF WS-SET-KEY NOT = WS-HOLD-KEY                              PPCTT27
013700        PERFORM 3000-FINISH-PREVIOUS-SET                          PPCTT27
013800        PERFORM 2100-BEGIN-NEW-SET                                PPCTT27
013900     END-IF.                                                      PPCTT27
014000     MOVE EMPL-SUPPORT-ESP-TABLE-TRANS TO CTRI-TRANSACTION.       PPCTT27
014100     MOVE ESPT-ACTION-CODE TO KCTE-ACTION.                        PPCTT27
014200     ADD 1 TO WC-SET-CNT.                                         PPCTT27
014300     IF ESPT-ACTION-CODE = 'D'                                    PPCTT27
014400        SET WS-SET-END-OK TO TRUE                                 PPCTT27
014500        PERFORM 9100-CALL-PPCTLRPT                                PPCTT27
014600        PERFORM 9300-FETCH-NEXT-TRANSACTION                       PPCTT27
014700        GO TO 2099-EXIT                                           PPCTT27
014800     END-IF.                                                      PPCTT27
014900     IF ESPT-RECORD-TYPE-VALID                                    PPCTT27
015000        MOVE ZERO TO WC-TALLY                                     PPCTT27
015100        INSPECT WS-VALID-NEXT-SEQ-NOS TALLYING WC-TALLY           PPCTT27
015200            FOR ALL ESPT-RECORD-TYPE                              PPCTT27
015300        IF WC-TALLY = ZERO                                        PPCTT27
015400            MOVE MCT021 TO CTRI-MESSAGE-NUMBER                    PPCTT27
015500            PERFORM 9100-CALL-PPCTLRPT                            PPCTT27
015600            SET WS-SET-NOT-OK TO TRUE                             PPCTT27
015700        ELSE                                                      PPCTT27
015800            PERFORM 2200-MOVE-TRANSACTION-DATA                    PPCTT27
015900        END-IF                                                    PPCTT27
016000        EVALUATE TRUE                                             PPCTT27
016100          WHEN WS-ADD-SET                                         PPCTT27
016200            EVALUATE ESPT-RECORD-TYPE                             PPCTT27
016300              WHEN '1'                                            PPCTT27
016400                  MOVE '    ' TO WS-VALID-NEXT-SEQ-NOS            PPCTT27
016500                  SET WS-SET-END-OK TO TRUE                       PPCTT27
016600              WHEN '2'                                            PPCTT27
016700                  MOVE '    ' TO WS-VALID-NEXT-SEQ-NOS            PPCTT27
016800                  SET WS-SET-END-OK TO TRUE                       PPCTT27
016900              WHEN '3'                                            PPCTT27
017000                  MOVE '    ' TO WS-VALID-NEXT-SEQ-NOS            PPCTT27
017100                  SET WS-SET-END-OK TO TRUE                       PPCTT27
017200              WHEN '4'                                            PPCTT27
017300                  MOVE '    ' TO WS-VALID-NEXT-SEQ-NOS            PPCTT27
017400                  SET WS-SET-END-OK TO TRUE                       PPCTT27
017500            END-EVALUATE                                          PPCTT27
017600          WHEN WS-CHANGE-SET                                      PPCTT27
017700            SET WS-SET-END-OK TO TRUE                             PPCTT27
017800        END-EVALUATE                                              PPCTT27
017900        IF CTRI-TRANSACTION NOT = SPACES                          PPCTT27
018000            PERFORM 9100-CALL-PPCTLRPT                            PPCTT27
018100        END-IF                                                    PPCTT27
018200     ELSE                                                         PPCTT27
018300        MOVE MCT020 TO CTRI-MESSAGE-NUMBER                        PPCTT27
018400        PERFORM 9100-CALL-PPCTLRPT                                PPCTT27
018500        SET WS-SET-NOT-OK TO TRUE                                 PPCTT27
018600     END-IF.                                                      PPCTT27
018700                                                                  PPCTT27
018800     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT27
018900                                                                  PPCTT27
019000 2099-EXIT.                                                       PPCTT27
019100     EXIT.                                                        PPCTT27
019200                                                                  PPCTT27
019300 2100-BEGIN-NEW-SET SECTION.                                      PPCTT27
019400                                                                  PPCTT27
019500     MOVE WS-SET-KEY TO WS-HOLD-KEY.                              PPCTT27
019600     SET WS-SET-OK TO TRUE.                                       PPCTT27
019700     SET WS-SET-END-NOT-OK TO TRUE.                               PPCTT27
019800     MOVE ZERO TO WC-SET-CNT.                                     PPCTT27
019900     MOVE SPACES TO EMPL-SUPPORT-ESP-TABLE-INPUT.                 PPCTT27
020000     MOVE ESPT-LOC-CODE    TO ESPI-LOC-CODE.                      PPCTT27
020100     MOVE ESPT-RECORD-TYPE TO ESPI-RECORD-TYPE.                   PPCTT27
020200     EVALUATE TRUE                                                PPCTT27
020300       WHEN WS-ADD-SET                                            PPCTT27
020400           MOVE '1234' TO WS-VALID-NEXT-SEQ-NOS                   PPCTT27
020500       WHEN WS-CHANGE-SET                                         PPCTT27
020600           MOVE '1234' TO WS-VALID-NEXT-SEQ-NOS                   PPCTT27
020700     END-EVALUATE.                                                PPCTT27
020800                                                                  PPCTT27
020900 2199-EXIT.                                                       PPCTT27
021000     EXIT.                                                        PPCTT27
021100                                                                  PPCTT27
021200 2200-MOVE-TRANSACTION-DATA SECTION.                              PPCTT27
021300                                                                  PPCTT27
021400     EVALUATE ESPT-RECORD-TYPE                                    PPCTT27
021500      WHEN '1'                                                    PPCTT27
021600        IF ESPT-RATE             NOT = SPACES                     PPCTT27
021700           MOVE ESPT-RATE              TO ESPI-RATE               PPCTT27
021800        END-IF                                                    PPCTT27
021900      WHEN '2'                                                    PPCTT27
022000        IF ESPT-DATA-2-ACCOUNT-1 NOT = SPACES                     PPCTT27
022100           MOVE ESPT-DATA-2-ACCOUNT-1  TO ESPI-ACCT-OR-FUND-1     PPCTT27
022200        END-IF                                                    PPCTT27
022300        IF ESPT-DATA-2-ACCOUNT-2 NOT = SPACES                     PPCTT27
022400           MOVE ESPT-DATA-2-ACCOUNT-2  TO ESPI-ACCT-OR-FUND-2     PPCTT27
022500        END-IF                                                    PPCTT27
022600        IF ESPT-RATE             NOT = SPACES                     PPCTT27
022700           MOVE ESPT-RATE              TO ESPI-RATE               PPCTT27
022800        END-IF                                                    PPCTT27
022900      WHEN '3'                                                    PPCTT27
023000        IF ESPT-DATA-3-FUND-1    NOT = SPACES                     PPCTT27
023100           MOVE ESPT-DATA-3-FUND-1     TO ESPI-ACCT-OR-FUND-1     PPCTT27
023200        END-IF                                                    PPCTT27
023300        IF ESPT-DATA-3-FUND-2    NOT = SPACES                     PPCTT27
023400           MOVE ESPT-DATA-3-FUND-2     TO ESPI-ACCT-OR-FUND-2     PPCTT27
023500        END-IF                                                    PPCTT27
023600        IF ESPT-RATE            NOT = SPACES                      PPCTT27
023700           MOVE ESPT-RATE              TO ESPI-RATE               PPCTT27
023800        END-IF                                                    PPCTT27
023900      WHEN '4'                                                    PPCTT27
024000        IF ESPT-DATA-4-ACCOUNT-1 NOT = SPACES                     PPCTT27
024100           MOVE ESPT-DATA-4-ACCOUNT-1  TO ESPI-ACCT-OR-FUND-1     PPCTT27
024200        END-IF                                                    PPCTT27
024300        IF ESPT-DATA-4-FUND-1    NOT = SPACES                     PPCTT27
024400           MOVE ESPT-DATA-4-FUND-1     TO ESPI-ACCT-OR-FUND-2     PPCTT27
024500        END-IF                                                    PPCTT27
024600        IF ESPT-RATE            NOT = SPACES                      PPCTT27
024700           MOVE ESPT-RATE              TO ESPI-RATE               PPCTT27
024800        END-IF                                                    PPCTT27
024900     END-EVALUATE.                                                PPCTT27
025000                                                                  PPCTT27
025100 2299-EXIT.                                                       PPCTT27
025200     EXIT.                                                        PPCTT27
025300                                                                  PPCTT27
025400 3000-FINISH-PREVIOUS-SET SECTION.                                PPCTT27
025500                                                                  PPCTT27
025600     IF WS-HOLD-KEY NOT = LOW-VALUES                              PPCTT27
025700       IF WS-SET-END-NOT-OK                                       PPCTT27
025800          MOVE MCT023 TO CTRI-MESSAGE-NUMBER                      PPCTT27
025900          PERFORM 9100-CALL-PPCTLRPT                              PPCTT27
026000          SET WS-SET-NOT-OK TO TRUE                               PPCTT27
026100       END-IF                                                     PPCTT27
026200       IF WS-SET-OK                                               PPCTT27
026300          SET WF-NO-ERRORS TO TRUE                                PPCTT27
026400          MOVE CTTI-ACTION-DATE TO KCTE-LAST-ACTION-DT            PPCTT27
026500          CALL 'PPCTESPE' USING CONTROL-TABLE-EDIT-INTERFACE      PPCTT27
026600                                EMPL-SUPPORT-ESP-TABLE-INPUT      PPCTT27
026700                                EMPL-SUPPORT-ESP-TABLE-DATA       PPCTT27
026800          IF KCTE-ERR > ZERO                                      PPCTT27
026900             PERFORM 3100-PRINT-ERROR                             PPCTT27
027000               VARYING WC-ERR-IX FROM 1 BY 1                      PPCTT27
027100                 UNTIL WC-ERR-IX > KCTE-ERR                       PPCTT27
027200             IF WF-REJECT-SET OR KCTE-STATUS-NOT-COMPLETED        PPCTT27
027300                MOVE MCT022 TO CTRI-MESSAGE-NUMBER                PPCTT27
027400                SET WS-SET-NOT-OK TO TRUE                         PPCTT27
027500             END-IF                                               PPCTT27
027600          END-IF                                                  PPCTT27
027700       END-IF                                                     PPCTT27
027800       IF WS-SET-OK                                               PPCTT27
027900          MOVE 'SET ACCEPTED' TO CTRI-TEXT-DISPO                  PPCTT27
028000          ADD WC-SET-CNT TO CTTI-TRANS-ACCEPTED                   PPCTT27
028100          PERFORM 3200-UPDATE-DB2-TABLE                           PPCTT27
028200       ELSE                                                       PPCTT27
028300          MOVE 'SET REJECTED' TO CTRI-TEXT-DISPO                  PPCTT27
028400          ADD WC-SET-CNT TO CTTI-TRANS-REJECTED                   PPCTT27
028500       END-IF                                                     PPCTT27
028600       PERFORM 9100-CALL-PPCTLRPT                                 PPCTT27
028700       MOVE 2 TO CTRI-SPACING-OVERRIDE                            PPCTT27
028800     END-IF.                                                      PPCTT27
028900                                                                  PPCTT27
029000 3099-EXIT.                                                       PPCTT27
029100     EXIT.                                                        PPCTT27
029200                                                                  PPCTT27
029300 3100-PRINT-ERROR SECTION.                                        PPCTT27
029400                                                                  PPCTT27
029500     MOVE KCTE-ERR-NO (WC-ERR-IX) TO CTRI-MESSAGE-NUMBER.         PPCTT27
029600     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT27
029700     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT27
029800         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT27
029900     END-IF.                                                      PPCTT27
030000                                                                  PPCTT27
030100 3199-EXIT.                                                       PPCTT27
030200     EXIT.                                                        PPCTT27
030300                                                                  PPCTT27
030400 3200-UPDATE-DB2-TABLE SECTION.                                   PPCTT27
030500                                                                  PPCTT27
030600     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT27
030700     CALL 'PPCTESPU' USING CONTROL-TABLE-UPDT-INTERFACE,          PPCTT27
030800                           EMPL-SUPPORT-ESP-TABLE-DATA.           PPCTT27
030900     IF KCTU-STATUS-OK                                            PPCTT27
031000        EVALUATE TRUE                                             PPCTT27
031100          WHEN KCTU-ACTION-ADD                                    PPCTT27
031200            ADD 1 TO CTTI-ENTRIES-ADDED                           PPCTT27
031300          WHEN KCTU-ACTION-CHANGE                                 PPCTT27
031400            ADD 1 TO CTTI-ENTRIES-UPDATED                         PPCTT27
031500          WHEN KCTU-ACTION-DELETE                                 PPCTT27
031600            ADD 1 TO CTTI-ENTRIES-DELETED                         PPCTT27
031700          WHEN OTHER                                              PPCTT27
031800            GO TO 3300-BAD-UPDATE-CALL                            PPCTT27
031900        END-EVALUATE                                              PPCTT27
032000     ELSE                                                         PPCTT27
032100        GO TO 3300-BAD-UPDATE-CALL                                PPCTT27
032200     END-IF.                                                      PPCTT27
032300                                                                  PPCTT27
032400 3299-EXIT.                                                       PPCTT27
032500     EXIT.                                                        PPCTT27
032600                                                                  PPCTT27
032700 3300-BAD-UPDATE-CALL SECTION.                                    PPCTT27
032800                                                                  PPCTT27
032900     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT27
033000     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT27
033100     STRING 'PPCTESPU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT27
033200            '" STATUS WAS "'               KCTU-STATUS            PPCTT27
033300            '" COUNT='                     WA-WORK-Z5             PPCTT27
033400            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT27
033500     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT27
033600     GO TO 9999-ABEND.                                            PPCTT27
033700                                                                  PPCTT27
033800 3399-EXIT.                                                       PPCTT27
033900     EXIT.                                                        PPCTT27
034000                                                                  PPCTT27
034100 9100-CALL-PPCTLRPT SECTION.                                      PPCTT27
034200                                                                  PPCTT27
034300     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT27
034400     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT27
034500         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT27
034600     END-IF.                                                      PPCTT27
034700     IF CTRI-RUN-ABORTED                                          PPCTT27
034800        GO TO 9999-ABEND                                          PPCTT27
034900     END-IF.                                                      PPCTT27
035000                                                                  PPCTT27
035100 9199-EXIT.                                                       PPCTT27
035200     EXIT.                                                        PPCTT27
035300                                                                  PPCTT27
035400 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT27
035500                                                                  PPCTT27
035600     PERFORM WITH TEST AFTER                                      PPCTT27
035700       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT27
035800          OR CSRI-TRANSACTION(1:1) NOT = '*'                      PPCTT27
035900      CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE        PPCTT27
036000      IF CSRI-NORMAL                                              PPCTT27
036100         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT27
036200         IF CSRI-TRANSACTION(1:1) = '*'                           PPCTT27
036300            MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION             PPCTT27
036400            MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO     PPCTT27
036500            PERFORM 9100-CALL-PPCTLRPT                            PPCTT27
036600            ADD 1 TO CTTI-TRANS-ACCEPTED                          PPCTT27
036700         ELSE                                                     PPCTT27
036800            MOVE CSRI-TRANSACTION TO EMPL-SUPPORT-ESP-TABLE-TRANS PPCTT27
036900            MOVE ESPT-ACTION-CODE TO WS-SET-ACTION                PPCTT27
037000            MOVE ESPT-LOC-CODE    TO WS-SET-LOC-CODE              PPCTT27
037100            MOVE ESPT-RECORD-TYPE TO WS-SET-RECORD-TYPE           PPCTT27
037200            MOVE ESPT-DATA-2-ACCOUNT-1 TO WS-SET-ACCT-OR-FUND-1   PPCTT27
037300            MOVE ESPT-DATA-2-ACCOUNT-2 TO WS-SET-ACCT-OR-FUND-2   PPCTT27
037400         END-IF                                                   PPCTT27
037500      ELSE                                                        PPCTT27
037600        IF NOT CSRI-TABLE-TRANS-END                               PPCTT27
037700           MOVE MCT001              TO CTRI-MESSAGE-NUMBER        PPCTT27
037800           MOVE CSRI-RETURN-CODE    TO WA-RTN-CODE                PPCTT27
037900           MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                PPCTT27
038000           MOVE WA-READ-ERROR       TO CTRI-TEXT-LINE             PPCTT27
038100           PERFORM 9100-CALL-PPCTLRPT                             PPCTT27
038200           GO TO 9999-ABEND                                       PPCTT27
038300        END-IF                                                    PPCTT27
038400      END-IF                                                      PPCTT27
038500     END-PERFORM.                                                 PPCTT27
038600                                                                  PPCTT27
038700 9399-EXIT.                                                       PPCTT27
038800     EXIT.                                                        PPCTT27
038900                                                                  PPCTT27
039000 9999-ABEND SECTION.                                              PPCTT27
039100                                                                  PPCTT27
039200     SET CTTI-ABORT TO TRUE.                                      PPCTT27
039300     GOBACK.                                                      PPCTT27
039400*************************END OF SOURCE CODE***********************PPCTT27
