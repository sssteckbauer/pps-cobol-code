000011**************************************************************/   52521419
000012*  PROGRAM: PPO1419B                                         */   52521419
000013*  RELEASE: ___1419______ SERVICE REQUEST(S): ____15252____  */   52521419
000015*  NAME:______RIDER______ CREATION DATE:      ___MM/DD/YY__  */   52521419
000016*  DESCRIPTION:                                              */   52521419
000017*  - CONVERT PPPDST UNLOADED ROW FROM OLD FORMAT TO NEW.     */   52521419
000018*    INCREASED THE SIZE OF THE DIST STEP FIELD.              */   52521419
000020**************************************************************/   52521419
001000 IDENTIFICATION DIVISION.                                         PPO1419B
001100 PROGRAM-ID. PPO1419B.                                            PPO1419B
001200 AUTHOR. UCOP                                                     PPO1419B
001300                                                                  PPO1419B
001400*REMARKS.                                                         PPO1419B
001500                                                                  PPO1419B
001700***********************************************************       PPO1419B
001800*        E N V I R O N M E N T     D I V I S I O N        *       PPO1419B
001900***********************************************************       PPO1419B
002000                                                                  PPO1419B
002100 ENVIRONMENT DIVISION.                                            PPO1419B
002200 CONFIGURATION SECTION.                                           PPO1419B
002300                                                                  PPO1419B
002400 SPECIAL-NAMES.                                                   PPO1419B
002500     C01 IS NEW-PAGE.                                             PPO1419B
002600                                                                  PPO1419B
002700 INPUT-OUTPUT SECTION.                                            PPO1419B
002800 FILE-CONTROL.                                                    PPO1419B
002900                                                                  PPO1419B
003556     SELECT DSTIN          ASSIGN TO UT-S-DSTIN.                  PPO1419B
003557     SELECT DSTOUT         ASSIGN TO UT-S-DSTOUT.                 PPO1419B
004100                                                                  PPO1419B
004200***********************************************************       PPO1419B
004300*                D A T A     D I V I S I O N              *       PPO1419B
004400***********************************************************       PPO1419B
004500                                                                  PPO1419B
004600 DATA DIVISION.                                                   PPO1419B
004700 FILE SECTION.                                                    PPO1419B
006910                                                                  PPO1419B
009915***********************************************************       PPO1419B
009916*    OLD FORMAT PPPDST ROW DATA                           *       PPO1419B
009918***********************************************************       PPO1419B
009919 FD  DSTIN                                                        PPO1419B
009920     LABEL RECORDS ARE OMITTED                                    PPO1419B
009921     BLOCK CONTAINS 0 RECORDS                                     PPO1419B
009922     RECORDING MODE IS F                                          PPO1419B
009923     RECORD CONTAINS 129 CHARACTERS.                              PPO1419B
009924                                                                  PPO1419B
009928 01  DSTIN-REC.                                                   PPO1419B
010441     10 EMPLID               PIC X(9).                            PPO1419B
010442     10 ITERATION-NUMBER     PIC X(3).                            PPO1419B
010443     10 SYSTEM-ENTRY-DATE    PIC X(10).                           PPO1419B
010444     10 SYSTEM-ENTRY-TIME    PIC X(8).                            PPO1419B
010445     10 APPT-NUM             PIC S9(4) USAGE COMP.                PPO1419B
010446     10 DIST-NUM             PIC S9(4) USAGE COMP.                PPO1419B
010447     10 DELETE-FLAG          PIC X(1).                            PPO1419B
010448     10 ERH-INCORRECT        PIC X(1).                            PPO1419B
010451     10 FULL-ACCT-UNIT       PIC X(30).                           PPO1419B
010452     10 FULL-ACCT-UNIT-C     PIC X(1).                            PPO1419B
010453     10 DIST-DEPT-CODE       PIC X(6).                            PPO1419B
010454     10 DIST-DEPT-CODE-C     PIC X(1).                            PPO1419B
010455     10 DIST-DOS             PIC X(3).                            PPO1419B
010456     10 DIST-DOS-C           PIC X(1).                            PPO1419B
010457     10 DIST-FTE             PIC S9V99 USAGE COMP-3.              PPO1419B
010458     10 DIST-FTE-C           PIC X(1).                            PPO1419B
010459     10 DIST-OFF-ABOVE       PIC X(1).                            PPO1419B
010460     10 DIST-OFF-ABOVE-C     PIC X(1).                            PPO1419B
010461     10 DIST-PAY-RATE        PIC S99999V9999 USAGE COMP-3.        PPO1419B
010462     10 DIST-PAY-RATE-C      PIC X(1).                            PPO1419B
010463     10 DIST-PERQ            PIC X(3).                            PPO1419B
010464     10 DIST-PERQ-C          PIC X(1).                            PPO1419B
010465     10 DIST-STEP            PIC X(3).                            PPO1419B
010466     10 DIST-STEP-C          PIC X(1).                            PPO1419B
010467     10 DIST-PCT-FULL        PIC S9V9999 USAGE COMP-3.            PPO1419B
010468     10 DIST-PCT-FULL-C      PIC X(1).                            PPO1419B
010469     10 DIST-UNIT-CODE       PIC X(1).                            PPO1419B
010470     10 DIST-UNIT-CODE-C     PIC X(1).                            PPO1419B
010475     10 PAY-BEGIN-DATE       PIC X(10).                           PPO1419B
010476     10 PAY-BEGIN-DATE-C     PIC X(1).                            PPO1419B
010477     10 PAY-END-DATE         PIC X(10).                           PPO1419B
010478     10 PAY-END-DATE-C       PIC X(1).                            PPO1419B
010479     10 RANGE-ADJ-DUC        PIC X(1).                            PPO1419B
010480     10 RANGE-ADJ-DUC-C      PIC X(1).                            PPO1419B
010483     10 WORK-STUDY-PGM       PIC X(1).                            PPO1419B
010484     10 WORK-STUDY-PGM-C     PIC X(1).                            PPO1419B
010510                                                                  PPO1419B
010511***********************************************************       PPO1419B
010512*    NEW FORMAT PPPDST ROW DATA                           *       PPO1419B
010513***********************************************************       PPO1419B
010514 FD  DSTOUT                                                       PPO1419B
010515     LABEL RECORDS ARE OMITTED                                    PPO1419B
010516     BLOCK CONTAINS 0 RECORDS                                     PPO1419B
010517     RECORDING MODE IS F                                          PPO1419B
010518     RECORD CONTAINS 130 CHARACTERS.                              PPO1419B
010520                                                                  PPO1419B
010600 01  DSTOUT-REC.                                                  PPO1419B
010716     10 EMPLID               PIC X(9).                            PPO1419B
010717     10 ITERATION-NUMBER     PIC X(3).                            PPO1419B
010718     10 SYSTEM-ENTRY-DATE    PIC X(10).                           PPO1419B
010719     10 SYSTEM-ENTRY-TIME    PIC X(8).                            PPO1419B
010720     10 APPT-NUM             PIC S9(4) USAGE COMP.                PPO1419B
010721     10 DIST-NUM             PIC S9(4) USAGE COMP.                PPO1419B
010722     10 DELETE-FLAG          PIC X(1).                            PPO1419B
010723     10 ERH-INCORRECT        PIC X(1).                            PPO1419B
010725     10 FULL-ACCT-UNIT       PIC X(30).                           PPO1419B
010726     10 FULL-ACCT-UNIT-C     PIC X(1).                            PPO1419B
010727     10 DIST-DEPT-CODE       PIC X(6).                            PPO1419B
010728     10 DIST-DEPT-CODE-C     PIC X(1).                            PPO1419B
010729     10 DIST-DOS             PIC X(3).                            PPO1419B
010730     10 DIST-DOS-C           PIC X(1).                            PPO1419B
010731     10 DIST-FTE             PIC S9V99 USAGE COMP-3.              PPO1419B
010732     10 DIST-FTE-C           PIC X(1).                            PPO1419B
010733     10 DIST-OFF-ABOVE       PIC X(1).                            PPO1419B
010734     10 DIST-OFF-ABOVE-C     PIC X(1).                            PPO1419B
010735     10 DIST-PAY-RATE        PIC S99999V9999 USAGE COMP-3.        PPO1419B
010736     10 DIST-PAY-RATE-C      PIC X(1).                            PPO1419B
010737     10 DIST-PERQ            PIC X(3).                            PPO1419B
010738     10 DIST-PERQ-C          PIC X(1).                            PPO1419B
010739*****10 DIST-STEP            PIC X(3).                            52521419
010740     10 DIST-STEP            PIC X(4).                            52521419
010741     10 DIST-STEP-C          PIC X(1).                            PPO1419B
010742     10 DIST-PCT-FULL        PIC S9V9999 USAGE COMP-3.            PPO1419B
010743     10 DIST-PCT-FULL-C      PIC X(1).                            PPO1419B
010744     10 DIST-UNIT-CODE       PIC X(1).                            PPO1419B
010745     10 DIST-UNIT-CODE-C     PIC X(1).                            PPO1419B
010747     10 PAY-BEGIN-DATE       PIC X(10).                           PPO1419B
010748     10 PAY-BEGIN-DATE-C     PIC X(1).                            PPO1419B
010749     10 PAY-END-DATE         PIC X(10).                           PPO1419B
010750     10 PAY-END-DATE-C       PIC X(1).                            PPO1419B
010751     10 RANGE-ADJ-DUC        PIC X(1).                            PPO1419B
010752     10 RANGE-ADJ-DUC-C      PIC X(1).                            PPO1419B
010754     10 WORK-STUDY-PGM       PIC X(1).                            PPO1419B
010755     10 WORK-STUDY-PGM-C     PIC X(1).                            PPO1419B
012730     EJECT                                                        PPO1419B
012800 WORKING-STORAGE SECTION.                                         PPO1419B
012900***********************************************************       PPO1419B
013000*    WORKING STORAGE                                      *       PPO1419B
013100***********************************************************       PPO1419B
021800 01  WORK-STEP                   PIC X(4).                        PPO1419B
021930 01  WS-SWITCHES.                                                 PPO1419B
021931     05  EOF-SW                  PIC X(01)  VALUE SPACES.         PPO1419B
021932         88 EOF-DSTIN                       VALUE 'Y'.            PPO1419B
021940     EJECT                                                        PPO1419B
048010                                                                  PPO1419B
048100 PROCEDURE DIVISION.                                              PPO1419B
048600***********************************************************       PPO1419B
048700*                    M A I N L I N E                      *       PPO1419B
048800***********************************************************       PPO1419B
048900                                                                  PPO1419B
049000 0000-MAINLINE.                                                   PPO1419B
049100                                                                  PPO1419B
049300     PERFORM 1000-INITIALIZATION.                                 PPO1419B
049500     PERFORM 2000-REFORMAT-RECORDS.                               PPO1419B
049710                                                                  PPO1419B
049800     STOP RUN.                                                    PPO1419B
049900                                                                  PPO1419B
050000 0001-EXIT. EXIT.                                                 PPO1419B
050100     EJECT                                                        PPO1419B
050200***********************************************************       PPO1419B
050300*                   I N I T I A L I Z E                   *       PPO1419B
050400***********************************************************       PPO1419B
050500                                                                  PPO1419B
050600 1000-INITIALIZATION       SECTION.                               PPO1419B
050700                                                                  PPO1419B
051900     OPEN INPUT  DSTIN.                                           PPO1419B
051922     OPEN OUTPUT DSTOUT.                                          PPO1419B
062000                                                                  PPO1419B
062100 1000-EXIT.  EXIT.                                                PPO1419B
064719     EJECT                                                        PPO1419B
064720                                                                  PPO1419B
064800 2000-REFORMAT-RECORDS     SECTION.                               PPO1419B
064810***********************************************************       PPO1419B
064820*    PROCESS THE OLD FORMATTED DST ROW UNTIL EOF.         *       PPO1419B
064830***********************************************************       PPO1419B
064900                                                                  PPO1419B
065300     READ DSTIN AT END SET EOF-DSTIN TO TRUE.                     PPO1419B
065510                                                                  PPO1419B
065700     PERFORM 2100-PROCESS-FILE                                    PPO1419B
065800             UNTIL EOF-DSTIN.                                     PPO1419B
065900                                                                  PPO1419B
074000 2000-EXIT. EXIT.                                                 PPO1419B
074100     EJECT                                                        PPO1419B
074200                                                                  PPO1419B
074600 2100-PROCESS-FILE   SECTION.                                     PPO1419B
074700***********************************************************       PPO1419B
074800*    MOVE DATA FROM OLD FORMAT TO NEW FORMAT.             *       PPO1419B
074801*    WRITE RECORD.                                        *       PPO1419B
074802*    READ NEXT RECORD.                                    *       PPO1419B
074900***********************************************************       PPO1419B
075000                                                                  PPO1419B
075001     MOVE CORRESPONDING DSTIN-REC TO DSTOUT-REC.                  PPO1419B
075002     MOVE DIST-STEP OF DSTIN-REC TO WORK-STEP.                    PPO1419B
075003     PERFORM 3 TIMES                                              PPO1419B
075004         IF WORK-STEP (4:1) = SPACE                               PPO1419B
075005             MOVE WORK-STEP (3:1) TO WORK-STEP (4:1)              PPO1419B
075006             MOVE WORK-STEP (2:1) TO WORK-STEP (3:1)              PPO1419B
075007             MOVE WORK-STEP (1:1) TO WORK-STEP (2:1)              PPO1419B
075008             MOVE SPACE           TO WORK-STEP (1:1)              PPO1419B
075009         END-IF                                                   PPO1419B
075010     END-PERFORM.                                                 PPO1419B
075017     MOVE WORK-STEP     TO DIST-STEP OF DSTOUT-REC.               PPO1419B
075018     WRITE DSTOUT-REC.                                            PPO1419B
075020     READ DSTIN AT END SET EOF-DSTIN TO TRUE.                     PPO1419B
075110                                                                  PPO1419B
075200 2100-EXIT. EXIT.                                                 PPO1419B
108025     EJECT                                                        PPO1419B
127030***************END OF PPO1419B ****************************       PPO1419B
