000100**************************************************************/   30871460
000200*  PROGRAM: PPCTC01                                          */   30871460
000300*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000400*  NAME:___SRS___________ CREATION DATE:      ___01/22/03__  */   30871460
000500*  DESCRIPTION:                                              */   30871460
000600*  CONTROL TABLE CON EDIT MODULE 01                          */   30871460
000700**************************************************************/   30871460
000800 IDENTIFICATION DIVISION.                                         PPCTC01
000900 PROGRAM-ID. PPCTC01.                                             PPCTC01
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTC01
001100 DATE-COMPILED.                                                   PPCTC01
001200 ENVIRONMENT DIVISION.                                            PPCTC01
001300 DATA DIVISION.                                                   PPCTC01
001400 WORKING-STORAGE SECTION.                                         PPCTC01
001500 01  MISC.                                                        PPCTC01
001600     05  WS-COUNT              PIC S9(08)  COMP SYNC VALUE ZERO.  PPCTC01
001700     05  M01943                PIC  X(05)  VALUE '01943'.         PPCTC01
001800     05  MCT005                PIC  X(05)  VALUE 'CT005'.         PPCTC01
001900     05 GTN-TABLE-FLAG             PIC X VALUE SPACE.             PPCTC01
002000        88 GTN-TABLE-END                 VALUE 'X'.               PPCTC01
002100        88 GTN-TABLE-NOT-END             VALUE SPACE.             PPCTC01
002200                                                                  PPCTC01
002300 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTC01
002400 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTC01
002500 01  CTRI-CTL-REPORT-INTERFACE.                 COPY CPLNCTRI.    PPCTC01
002600                                                                  PPCTC01
002700 01  GTN-TABLE-DATA.                                              PPCTC01
002800     EXEC SQL                                                     PPCTC01
002900        INCLUDE PPPVZGTN                                          PPCTC01
003000     END-EXEC.                                                    PPCTC01
003100                                                                  PPCTC01
003200     EXEC SQL                                                     PPCTC01
003300         DECLARE  GTN_ROW CURSOR FOR                              PPCTC01
003400         SELECT   GTN_NUMBER                                      PPCTC01
003500                 ,GTN_CB_ELIG_IND                                 PPCTC01
003600           FROM   PPPVZGTN_GTN                                    PPCTC01
003700         ORDER BY GTN_NUMBER                                      PPCTC01
003800     END-EXEC.                                                    PPCTC01
003900                                                                  PPCTC01
004000 01  BUG-TABLE-DATA.                                              PPCTC01
004100     EXEC SQL                                                     PPCTC01
004200        INCLUDE PPPVZBUG                                          PPCTC01
004300     END-EXEC.                                                    PPCTC01
004400                                                                  PPCTC01
004500     EXEC SQL                                                     PPCTC01
004600        INCLUDE SQLCA                                             PPCTC01
004700     END-EXEC.                                                    PPCTC01
004800                                                                  PPCTC01
004900 LINKAGE SECTION.                                                 PPCTC01
005000                                                                  PPCTC01
005100 01  CTCI-CTL-TBL-CONEDIT-INTERFACE.            COPY CPLNCTCI.    PPCTC01
005200                                                                  PPCTC01
005300 PROCEDURE DIVISION USING CTCI-CTL-TBL-CONEDIT-INTERFACE.         PPCTC01
005400                                                                  PPCTC01
005500 0000-MAIN SECTION.                                               PPCTC01
005600                                                                  PPCTC01
005700     EXEC SQL                                                     PPCTC01
005800        INCLUDE CPPDXE99                                          PPCTC01
005900     END-EXEC.                                                    PPCTC01
006000                                                                  PPCTC01
006100     MOVE 'PPCTC01' TO XWHC-DISPLAY-MODULE.                       PPCTC01
006200     COPY CPPDXWHC.                                               PPCTC01
006300                                                                  PPCTC01
006400     SET CTCI-NORMAL TO TRUE.                                     PPCTC01
006500     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTC01
006600     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTC01
006700                                   CTRI-TRANSACTION               PPCTC01
006800                                   CTRI-TRANSACTION-DISPO         PPCTC01
006900                                   CTRI-TEXT-LINE                 PPCTC01
007000                                   CTRI-MESSAGE-NUMBER.           PPCTC01
007100     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTC01
007200                                                                  PPCTC01
007300     PERFORM 1000-EDIT.                                           PPCTC01
007400                                                                  PPCTC01
007500 0100-END.                                                        PPCTC01
007600                                                                  PPCTC01
007700     GOBACK.                                                      PPCTC01
007800                                                                  PPCTC01
007900 1000-EDIT SECTION.                                               PPCTC01
008000                                                                  PPCTC01
008100     PERFORM 9000-OPEN-GTN.                                       PPCTC01
008200     PERFORM 9100-FETCH-GTN.                                      PPCTC01
008300     PERFORM UNTIL GTN-TABLE-END                                  PPCTC01
008400      IF GTN-CB-ELIG-IND NOT = SPACE                              PPCTC01
008500         PERFORM 9300-COUNT-BUG                                   PPCTC01
008600         IF WS-COUNT = ZERO                                       PPCTC01
008700            STRING 'GTN NUMBER: ' GTN-NUMBER                      PPCTC01
008800            DELIMITED BY SIZE INTO CTRI-TEXT-LINE                 PPCTC01
008900            MOVE M01943 TO CTRI-MESSAGE-NUMBER                    PPCTC01
009000            PERFORM 3100-PRINT-ERROR                              PPCTC01
009100         END-IF                                                   PPCTC01
009200      END-IF                                                      PPCTC01
009300      PERFORM 9100-FETCH-GTN                                      PPCTC01
009400     END-PERFORM                                                  PPCTC01
009500     PERFORM 9200-CLOSE-GTN.                                      PPCTC01
009600                                                                  PPCTC01
009700 1099-EXIT.                                                       PPCTC01
009800     EXIT.                                                        PPCTC01
009900                                                                  PPCTC01
010000 3100-PRINT-ERROR SECTION.                                        PPCTC01
010100                                                                  PPCTC01
010200     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTC01
010300     IF CTRI-RUN-ABORTED                                          PPCTC01
010400        SET CTCI-ABORT TO TRUE                                    PPCTC01
010500        GO TO 0100-END                                            PPCTC01
010600     END-IF.                                                      PPCTC01
010700                                                                  PPCTC01
010800 3199-EXIT.                                                       PPCTC01
010900     EXIT.                                                        PPCTC01
011000                                                                  PPCTC01
011100 9000-OPEN-GTN SECTION.                                           PPCTC01
011200                                                                  PPCTC01
011300     MOVE 'OPEN GTN CURSOR' TO DB2MSG-TAG.                        PPCTC01
011400     EXEC SQL                                                     PPCTC01
011500         OPEN GTN_ROW                                             PPCTC01
011600     END-EXEC.                                                    PPCTC01
011700                                                                  PPCTC01
011800 9099-EXIT.                                                       PPCTC01
011900     EXIT.                                                        PPCTC01
012000                                                                  PPCTC01
012100 9100-FETCH-GTN SECTION.                                          PPCTC01
012200                                                                  PPCTC01
012300     MOVE 'FETCH GTN ROW' TO DB2MSG-TAG.                          PPCTC01
012400     EXEC SQL                                                     PPCTC01
012500     FETCH GTN_ROW                                                PPCTC01
012600           INTO                                                   PPCTC01
012700                 :GTN-NUMBER                                      PPCTC01
012800                ,:GTN-CB-ELIG-IND                                 PPCTC01
012900     END-EXEC.                                                    PPCTC01
013000     IF SQLCODE NOT = ZERO                                        PPCTC01
013100        SET GTN-TABLE-END TO TRUE                                 PPCTC01
013200     END-IF.                                                      PPCTC01
013300                                                                  PPCTC01
013400 9029-EXIT.                                                       PPCTC01
013500     EXIT.                                                        PPCTC01
013600                                                                  PPCTC01
013700 9200-CLOSE-GTN SECTION.                                          PPCTC01
013800                                                                  PPCTC01
013900     MOVE 'CLOSE GTN CURSOR' TO DB2MSG-TAG.                       PPCTC01
014000     EXEC SQL                                                     PPCTC01
014100         CLOSE GTN_ROW                                            PPCTC01
014200     END-EXEC.                                                    PPCTC01
014300                                                                  PPCTC01
014400 9229-EXIT.                                                       PPCTC01
014500     EXIT.                                                        PPCTC01
014600                                                                  PPCTC01
014700 9300-COUNT-BUG SECTION.                                          PPCTC01
014800                                                                  PPCTC01
014900     MOVE '9300 COUNT BUG ' TO DB2MSG-TAG.                        PPCTC01
015000     EXEC SQL                                                     PPCTC01
015100       SELECT  COUNT(*)                                           PPCTC01
015200       INTO    :WS-COUNT                                          PPCTC01
015300       FROM    PPPVZBUG_BUG                                       PPCTC01
015400       WHERE   BUG_GTN_DED_NO  = :GTN-NUMBER                      PPCTC01
015500     END-EXEC.                                                    PPCTC01
015510                                                                  PPCTC01
015600*999999-SQL-ERROR.                                                PPCTC01
015700     EXEC SQL                                                     PPCTC01
015800        INCLUDE CPPDXP99                                          PPCTC01
015900     END-EXEC.                                                    PPCTC01
016000     MOVE MCT005 TO CTRI-MESSAGE-NUMBER                           PPCTC01
016100     PERFORM 3100-PRINT-ERROR                                     PPCTC01
016200     SET CTCI-ABORT TO TRUE.                                      PPCTC01
016300     GO TO 0100-END.                                              PPCTC01
