000100**************************************************************/  *36090553
000200*  PROGRAM: PPFADUTL                                         */  *36090553
000300*  RELEASE: ____0553____  SERVICE REQUEST(S): ____3609____   */  *36090553
000400*  NAME:__PXP_____________CREATION DATE:      __04/08/91__   */  *36090553
000500*  DESCRIPTION:                                              */  *36090553
000600*  - NEW PROGRAM TO ACCESS FAD TABLE                         */  *36090553
000700**************************************************************/  *36090553
000800 IDENTIFICATION DIVISION.                                         PPFADUTL
000900 PROGRAM-ID. PPFADUTL                                             PPFADUTL
001000 AUTHOR. UCOP.                                                    PPFADUTL
001100 DATE-WRITTEN.  NOVEMBER  1990.                                   PPFADUTL
001200 DATE-COMPILED.                                                   PPFADUTL
001300*REMARKS.                                                         PPFADUTL
001400*            CALL 'PPFADUTL' USING                                PPFADUTL
001500*                            PPFADUTL-INTERFACE                   PPFADUTL
001600*                            FAD-ROW.                             PPFADUTL
001700*                                                                 PPFADUTL
001800*                                                                 PPFADUTL
001900     EJECT                                                        PPFADUTL
002000 ENVIRONMENT DIVISION.                                            PPFADUTL
002100 CONFIGURATION SECTION.                                           PPFADUTL
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPFADUTL
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPFADUTL
002400      EJECT                                                       PPFADUTL
002500*                D A T A  D I V I S I O N                         PPFADUTL
002600*                                                                 PPFADUTL
002700 DATA DIVISION.                                                   PPFADUTL
002800 WORKING-STORAGE SECTION.                                         PPFADUTL
002900 77  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                     PPFADUTL
003000     88  FIRST-TIME                VALUE 'Y'.                     PPFADUTL
003100     88  NOT-FIRST-TIME            VALUE 'N'.                     PPFADUTL
003200 77  A-STND-PROG-ID      PIC X(08) VALUE 'PPFADUTL'.              PPFADUTL
003300 01  ONLINE-SIGNALS EXTERNAL.                                     PPFADUTL
003400                                    COPY CPWSONLI.                PPFADUTL
003500 01  WS-EMPLOYEE-ID      PIC X(9).                                PPFADUTL
003600 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPFADUTL
003700     EJECT                                                        PPFADUTL
003800 01  XWHC-COMPILE-WORK-AREA.                                      PPFADUTL
003900                                    COPY CPWSXWHC.                PPFADUTL
004000******************************************************************PPFADUTL
004100*          SQL - WORKING STORAGE                                 *PPFADUTL
004200******************************************************************PPFADUTL
004300 01  FAD-ROW-DATA.                                                PPFADUTL
004400     EXEC SQL                                                     PPFADUTL
004500          INCLUDE PPPVFAD1                                        PPFADUTL
004600     END-EXEC.                                                    PPFADUTL
004700     EXEC SQL                                                     PPFADUTL
004800          INCLUDE SQLCA                                           PPFADUTL
004900     END-EXEC.                                                    PPFADUTL
005000******************************************************************PPFADUTL
005100*                                                                *PPFADUTL
005200*                SQL- SELECTS                                    *PPFADUTL
005300*                                                                *PPFADUTL
005400******************************************************************PPFADUTL
005500*                                                                *PPFADUTL
005600*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPFADUTL
005700*                                                                *PPFADUTL
005800*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPFADUTL
005900*                        MEMBER NAMES                            *PPFADUTL
006000*                                                                *PPFADUTL
006100*  PPPVFAD1_FAD          PPPVFAD1                                *PPFADUTL
006200*                                                                *PPFADUTL
006300******************************************************************PPFADUTL
006400*                                                                 PPFADUTL
006500 LINKAGE SECTION.                                                 PPFADUTL
006600*                                                                 PPFADUTL
006700******************************************************************PPFADUTL
006800*                L I N K A G E   S E C T I O N                   *PPFADUTL
006900******************************************************************PPFADUTL
007000 01  PPFADUTL-INTERFACE.            COPY CPLNKFAD.                PPFADUTL
007100 01  FAD-ROW.                       COPY CPWSRFAD.                PPFADUTL
007200*                                                                 PPFADUTL
007300 PROCEDURE DIVISION USING                                         PPFADUTL
007400                          PPFADUTL-INTERFACE                      PPFADUTL
007500                          FAD-ROW.                                PPFADUTL
007600******************************************************************PPFADUTL
007700*            P R O C E D U R E  D I V I S I O N                  *PPFADUTL
007800******************************************************************PPFADUTL
007900******************************************************************PPFADUTL
008000*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPFADUTL
008100*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPFADUTL
008200******************************************************************PPFADUTL
008300     EXEC SQL                                                     PPFADUTL
008400          INCLUDE CPPDXE99                                        PPFADUTL
008500     END-EXEC.                                                    PPFADUTL
008600 MAINLINE-100 SECTION.                                            PPFADUTL
008700     IF FIRST-TIME                                                PPFADUTL
008800         SET NOT-FIRST-TIME TO TRUE                               PPFADUTL
008900     IF  NOT ENVIRON-IS-CICS                                      PPFADUTL
009000         PERFORM 7900-FIRST-TIME                                  PPFADUTL
009100     END-IF                                                       PPFADUTL
009200     END-IF.                                                      PPFADUTL
009300     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPFADUTL
009400     MOVE PPFADUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                 PPFADUTL
009500     PERFORM SQL-SELECT-FAD.                                      PPFADUTL
009600     IF  SQLCODE = +0                                             PPFADUTL
009700         MOVE  EMPLOYEE-ID       OF FAD-ROW-DATA                  PPFADUTL
009800         TO    EMPLOYEE-ID       OF FAD-ROW                       PPFADUTL
009900         MOVE FAD-COUNTRY        OF FAD-ROW-DATA                  PPFADUTL
010000         TO   FAD-COUNTRY        OF FAD-ROW                       PPFADUTL
010100         MOVE FAD-PROVINCE       OF FAD-ROW-DATA                  PPFADUTL
010200         TO   FAD-PROVINCE       OF FAD-ROW                       PPFADUTL
010300         MOVE FAD-POSTAL-CODE    OF FAD-ROW-DATA                  PPFADUTL
010400         TO   FAD-POSTAL-CODE    OF FAD-ROW                       PPFADUTL
010500     END-IF.                                                      PPFADUTL
010600 MAINLINE-999.                                                    PPFADUTL
010700     GOBACK.                                                      PPFADUTL
010800******************************************************************PPFADUTL
010900*  PROCEDURE SQL       SELECT        FOR FAD VIEW                *PPFADUTL
011000******************************************************************PPFADUTL
011100 SQL-SELECT-FAD     SECTION.                                      PPFADUTL
011200     MOVE 'SELECT FAD ROW' TO DB2MSG-TAG.                         PPFADUTL
011300     EXEC SQL                                                     PPFADUTL
011400         SELECT * INTO :FAD-ROW-DATA                              PPFADUTL
011500                  FROM PPPVFAD1_FAD                               PPFADUTL
011600             WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                  PPFADUTL
011700     END-EXEC.                                                    PPFADUTL
011800     IF  SQLCODE        = +100                                    PPFADUTL
011900         MOVE 'N' TO PPFADUTL-FAD-FOUND-SW                        PPFADUTL
012000         INITIALIZE FAD-ROW                                       PPFADUTL
012100     ELSE                                                         PPFADUTL
012200         SET PPFADUTL-FAD-FOUND TO TRUE                           PPFADUTL
012300     END-IF.                                                      PPFADUTL
012400    SKIP1                                                         PPFADUTL
012500     EJECT                                                        PPFADUTL
012600 7900-FIRST-TIME SECTION.                                         PPFADUTL
012700     SKIP1                                                        PPFADUTL
012800     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPFADUTL
012900     SKIP1                                                        PPFADUTL
013000                                    COPY CPPDXWHC.                PPFADUTL
013100*999999-SQL-ERROR.                                               *PPFADUTL
013200     COPY CPPDXP99.                                               PPFADUTL
013300     SET PPFADUTL-ERROR TO TRUE.                                  PPFADUTL
013400     GOBACK.                                                      PPFADUTL
