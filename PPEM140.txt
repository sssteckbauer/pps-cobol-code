000000**************************************************************/   04611523
000001*  PROGRAM: PPEM140                                          */   04611523
000002*  RELEASE: ___1523______ SERVICE REQUEST(S): ____80461____  */   04611523
000003*  NAME:____J KENNEDY____ MODIFICATION DATE:  ___07/28/03__  */   04611523
000004*  DESCRIPTION:                                              */   04611523
000005*  - REMOVE THE DE-ENROLLMENT OF SEPARATED EMPLOYEES FROM    */   04611523
000006*  DEPCARE.  THIS IS NOW ACCOMPLISHED  WHEN THE DEPCARE      */   04611523
000007*  TERMINATION DATE IS REACHED (NEW EDB0315) WHICH IS SET    */   04611523
000008*  TO THE LAST DAY OF THE MONTH FOLLOWING THE DATE OF SEPAR- */   04611523
000009*  ATION AT THE TIME THAT THE SEPARATION ENTERS THE SYSTEM.  */   04611523
000010*                                                            */   04611523
000020**************************************************************/   04611523
000100**************************************************************/   34121120
000200*  PROGRAM: PPEM140                                          */   34121120
000300*  RELEASE: ___1120______ SERVICE REQUEST(S): _14280, 13412  */   34121120
000400*  REF REL: ___1061______                                    */   34121120
000500*  NAME:______M SANO_____ MODIFICATION DATE:  ___04/07/97__  */   34121120
000600*  DESCRIPTION:                                              */   34121120
000700*  - CHECK SEPARATE DATE AGAINST FIRST OF MONTH DATE BEFORE  */   42801120
000800*    DE-ENROLLMENT                                           */   42801120
000900*  - DE-ENROLL APPROPRIATE SEPARATED EMPLOYEES FROM DEPCARE  */   34121120
001000**************************************************************/   34121120
000000**************************************************************/   42801061
000001*  PROGRAM: PPEM140                                          */   42801061
000002*  RELEASE: ___1061______ SERVICE REQUEST(S): ____14280____  */   42801061
000003*  NAME:_______QUAN______ CREATION DATE:      ___03/18/96__  */   42801061
000004*  DESCRIPTION:                                              */   42801061
000005*  - DE-ENROLL APPROPRIATE SEPARATED EMPLOYEES FROM THE      */   42801061
000006*    403(B) PLANS. EMPLOYEES WITH SEPARATION REASON CODE OF  */   42801061
000007*    'BA' AND 'BB' ARE NOT DE-ENROLLED FROM 403(B) PLAN(S).  */   42801061
000008**************************************************************/   42801061
002600*----------------------------------------------------------------*PPEM140
002700                                                                  PPEM140
002800 IDENTIFICATION DIVISION.                                         PPEM140
002900 PROGRAM-ID. PPEM140.                                             PPEM140
003000 AUTHOR. UCOP.                                                    PPEM140
003100 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEM140
003200                                                                  PPEM140
003300 DATE-WRITTEN.  JULY 22,1992.                                     PPEM140
003400 DATE-COMPILED. XX/XX/XX.                                         PPEM140
003500     EJECT                                                        PPEM140
003600 ENVIRONMENT DIVISION.                                            PPEM140
003700 CONFIGURATION SECTION.                                           PPEM140
003800 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEM140
003900 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEM140
004000 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEM140
004100 INPUT-OUTPUT SECTION.                                            PPEM140
004200 FILE-CONTROL.                                                    PPEM140
004300                                                                  PPEM140
004400 DATA DIVISION.                                                   PPEM140
004500 FILE SECTION.                                                    PPEM140
004600     EJECT                                                        PPEM140
004700*----------------------------------------------------------------*PPEM140
004800 WORKING-STORAGE SECTION.                                         PPEM140
004900*----------------------------------------------------------------*PPEM140
005000                                                                  PPEM140
005100 01  A-STND-PROG-ID               PIC X(15) VALUE                 PPEM140
004700****                                                           CD 04611523
004701**** 'PPEM140 /030196'.                                           04611523
004710     'PPEM140 /072803'.                                           04611523
005400                                                                  PPEM140
005500 01  A-STND-MSG-PARMS             REDEFINES                       PPEM140
005600     A-STND-PROG-ID.                                              PPEM140
005700     05  FILLER                   PIC X(03).                      PPEM140
005800     05  WS-ROUTINE-TYPE          PIC X(01).                      PPEM140
005900     05  WS-ROUTINE-NUM           PIC 9(03).                      PPEM140
006000     05  FILLER                   PIC X(08).                      PPEM140
006100                                                                  PPEM140
006200     EJECT                                                        PPEM140
006210 01  WS-CALLED-PGMS.                                              PPEM140
006211     05  WS-PPSETUTL              PIC X(08)  VALUE 'PPSETUTL'.    PPEM140
006212                                                                  PPEM140
006220 01  WS-MESSAGE                   PIC X(05).                      PPEM140
006300 01  MESSAGES-USED.                                               PPEM140
006500     05  M13105                   PIC X(05)  VALUE '13105'.       PPEM140
006600     05  M13106                   PIC X(05)  VALUE '13106'.       PPEM140
006700     05  M13107                   PIC X(05)  VALUE '13107'.       PPEM140
006800                                                                  PPEM140
006900 01  ELEMENTS-USED.                                               PPEM140
007000     05  E6000                     PIC 9(4)  VALUE  6000.         PPEM140
007100     05  E7000                     PIC 9(4)  VALUE  7000.         PPEM140
008300                                                                  PPEM140
008400 01  MISC-WORK-AREAS.                                             PPEM140
008500     05  ACTION-ON                PIC X(01) VALUE 'Y'.            PPEM140
008600     05  FIRST-TIME-SW            PIC X(01) VALUE LOW-VALUES.     PPEM140
008700         88  THIS-IS-THE-FIRST-TIME         VALUE LOW-VALUES.     PPEM140
008800         88  NOT-THE-FIRST-TIME             VALUE 'Y'.            PPEM140
008900     05  I1                       PIC S9(4) VALUE +0.             PPEM140
009010     05  403B-GTN-INDX            PIC S9(4) VALUE +0.             PPEM140
009100     05  DSA-GTN-P                PIC S9(1) COMP-3 VALUE +1.      PPEM140
007800     05  DEP-CARE-GTN             PIC S9(4) COMP VALUE +225.      34121120
010000                                                                  PPEM140
010100     EJECT                                                        PPEM140
010200 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEM140
010600                                                                  PPEM140
010700     EJECT                                                        PPEM140
010800 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEM140
010900                                                                  PPEM140
011000     EJECT                                                        PPEM140
011100 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEM140
011200                                                                  PPEM140
011300     EJECT                                                        PPEM140
011700 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEM140
011800                                                                  PPEM140
011900     EJECT                                                        PPEM140
012000 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEM140
012100                                                                  PPEM140
012110     EJECT                                                        PPEM140
012120******************************************************************PPEM140
012130**               PPSETUTL INTERFACE RECORD                      **PPEM140
012140******************************************************************PPEM140
012150     SKIP1                                                        PPEM140
012160 01  PPSETUTL-INTERFACE.              COPY CPLNKSET.              PPEM140
012170     SKIP2                                                        PPEM140
012180 01  XSET-GTN-SET-TABLE.              COPY CPWSXSET.              PPEM140
012200                                                                  PPEM140
012300     EJECT                                                        PPEM140
012400******************************************************************PPEM140
012500**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEM140
012600**--------------------------------------------------------------**PPEM140
012700** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEM140
012800******************************************************************PPEM140
012900                                                                  PPEM140
012920 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEM140
012921                                                                  PPEM140
012922     EJECT                                                        PPEM140
012923 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEM140
012924                                                                  PPEM140
012930                                                                  PPEM140
012940     EJECT                                                        PPEM140
013000 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEM140
013100                                                                  PPEM140
013200     EJECT                                                        PPEM140
013300 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEM140
013400                                                                  PPEM140
013500     EJECT                                                        PPEM140
013600 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEM140
013700                                                                  PPEM140
013800     EJECT                                                        PPEM140
013900 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEM140
014000                                                                  PPEM140
014100 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEM140
014200                                                                  PPEM140
014300 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEM140
014400                                                                  PPEM140
014500 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEM140
014800                                                                  PPEM140
017300     EJECT                                                        PPEM140
017400                                                                  PPEM140
017500 01  DEDUCTION-SEGMENT-ARRAY             EXTERNAL. COPY CPWSEDSA. PPEM140
017600                                                                  PPEM140
059910 01  BENEFIT-SEGMENT-ARRAY               EXTERNAL. COPY CPWSEBSA. PPEM140
059911                                                                  PPEM140
059920     EJECT                                                        PPEM140
059930******************************************************************PPEM140
059940**  DATA BASE AREAS                                             **PPEM140
059941******************************************************************PPEM140
059942                                                                  PPEM140
059947 01  PER-ROW                             EXTERNAL.                PPEM140
059948     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEM140
059950                                                                  PPEM140
059951     EJECT                                                        PPEM140
059952******************************************************************PPEM140
059953**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEM140
059954******************************************************************PPEM140
059955                                                                  PPEM140
059956*----------------------------------------------------------------*PPEM140
059957 LINKAGE SECTION.                                                 PPEM140
059958*----------------------------------------------------------------*PPEM140
059959                                                                  PPEM140
059960******************************************************************PPEM140
059961**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEM140
059962**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEM140
059963******************************************************************PPEM140
059964                                                                  PPEM140
059965 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEM140
059966                                                                  PPEM140
059967     EJECT                                                        PPEM140
059968******************************************************************PPEM140
059969**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEM140
059970**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEM140
059971******************************************************************PPEM140
059972                                                                  PPEM140
059973 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEM140
059974                                                                  PPEM140
059975     EJECT                                                        PPEM140
059976******************************************************************PPEM140
059977**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEM140
059978******************************************************************PPEM140
059979                                                                  PPEM140
059980 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEM140
059981                          KMTA-MESSAGE-TABLE-ARRAY.               PPEM140
059982                                                                  PPEM140
059983                                                                  PPEM140
059984*----------------------------------------------------------------*PPEM140
059985 0000-DRIVER.                                                     PPEM140
059986*----------------------------------------------------------------*PPEM140
059987                                                                  PPEM140
059988******************************************************************PPEM140
059989**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEM140
059990******************************************************************PPEM140
059991                                                                  PPEM140
059992     IF  THIS-IS-THE-FIRST-TIME                                   PPEM140
059993         PERFORM 0100-INITIALIZE                                  PPEM140
059994     END-IF.                                                      PPEM140
059995                                                                  PPEM140
059996******************************************************************PPEM140
059997**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEM140
059998**  TO THE CALLING PROGRAM                                      **PPEM140
059999******************************************************************PPEM140
060000                                                                  PPEM140
060001     PERFORM 1000-MAINLINE-ROUTINE.                               PPEM140
060002                                                                  PPEM140
060003     EXIT PROGRAM.                                                PPEM140
060004                                                                  PPEM140
060005                                                                  PPEM140
060006     EJECT                                                        PPEM140
060007*----------------------------------------------------------------*PPEM140
060008 0100-INITIALIZE    SECTION.                                      PPEM140
060009*----------------------------------------------------------------*PPEM140
060010                                                                  PPEM140
060011******************************************************************PPEM140
060012**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEM140
060013******************************************************************PPEM140
060014                                                                  PPEM140
060015     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEM140
060016                                                                  PPEM140
060017*                                               *-------------*   PPEM140
060018                                                 COPY CPPDXWHC.   PPEM140
060019*                                               *-------------*   PPEM140
060020                                                                  PPEM140
060021******************************************************************PPEM140
060022**   CALL UTILITY TO LOAD 403(B) RELATED GTN DATA.              **PPEM140
060023******************************************************************PPEM140
060024                                                                  PPEM140
060025     MOVE '1' TO KSET-SET-INDICATOR.                              PPEM140
060026     CALL WS-PPSETUTL                                             PPEM140
060027          USING  PPSETUTL-INTERFACE                               PPEM140
060028                 XSET-GTN-SET-TABLE.                              PPEM140
060029                                                                  PPEM140
060030     IF NOT KSET-TABLE-LOADED                                     PPEM140
060031        EVALUATE TRUE                                             PPEM140
060032           WHEN KSET-DB2-STATUS-ERROR                             PPEM140
060033              MOVE M13105 TO WS-MESSAGE                           PPEM140
060034           WHEN KSET-BAD-GTN-RECORD                               PPEM140
060035              MOVE M13106 TO WS-MESSAGE                           PPEM140
060036           WHEN KSET-EMPTY-GTN-TABLE                              PPEM140
060037              MOVE M13107 TO WS-MESSAGE                           PPEM140
060038        END-EVALUATE                                              PPEM140
060039        ADD +1               TO KMTA-CTR                          PPEM140
060040        MOVE WS-MESSAGE      TO KMTA-MSG-NUMBER   (KMTA-CTR)      PPEM140
060041        MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE (KMTA-CTR)      PPEM140
060042        MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM  (KMTA-CTR)      PPEM140
060043        MOVE SPACES          TO KMTA-DATA-FIELD   (KMTA-CTR)      PPEM140
060044     END-IF.                                                      PPEM140
060045                                                                  PPEM140
060046******************************************************************PPEM140
060047**   SET FIRST CALL SWITCH OFF                                  **PPEM140
060048******************************************************************PPEM140
060049                                                                  PPEM140
060050     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEM140
060055                                                                  PPEM140
060056     EJECT                                                        PPEM140
060057*----------------------------------------------------------------*PPEM140
060058 1000-MAINLINE-ROUTINE    SECTION.                                PPEM140
060059*----------------------------------------------------------------*PPEM140
060060                                                                  PPEM140
060061     MOVE EMP-STATUS              TO W88-EMP-STATUS.              PPEM140
060062     MOVE SEPARATE-REASON         TO W88-SEPARATE-REASON.         PPEM140
060063                                                                  PPEM140
060064     IF   EMP-STATUS-SEPARATED-STATUS                             PPEM140
025900       IF SEPARATE-DATE <  XDTS-ISO-FIRST-OF-MONTH-DATE           42801120
060065          IF    (NOT SEPARATE-REASON-GRANT-EXPIRED)               PPEM140
060066             AND                                                  PPEM140
060067                (NOT SEPARATE-REASON-APPTMNT-EXPIRE)              PPEM140
060068             PERFORM 1100-INITIAL-403B-DEDUCTION                  PPEM140
026400*            PERFORM 1200-INITIAL-DEPCARE-DEDUCTION               04611523
060069          END-IF                                                  PPEM140
026600       END-IF                                                     42801120
060070     END-IF.                                                      PPEM140
060071                                                                  PPEM140
060072                                                                  PPEM140
060073*----------------------------------------------------------------*PPEM140
060074 1100-INITIAL-403B-DEDUCTION   SECTION.                           PPEM140
060080*----------------------------------------------------------------*PPEM140
060160                                                                  PPEM140
060161******************************************************************PPEM140
060162**  CLEAR ALL OF THE OUTSTANDING 403(B) 'G' BALANCES            **PPEM140
060164******************************************************************PPEM140
060165                                                                  PPEM140
060169     PERFORM                                                      PPEM140
060170         VARYING I1  FROM 1 BY 1                                  PPEM140
060180            UNTIL I1  > XSET-ENTRY-COUNT                          PPEM140
085014            MOVE XSET-GTN-NUMBER (I1) TO 403B-GTN-INDX            PPEM140
085015            IF  EDSA-BALAMT (403B-GTN-INDX, DSA-GTN-P) > ZERO     PPEM140
085016                MOVE ZERO                                         PPEM140
085017                     TO EDSA-BALAMT (403B-GTN-INDX, DSA-GTN-P)    PPEM140
085018                COMPUTE DL-FIELD-R = E6000 + 403B-GTN-INDX        PPEM140
085019                PERFORM 9050-AUDITING-RESPONSIBILITIES            PPEM140
085020                MOVE XDTS-ISO-ZERO-DATE                           PPEM140
085021                          TO EBSA-DED-EFF-DATE (403B-GTN-INDX)    PPEM140
085022                COMPUTE DL-FIELD-R = E7000 + 403B-GTN-INDX        PPEM140
085023                PERFORM 9050-AUDITING-RESPONSIBILITIES            PPEM140
085030            END-IF                                                PPEM140
085035     END-PERFORM.                                                 PPEM140
085037                                                                  PPEM140
085038     EJECT                                                        PPEM140
029500*NO LONGER NEEDED                                                *04611523
029510*----------------------------------------------------------------*04611523
029600*1200-INITIAL-DEPCARE-DEDUCTION SECTION.                          04611523
029700*----------------------------------------------------------------*04611523
029800*    IF  EDSA-BALAMT (DEP-CARE-GTN, DSA-GTN-P) > ZERO             04611523
029900*        MOVE ZERO                                                04611523
030000*              TO EDSA-BALAMT (DEP-CARE-GTN, DSA-GTN-P)           04611523
030100*        COMPUTE DL-FIELD-R = E6000 + DEP-CARE-GTN                04611523
030200*        PERFORM 9050-AUDITING-RESPONSIBILITIES                   04611523
030300*        MOVE XDTS-ISO-ZERO-DATE                                  04611523
030400*                   TO EBSA-DED-EFF-DATE (DEP-CARE-GTN)           04611523
030500*        COMPUTE DL-FIELD-R = E7000 + DEP-CARE-GTN                04611523
030600*        PERFORM 9050-AUDITING-RESPONSIBILITIES                   04611523
030700*    END-IF.                                                      04611523
085039*----------------------------------------------------------------*PPEM140
085040 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEM140
085041*----------------------------------------------------------------*PPEM140
085042                                                                  PPEM140
085043*                                                 *-------------* PPEM140
085044                                                   COPY CPPDXDEC. PPEM140
085045*                                                 *-------------* PPEM140
085046                                                                  PPEM140
085047*----------------------------------------------------------------*PPEM140
085048 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEM140
085049*----------------------------------------------------------------*PPEM140
085050                                                                  PPEM140
085051*                                                 *-------------* PPEM140
085052                                                   COPY CPPDADLC. PPEM140
085053*                                                 *-------------* PPEM140
085054                                                                  PPEM140
085068     EJECT                                                        PPEM140
085069******************************************************************PPEM140
085070*    E N D  S O U R C E   ----  PPEM140 ----                     *PPEM140
085071******************************************************************PPEM140
