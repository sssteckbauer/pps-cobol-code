000000**************************************************************/   28521025
000001*  PROGRAM: PPHCUUTL                                         */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ CREATION DATE:      ___08/22/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THIS IS A TOTAL REPLACEMENT PROGRAM FOR PPHCUUTL.       */   28521025
000008*    THIS MODULE CALLS THE DB2 HEALTH SCIENCE SEVERANCE PAY  */   28521025
000009*    CONTRIBUTION TABLE.                                     */   28521025
000010**************************************************************/   28521025
000900 IDENTIFICATION DIVISION.                                         PPHCUUTL
001000 PROGRAM-ID. PPHCUUTL.                                            PPHCUUTL
001100 INSTALLATION. UNIVERSITY OF CALIFORNIA, BASE PAYROLL SYSTEM.     PPHCUUTL
001200 AUTHOR.  UCOP.                                                   PPHCUUTL
001300 DATE-WRITTEN.  AUGUST,1995.                                      PPHCUUTL
001400 DATE-COMPILED.                                                   PPHCUUTL
001500******************************************************************PPHCUUTL
001600*                                                                *PPHCUUTL
001700*  - PPHCUUTL IS A MODULE WHICH RETRIEVES THE MATCHING HEALTH    *PPHCUUTL
001800*    SCIENCE SEVERANCE PAY CONTRIBUTION UNIT ID, NAME, AND       *PPHCUUTL
001900*    CONTRIBUTION PERCENTAGE FOR A GIVEN ACCOUNT NUMBER OR       *PPHCUUTL
002000*    DEPARTMENT CODE.                                            *PPHCUUTL
002100*  - EACH CAMPUS MUST DETERMINE IF THEIR CONTRIBUTION UNITS ARE  *PPHCUUTL
002200*    BASED ON DEPARTMENTS OR ON ACCOUNTS.                        *PPHCUUTL
002300*  - THE FIRST CALL LOADS THE HCU TABLE FROM THE DB2 CONTROL     *PPHCUUTL
002400*    TABLE FILE.                                                 *PPHCUUTL
002700*                                                                *PPHCUUTL
002800******************************************************************PPHCUUTL
002900 ENVIRONMENT DIVISION.                                            PPHCUUTL
003000 CONFIGURATION SECTION.                                           PPHCUUTL
003100 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       PPHCUUTL
003200 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       PPHCUUTL
003300 SPECIAL-NAMES.                                                   PPHCUUTL
003400 INPUT-OUTPUT SECTION.                                            PPHCUUTL
003500 FILE-CONTROL.                                                    PPHCUUTL
003600     EJECT                                                        PPHCUUTL
003700******************************************************************PPHCUUTL
003800*                                                                *PPHCUUTL
003900*                         DATA DIVISION                          *PPHCUUTL
004000*                                                                *PPHCUUTL
004100******************************************************************PPHCUUTL
004200 DATA DIVISION.                                                   PPHCUUTL
004300 FILE SECTION.                                                    PPHCUUTL
004400     SKIP3                                                        PPHCUUTL
004500******************************************************************PPHCUUTL
004600*                                                                *PPHCUUTL
004700*                    WORKING-STORAGE SECTION                     *PPHCUUTL
004800*                                                                *PPHCUUTL
004900******************************************************************PPHCUUTL
005000 WORKING-STORAGE SECTION.                                         PPHCUUTL
005100     SKIP1                                                        PPHCUUTL
005200 01  MISCELLANEOUS-VARIABLES.                                     PPHCUUTL
005201     05  HCA-INDEX            PIC S9(4)  VALUE ZEROES  COMP.      PPHCUUTL
005202     05  WS-HCU-SQLCODE       PIC S9(9)  VALUE ZEROES  COMP.      PPHCUUTL
005210     05 FILLER            PIC X(01)         VALUE SPACE.          PPHCUUTL
005220         88 HCU-ROW-OPEN VALUE 'Y'.                               PPHCUUTL
005230         88 NOT-HCU-ROW-OPEN VALUE SPACE.                         PPHCUUTL
005240     05 FILLER            PIC X(01)         VALUE SPACE.          PPHCUUTL
005250         88 HCA-ROW-OPEN VALUE 'Y'.                               PPHCUUTL
005260         88 NOT-HCA-ROW-OPEN VALUE SPACE.                         PPHCUUTL
005300     05  HCU-RUN-FLAG           PIC X VALUE 'F'.                  PPHCUUTL
005400         88  FIRST-CALL               VALUE 'F'.                  PPHCUUTL
005500         88  LOADING-TABLE            VALUE 'L'.                  PPHCUUTL
005600         88  TABLE-LOADED             VALUE 'Y'.                  PPHCUUTL
005700         88  TABLE-LOAD-FAILED        VALUE 'X'.                  PPHCUUTL
005800     05  HCU-SEARCH-FLAG        PIC X.                            PPHCUUTL
005900         88  ENTRY-FOUND              VALUE 'Y'.                  PPHCUUTL
006000         88  ENTRY-NOT-FOUND          VALUE 'N'.                  PPHCUUTL
006500     05  HOLD-ACTION            PIC X(12).                        PPHCUUTL
006600     EJECT                                                        PPHCUUTL
006700 01  HCU-TABLE-AREA.                                              PPHCUUTL
006800     05  HCU-ENTRY-COUNT          PIC S9(04) COMP SYNC.           PPHCUUTL
006900     05  HCU-TABLE.                                               PPHCUUTL
007000         10  HCU-ROW              OCCURS 0 TO 999 TIMES           PPHCUUTL
007100                                  DEPENDING ON HCU-ENTRY-COUNT    PPHCUUTL
007200                                  INDEXED BY HCU-INDEX.           PPHCUUTL
007300             15  HCU-DELETE                  PIC X(01).           PPHCUUTL
007400             15  HCU-KEY.                                         PPHCUUTL
007500                 20  HCU-KEY-CONSTANT        PIC X(04).           PPHCUUTL
007600                 20  HCU-KEY-UNIT-ID         PIC X(06).           PPHCUUTL
007700                 20  FILLER                  PIC X(03).           PPHCUUTL
007800             15  HCU-UNIT-NAME-X             PIC X(30).           PPHCUUTL
007900             15  HCU-UNIT-PERCENTAGE         PIC 99V99.           PPHCUUTL
008000             15  HCU-ACCT-DEPT-RANGES.                            PPHCUUTL
008100                 20  HCU-ACCT-DEPT-RANGE     OCCURS 14 TIMES      PPHCUUTL
008200                                             INDEXED BY           PPHCUUTL
008300                                               HCU-RANGE-INDEX.   PPHCUUTL
008400                     25  HCU-LOW-ACCT-DEPT-X PIC X(06).           PPHCUUTL
008500                     25  HCU-HIGH-ACCT-DEPT-X  PIC X(06).         PPHCUUTL
008600             15  FILLER                      PIC X(01).           PPHCUUTL
008700             15  HCU-LAST-ACTION             PIC X(01).           PPHCUUTL
008800             15  HCU-LAST-UPDATE             PIC X(06).           PPHCUUTL
008900     EJECT                                                        PPHCUUTL
009000 01  XHCU-HSSP-CONT-UNIT-RECORD.  COPY 'CPWSXHCU'.                PPHCUUTL
009010 01  PPDB2MSG-INTERFACE.                    COPY CPLNKDB2.        PPHCUUTL
009100     EJECT                                                        PPHCUUTL
009800******************************************************************PPHCUUTL
009300*          SQL - WORKING STORAGE                                 *PPHCUUTL
009400******************************************************************PPHCUUTL
009500 01  HCU-ROW-DATA.                                                PPHCUUTL
009600     EXEC SQL                                                     PPHCUUTL
009700          INCLUDE PPPVZHCU                                        PPHCUUTL
009710     END-EXEC.                                                    PPHCUUTL
009711 01  HCA-ROW-DATA.                                                PPHCUUTL
009712     EXEC SQL                                                     PPHCUUTL
009713          INCLUDE PPPVZHCA                                        PPHCUUTL
009714     END-EXEC.                                                    PPHCUUTL
009720     EXEC SQL                                                     PPHCUUTL
009730          INCLUDE SQLCA                                           PPHCUUTL
009740     END-EXEC.                                                    PPHCUUTL
009750*                                                                 PPHCUUTL
009760     EXEC SQL                                                     PPHCUUTL
009770          DECLARE HCU_ROW CURSOR FOR                              PPHCUUTL
009780              SELECT *                                            PPHCUUTL
009790              FROM PPPVZHCU_HCU                                   PPHCUUTL
009791              ORDER BY                                            PPHCUUTL
009792                  HCU_UNIT_ID                                     PPHCUUTL
009794     END-EXEC.                                                    PPHCUUTL
009795*                                                                 PPHCUUTL
009796     EXEC SQL                                                     PPHCUUTL
009797          DECLARE HCA_ROW CURSOR FOR                              PPHCUUTL
009798              SELECT *                                            PPHCUUTL
009799              FROM PPPVZHCA_HCA                                   PPHCUUTL
009800              WHERE  HCA_UNIT_ID  = :HCU-UNIT-ID                  PPHCUUTL
009801              ORDER BY                                            PPHCUUTL
009802                  HCA_UNIT_ID, HCA_LOW_ACCT_DEPT                  PPHCUUTL
009803     END-EXEC.                                                    PPHCUUTL
009810******************************************************************PPHCUUTL
009900*                                                                *PPHCUUTL
010000*                        LINKAGE SECTION                         *PPHCUUTL
010100*                                                                *PPHCUUTL
010200******************************************************************PPHCUUTL
010210     EJECT                                                        PPHCUUTL
010300 LINKAGE SECTION.                                                 PPHCUUTL
010400     SKIP1                                                        PPHCUUTL
010500 01  PPHCUUTL-INTERFACE.          COPY 'CPLNKHCU'.                PPHCUUTL
010600     EJECT                                                        PPHCUUTL
010700******************************************************************PPHCUUTL
010800*                                                                *PPHCUUTL
010900*                       PROCEDURE DIVISION                       *PPHCUUTL
011000*                                                                *PPHCUUTL
011100******************************************************************PPHCUUTL
011200     SKIP1                                                        PPHCUUTL
011300 PROCEDURE DIVISION USING PPHCUUTL-INTERFACE.                     PPHCUUTL
011400     SKIP1                                                        PPHCUUTL
011410******************************************************************PPHCUUTL
011420*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPHCUUTL
011430*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPHCUUTL
011440******************************************************************PPHCUUTL
011450     EXEC SQL                                                     PPHCUUTL
011460          INCLUDE CPPDXE99                                        PPHCUUTL
011470     END-EXEC.                                                    PPHCUUTL
011480     MOVE 'PPHCUUTL'     TO DB2MSG-PGM-ID.                        PPHCUUTL
011490     SKIP1                                                        PPHCUUTL
011500 1000-MAINLINE SECTION.                                           PPHCUUTL
011600 1000-ENTRY.                                                      PPHCUUTL
011700     SKIP1                                                        PPHCUUTL
011800     SET KHCU-NORMAL-RETURN TO TRUE.                              PPHCUUTL
011900     MOVE SPACES TO KHCU-REF.                                     PPHCUUTL
012000     EVALUATE TRUE                                                PPHCUUTL
012100       WHEN TABLE-LOADED                                          PPHCUUTL
012200             PERFORM 3000-LOOKUP-CONT-UNIT                        PPHCUUTL
012300       WHEN FIRST-CALL                                            PPHCUUTL
012400         SET LOADING-TABLE TO TRUE                                PPHCUUTL
012500         MOVE 999 TO HCU-ENTRY-COUNT                              PPHCUUTL
012600         MOVE SPACES TO HOLD-ACTION,                              PPHCUUTL
012700                        HCU-TABLE                                 PPHCUUTL
012800         SKIP1                                                    PPHCUUTL
013200         PERFORM 1200-LOAD-HCU-TABLE                              PPHCUUTL
013000         SKIP1                                                    PPHCUUTL
013100         IF KHCU-NORMAL-RETURN                                    PPHCUUTL
014000             SET TABLE-LOADED TO TRUE                             PPHCUUTL
014100             PERFORM 3000-LOOKUP-CONT-UNIT                        PPHCUUTL
014200         ELSE                                                     PPHCUUTL
014300             SET TABLE-LOAD-FAILED TO TRUE                        PPHCUUTL
014400         END-IF                                                   PPHCUUTL
014500       WHEN OTHER                                                 PPHCUUTL
014600         SET KHCU-PRIOR-ABEND TO TRUE                             PPHCUUTL
014700     END-EVALUATE.                                                PPHCUUTL
014800     SKIP3                                                        PPHCUUTL
014900     GOBACK.                                                      PPHCUUTL
017700 1200-LOAD-HCU-TABLE.                                             PPHCUUTL
017800     SKIP1                                                        PPHCUUTL
017900     PERFORM SQL-OPEN-HCU.                                        PPHCUUTL
018100     IF KHCU-NORMAL-RETURN                                        PPHCUUTL
018200        SET HCU-INDEX TO 1                                        PPHCUUTL
018210        PERFORM SQL-FETCH-HCU                                     PPHCUUTL
018220            UNTIL WS-HCU-SQLCODE NOT = ZERO                       PPHCUUTL
019300        IF KHCU-NORMAL-RETURN                                     PPHCUUTL
019400            SET HCU-INDEX DOWN BY 1                               PPHCUUTL
019500            SET HCU-ENTRY-COUNT TO HCU-INDEX                      PPHCUUTL
019600            IF HCU-ENTRY-COUNT > ZERO                             PPHCUUTL
019700                CONTINUE                                          PPHCUUTL
019800            ELSE                                                  PPHCUUTL
019900                SET KHCU-EMPTY-HCU-TABLE TO TRUE                  PPHCUUTL
020000            END-IF                                                PPHCUUTL
020100        END-IF                                                    PPHCUUTL
020200     END-IF.                                                      PPHCUUTL
020300     PERFORM SQL-CLOSE-HCU.                                       PPHCUUTL
020300     EJECT                                                        PPHCUUTL
024100 3000-LOOKUP-CONT-UNIT.                                           PPHCUUTL
024200     SKIP1                                                        PPHCUUTL
024300     SET ENTRY-NOT-FOUND TO TRUE.                                 PPHCUUTL
024400     PERFORM VARYING HCU-INDEX FROM 1 BY 1                        PPHCUUTL
024500       UNTIL HCU-INDEX > HCU-ENTRY-COUNT OR                       PPHCUUTL
024600             ENTRY-FOUND                                          PPHCUUTL
024700         SET HCU-RANGE-INDEX TO 1                                 PPHCUUTL
024800         SEARCH HCU-ACCT-DEPT-RANGE VARYING HCU-RANGE-INDEX       PPHCUUTL
024900             AT END                                               PPHCUUTL
025000               CONTINUE;                                          PPHCUUTL
025100             WHEN HCU-LOW-ACCT-DEPT-X (HCU-INDEX, HCU-RANGE-INDEX)PPHCUUTL
025200                    = SPACES                                      PPHCUUTL
025300               CONTINUE;                                          PPHCUUTL
025400             WHEN KHCU-ACCT-DEPT <                                PPHCUUTL
025500                  HCU-LOW-ACCT-DEPT-X (HCU-INDEX, HCU-RANGE-INDEX)PPHCUUTL
025600               CONTINUE;                                          PPHCUUTL
025700             WHEN KHCU-ACCT-DEPT NOT >                            PPHCUUTL
025800                 HCU-HIGH-ACCT-DEPT-X (HCU-INDEX, HCU-RANGE-INDEX)PPHCUUTL
025900               MOVE HCU-KEY-UNIT-ID (HCU-INDEX) TO KHCU-CU-ID     PPHCUUTL
026000               MOVE HCU-UNIT-NAME-X (HCU-INDEX) TO KHCU-CU-NAME   PPHCUUTL
026100               MOVE HCU-UNIT-PERCENTAGE (HCU-INDEX) TO            PPHCUUTL
026200                   KHCU-CU-PERCENTAGE                             PPHCUUTL
026300               SET ENTRY-FOUND TO TRUE                            PPHCUUTL
026400         END-SEARCH                                               PPHCUUTL
026500     END-PERFORM.                                                 PPHCUUTL
026600     SKIP1                                                        PPHCUUTL
026700     IF ENTRY-NOT-FOUND                                           PPHCUUTL
026800         MOVE SPACES TO KHCU-CU-ID,                               PPHCUUTL
026900                        KHCU-CU-NAME                              PPHCUUTL
027000         MOVE ZERO TO KHCU-CU-PERCENTAGE                          PPHCUUTL
027100         SET KHCU-UNKNOWN-UNIT TO TRUE                            PPHCUUTL
027200     END-IF.                                                      PPHCUUTL
027210     SKIP1                                                        PPHCUUTL
027220 SQL-OPEN-HCU      SECTION.                                       PPHCUUTL
027230     IF NOT HCU-ROW-OPEN                                          PPHCUUTL
027240         MOVE 'OPEN HCU CURSOR' TO DB2MSG-TAG                     PPHCUUTL
027250         EXEC SQL                                                 PPHCUUTL
027260             OPEN HCU_ROW                                         PPHCUUTL
027270         END-EXEC                                                 PPHCUUTL
027280         SET HCU-ROW-OPEN TO TRUE                                 PPHCUUTL
027290     END-IF.                                                      PPHCUUTL
027291 SQL-CLOSE-HCU      SECTION.                                      PPHCUUTL
027292     IF HCU-ROW-OPEN                                              PPHCUUTL
027293         MOVE 'CLOSE HCU CURSOR' TO DB2MSG-TAG                    PPHCUUTL
027294         EXEC SQL                                                 PPHCUUTL
027295             CLOSE HCU_ROW                                        PPHCUUTL
027296         END-EXEC                                                 PPHCUUTL
027297         SET NOT-HCU-ROW-OPEN TO TRUE                             PPHCUUTL
027298     END-IF.                                                      PPHCUUTL
027299 SQL-FETCH-HCU      SECTION.                                      PPHCUUTL
027300     MOVE 'FETCH HCU ROW' TO DB2MSG-TAG.                          PPHCUUTL
027301     EXEC SQL                                                     PPHCUUTL
027302         FETCH HCU_ROW INTO                                       PPHCUUTL
027303                     :HCU-ROW-DATA                                PPHCUUTL
027304     END-EXEC.                                                    PPHCUUTL
027305     MOVE SQLCODE TO WS-HCU-SQLCODE.                              PPHCUUTL
027306     IF SQLCODE = ZERO                                            PPHCUUTL
027307         PERFORM CONSTRUCT-XHCU                                   PPHCUUTL
027308         MOVE 1 TO HCA-INDEX                                      PPHCUUTL
027309         PERFORM SQL-OPEN-HCA                                     PPHCUUTL
027310         PERFORM SQL-FETCH-HCA                                    PPHCUUTL
027311             UNTIL SQLCODE NOT = ZERO                             PPHCUUTL
027312         PERFORM SQL-CLOSE-HCA                                    PPHCUUTL
027313         MOVE XHCU-HSSP-CONT-UNIT-RECORD TO                       PPHCUUTL
027314                                  HCU-ROW (HCU-INDEX)             PPHCUUTL
027315         SET HCU-INDEX UP BY 1                                    PPHCUUTL
027316     END-IF.                                                      PPHCUUTL
027317     SKIP1                                                        PPHCUUTL
027318 SQL-OPEN-HCA      SECTION.                                       PPHCUUTL
027319     IF NOT HCA-ROW-OPEN                                          PPHCUUTL
027320         MOVE 'OPEN HCA CURSOR' TO DB2MSG-TAG                     PPHCUUTL
027321         EXEC SQL                                                 PPHCUUTL
027322             OPEN HCA_ROW                                         PPHCUUTL
027323         END-EXEC                                                 PPHCUUTL
027324         SET HCA-ROW-OPEN TO TRUE                                 PPHCUUTL
027325     END-IF.                                                      PPHCUUTL
027326 SQL-CLOSE-HCA      SECTION.                                      PPHCUUTL
027327     IF HCA-ROW-OPEN                                              PPHCUUTL
027328         MOVE 'CLOSE HCA CURSOR' TO DB2MSG-TAG                    PPHCUUTL
027329         EXEC SQL                                                 PPHCUUTL
027330             CLOSE HCA_ROW                                        PPHCUUTL
027331         END-EXEC                                                 PPHCUUTL
027332         SET NOT-HCA-ROW-OPEN TO TRUE                             PPHCUUTL
027333     END-IF.                                                      PPHCUUTL
027334 SQL-FETCH-HCA      SECTION.                                      PPHCUUTL
027335     MOVE 'FETCH HCA ROW' TO DB2MSG-TAG.                          PPHCUUTL
027336     EXEC SQL                                                     PPHCUUTL
027337         FETCH HCA_ROW INTO                                       PPHCUUTL
027338                     :HCA-ROW-DATA                                PPHCUUTL
027339     END-EXEC.                                                    PPHCUUTL
027340     IF SQLCODE = ZERO                                            PPHCUUTL
027341         PERFORM CONSTRUCT-XHCA                                   PPHCUUTL
027342     END-IF.                                                      PPHCUUTL
027343                                                                  PPHCUUTL
027344 CONSTRUCT-XHCU SECTION.                                          PPHCUUTL
027345     INITIALIZE XHCU-HSSP-CONT-UNIT-RECORD.                       PPHCUUTL
027346     MOVE HCU-UNIT-ID     TO  XHCU-KEY-UNIT-ID.                   PPHCUUTL
027347     MOVE HCU-UNIT-NAME   TO  XHCU-UNIT-NAME.                     PPHCUUTL
027348     MOVE HCU-UNIT-PCT    TO  XHCU-UNIT-PERCENTAGE.               PPHCUUTL
027350                                                                  PPHCUUTL
027351 CONSTRUCT-XHCA SECTION.                                          PPHCUUTL
027352     MOVE HCA-LOW-ACCT-DEPT                                       PPHCUUTL
027353                  TO  XHCU-LOW-ACCT-DEPT (HCA-INDEX).             PPHCUUTL
027354     MOVE HCA-HI-ACCT-DEPT                                        PPHCUUTL
027355                  TO  XHCU-HIGH-ACCT-DEPT (HCA-INDEX).            PPHCUUTL
027356     ADD 1 TO HCA-INDEX.                                          PPHCUUTL
027357                                                                  PPHCUUTL
027360*999999-SQL-ERROR.                                               *PPHCUUTL
027400     COPY CPPDXP99.                                               PPHCUUTL
027500     SET KHCU-CTL-FILE-ERROR TO TRUE.                             PPHCUUTL
027600     MOVE DB2MSG-TAG                     TO KHCU-REF.             PPHCUUTL
028100     GOBACK.                                                      PPHCUUTL
