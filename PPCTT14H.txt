000100**************************************************************/   01341440
000200*  PROGRAM: PPCTT14H                                         */   01341440
000300*  RELEASE: ___1440______ SERVICE REQUEST(S): 80134, 80191_  */   01341440
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___11/08/02__  */   01341440
000500*  DESCRIPTION:                                              */   01341440
000600*  - Replacment PPPBRH Table Transaction Assembly Module     */   01341440
000700*  reflecting the structure changes necessary to implement   */   01341440
000800*  the four-tier rate structure.                             */   01341440
000900**************************************************************/   01341440
000800 IDENTIFICATION DIVISION.                                         PPCTT14H
000900 PROGRAM-ID. PPCTT14H.                                            PPCTT14H
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT14H
001100 DATE-COMPILED.                                                   PPCTT14H
001200 DATA DIVISION.                                                   PPCTT14H
001300 FILE SECTION.                                                    PPCTT14H
001400 WORKING-STORAGE SECTION.                                         PPCTT14H
001500                                                                  PPCTT14H
001600 01  WA-WORK-VARIABLES.                                           PPCTT14H
001700     05  WA-READ-ERROR.                                           PPCTT14H
001800         10 FILLER                     PIC X(37) VALUE            PPCTT14H
001900                 'PPCTLTRD ERROR ON TRANS CALL: RETURN='.         PPCTT14H
002000         10 WA-RTN-CODE                PIC XX.                    PPCTT14H
002100         10 FILLER                     PIC X(07) VALUE            PPCTT14H
002200                 ' COUNT='.                                       PPCTT14H
002300         10 WA-WORK-Z5                 PIC ZZ,ZZ9.                PPCTT14H
002400 01  WB-PROGRAM-CONSTANTS.                                        PPCTT14H
002500     05  WB-TABLE-NO                   PIC X(2) VALUE '14'.       PPCTT14H
002600 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT14H
002700     05  WC-ERR-IX                     PIC S9(04) VALUE ZERO.     PPCTT14H
002800     05  WC-SET-CNT                    PIC S9(04) VALUE ZERO.     PPCTT14H
003000     05  IX                            PIC S9(04) VALUE ZERO.     PPCTT14H
003100 01  WF-PROGRAM-FLAGS.                                            PPCTT14H
003200     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT14H
003300         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT14H
003400         88  WF-ACCEPT-SET                      VALUES 00 THRU 04.PPCTT14H
003500         88  WF-REJECT-SET                      VALUES 05 THRU 99.PPCTT14H
003600     05  WF-H-TRANS-FLAG               PIC X    VALUE 'N'.        PPCTT14H
003700         88  WF-H-TRANS-END                     VALUE 'Y'.        PPCTT14H
003800         88  WF-H-TRANS-NOT-END                 VALUE 'N'.        PPCTT14H
003900 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT14H
004000     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT14H
004100     05  MCT011                     PIC X(05) VALUE 'CT011'.      PPCTT14H
004200     05  MCT020                     PIC X(05) VALUE 'CT020'.      PPCTT14H
004300     05  MCT021                     PIC X(05) VALUE 'CT021'.      PPCTT14H
004400     05  MCT022                     PIC X(05) VALUE 'CT022'.      PPCTT14H
004500     05  MCT023                     PIC X(05) VALUE 'CT023'.      PPCTT14H
004600 01  SAVE-CONSTANTS.                                              PPCTT14H
004800     05  SAVE-DATA                  PIC X(33) VALUE               PPCTT14H
004900         'U  UA UC UACM  MM MMMMA MC MACMMC'.                     PPCTT14H
005000     05  SAVE-TABLE REDEFINES SAVE-DATA.                          PPCTT14H
005100         10 SAVE-COVERAGE           OCCURS 11 TIMES               PPCTT14H
005100                                    PIC XXX.                      PPCTT14H
005300 01  SAVE-RATES.                                                  PPCTT14H
005400     05  SAVE-RATES-X     OCCURS 11 TIMES.                        PPCTT14H
005500         10  SAVE-PREM-X.                                         PPCTT14H
005600             15  SAVE-PREMIUM       PIC 9999V99.                  PPCTT14H
005700         10  SAVE-CONT-X.                                         PPCTT14H
005800             15  SAVE-CONTRIBUTION  PIC 9999V99.                  PPCTT14H
005900         10  SAVE-COST              PIC 9999V99.                  PPCTT14H
006700 01  WS-SET-WORK-AREA.                                            PPCTT14H
006100     05  WS-VALID-NEXT-ENTRY        PIC S9(04) COMP SYNC          PPCTT14H
006200                                                  VALUE ZERO.     PPCTT14H
006900     05  WS-SET-END-FLAG            PIC X(01) VALUE 'N'.          PPCTT14H
007000         88  WS-SET-END-NOT-OK                VALUE 'N'.          PPCTT14H
007100         88  WS-SET-END-OK                    VALUE 'Y'.          PPCTT14H
007200     05  WS-SET-STATUS-FLAG         PIC X(01) VALUE 'N'.          PPCTT14H
007300         88  WS-SET-OK                        VALUE 'Y'.          PPCTT14H
007400         88  WS-SET-NOT-OK                    VALUE 'N'.          PPCTT14H
007500     05  WS-SET-KEY.                                              PPCTT14H
007600         10  WS-SET-ACTION          PIC X(01).                    PPCTT14H
007700             88  WS-ADD-SET                   VALUE 'A'.          PPCTT14H
007800             88  WS-CHANGE-SET                VALUE 'C'.          PPCTT14H
007900             88  WS-DELETE-SET                VALUE 'D'.          PPCTT14H
007400         10  WS-SET-BUC             PIC X(02).                    PPCTT14H
008100         10  WS-SET-REP             PIC X(01).                    PPCTT14H
008200         10  WS-SET-SHC             PIC X(01).                    PPCTT14H
008300         10  WS-SET-DUC             PIC X(01).                    PPCTT14H
008600         10  WS-SET-PLAN-CODE       PIC X(02).                    PPCTT14H
007900         10  WS-SET-EFFECTIVE-DATE  PIC X(06).                    PPCTT14H
008000         10  WS-SET-SALARY-BASE     PIC X(03).                    PPCTT14H
008100     05  WS-HOLD-KEY                PIC X(17) VALUE LOW-VALUES.   PPCTT14H
008800                                                                  PPCTT14H
008900 01  BRH-HEALTH-TABLE-TRANS.                                      PPCTT14H
009000     05  BRHT-ACTION-CODE           PIC X.                        PPCTT14H
009100         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT14H
009200         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT14H
009300         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT14H
009400         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT14H
009500     05  FILLER                     PIC X(05).                    PPCTT14H
009600     05  BRHT-PLAN-CODE             PIC XX.                       PPCTT14H
009700     05  FILLER                     PIC X.                        PPCTT14H
009200     05  BRHT-ENTRY-NUMBER-X        PIC XX.                       PPCTT14H
009300         88  BRHT-ENTRY-NUMBER-VALID           VALUES '01'        PPCTT14H
009400                                                      '02'        PPCTT14H
009500                                                      '03'        PPCTT14H
009600                                                      '04'        PPCTT14H
009700                                                      '05'        PPCTT14H
009800                                                      '06'        PPCTT14H
009900                                                      '07'        PPCTT14H
010000                                                      '08'        PPCTT14H
010100                                                      '09'        PPCTT14H
010200                                                      '10'        PPCTT14H
010300                                                      '11'.       PPCTT14H
010400     05  BRHT-ENTRY-NUMBER          REDEFINES                     PPCTT14H
010500         BRHT-ENTRY-NUMBER-X        PIC 99.                       PPCTT14H
010600     05  BRHT-SALARY-BASE           PIC X(03).                    PPCTT14H
010700     05  BRHT-PREMIUM               PIC X(06).                    PPCTT14H
010800     05  BRHT-CONTRIBUTION          PIC X(06).                    PPCTT14H
010900     05  FILLER                     PIC X(34).                    PPCTT14H
011000     05  BRHT-BUC                   PIC XX.                       PPCTT14H
010500     05  BRHT-REP                   PIC X.                        PPCTT14H
010600     05  BRHT-SHC                   PIC X.                        PPCTT14H
010700     05  BRHT-DUC                   PIC X.                        PPCTT14H
010800     05  BRHT-EFFECTIVE-DATE        PIC X(06).                    PPCTT14H
010900     05  FILLER                     PIC X(09).                    PPCTT14H
011000                                                                  PPCTT14H
011100 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT14H
011200 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT14H
011300 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT14H
011400 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT14H
011500 01  BRH-HEALTH-TABLE-INPUT.           COPY CPCTBRHI.             PPCTT14H
011600 01  BRH-HEALTH-TABLE-DATA.                                       PPCTT14H
011700     EXEC SQL                                                     PPCTT14H
011800         INCLUDE PPPVZBRH                                         PPCTT14H
011900     END-EXEC.                                                    PPCTT14H
012000                                                                  PPCTT14H
012100 LINKAGE SECTION.                                                 PPCTT14H
012200                                                                  PPCTT14H
012300 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT14H
012400 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT14H
012500                                                                  PPCTT14H
012600 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE            PPCTT14H
012700                          CSRI-CTL-SORTED-READ-INTERFACE.         PPCTT14H
012800                                                                  PPCTT14H
012900 0000-MAIN SECTION.                                               PPCTT14H
013000                                                                  PPCTT14H
013100     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT14H
013200     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT14H
013300       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT14H
013400          OR WF-H-TRANS-END.                                      PPCTT14H
013500     PERFORM 3000-FINISH-PREVIOUS-SET.                            PPCTT14H
013600                                                                  PPCTT14H
013700 0999-END.                                                        PPCTT14H
013800                                                                  PPCTT14H
013900     GOBACK.                                                      PPCTT14H
014000                                                                  PPCTT14H
014100 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT14H
014200                                                                  PPCTT14H
014300     MOVE 'PPCTT14H'            TO XWHC-DISPLAY-MODULE.           PPCTT14H
014400     COPY CPPDXWHC.                                               PPCTT14H
014500     SET CTTI-NORMAL            TO TRUE.                          PPCTT14H
014600     SET WF-H-TRANS-NOT-END     TO TRUE.                          PPCTT14H
014700     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTT14H
014800     MOVE LOW-VALUES            TO WS-HOLD-KEY.                   PPCTT14H
014900     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTT14H
015000                                   CTRI-TRANSACTION               PPCTT14H
015100                                   CTRI-TRANSACTION-DISPO         PPCTT14H
015200                                   CTRI-TEXT-LINE                 PPCTT14H
015300                                   CTRI-MESSAGE-NUMBER.           PPCTT14H
015400     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTT14H
015500                                   CTTI-ERROR-SEVERITY.           PPCTT14H
015600     MOVE WB-TABLE-NO           TO CSRI-CALL-TYPE-NUM.            PPCTT14H
015700     MOVE CSRI-TRANSACTION      TO BRH-HEALTH-TABLE-TRANS.        PPCTT14H
015800     MOVE CSRI-TRANSACTION(1:1) TO WS-SET-ACTION.                 PPCTT14H
016500     MOVE BRHT-BUC              TO WS-SET-BUC.                    PPCTT14H
016000     MOVE BRHT-REP              TO WS-SET-REP.                    PPCTT14H
016100     MOVE BRHT-SHC              TO WS-SET-SHC.                    PPCTT14H
016200     MOVE BRHT-DUC              TO WS-SET-DUC.                    PPCTT14H
016900     MOVE BRHT-PLAN-CODE        TO WS-SET-PLAN-CODE.              PPCTT14H
016300     MOVE BRHT-EFFECTIVE-DATE   TO WS-SET-EFFECTIVE-DATE.         PPCTT14H
017100     MOVE BRHT-SALARY-BASE      TO WS-SET-SALARY-BASE.            PPCTT14H
016500                                                                  PPCTT14H
016600 1999-INITIALIZATION-EXIT.                                        PPCTT14H
016700     EXIT.                                                        PPCTT14H
016800                                                                  PPCTT14H
016900 2000-PROCESS-TRANSACTIONS SECTION.                               PPCTT14H
017000                                                                  PPCTT14H
017100     IF WS-SET-KEY NOT = WS-HOLD-KEY                              PPCTT14H
017900         PERFORM 3000-FINISH-PREVIOUS-SET                         PPCTT14H
018000         PERFORM 2100-BEGIN-NEW-SET                               PPCTT14H
017400     END-IF.                                                      PPCTT14H
017500     MOVE BRH-HEALTH-TABLE-TRANS TO CTRI-TRANSACTION.             PPCTT14H
017600     MOVE BRHT-ACTION-CODE TO KCTE-ACTION.                        PPCTT14H
017700     ADD 1 TO WC-SET-CNT.                                         PPCTT14H
018500     IF BRHT-ENTRY-NUMBER-VALID                                   PPCTT14H
018600         IF BRHT-ENTRY-NUMBER = WS-VALID-NEXT-ENTRY               PPCTT14H
018700             PERFORM 2200-MOVE-TRANSACTION-DATA                   PPCTT14H
018800         ELSE                                                     PPCTT14H
018900             MOVE MCT021 TO CTRI-MESSAGE-NUMBER                   PPCTT14H
019000             PERFORM 9100-CALL-PPCTLRPT                           PPCTT14H
019100             SET WS-SET-NOT-OK TO TRUE                            PPCTT14H
019200         END-IF                                                   PPCTT14H
019300         IF WS-ADD-SET                                            PPCTT14H
019400             IF BRHT-ENTRY-NUMBER < 11                            PPCTT14H
019500                 COMPUTE WS-VALID-NEXT-ENTRY =                    PPCTT14H
019600                         BRHT-ENTRY-NUMBER + 1                    PPCTT14H
019700             ELSE                                                 PPCTT14H
019800                 MOVE ZERO TO WS-VALID-NEXT-ENTRY                 PPCTT14H
019900                 SET WS-SET-END-OK TO TRUE                        PPCTT14H
020000             END-IF                                               PPCTT14H
020100         ELSE                                                     PPCTT14H
020200             MOVE ZERO TO WS-VALID-NEXT-ENTRY                     PPCTT14H
020300             SET WS-SET-END-OK TO TRUE                            PPCTT14H
020400         END-IF                                                   PPCTT14H
020500         IF CTRI-TRANSACTION NOT = SPACES                         PPCTT14H
020600             PERFORM 9100-CALL-PPCTLRPT                           PPCTT14H
020700         END-IF                                                   PPCTT14H
020500     ELSE                                                         PPCTT14H
020900         MOVE MCT020 TO CTRI-MESSAGE-NUMBER                       PPCTT14H
021000         PERFORM 9100-CALL-PPCTLRPT                               PPCTT14H
021100         SET WS-SET-NOT-OK TO TRUE                                PPCTT14H
020900     END-IF.                                                      PPCTT14H
021000                                                                  PPCTT14H
021100     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT14H
021200                                                                  PPCTT14H
021300 2099-EXIT.                                                       PPCTT14H
021400     EXIT.                                                        PPCTT14H
021500                                                                  PPCTT14H
021600 2100-BEGIN-NEW-SET SECTION.                                      PPCTT14H
021700                                                                  PPCTT14H
021800     MOVE WS-SET-KEY TO WS-HOLD-KEY.                              PPCTT14H
021900     SET WS-SET-OK TO TRUE.                                       PPCTT14H
022000     SET WS-SET-END-NOT-OK TO TRUE.                               PPCTT14H
022100     MOVE ZERO TO WC-SET-CNT.                                     PPCTT14H
022200     MOVE SPACES TO BRH-HEALTH-TABLE-INPUT.                       PPCTT14H
022600     MOVE BRHT-BUC        TO BRHI-BUC.                            PPCTT14H
022400     MOVE BRHT-REP        TO BRHI-REP.                            PPCTT14H
022500     MOVE BRHT-SHC        TO BRHI-SHC.                            PPCTT14H
022600     MOVE BRHT-DUC        TO BRHI-DUC.                            PPCTT14H
023000     MOVE BRHT-PLAN-CODE      TO BRHI-PLAN-CODE                   PPCTT14H
022700     MOVE BRHT-EFFECTIVE-DATE TO BRHI-EFFECTIVE-DATE              PPCTT14H
023200     MOVE BRHT-SALARY-BASE    TO BRHI-SALARY-BASE-X.              PPCTT14H
023000     EVALUATE TRUE                                                PPCTT14H
023400       WHEN WS-ADD-SET                                            PPCTT14H
023500          MOVE 1 TO WS-VALID-NEXT-ENTRY                           PPCTT14H
023600       WHEN WS-DELETE-SET                                         PPCTT14H
023700          MOVE 1 TO WS-VALID-NEXT-ENTRY                           PPCTT14H
023500     END-EVALUATE.                                                PPCTT14H
023600                                                                  PPCTT14H
023700 2199-EXIT.                                                       PPCTT14H
023800     EXIT.                                                        PPCTT14H
023900                                                                  PPCTT14H
024000 2200-MOVE-TRANSACTION-DATA SECTION.                              PPCTT14H
024100                                                                  PPCTT14H
024500     MOVE WS-VALID-NEXT-ENTRY TO IX.                              PPCTT14H
024600     MOVE BRHT-PREMIUM        TO SAVE-PREM-X (IX).                PPCTT14H
024700     MOVE BRHT-CONTRIBUTION   TO SAVE-CONT-X (IX).                PPCTT14H
026800                                                                  PPCTT14H
026900 2299-EXIT.                                                       PPCTT14H
027000     EXIT.                                                        PPCTT14H
027100                                                                  PPCTT14H
027200 3000-FINISH-PREVIOUS-SET SECTION.                                PPCTT14H
027300                                                                  PPCTT14H
027400     IF WS-HOLD-KEY = LOW-VALUES                                  PPCTT14H
025500         GO TO 3099-EXIT                                          PPCTT14H
027600     END-IF.                                                      PPCTT14H
027700     IF KCTE-ACTION = 'C'                                         PPCTT14H
025800         MOVE MCT011 TO CTRI-MESSAGE-NUMBER                       PPCTT14H
025900         PERFORM 9100-CALL-PPCTLRPT                               PPCTT14H
026000         SET WS-SET-NOT-OK TO TRUE                                PPCTT14H
028100     END-IF.                                                      PPCTT14H
028200     IF WS-SET-END-NOT-OK                                         PPCTT14H
026300         MOVE MCT023 TO CTRI-MESSAGE-NUMBER                       PPCTT14H
026400         PERFORM 9100-CALL-PPCTLRPT                               PPCTT14H
026500         SET WS-SET-NOT-OK TO TRUE                                PPCTT14H
028600     END-IF.                                                      PPCTT14H
028700     IF WS-SET-NOT-OK                                             PPCTT14H
026800         MOVE 'SET REJECTED' TO CTRI-TEXT-DISPO                   PPCTT14H
026900         ADD WC-SET-CNT TO CTTI-TRANS-REJECTED                    PPCTT14H
027000         PERFORM 9100-CALL-PPCTLRPT                               PPCTT14H
027100         MOVE 2 TO CTRI-SPACING-OVERRIDE                          PPCTT14H
027200         GO TO 3099-EXIT                                          PPCTT14H
029300     END-IF.                                                      PPCTT14H
029400     SET WF-NO-ERRORS TO TRUE.                                    PPCTT14H
029500     MOVE CTTI-ACTION-DATE TO KCTE-LAST-ACTION-DT.                PPCTT14H
029600     IF KCTE-ACTION = 'D'                                         PPCTT14H
027700         CALL 'PPCTBRHE' USING CONTROL-TABLE-EDIT-INTERFACE       PPCTT14H
027800                               BRH-HEALTH-TABLE-INPUT             PPCTT14H
027900                               BRH-HEALTH-TABLE-DATA              PPCTT14H
028000         IF KCTE-ERR > ZERO                                       PPCTT14H
028100             PERFORM 3100-PRINT-ERROR                             PPCTT14H
028200               VARYING WC-ERR-IX FROM 1 BY 1                      PPCTT14H
028300                 UNTIL WC-ERR-IX > KCTE-ERR                       PPCTT14H
028400             IF WF-REJECT-SET OR KCTE-STATUS-NOT-COMPLETED        PPCTT14H
028500                 MOVE MCT022 TO CTRI-MESSAGE-NUMBER               PPCTT14H
028600                 SET WS-SET-NOT-OK TO TRUE                        PPCTT14H
028700             END-IF                                               PPCTT14H
028800         END-IF                                                   PPCTT14H
028900         IF WS-SET-OK                                             PPCTT14H
029000             MOVE 'SET ACCEPTED' TO CTRI-TEXT-DISPO               PPCTT14H
029100             ADD WC-SET-CNT TO CTTI-TRANS-ACCEPTED                PPCTT14H
029200             PERFORM 3200-UPDATE-DB2-TABLE                        PPCTT14H
029300         ELSE                                                     PPCTT14H
029400             MOVE 'SET REJECTED' TO CTRI-TEXT-DISPO               PPCTT14H
029500             ADD WC-SET-CNT TO CTTI-TRANS-REJECTED                PPCTT14H
029600         END-IF                                                   PPCTT14H
029700         PERFORM 9100-CALL-PPCTLRPT                               PPCTT14H
029800         MOVE 2 TO CTRI-SPACING-OVERRIDE                          PPCTT14H
029900         GO TO 3099-EXIT                                          PPCTT14H
032000     END-IF.                                                      PPCTT14H
032100     PERFORM VARYING IX FROM 1 BY 1                               PPCTT14H
030200       UNTIL IX > 11                                              PPCTT14H
030300         MOVE SAVE-COVERAGE (IX)     TO BRHI-COVERAGE-CODE        PPCTT14H
030400         MOVE SAVE-PREM-X (IX)       TO BRHI-PREMIUM-X            PPCTT14H
030500         MOVE SAVE-CONT-X (IX)       TO BRHI-CONTRIBUTION-X       PPCTT14H
030600         CALL 'PPCTBRHE' USING CONTROL-TABLE-EDIT-INTERFACE       PPCTT14H
030700                               BRH-HEALTH-TABLE-INPUT             PPCTT14H
030800                               BRH-HEALTH-TABLE-DATA              PPCTT14H
030900         MOVE BRHI-PREMIUM-X         TO SAVE-PREM-X (IX)          PPCTT14H
031000         MOVE BRHI-CONTRIBUTION-X    TO SAVE-CONT-X (IX)          PPCTT14H
031100         MOVE BRH-COST               TO SAVE-COST (IX)            PPCTT14H
031200         IF KCTE-ERR > ZERO                                       PPCTT14H
031300             PERFORM 3100-PRINT-ERROR                             PPCTT14H
031400               VARYING WC-ERR-IX FROM 1 BY 1                      PPCTT14H
031500                 UNTIL WC-ERR-IX > KCTE-ERR                       PPCTT14H
031600             IF WF-REJECT-SET OR KCTE-STATUS-NOT-COMPLETED        PPCTT14H
031700                MOVE MCT022 TO CTRI-MESSAGE-NUMBER                PPCTT14H
031800                SET WS-SET-NOT-OK TO TRUE                         PPCTT14H
031900             END-IF                                               PPCTT14H
032000         END-IF                                                   PPCTT14H
034600     END-PERFORM.                                                 PPCTT14H
034700     IF WS-SET-OK                                                 PPCTT14H
032300         MOVE 'SET ACCEPTED' TO CTRI-TEXT-DISPO                   PPCTT14H
032400         ADD WC-SET-CNT   TO CTTI-TRANS-ACCEPTED                  PPCTT14H
032500         PERFORM VARYING IX FROM 1 BY 1                           PPCTT14H
032600           UNTIL IX > 11                                          PPCTT14H
032700             MOVE SAVE-COVERAGE (IX)     TO BRH-COVERAGE-CODE     PPCTT14H
032800             MOVE SAVE-PREMIUM (IX)      TO BRH-PREMIUM           PPCTT14H
032900             MOVE SAVE-CONTRIBUTION (IX) TO BRH-CONTRIBUTION      PPCTT14H
033000             MOVE SAVE-COST (IX)         TO BRH-COST              PPCTT14H
033100             PERFORM 3200-UPDATE-DB2-TABLE                        PPCTT14H
033200         END-PERFORM                                              PPCTT14H
035900     ELSE                                                         PPCTT14H
033400         MOVE 'SET REJECTED' TO CTRI-TEXT-DISPO                   PPCTT14H
033500         ADD WC-SET-CNT TO CTTI-TRANS-REJECTED                    PPCTT14H
036200     END-IF.                                                      PPCTT14H
036300     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT14H
036400     MOVE 2 TO CTRI-SPACING-OVERRIDE.                             PPCTT14H
036500                                                                  PPCTT14H
036600 3099-EXIT.                                                       PPCTT14H
036700     EXIT.                                                        PPCTT14H
036800                                                                  PPCTT14H
036900 3100-PRINT-ERROR SECTION.                                        PPCTT14H
037000                                                                  PPCTT14H
037100     MOVE KCTE-ERR-NO (WC-ERR-IX) TO CTRI-MESSAGE-NUMBER.         PPCTT14H
037200     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT14H
037300     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT14H
037400         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT14H
037500     END-IF.                                                      PPCTT14H
037600                                                                  PPCTT14H
037700 3199-EXIT.                                                       PPCTT14H
037800     EXIT.                                                        PPCTT14H
037900                                                                  PPCTT14H
038000 3200-UPDATE-DB2-TABLE SECTION.                                   PPCTT14H
038100                                                                  PPCTT14H
038200     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT14H
038300     CALL 'PPCTBRHU' USING CONTROL-TABLE-UPDT-INTERFACE,          PPCTT14H
038400                           BRH-HEALTH-TABLE-DATA.                 PPCTT14H
038500     IF KCTU-STATUS-OK                                            PPCTT14H
036000         EVALUATE TRUE                                            PPCTT14H
036100           WHEN KCTU-ACTION-ADD                                   PPCTT14H
036200             ADD 1 TO CTTI-ENTRIES-ADDED                          PPCTT14H
036300           WHEN KCTU-ACTION-CHANGE                                PPCTT14H
036400             ADD 1 TO CTTI-ENTRIES-UPDATED                        PPCTT14H
036500           WHEN KCTU-ACTION-DELETE                                PPCTT14H
036600             ADD 1 TO CTTI-ENTRIES-DELETED                        PPCTT14H
036700           WHEN OTHER                                             PPCTT14H
036800             GO TO 3300-BAD-UPDATE-CALL                           PPCTT14H
036900         END-EVALUATE                                             PPCTT14H
039600     ELSE                                                         PPCTT14H
037100         GO TO 3300-BAD-UPDATE-CALL                               PPCTT14H
039800     END-IF.                                                      PPCTT14H
039900                                                                  PPCTT14H
040000 3299-EXIT.                                                       PPCTT14H
040100     EXIT.                                                        PPCTT14H
040200                                                                  PPCTT14H
040300 3300-BAD-UPDATE-CALL SECTION.                                    PPCTT14H
040400                                                                  PPCTT14H
040500     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT14H
040600     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT14H
040700     STRING 'PPCTBRHU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT14H
040800            '" STATUS WAS "'               KCTU-STATUS            PPCTT14H
040900            ' COUNT='                      WA-WORK-Z5             PPCTT14H
041000            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT14H
041100     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT14H
041200     GO TO 9999-ABEND.                                            PPCTT14H
041300                                                                  PPCTT14H
041400 3399-EXIT.                                                       PPCTT14H
041500     EXIT.                                                        PPCTT14H
041600                                                                  PPCTT14H
041700 9100-CALL-PPCTLRPT SECTION.                                      PPCTT14H
041800                                                                  PPCTT14H
041900     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT14H
042000     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT14H
042100         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT14H
042200     END-IF.                                                      PPCTT14H
042300     IF CTRI-RUN-ABORTED                                          PPCTT14H
039800         GO TO 9999-ABEND                                         PPCTT14H
042500     END-IF.                                                      PPCTT14H
042600                                                                  PPCTT14H
042700 9199-EXIT.                                                       PPCTT14H
042800     EXIT.                                                        PPCTT14H
042900                                                                  PPCTT14H
043000 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT14H
043100                                                                  PPCTT14H
043200     PERFORM WITH TEST AFTER                                      PPCTT14H
043300       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT14H
043400          OR CSRI-TRANSACTION(1:1) NOT = '*'                      PPCTT14H
040900        CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE      PPCTT14H
041000        IF CSRI-NORMAL                                            PPCTT14H
041100           ADD 1 TO CTTI-TRANS-PROCESSED                          PPCTT14H
041200           IF CSRI-TRANSACTION(1:1) = '*'                         PPCTT14H
041300                MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION         PPCTT14H
041400                MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO PPCTT14H
041500                PERFORM 9100-CALL-PPCTLRPT                        PPCTT14H
041600                ADD 1 TO CTTI-TRANS-ACCEPTED                      PPCTT14H
041700           ELSE                                                   PPCTT14H
041800               IF CSRI-TRANSACTION(4:2) = 'HI'                    PPCTT14H
041900                   MOVE CSRI-TRANSACTION      TO                  PPCTT14H
042000                                           BRH-HEALTH-TABLE-TRANS PPCTT14H
042100                   MOVE CSRI-TRANSACTION(1:1) TO WS-SET-ACTION    PPCTT14H
042200                   MOVE BRHT-BUC              TO WS-SET-BUC       PPCTT14H
042300                   MOVE BRHT-REP              TO WS-SET-REP       PPCTT14H
042400                   MOVE BRHT-SHC              TO WS-SET-SHC       PPCTT14H
042500                   MOVE BRHT-DUC              TO WS-SET-DUC       PPCTT14H
042600                   MOVE BRHT-PLAN-CODE        TO WS-SET-PLAN-CODE PPCTT14H
042700                   MOVE BRHT-EFFECTIVE-DATE   TO                  PPCTT14H
042800                                           WS-SET-EFFECTIVE-DATE  PPCTT14H
042900                   MOVE BRHT-SALARY-BASE      TO                  PPCTT14H
043000                                           WS-SET-SALARY-BASE     PPCTT14H
043100               ELSE                                               PPCTT14H
043200                   SET WF-H-TRANS-END         TO TRUE             PPCTT14H
043300               END-IF                                             PPCTT14H
043400           END-IF                                                 PPCTT14H
043500        ELSE                                                      PPCTT14H
043600          IF NOT CSRI-TABLE-TRANS-END                             PPCTT14H
043700              MOVE MCT001              TO CTRI-MESSAGE-NUMBER     PPCTT14H
043800              MOVE CSRI-RETURN-CODE    TO WA-RTN-CODE             PPCTT14H
043900              MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5             PPCTT14H
044000              MOVE WA-READ-ERROR       TO CTRI-TEXT-LINE          PPCTT14H
044100              PERFORM 9100-CALL-PPCTLRPT                          PPCTT14H
044200              GO TO 9999-ABEND                                    PPCTT14H
045500          END-IF                                                  PPCTT14H
046500        END-IF                                                    PPCTT14H
046700     END-PERFORM.                                                 PPCTT14H
046800                                                                  PPCTT14H
046900 9399-EXIT.                                                       PPCTT14H
047000     EXIT.                                                        PPCTT14H
047100                                                                  PPCTT14H
047200 9999-ABEND SECTION.                                              PPCTT14H
047300                                                                  PPCTT14H
047400     SET CTTI-ABORT TO TRUE.                                      PPCTT14H
047500     GOBACK.                                                      PPCTT14H
047600*************************END OF SOURCE CODE***********************PPCTT14H
