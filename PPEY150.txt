000000**************************************************************/   54541281
000001*  PROGRAM: PPEY150                                          */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ CREATION DATE:      ___08/11/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  - PURGE  FUTURE/CURRENT BENEFITS (FCB) RECORDS.           */   54541281
000006*    AFTER TWO YEARS (DETERMINED FROM THE VALUE IN SYSTEM    */   54541281
000007*    PARAMETER 99), THE OLD FCB RECORDS ARE PURGED.          */   54541281
000009**************************************************************/   54541281
001100                                                                  PPEY150
001200 IDENTIFICATION DIVISION.                                         PPEY150
001300 PROGRAM-ID. PPEY150.                                             PPEY150
001400 AUTHOR. UCOP.                                                    PPEY150
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEY150
001600                                                                  PPEY150
001700 DATE-WRITTEN.  AUGUST, 1999.                                     PPEY150
001800 DATE-COMPILED. XX/XX/XX.                                         PPEY150
001900     EJECT                                                        PPEY150
002000 ENVIRONMENT DIVISION.                                            PPEY150
002100 CONFIGURATION SECTION.                                           PPEY150
002200 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEY150
002300 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEY150
002400 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEY150
002500 INPUT-OUTPUT SECTION.                                            PPEY150
002600 FILE-CONTROL.                                                    PPEY150
003100 DATA DIVISION.                                                   PPEY150
003200 FILE SECTION.                                                    PPEY150
003300                                                                  PPEY150
003600                                                                  PPEY150
003700     EJECT                                                        PPEY150
003800*----------------------------------------------------------------*PPEY150
003900 WORKING-STORAGE SECTION.                                         PPEY150
004000*----------------------------------------------------------------*PPEY150
004100 01  A-STND-PROG-ID                  PIC X(15) VALUE              PPEY150
004210     'PPEY150/081199'.                                            PPEY150
004220 01  A-STND-MSG-PARMS                 REDEFINES                   PPEY150
004230     A-STND-PROG-ID.                                              PPEY150
004240     05  FILLER                       PIC X(03).                  PPEY150
004250     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEY150
004260     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEY150
004270     05  FILLER                       PIC X(08).                  PPEY150
004300                                                                  PPEY150
004400 01  SWITCHES-AND-COUNTERS.                                       PPEY150
004500                                                                  PPEY150
004600     05  FIRST-TIME-SW               PIC X(01) VALUE LOW-VALUES.  PPEY150
004700         88  THIS-IS-THE-FIRST-TIME            VALUE LOW-VALUES.  PPEY150
004800         88  NOT-THE-FIRST-TIME                VALUE 'Y'.         PPEY150
004801     05  YEARS-RETENTION-SW          PIC X(01) VALUE 'Y'.         PPEY150
004802         88 YEARS-RETENTION-VALUE-INVALID      VALUE 'N'.         PPEY150
004803         88 YEARS-RETENTION-VALUE-VALID        VALUE 'Y'.         PPEY150
004804                                                                  PPEY150
004810     05  WS-COUNTERS.                                             PPEY150
004820         07  WS-RETAIN-REC-CT        PIC 9(7) COMP-3 VALUE ZERO.  PPEY150
004860         07  WS-DELETE-FCB-ENTRIES   PIC S9(04) COMP VALUE ZEROS. PPEY150
005000                                                                  PPEY150
005100 01  SYSTEM-PARAMETER-NUMBERS-USED.                               PPEY150
005200     05  FCB-RETENTION-PARM-NO        PIC S9(03) COMP-3 VALUE +99.PPEY150
005210                                                                  PPEY150
005220 01  SYSTEM-PARAMETER-VALUES-USED.                                PPEY150
005230     05  FCB-NUMBER-YEARS-RETENTION   PIC S9(5)V9999 COMP-3       PPEY150
005240                                                       VALUE ZERO.PPEY150
005241 01  FCB-RELATED-FIELDS.                                          PPEY150
005242     05  ENTRY-X               PIC S9(04) COMP VALUE ZEROS.       PPEY150
005250     05  NUM-EFCB-NON-DELETES  PIC S9(04) COMP VALUE ZEROS.       PPEY150
005251     05  NUM-EFCB-ENTRIES      PIC S9(04) COMP VALUE ZEROS.       PPEY150
005252     05  INIT-EFCB-KEY.                                           PPEY150
005254         10  INIT-EFCB-BEN-TYPE    PIC  X(01) VALUE SPACE.        PPEY150
005255         10  INIT-EFCB-COVEFF-DATE PIC  X(10) VALUE '0001-01-01'. PPEY150
005256     05  FCB-OCCURRENCE-KEY.                                      PPEY150
005257         10  FCB-OCUR-FCB-BENEFIT-TYPE                            PPEY150
005258                               PIC X(01)  VALUE SPACES.           PPEY150
005259         10  FCB-OCUR-FCB-COVEFF-DATE                             PPEY150
005260                               PIC X(10)  VALUE SPACES.           PPEY150
005261         10  FILLER            PIC X(07)  VALUE SPACES.           PPEY150
005262                                                                  PPEY150
005270                                                                  PPEY150
005300     EJECT                                                        PPEY150
005400 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEY150
005410                                                                  PPEY150
005411     EJECT                                                        PPEY150
005412 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEY150
005413                                                                  PPEY150
005414     EJECT                                                        PPEY150
005415 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEY150
005416                                                                  PPEY150
005417     EJECT                                                        PPEY150
005418 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEY150
005419                                                                  PPEY150
005420 01  WS-RETENTION-DATE.                                           PPEY150
005421     05  WS-RETENTION-YYYY   PIC 9(04).                           PPEY150
005422     05  FILLER              PIC X(01) VALUE '-'.                 PPEY150
005423     05  WS-RETENTION-MM     PIC 9(02).                           PPEY150
005424     05  FILLER              PIC X(01) VALUE '-'.                 PPEY150
005425     05  WS-RETENTION-DD     PIC 9(02).                           PPEY150
005426                                                                  PPEY150
005427     EJECT                                                        PPEY150
005430 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. PPEY150
006001                                                                  PPEY150
006002     EJECT                                                        PPEY150
006003 01  MESSAGES-USED.                                               PPEY150
006004     05  M13801                        PIC X(05) VALUE '13801'.   PPEY150
006005                                                                  PPEY150
006010 01  ELEMENTS-USED.                                               PPEY150
006020     05  E0693                         PIC 9(04) VALUE  0693.     PPEY150
006030     05  E0694                         PIC 9(04) VALUE  0694.     PPEY150
006031     05  E0695                         PIC 9(04) VALUE  0695.     PPEY150
006032     05  E0696                         PIC 9(04) VALUE  0696.     PPEY150
006033     05  E0697                         PIC 9(04) VALUE  0697.     PPEY150
006034     05  E0698                         PIC 9(04) VALUE  0698.     PPEY150
006035     05  E0699                         PIC 9(04) VALUE  0699.     PPEY150
006036                                                                  PPEY150
006040     EJECT                                                        PPEY150
006100******************************************************************PPEY150
006200**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEY150
006300**--------------------------------------------------------------**PPEY150
006401 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEY150
006402                                                                  PPEY150
006403 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEY150
006404                                                                  PPEY150
006405 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEY150
006406                                                                  PPEY150
006407 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEY150
006408                                                                  PPEY150
006409 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEY150
006410                                                                  PPEY150
006411     EJECT                                                        PPEY150
006412 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEY150
006413                                                                  PPEY150
006414     EJECT                                                        PPEY150
006415 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEY150
006416                                                                  PPEY150
006417     EJECT                                                        PPEY150
006418 01  PPFCCRPT-INTERFACE                  EXTERNAL. COPY CPLNKFCC. PPEY150
006419                                                                  PPEY150
006420     EJECT                                                        PPEY150
006500 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEY150
006510                                                                  PPEY150
006600     EJECT                                                        PPEY150
006700 01  SYSTEM-PARAMETERS                   EXTERNAL. COPY CPWSXSP2. PPEY150
006710                                                                  PPEY150
006800     EJECT                                                        PPEY150
006900 01  PPPFCB-ARRAY                        EXTERNAL. COPY CPWSEFCB. PPEY150
007000                                                                  PPEY150
007010     EJECT                                                        PPEY150
007100 01  BEN-ROW                             EXTERNAL.  COPY CPWSRBEN.PPEY150
016300                                                                  PPEY150
016400     EJECT                                                        PPEY150
016500******************************************************************PPEY150
016600**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEY150
016700******************************************************************PPEY150
016800                                                                  PPEY150
016900*----------------------------------------------------------------*PPEY150
017000 LINKAGE SECTION.                                                 PPEY150
017100*----------------------------------------------------------------*PPEY150
017200                                                                  PPEY150
017220******************************************************************PPEY150
017230**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEY150
017240**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEY150
017250******************************************************************PPEY150
017260                                                                  PPEY150
017270 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEY150
017280                                                                  PPEY150
017290     EJECT                                                        PPEY150
017291******************************************************************PPEY150
017292**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEY150
017293**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEY150
017294******************************************************************PPEY150
017295                                                                  PPEY150
017296 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEY150
017297                                                                  PPEY150
017298     EJECT                                                        PPEY150
017299******************************************************************PPEY150
017300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEY150
017301******************************************************************PPEY150
017302                                                                  PPEY150
017303 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEY150
017304                          KMTA-MESSAGE-TABLE-ARRAY.               PPEY150
017305                                                                  PPEY150
018500                                                                  PPEY150
018700*----------------------------------------------------------------*PPEY150
018800 0000-DRIVER.                                                     PPEY150
018900*----------------------------------------------------------------*PPEY150
019000******************************************************************PPEY150
019100**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEY150
019200******************************************************************PPEY150
019300                                                                  PPEY150
019400     IF  THIS-IS-THE-FIRST-TIME                                   PPEY150
019500         PERFORM 0100-INITIALIZE                                  PPEY150
019600     END-IF.                                                      PPEY150
019700                                                                  PPEY150
019800******************************************************************PPEY150
019900**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEY150
020000**  TO THE CALLING PROGRAM                                      **PPEY150
020100******************************************************************PPEY150
020200                                                                  PPEY150
020210     IF  YEARS-RETENTION-VALUE-VALID                              PPEY150
020300         PERFORM 1000-MAINLINE-ROUTINE                            PPEY150
020310     END-IF.                                                      PPEY150
020400                                                                  PPEY150
020500                                                                  PPEY150
020600     EXIT PROGRAM.                                                PPEY150
020700                                                                  PPEY150
020800                                                                  PPEY150
020900     EJECT                                                        PPEY150
021000*----------------------------------------------------------------*PPEY150
021100 0100-INITIALIZE    SECTION.                                      PPEY150
021200*----------------------------------------------------------------*PPEY150
021300                                                                  PPEY150
021400******************************************************************PPEY150
021500**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEY150
021600******************************************************************PPEY150
021700                                                                  PPEY150
021800     MOVE A-STND-PROG-ID          TO XWHC-DISPLAY-MODULE.         PPEY150
021900                                                                  PPEY150
022000*                                               *-------------*   PPEY150
022100                                                 COPY CPPDXWHC.   PPEY150
022200*                                               *-------------*   PPEY150
022300                                                                  PPEY150
022400     SET NOT-THE-FIRST-TIME       TO TRUE.                        PPEY150
022500                                                                  PPEY150
022501******************************************************************PPEY150
022502**   OBTAIN NUMBER OF YEARS BACK TO DELETE FCB RECORDS          **PPEY150
022503******************************************************************PPEY150
022504                                                                  PPEY150
022506     MOVE XSP-PARAM (FCB-RETENTION-PARM-NO)                       PPEY150
022507                             TO FCB-NUMBER-YEARS-RETENTION.       PPEY150
022510                                                                  PPEY150
022511     INITIALIZE PPFCCRPT-INTERFACE.                               PPEY150
022512                                                                  PPEY150
022513     IF FCB-NUMBER-YEARS-RETENTION NOT > ZERO                     PPEY150
022514        ADD 1 TO KMTA-CTR                                         PPEY150
022515        MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE     (KMTA-CTR)  PPEY150
022516        MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM      (KMTA-CTR)  PPEY150
022520        MOVE M13801          TO KMTA-MSG-NUMBER       (KMTA-CTR)  PPEY150
022521        SET YEARS-RETENTION-VALUE-INVALID TO TRUE                 PPEY150
022522     ELSE                                                         PPEY150
022523        PERFORM 0110-DERIVE-RETENTION-DATE                        PPEY150
022524     END-IF.                                                      PPEY150
022600                                                                  PPEY150
022700     EJECT                                                        PPEY150
022800*----------------------------------------------------------------*PPEY150
022900 0110-DERIVE-RETENTION-DATE   SECTION.                            PPEY150
023000*----------------------------------------------------------------*PPEY150
023010                                                                  PPEY150
023100     MOVE XDTS-ISO-FIRST-OF-MONTH-DATE TO WS-RETENTION-DATE.      PPEY150
023102                                                                  PPEY150
023103     SUBTRACT FCB-NUMBER-YEARS-RETENTION                          PPEY150
023104                             FROM WS-RETENTION-YYYY.              PPEY150
023110                                                                  PPEY150
023111     EJECT                                                        PPEY150
023112*----------------------------------------------------------------*PPEY150
023113 0150-COUNT-FCB-ROWS          SECTION.                            PPEY150
023115*----------------------------------------------------------------*PPEY150
023116                                                                  PPEY150
023117****************************************************************  PPEY150
023118*  COUNT THE NUMBER OF ENTRIES IN THE FCB ARRAY.                  PPEY150
023119*  COUNT ONLY THOSE ENTRIES NOT PREVIOUSLY MARKED FOR DELETION.   PPEY150
023121****************************************************************  PPEY150
023122                                                                  PPEY150
023123     MOVE ZEROES TO NUM-EFCB-NON-DELETES.                         PPEY150
023124                                                                  PPEY150
023125     PERFORM WITH TEST BEFORE                                     PPEY150
023126      VARYING NUM-EFCB-ENTRIES  FROM +1 BY +1                     PPEY150
023127        UNTIL NUM-EFCB-ENTRIES  > IDC-MAX-NO-FCB                  PPEY150
023128           OR EFCB-OCCURRENCE-KEY (NUM-EFCB-ENTRIES)              PPEY150
023129                                         = INIT-EFCB-KEY          PPEY150
023130           OR EFCB-OCCURRENCE-KEY (NUM-EFCB-ENTRIES)              PPEY150
023131                                         = X'0000'                PPEY150
023132           IF EFCB-FCB-ADC-CODE (NUM-EFCB-ENTRIES) NOT = 'D'      PPEY150
023133              ADD +1 TO NUM-EFCB-NON-DELETES                      PPEY150
023134           END-IF                                                 PPEY150
023135     END-PERFORM.                                                 PPEY150
023136                                                                  PPEY150
023137     COMPUTE NUM-EFCB-ENTRIES  = NUM-EFCB-ENTRIES - +1.           PPEY150
023138     EJECT                                                        PPEY150
023139*----------------------------------------------------------------*PPEY150
023140 1000-MAINLINE-ROUTINE    SECTION.                                PPEY150
023150*----------------------------------------------------------------*PPEY150
023230                                                                  PPEY150
023240     PERFORM 2000-PROCESS-FCB-RECORDS.                            PPEY150
023250                                                                  PPEY150
023260     ADD WS-DELETE-FCB-ENTRIES TO PPFCCRPT-TT-RECS-DEL.           PPEY150
023264     COMPUTE WS-RETAIN-REC-CT                                     PPEY150
023265                = NUM-EFCB-NON-DELETES - WS-DELETE-FCB-ENTRIES.   PPEY150
023266     ADD  WS-RETAIN-REC-CT TO PPFCCRPT-TT-RECS-RET.               PPEY150
023270                                                                  PPEY150
024412     EJECT                                                        PPEY150
024413*----------------------------------------------------------------*PPEY150
024414 2000-PROCESS-FCB-RECORDS  SECTION.                               PPEY150
024416*----------------------------------------------------------------*PPEY150
024417                                                                  PPEY150
024419     PERFORM 0150-COUNT-FCB-ROWS.                                 PPEY150
024420                                                                  PPEY150
024421     MOVE ZERO TO WS-DELETE-FCB-ENTRIES                           PPEY150
024422                  WS-RETAIN-REC-CT.                               PPEY150
024423                                                                  PPEY150
024424     PERFORM WITH TEST BEFORE                                     PPEY150
024425     VARYING ENTRY-X  FROM +1 BY +1                               PPEY150
024430     UNTIL ENTRY-X  > NUM-EFCB-ENTRIES                            PPEY150
024450                                                                  PPEY150
024460         EVALUATE EFCB-FCB-BENEFIT-TYPE (ENTRY-X)                 PPEY150
024461             WHEN 'M'                                             PPEY150
024464               IF  EFCB-FCB-ADC-CODE (ENTRY-X) NOT = 'D'          PPEY150
025230                    IF EFCB-FCB-COV-END-DATE (ENTRY-X)            PPEY150
025240                                        NOT = XDTS-ISO-ZERO-DATE  PPEY150
025250                       IF  EFCB-FCB-COV-END-DATE  (ENTRY-X)       PPEY150
025260                                           < WS-RETENTION-DATE    PPEY150
025270                                                                  PPEY150
025271                           PERFORM 2010-DELETE-FCB-RECORD         PPEY150
025302                       END-IF                                     PPEY150
025410                   END-IF                                         PPEY150
025420               END-IF                                             PPEY150
025430             WHEN 'D'                                             PPEY150
025431               IF  EFCB-FCB-ADC-CODE (ENTRY-X) NOT = 'D'          PPEY150
025503                   IF EFCB-FCB-COV-END-DATE (ENTRY-X)             PPEY150
025504                                        NOT = XDTS-ISO-ZERO-DATE  PPEY150
025505                      IF  EFCB-FCB-COV-END-DATE  (ENTRY-X)        PPEY150
025506                                           < WS-RETENTION-DATE    PPEY150
025507                                                                  PPEY150
025508                          PERFORM 2010-DELETE-FCB-RECORD          PPEY150
025509                      END-IF                                      PPEY150
025512                   END-IF                                         PPEY150
025513               END-IF                                             PPEY150
025514             WHEN 'V'                                             PPEY150
025515               IF  EFCB-FCB-ADC-CODE (ENTRY-X) NOT = 'D'          PPEY150
025533                   IF EFCB-FCB-COV-END-DATE (ENTRY-X)             PPEY150
025534                                        NOT = XDTS-ISO-ZERO-DATE  PPEY150
025535                      IF  EFCB-FCB-COV-END-DATE  (ENTRY-X)        PPEY150
025536                                           < WS-RETENTION-DATE    PPEY150
025537                                                                  PPEY150
025538                          PERFORM 2010-DELETE-FCB-RECORD          PPEY150
025539                      END-IF                                      PPEY150
025542                   END-IF                                         PPEY150
025543               END-IF                                             PPEY150
025544             WHEN 'J'                                             PPEY150
025545               IF  EFCB-FCB-ADC-CODE (ENTRY-X) NOT = 'D'          PPEY150
025563                   IF EFCB-FCB-COV-END-DATE (ENTRY-X)             PPEY150
025564                                        NOT = XDTS-ISO-ZERO-DATE  PPEY150
025565                      IF  EFCB-FCB-COV-END-DATE  (ENTRY-X)        PPEY150
025566                                           < WS-RETENTION-DATE    PPEY150
025567                                                                  PPEY150
025568                          PERFORM 2010-DELETE-FCB-RECORD          PPEY150
025569                      END-IF                                      PPEY150
025572                   END-IF                                         PPEY150
025573               END-IF                                             PPEY150
025574         END-EVALUATE                                             PPEY150
025580                                                                  PPEY150
025600     END-PERFORM.                                                 PPEY150
029111                                                                  PPEY150
029113     EJECT                                                        PPEY150
029114*----------------------------------------------------------------*PPEY150
029115 2010-DELETE-FCB-RECORD   SECTION.                                PPEY150
029117*----------------------------------------------------------------*PPEY150
029118*----> LATER MODULE PPEI715 WILL DO THE ACTUAL DELETES            PPEY150
029119                                                                  PPEY150
029120     ADD 1 TO WS-DELETE-FCB-ENTRIES.                              PPEY150
029123                                                                  PPEY150
029124     MOVE XDC3-LOW-ISO-DATE                                       PPEY150
029126                 TO EFCB-FCB-COV-END-DATE (ENTRY-X).              PPEY150
029127                                                                  PPEY150
029128     MOVE SPACES TO EFCB-FCB-PLAN-INFO-DATA (ENTRY-X)             PPEY150
029129                    EFCB-FCB-COVERAGE-CODE  (ENTRY-X)             PPEY150
029130                    EFCB-FCB-ENRL-REAS-CODE (ENTRY-X).            PPEY150
029131                                                                  PPEY150
029132     MOVE 'D'  TO   EFCB-FCB-ADC-CODE       (ENTRY-X).            PPEY150
029133                                                                  PPEY150
029134     PERFORM 2100-SET-FIELDS-CHANGE.                              PPEY150
029135                                                                  PPEY150
029136     EJECT                                                        PPEY150
029137*----------------------------------------------------------------*PPEY150
029138 2100-SET-FIELDS-CHANGE    SECTION.                               PPEY150
029139*----------------------------------------------------------------*PPEY150
029140                                                                  PPEY150
029141     MOVE E0693 TO DL-FIELD.                                      PPEY150
029142     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      PPEY150
029143                                                                  PPEY150
029144     MOVE E0695 TO DL-FIELD.                                      PPEY150
029145     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      PPEY150
029148                                                                  PPEY150
029149     MOVE E0696 TO DL-FIELD.                                      PPEY150
029150     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      PPEY150
029151                                                                  PPEY150
029152     MOVE E0697 TO DL-FIELD.                                      PPEY150
029153     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      PPEY150
029154                                                                  PPEY150
029155     MOVE E0698 TO DL-FIELD.                                      PPEY150
029156     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      PPEY150
029157                                                                  PPEY150
029158     MOVE E0699 TO DL-FIELD.                                      PPEY150
029159     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      PPEY150
029160                                                                  PPEY150
029161     EJECT                                                        PPEY150
029162*----------------------------------------------------------------*PPEY150
029163 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEY150
029164*----------------------------------------------------------------*PPEY150
029165                                                                  PPEY150
029170*                                                 *-------------* PPEY150
029180                                                   COPY CPPDXDEC. PPEY150
029190*                                                 *-------------* PPEY150
029191                                                                  PPEY150
029192*----------------------------------------------------------------*PPEY150
029193 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEY150
029194*----------------------------------------------------------------*PPEY150
029195                                                                  PPEY150
029196*                                                 *-------------* PPEY150
029197                                                   COPY CPPDADLC. PPEY150
029198*                                                 *-------------* PPEY150
029199                                                                  PPEY150
029200                                                                  PPEY150
029300******************************************************************PPEY150
029400**   E N D  S O U R C E   ----  PPEY150  ----                   **PPEY150
029500******************************************************************PPEY150
