000100**************************************************************/   30871460
000200*  PROGRAM: PPCTT01                                          */   30871460
000300*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000400*  NAME:_____J.WILCOX____ CREATION DATE:      ___01/22/03__  */   30871460
000500*  DESCRIPTION:                                              */   30871460
000600*                                                            */   30871460
000700*  - NEW BASE SYSTEM MODULE                                  */   30871460
000800**************************************************************/   30871460
000900 IDENTIFICATION DIVISION.                                         PPCTT01
001000 PROGRAM-ID. PPCTT01.                                             PPCTT01
001100 AUTHOR. JEROME C. WILCOX.                                        PPCTT01
001200 INSTALLATION. UNIVERSITY OF CALIFORNIA, BASE PAYROLL SYSTEM.     PPCTT01
001300 DATE-WRITTEN. JUNE 16, 1000.                                     PPCTT01
001400 DATE-COMPILED.                                                   PPCTT01
001500     SKIP2                                                        PPCTT01
001600******************************************************************PPCTT01
001700**                                                              **PPCTT01
001800** THIS MODULE IS THE TRANSACTION HANDLER FOR PPS CONTROL TABLE **PPCTT01
001900** UPDATES OF THE SYSTEM PARAMETER TABLE (TABLE 01). SEE THE    **PPCTT01
002000** DOCUMENTATION FOR PROGRAM PPP010 FOR DETAILS.                **PPCTT01
002100**                                                              **PPCTT01
002200******************************************************************PPCTT01
002300     EJECT                                                        PPCTT01
002400 ENVIRONMENT DIVISION.                                            PPCTT01
002500 CONFIGURATION SECTION.                                           PPCTT01
002600     SKIP2                                                        PPCTT01
002700 SOURCE-COMPUTER. IBM-390.                                        PPCTT01
002800 OBJECT-COMPUTER. IBM-390.                                        PPCTT01
002900     SKIP2                                                        PPCTT01
003000 INPUT-OUTPUT SECTION.                                            PPCTT01
003100 FILE-CONTROL.                                                    PPCTT01
003200 I-O-CONTROL.                                                     PPCTT01
003300     EJECT                                                        PPCTT01
003400 DATA DIVISION.                                                   PPCTT01
003500 FILE SECTION.                                                    PPCTT01
003600     SKIP3                                                        PPCTT01
003700 WORKING-STORAGE SECTION.                                         PPCTT01
003800******************************************************************PPCTT01
003900*                WORKING STORAGE                                 *PPCTT01
004000******************************************************************PPCTT01
004100*                                                                 PPCTT01
004200 01  WA-WORK-VARIABLES.                                           PPCTT01
004300     05  WA-WORK-Z5                    PIC ZZ,ZZ9.                PPCTT01
004400     SKIP3                                                        PPCTT01
004500 01  WB-PROGRAM-CONSTANTS.                                        PPCTT01
004600     05  WB-SYSTEM-PARAMETER-TABLE-NO  PIC X(2) VALUE '01'.       PPCTT01
004700     SKIP3                                                        PPCTT01
004800 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT01
004900     05  WC-ERROR-INDEX                PIC S9(04) VALUE ZERO.     PPCTT01
005000     EJECT                                                        PPCTT01
005100 01  WF-PROGRAM-FLAGS.                                            PPCTT01
005200     05  WF-ABEND-FLAG                 PIC X(1) VALUE SPACE.      PPCTT01
005300         88  WF-ABENDED                         VALUE 'Y'.        PPCTT01
005400         88  WF-NORMAL-FLOW                     VALUE SPACE.      PPCTT01
005500     05  WF-TRANSACTION-FLAG           PIC X(1) VALUE SPACE.      PPCTT01
005600         88  WF-TRANSACTIONS-DONE               VALUE 'Y'.        PPCTT01
005700         88  WF-TRANSACTIONS-REMAIN             VALUE SPACE.      PPCTT01
005800     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT01
005900         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT01
006000         88  WF-ACCEPT-TRANSACTION              VALUES 00 THRU 04.PPCTT01
006100         88  WF-REJECT-TRANSACTION              VALUES 05 THRU 99.PPCTT01
006200     EJECT                                                        PPCTT01
006300 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT01
006400     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT01
006500     05  MCT003                     PIC X(05) VALUE 'CT003'.      PPCTT01
006600     EJECT                                                        PPCTT01
006700 01  WT-TRANSACTION.                                              PPCTT01
006800     05  WT-ACTION-CODE             PIC X(01).                    PPCTT01
006900         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT01
007000         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT01
007100         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT01
007200         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT01
007300     05  FILLER                     PIC X(02).                    PPCTT01
007400     05  WT-DATA.                                                 PPCTT01
007500         10  WT-PARM-NO             PIC X(03).                    PPCTT01
007600         10  FILLER                 PIC X(01).                    PPCTT01
007700         10  WT-PARM-VALUE          PIC X(09).                    PPCTT01
007800         10  WT-PARM-DESC           PIC X(20).                    PPCTT01
007900         10  FILLER                 PIC X(44).                    PPCTT01
008000     EJECT                                                        PPCTT01
008100******************************************************************PPCTT01
008200*****                                                            *PPCTT01
008300*****   C O P I E D  W O R K I N G  S T O R A G E                *PPCTT01
008400*****                                                            *PPCTT01
008500******************************************************************PPCTT01
008600     SKIP1                                                        PPCTT01
008700 01  XWHC-COMPILE-WORK-AREA.        COPY CPWSXWHC.                PPCTT01
008800     EJECT                                                        PPCTT01
008900******************************************************************PPCTT01
009000*****                                                            *PPCTT01
009100*****   L I N K A G E  T O  C A L L E D  M O D U L E S           *PPCTT01
009200*****                                                            *PPCTT01
009300******************************************************************PPCTT01
009400     SKIP1                                                        PPCTT01
009500*--->                                                             PPCTT01
009600*---> LINKAGE TO CALLED MODULE PPCTLRPT                           PPCTT01
009700*--->                                                             PPCTT01
009800 01  CTRI-CTL-REPORT-INTERFACE.     COPY CPLNCTRI.                PPCTT01
009900     EJECT                                                        PPCTT01
010000*--->                                                             PPCTT01
010100*---> LINKAGE TO CALLED MODULE PPCTLTRD                           PPCTT01
010200*--->                                                             PPCTT01
010300 01  CSRI-CTL-SORTED-READ-INTERFACE. COPY CPLNCSRI.               PPCTT01
010400     EJECT                                                        PPCTT01
010500*--->                                                             PPCTT01
010600*---> LINKAGES TO CALLED MODULE PPCTPRME                          PPCTT01
010700*--->                                                             PPCTT01
010800 01  CONTROL-TABLE-EDIT-INTERFACE.  COPY CPLNKCTE.                PPCTT01
010900     SKIP3                                                        PPCTT01
011000 01  PARAMETER-TABLE-INPUT.         COPY CPCTPRMI.                PPCTT01
011100     EJECT                                                        PPCTT01
011200*--->                                                             PPCTT01
011300*---> LINKAGES TO CALLED MODULE PPCTPRMU                          PPCTT01
011400*--->                                                             PPCTT01
011500 01  CONTROL-TABLE-UPDT-INTERFACE.  COPY CPLNKCTU.                PPCTT01
011600     SKIP3                                                        PPCTT01
011700 01  PARAMETER-TABLE-DATA.                                        PPCTT01
011800     EXEC SQL                                                     PPCTT01
011900         INCLUDE PPPVZPRM                                         PPCTT01
012000     END-EXEC.                                                    PPCTT01
012100     EJECT                                                        PPCTT01
012200 LINKAGE SECTION.                                                 PPCTT01
012300******************************************************************PPCTT01
012400*****                                                            *PPCTT01
012500*****  L I N K A G E  F R O M  C A L L I N G  P R O G R A M      *PPCTT01
012600*****                                                            *PPCTT01
012700******************************************************************PPCTT01
012800 01  CTTI-CTL-TBL-TRANS-INTERFACE.  COPY CPLNCTTI.                PPCTT01
012900     EJECT                                                        PPCTT01
013000 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE.           PPCTT01
013100 00000-MAINLINE SECTION.                                          PPCTT01
013200 00000-ENTRY.                                                     PPCTT01
013300*                                                                 PPCTT01
013400     PERFORM 10000-PROGRAM-INITIALIZATION.                        PPCTT01
013500*                                                                 PPCTT01
013600     PERFORM UNTIL WF-TRANSACTIONS-DONE OR WF-ABENDED             PPCTT01
013700         PERFORM 20000-PROCESS-TRANSACTION                        PPCTT01
013800         PERFORM 92000-RETURN-TRANSACTION                         PPCTT01
013900     END-PERFORM.                                                 PPCTT01
014000*                                                                 PPCTT01
014100     PERFORM 80000-PROGRAM-TERMINATION.                           PPCTT01
014200*                                                                 PPCTT01
014300     GOBACK.                                                      PPCTT01
014400     EJECT                                                        PPCTT01
014500 10000-PROGRAM-INITIALIZATION SECTION.                            PPCTT01
014600 10000-ENTRY.                                                     PPCTT01
014700     SKIP1                                                        PPCTT01
014800     PERFORM 11000-SELF-IDENTIFICATION.                           PPCTT01
014900     SKIP1                                                        PPCTT01
015000     SET CTRI-PRINT-CALL TO TRUE.                                 PPCTT01
015100     MOVE SPACES TO CTRI-SPACING-OVERRIDE-FLAG,                   PPCTT01
015200                    CTRI-TRANSACTION,                             PPCTT01
015300                    CTRI-TRANSACTION-DISPO,                       PPCTT01
015400                    CTRI-TEXT-LINE,                               PPCTT01
015500                    CTRI-MESSAGE-NUMBER.                          PPCTT01
015600     MOVE ZERO TO CTRI-MESSAGE-SEVERITY.                          PPCTT01
015700     SKIP1                                                        PPCTT01
015800     MOVE WB-SYSTEM-PARAMETER-TABLE-NO TO CSRI-CALL-TYPE-NUM.     PPCTT01
015900     SKIP1                                                        PPCTT01
016000     MOVE ZERO TO CTTI-ERROR-SEVERITY.                            PPCTT01
016100*--->                                                             PPCTT01
016200*---> GET THE FIRST TRANSACTION FOR THIS TABLE                    PPCTT01
016300*--->                                                             PPCTT01
016400     PERFORM 92000-RETURN-TRANSACTION.                            PPCTT01
016500     SKIP3                                                        PPCTT01
016600     GO TO 19999-EXIT.                                            PPCTT01
016700     EJECT                                                        PPCTT01
016800 11000-SELF-IDENTIFICATION.                                       PPCTT01
016900     SKIP1                                                        PPCTT01
017000     MOVE 'PPCTT01' TO XWHC-DISPLAY-MODULE.                       PPCTT01
017100     SKIP1                                                        PPCTT01
017200     COPY CPPDXWHC.                                               PPCTT01
017300     SKIP3                                                        PPCTT01
017400 19999-EXIT.                                                      PPCTT01
017500     EXIT.                                                        PPCTT01
017600     EJECT                                                        PPCTT01
017700******************************************************************PPCTT01
017800*                TRANSACTION PROCESSING                          *PPCTT01
017900******************************************************************PPCTT01
018000 20000-PROCESS-TRANSACTION SECTION.                               PPCTT01
018100 20000-ENTRY.                                                     PPCTT01
018200     SKIP1                                                        PPCTT01
018300     MOVE CSRI-TRANSACTION TO WT-TRANSACTION,                     PPCTT01
018400                              CTRI-TRANSACTION.                   PPCTT01
018500     IF COMMENT-TRANSACTION                                       PPCTT01
018600         PERFORM 21000-PRINT-COMMENT                              PPCTT01
018700     ELSE                                                         PPCTT01
018800         PERFORM 22000-EDIT-TRANSACTION                           PPCTT01
018900         IF WF-ACCEPT-TRANSACTION                                 PPCTT01
019000             PERFORM 23000-UPDATE-DB2-TABLE                       PPCTT01
019100         END-IF                                                   PPCTT01
019200     END-IF.                                                      PPCTT01
019300     SKIP1                                                        PPCTT01
019400     GO TO 29999-EXIT.                                            PPCTT01
019500     EJECT                                                        PPCTT01
019600 21000-PRINT-COMMENT.                                             PPCTT01
019700     SKIP1                                                        PPCTT01
019800     MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO.           PPCTT01
019900     PERFORM 91000-CALL-PPCTLRPT.                                 PPCTT01
020000     ADD 1 TO CTTI-TRANS-ACCEPTED.                                PPCTT01
020010     EJECT                                                        PPCTT01
020100 22000-EDIT-TRANSACTION.                                          PPCTT01
020200     SKIP1                                                        PPCTT01
020300     SET WF-NO-ERRORS TO TRUE.                                    PPCTT01
020400     MOVE WT-ACTION-CODE TO KCTE-ACTION.                          PPCTT01
020500     MOVE CTTI-ACTION-DATE TO KCTE-LAST-ACTION-DT.                PPCTT01
020600*---> MOVE THE DATA SPECIFIC TO THIS PARTICULAR TABLE             PPCTT01
020700     MOVE WT-PARM-NO TO PRMI-NUMBER-X.                            PPCTT01
020800     MOVE WT-PARM-VALUE TO PRMI-DATA-X.                           PPCTT01
020900     MOVE WT-PARM-DESC TO PRMI-DESCRIPTION.                       PPCTT01
021000*---> AND CALL THE EDIT MODULE                                    PPCTT01
021100     CALL 'PPCTPRME' USING CONTROL-TABLE-EDIT-INTERFACE,          PPCTT01
021200                           PARAMETER-TABLE-INPUT,                 PPCTT01
021300                           PARAMETER-TABLE-DATA.                  PPCTT01
021400     SKIP1                                                        PPCTT01
021500     IF KCTE-ERR = ZERO                                           PPCTT01
021600         MOVE 'TRANSACTION ACCEPTED' TO CTRI-TRANSACTION-DISPO    PPCTT01
021700         ADD 1 TO CTTI-TRANS-ACCEPTED                             PPCTT01
021800     ELSE                                                         PPCTT01
021900         PERFORM 22100-PRINT-ERROR                                PPCTT01
022000           VARYING WC-ERROR-INDEX FROM 1 BY 1                     PPCTT01
022100             UNTIL WC-ERROR-INDEX > KCTE-ERR                      PPCTT01
022200         IF WF-ACCEPT-TRANSACTION AND KCTE-STATUS-COMPLETED       PPCTT01
022300             MOVE 'TRANSACTION ACCEPTED' TO CTRI-TEXT-DISPO       PPCTT01
022400             ADD 1 TO CTTI-TRANS-ACCEPTED                         PPCTT01
022500         ELSE                                                     PPCTT01
022600             IF KCTE-STATUS-NOT-COMPLETED                         PPCTT01
022700                 MOVE MCT003 TO CTRI-MESSAGE-NUMBER               PPCTT01
022800             END-IF                                               PPCTT01
022900             MOVE 'TRANSACTION REJECTED' TO CTRI-TEXT-DISPO       PPCTT01
023000             ADD 1 TO CTTI-TRANS-REJECTED                         PPCTT01
023100         END-IF                                                   PPCTT01
023200     END-IF.                                                      PPCTT01
023300*---> PRINT THE FINAL (OR PERHAPS ONLY) LINE OF OUTPUT            PPCTT01
023400     PERFORM 91000-CALL-PPCTLRPT.                                 PPCTT01
023410     MOVE 2 TO CTRI-SPACING-OVERRIDE.                             PPCTT01
023500     SKIP3                                                        PPCTT01
023600 22100-PRINT-ERROR.                                               PPCTT01
023700     SKIP1                                                        PPCTT01
023800     MOVE KCTE-ERR-NO (WC-ERROR-INDEX) TO CTRI-MESSAGE-NUMBER.    PPCTT01
023900     PERFORM 91000-CALL-PPCTLRPT.                                 PPCTT01
024000     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT01
024100         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT01
024200     END-IF.                                                      PPCTT01
024300     EJECT                                                        PPCTT01
024400 23000-UPDATE-DB2-TABLE.                                          PPCTT01
024500     SKIP1                                                        PPCTT01
024600     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT01
024700     CALL 'PPCTPRMU' USING CONTROL-TABLE-UPDT-INTERFACE,          PPCTT01
024800                           PARAMETER-TABLE-DATA.                  PPCTT01
024900     IF KCTU-STATUS-OK                                            PPCTT01
025000         EVALUATE TRUE                                            PPCTT01
025100           WHEN KCTU-ACTION-ADD                                   PPCTT01
025200             ADD 1 TO CTTI-ENTRIES-ADDED                          PPCTT01
025300           WHEN KCTU-ACTION-CHANGE                                PPCTT01
025400             ADD 1 TO CTTI-ENTRIES-UPDATED                        PPCTT01
025500           WHEN KCTU-ACTION-DELETE                                PPCTT01
025600             ADD 1 TO CTTI-ENTRIES-DELETED                        PPCTT01
025700           WHEN OTHER                                             PPCTT01
025800             PERFORM 23100-BAD-UPDATE-CALL                        PPCTT01
025900         END-EVALUATE                                             PPCTT01
026000     ELSE                                                         PPCTT01
026100         PERFORM 23100-BAD-UPDATE-CALL                            PPCTT01
026200     END-IF.                                                      PPCTT01
026300     SKIP3                                                        PPCTT01
026400 23100-BAD-UPDATE-CALL.                                           PPCTT01
026500     SKIP1                                                        PPCTT01
026600     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT01
026700     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT01
026800     STRING 'PPCTPRMU ERROR: ACTION WAS "',                       PPCTT01
026900            KCTU-ACTION,                                          PPCTT01
027000            '" STATUS WAS "',                                     PPCTT01
027100            KCTU-STATUS,                                          PPCTT01
027200            ' COUNT=',                                            PPCTT01
027300            WA-WORK-Z5                                            PPCTT01
027400         DELIMITED BY SIZE                                        PPCTT01
027500         INTO CTRI-TEXT-LINE                                      PPCTT01
027600     PERFORM 91000-CALL-PPCTLRPT                                  PPCTT01
027700     SET WF-ABENDED TO TRUE                                       PPCTT01
027800     PERFORM 99990-ABEND.                                         PPCTT01
027900     SKIP3                                                        PPCTT01
028000 29999-EXIT.                                                      PPCTT01
028100     EXIT.                                                        PPCTT01
028200     EJECT                                                        PPCTT01
028300******************************************************************PPCTT01
028400*                PROGRAM TERMINATION                             *PPCTT01
028500******************************************************************PPCTT01
028600 80000-PROGRAM-TERMINATION SECTION.                               PPCTT01
028700 80000-ENTRY.                                                     PPCTT01
028800     SKIP1                                                        PPCTT01
028900     IF WF-NORMAL-FLOW                                            PPCTT01
029000         SET CTTI-NORMAL TO TRUE                                  PPCTT01
029100     ELSE                                                         PPCTT01
029200         SET CTTI-ABORT TO TRUE                                   PPCTT01
029300     END-IF.                                                      PPCTT01
029400     SKIP3                                                        PPCTT01
029500 89999-EXIT.                                                      PPCTT01
029600     EXIT.                                                        PPCTT01
029700     EJECT                                                        PPCTT01
029800*--->                                                             PPCTT01
029900*---> PARAGRAPHS IN THIS SECTION ARE INDIVIDUALLY PERFORMED       PPCTT01
030000*---> TO CALL UTILITY MODULES AS NEEDED BY THE OTHER ROUTINES     PPCTT01
030100*--->                                                             PPCTT01
030200     SKIP1                                                        PPCTT01
030300 90000-PERFORMED-MODULES SECTION.                                 PPCTT01
030400     SKIP1                                                        PPCTT01
030500 91000-CALL-PPCTLRPT.                                             PPCTT01
030600     SKIP1                                                        PPCTT01
030700     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT01
030800     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT01
030900         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT01
031000     END-IF.                                                      PPCTT01
031100     IF CTRI-RUN-ABORTED                                          PPCTT01
031200         SET WF-ABENDED TO TRUE                                   PPCTT01
031300         PERFORM 99990-ABEND                                      PPCTT01
031400     END-IF.                                                      PPCTT01
031500     SKIP3                                                        PPCTT01
031600 92000-RETURN-TRANSACTION.                                        PPCTT01
031700     SKIP1                                                        PPCTT01
031800*---> USE PPCTLTRD TO RETRIEVE A TRANSACTION                      PPCTT01
031900     CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE.        PPCTT01
032000     EVALUATE TRUE                                                PPCTT01
032100       WHEN CSRI-NORMAL                                           PPCTT01
032200*-------> JUST PASS THE TRANSACTION BACK FOR PROCESSING           PPCTT01
032300         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT01
032400       WHEN CSRI-TABLE-TRANS-END                                  PPCTT01
032500*-------> WE'RE DONE WITH TRANSACTIONS FOR THIS TABLE             PPCTT01
032600         SET WF-TRANSACTIONS-DONE TO TRUE                         PPCTT01
032700       WHEN OTHER                                                 PPCTT01
032800*-------> BIG PROBLEM! RUN MUST BE TERMINATED                     PPCTT01
032900         MOVE MCT001 TO CTRI-MESSAGE-NUMBER                       PPCTT01
033000         MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                  PPCTT01
033100         STRING 'PPCTLTRD ERROR ON TRANS CALL: RETURN='           PPCTT01
033200                CSRI-RETURN-CODE,                                 PPCTT01
033300                ' COUNT=',                                        PPCTT01
033400                WA-WORK-Z5                                        PPCTT01
033500             DELIMITED BY SIZE                                    PPCTT01
033600             INTO CTRI-TEXT-LINE                                  PPCTT01
033700         PERFORM 91000-CALL-PPCTLRPT                              PPCTT01
033800         SET WF-ABENDED TO TRUE                                   PPCTT01
033900         PERFORM 99990-ABEND                                      PPCTT01
034000      END-EVALUATE.                                               PPCTT01
034100     SKIP3                                                        PPCTT01
034200 99990-ABEND.                                                     PPCTT01
034300     SKIP1                                                        PPCTT01
034400*---> NOTE: THIS PERFORM DOES NOT RETURN TO ITS CALLER            PPCTT01
034500     SET CTTI-ABORT TO TRUE.                                      PPCTT01
034600     GOBACK.                                                      PPCTT01
034700     SKIP3                                                        PPCTT01
034800******************************************************************PPCTT01
034900* DO NOT PUT ANY PROGAM-SPECIFIC CODE AFTER THIS POINT.          *PPCTT01
035000******************************************************************PPCTT01
035100******************************************************************PPCTT01
035200* END OF SOURCE CODE                                             *PPCTT01
035300******************************************************************PPCTT01
