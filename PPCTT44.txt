000000**************************************************************/   72751413
000001*  PROGRAM: PPCTT44                                          */   72751413
000002*  RELEASE: ___1413______ SERVICE REQUEST(S): ____17275____  */   72751413
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___05/21/02__  */   72751413
000004*  DESCRIPTION:                                              */   72751413
000005*  PPPMCP TABLE TRANSACTION ASSEMBLY MODULE                  */   72751413
000007**************************************************************/   72751413
000060 IDENTIFICATION DIVISION.                                         PPCTT44
000070 PROGRAM-ID. PPCTT44.                                             PPCTT44
000080 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT44
000090 DATE-COMPILED.                                                   PPCTT44
000100 DATA DIVISION.                                                   PPCTT44
000200 FILE SECTION.                                                    PPCTT44
000300 WORKING-STORAGE SECTION.                                         PPCTT44
000400                                                                  PPCTT44
000500 01  WA-WORK-VARIABLES.                                           PPCTT44
000600     05  WA-WORK-Z5                    PIC ZZ,ZZ9.                PPCTT44
000700 01  WB-PROGRAM-CONSTANTS.                                        PPCTT44
000800     05  WB-TABLE-NO                   PIC X(2) VALUE '44'.       PPCTT44
000900 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT44
001000     05  WC-ERROR-INDEX                PIC S9(04) VALUE ZERO.     PPCTT44
001200     05  WC-TALLY                      PIC S9(04) VALUE ZERO.     PPCTT44
001300 01  WF-PROGRAM-FLAGS.                                            PPCTT44
001400     05  WF-ABEND-FLAG                 PIC X(1) VALUE SPACE.      PPCTT44
001500         88  WF-ABENDED                         VALUE 'Y'.        PPCTT44
001600         88  WF-NORMAL-FLOW                     VALUE SPACE.      PPCTT44
001700     05  WF-TRANSACTION-FLAG           PIC X(1) VALUE SPACE.      PPCTT44
001800         88  WF-TRANSACTIONS-DONE               VALUE 'Y'.        PPCTT44
001900         88  WF-TRANSACTIONS-REMAIN             VALUE SPACE.      PPCTT44
002000     05  WF-PROCESS-FLAG               PIC X(1) VALUE SPACE.      PPCTT44
002100         88  WF-PROCESS-COMPLETE                VALUE 'Y'.        PPCTT44
002200         88  WF-PROCESS-INCOMPLETE              VALUE SPACE.      PPCTT44
002500     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT44
002600         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT44
002610         88  WF-ACCEPT-TRANSACTION              VALUES 00 THRU 04.PPCTT44
002620         88  WF-REJECT-TRANSACTION              VALUES 05 THRU 99.PPCTT44
002700 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT44
002800     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT44
002900     05  MCT003                     PIC X(05) VALUE 'CT003'.      PPCTT44
004800 01  CONTROL-DEPARTMENT-TABLE-TRANS.                              PPCTT44
004900     05  MCPT-ACTION-CODE           PIC X(01).                    PPCTT44
005000         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT44
005100         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT44
005200         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT44
005300         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT44
005400     05  FILLER                     PIC X(02).                    PPCTT44
005500     05  MCPT-DEPT-NO               PIC X(06).                    PPCTT44
005900     05  MCPT-DATA-1.                                             PPCTT44
005910         10 MCPT-CONTROL-DEPT       PIC X(06).                    PPCTT44
006000         10 FILLER                  PIC X(65).                    PPCTT44
007400                                                                  PPCTT44
007500 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT44
007600 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT44
007700 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT44
007800 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT44
007900 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT44
008000 01  CONTROL-DEPARTMENT-TABLE-INPUT.   COPY CPCTMCPI.             PPCTT44
008100 01  CONTROL-DEPARTMENT-TABLE-DATA.                               PPCTT44
008200     EXEC SQL                                                     PPCTT44
008300         INCLUDE PPPVZMCP                                         PPCTT44
008400     END-EXEC.                                                    PPCTT44
008500                                                                  PPCTT44
008600 LINKAGE SECTION.                                                 PPCTT44
008700                                                                  PPCTT44
008800 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT44
008900                                                                  PPCTT44
009000 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE.           PPCTT44
009100                                                                  PPCTT44
009200 0000-MAIN                   SECTION.                             PPCTT44
009300                                                                  PPCTT44
009400     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT44
009500     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT44
009600       UNTIL WF-TRANSACTIONS-DONE OR WF-ABENDED.                  PPCTT44
009800     PERFORM 8000-PROGRAM-TERMINATION.                            PPCTT44
009900                                                                  PPCTT44
010000 0999-END.                                                        PPCTT44
010100                                                                  PPCTT44
010200     GOBACK.                                                      PPCTT44
010300                                                                  PPCTT44
010400 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT44
010500                                                                  PPCTT44
010600     MOVE 'PPCTT44' TO XWHC-DISPLAY-MODULE.                       PPCTT44
010700     COPY CPPDXWHC.                                               PPCTT44
010800     SET CTRI-PRINT-CALL TO TRUE.                                 PPCTT44
010900     MOVE SPACES TO CTRI-SPACING-OVERRIDE-FLAG                    PPCTT44
011000                    CTRI-TRANSACTION                              PPCTT44
011100                    CTRI-TRANSACTION-DISPO                        PPCTT44
011200                    CTRI-TEXT-LINE                                PPCTT44
011300                    CTRI-MESSAGE-NUMBER.                          PPCTT44
011310     MOVE SPACES TO CONTROL-DEPARTMENT-TABLE-TRANS.               PPCTT44
011400     MOVE ZERO TO CTRI-MESSAGE-SEVERITY.                          PPCTT44
011500     MOVE WB-TABLE-NO TO CSRI-CALL-TYPE-NUM.                      PPCTT44
011600     MOVE ZERO TO CTTI-ERROR-SEVERITY.                            PPCTT44
011700     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT44
011800                                                                  PPCTT44
011900 1999-INITIALIZATION-EXIT.                                        PPCTT44
012000     EXIT.                                                        PPCTT44
012100                                                                  PPCTT44
012200 2000-PROCESS-TRANSACTIONS   SECTION.                             PPCTT44
012300                                                                  PPCTT44
012800     MOVE CONTROL-DEPARTMENT-TABLE-TRANS TO CTRI-TRANSACTION.     PPCTT44
012810     MOVE SPACES TO CONTROL-DEPARTMENT-TABLE-INPUT.               PPCTT44
012900     PERFORM 2200-EDIT-TRANSACTION                                PPCTT44
013000     IF WF-ACCEPT-TRANSACTION                                     PPCTT44
013100        PERFORM 2300-UPDATE-DB2-TABLE                             PPCTT44
013200     END-IF.                                                      PPCTT44
013201     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT44
013210                                                                  PPCTT44
013300 2200-EDIT-TRANSACTION       SECTION.                             PPCTT44
013400                                                                  PPCTT44
013500     SET WF-NO-ERRORS TO TRUE.                                    PPCTT44
013600     MOVE MCPT-ACTION-CODE TO KCTE-ACTION.                        PPCTT44
013700     MOVE CTTI-ACTION-DATE TO KCTE-LAST-ACTION-DT.                PPCTT44
013800*---> MOVE THE DATA SPECIFIC TO THIS PARTICULAR TABLE             PPCTT44
013810     IF MCPT-DEPT-NO NOT = SPACES                                 PPCTT44
013820        MOVE MCPT-DEPT-NO TO MCPI-DEPT-NO                         PPCTT44
013830     END-IF.                                                      PPCTT44
013840     IF MCPT-CONTROL-DEPT NOT = SPACES                            PPCTT44
013850        MOVE MCPT-CONTROL-DEPT TO MCPI-CONTROL-DEPT               PPCTT44
013860     END-IF.                                                      PPCTT44
014200*---> AND CALL THE EDIT MODULE                                    PPCTT44
014300     CALL 'PPCTMCPE' USING CONTROL-TABLE-EDIT-INTERFACE,          PPCTT44
014400                           CONTROL-DEPARTMENT-TABLE-INPUT,        PPCTT44
014500                           CONTROL-DEPARTMENT-TABLE-DATA.         PPCTT44
014600                                                                  PPCTT44
014700     IF KCTE-ERR = ZERO                                           PPCTT44
014800         MOVE 'TRANSACTION ACCEPTED' TO CTRI-TRANSACTION-DISPO    PPCTT44
014900         ADD 1 TO CTTI-TRANS-ACCEPTED                             PPCTT44
015000     ELSE                                                         PPCTT44
015100         PERFORM 2210-PRINT-ERROR                                 PPCTT44
015200           VARYING WC-ERROR-INDEX FROM 1 BY 1                     PPCTT44
015300             UNTIL WC-ERROR-INDEX > KCTE-ERR                      PPCTT44
015400         IF WF-ACCEPT-TRANSACTION AND KCTE-STATUS-COMPLETED       PPCTT44
015500             MOVE 'TRANSACTION ACCEPTED' TO CTRI-TEXT-DISPO       PPCTT44
015600             ADD 1 TO CTTI-TRANS-ACCEPTED                         PPCTT44
015700         ELSE                                                     PPCTT44
015800             IF KCTE-STATUS-NOT-COMPLETED                         PPCTT44
015900                 MOVE MCT003 TO CTRI-MESSAGE-NUMBER               PPCTT44
016000             END-IF                                               PPCTT44
016100             MOVE 'TRANSACTION REJECTED' TO CTRI-TEXT-DISPO       PPCTT44
016200             ADD 1 TO CTTI-TRANS-REJECTED                         PPCTT44
016300         END-IF                                                   PPCTT44
016400     END-IF.                                                      PPCTT44
016500*---> PRINT THE FINAL (OR PERHAPS ONLY) LINE OF OUTPUT            PPCTT44
016600     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT44
016700     MOVE 2 TO CTRI-SPACING-OVERRIDE.                             PPCTT44
016800                                                                  PPCTT44
016900 2210-PRINT-ERROR            SECTION.                             PPCTT44
017000                                                                  PPCTT44
017100     MOVE KCTE-ERR-NO (WC-ERROR-INDEX) TO CTRI-MESSAGE-NUMBER.    PPCTT44
017200     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT44
017300     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT44
017400         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT44
017500     END-IF.                                                      PPCTT44
017600                                                                  PPCTT44
017700 2300-UPDATE-DB2-TABLE       SECTION.                             PPCTT44
017800                                                                  PPCTT44
017900     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT44
018000     CALL 'PPCTMCPU' USING CONTROL-TABLE-UPDT-INTERFACE,          PPCTT44
018100                           CONTROL-DEPARTMENT-TABLE-DATA.         PPCTT44
018200     IF KCTU-STATUS-OK                                            PPCTT44
018300         EVALUATE TRUE                                            PPCTT44
018400           WHEN KCTU-ACTION-ADD                                   PPCTT44
018500             ADD 1 TO CTTI-ENTRIES-ADDED                          PPCTT44
018600           WHEN KCTU-ACTION-CHANGE                                PPCTT44
018700             ADD 1 TO CTTI-ENTRIES-UPDATED                        PPCTT44
018800           WHEN KCTU-ACTION-DELETE                                PPCTT44
018900             ADD 1 TO CTTI-ENTRIES-DELETED                        PPCTT44
019000           WHEN OTHER                                             PPCTT44
019100             PERFORM 2310-BAD-UPDATE-CALL                         PPCTT44
019200         END-EVALUATE                                             PPCTT44
019300     ELSE                                                         PPCTT44
019400         PERFORM 2310-BAD-UPDATE-CALL                             PPCTT44
019500     END-IF.                                                      PPCTT44
019600                                                                  PPCTT44
019700 2310-BAD-UPDATE-CALL        SECTION.                             PPCTT44
019800                                                                  PPCTT44
019900     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT44
020000     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT44
020100     STRING 'PPCTMCPU ERROR: ACTION WAS "',                       PPCTT44
020200            KCTU-ACTION,                                          PPCTT44
020300            '" STATUS WAS "',                                     PPCTT44
020400            KCTU-STATUS,                                          PPCTT44
020500            ' COUNT=',                                            PPCTT44
020600            WA-WORK-Z5                                            PPCTT44
020700         DELIMITED BY SIZE                                        PPCTT44
020800         INTO CTRI-TEXT-LINE                                      PPCTT44
020900     PERFORM 9100-CALL-PPCTLRPT                                   PPCTT44
021000     SET WF-ABENDED TO TRUE                                       PPCTT44
021100     PERFORM 9999-ABEND.                                          PPCTT44
033000                                                                  PPCTT44
033100 8000-PROGRAM-TERMINATION    SECTION.                             PPCTT44
033200                                                                  PPCTT44
033300     IF WF-NORMAL-FLOW                                            PPCTT44
033400         SET CTTI-NORMAL TO TRUE                                  PPCTT44
033500     ELSE                                                         PPCTT44
033600         SET CTTI-ABORT TO TRUE                                   PPCTT44
033700     END-IF.                                                      PPCTT44
034100                                                                  PPCTT44
034200 9100-CALL-PPCTLRPT          SECTION.                             PPCTT44
034300                                                                  PPCTT44
034400     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT44
034500     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT44
034600         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT44
034700     END-IF.                                                      PPCTT44
034800     IF CTRI-RUN-ABORTED                                          PPCTT44
034900         SET WF-ABENDED TO TRUE                                   PPCTT44
035000         GO TO 9999-ABEND                                         PPCTT44
035100     END-IF.                                                      PPCTT44
035200                                                                  PPCTT44
035300 9199-EXIT.                                                       PPCTT44
035400     EXIT.                                                        PPCTT44
035500                                                                  PPCTT44
035600 9200-RETURN-TRANSACTION     SECTION.                             PPCTT44
035700                                                                  PPCTT44
035800     CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE.        PPCTT44
035900     EVALUATE TRUE                                                PPCTT44
036000       WHEN CSRI-NORMAL                                           PPCTT44
036100         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT44
036200       WHEN CSRI-TABLE-TRANS-END                                  PPCTT44
036300         SET WF-TRANSACTIONS-DONE TO TRUE                         PPCTT44
036400       WHEN OTHER                                                 PPCTT44
036500         MOVE MCT001 TO CTRI-MESSAGE-NUMBER                       PPCTT44
036600         MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                  PPCTT44
036700         STRING 'PPCTLTRD ERROR ON TRANS CALL: RETURN='           PPCTT44
036800                CSRI-RETURN-CODE,                                 PPCTT44
036900                ' COUNT=',                                        PPCTT44
037000                WA-WORK-Z5                                        PPCTT44
037100             DELIMITED BY SIZE                                    PPCTT44
037200             INTO CTRI-TEXT-LINE                                  PPCTT44
037300         PERFORM 9100-CALL-PPCTLRPT                               PPCTT44
037400         SET WF-ABENDED TO TRUE                                   PPCTT44
037500         GO TO 9999-ABEND                                         PPCTT44
037600      END-EVALUATE.                                               PPCTT44
037700                                                                  PPCTT44
037800 9299-EXIT.                                                       PPCTT44
037900     EXIT.                                                        PPCTT44
038000                                                                  PPCTT44
038100 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT44
038200                                                                  PPCTT44
038300     SET WF-PROCESS-INCOMPLETE TO TRUE.                           PPCTT44
038400     PERFORM WITH TEST AFTER                                      PPCTT44
038500        UNTIL WF-PROCESS-COMPLETE                                 PPCTT44
038600           OR WF-TRANSACTIONS-DONE                                PPCTT44
038700      PERFORM 9200-RETURN-TRANSACTION                             PPCTT44
038800      IF WF-TRANSACTIONS-REMAIN                                   PPCTT44
038900         MOVE CSRI-TRANSACTION TO CONTROL-DEPARTMENT-TABLE-TRANS  PPCTT44
039000         IF COMMENT-TRANSACTION                                   PPCTT44
039100            MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION             PPCTT44
039200            MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO     PPCTT44
039300            PERFORM 9100-CALL-PPCTLRPT                            PPCTT44
039400            ADD 1 TO CTTI-TRANS-ACCEPTED                          PPCTT44
039500         ELSE                                                     PPCTT44
039800            SET WF-PROCESS-COMPLETE TO TRUE                       PPCTT44
039900         END-IF                                                   PPCTT44
040000      END-IF                                                      PPCTT44
040100     END-PERFORM.                                                 PPCTT44
040200                                                                  PPCTT44
040300 9399-EXIT.                                                       PPCTT44
040400     EXIT.                                                        PPCTT44
040500                                                                  PPCTT44
040600 9999-ABEND                  SECTION.                             PPCTT44
040700                                                                  PPCTT44
040800     SET CTTI-ABORT TO TRUE.                                      PPCTT44
040900     GOBACK.                                                      PPCTT44
041000*************************END OF PPCTT44 SOURCE********************PPCTT44
