000100*==========================================================%      UCSD0144
000200*=    PROGRAM: PPP564                                     =%      UCSD0144
000300*=    CHANGE # UCSD0144     PROJ. REQUEST: PAYROLL OFFICE =%      UCSD0144
000400*=    NAME:G CHIU           MODIFICATION DATE: 5/11/01    =%      UCSD0144
000500*=                                                        =%      UCSD0144
000600*=    DESCRIPTION                                         =%      UCSD0144
000700*=    Add a new report which is the same as report1 but   =%      UCSD0144
000800*=    sorted in name order.                               =%      UCSD0144
000900*=                                                        =%      UCSD0144
001000*===========================================================%     UCSD0144
000160**************************************************************/   48491342
000200*  PROGRAM: PPP564                                           */   48491342
000300*  RELEASE: ___1342______ SERVICE REQUEST(S): ____14849____  */   48491342
000400*  NAME:__P. THOMPSON____ CREATION DATE:      ___04/26/01__  */   48491342
000500*  DESCRIPTION:                                              */   48491342
000600*  - PPP564 calls several modules which report variously on  */   48491342
000700*    Historical Premium Activity and related issues. The     */   48491342
000800*    modules are called per Run Specification Record request */   48491342
000810*    (see UPAYxxx for layout).                               */   48491342
000900*    Called sub-modules as of this initial release:          */   48491342
001000*    - PPHPARP1 produces the PPP5641 report Reconciliation   */   48491342
001100*      by Employee Within Department. This reports on all    */   48491342
001110*      PPPHPA rows which are out of balance and not offset   */   48491342
001120*      by another row.                                       */   48491342
001200*    - PPHPARP2 produces a tab delimited file from all rows  */   48491342
001300*      on the PPPHPA table.                                  */   48491342
001900**************************************************************/   48491342
002000 IDENTIFICATION DIVISION.                                         PPP564
002100 PROGRAM-ID. PPP564.                                              PPP564
002200 AUTHOR. PHILLIP THOMPSON, UCOP IS&C.                             PPP564
002300 INSTALLATION. UNIVERSITY OF CALIFORNIA, BASE PAYROLL SYSTEM.     PPP564
002400 DATE-WRITTEN. 04/01/01.                                          PPP564
002500 DATE-COMPILED.                                                   PPP564
002600                                                                  PPP564
003200 ENVIRONMENT DIVISION.                                            PPP564
003300 CONFIGURATION SECTION.                                           PPP564
003400                                                                  PPP564
003500 SOURCE-COMPUTER.            COPY CPOTXUCS.                       PPP564
003600 OBJECT-COMPUTER.            COPY CPOTXOBJ.                       PPP564
003700                                                                  PPP564
003800 SPECIAL-NAMES.                                                   PPP564
003900     C01 IS NEW-PAGE.                                             PPP564
004000                                                                  PPP564
004100 INPUT-OUTPUT SECTION.                                            PPP564
004200 FILE-CONTROL.                                                    PPP564
004300     SELECT SPEC-CARD-FILE           ASSIGN TO   UT-S-CARDFIL.    PPP564
004400                                                                  PPP564
004500 I-O-CONTROL.                                                     PPP564
004600                                                                  PPP564
004700 DATA DIVISION.                                                   PPP564
004800 FILE SECTION.                                                    PPP564
004810                                                                  PPP564
004820 FD  SPEC-CARD-FILE                                               PPP564
004830     LABEL RECORDS ARE STANDARD                                   PPP564
004840     RECORDING MODE IS F                                          PPP564
004850     RECORD CONTAINS 80 CHARACTERS                                PPP564
004860     DATA RECORD IS SPEC-CARD-RECORD.                             PPP564
004870 01  SPEC-CARD-RECORD            PIC X(80).                       PPP564
004900                                                                  PPP564
005600     EJECT                                                        PPP564
005700 WORKING-STORAGE SECTION.                                         PPP564
005800******************************************************************PPP564
005900*                Working Storage                                 *PPP564
006000******************************************************************PPP564
006100                                                                  PPP564
006200 01  WS-PROGRAM-CONSTANTS.                                        PPP564
006300     05  M56401                     PIC X(5)    VALUE '56401'.    PPP564
006400     05  M56405                     PIC X(5)    VALUE '56405'.    PPP564
006500     05  M56406                     PIC X(5)    VALUE '56406'.    PPP564
006930     05  WS-STND-PGM-ID             PIC X(15)   VALUE             PPP564
006940                                               'PPP564/042601  '. PPP564
007200     05  WS-PPHPARP1                PIC X(08)   VALUE 'PPHPARP1'. PPP564
007210     05  WS-PPHPARP2                PIC X(08)   VALUE 'PPHPARP2'. PPP564
007400     05  WS-PPHPARP9                PIC X(08)   VALUE 'PPHPARP9'. UCSD0144
011731******************************************************************PPP564
011732*    Run Specification Record.                                   *PPP564
011733******************************************************************PPP564
011740 01  WS-SPEC-CARD-RECORD.                                         PPP564
011750     05  WS-SPEC-ID              PIC X(11).                       PPP564
011760         88  VALID-SPEC-ID       VALUE 'PPP564-SPEC'.             PPP564
011770     05  WS-SPEC-RPT-REQUESTS.                                    PPP564
011771         10  WS-SPEC-RPT1        PIC X(1).                        PPP564
011772         88  CALL-PPHPARP1       VALUE 'Y'.                       PPP564
011773         10  WS-SPEC-RPT2        PIC X(1).                        PPP564
011774         88  CALL-PPHPARP2       VALUE 'Y'.                       PPP564
011780     05  FILLER                  PIC X(67).                       PPP564
011800******************************************************************PPP564
015100*    Report headings and detail lines                            *PPP564
015200******************************************************************PPP564
015300 01  STND-RPT-HD1.                           COPY CPWSXSHR.       PPP564
015400******************************************************************PPP564
015500*    PPP5640 Messages/Audit Report                               *PPP564
015600******************************************************************PPP564
015700 01  WS-5640-MISC-PRINT.                                          PPP564
015800     05  WS-5640-TTL           PIC X(40)       VALUE              PPP564
015900         '    HISTORICAL PREMIUM ACTIVITY MESSAGES'.              PPP564
016000******************************************************************PPP564
016100*    PPP564N Unmatched activity reports                          *PPP564
016200******************************************************************PPP564
016300 01  WS-564N-MISC-PRINT.                                          PPP564
016400     05  WS-564N-STD-RPT1.                                        PPP564
016500         10  FILLER            PIC X           VALUE SPACE.       PPP564
016600         10  FILLER            PIC X(126)      VALUE SPACES.      PPP564
016700         10  WS-564N-PAGE-NO   PIC 999999      VALUE ZERO.        PPP564
016800     05  WS-564N-TTL           PIC X(40)       VALUE SPACES.      PPP564
016900     05  WS-564N-LINES         PIC S99         VALUE +99.         PPP564
017000 01  WS-5642-MISC-PRINT.                                          PPP564
017100     05  WS-5642-STD-RPT1.                                        PPP564
017200         10  FILLER            PIC X           VALUE SPACE.       PPP564
017300         10  FILLER            PIC X(132)      VALUE SPACES.      PPP564
017400     05  WS-5642-TTL           PIC X(40)       VALUE              PPP564
017500         'RECONCILIATION BY EMPLOYEE WITHIN DEPART'.              PPP564
032300 01  CPWSXDC3.                               COPY CPWSXDC3.       PPP564
032400 01  MSSG-INTERFACE          EXTERNAL.       COPY CPWSXMIF.       PPP564
032610 01  XWHC-COMPILE-WORK-AREA.                 COPY CPWSXWHC.       PPP564
032611******************************************************************PPP564
032612*    Linkage for calls to PPHPA* modules                         *PPP564
032613******************************************************************PPP564
032620 01  CPLNKHPA-INTERFACE.                     COPY CPLNKHPA.       PPP564
037500     EJECT                                                        PPP564
037600******************************************************************PPP564
037700*                Procedure Division                              *PPP564
037800******************************************************************PPP564
037900 PROCEDURE DIVISION.                                              PPP564
038000******************************************************************PPP564
038100 00000-MAINLINE SECTION.                                          PPP564
038600                                                                  PPP564
038700     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPP564
038800     IF KHPA-PROGRAM-FATAL-ERROR                                  PPP564
038900        GO TO 0000-EOJ                                            PPP564
039000     END-IF.                                                      PPP564
040700                                                                  PPP564
040800******************************************************************PPP564
040900*    Perform program calls, per Run Specification Record.        *PPP564
041000******************************************************************PPP564
041100******************************************************************PPP564
041200*    Call PPHPARP1 for Reconciliation by Employee Within         *PPP564
041210*    Department report.                                          *PPP564
041300******************************************************************PPP564
041400     IF CALL-PPHPARP1                                             PPP564
041410     AND NOT KHPA-PROGRAM-FATAL-ERROR                             PPP564
041500        CALL WS-PPHPARP1 USING CPLNKHPA-INTERFACE                 PPP564
014300        IF  NOT KHPA-PROGRAM-FATAL-ERROR                          UCSD0144
014400            CALL WS-PPHPARP9 USING CPLNKHPA-INTERFACE             UCSD0144
014500        END-IF                                                    UCSD0144
041600     END-IF.                                                      PPP564
044721******************************************************************PPP564
044722*    Call PPHPARP2 to create the tab-delimited file from PPPHPA  *PPP564
044723*    row data.                                                   *PPP564
044724******************************************************************PPP564
044725     IF CALL-PPHPARP2                                             PPP564
044726     AND NOT KHPA-PROGRAM-FATAL-ERROR                             PPP564
044727        CALL WS-PPHPARP2 USING CPLNKHPA-INTERFACE                 PPP564
044728     END-IF.                                                      PPP564
044730                                                                  PPP564
044800 0000-EOJ.                                                        PPP564
044900     MOVE KHPA-HIGHEST-PGM-SEVERITY TO RETURN-CODE.               PPP564
045000     STOP RUN.                                                    PPP564
045100     EJECT                                                        PPP564
045200******************************************************************PPP564
045300*                Initialization                                  *PPP564
045400******************************************************************PPP564
045500 1000-PROGRAM-INITIALIZATION  SECTION.                            PPP564
045600                                                                  PPP564
045601******************************************************************PPP564
045602*    Set up report headings                                      *PPP564
045603******************************************************************PPP564
045610     MOVE 'PPP564'            TO XWHC-DISPLAY-MODULE.             PPP564
045620*                                               *-------------*   PPP564
045630                                                 COPY CPPDXWHC.   PPP564
045640*                                               *-------------*   PPP564
045642                                                                  PPP564
045700     PERFORM XDC3-GET-CURRENT-DATE.                               PPP564
045800     MOVE  XDC3-FMT-DATE               TO  STND-RPT-DATE.         PPP564
045900     MOVE  '     PAYROLL PROCESSING'   TO  STND-FUNC-AREA.        PPP564
046000     MOVE  WS-STND-PGM-ID              TO  STND-PROG-ID.          PPP564
046200     MOVE SPACES                       TO STND-RPT-HD3.           PPP564
046900     MOVE 'PPP5640'                    TO STND-RPT-ID.            PPP564
047000                                                                  PPP564
047300     MOVE SPACES         TO  MSSG-INTERFACE.                      PPP564
047400     MOVE XDC3-FMT-DATE  TO  XMSG-RUN-DATE.                       PPP564
047500     MOVE 'PPP5640'      TO  XMSG-RPT-ID.                         PPP564
047600     MOVE STND-PROG-ID   TO  XMSG-PROG-ID.                        PPP564
047700     MOVE WS-5640-TTL    TO  XMSG-RPT-TTL.                        PPP564
047800     MOVE SPACE          TO  XMSG-REFERENCE.                      PPP564
047900     MOVE ZERO           TO  XMSG-SEVERITY.                       PPP564
048000     MOVE ZEROES         TO  XMSG-NUMBER.                         PPP564
048100                                                                  PPP564
048200     PERFORM 9000-PPMSGUTL-CALL.                                  PPP564
085600                                                                  PPP564
085700******************************************************************PPP564
085800*    Initialize linkage.                                         *PPP564
085900******************************************************************PPP564
086000     INITIALIZE CPLNKHPA-INTERFACE.                               PPP564
086100                                                                  PPP564
086200******************************************************************PPP564
086300*    Edit the Run Specification Record                           *PPP564
086301******************************************************************PPP564
086302     PERFORM 1010-CHECK-SPEC-CARD.                                PPP564
086310                                                                  PPP564
086400******************************************************************PPP564
086500*    Edit the Run Specification Record                           *PPP564
086600******************************************************************PPP564
086700 1010-CHECK-SPEC-CARD        SECTION.                             PPP564
086800                                                                  PPP564
086900     OPEN INPUT SPEC-CARD-FILE.                                   PPP564
087000                                                                  PPP564
087100******************************************************************PPP564
087200*    If the Run Specification Record is missing, issue an error  *PPP564
087300*    message. If it exists, display it.                          *PPP564
087400******************************************************************PPP564
087500     READ SPEC-CARD-FILE INTO WS-SPEC-CARD-RECORD                 PPP564
087600     AT END                                                       PPP564
087700         MOVE SPACES TO XMSG-DATA                                 PPP564
087800         MOVE M56405 TO XMSG-NUMBER                               PPP564
087900         PERFORM 9000-PPMSGUTL-CALL                               PPP564
088000         SET KHPA-PROGRAM-FATAL-ERROR TO TRUE                     PPP564
088100         GO TO 1010-EXIT                                          PPP564
088200     NOT AT END                                                   PPP564
088220         IF VALID-SPEC-ID                                         PPP564
088300            MOVE WS-SPEC-CARD-RECORD TO XMSG-DATA                 PPP564
088400            MOVE M56401 TO XMSG-NUMBER                            PPP564
088500            PERFORM 9000-PPMSGUTL-CALL                            PPP564
088510         ELSE                                                     PPP564
088511            MOVE WS-SPEC-CARD-RECORD TO XMSG-DATA                 PPP564
088512            MOVE M56401 TO XMSG-NUMBER                            PPP564
088513            PERFORM 9000-PPMSGUTL-CALL                            PPP564
088514            MOVE SPACES TO XMSG-DATA                              PPP564
088515            MOVE M56405 TO XMSG-NUMBER                            PPP564
088516            PERFORM 9000-PPMSGUTL-CALL                            PPP564
088517            SET KHPA-PROGRAM-FATAL-ERROR TO TRUE                  PPP564
088520         END-IF.                                                  PPP564
088600                                                                  PPP564
088700******************************************************************PPP564
088800*    If none of the sub-calls are requested issue a warning.     *PPP564
088900******************************************************************PPP564
089000     IF  NOT CALL-PPHPARP1                                        PPP564
089010     AND NOT CALL-PPHPARP2                                        PPP564
089100     THEN                                                         PPP564
089300         MOVE M56406 TO XMSG-NUMBER                               PPP564
089400         PERFORM 9000-PPMSGUTL-CALL                               PPP564
089600     END-IF.                                                      PPP564
089700                                                                  PPP564
089800     CLOSE SPEC-CARD-FILE.                                        PPP564
089900                                                                  PPP564
090000 1010-EXIT.                                                       PPP564
090100     EXIT.                                                        PPP564
090200                                                                  PPP564
090300******************************************************************PPP564
090400*                Message Utility                                 *PPP564
090500******************************************************************PPP564
090600 9000-PPMSGUTL-CALL           SECTION.                            PPP564
090700                                                                  PPP564
090800     CALL 'PPMSGUT2' USING MSSG-INTERFACE.                        PPP564
090900     IF XMSG-HIGHEST-SEVERITY >  KHPA-HIGHEST-PGM-SEVERITY        PPP564
091000        MOVE XMSG-HIGHEST-SEVERITY TO KHPA-HIGHEST-PGM-SEVERITY   PPP564
091100     END-IF.                                                      PPP564
091200******************************************************************PPP564
091300*       XDC3 Functions                                           *PPP564
091400******************************************************************PPP564
091500 9100-XDC3-FUNCTIONS          SECTION.                            PPP564
091600     COPY CPPDXDC3.                                               PPP564
091700                                                                  PPP564
093000**************************  END OF PPP564  ***********************PPP564
