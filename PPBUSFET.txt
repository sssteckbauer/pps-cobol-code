000100**************************************************************/   36330704
000200*  PROGRAM: PPBUSFET                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPBUSH SELECT MODULE                               */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPBUSFET
001000 PROGRAM-ID. PPBUSFET.                                            PPBUSFET
001100 ENVIRONMENT DIVISION.                                            PPBUSFET
001200 DATA DIVISION.                                                   PPBUSFET
001300 WORKING-STORAGE SECTION.                                         PPBUSFET
001400*                                                                 PPBUSFET
001500     COPY CPWSXBEG.                                               PPBUSFET
001600*                                                                 PPBUSFET
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPBUSFET'.       PPBUSFET
001800*                                                                 PPBUSFET
001900     EXEC SQL                                                     PPBUSFET
002000         INCLUDE CPWSWRKP                                         PPBUSFET
002100     END-EXEC.                                                    PPBUSFET
002200*                                                                 PPBUSFET
002300 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPBUSFET
002400*                                                                 PPBUSFET
002500 01  PPPBUS-ROW EXTERNAL.                                         PPBUSFET
002600     EXEC SQL                                                     PPBUSFET
002700         INCLUDE PPPVZBUS                                         PPBUSFET
002800     END-EXEC.                                                    PPBUSFET
002900*                                                                 PPBUSFET
003000*------------------------------------------------------------*    PPBUSFET
003100*  SQL CURSOR DECLARE AREA                                   *    PPBUSFET
003200*------------------------------------------------------------*    PPBUSFET
003300     EXEC SQL                                                     PPBUSFET
003400         DECLARE PPPBUS-CURSOR                                    PPBUSFET
003500         CURSOR FOR                                               PPBUSFET
003600         SELECT *                                                 PPBUSFET
003700         FROM PPPVZBUS_BUS                                        PPBUSFET
003800         ORDER BY BUS_BUC,                                        PPBUSFET
003900                  BUS_SHC,                                        PPBUSFET
004000                  BUS_DISTRIBUTION                                PPBUSFET
004100     END-EXEC.                                                    PPBUSFET
004200*                                                                 PPBUSFET
004300*------------------------------------------------------------*    PPBUSFET
004400*  SQL DCLGEN AREA                                           *    PPBUSFET
004500*------------------------------------------------------------*    PPBUSFET
004600     EXEC SQL                                                     PPBUSFET
004700         INCLUDE SQLCA                                            PPBUSFET
004800     END-EXEC.                                                    PPBUSFET
004900*                                                                 PPBUSFET
005000 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPBUSFET
005100*                                                                 PPBUSFET
005200     COPY CPWSXEND.                                               PPBUSFET
005300*                                                                 PPBUSFET
005400 LINKAGE SECTION.                                                 PPBUSFET
005500*                                                                 PPBUSFET
005600 01  SELECT-INTERFACE-AREA.                                       PPBUSFET
005700     COPY CPWSXPIF REPLACING ==:TAG:== BY ==XSEL==.               PPBUSFET
005800*                                                                 PPBUSFET
005900 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPBUSFET
006000*                                                                 PPBUSFET
006100     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPBUSFET
006200     COPY  CPPDXWHC.                                              PPBUSFET
006300*                                                                 PPBUSFET
006400     EXEC SQL                                                     PPBUSFET
006500         INCLUDE CPPDXE99                                         PPBUSFET
006600     END-EXEC.                                                    PPBUSFET
006700*                                                                 PPBUSFET
006800     COPY CPPDSELP.                                               PPBUSFET
006900*                                                                 PPBUSFET
007000 2000-OPEN-CURSOR SECTION.                                        PPBUSFET
007100     IF WS-CURSOR-OPEN                                            PPBUSFET
007200        SET XSEL-INVALID-REQUEST TO TRUE                          PPBUSFET
007300     ELSE                                                         PPBUSFET
007400        EXEC SQL                                                  PPBUSFET
007500            OPEN PPPBUS-CURSOR                                    PPBUSFET
007600        END-EXEC                                                  PPBUSFET
007700        SET WS-CURSOR-OPEN TO TRUE                                PPBUSFET
007800     END-IF.                                                      PPBUSFET
007900 2000-EXIT.                                                       PPBUSFET
008000     EXIT.                                                        PPBUSFET
008100*                                                                 PPBUSFET
008200 3000-FETCH-ROW SECTION.                                          PPBUSFET
008300*                                                                 PPBUSFET
008400     IF NOT WS-CURSOR-OPEN                                        PPBUSFET
008500        PERFORM 2000-OPEN-CURSOR                                  PPBUSFET
008600     END-IF.                                                      PPBUSFET
008700*                                                                 PPBUSFET
008800     IF NOT XSEL-OK                                               PPBUSFET
008900        GO TO 3000-EXIT                                           PPBUSFET
009000     END-IF.                                                      PPBUSFET
009100*                                                                 PPBUSFET
009200     EXEC SQL                                                     PPBUSFET
009300        FETCH PPPBUS-CURSOR                                       PPBUSFET
009400        INTO :PPPBUS-ROW                                          PPBUSFET
009500     END-EXEC.                                                    PPBUSFET
009600*                                                                 PPBUSFET
009700     IF SQLCODE = +100                                            PPBUSFET
009800        INITIALIZE XSEL-RETURN-KEY                                PPBUSFET
009900                   PPPBUS-ROW                                     PPBUSFET
010000        SET XSEL-INVALID-KEY TO TRUE                              PPBUSFET
010100        GO TO 3000-EXIT                                           PPBUSFET
010200     END-IF.                                                      PPBUSFET
010300*                                                                 PPBUSFET
010400     IF SQLCODE = +0                                              PPBUSFET
010500        SET XSEL-OK TO TRUE                                       PPBUSFET
010600        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPBUSFET
010700        GO TO 3000-EXIT                                           PPBUSFET
010800     END-IF.                                                      PPBUSFET
010900 3000-EXIT.                                                       PPBUSFET
011000     EXIT.                                                        PPBUSFET
011100*                                                                 PPBUSFET
011200 4000-SELECT SECTION.                                             PPBUSFET
011300*                                                                 PPBUSFET
011400     MOVE 'SELECT PPPBUS ROW' TO DB2MSG-TAG.                      PPBUSFET
011500*                                                                 PPBUSFET
011600     EXEC SQL                                                     PPBUSFET
011700         SELECT *                                                 PPBUSFET
011800         INTO :PPPBUS-ROW                                         PPBUSFET
011900         FROM PPPVZBUS_BUS                                        PPBUSFET
012000         WHERE BUS_BUC            = :WS-PPPBUS-BUC                PPBUSFET
012100         AND   BUS_SHC            = :WS-PPPBUS-SHC                PPBUSFET
012200         AND   BUS_DISTRIBUTION   = :WS-PPPBUS-DIST               PPBUSFET
012300     END-EXEC.                                                    PPBUSFET
012400*                                                                 PPBUSFET
012500     IF SQLCODE = +100                                            PPBUSFET
012600        INITIALIZE XSEL-RETURN-KEY                                PPBUSFET
012700                   PPPBUS-ROW                                     PPBUSFET
012800        SET XSEL-INVALID-KEY TO TRUE                              PPBUSFET
012900        GO TO 4000-EXIT                                           PPBUSFET
013000     END-IF.                                                      PPBUSFET
013100*                                                                 PPBUSFET
013200     IF SQLCODE = +0                                              PPBUSFET
013300        MOVE WS-CONTROL-KEY    TO XSEL-RETURN-CTL-KEY             PPBUSFET
013400        SET XSEL-OK TO TRUE                                       PPBUSFET
013500        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPBUSFET
013600        GO TO 4000-EXIT                                           PPBUSFET
013700     END-IF.                                                      PPBUSFET
013800*                                                                 PPBUSFET
013900 4000-EXIT.                                                       PPBUSFET
014000     EXIT.                                                        PPBUSFET
014100*                                                                 PPBUSFET
014200 5000-CLOSE-CURSOR SECTION.                                       PPBUSFET
014300     IF WS-CURSOR-CLOSED                                          PPBUSFET
014400        SET XSEL-INVALID-REQUEST TO TRUE                          PPBUSFET
014500     ELSE                                                         PPBUSFET
014600        EXEC SQL                                                  PPBUSFET
014700            CLOSE PPPBUS-CURSOR                                   PPBUSFET
014800        END-EXEC                                                  PPBUSFET
014900        SET WS-CURSOR-CLOSED TO TRUE                              PPBUSFET
015000     END-IF.                                                      PPBUSFET
015100 5000-EXIT.                                                       PPBUSFET
015200     EXIT.                                                        PPBUSFET
015300*                                                                 PPBUSFET
015400*999999-SQL-ERROR SECTION.                                        PPBUSFET
015500     COPY CPPDXP99.                                               PPBUSFET
015600     SET XSEL-DB2-ERROR TO TRUE.                                  PPBUSFET
015700     MOVE SQLCODE TO XSEL-DB2-MSG-NUMBER.                         PPBUSFET
015800     GOBACK.                                                      PPBUSFET
015900 999999-EXIT.                                                     PPBUSFET
016000     EXIT.                                                        PPBUSFET
016100*                                                                 PPBUSFET
