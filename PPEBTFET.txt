000000**************************************************************/   12680775
000001*  PROGRAM: PPEBTFET                                         */   12680775
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____1268____  */   12680775
000003*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___06/01/93__  */   12680775
000004*  DESCRIPTION:                                              */   12680775
000005*    FETCH DATA ELEMENT FROM THE EBT TABLE                   */   12680775
000006*                                                            */   12680775
000007**************************************************************/   12680775
004600 IDENTIFICATION DIVISION.                                         PPEBTFET
004700 PROGRAM-ID. PPEBTFET.                                            PPEBTFET
004800 AUTHOR. UCOP.                                                    PPEBTFET
004900 DATE-WRITTEN. MAY 1993.                                          PPEBTFET
005000 DATE-COMPILED.                                                   PPEBTFET
005100*REMARKS.                                                         PPEBTFET
005200******************************************************************PPEBTFET
005300*                                                                *PPEBTFET
005400*  THIS PROGRAM DRIVES THE RETRIEVAL OF EMPLOYEE DATA BASE       *PPEBTFET
005500* DATA ELEMENTS, BY DATA ELEMENT NUMBER, FROM EBT-TABLE ROWS.    *PPEBTFET
005600* THIS PROGRAM IS CALLED BY PPEDBFET.                            *PPEBTFET
005700*                                                                *PPEBTFET
005800*  THE DETAILS OF THIS PROGRAMS FUNCTION ARE DESCRIBED IN THE    *PPEBTFET
005900* PROCEDURE DIVISION COPYMEMBER CPPDEFET. ADDITIONAL CODE IS     *PPEBTFET
006000* HARCODED IN THIS SOURCE TO PROVIDE THE NECESSARY MOVEMENT OF   *PPEBTFET
006100* EBT-TABLE ROW COLUMNS TO THE EDB FETCH COMPLEX DATA RETURN     *PPEBTFET
006200* AREA.                                                          *PPEBTFET
006300*                                                                *PPEBTFET
006400*  AS SHOULD BE OBVIOUS, THIS PROGRAM USES COPY CODE FOR THE     *PPEBTFET
006500* MAJORITY OF THE PROCEDURE DIVISION. THIS COPY CODE ADDRESSES   *PPEBTFET
006600* THE ROOT FUNCTIONS WHICH ARE COMMON TO THE PPXXXFET PROGRAM    *PPEBTFET
006700* SERIES.                                                        *PPEBTFET
006800*                                                                *PPEBTFET
006900******************************************************************PPEBTFET
007000     EJECT                                                        PPEBTFET
007100 ENVIRONMENT DIVISION.                                            PPEBTFET
007200 CONFIGURATION SECTION.                                           PPEBTFET
007300 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPEBTFET
007400 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPEBTFET
007500      EJECT                                                       PPEBTFET
007600******************************************************************PPEBTFET
007700*                                                                *PPEBTFET
007800*                D A T A  D I V I S I O N                        *PPEBTFET
007900*                                                                *PPEBTFET
008000******************************************************************PPEBTFET
008100 DATA DIVISION.                                                   PPEBTFET
008200 WORKING-STORAGE SECTION.                                         PPEBTFET
008300 01  MISCELLANEOUS-WS.                                            PPEBTFET
008400******************************************************************PPEBTFET
008500*                                                                *PPEBTFET
008600* NOTE:                                                          *PPEBTFET
008700*                                                                *PPEBTFET
008800*   DATA NAMES A-STND-PROG-ID AND PPXXXUTL-NAME                  *PPEBTFET
008900*  ARE REFERENCED IN COPY CODE CPPDEFET. A-STND-PROG-ID IS USED  *PPEBTFET
009000*  IN DIAGNOSTIC REFERENCES, WHILE PPXXXUTL-NAME                 *PPEBTFET
009100*  IS USED IN DYNAMIC CALLS TO THE APPROPRIATE TABLE READ        *PPEBTFET
009200*  UTILITY.                                                      *PPEBTFET
009300*                                                                *PPEBTFET
009400******************************************************************PPEBTFET
009500     05  A-STND-PROG-ID      PIC X(08) VALUE 'PPEBTFET'.          PPEBTFET
009600     05  PPXXXUTL-NAME       PIC X(08) VALUE 'PPEBTUTL'.          PPEBTFET
009700     05  WS-INIT-DATE        PIC X(10) VALUE '01/01/0001'.        PPEBTFET
009800     05  WS-ISO-INIT-DATE    PIC X(10) VALUE '0001-01-01'.        PPEBTFET
009900     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPEBTFET
010000         88  FIRST-TIME                VALUE 'Y'.                 PPEBTFET
010100         88  NOT-FIRST-TIME            VALUE 'N'.                 PPEBTFET
010200     05  INIT-VALUE-SWITCH   PIC X(01) VALUE 'Y'.                 PPEBTFET
010300         88  INIT-VALUE                VALUE 'Y'.                 PPEBTFET
010400         88  NON-INIT-VALUE            VALUE 'N'.                 PPEBTFET
010500     05  MULTIPLE-OCCURR-SW  PIC X(01) VALUE 'N'.                 PPEBTFET
010600         88  MULTIPLE-OCCURRENCE       VALUE 'Y'.                 PPEBTFET
010700         88  SINGLE-OCCURRENCE         VALUE 'N'.                 PPEBTFET
010800     05  DATE-VALUE-SWITCH   PIC X(01) VALUE 'N'.                 PPEBTFET
010900         88  NO-DATE-VALUE             VALUE 'N'.                 PPEBTFET
011000         88  ISO-DATE-VALUE            VALUE 'I'.                 PPEBTFET
011100         88  USA-DATE-VALUE            VALUE 'U'.                 PPEBTFET
011200         88  DATE-VALUE                VALUES 'I' 'U'.            PPEBTFET
011300     05  DATE-HOLD-SWITCH    PIC X(01) VALUE 'N'.                 PPEBTFET
011400     05  WS-DATA-TYPE        PIC X(01) VALUE 'A'.                 PPEBTFET
011500         88  WS-DATA-TYPE-AN           VALUE 'A'.                 PPEBTFET
011600         88  WS-DATA-TYPE-NUM          VALUE 'N'.                 PPEBTFET
011700         88  WS-DATA-TYPE-DATE         VALUE 'D'.                 PPEBTFET
011800     05  WS-EMPLOYEE-ID      PIC X(09) VALUE SPACES.              PPEBTFET
011900     05  WS-DATA-ELEM-NO     PIC 9(04) VALUE ZEROES.              PPEBTFET
012000     05  WS-OCCURRENCE-KEY.                                       PPEBTFET
012100         10  FILLER          PIC X(18) VALUE SPACES.              PPEBTFET
012200     05  WS-IX1              PIC S9(4) COMP VALUE ZERO.           PPEBTFET
012300     05  WS-IX2              PIC S9(4) COMP VALUE ZERO.           PPEBTFET
012400     05  WS-IX3              PIC S9(4) COMP VALUE ZERO.           PPEBTFET
012500     05  WS-IX4              PIC S9(4) COMP VALUE ZERO.           PPEBTFET
012600     05  WS-IX5              PIC S9(4) COMP VALUE ZERO.           PPEBTFET
012700     05  WS-IX9              PIC S9(4) COMP VALUE ZERO.           PPEBTFET
012800     SKIP3                                                        PPEBTFET
012900 01  WS-NUMBER-WORK-AREA.                                         36090574
013000     05  WS-AREA1.                                                36090574
013100         10  WS-TEMP-NUMBER       PIC 9(09)V9(09).                36090574
013200     05  FILLER                   REDEFINES WS-AREA1.             36090574
013300         10  FILLER               PIC X(08).                      36090574
013400         10  WS-NUM-1-X           PIC X(01).                      36090574
013500         10  FILLER               PIC X(09).                      36090574
013600     05  FILLER                   REDEFINES WS-AREA1.             36090574
013700         10  FILLER               PIC X(07).                      36090574
013800         10  WS-NUM-2-X           PIC X(02).                      36090574
013900         10  FILLER               PIC X(09).                      36090574
014000     05  FILLER                   REDEFINES WS-AREA1.             36090574
014100         10  FILLER               PIC X(06).                      36090574
014200         10  WS-NUM-3-X           PIC X(03).                      36090574
014300         10  FILLER               PIC X(09).                      36090574
014400     05  FILLER                   REDEFINES WS-AREA1.             36090574
014500         10  FILLER               PIC X(05).                      36090574
014600         10  WS-NUM-4-X           PIC X(04).                      36090574
014700         10  FILLER               PIC X(09).                      36090574
014800     05  FILLER                   REDEFINES WS-AREA1.             36090574
014900         10  FILLER               PIC X(04).                      36090574
015000         10  WS-NUM-5-X           PIC X(05).                      36090574
015100         10  FILLER               PIC X(09).                      36090574
015200     05  FILLER                   REDEFINES WS-AREA1.             36090574
015300         10  FILLER               PIC X(03).                      36090574
015400         10  WS-NUM-6-X           PIC X(06).                      36090574
015500         10  FILLER               PIC X(09).                      36090574
015600     05  FILLER                   REDEFINES WS-AREA1.             36090574
015700         10  FILLER               PIC X(02).                      36090574
015800         10  WS-NUM-7-X           PIC X(07).                      36090574
015900         10  FILLER               PIC X(09).                      36090574
016000     05  FILLER                   REDEFINES WS-AREA1.             36090574
016100         10  FILLER               PIC X(01).                      36090574
016200         10  WS-NUM-8-X           PIC X(08).                      36090574
016300         10  FILLER               PIC X(09).                      36090574
016400     05  FILLER                   REDEFINES WS-AREA1.             36090574
016500         10  WS-NUM-9-X           PIC X(09).                      36090574
016600         10  FILLER               PIC X(09).                      36090574
016700 01  DATE-CONVERSION-WORK-AREAS.                                  PPEBTFET
016800                                    COPY CPWSXDC2.                PPEBTFET
016900     EJECT                                                        PPEBTFET
017000 01  TEMP-DATA-AREA.                                              PPEBTFET
017100                                    COPY CPWSXMMM.                PPEBTFET
017200     EJECT                                                        PPEBTFET
017300******************************************************************PPEBTFET
017400*                                                                *PPEBTFET
017500*  EBT-ROW IS THE ROW THAT WILL BE PASSED AROUND FOR USE BY      *PPEBTFET
017600* HIGHER LEVEL PROGRAMS. IT IS DEFINED AS AN EXTERNAL DATA AREA  *PPEBTFET
017700* TO ALLOW HIGHER LEVEL PROGRAMS TO AVOID PASSING THE ROW        *PPEBTFET
017800* THROUGH UNNECESSARY CALLING CHAINS.                            *PPEBTFET
017900*                                                                *PPEBTFET
018000*  EBT-ROW-HOLD IS THE PURE ROW AS READ FROM THE EBT-TABLE. THIS *PPEBTFET
018100* ROW IS RETRIEVED BY THE CORRESPONDING PPXXXUPD MODULE, AND IS  *PPEBTFET
018200* REFERNCED IN THIS PPXXXFET MODULE FOR THE SOLE PURPOSE OF      *PPEBTFET
018300* FILLING THE CHANGE-RETURN-DATA ARRAY.                          *PPEBTFET
018400*                                                                *PPEBTFET
018500*  EBT-ROW-HOLD1 IS THE ROW AS SET ASIDE, AFTER SOME UPDATE      *PPEBTFET
018600* ACTIVITY, BY THE EDB UPDATE COMPLEX  TO ALLOW THE GENERATION   *PPEBTFET
018700* OF THE "FIRST" CHANGE ENTRY IN CHANGE RECORDS.                 *PPEBTFET
018800*                                                                *PPEBTFET
018900******************************************************************PPEBTFET
019000 01  EBT-ROW        EXTERNAL.                                     PPEBTFET
019100                                    COPY CPWSREBT.                PPEBTFET
019200     EJECT                                                        PPEBTFET
019300 01  EBT-ROW-HOLD   EXTERNAL.                                     PPEBTFET
019400                                    COPY CPWSREBT.                PPEBTFET
019500     EJECT                                                        PPEBTFET
019600 01  EBT-ROW-HOLD1  EXTERNAL.                                     PPEBTFET
019700                                    COPY CPWSREBT.                PPEBTFET
019800     EJECT                                                        PPEBTFET
019900 01  PPEDBFET-REQUEST-ARRAY     EXTERNAL.                         PPEBTFET
020000                                    COPY CPWSEREQ.                PPEBTFET
020100     EJECT                                                        PPEBTFET
020200 01  PPEDBFET-D-E-RETURN-ARRAY  EXTERNAL.                         PPEBTFET
020300                                    COPY CPWSERET.                PPEBTFET
020400     EJECT                                                        PPEBTFET
020500 01  PPEDBFET-CHANGE-RETURN-ARRAY EXTERNAL.                       PPEBTFET
020600                                    COPY CPWSECHG.                PPEBTFET
020700     EJECT                                                        PPEBTFET
020800 01  PPEDBFET-RETURN-POINTERS     EXTERNAL.                       PPEBTFET
020900                                    COPY CPWSEPTR.                PPEBTFET
021000     EJECT                                                        PPEBTFET
021100 01  PPEBTUTL-INTERFACE.                                          PPEBTFET
021200                                    COPY CPLNKEBT.                PPEBTFET
021300     EJECT                                                        PPEBTFET
021400 01  XWHC-COMPILE-WORK-AREA.                                      PPEBTFET
021500                                    COPY CPWSXWHC.                PPEBTFET
021600     EJECT                                                        PPEBTFET
021700 01  PPXXXFET-INTERFACE  EXTERNAL.                                PPEBTFET
021800                                    COPY CPLNFXXX.                PPEBTFET
021900     EJECT                                                        PPEBTFET
022000 PROCEDURE DIVISION.                                              PPEBTFET
022100*                                                                 PPEBTFET
022200******************************************************************PPEBTFET
022300*            P R O C E D U R E  D I V I S I O N                  *PPEBTFET
022400******************************************************************PPEBTFET
022500*                                                                 PPEBTFET
022600******************************************************************PPEBTFET
022700*                                                                *PPEBTFET
022800*  THE PROCEDURE DIVISION OF THIS PROGRAM IS CONTAINED PRIMARILY *PPEBTFET
022900* IN COPY MEMBER CPPDEFET. HERE THE GENERIC DATA-NAMES IN        *PPEBTFET
023000* CPPDEFET ARE REPLACED WITH THE SPECIFIC DATA-NAMES OF THE      *PPEBTFET
023100* WORKING STORAGE TABLE ROWS AND THE PPXXXUTL MODULE INTERFACE.  *PPEBTFET
023200*                                                                *PPEBTFET
023300*                                                                *PPEBTFET
023400******************************************************************PPEBTFET
023500                                    COPY CPPDEFET                 PPEBTFET
023600        REPLACING PP000UTL-INTERFACE   BY PPEBTUTL-INTERFACE      PPEBTFET
023700                  PP000UTL-ERROR       BY PPEBTUTL-ERROR          PPEBTFET
023800                  PP000UTL-EMPLOYEE-ID BY PPEBTUTL-EMPLOYEE-ID    PPEBTFET
023900                  000-ROW              BY EBT-ROW.                PPEBTFET
024000     EJECT                                                        PPEBTFET
024100 7900-FIRST-TIME SECTION.                                         PPEBTFET
024200     SKIP1                                                        PPEBTFET
024300******************************************************************PPEBTFET
024400*  THIS SECTION IS REFERENCED IN PROCEDURE DIVISION COPYMEMBER   *PPEBTFET
024500* CPPDEFET AND PRESENTS THE MODULE NAME, COMPILE DATE/TIME       *PPEBTFET
024600* ON THE DISPLAY SYSOUT ONLY WHEN THIS MODULE IS CALLED FOR THE  *PPEBTFET
024700* FIRST TIME.                                                    *PPEBTFET
024800******************************************************************PPEBTFET
024900     SKIP1                                                        PPEBTFET
025000     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPEBTFET
025100     SKIP1                                                        PPEBTFET
025200                                    COPY CPPDXWHC.                PPEBTFET
025300     EJECT                                                        PPEBTFET
025400 9000-RESTORE-INDEXES SECTION.                                    PPEBTFET
025500     SKIP1                                                        PPEBTFET
025600******************************************************************PPEBTFET
025700*                                                                *PPEBTFET
025800* THIS SECTION IS INCLUDED IN THIS MODULE TO RESOLVE REFERENCES  *PPEBTFET
025900* IN CPPDEFET. SINCE THIS MODULE WORKS WITH A SINGLE DB2 EDB     *PPEBTFET
026000* EBT ROW, THERE ARE NO INDEXES THAT NEED TO BE RESTORED WHEN    *PPEBTFET
026100* PROCESSING THE "MORE-DATA" CONDITION. THEREFORE, THIS SECTION  *PPEBTFET
026200* PERFORMS THE "DUMMY" ACTION OF INITIALIZING WS-IX5.            *PPEBTFET
026300*                                                                *PPEBTFET
026400******************************************************************PPEBTFET
026500     SKIP1                                                        PPEBTFET
026600     INITIALIZE WS-IX5.                                           PPEBTFET
026700     EJECT                                                        PPEBTFET
026800 9100-EVALUATE-DATA-ELEMENT SECTION.                              PPEBTFET
026900     SKIP1                                                        PPEBTFET
027000******************************************************************PPEBTFET
027100* THIS SECTION TAKES THE DATA ELEMENT NUMBER FROM THE REQUEST    *PPEBTFET
027200* LIST AND PLACES THE APPROPRIATE EBT TABLE ROW COLUMN VALUE(S)  *PPEBTFET
027300* INTO A WORKING-STORAGE DATA AREA. THE VALUES WILL BE TESTED    *PPEBTFET
027400* BY LOGIC IN PROCEDURE DIVISION COPYMEMBER CPPDEFET TO          *PPEBTFET
027500* DETERMINE WHERE IN THE DATA-RETURN ARRAY THE VALUE(S) WILL     *PPEBTFET
027600* BE PLACED.                                                     *PPEBTFET
027700******************************************************************PPEBTFET
027800     SKIP1                                                        PPEBTFET
027900     SET NOT-DB2X-DET-NUMERIC TO TRUE.                            PPEBTFET
028000     SKIP1                                                        PPEBTFET
028100     EVALUATE WS-DATA-ELEM-NO                                     PPEBTFET
028200         WHEN '0644'                                              PPEBTFET
028300             MOVE    UCI-CODE           OF EBT-ROW       TO       PPEBTFET
028400                     TEMP-DATA-VALUE                              PPEBTFET
028500              IF PPXXXFET-CHANGE-REQUEST                          PPEBTFET
028600                  MOVE    UCI-CODE           OF EBT-ROW-HOLD  TO  PPEBTFET
028700                          ORIG-DATA-VALUE                         PPEBTFET
028800                  MOVE    UCI-CODE           OF EBT-ROW-HOLD1 TO  PPEBTFET
028900                          CHG1-DATA-VALUE                         PPEBTFET
029000              END-IF                                              PPEBTFET
029100         WHEN '0645'                                              PPEBTFET
029200             MOVE    UCI-BEGIN-DATE     OF EBT-ROW       TO       PPEBTFET
029300                     TEMP-DATA-VALUE                              PPEBTFET
029400              IF PPXXXFET-CHANGE-REQUEST                          PPEBTFET
029500                  MOVE    UCI-BEGIN-DATE     OF EBT-ROW-HOLD  TO  PPEBTFET
029600                          ORIG-DATA-VALUE                         PPEBTFET
029700                  MOVE    UCI-BEGIN-DATE     OF EBT-ROW-HOLD1 TO  PPEBTFET
029800                          CHG1-DATA-VALUE                         PPEBTFET
029900              END-IF                                              PPEBTFET
030000         WHEN '0646'                                              PPEBTFET
030100             MOVE    UCI-END-DATE       OF EBT-ROW       TO       PPEBTFET
030200                     TEMP-DATA-VALUE                              PPEBTFET
030300              IF PPXXXFET-CHANGE-REQUEST                          PPEBTFET
030400                  MOVE    UCI-END-DATE       OF EBT-ROW-HOLD  TO  PPEBTFET
030500                          ORIG-DATA-VALUE                         PPEBTFET
030600                  MOVE    UCI-END-DATE       OF EBT-ROW-HOLD1 TO  PPEBTFET
030700                          CHG1-DATA-VALUE                         PPEBTFET
030800              END-IF                                              PPEBTFET
101700         WHEN OTHER                                               PPEBTFET
101800              IF PPXXXFET-DIAGNOSTICS-ON                          PPEBTFET
101900                  DISPLAY A-STND-PROG-ID                          PPEBTFET
102000                          ': UNRECOGNIZED DATA ELEMENT NUMBER ('  PPEBTFET
102100                          WS-DATA-ELEM-NO                         PPEBTFET
102200                          ')'                                     PPEBTFET
102300              END-IF                                              PPEBTFET
102400              SET PPXXXFET-DATA-ELEM-NO-ERROR TO TRUE             PPEBTFET
102500              MOVE WS-DATA-ELEM-NO TO PPXXXFET-D-E-IN-ERROR       PPEBTFET
102600     END-EVALUATE.                                                PPEBTFET
102700     SKIP3                                                        PPEBTFET
102800************************   END OF PROGRAM ************************PPEBTFET
