000100**************************************************************/   30871401
000200*  PROGRAM: PPCTT39                                          */   30871401
000300*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000400*  NAME:___SRS___________ CREATION DATE:      ___03/20/02__  */   30871401
000500*  DESCRIPTION:                                              */   30871401
000600*  PPPDES TABLE TRANSACTION ASSEMBLY MODULE                  */   30871401
000700**************************************************************/   30871401
000800 IDENTIFICATION DIVISION.                                         PPCTT39
000900 PROGRAM-ID. PPCTT39.                                             PPCTT39
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT39
001100 DATE-COMPILED.                                                   PPCTT39
001200 DATA DIVISION.                                                   PPCTT39
001300 FILE SECTION.                                                    PPCTT39
001400 WORKING-STORAGE SECTION.                                         PPCTT39
001500                                                                  PPCTT39
001600 01  WA-WORK-VARIABLES.                                           PPCTT39
001700     05  WA-READ-ERROR.                                           PPCTT39
001800         10 FILLER                     PIC X(37) VALUE            PPCTT39
001900                 'PPCTLTRD ERROR ON TRANS CALL: RETURN='.         PPCTT39
002000         10 WA-RTN-CODE                PIC XX.                    PPCTT39
002100         10 FILLER                     PIC X(07) VALUE            PPCTT39
002200                 ' COUNT='.                                       PPCTT39
002300         10 WA-WORK-Z5                 PIC ZZ,ZZ9.                PPCTT39
002400 01  WB-PROGRAM-CONSTANTS.                                        PPCTT39
002500     05  WB-TABLE-NO                   PIC X(2) VALUE '39'.       PPCTT39
002600 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT39
002700     05  WC-ERR-IX                     PIC S9(04) VALUE ZERO.     PPCTT39
002800 01  WF-PROGRAM-FLAGS.                                            PPCTT39
002900     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT39
003000         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT39
003100         88  WF-ACCEPT-TRANSACTION              VALUES 00 THRU 04.PPCTT39
003200         88  WF-REJECT-TRANSACTION              VALUES 05 THRU 99.PPCTT39
003300 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT39
003400     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT39
003500     05  MCT003                     PIC X(05) VALUE 'CT003'.      PPCTT39
003600     05  MCT020                     PIC X(05) VALUE 'CT020'.      PPCTT39
003700     05  MCT021                     PIC X(05) VALUE 'CT021'.      PPCTT39
003800     05  MCT022                     PIC X(05) VALUE 'CT022'.      PPCTT39
003900     05  MCT023                     PIC X(05) VALUE 'CT023'.      PPCTT39
004000 01  DATA-ELEM-SCRN-DES-TRANS.                                    PPCTT39
004100     05  DEST-ACTION-CODE           PIC X.                        PPCTT39
004200         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT39
004300         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT39
004400         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT39
004500         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT39
004600     05  FILLER                     PIC XXXX.                     PPCTT39
004700     05  DEST-DATABASE-ID           PIC XXX.                      PPCTT39
004800     05  DEST-ELEM-NO               PIC XXXX.                     PPCTT39
004900     05  DEST-SCREEN-ID             PIC XXXX.                     PPCTT39
005000     05  DEST-PROTECT-IND           PIC X.                        PPCTT39
005100     05  FILLER                     PIC X(63).                    PPCTT39
005200                                                                  PPCTT39
005300 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT39
005400 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT39
005500 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT39
005600 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT39
005700 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT39
005800 01  DATA-ELEM-SCRN-DES-TABLE-INPUT.   COPY CPCTDESI.             PPCTT39
005900 01  DATA-ELEM-SCRN-DES-TABLE-DATA.                               PPCTT39
006000     EXEC SQL                                                     PPCTT39
006100         INCLUDE PPPVZDES                                         PPCTT39
006200     END-EXEC.                                                    PPCTT39
006300                                                                  PPCTT39
006400 LINKAGE SECTION.                                                 PPCTT39
006500                                                                  PPCTT39
006600 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT39
006700                                                                  PPCTT39
006800 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE.           PPCTT39
006900                                                                  PPCTT39
007000 0000-MAIN SECTION.                                               PPCTT39
007100                                                                  PPCTT39
007200     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT39
007300     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT39
007400       UNTIL CSRI-TABLE-TRANS-END.                                PPCTT39
007500                                                                  PPCTT39
007600 0099-END.                                                        PPCTT39
007700                                                                  PPCTT39
007800     GOBACK.                                                      PPCTT39
007900                                                                  PPCTT39
008000 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT39
008100                                                                  PPCTT39
008200     MOVE 'PPCTT39'             TO XWHC-DISPLAY-MODULE.           PPCTT39
008300     COPY CPPDXWHC.                                               PPCTT39
008400     SET CTTI-NORMAL            TO TRUE.                          PPCTT39
008500     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTT39
008600     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTT39
008700                                   CTRI-TRANSACTION               PPCTT39
008800                                   CTRI-TRANSACTION-DISPO         PPCTT39
008900                                   CTRI-TEXT-LINE                 PPCTT39
009000                                   CTRI-MESSAGE-NUMBER.           PPCTT39
009100     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTT39
009200                                   CTTI-ERROR-SEVERITY.           PPCTT39
009300     MOVE WB-TABLE-NO           TO CSRI-CALL-TYPE-NUM.            PPCTT39
009400     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT39
009500                                                                  PPCTT39
009600 1099-INITIALIZATION-EXIT.                                        PPCTT39
009700     EXIT.                                                        PPCTT39
009800                                                                  PPCTT39
009900 2000-PROCESS-TRANSACTIONS SECTION.                               PPCTT39
010000                                                                  PPCTT39
010100     MOVE SPACES                TO DATA-ELEM-SCRN-DES-TABLE-INPUT.PPCTT39
010200     MOVE DATA-ELEM-SCRN-DES-TRANS TO CTRI-TRANSACTION.           PPCTT39
010300     MOVE DEST-DATABASE-ID         TO DESI-DATABASE-ID.           PPCTT39
010400     MOVE DEST-ELEM-NO             TO DESI-ELEM-NO.               PPCTT39
010500     MOVE DEST-SCREEN-ID           TO DESI-SCREEN-ID.             PPCTT39
010600     MOVE DEST-PROTECT-IND         TO DESI-PROTECT-IND.           PPCTT39
010700     MOVE DEST-ACTION-CODE         TO KCTE-ACTION.                PPCTT39
010800     MOVE CTTI-ACTION-DATE         TO KCTE-LAST-ACTION-DT.        PPCTT39
010900     PERFORM 3000-UPDATE-TRANS.                                   PPCTT39
011000     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT39
011100                                                                  PPCTT39
011200 2099-EXIT.                                                       PPCTT39
011300     EXIT.                                                        PPCTT39
011400                                                                  PPCTT39
011500                                                                  PPCTT39
011600 3000-UPDATE-TRANS SECTION.                                       PPCTT39
011700                                                                  PPCTT39
011800     SET WF-NO-ERRORS TO TRUE                                     PPCTT39
011900     CALL 'PPCTDESE' USING CONTROL-TABLE-EDIT-INTERFACE           PPCTT39
012000                           DATA-ELEM-SCRN-DES-TABLE-INPUT         PPCTT39
012100                           DATA-ELEM-SCRN-DES-TABLE-DATA.         PPCTT39
012200     IF KCTE-ERR > ZERO                                           PPCTT39
012300        PERFORM 3100-PRINT-ERROR VARYING WC-ERR-IX FROM 1 BY 1    PPCTT39
012400          UNTIL WC-ERR-IX > KCTE-ERR                              PPCTT39
012500     END-IF.                                                      PPCTT39
012600     IF KCTE-STATUS-NOT-COMPLETED                                 PPCTT39
012700        MOVE MCT003 TO CTRI-MESSAGE-NUMBER                        PPCTT39
012800        SET WF-REJECT-TRANSACTION TO TRUE                         PPCTT39
012900     END-IF.                                                      PPCTT39
013000     IF WF-ACCEPT-TRANSACTION                                     PPCTT39
013100        MOVE 'TRANSACTION ACCEPTED' TO CTRI-TEXT-DISPO            PPCTT39
013200        ADD 1 TO CTTI-TRANS-ACCEPTED                              PPCTT39
013300        PERFORM 3200-UPDATE-DB2-TABLE                             PPCTT39
013400     ELSE                                                         PPCTT39
013500        MOVE 'TRANSACTION REJECTED' TO CTRI-TEXT-DISPO            PPCTT39
013600        ADD 1 TO CTTI-TRANS-REJECTED                              PPCTT39
013700     END-IF.                                                      PPCTT39
013800     PERFORM 9100-CALL-PPCTLRPT                                   PPCTT39
013900     MOVE 2 TO CTRI-SPACING-OVERRIDE.                             PPCTT39
014000                                                                  PPCTT39
014100 3099-EXIT.                                                       PPCTT39
014200     EXIT.                                                        PPCTT39
014300                                                                  PPCTT39
014400 3100-PRINT-ERROR SECTION.                                        PPCTT39
014500                                                                  PPCTT39
014600     MOVE KCTE-ERR-NO (WC-ERR-IX) TO CTRI-MESSAGE-NUMBER.         PPCTT39
014700     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT39
014800     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT39
014900         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT39
015000     END-IF.                                                      PPCTT39
015100                                                                  PPCTT39
015200 3199-EXIT.                                                       PPCTT39
015300     EXIT.                                                        PPCTT39
015400                                                                  PPCTT39
015500 3200-UPDATE-DB2-TABLE SECTION.                                   PPCTT39
015600                                                                  PPCTT39
015700     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT39
015800     CALL 'PPCTDESU' USING CONTROL-TABLE-UPDT-INTERFACE           PPCTT39
015900                           DATA-ELEM-SCRN-DES-TABLE-DATA          PPCTT39
016000     IF KCTU-STATUS-OK                                            PPCTT39
016100        EVALUATE TRUE                                             PPCTT39
016200          WHEN KCTU-ACTION-ADD                                    PPCTT39
016300            ADD 1 TO CTTI-ENTRIES-ADDED                           PPCTT39
016400          WHEN KCTU-ACTION-CHANGE                                 PPCTT39
016500            ADD 1 TO CTTI-ENTRIES-UPDATED                         PPCTT39
016600          WHEN KCTU-ACTION-DELETE                                 PPCTT39
016700            ADD 1 TO CTTI-ENTRIES-DELETED                         PPCTT39
016800          WHEN OTHER                                              PPCTT39
016900            GO TO 3300-BAD-UPDATE-CALL                            PPCTT39
017000        END-EVALUATE                                              PPCTT39
017100     ELSE                                                         PPCTT39
017200        GO TO 3300-BAD-UPDATE-CALL                                PPCTT39
017300     END-IF.                                                      PPCTT39
017400                                                                  PPCTT39
017500 3299-EXIT.                                                       PPCTT39
017600     EXIT.                                                        PPCTT39
017700                                                                  PPCTT39
017800 3300-BAD-UPDATE-CALL SECTION.                                    PPCTT39
017900                                                                  PPCTT39
018000     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT39
018100     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT39
018200     STRING 'PPCTDESU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT39
018300            '" STATUS WAS "'               KCTU-STATUS            PPCTT39
018400            '" COUNT='                     WA-WORK-Z5             PPCTT39
018500            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT39
018600     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT39
018700     GO TO 9999-ABEND.                                            PPCTT39
018800                                                                  PPCTT39
018900 3399-EXIT.                                                       PPCTT39
019000     EXIT.                                                        PPCTT39
019100                                                                  PPCTT39
019200 9100-CALL-PPCTLRPT SECTION.                                      PPCTT39
019300                                                                  PPCTT39
019400     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT39
019500     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT39
019600         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT39
019700     END-IF.                                                      PPCTT39
019800     IF CTRI-RUN-ABORTED                                          PPCTT39
019900        GO TO 9999-ABEND                                          PPCTT39
020000     END-IF.                                                      PPCTT39
020100                                                                  PPCTT39
020200 9199-EXIT.                                                       PPCTT39
020300     EXIT.                                                        PPCTT39
020400                                                                  PPCTT39
020500 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT39
020600                                                                  PPCTT39
020700     PERFORM WITH TEST AFTER                                      PPCTT39
020800       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT39
020900          OR CSRI-TRANSACTION(1:1) NOT = '*'                      PPCTT39
021000      CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE        PPCTT39
021100      IF CSRI-NORMAL                                              PPCTT39
021200         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT39
021300         IF CSRI-TRANSACTION(1:1) = '*'                           PPCTT39
021400            MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION             PPCTT39
021500            MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO     PPCTT39
021600            PERFORM 9100-CALL-PPCTLRPT                            PPCTT39
021700            ADD 1 TO CTTI-TRANS-ACCEPTED                          PPCTT39
021800         ELSE                                                     PPCTT39
021900            MOVE CSRI-TRANSACTION TO DATA-ELEM-SCRN-DES-TRANS     PPCTT39
022000         END-IF                                                   PPCTT39
022100      ELSE                                                        PPCTT39
022200        IF NOT CSRI-TABLE-TRANS-END                               PPCTT39
022300           MOVE MCT001              TO CTRI-MESSAGE-NUMBER        PPCTT39
022400           MOVE CSRI-RETURN-CODE    TO WA-RTN-CODE                PPCTT39
022500           MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                PPCTT39
022600           MOVE WA-READ-ERROR       TO CTRI-TEXT-LINE             PPCTT39
022700           PERFORM 9100-CALL-PPCTLRPT                             PPCTT39
022800           GO TO 9999-ABEND                                       PPCTT39
022900        END-IF                                                    PPCTT39
023000      END-IF                                                      PPCTT39
023100     END-PERFORM.                                                 PPCTT39
023200                                                                  PPCTT39
023300 9399-EXIT.                                                       PPCTT39
023400     EXIT.                                                        PPCTT39
023500                                                                  PPCTT39
023600 9999-ABEND SECTION.                                              PPCTT39
023700                                                                  PPCTT39
023800     SET CTTI-ABORT TO TRUE.                                      PPCTT39
023900     GOBACK.                                                      PPCTT39
024000*************************END OF SOURCE CODE***********************PPCTT39
