000100**************************************************************/   52121330
000200*  PROGRAM: PPELCRPT                                         */   52121330
000300*  RELEASE: ___1330______ SERVICE REQUEST(S): ____15212____  */   52121330
000400*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___02/01/01__  */   52121330
000500*  DESCRIPTION:                                              */   52121330
000600*     I/O module for reporting employees achieving career    */   52121330
000700*     status eligibility, or approaching eligibility, based  */   52121330
000800*     on achieving the required Hours Toward Career Status   */   52121330
000900*     Eligibility.                                           */   52121330
001000*     PPELCRPT is called from PPP130 to open and close the   */   52121330
001100*     file.                                                  */   52121330
001200*     PPELCRPT is called by PPEI207 to write report records. */   52121330
001300*     PPP139 subsequently reads the file and produces the    */   52121330
001400*     actual reports.                                        */   52121330
001500**************************************************************/   52121330
001600 IDENTIFICATION DIVISION.                                         PPELCRPT
001700 PROGRAM-ID. PPELCRPT.                                            PPELCRPT
001800 AUTHOR. UCOP.                                                    PPELCRPT
001900 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPELCRPT
002000*                                                                 PPELCRPT
002100 DATE-WRITTEN.  AUGUST 2000.                                      PPELCRPT
002200 DATE-COMPILED.                                                   PPELCRPT
002300     EJECT                                                        PPELCRPT
002400 ENVIRONMENT DIVISION.                                            PPELCRPT
002500 CONFIGURATION SECTION.                                           PPELCRPT
002600 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPELCRPT
002700 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPELCRPT
002800 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPELCRPT
002900 INPUT-OUTPUT SECTION.                                            PPELCRPT
003000 FILE-CONTROL.                                                    PPELCRPT
003100                                                                  PPELCRPT
003200     SELECT  PPELCRPT-FILE     ASSIGN TO UT-S-PPELCRPT.           PPELCRPT
003300                                                                  PPELCRPT
003400 DATA DIVISION.                                                   PPELCRPT
003500 FILE SECTION.                                                    PPELCRPT
003600                                                                  PPELCRPT
003700 FD  PPELCRPT-FILE                                                PPELCRPT
003800     BLOCK CONTAINS 0 RECORDS                                     PPELCRPT
003900     LABEL RECORDS ARE STANDARD                                   PPELCRPT
004000     RECORDING MODE IS F                                          PPELCRPT
004100     DATA RECORD IS PPELCRPT-RECORD.                              PPELCRPT
004200 01  PPELCRPT-RECORD                PIC X(70).                    PPELCRPT
004300     EJECT                                                        PPELCRPT
004400******************************************************************PPELCRPT
004500 WORKING-STORAGE SECTION.                                         PPELCRPT
004600******************************************************************PPELCRPT
004700 77  A-STND-PROG-ID                  PIC X(15) VALUE              PPELCRPT
004800                                              'PPELCRPT/020101'.  PPELCRPT
004900 01  MISC-WORK-AREAS.                                             PPELCRPT
005000     05  FIRST-TIME-SW               PIC X(01) VALUE LOW-VALUES.  PPELCRPT
005100         88  THIS-IS-THE-FIRST-TIME            VALUE LOW-VALUES.  PPELCRPT
005200         88  NOT-THE-FIRST-TIME                VALUE 'Y'.         PPELCRPT
005300     05  ELC-REPORT-FILE-SW          PIC X(01) VALUE 'N'.         PPELCRPT
005400         88  ELC-REPORT-FILES-OPEN             VALUE 'Y'.         PPELCRPT
005500         88  ELC-REPORT-FILES-CLOSED           VALUE 'N'.         PPELCRPT
005600                                                                  PPELCRPT
005700 01  WS-ELC-RECORD.                                COPY CPWSXELC. PPELCRPT
005800     EJECT                                                        PPELCRPT
005900                                                                  PPELCRPT
006000 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPELCRPT
006100                                                                  PPELCRPT
006200 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPELCRPT
006300     EJECT                                                        PPELCRPT
006400******************************************************************PPELCRPT
006500*   Data Base Areas.                                             *PPELCRPT
006600******************************************************************PPELCRPT
006700                                                                  PPELCRPT
006800 01  PER-ROW                             EXTERNAL.                PPELCRPT
006900     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPELCRPT
007000     EJECT                                                        PPELCRPT
007100                                                                  PPELCRPT
007200 01  BEN-ROW                             EXTERNAL.                PPELCRPT
007300     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPELCRPT
007700                                                                  PPELCRPT
007800 01  BEL-ROW                             EXTERNAL.                PPELCRPT
007900     EXEC SQL INCLUDE PPPVBEL1 END-EXEC.                          PPELCRPT
008300                                                                  PPELCRPT
008400 01  PAY-ROW                             EXTERNAL.                PPELCRPT
008500     EXEC SQL INCLUDE PPPVPAY1 END-EXEC.                          PPELCRPT
008600                                                                  PPELCRPT
008700 01  PCM-ROW                             EXTERNAL.                PPELCRPT
008800     EXEC SQL INCLUDE PPPVPCM1 END-EXEC.                          PPELCRPT
009100                                                                  PPELCRPT
010600     EJECT                                                        PPELCRPT
010700******************************************************************PPELCRPT
010800**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPELCRPT
010900******************************************************************PPELCRPT
011000                                                                  PPELCRPT
011100******************************************************************PPELCRPT
011200 LINKAGE SECTION.                                                 PPELCRPT
011300******************************************************************PPELCRPT
011400                                                                  PPELCRPT
011500******************************************************************PPELCRPT
011600**  Interface record is passed from PPP130 to open and close the**PPELCRPT
011700**  file. Interface record is passed from PPEI207 to write to   **PPELCRPT
011800**  the file.                                                   **PPELCRPT
011900******************************************************************PPELCRPT
012000 01  PPELCRPT-INTERFACE.                            COPY CPLNKELC.PPELCRPT
012100     EJECT                                                        PPELCRPT
012200******************************************************************PPELCRPT
012300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPELCRPT
012400******************************************************************PPELCRPT
012500                                                                  PPELCRPT
012600 PROCEDURE DIVISION USING PPELCRPT-INTERFACE.                     PPELCRPT
012700                                                                  PPELCRPT
012800     IF THIS-IS-THE-FIRST-TIME                                    PPELCRPT
012900        PERFORM 0100-INITIALIZE                                   PPELCRPT
013000     END-IF.                                                      PPELCRPT
013100                                                                  PPELCRPT
013200******************************************************************PPELCRPT
013300**  Perform the main routine now - after which we will return   **PPELCRPT
013400**  to the calling program.                                     **PPELCRPT
013500******************************************************************PPELCRPT
013600                                                                  PPELCRPT
013700     PERFORM 1000-MAINLINE-ROUTINE.                               PPELCRPT
013800                                                                  PPELCRPT
013900     EXIT PROGRAM.                                                PPELCRPT
014000                                                                  PPELCRPT
014100     EJECT                                                        PPELCRPT
014200******************************************************************PPELCRPT
014300 0100-INITIALIZE    SECTION.                                      PPELCRPT
014400******************************************************************PPELCRPT
014500                                                                  PPELCRPT
014600******************************************************************PPELCRPT
014700**  Display the program ID and Date/Time compiled.              **PPELCRPT
014800******************************************************************PPELCRPT
014900                                                                  PPELCRPT
015000     MOVE A-STND-PROG-ID          TO XWHC-DISPLAY-MODULE.         PPELCRPT
015100                                                                  PPELCRPT
015200*                                               ***************   PPELCRPT
015300                                                 COPY CPPDXWHC.   PPELCRPT
015400*                                               ***************   PPELCRPT
015500                                                                  PPELCRPT
015600******************************************************************PPELCRPT
015700**   The file should be closed the first time.                  **PPELCRPT
015800**   Turn off the First Time switch.                            **PPELCRPT
015900******************************************************************PPELCRPT
016000     SET ELC-REPORT-FILES-CLOSED TO TRUE.                         PPELCRPT
016100     SET NOT-THE-FIRST-TIME TO TRUE.                              PPELCRPT
016200                                                                  PPELCRPT
016300******************************************************************PPELCRPT
016400 1000-MAINLINE-ROUTINE    SECTION.                                PPELCRPT
016500******************************************************************PPELCRPT
016600                                                                  PPELCRPT
016700     MOVE 0 TO PPELCRPT-RETURN-STATUS.                            PPELCRPT
016800                                                                  PPELCRPT
016900     EVALUATE PPELCRPT-ACTION                                     PPELCRPT
017000                                                                  PPELCRPT
017100         WHEN 'OPEN '                                             PPELCRPT
017200******************************************************************PPELCRPT
017300**             Open the PPELCRPT file.                          **PPELCRPT
017400**             If already open, set error condition.            **PPELCRPT
017500******************************************************************PPELCRPT
017600               IF ELC-REPORT-FILES-CLOSED                         PPELCRPT
017700                  OPEN OUTPUT PPELCRPT-FILE                       PPELCRPT
017800                  SET ELC-REPORT-FILES-OPEN TO TRUE               PPELCRPT
017900               ELSE                                               PPELCRPT
018000                  SET PPELCRPT-FILE-STATUS-ERROR TO TRUE          PPELCRPT
018100               END-IF                                             PPELCRPT
018200                                                                  PPELCRPT
018300         WHEN 'CLOSE'                                             PPELCRPT
018400******************************************************************PPELCRPT
018500**             Close the PPELCRPT file.                         **PPELCRPT
018600**             If already closed, set error condition           **PPELCRPT
018700******************************************************************PPELCRPT
018800               IF ELC-REPORT-FILES-OPEN                           PPELCRPT
018900                  CLOSE PPELCRPT-FILE                             PPELCRPT
019000                  SET ELC-REPORT-FILES-CLOSED TO TRUE             PPELCRPT
019100               ELSE                                               PPELCRPT
019200                  SET PPELCRPT-FILE-STATUS-ERROR TO TRUE          PPELCRPT
019300               END-IF                                             PPELCRPT
019400                                                                  PPELCRPT
019500         WHEN 'WRITE'                                             PPELCRPT
019600******************************************************************PPELCRPT
019700**             Write to the PPELCRPT file.                      **PPELCRPT
019800**             If file is not open, set error condition.        **PPELCRPT
019900******************************************************************PPELCRPT
020000               IF ELC-REPORT-FILES-OPEN                           PPELCRPT
020100                  PERFORM 2000-ELC-REPORT-WRITE                   PPELCRPT
020200               ELSE                                               PPELCRPT
020300                  SET PPELCRPT-FILE-STATUS-ERROR TO TRUE          PPELCRPT
020400               END-IF                                             PPELCRPT
020500                                                                  PPELCRPT
020600         WHEN  OTHER                                              PPELCRPT
020700******************************************************************PPELCRPT
020800**             Function unknown. Set error condition.           **PPELCRPT
020900******************************************************************PPELCRPT
021000               SET PPELCRPT-FILE-STATUS-ERROR TO TRUE             PPELCRPT
021100                                                                  PPELCRPT
021200     END-EVALUATE.                                                PPELCRPT
021300                                                                  PPELCRPT
021400******************************************************************PPELCRPT
021500 2000-ELC-REPORT-WRITE      SECTION.                              PPELCRPT
021600******************************************************************PPELCRPT
021700                                                                  PPELCRPT
021800******************************************************************PPELCRPT
021900*    Set the Report Type based on whether the employee was       *PPELCRPT
022000*    achieved in benefits or has just reached the reporting      *PPELCRPT
022100*    threshold.                                                  *PPELCRPT
022200******************************************************************PPELCRPT
022300     IF PPELCRPT-HRS-CAR-ELIG-ACHIEVED                            PPELCRPT
022400        SET XELC-REPORT-ACHIEVED TO TRUE                          PPELCRPT
022500     ELSE                                                         PPELCRPT
022600        SET XELC-REPORT-THRESHOLD TO TRUE                         PPELCRPT
022700     END-IF.                                                      PPELCRPT
022800                                                                  PPELCRPT
022900     PERFORM 2200-WRITE-PPELCRPT-RECORD.                          PPELCRPT
023000                                                                  PPELCRPT
023100 2200-WRITE-PPELCRPT-RECORD SECTION.                              PPELCRPT
023200******************************************************************PPELCRPT
023300*    Move the data from EXTERNAL rows.                           *PPELCRPT
023600******************************************************************PPELCRPT
023700     MOVE EMPLOYEE-ID OF PER-ROW TO XELC-EMPLOYEE-ID.             PPELCRPT
023800     MOVE EMP-NAME OF PER-ROW TO XELC-EMP-NAME.                   PPELCRPT
023900     MOVE HOME-DEPT OF PER-ROW TO XELC-HOME-DEPT.                 PPELCRPT
024000     MOVE EMP-STATUS OF PER-ROW TO XELC-EMP-STATUS.               PPELCRPT
024100     MOVE HIRE-DATE OF PER-ROW TO XELC-HIRE-DATE.                 PPELCRPT
025600     MOVE HRS-CAR-ELIG-TOT TO XELC-HRS-CAR-ELIG-TOT.              PPELCRPT
025700                                                                  PPELCRPT
025800     WRITE PPELCRPT-RECORD FROM WS-ELC-RECORD.                    PPELCRPT
025900                                                                  PPELCRPT
026000******************END OF SOURCE PPELCRPT**************************PPELCRPT
