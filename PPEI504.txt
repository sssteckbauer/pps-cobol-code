000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEI504                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    LEAVE FLAG MAINTENANCE                                  *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEI504
001200 IDENTIFICATION DIVISION.                                         PPEI504
001300 PROGRAM-ID. PPEI504.                                             PPEI504
001400 AUTHOR. UCOP.                                                    PPEI504
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEI504
001600                                                                  PPEI504
001700 DATE-WRITTEN.  JULY 15,1992.                                     PPEI504
001800 DATE-COMPILED. XX/XX/XX.                                         PPEI504
001900     EJECT                                                        PPEI504
002000 ENVIRONMENT DIVISION.                                            PPEI504
002100 CONFIGURATION SECTION.                                           PPEI504
002200 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEI504
002300 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEI504
002400 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEI504
002500                                                                  PPEI504
002600 INPUT-OUTPUT SECTION.                                            PPEI504
002700 FILE-CONTROL.                                                    PPEI504
002800                                                                  PPEI504
002900 DATA DIVISION.                                                   PPEI504
003000 FILE SECTION.                                                    PPEI504
003100     EJECT                                                        PPEI504
003200*----------------------------------------------------------------*PPEI504
003300 WORKING-STORAGE SECTION.                                         PPEI504
003400*----------------------------------------------------------------*PPEI504
003500                                                                  PPEI504
003600 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEI504
003700     'PPEI504 /111992'.                                           PPEI504
003800                                                                  PPEI504
003900 01  A-STND-MSG-PARMS                 REDEFINES                   PPEI504
004000     A-STND-PROG-ID.                                              PPEI504
004100     05  FILLER                       PIC X(03).                  PPEI504
004200     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEI504
004300     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEI504
004400     05  FILLER                       PIC X(08).                  PPEI504
004500                                                                  PPEI504
004600 01  MESSAGES-USED.                                               PPEI504
004700     05  MXX001                   PIC X(05)  VALUE 'XX001'.       PPEI504
004800                                                                  PPEI504
004900 01  ELEMENTS-USED.                                               PPEI504
005000     05  E5169                    PIC 9(04) VALUE  5169.          PPEI504
005100     05  E5170                    PIC 9(04) VALUE  5170.          PPEI504
005200     05  E5171                    PIC 9(04) VALUE  5171.          PPEI504
005300     05  E5172                    PIC 9(04) VALUE  5172.          PPEI504
005400     05  E5173                    PIC 9(04) VALUE  5173.          PPEI504
005500     05  E5174                    PIC 9(04) VALUE  5174.          PPEI504
005600     05  E5175                    PIC 9(04) VALUE  5175.          PPEI504
005700     05  E5176                    PIC 9(04) VALUE  5176.          PPEI504
005800     05  E5177                    PIC 9(04) VALUE  5177.          PPEI504
005900     05  E5178                    PIC 9(04) VALUE  5178.          PPEI504
006000     05  E5179                    PIC 9(04) VALUE  5179.          PPEI504
006100     05  E5180                    PIC 9(04) VALUE  5180.          PPEI504
006200     05  E5181                    PIC 9(04) VALUE  5181.          PPEI504
006300                                                                  PPEI504
006400 01  MISC-WORK-AREAS.                                             PPEI504
006500     05  FIRST-TIME-SW            PIC X(01) VALUE LOW-VALUES.     PPEI504
006600         88  THIS-IS-THE-FIRST-TIME          VALUE LOW-VALUES.    PPEI504
006700         88  NOT-THE-FIRST-TIME              VALUE 'Y'.           PPEI504
006800     05  LEAVE-BGN-DATE.                                          PPEI504
006900         07  LEAVE-BGN-YY         PIC  9(2) VALUE ZEROS.          PPEI504
007000         07  LEAVE-BGN-MM         PIC  9(2) VALUE ZEROS.          PPEI504
007100     05  LEAVE-END-DATE.                                          PPEI504
007200         07  LEAVE-END-YY         PIC  9(2) VALUE ZEROS.          PPEI504
007300         07  LEAVE-END-MM         PIC  9(2) VALUE ZEROS.          PPEI504
007400     05  LV-FLAG-SUB              PIC S9(3) VALUE ZERO.           PPEI504
007500     05  LV-FLAG-DATE-TABLE.                                      PPEI504
007600         10  MTHLY-LV-FLAG-DATES OCCURS 12 TIMES.                 PPEI504
007700             15  LV-FLAG-DATE.                                    PPEI504
007800                 20  LV-FLAG-YY   PIC 9(02).                      PPEI504
007900                 20  LV-FLAG-MM   PIC 9(02).                      PPEI504
008000     05  WRK-MISC-DATE-YYMMDD.                                    PPEI504
008100         10  WRK-MISC-DATE-YYMM.                                  PPEI504
008200             15  WRK-MISC-DATE-YY PIC 9(2)  VALUE ZEROS.          PPEI504
008300             15  WRK-MISC-DATE-MM PIC 9(2)  VALUE ZEROS.          PPEI504
008400         10  WRK-MISC-DATE-DD     PIC 9(2)  VALUE ZEROS.          PPEI504
008500                                                                  PPEI504
008600                                                                  PPEI504
008700     EJECT                                                        PPEI504
008800 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEI504
008900                                                                  PPEI504
009000     EJECT                                                        PPEI504
009100 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPEI504
009200                                                                  PPEI504
009300     EJECT                                                        PPEI504
009400 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEI504
009500                                                                  PPEI504
009600     EJECT                                                        PPEI504
009700 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEI504
009800                                                                  PPEI504
009900     EJECT                                                        PPEI504
010000 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPEI504
010100                                                                  PPEI504
010200     EJECT                                                        PPEI504
010300 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEI504
010400                                                                  PPEI504
010500     EJECT                                                        PPEI504
010600 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEI504
010700                                                                  PPEI504
010800     EJECT                                                        PPEI504
010900******************************************************************PPEI504
011000**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEI504
011100**--------------------------------------------------------------**PPEI504
011200** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEI504
011300******************************************************************PPEI504
011400                                                                  PPEI504
011500 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEI504
011600                                                                  PPEI504
011700     EJECT                                                        PPEI504
011800 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEI504
011900                                                                  PPEI504
012000     EJECT                                                        PPEI504
012100 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEI504
012200                                                                  PPEI504
012300     EJECT                                                        PPEI504
012400 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEI504
012500                                                                  PPEI504
012600 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEI504
012700                                                                  PPEI504
012800 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEI504
012900                                                                  PPEI504
013000 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEI504
013100                                                                  PPEI504
013200     EJECT                                                        PPEI504
013300 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEI504
013400                                                                  PPEI504
013500     EJECT                                                        PPEI504
013600 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEI504
013700                                                                  PPEI504
013800     EJECT                                                        PPEI504
013900 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEI504
014000                                                                  PPEI504
014100     EJECT                                                        PPEI504
014200******************************************************************PPEI504
014300**  DATA BASE AREAS                                             **PPEI504
014400******************************************************************PPEI504
014500                                                                  PPEI504
014600 01  SCR-ROW                             EXTERNAL.                PPEI504
014700     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEI504
014800                                                                  PPEI504
014900     EJECT                                                        PPEI504
015000 01  PER-ROW                             EXTERNAL.                PPEI504
015100     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEI504
015200                                                                  PPEI504
015300     EJECT                                                        PPEI504
015400 01  BEL-ROW                             EXTERNAL.                PPEI504
015500     EXEC SQL INCLUDE PPPVBEL1 END-EXEC.                          PPEI504
015600                                                                  PPEI504
015700     EJECT                                                        PPEI504
015800******************************************************************PPEI504
015900**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEI504
016000******************************************************************PPEI504
016100                                                                  PPEI504
016200*----------------------------------------------------------------*PPEI504
016300 LINKAGE SECTION.                                                 PPEI504
016400*----------------------------------------------------------------*PPEI504
016500                                                                  PPEI504
016600******************************************************************PPEI504
016700**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEI504
016800**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEI504
016900******************************************************************PPEI504
017000                                                                  PPEI504
017100 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEI504
017200                                                                  PPEI504
017300     EJECT                                                        PPEI504
017400******************************************************************PPEI504
017500**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEI504
017600**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEI504
017700******************************************************************PPEI504
017800                                                                  PPEI504
017900 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEI504
018000                                                                  PPEI504
018100     EJECT                                                        PPEI504
018200******************************************************************PPEI504
018300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEI504
018400******************************************************************PPEI504
018500                                                                  PPEI504
018600 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEI504
018700                          KMTA-MESSAGE-TABLE-ARRAY.               PPEI504
018800                                                                  PPEI504
018900                                                                  PPEI504
019000*----------------------------------------------------------------*PPEI504
019100 0000-DRIVER.                                                     PPEI504
019200*----------------------------------------------------------------*PPEI504
019300                                                                  PPEI504
019400******************************************************************PPEI504
019500**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEI504
019600******************************************************************PPEI504
019700                                                                  PPEI504
019800     IF  THIS-IS-THE-FIRST-TIME                                   PPEI504
019900         PERFORM 0100-INITIALIZE                                  PPEI504
020000     END-IF.                                                      PPEI504
020100                                                                  PPEI504
020200******************************************************************PPEI504
020300**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEI504
020400**  TO THE CALLING PROGRAM                                      **PPEI504
020500******************************************************************PPEI504
020600                                                                  PPEI504
020700     PERFORM 1000-MAINLINE-ROUTINE.                               PPEI504
020800                                                                  PPEI504
020900     EXIT PROGRAM.                                                PPEI504
021000                                                                  PPEI504
021100                                                                  PPEI504
021200     EJECT                                                        PPEI504
021300*----------------------------------------------------------------*PPEI504
021400 0100-INITIALIZE    SECTION.                                      PPEI504
021500*----------------------------------------------------------------*PPEI504
021600                                                                  PPEI504
021700******************************************************************PPEI504
021800**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEI504
021900******************************************************************PPEI504
022000                                                                  PPEI504
022100     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEI504
022200                                                                  PPEI504
022300*                                               *-------------*   PPEI504
022400                                                 COPY CPPDXWHC.   PPEI504
022500*                                               *-------------*   PPEI504
022600                                                                  PPEI504
022700*                                               *-------------*   PPEI504
022800                                                 COPY CPPDDATE.   PPEI504
022900*                                               *-------------*   PPEI504
023000                                                                  PPEI504
023100     MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 PPEI504
023200                                                                  PPEI504
023300******************************************************************PPEI504
023400**   SET FIRST CALL SWITCH OFF                                  **PPEI504
023500******************************************************************PPEI504
023600                                                                  PPEI504
023700     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEI504
023800                                                                  PPEI504
023900     EJECT                                                        PPEI504
024000*----------------------------------------------------------------*PPEI504
024100 1000-MAINLINE-ROUTINE    SECTION.                                PPEI504
024200*----------------------------------------------------------------*PPEI504
024300                                                                  PPEI504
024400     IF      LOA-BEGIN-DATE  NOT = ISO-ZERO-DATE                  PPEI504
024500         AND LOA-RETURN-DATE NOT = ISO-ZERO-DATE                  PPEI504
024600         PERFORM 1100-SET-LEAVE-FLAGS                             PPEI504
024700     END-IF.                                                      PPEI504
024800                                                                  PPEI504
024900                                                                  PPEI504
025000     EJECT                                                        PPEI504
025100*----------------------------------------------------------------*PPEI504
025200 1100-SET-LEAVE-FLAGS  SECTION.                                   PPEI504
025300*----------------------------------------------------------------*PPEI504
025400                                                                  PPEI504
025500     MOVE LOA-BEGIN-DATE          TO DB2-DATE.                    PPEI504
025600                                                                  PPEI504
025700     PERFORM CONVERT-DB2-DATE                                     PPEI504
025800                                                                  PPEI504
025900     MOVE WSX-STD-DATE            TO WRK-MISC-DATE-YYMMDD.        PPEI504
026000     MOVE WRK-MISC-DATE-YY        TO LEAVE-BGN-YY.                PPEI504
026100     MOVE WRK-MISC-DATE-MM        TO LEAVE-BGN-MM.                PPEI504
026200     MOVE LOA-RETURN-DATE         TO DB2-DATE.                    PPEI504
026300                                                                  PPEI504
026400     PERFORM CONVERT-DB2-DATE                                     PPEI504
026500                                                                  PPEI504
026600     MOVE WSX-STD-DATE            TO WRK-MISC-DATE-YYMMDD.        PPEI504
026700     MOVE WRK-MISC-DATE-YY        TO LEAVE-END-YY.                PPEI504
026800     MOVE WRK-MISC-DATE-MM        TO LEAVE-END-MM.                PPEI504
026900                                                                  PPEI504
027000     IF  WRK-MISC-DATE-DD = 01                                    PPEI504
027100         IF  LEAVE-END-MM = 01                                    PPEI504
027200             MOVE 12              TO LEAVE-END-MM                 PPEI504
027300             SUBTRACT 1         FROM LEAVE-END-YY                 PPEI504
027400         ELSE                                                     PPEI504
027500             SUBTRACT 1         FROM LEAVE-END-MM                 PPEI504
027600         END-IF                                                   PPEI504
027700     END-IF.                                                      PPEI504
027800                                                                  PPEI504
027900******************************************************************PPEI504
028000** IF FUTURE LEAVE, NO FURTHER PROCESSING, OTHERWISE CHECK      **PPEI504
028100** CURRENT AND HISTORICAL LV FLAGS.                             **PPEI504
028200******************************************************************PPEI504
028300                                                                  PPEI504
028400     IF  LEAVE-BGN-DATE > FIRST-OF-MONTH-YRMO                     PPEI504
028500         CONTINUE                                                 PPEI504
028600     ELSE                                                         PPEI504
028700         MOVE SPACES              TO LV-FLAG-DATE-TABLE           PPEI504
028800         PERFORM 1200-SET-HIST-LV-FLAGS                           PPEI504
028900             VARYING LV-FLAG-SUB FROM +1 BY +1                    PPEI504
029000               UNTIL LV-FLAG-SUB > +12                            PPEI504
029100         IF  LEAVE-END-DATE NOT < FIRST-OF-MONTH-YRMO             PPEI504
029200             MOVE 'L'             TO LOA-CURR-AVGEXCLUD           PPEI504
029300             MOVE E5169           TO DL-FIELD                     PPEI504
029400             PERFORM 9050-AUDITING-RESPONSIBILITIES               PPEI504
029500         END-IF                                                   PPEI504
029600     END-IF.                                                      PPEI504
029700                                                                  PPEI504
029800                                                                  PPEI504
029900     EJECT                                                        PPEI504
030000*----------------------------------------------------------------*PPEI504
030100 1200-SET-HIST-LV-FLAGS  SECTION.                                 PPEI504
030200*----------------------------------------------------------------*PPEI504
030300                                                                  PPEI504
030400     MOVE LV-FLAG-SUB             TO LV-FLAG-MM (LV-FLAG-SUB).    PPEI504
030500                                                                  PPEI504
030600     IF  LV-FLAG-MM (LV-FLAG-SUB) < FIRST-OF-MONTH-MO             PPEI504
030700         MOVE FIRST-OF-MONTH-YR   TO LV-FLAG-YY (LV-FLAG-SUB)     PPEI504
030800     ELSE                                                         PPEI504
030900         COMPUTE LV-FLAG-YY (LV-FLAG-SUB) = FIRST-OF-MONTH-YR - 1 PPEI504
031000     END-IF.                                                      PPEI504
031100                                                                  PPEI504
031200     IF      LV-FLAG-DATE (LV-FLAG-SUB) NOT < LEAVE-BGN-DATE      PPEI504
031300         AND LV-FLAG-DATE (LV-FLAG-SUB) NOT > LEAVE-END-DATE      PPEI504
031400                                                                  PPEI504
031500         EVALUATE LV-FLAG-SUB                                     PPEI504
031600                                                                  PPEI504
031700             WHEN 1                                               PPEI504
031800                  MOVE 'L'        TO LOA-JAN-AVGEXCLUD            PPEI504
031900                  MOVE E5170      TO DL-FIELD                     PPEI504
032000                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
032100                                                                  PPEI504
032200             WHEN 2                                               PPEI504
032300                  MOVE 'L'        TO LOA-FEB-AVGEXCLUD            PPEI504
032400                  MOVE E5171      TO DL-FIELD                     PPEI504
032500                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
032600                                                                  PPEI504
032700             WHEN 3                                               PPEI504
032800                  MOVE 'L'        TO LOA-MAR-AVGEXCLUD            PPEI504
032900                  MOVE E5172      TO DL-FIELD                     PPEI504
033000                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
033100                                                                  PPEI504
033200             WHEN 4                                               PPEI504
033300                  MOVE 'L'        TO LOA-APR-AVGEXCLUD            PPEI504
033400                  MOVE E5173      TO DL-FIELD                     PPEI504
033500                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
033600                                                                  PPEI504
033700             WHEN 5                                               PPEI504
033800                  MOVE 'L'        TO LOA-MAY-AVGEXCLUD            PPEI504
033900                  MOVE E5174      TO DL-FIELD                     PPEI504
034000                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
034100                                                                  PPEI504
034200             WHEN 6                                               PPEI504
034300                  MOVE 'L'        TO LOA-JUN-AVGEXCLUD            PPEI504
034400                  MOVE E5175      TO DL-FIELD                     PPEI504
034500                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
034600                                                                  PPEI504
034700             WHEN 7                                               PPEI504
034800                  MOVE 'L'        TO LOA-JUL-AVGEXCLUD            PPEI504
034900                  MOVE E5176      TO DL-FIELD                     PPEI504
035000                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
035100                                                                  PPEI504
035200             WHEN 8                                               PPEI504
035300                  MOVE 'L'        TO LOA-AUG-AVGEXCLUD            PPEI504
035400                  MOVE E5177      TO DL-FIELD                     PPEI504
035500                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
035600                                                                  PPEI504
035700             WHEN 9                                               PPEI504
035800                  MOVE 'L'        TO LOA-SEP-AVGEXCLUD            PPEI504
035900                  MOVE E5178      TO DL-FIELD                     PPEI504
036000                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
036100                                                                  PPEI504
036200             WHEN 10                                              PPEI504
036300                  MOVE 'L'        TO LOA-OCT-AVGEXCLUD            PPEI504
036400                  MOVE E5179      TO DL-FIELD                     PPEI504
036500                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
036600                                                                  PPEI504
036700             WHEN 11                                              PPEI504
036800                  MOVE 'L'        TO LOA-NOV-AVGEXCLUD            PPEI504
036900                  MOVE E5180      TO DL-FIELD                     PPEI504
037000                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
037100                                                                  PPEI504
037200             WHEN 12                                              PPEI504
037300                  MOVE 'L'        TO LOA-DEC-AVGEXCLUD            PPEI504
037400                  MOVE E5181      TO DL-FIELD                     PPEI504
037500                  PERFORM 9050-AUDITING-RESPONSIBILITIES          PPEI504
037600                                                                  PPEI504
037700         END-EVALUATE                                             PPEI504
037800                                                                  PPEI504
037900     END-IF.                                                      PPEI504
038000     EJECT                                                        PPEI504
038100*----------------------------------------------------------------*PPEI504
038200 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEI504
038300*----------------------------------------------------------------*PPEI504
038400                                                                  PPEI504
038500*                                                 *-------------* PPEI504
038600                                                   COPY CPPDXDEC. PPEI504
038700*                                                 *-------------* PPEI504
038800                                                                  PPEI504
038900*----------------------------------------------------------------*PPEI504
039000 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEI504
039100*----------------------------------------------------------------*PPEI504
039200                                                                  PPEI504
039300*                                                 *-------------* PPEI504
039400                                                   COPY CPPDADLC. PPEI504
039500*                                                 *-------------* PPEI504
039600                                                                  PPEI504
039700     EJECT                                                        PPEI504
039800*----------------------------------------------------------------*PPEI504
039900 9300-DATE-CONVERSION-DB2  SECTION.                               PPEI504
040000*----------------------------------------------------------------*PPEI504
040100                                                                  PPEI504
040200*                                                 *-------------* PPEI504
040300                                                   COPY CPPDXDC2. PPEI504
040400*                                                 *-------------* PPEI504
040500                                                                  PPEI504
040600******************************************************************PPEI504
040700**   E N D  S O U R C E   ----  PPEI504 ----                    **PPEI504
040800******************************************************************PPEI504
