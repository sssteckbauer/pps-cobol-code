000000**************************************************************/   28521087
000001*  PROGRAM: PPBRRHFT                                         */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/22/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  ** DATE CONVERSION II **                                  */   28521087
000006*  - REPLACED CPWSXDC2 AND CPPDXDC2 WITH CPWSXDC3 AND CPPDXDC3/   28521087
000007*  - COMMENTED PARAGRAPH 999999-EXIT. CAN NEVER BE EXECUTED  */   28521087
000008**************************************************************/   28521087
000100**************************************************************/   36330704
000200*  PROGRAM: PPBRRHFT                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPBRRH SELECT MODULE                               */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPBRRHFT
001000 PROGRAM-ID. PPBRRHFT.                                            PPBRRHFT
001100 ENVIRONMENT DIVISION.                                            PPBRRHFT
001200 DATA DIVISION.                                                   PPBRRHFT
001300 WORKING-STORAGE SECTION.                                         PPBRRHFT
001400*                                                                 PPBRRHFT
001500     COPY CPWSXBEG.                                               PPBRRHFT
001600*                                                                 PPBRRHFT
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPBRRHFT'.       PPBRRHFT
001800*                                                                 PPBRRHFT
001900     EXEC SQL                                                     PPBRRHFT
002000         INCLUDE CPWSWRKC                                         PPBRRHFT
002100     END-EXEC.                                                    PPBRRHFT
002200*                                                                 PPBRRHFT
002300*01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC2.                28521087
002310 01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC3.                28521087
002400*                                                                 PPBRRHFT
002500 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPBRRHFT
002600*                                                                 PPBRRHFT
002700 01  PPPBRRH-ROW EXTERNAL.                                        PPBRRHFT
002800     EXEC SQL                                                     PPBRRHFT
002900         INCLUDE PPPVBRRH                                         PPBRRHFT
003000     END-EXEC.                                                    PPBRRHFT
003100*                                                                 PPBRRHFT
003200*------------------------------------------------------------*    PPBRRHFT
003300*  SQL DCLGEN AREA                                           *    PPBRRHFT
003400*------------------------------------------------------------*    PPBRRHFT
003500     EXEC SQL                                                     PPBRRHFT
003600         INCLUDE SQLCA                                            PPBRRHFT
003700     END-EXEC.                                                    PPBRRHFT
003800*                                                                 PPBRRHFT
003900 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPBRRHFT
004000*                                                                 PPBRRHFT
004100     COPY CPWSXEND.                                               PPBRRHFT
004200*                                                                 PPBRRHFT
004300 LINKAGE SECTION.                                                 PPBRRHFT
004400*                                                                 PPBRRHFT
004500 01  SELECT-INTERFACE-AREA.                                       PPBRRHFT
004600     COPY CPWSXTIF REPLACING ==:TAG:== BY ==XSEL==.               PPBRRHFT
004700*                                                                 PPBRRHFT
004800 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPBRRHFT
004900*                                                                 PPBRRHFT
005000     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPBRRHFT
005100     COPY  CPPDXWHC.                                              PPBRRHFT
005200*                                                                 PPBRRHFT
005300     EXEC SQL                                                     PPBRRHFT
005400         INCLUDE CPPDXE99                                         PPBRRHFT
005500     END-EXEC.                                                    PPBRRHFT
005600*                                                                 PPBRRHFT
005700     COPY CPPDSELC.                                               PPBRRHFT
005800*                                                                 PPBRRHFT
005900 2000-SELECT SECTION.                                             PPBRRHFT
006000*                                                                 PPBRRHFT
006100     MOVE 'SELECT MAX DATE ROW' TO DB2MSG-TAG.                    PPBRRHFT
006200*                                                                 PPBRRHFT
006300     EXEC SQL                                                     PPBRRHFT
006400         SELECT VALUE(MAX(SYSTEM_ENTRY_DATE),DATE('0001-01-01'))  PPBRRHFT
006500         INTO :WS-MAX-DATE                                        PPBRRHFT
006600         FROM PPPVBRRH_BRRH                                       PPBRRHFT
006700         WHERE BRR_CBUC           = :WS-PPPBRR-CBUC               PPBRRHFT
006800         AND   BRR_REP            = :WS-PPPBRR-REP                PPBRRHFT
006900         AND   BRR_SHC            = :WS-PPPBRR-SHC                PPBRRHFT
007000         AND   BRR_DUC            = :WS-PPPBRR-DUC                PPBRRHFT
007100         AND   BRR_ENTRY_NUMBER   = :WS-PPPBRR-ENTRY-NO           PPBRRHFT
007200         AND   SYSTEM_ENTRY_DATE ^> :WS-QUERY-DATE                PPBRRHFT
007300     END-EXEC.                                                    PPBRRHFT
007400*                                                                 PPBRRHFT
007500     IF SQLCODE = +100                                            PPBRRHFT
007600     OR WS-MAX-DATE = '0001-01-01'                                PPBRRHFT
007700     OR WS-MAX-DATE = '01/01/0001'                                PPBRRHFT
007800        INITIALIZE XSEL-RETURN-KEY                                PPBRRHFT
007900                   PPPBRRH-ROW                                    PPBRRHFT
008000        PERFORM 5000-INIT-DATES                                   PPBRRHFT
008100        SET XSEL-INVALID-KEY TO TRUE                              PPBRRHFT
008200        GO TO 2000-EXIT                                           PPBRRHFT
008300     END-IF.                                                      PPBRRHFT
008400*                                                                 PPBRRHFT
008500     MOVE 'SELECT MAX TIME ROW' TO DB2MSG-TAG.                    PPBRRHFT
008600*                                                                 PPBRRHFT
008700     IF WS-MAX-DATE = WS-QUERY-DATE                               PPBRRHFT
008800        EXEC SQL                                                  PPBRRHFT
008900            SELECT VALUE(MAX(SYSTEM_ENTRY_TIME),TIME('24.00.00')) PPBRRHFT
009000            INTO :WS-MAX-TIME                                     PPBRRHFT
009100            FROM PPPVBRRH_BRRH                                    PPBRRHFT
009200            WHERE BRR_CBUC           = :WS-PPPBRR-CBUC            PPBRRHFT
009300            AND   BRR_REP            = :WS-PPPBRR-REP             PPBRRHFT
009400            AND   BRR_SHC            = :WS-PPPBRR-SHC             PPBRRHFT
009500            AND   BRR_DUC            = :WS-PPPBRR-DUC             PPBRRHFT
009600            AND   BRR_ENTRY_NUMBER   = :WS-PPPBRR-ENTRY-NO        PPBRRHFT
009700            AND   SYSTEM_ENTRY_DATE  = :WS-MAX-DATE               PPBRRHFT
009800            AND   SYSTEM_ENTRY_TIME ^> :WS-QUERY-TIME             PPBRRHFT
009900        END-EXEC                                                  PPBRRHFT
010000        IF SQLCODE = +100                                         PPBRRHFT
010100        OR WS-MAX-TIME = '24.00.00'                               PPBRRHFT
010200           MOVE 'SELECT MAX DATE ROW' TO DB2MSG-TAG               PPBRRHFT
010300           EXEC SQL                                               PPBRRHFT
010400              SELECT VALUE(MAX(SYSTEM_ENTRY_DATE),                PPBRRHFT
010500                           DATE('0001-01-01'))                    PPBRRHFT
010600              INTO :WS-MAX-DATE                                   PPBRRHFT
010700              FROM PPPVBRRH_BRRH                                  PPBRRHFT
010800              WHERE BRR_CBUC           = :WS-PPPBRR-CBUC          PPBRRHFT
010900              AND   BRR_REP            = :WS-PPPBRR-REP           PPBRRHFT
011000              AND   BRR_SHC            = :WS-PPPBRR-SHC           PPBRRHFT
011100              AND   BRR_DUC            = :WS-PPPBRR-DUC           PPBRRHFT
011200              AND   BRR_ENTRY_NUMBER   = :WS-PPPBRR-ENTRY-NO      PPBRRHFT
011300              AND   SYSTEM_ENTRY_DATE  < :WS-QUERY-DATE           PPBRRHFT
011400           END-EXEC                                               PPBRRHFT
011500           IF SQLCODE = +100                                      PPBRRHFT
011600           OR WS-MAX-DATE = '0001-01-01'                          PPBRRHFT
011700           OR WS-MAX-DATE = '01/01/0001'                          PPBRRHFT
011800              INITIALIZE XSEL-RETURN-KEY                          PPBRRHFT
011900                         PPPBRRH-ROW                              PPBRRHFT
012000              PERFORM 5000-INIT-DATES                             PPBRRHFT
012100              SET XSEL-INVALID-KEY TO TRUE                        PPBRRHFT
012200              GO TO 2000-EXIT                                     PPBRRHFT
012300           END-IF                                                 PPBRRHFT
012400           MOVE 'SELECT MAX TIME ROW' TO DB2MSG-TAG               PPBRRHFT
012500           EXEC SQL                                               PPBRRHFT
012600              SELECT VALUE(MAX(SYSTEM_ENTRY_TIME),                PPBRRHFT
012700                           TIME('24.00.00'))                      PPBRRHFT
012800              INTO :WS-MAX-TIME                                   PPBRRHFT
012900              FROM PPPVBRRH_BRRH                                  PPBRRHFT
013000              WHERE BRR_CBUC           = :WS-PPPBRR-CBUC          PPBRRHFT
013100              AND   BRR_REP            = :WS-PPPBRR-REP           PPBRRHFT
013200              AND   BRR_SHC            = :WS-PPPBRR-SHC           PPBRRHFT
013300              AND   BRR_DUC            = :WS-PPPBRR-DUC           PPBRRHFT
013400              AND   BRR_ENTRY_NUMBER   = :WS-PPPBRR-ENTRY-NO      PPBRRHFT
013500              AND   SYSTEM_ENTRY_DATE  = :WS-MAX-DATE             PPBRRHFT
013600           END-EXEC                                               PPBRRHFT
013700           IF SQLCODE = +100                                      PPBRRHFT
013800           OR WS-MAX-TIME = '24.00.00'                            PPBRRHFT
013900              INITIALIZE XSEL-RETURN-KEY                          PPBRRHFT
014000                         PPPBRRH-ROW                              PPBRRHFT
014100              PERFORM 5000-INIT-DATES                             PPBRRHFT
014200              SET XSEL-INVALID-KEY TO TRUE                        PPBRRHFT
014300              GO TO 2000-EXIT                                     PPBRRHFT
014400           END-IF                                                 PPBRRHFT
014500        END-IF                                                    PPBRRHFT
014600     ELSE                                                         PPBRRHFT
014700        MOVE 'SELECT MAX TIME ROW' TO DB2MSG-TAG                  PPBRRHFT
014800        EXEC SQL                                                  PPBRRHFT
014900            SELECT VALUE(MAX(SYSTEM_ENTRY_TIME),TIME('24.00.00')) PPBRRHFT
015000            INTO :WS-MAX-TIME                                     PPBRRHFT
015100            FROM PPPVBRRH_BRRH                                    PPBRRHFT
015200            WHERE BRR_CBUC           = :WS-PPPBRR-CBUC            PPBRRHFT
015300            AND   BRR_REP            = :WS-PPPBRR-REP             PPBRRHFT
015400            AND   BRR_SHC            = :WS-PPPBRR-SHC             PPBRRHFT
015500            AND   BRR_DUC            = :WS-PPPBRR-DUC             PPBRRHFT
015600            AND   BRR_ENTRY_NUMBER   = :WS-PPPBRR-ENTRY-NO        PPBRRHFT
015700            AND   SYSTEM_ENTRY_DATE  = :WS-MAX-DATE               PPBRRHFT
015800        END-EXEC                                                  PPBRRHFT
015900        IF SQLCODE = +100                                         PPBRRHFT
016000        OR WS-MAX-TIME = '24.00.00'                               PPBRRHFT
016100           INITIALIZE XSEL-RETURN-KEY                             PPBRRHFT
016200                      PPPBRRH-ROW                                 PPBRRHFT
016300           PERFORM 5000-INIT-DATES                                PPBRRHFT
016400           SET XSEL-INVALID-KEY TO TRUE                           PPBRRHFT
016500           GO TO 2000-EXIT                                        PPBRRHFT
016600        END-IF                                                    PPBRRHFT
016700     END-IF.                                                      PPBRRHFT
016800*                                                                 PPBRRHFT
016900*                                                                 PPBRRHFT
017000     MOVE 'SELECT PPPBRRH ROW' TO DB2MSG-TAG.                     PPBRRHFT
017100*                                                                 PPBRRHFT
017200     EXEC SQL                                                     PPBRRHFT
017300         SELECT *                                                 PPBRRHFT
017400         INTO :PPPBRRH-ROW                                        PPBRRHFT
017500         FROM PPPVBRRH_BRRH                                       PPBRRHFT
017600         WHERE BRR_CBUC           = :WS-PPPBRR-CBUC               PPBRRHFT
017700         AND   BRR_REP            = :WS-PPPBRR-REP                PPBRRHFT
017800         AND   BRR_SHC            = :WS-PPPBRR-SHC                PPBRRHFT
017900         AND   BRR_DUC            = :WS-PPPBRR-DUC                PPBRRHFT
018000         AND   BRR_ENTRY_NUMBER   = :WS-PPPBRR-ENTRY-NO           PPBRRHFT
018100         AND   SYSTEM_ENTRY_DATE  = :WS-MAX-DATE                  PPBRRHFT
018200         AND   SYSTEM_ENTRY_TIME  = :WS-MAX-TIME                  PPBRRHFT
018300     END-EXEC.                                                    PPBRRHFT
018400*                                                                 PPBRRHFT
018500     IF SQLCODE = +100                                            PPBRRHFT
018600        INITIALIZE XSEL-RETURN-KEY                                PPBRRHFT
018700                   PPPBRRH-ROW                                    PPBRRHFT
018800        PERFORM 5000-INIT-DATES                                   PPBRRHFT
018900        SET XSEL-INVALID-KEY TO TRUE                              PPBRRHFT
019000        GO TO 2000-EXIT                                           PPBRRHFT
019100     END-IF.                                                      PPBRRHFT
019200*                                                                 PPBRRHFT
019300     IF SQLCODE = +0                                              PPBRRHFT
019400        MOVE WS-CONTROL-KEY    TO XSEL-RETURN-CTL-KEY             PPBRRHFT
019500        MOVE SYSTEM-ENTRY-DATE TO XSEL-RETURN-DATE                PPBRRHFT
019600        MOVE SYSTEM-ENTRY-TIME TO XSEL-RETURN-TIME                PPBRRHFT
019700        SET XSEL-OK TO TRUE                                       PPBRRHFT
019800        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPBRRHFT
019900        GO TO 2000-EXIT                                           PPBRRHFT
020000     END-IF.                                                      PPBRRHFT
020100*                                                                 PPBRRHFT
020200 2000-EXIT.                                                       PPBRRHFT
020300     EXIT.                                                        PPBRRHFT
020400*                                                                 PPBRRHFT
020500 5000-INIT-DATES SECTION.                                         PPBRRHFT
020600*****MOVE WS-MAX-DATE TO WSX-INQ-DATE.                            28521087
020700*****PERFORM GET-INIT-DATE.                                       28521087
020800*****MOVE WSX-INIT-DATE TO SYSTEM-ENTRY-DATE                      28521087
020900*****                      BRR-EFFECTIVE-DATE                     28521087
021000*****                      BRR-LAST-ACTION-DT.                    28521087
021010     MOVE WS-MAX-DATE TO XDC3-DB2-DATE.                           28521087
021020     PERFORM XDC3-GET-INIT-DATE.                                  28521087
021030     MOVE XDC3-INIT-DATE TO SYSTEM-ENTRY-DATE                     28521087
021040                           BRR-EFFECTIVE-DATE                     28521087
021050                           BRR-LAST-ACTION-DT.                    28521087
021100 5000-EXIT.                                                       PPBRRHFT
021200     EXIT.                                                        PPBRRHFT
021300*                                                                 PPBRRHFT
021400*****COPY CPPDXDC2.                                               28521087
021410 9000-DATE-CONVERSION-DB2 SECTION.                                28521087
021420     COPY CPPDXDC3.                                               28521087
021500*                                                                 PPBRRHFT
021600*999999-SQL-ERROR SECTION.                                        PPBRRHFT
021700     COPY CPPDXP99.                                               PPBRRHFT
021800     SET XSEL-DB2-ERROR TO TRUE.                                  PPBRRHFT
021900     MOVE SQLCODE TO XSEL-DB2-MSG-NUMBER.                         PPBRRHFT
022000     GOBACK.                                                      PPBRRHFT
022100*999999-EXIT.                                                     28521087
022200*****EXIT.                                                        28521087
022300*                                                                 PPBRRHFT
