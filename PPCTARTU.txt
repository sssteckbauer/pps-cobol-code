000000**************************************************************/   30871424
000001*  PROGRAM: PPCTARTU                                         */   30871424
000002*  RELEASE: ___1424______ SERVICE REQUEST(S): _____3087____  */   30871424
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___07/30/02__  */   30871424
000004*  ERROR REPORT 1808:                                        */   30871424
000005*  SET ERROR FLAGS FOR NEGATIVE SQL ERRORS TO TRIGGER DB2    */   30871424
000006*  ERROR REPORTING IN TRANSACTION HANDLER PROGRAM.           */   30871424
000007**************************************************************/   30871424
000100**************************************************************/   30871401
000200*  PROGRAM: PPCTARTU                                         */   3087SRRR
000300*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000400*  NAME:___SRS___________ CREATION DATE:      ___03/20/02__  */   30871401
000500*  DESCRIPTION:                                              */   30871401
000600*  ASSESSMT RATE (ART) TABLE UPDATE MODULE                   */   30871401
000700**************************************************************/   30871401
000800 IDENTIFICATION DIVISION.                                         PPCTARTU
000900 PROGRAM-ID. PPCTARTU.                                            PPCTARTU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTARTU
001100 DATE-COMPILED.                                                   PPCTARTU
001200 ENVIRONMENT DIVISION.                                            PPCTARTU
001300 DATA DIVISION.                                                   PPCTARTU
001400 WORKING-STORAGE SECTION.                                         PPCTARTU
001500                                                                  PPCTARTU
001600 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTARTU
001700 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTARTU
001800                                                                  PPCTARTU
001900     EXEC SQL                                                     PPCTARTU
002000        INCLUDE SQLCA                                             PPCTARTU
002100     END-EXEC.                                                    PPCTARTU
002200                                                                  PPCTARTU
002300 LINKAGE SECTION.                                                 PPCTARTU
002400                                                                  PPCTARTU
002500 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTARTU
002600 01  ASSESSMT-RATE-ART-TABLE-DATA.                                PPCTARTU
002700     EXEC SQL                                                     PPCTARTU
002800        INCLUDE PPPVZART                                          PPCTARTU
002900     END-EXEC.                                                    PPCTARTU
003000                                                                  PPCTARTU
003100 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE            PPCTARTU
003200                          ASSESSMT-RATE-ART-TABLE-DATA.           PPCTARTU
003300                                                                  PPCTARTU
003400 0000-MAIN.                                                       PPCTARTU
003500                                                                  PPCTARTU
003600     EXEC SQL                                                     PPCTARTU
003700        INCLUDE CPPDXE99                                          PPCTARTU
003800     END-EXEC.                                                    PPCTARTU
003900     MOVE 'PPCTARTU' TO XWHC-DISPLAY-MODULE.                      PPCTARTU
004000     COPY CPPDXWHC.                                               PPCTARTU
004100     MOVE SPACE TO KCTU-STATUS.                                   PPCTARTU
004200     IF KCTU-ACTION-ADD                                           PPCTARTU
004300        PERFORM 2000-INSERT-ART                                   PPCTARTU
004400     ELSE                                                         PPCTARTU
004500       IF KCTU-ACTION-CHANGE                                      PPCTARTU
004600          PERFORM 2100-UPDATE-ART                                 PPCTARTU
004700       ELSE                                                       PPCTARTU
004800         IF KCTU-ACTION-DELETE                                    PPCTARTU
004900            PERFORM 2200-DELETE-ART                               PPCTARTU
005000         ELSE                                                     PPCTARTU
005100            SET KCTU-INVALID-ACTION TO TRUE                       PPCTARTU
005200         END-IF                                                   PPCTARTU
005300       END-IF                                                     PPCTARTU
005400     END-IF.                                                      PPCTARTU
005500                                                                  PPCTARTU
005600 0100-END.                                                        PPCTARTU
005700                                                                  PPCTARTU
005800     GOBACK.                                                      PPCTARTU
005900                                                                  PPCTARTU
006000 2000-INSERT-ART.                                                 PPCTARTU
006100                                                                  PPCTARTU
006200     MOVE '2000-INSRT-ART' TO DB2MSG-TAG.                         PPCTARTU
006300     EXEC SQL                                                     PPCTARTU
006400         INSERT INTO PPPVZART_ART                                 PPCTARTU
006500              VALUES (:ART-BEN-ASSMT-TYP                          PPCTARTU
006600                     ,:ART-ASSMT-RATE-CD                          PPCTARTU
006700                     ,:ART-EFFECTIVE-DT                           PPCTARTU
006800                     ,:ART-ASSESSMT-RATE                          PPCTARTU
006900                     ,:ART-LAST-ACTION                            PPCTARTU
007000                     ,:ART-LAST-ACTION-DT                         PPCTARTU
007100                     )                                            PPCTARTU
007200     END-EXEC.                                                    PPCTARTU
007300     IF SQLCODE NOT = ZERO                                        PPCTARTU
007400        SET KCTU-INSERT-ERROR TO TRUE                             PPCTARTU
007500     END-IF.                                                      PPCTARTU
007600                                                                  PPCTARTU
007700 2100-UPDATE-ART.                                                 PPCTARTU
007800                                                                  PPCTARTU
007900     MOVE '2100-UPD-ART' TO DB2MSG-TAG.                           PPCTARTU
008000     EXEC SQL                                                     PPCTARTU
008100         UPDATE PPPVZART_ART                                      PPCTARTU
008200            SET ART_BEN_ASSMT_TYP      = :ART-BEN-ASSMT-TYP       PPCTARTU
008300               ,ART_ASSMT_RATE_CD      = :ART-ASSMT-RATE-CD       PPCTARTU
008400               ,ART_EFFECTIVE_DT       = :ART-EFFECTIVE-DT        PPCTARTU
008500               ,ART_ASSESSMT_RATE      = :ART-ASSESSMT-RATE       PPCTARTU
008600               ,ART_LAST_ACTION        = :ART-LAST-ACTION         PPCTARTU
008700               ,ART_LAST_ACTION_DT     = :ART-LAST-ACTION-DT      PPCTARTU
008800          WHERE ART_BEN_ASSMT_TYP      = :ART-BEN-ASSMT-TYP       PPCTARTU
008900            AND ART_ASSMT_RATE_CD      = :ART-ASSMT-RATE-CD       PPCTARTU
009000            AND ART_EFFECTIVE_DT       = :ART-EFFECTIVE-DT        PPCTARTU
009100     END-EXEC.                                                    PPCTARTU
009200     IF SQLCODE NOT = ZERO                                        PPCTARTU
009300        SET KCTU-UPDATE-ERROR TO TRUE                             PPCTARTU
009400     END-IF.                                                      PPCTARTU
009500                                                                  PPCTARTU
009600 2200-DELETE-ART.                                                 PPCTARTU
009700                                                                  PPCTARTU
009800     MOVE '2200-DLTE-ART  ' TO DB2MSG-TAG.                        PPCTARTU
009900     EXEC SQL                                                     PPCTARTU
010000         DELETE FROM PPPVZART_ART                                 PPCTARTU
010100          WHERE ART_BEN_ASSMT_TYP      = :ART-BEN-ASSMT-TYP       PPCTARTU
010200            AND ART_ASSMT_RATE_CD      = :ART-ASSMT-RATE-CD       PPCTARTU
010300            AND ART_EFFECTIVE_DT       = :ART-EFFECTIVE-DT        PPCTARTU
010400     END-EXEC.                                                    PPCTARTU
010500     IF SQLCODE NOT = ZERO                                        PPCTARTU
010600        SET KCTU-DELETE-ERROR TO TRUE                             PPCTARTU
010700     END-IF.                                                      PPCTARTU
010800                                                                  PPCTARTU
010900*999999-SQL-ERROR.                                                PPCTARTU
011000     EXEC SQL                                                     PPCTARTU
011100        INCLUDE CPPDXP99                                          PPCTARTU
011200     END-EXEC.                                                    PPCTARTU
011210     EVALUATE TRUE                                                30871424
011220       WHEN (KCTU-ACTION-ADD)                                     30871424
011230         SET KCTU-INSERT-ERROR TO TRUE                            30871424
011240       WHEN (KCTU-ACTION-CHANGE)                                  30871424
011250         SET KCTU-UPDATE-ERROR TO TRUE                            30871424
011260       WHEN (KCTU-ACTION-DELETE)                                  30871424
011270         SET KCTU-DELETE-ERROR TO TRUE                            30871424
011280     END-EVALUATE.                                                30871424
011300     GO TO 0100-END.                                              PPCTARTU
