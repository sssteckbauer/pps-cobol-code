000100**************************************************************/   30871405
000200*  PROGRAM: PPCTT17G                                         */   30871405
000300*  RELEASE: ___1405______ SERVICE REQUEST(S): _____3087____  */   30871405
000400*  NAME:___SRS___________ CREATION DATE:      ___04/16/02__  */   30871405
000500*  DESCRIPTION:                                              */   30871405
000600*  PPPBUG TABLE TRANSACTION ASSEMBLY MODULE                  */   30871405
000700**************************************************************/   30871405
000800 IDENTIFICATION DIVISION.                                         PPCTT17G
000900 PROGRAM-ID. PPCTT17G.                                            PPCTT17G
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT17G
001100 DATE-COMPILED.                                                   PPCTT17G
001200 DATA DIVISION.                                                   PPCTT17G
001300 FILE SECTION.                                                    PPCTT17G
001400 WORKING-STORAGE SECTION.                                         PPCTT17G
001500                                                                  PPCTT17G
001600 01  WA-WORK-VARIABLES.                                           PPCTT17G
001700     05  WA-READ-ERROR.                                           PPCTT17G
001800         10 FILLER                     PIC X(37) VALUE            PPCTT17G
001900                 'PPCTLTRD ERROR ON TRANS CALL: RETURN='.         PPCTT17G
002000         10 WA-RTN-CODE                PIC XX.                    PPCTT17G
002100         10 FILLER                     PIC X(07) VALUE            PPCTT17G
002200                 ' COUNT='.                                       PPCTT17G
002300         10 WA-WORK-Z5                 PIC ZZ,ZZ9.                PPCTT17G
002400 01  WB-PROGRAM-CONSTANTS.                                        PPCTT17G
002500     05  WB-TABLE-NO                   PIC X(2) VALUE '17'.       PPCTT17G
002600 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT17G
002700     05  WC-ERR-IX                     PIC S9(04) VALUE ZERO.     PPCTT17G
002800     05  IX                            PIC S9(04) VALUE ZERO.     PPCTT17G
002900 01  WF-PROGRAM-FLAGS.                                            PPCTT17G
003000     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT17G
003100         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT17G
003200         88  WF-ACCEPT-TRANSACTION              VALUES 00 THRU 04.PPCTT17G
003300         88  WF-REJECT-TRANSACTION              VALUES 05 THRU 99.PPCTT17G
003400     05  WF-G-TRANS-FLAG               PIC X    VALUE 'N'.        PPCTT17G
003500         88  WF-G-TRANS-END                     VALUE 'Y'.        PPCTT17G
003600         88  WF-G-TRANS-NOT-END                 VALUE 'N'.        PPCTT17G
003700 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT17G
003800     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT17G
003900     05  MCT003                     PIC X(05) VALUE 'CT003'.      PPCTT17G
004000                                                                  PPCTT17G
004100 01  BARG-UNIT-BUG-TABLE-TRANS.                                   PPCTT17G
004200     05  BUGT-ACTION-CODE           PIC X.                        PPCTT17G
004300         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT17G
004400         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT17G
004500         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT17G
004600         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT17G
004700     05  FILLER                     PIC XX.                       PPCTT17G
004800     05  BUGT-BUC                   PIC XX.                       PPCTT17G
004900     05  BUGT-TRAN-CODE             PIC X.                        PPCTT17G
005000     05  BUGT-REP                   PIC X.                        PPCTT17G
005100     05  BUGT-SHC                   PIC X.                        PPCTT17G
005200     05  BUGT-DISTRIBUTION          PIC X.                        PPCTT17G
005300     05  BUGT-GTN-DATA.                                           PPCTT17G
005400         10 BUGT-DATA OCCURS 5 TIMES.                             PPCTT17G
005500             15 BUGT-GTN-DED-NO     PIC XXX.                      PPCTT17G
005600             15 BUGT-GTN-LEVEL      PIC X.                        PPCTT17G
005700     05  FILLER                     PIC X(51).                    PPCTT17G
005800                                                                  PPCTT17G
005900 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT17G
006000 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT17G
006100 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT17G
006200 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT17G
006300 01  BARG-UNIT-BUG-TABLE-INPUT.        COPY CPCTBUGI.             PPCTT17G
006400 01  BARG-UNIT-BUG-TABLE-DATA.                                    PPCTT17G
006500     EXEC SQL                                                     PPCTT17G
006600         INCLUDE PPPVZBUG                                         PPCTT17G
006700     END-EXEC.                                                    PPCTT17G
006800                                                                  PPCTT17G
006900 LINKAGE SECTION.                                                 PPCTT17G
007000                                                                  PPCTT17G
007100 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT17G
007200 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT17G
007300                                                                  PPCTT17G
007400 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE            PPCTT17G
007500                          CSRI-CTL-SORTED-READ-INTERFACE.         PPCTT17G
007600                                                                  PPCTT17G
007700 0000-MAIN SECTION.                                               PPCTT17G
007800                                                                  PPCTT17G
007900     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT17G
008000     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT17G
008100       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT17G
008200          OR WF-G-TRANS-END.                                      PPCTT17G
008300                                                                  PPCTT17G
008400 0099-END.                                                        PPCTT17G
008500                                                                  PPCTT17G
008600     GOBACK.                                                      PPCTT17G
008700                                                                  PPCTT17G
008800 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT17G
008900                                                                  PPCTT17G
009000     MOVE 'PPCTT17G'            TO XWHC-DISPLAY-MODULE.           PPCTT17G
009100     COPY CPPDXWHC.                                               PPCTT17G
009200     SET CTTI-NORMAL            TO TRUE.                          PPCTT17G
009300     SET WF-G-TRANS-NOT-END     TO TRUE.                          PPCTT17G
009400     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTT17G
009500     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTT17G
009600                                   CTRI-TRANSACTION               PPCTT17G
009700                                   CTRI-TRANSACTION-DISPO         PPCTT17G
009800                                   CTRI-TEXT-LINE                 PPCTT17G
009900                                   CTRI-MESSAGE-NUMBER.           PPCTT17G
010000     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTT17G
010100                                   CTTI-ERROR-SEVERITY.           PPCTT17G
010200     MOVE WB-TABLE-NO           TO CSRI-CALL-TYPE-NUM.            PPCTT17G
010300     MOVE CSRI-TRANSACTION      TO BARG-UNIT-BUG-TABLE-TRANS.     PPCTT17G
010400                                                                  PPCTT17G
010500 1099-INITIALIZATION-EXIT.                                        PPCTT17G
010600     EXIT.                                                        PPCTT17G
010700                                                                  PPCTT17G
010800 2000-PROCESS-TRANSACTIONS SECTION.                               PPCTT17G
010900                                                                  PPCTT17G
011000     MOVE CTTI-ACTION-DATE       TO KCTE-LAST-ACTION-DT.          PPCTT17G
011100     MOVE BUGT-ACTION-CODE       TO KCTE-ACTION.                  PPCTT17G
011200     MOVE BUGT-BUC               TO BUGI-BUC.                     PPCTT17G
011300     MOVE BUGT-REP               TO BUGI-REP.                     PPCTT17G
011400     MOVE BUGT-SHC               TO BUGI-SHC.                     PPCTT17G
011500     MOVE BUGT-DISTRIBUTION      TO BUGI-DISTRIBUTION.            PPCTT17G
011600     MOVE BARG-UNIT-BUG-TABLE-TRANS(1:9)                          PPCTT17G
011700                                 TO CTRI-TRANSACTION.             PPCTT17G
011800     IF  KCTE-ACTION-DELETE                                       PPCTT17G
011900     AND BUGT-GTN-DATA = SPACES                                   PPCTT17G
012000         MOVE SPACES TO BUGI-GTN-DED-NO                           PPCTT17G
012100                        BUGI-GTN-LEVEL                            PPCTT17G
012200         PERFORM 2100-EDIT-AND-UPDATE-TRANS                       PPCTT17G
012300     ELSE                                                         PPCTT17G
012400         PERFORM VARYING IX FROM 1 BY 1                           PPCTT17G
012500           UNTIL IX > 5                                           PPCTT17G
012600              OR BUGT-GTN-DED-NO (IX) = SPACES                    PPCTT17G
012700          MOVE BUGT-DATA         (IX) TO CTRI-TRANSACTION(10:4)   PPCTT17G
012800          MOVE BUGT-GTN-DED-NO   (IX) TO BUGI-GTN-DED-NO          PPCTT17G
012900          MOVE BUGT-GTN-LEVEL    (IX) TO BUGI-GTN-LEVEL           PPCTT17G
013000          PERFORM 2100-EDIT-AND-UPDATE-TRANS                      PPCTT17G
013100         END-PERFORM                                              PPCTT17G
013200     END-IF.                                                      PPCTT17G
013300     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT17G
013400                                                                  PPCTT17G
013500 2099-EXIT.                                                       PPCTT17G
013600     EXIT.                                                        PPCTT17G
013700                                                                  PPCTT17G
013800 2100-EDIT-AND-UPDATE-TRANS SECTION.                              PPCTT17G
013900                                                                  PPCTT17G
014000     SET WF-NO-ERRORS TO TRUE.                                    PPCTT17G
014100     CALL 'PPCTBUGE' USING CONTROL-TABLE-EDIT-INTERFACE           PPCTT17G
014200                           BARG-UNIT-BUG-TABLE-INPUT              PPCTT17G
014300                           BARG-UNIT-BUG-TABLE-DATA.              PPCTT17G
014400     IF KCTE-ERR > ZERO                                           PPCTT17G
014500        PERFORM 3100-PRINT-ERROR VARYING WC-ERR-IX FROM 1 BY 1    PPCTT17G
014600          UNTIL WC-ERR-IX > KCTE-ERR                              PPCTT17G
014700     END-IF.                                                      PPCTT17G
014800     IF KCTE-STATUS-NOT-COMPLETED                                 PPCTT17G
014900        MOVE MCT003 TO CTRI-MESSAGE-NUMBER                        PPCTT17G
015000        SET WF-REJECT-TRANSACTION TO TRUE                         PPCTT17G
015100     END-IF.                                                      PPCTT17G
015200     IF WF-ACCEPT-TRANSACTION                                     PPCTT17G
015300        MOVE 'TRANSACTION ACCEPTED' TO CTRI-TEXT-DISPO            PPCTT17G
015400        ADD 1 TO CTTI-TRANS-ACCEPTED                              PPCTT17G
015500        PERFORM 3200-UPDATE-DB2-TABLE                             PPCTT17G
015600     ELSE                                                         PPCTT17G
015700       MOVE 'TRANSACTION REJECTED' TO CTRI-TEXT-DISPO             PPCTT17G
015800       ADD 1 TO CTTI-TRANS-REJECTED                               PPCTT17G
015900     END-IF.                                                      PPCTT17G
016000     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT17G
016100     MOVE 2 TO CTRI-SPACING-OVERRIDE.                             PPCTT17G
016200                                                                  PPCTT17G
016300 2199-EXIT.                                                       PPCTT17G
016400     EXIT.                                                        PPCTT17G
016500                                                                  PPCTT17G
016600 3100-PRINT-ERROR SECTION.                                        PPCTT17G
016700                                                                  PPCTT17G
016800     MOVE KCTE-ERR-NO (WC-ERR-IX) TO CTRI-MESSAGE-NUMBER.         PPCTT17G
016900     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT17G
017000     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT17G
017100         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT17G
017200     END-IF.                                                      PPCTT17G
017300                                                                  PPCTT17G
017400 3199-EXIT.                                                       PPCTT17G
017500     EXIT.                                                        PPCTT17G
017600                                                                  PPCTT17G
017700 3200-UPDATE-DB2-TABLE SECTION.                                   PPCTT17G
017800                                                                  PPCTT17G
017900     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT17G
018000     CALL 'PPCTBUGU' USING CONTROL-TABLE-UPDT-INTERFACE,          PPCTT17G
018100                           BARG-UNIT-BUG-TABLE-DATA.              PPCTT17G
018200     IF KCTU-STATUS-OK                                            PPCTT17G
018300        EVALUATE TRUE                                             PPCTT17G
018400          WHEN KCTU-ACTION-ADD                                    PPCTT17G
018500            ADD 1 TO CTTI-ENTRIES-ADDED                           PPCTT17G
018600          WHEN KCTU-ACTION-CHANGE                                 PPCTT17G
018700            ADD 1 TO CTTI-ENTRIES-UPDATED                         PPCTT17G
018800          WHEN KCTU-ACTION-DELETE                                 PPCTT17G
018900            ADD 1 TO CTTI-ENTRIES-DELETED                         PPCTT17G
019000          WHEN OTHER                                              PPCTT17G
019100            GO TO 3300-BAD-UPDATE-CALL                            PPCTT17G
019200        END-EVALUATE                                              PPCTT17G
019300     ELSE                                                         PPCTT17G
019400        GO TO 3300-BAD-UPDATE-CALL                                PPCTT17G
019500     END-IF.                                                      PPCTT17G
019600                                                                  PPCTT17G
019700 3299-EXIT.                                                       PPCTT17G
019800     EXIT.                                                        PPCTT17G
019900                                                                  PPCTT17G
020000 3300-BAD-UPDATE-CALL SECTION.                                    PPCTT17G
020100                                                                  PPCTT17G
020200     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT17G
020300     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT17G
020400     STRING 'PPCTBUGU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT17G
020500            '" STATUS WAS "'               KCTU-STATUS            PPCTT17G
020600            '" COUNT='                     WA-WORK-Z5             PPCTT17G
020700            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT17G
020800     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT17G
020900     GO TO 9999-ABEND.                                            PPCTT17G
021000                                                                  PPCTT17G
021100 3399-EXIT.                                                       PPCTT17G
021200     EXIT.                                                        PPCTT17G
021300                                                                  PPCTT17G
021400 9100-CALL-PPCTLRPT SECTION.                                      PPCTT17G
021500                                                                  PPCTT17G
021600     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT17G
021700     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT17G
021800         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT17G
021900     END-IF.                                                      PPCTT17G
022000     IF CTRI-RUN-ABORTED                                          PPCTT17G
022100        GO TO 9999-ABEND                                          PPCTT17G
022200     END-IF.                                                      PPCTT17G
022300                                                                  PPCTT17G
022400 9199-EXIT.                                                       PPCTT17G
022500     EXIT.                                                        PPCTT17G
022600                                                                  PPCTT17G
022700 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT17G
022800                                                                  PPCTT17G
022900     PERFORM WITH TEST AFTER                                      PPCTT17G
023000       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT17G
023100          OR CSRI-TRANSACTION(1:1) NOT = '*'                      PPCTT17G
023200      CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE        PPCTT17G
023300      IF CSRI-NORMAL                                              PPCTT17G
023400         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT17G
023500         IF CSRI-TRANSACTION(1:1) = '*'                           PPCTT17G
023600            MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION             PPCTT17G
023700            MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO     PPCTT17G
023800            PERFORM 9100-CALL-PPCTLRPT                            PPCTT17G
023900            ADD 1 TO CTTI-TRANS-ACCEPTED                          PPCTT17G
024000         ELSE                                                     PPCTT17G
024100           IF CSRI-TRANSACTION(6:1) = 'G'                         PPCTT17G
024200             MOVE CSRI-TRANSACTION TO BARG-UNIT-BUG-TABLE-TRANS   PPCTT17G
024300           ELSE                                                   PPCTT17G
024400             SET WF-G-TRANS-END TO TRUE                           PPCTT17G
024500           END-IF                                                 PPCTT17G
024600         END-IF                                                   PPCTT17G
024700      ELSE                                                        PPCTT17G
024800        IF NOT CSRI-TABLE-TRANS-END                               PPCTT17G
024900           MOVE MCT001              TO CTRI-MESSAGE-NUMBER        PPCTT17G
025000           MOVE CSRI-RETURN-CODE    TO WA-RTN-CODE                PPCTT17G
025100           MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                PPCTT17G
025200           MOVE WA-READ-ERROR       TO CTRI-TEXT-LINE             PPCTT17G
025300           PERFORM 9100-CALL-PPCTLRPT                             PPCTT17G
025400           GO TO 9999-ABEND                                       PPCTT17G
025500        END-IF                                                    PPCTT17G
025600      END-IF                                                      PPCTT17G
025700     END-PERFORM.                                                 PPCTT17G
025800                                                                  PPCTT17G
025900 9399-EXIT.                                                       PPCTT17G
026000     EXIT.                                                        PPCTT17G
026100                                                                  PPCTT17G
026200 9999-ABEND SECTION.                                              PPCTT17G
026300                                                                  PPCTT17G
026400     SET CTTI-ABORT TO TRUE.                                      PPCTT17G
026500     GOBACK.                                                      PPCTT17G
026600*************************END OF SOURCE CODE***********************PPCTT17G
