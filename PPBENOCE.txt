000100**************************************************************/   30930413
000200*  PROGRAM: PPBENOCE                                         */   30930413
000300*  RELEASE: ____0413____  SERVICE REQUEST(S): ____3093____   */   30930413
000400*  NAME:____JIM WILLIAMS__CREATION DATE:      __05/23/89__   */   30930413
000500*  DESCRIPTION:                                              */   30930413
000600*  - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II. */   30930413
000700**************************************************************/   30930413
000800**************************************************************/   14240278
000801*  PROGRAM:  PPBENOCE                                        */   14240278
000802*  REL#:  0278   REF: NONE  SERVICE REQUESTS:   ____1424____ */   14240278
000803*  NAME ___BMB_________   MODIFICATION DATE ____01/20/87_____*/   14240278
000804*  DESCRIPTION                                               */   14240278
000805*      THIS CODE WAS REMOVED FROM USER40 AND PLACED IN       */   14240278
000806*   THIS STAND ALONE CALLED MODULE.                          */   14240278
000807*                                                            */   14240278
000808**************************************************************/   14240278
000809 IDENTIFICATION DIVISION.                                         PPBENOCE
000810 PROGRAM-ID. PPBENOCE.                                            PPBENOCE
000820*SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
000830*OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
000840 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENOCE
000850 AUTHOR.                                                          PPBENOCE
000860******************************************************************PPBENOCE
000870******************************************************************PPBENOCE
000880*NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE*PPBENOCE
000890******************************************************************PPBENOCE
000900******************************************************************PPBENOCE
001000* CONSULTANT SERVICES PROVIDED BY:                               *PPBENOCE
001100*                 BRUCE M. BRISCOE                               *PPBENOCE
001200*                 CREATIVELY APPLIED TECHNOLOGIES, INC.          *PPBENOCE
001300*                 7548 BRAIDBURN AVE.                            *PPBENOCE
001400*                 NEWARK, CA 94560                               *PPBENOCE
001500*                  (415) 791-7636                                *PPBENOCE
001600******************************************************************PPBENOCE
001700******************************************************************PPBENOCE
001800*NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE*PPBENOCE
001900******************************************************************PPBENOCE
002000******************************************************************PPBENOCE
002100 DATE-WRITTEN.                                                    PPBENOCE
002200 DATE-COMPILED.                                                   PPBENOCE
002300*REMARKS.                                                         30930413
002400******************************************************************PPBENADD
002500*                                                                *PPBENADD
002600*  THIS MODULE PERFORMS OCERS RETIREMENT DEDUCTION CALCULATIONS. *PPBENADD
002700*                                                                *PPBENADD
002800*  RECOMMENDED PRACTICE: CALLING PROGRAM SHOULD CALL THIS        *PPBENADD
002900* MODULE TO 'INITIALIZE' DURING CALLING PROGRAM INITIALIZATION.  *PPBENADD
003000*                                                                *PPBENADD
003100*                                                                *PPBENADD
003200******************************************************************PPBENADD
003300 ENVIRONMENT DIVISION.                                            PPBENOCE
003400 CONFIGURATION SECTION.                                           PPBENOCE
003500 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
003600 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
003700 SPECIAL-NAMES.                                                   PPBENOCE
003800 INPUT-OUTPUT SECTION.                                            PPBENOCE
003900 FILE-CONTROL.                                                    PPBENOCE
004000     SKIP3                                                        PPBENOCE
004100******************************************************************PPBENOCE
004200*                                                                *PPBENOCE
004300*                         DATA DIVISION                          *PPBENOCE
004400*                                                                *PPBENOCE
004500******************************************************************PPBENOCE
004600 DATA DIVISION.                                                   PPBENOCE
004700 FILE SECTION.                                                    PPBENOCE
004800     SKIP1                                                        PPBENOCE
004900     EJECT                                                        PPBENOCE
005000******************************************************************PPBENOCE
005100*                                                                *PPBENOCE
005200*                    WORKING-STORAGE SECTION                     *PPBENOCE
005300*                                                                *PPBENOCE
005400******************************************************************PPBENOCE
005500 WORKING-STORAGE SECTION.                                         PPBENOCE
005600     SKIP1                                                        PPBENOCE
005700 01  WS-ID                        PIC X(37)                       PPBENOCE
005800     VALUE 'PPBENOCE WORKING-STORAGE BEGINS HERE'.                PPBENOCE
005900     SKIP3                                                        PPBENOCE
006000 01  FLAGS-AND-SWITCHES.                                          PPBENOCE
006100     05  PROGRAM-STATUS-FLAG         PIC X(01)  VALUE '0'.        PPBENOCE
006200         88  PROGRAM-STATUS-NORMAL              VALUE '0'.        PPBENOCE
006300         88  PROGRAM-STATUS-EXITING             VALUE '1'.        PPBENOCE
006400     05  SYSPARM-TABLE-LOADED-SW     PIC X(01)  VALUE 'N'.        PPBENOCE
006500         88  SYSPARM-TABLE-LOADED               VALUE 'Y'.        PPBENOCE
006600     SKIP1                                                        PPBENOCE
006700 01  CONSTANT-VALUES.                                             PPBENOCE
006800     05  CONSTANT-NORMAL             PIC X(01)  VALUE '0'.        PPBENOCE
006900     05  CONSTANT-ABORT              PIC X(01)  VALUE '1'.        PPBENOCE
007000     05  CONSTANT-EXIT-PROGRAM       PIC X(01)  VALUE '1'.        PPBENOCE
007100     SKIP3                                                        PPBENOCE
007200 01  FILLER                       PIC X(37)                       PPBENOCE
007300     VALUE 'PPBENOCE WORKING-STORAGE ENDS HERE'.                  PPBENOCE
007400     EJECT                                                        PPBENOCE
007500******************************************************************PPBENOCE
007600*                                                                *PPBENOCE
007700*                        LINKAGE SECTION                         *PPBENOCE
007800*                                                                *PPBENOCE
007900******************************************************************PPBENOCE
008000 LINKAGE SECTION.                                                 PPBENOCE
008100     SKIP1                                                        PPBENOCE
008200 01  PPBENOCE-INTERFACE.          COPY 'CPLNKOCE'.                PPBENOCE
008300     EJECT                                                        PPBENOCE
008400******************************************************************PPBENOCE
008500*                                                                *PPBENOCE
008600*                       PROCEDURE DIVISION                       *PPBENOCE
008700*                                                                *PPBENOCE
008800******************************************************************PPBENOCE
008900     SKIP1                                                        PPBENOCE
009000 PROCEDURE DIVISION USING PPBENOCE-INTERFACE.                     PPBENOCE
009100     SKIP1                                                        PPBENOCE
009200 A000-MAINLINE SECTION.                                           PPBENOCE
009300     SKIP2                                                        PPBENOCE
009400     MOVE CONSTANT-NORMAL TO PROGRAM-STATUS-FLAG.                 PPBENOCE
009500     SKIP1                                                        PPBENOCE
009600     PERFORM B010-INITIALIZE                                      PPBENOCE
009700        THRU B019-INITIALIZE-EXIT.                                PPBENOCE
009800     SKIP1                                                        PPBENOCE
009900     IF PROGRAM-STATUS-NORMAL                                     PPBENOCE
010000     THEN                                                         PPBENOCE
010100        PERFORM D010-CALCULATE                                    PPBENOCE
010200           THRU D019-CALCULATE-EXIT                               PPBENOCE
010300     ELSE                                                         PPBENOCE
010400         NEXT SENTENCE.                                           PPBENOCE
010500     SKIP3                                                        PPBENOCE
010600 A009-GOBACK.                                                     PPBENOCE
010700     GOBACK.                                                      PPBENOCE
010800     EJECT                                                        PPBENOCE
010900 B000-INITIALIZATION SECTION.                                     PPBENOCE
011000     SKIP2                                                        PPBENOCE
011100 B010-INITIALIZE.                                                 PPBENOCE
011200     SKIP1                                                        PPBENOCE
011300     MOVE CONSTANT-NORMAL TO KOCE-RETURN-STATUS-FLAG.             PPBENOCE
011400*                                                                 PPBENOCE
011500******************************************************************PPBENOCE
011600*      SET NUMERIC RETURN FIELDS TO ZERO                         *PPBENOCE
011700******************************************************************PPBENOCE
011800*                                                                 PPBENOCE
011900     MOVE ZERO TO KOCE-DED-AMOUNT.                                PPBENOCE
012000     SKIP1                                                        PPBENOCE
012100     PERFORM B030-VALIDATE-LOOKUP-ARGS                            PPBENOCE
012200        THRU B039-VALIDATE-LOOKUP-ARGS-EXIT.                      PPBENOCE
012300     SKIP3                                                        PPBENOCE
012400 B019-INITIALIZE-EXIT.                                            PPBENOCE
012500     EXIT.                                                        PPBENOCE
012600     SKIP3                                                        PPBENOCE
012700 B030-VALIDATE-LOOKUP-ARGS.                                       PPBENOCE
012800     SKIP1                                                        PPBENOCE
012900     MOVE '0' TO KOCE-INVALID-LOOKUP-ARG-FLAG.                    PPBENOCE
013000     SKIP1                                                        PPBENOCE
013100     IF KOCE-VALID-SYSTEM-CODE                                    PPBENOCE
013200         NEXT SENTENCE                                            PPBENOCE
013300     ELSE                                                         PPBENOCE
013400         MOVE '1' TO KOCE-INVALID-LOOKUP-ARG-FLAG.                PPBENOCE
013500     SKIP1                                                        PPBENOCE
013600     IF KOCE-INVALID-LOOKUP-ARG                                   PPBENOCE
013700         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENOCE
013800     SKIP3                                                        PPBENOCE
013900 B039-VALIDATE-LOOKUP-ARGS-EXIT.                                  PPBENOCE
014000     EXIT.                                                        PPBENOCE
014100     EJECT                                                        PPBENOCE
014200 D000-ADD-CALC SECTION.                                           PPBENOCE
014300     SKIP2                                                        PPBENOCE
014400 D010-CALCULATE.                                                  PPBENOCE
014500     SKIP1                                                        PPBENOCE
014600     IF KOCE-RETIREMENT-GROSS = ZEROS                             PPBENOCE
014700                  OR                                              PPBENOCE
014800        KOCE-RETIREMENT-RATE = ZEROS                              PPBENOCE
014900         MOVE ZEROS TO KOCE-DED-AMOUNT                            PPBENOCE
015000     ELSE                                                         PPBENOCE
015100         COMPUTE KOCE-DED-AMOUNT ROUNDED =                        PPBENOBE
015200                (KOCE-RETIREMENT-GROSS *                          PPBENOBE
015300                 KOCE-RETIREMENT-RATE) / 100.                     PPBENOBE
015400     SKIP3                                                        PPBENOCE
015500 D019-CALCULATE-EXIT.                                             PPBENOCE
015600     EXIT.                                                        PPBENOCE
