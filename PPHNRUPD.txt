000000**************************************************************/   36450793
000001*  PROGRAM: PPHNRUPD                                         */   36450793
000002*  RELEASE: ___0793______ SERVICE REQUEST(S): _____3645____  */   36450793
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __07/09/93____ */   36450793
000004*  DESCRIPTION: INCORPORATED HONOR_DATE INTO PRIMARY KEY     */   36450793
000005*           FOR EDB ENTRY/UPDATE IMPLEMENTATION.             */   36450793
000007**************************************************************/   36450793
000100**************************************************************/   36330704
000200*  PROGRAM: PPHNRUPD                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPHNR UPDATE MODULE                                */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPHNRUPD
001000 PROGRAM-ID. PPHNRUPD.                                            PPHNRUPD
001100 ENVIRONMENT DIVISION.                                            PPHNRUPD
001200 DATA DIVISION.                                                   PPHNRUPD
001300 WORKING-STORAGE SECTION.                                         PPHNRUPD
001400*                                                                 PPHNRUPD
001500     COPY CPWSXBEG.                                               PPHNRUPD
001600*                                                                 PPHNRUPD
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPHNRUPD'.       PPHNRUPD
001800*                                                                 PPHNRUPD
001900     EXEC SQL                                                     PPHNRUPD
002000         INCLUDE CPWSWRKA                                         PPHNRUPD
002100     END-EXEC.                                                    PPHNRUPD
002200*                                                                 PPHNRUPD
002300 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPHNRUPD
002400*                                                                 PPHNRUPD
002500 01  PPPHNR-ROW EXTERNAL.                                         PPHNRUPD
002600     EXEC SQL                                                     PPHNRUPD
002700         INCLUDE PPPVHNR1                                         PPHNRUPD
002800     END-EXEC.                                                    PPHNRUPD
002900*                                                                 PPHNRUPD
003000*------------------------------------------------------------*    PPHNRUPD
003100*  SQL DCLGEN AREA                                           *    PPHNRUPD
003200*------------------------------------------------------------*    PPHNRUPD
003300     EXEC SQL                                                     PPHNRUPD
003400         INCLUDE SQLCA                                            PPHNRUPD
003500     END-EXEC.                                                    PPHNRUPD
003600*                                                                 PPHNRUPD
003700 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPHNRUPD
003800*                                                                 PPHNRUPD
003900     COPY CPWSXEND.                                               PPHNRUPD
004000*                                                                 PPHNRUPD
004100 LINKAGE SECTION.                                                 PPHNRUPD
004200*                                                                 PPHNRUPD
004300 01  SELECT-INTERFACE-AREA.                                       PPHNRUPD
004400     COPY CPWSXHIF REPLACING ==:TAG:== BY ==XSEL==.               PPHNRUPD
004500*                                                                 PPHNRUPD
004600 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPHNRUPD
004700*                                                                 PPHNRUPD
004800     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPHNRUPD
004900     COPY  CPPDXWHC.                                              PPHNRUPD
005000*                                                                 PPHNRUPD
005100     EXEC SQL                                                     PPHNRUPD
005200         INCLUDE CPPDXE99                                         PPHNRUPD
005300     END-EXEC.                                                    PPHNRUPD
005400*                                                                 PPHNRUPD
005500     COPY CPPDUPDM.                                               PPHNRUPD
005600*                                                                 PPHNRUPD
005700 2000-UPDATE SECTION.                                             PPHNRUPD
005800*                                                                 PPHNRUPD
005900     MOVE 'UPDATE PPPHNR ROW' TO DB2MSG-TAG.                      PPHNRUPD
006000*                                                                 PPHNRUPD
006100     EXEC SQL                                                     PPHNRUPD
006200         UPDATE PPPVHNR1_HNR                                      PPHNRUPD
006300         SET DELETE_FLAG        = :DELETE-FLAG        ,           PPHNRUPD
006400             ERH_INCORRECT      = :ERH-INCORRECT                  36450793
006500*****        HONOR_DATE         = :HONOR-DATE         ,           36450793
006600*****        HONOR_DATE_C       = :HONOR-DATE-C                   36450793
006700         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPHNRUPD
006800         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPHNRUPD
006900         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPHNRUPD
007000         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPHNRUPD
007100         AND   HONOR_TYPE         = :WS-PPPHNR-HONOR-TYPE         PPHNRUPD
007110         AND   HONOR_DATE         = :WS-PPPHNR-HONOR-DATE         36450793
007200     END-EXEC.                                                    PPHNRUPD
007300*                                                                 PPHNRUPD
007400     IF SQLCODE = +100                                            PPHNRUPD
007500         INITIALIZE XSEL-RETURN-KEY                               PPHNRUPD
007600         SET XSEL-INVALID-KEY TO TRUE                             PPHNRUPD
007700     END-IF.                                                      PPHNRUPD
007800*                                                                 PPHNRUPD
007900 2000-EXIT.                                                       PPHNRUPD
008000     EXIT.                                                        PPHNRUPD
008100*                                                                 PPHNRUPD
008200 3000-INSERT SECTION.                                             PPHNRUPD
008300*                                                                 PPHNRUPD
008400     MOVE 'INSERT PPPHNR ROW' TO DB2MSG-TAG.                      PPHNRUPD
008500*                                                                 PPHNRUPD
008600     EXEC SQL                                                     PPHNRUPD
008700         INSERT                                                   PPHNRUPD
008800         INTO PPPVHNR1_HNR                                        PPHNRUPD
008900         VALUES (:PPPHNR-ROW)                                     PPHNRUPD
009000     END-EXEC.                                                    PPHNRUPD
009100*                                                                 PPHNRUPD
009200 3000-EXIT.                                                       PPHNRUPD
009300     EXIT.                                                        PPHNRUPD
009400*                                                                 PPHNRUPD
009500 4000-DELETE SECTION.                                             PPHNRUPD
009600*                                                                 PPHNRUPD
009700     MOVE 'DELETE PPPHNR ROW' TO DB2MSG-TAG.                      PPHNRUPD
009800*                                                                 PPHNRUPD
009900     EXEC SQL                                                     PPHNRUPD
010000         DELETE                                                   PPHNRUPD
010100         FROM PPPVHNR1_HNR                                        PPHNRUPD
010200         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPHNRUPD
010300         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPHNRUPD
010400         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPHNRUPD
010500         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPHNRUPD
010600         AND   HONOR_TYPE         = :WS-PPPHNR-HONOR-TYPE         PPHNRUPD
010610         AND   HONOR_DATE         = :WS-PPPHNR-HONOR-DATE         36450793
010700     END-EXEC.                                                    PPHNRUPD
010800*                                                                 PPHNRUPD
010900     IF SQLCODE = +100                                            PPHNRUPD
011000         INITIALIZE XSEL-RETURN-KEY                               PPHNRUPD
011100         SET XSEL-INVALID-KEY TO TRUE                             PPHNRUPD
011200     END-IF.                                                      PPHNRUPD
011300*                                                                 PPHNRUPD
011400 4000-EXIT.                                                       PPHNRUPD
011500     EXIT.                                                        PPHNRUPD
011600*                                                                 PPHNRUPD
011700*999999-SQL-ERROR SECTION.                                        PPHNRUPD
011800     COPY CPPDXP99.                                               PPHNRUPD
011900     SET XSEL-DB2-ERROR TO TRUE.                                  PPHNRUPD
012000     GOBACK.                                                      PPHNRUPD
012100 999999-EXIT.                                                     PPHNRUPD
012200     EXIT.                                                        PPHNRUPD
012300*                                                                 PPHNRUPD
