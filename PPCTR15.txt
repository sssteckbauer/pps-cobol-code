000100**************************************************************/   30871460
000200*  PROGRAM: PPCTR15                                          */   30871460
000300*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000400*  NAME:___SRS___________ CREATION DATE:      ___01/22/03__  */   30871460
000500*  DESCRIPTION:                                              */   30871460
000600*  PPPRDT TABLE REPORT MODULE                                */   30871460
000700**************************************************************/   30871460
000800 IDENTIFICATION DIVISION.                                         PPCTR15
000900 PROGRAM-ID. PPCTR15.                                             PPCTR15
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTR15
001100 DATE-COMPILED.                                                   PPCTR15
001200 ENVIRONMENT DIVISION.                                            PPCTR15
001300 CONFIGURATION SECTION.                                           PPCTR15
001400 SPECIAL-NAMES.                                                   PPCTR15
001500     C01 IS NEW-PAGE,                                             PPCTR15
001600     CSP IS NO-LINES.                                             PPCTR15
001700 INPUT-OUTPUT SECTION.                                            PPCTR15
001800 FILE-CONTROL.                                                    PPCTR15
001900     SELECT REPORT-PRINT-FILE     ASSIGN TO UT-S-PPP0415.         PPCTR15
002000 DATA DIVISION.                                                   PPCTR15
002100 FILE SECTION.                                                    PPCTR15
002200 FD  REPORT-PRINT-FILE            COPY CPFDXPRT.                  PPCTR15
002300 WORKING-STORAGE SECTION.                                         PPCTR15
002400 01  MISC.                                                        PPCTR15
002500     05  LINE-COUNT               PIC S9(4) COMP SYNC VALUE +99.  PPCTR15
002600     05  NO-OF-LINES              PIC S9(4) COMP SYNC VALUE +1.   PPCTR15
002700     05  PAGE-COUNT               PIC S9(6) COMP SYNC VALUE ZERO. PPCTR15
002800     05  SAVE-REPORT-ID           PIC XX    VALUE SPACES.         PPCTR15
002900 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTR15
003000 01  CTH-CTL-TABLE-MAINT-HEADERS.               COPY CPWSXCHR.    PPCTR15
003100 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTR15
003200 01  RPT15-HD1.                                                   PPCTR15
003300     03  FILLER              PIC X(26) VALUE SPACE.               PPCTR15
003400     03  FILLER              PIC X(06) VALUE 'REPORT'.            PPCTR15
003500     03  FILLER              PIC X(02) VALUE SPACES.              PPCTR15
003600     03  FILLER              PIC X(04) VALUE 'LINE'.              PPCTR15
003700     03  FILLER              PIC X(02) VALUE SPACES.              PPCTR15
003800     03  FILLER              PIC X(05) VALUE 'START'.             PPCTR15
003900     03  FILLER              PIC X(03) VALUE SPACES.              PPCTR15
004000     03  FILLER              PIC X(04) VALUE 'DATA'.              PPCTR15
004100     03  FILLER              PIC X(04) VALUE SPACES.              PPCTR15
004200     03  FILLER              PIC X(11) VALUE 'TRANSLATION'.       PPCTR15
004300     03  FILLER              PIC X(02) VALUE SPACES.              PPCTR15
004400     03  FILLER              PIC X(05) VALUE 'FINAL'.             PPCTR15
004500     03  FILLER              PIC X(04) VALUE SPACES.              PPCTR15
004600     03  FILLER              PIC X(07) VALUE 'DEFAULT'.           PPCTR15
004700 01  RPT15-HD2.                                                   PPCTR15
004800     03  FILLER              PIC X(28) VALUE SPACES.              PPCTR15
004900     03  FILLER              PIC X(02) VALUE 'NO'.                PPCTR15
005000     03  FILLER              PIC X(05) VALUE SPACES.              PPCTR15
005100     03  FILLER              PIC X(02) VALUE 'NO'.                PPCTR15
005200     03  FILLER              PIC X(04) VALUE SPACES.              PPCTR15
005300     03  FILLER              PIC X(03) VALUE 'POS'.               PPCTR15
005400     03  FILLER              PIC X(03) VALUE SPACES.              PPCTR15
005500     03  FILLER              PIC X(07) VALUE 'ELEMENT'.           PPCTR15
005600     03  FILLER              PIC X(05) VALUE SPACES.              PPCTR15
005700     03  FILLER              PIC X(04) VALUE 'EDIT'.              PPCTR15
005800     03  FILLER              PIC X(06) VALUE SPACES.              PPCTR15
005900     03  FILLER              PIC X(06) VALUE 'LENGTH'.            PPCTR15
006000     03  FILLER              PIC X(03) VALUE SPACES.              PPCTR15
006100     03  FILLER              PIC X(06) VALUE ' VALUE'.            PPCTR15
006200 01  RPT15-D1.                                                    PPCTR15
006300     03  FILLER              PIC X(28).                           PPCTR15
006400     03  RPT15-REPORT-ID     PIC XX.                              PPCTR15
006500     03  FILLER              PIC X(5).                            PPCTR15
006600     03  RPT15-LINE-NUMBER   PIC 999.                             PPCTR15
006700     03  FILLER              PIC XXX.                             PPCTR15
006800     03  RPT15-START         PIC 999.                             PPCTR15
006900     03  FILLER              PIC X(4).                            PPCTR15
007000     03  RPT15-ELEMENT       PIC X(4).                            PPCTR15
007100     03  FILLER              PIC X(8).                            PPCTR15
007200     03  RPT15-TRANS-EDIT    PIC ZZ.                              PPCTR15
007300     03  FILLER              PIC X(8).                            PPCTR15
007400     03  RPT15-TRANS-LENGTH  PIC 999.                             PPCTR15
007500     03  FILLER              PIC X(4).                            PPCTR15
007600     03  RPT15-DEFAULT       PIC X(30).                           PPCTR15
007700     03  FILLER     PIC X(26).                                    PPCTR15
007800                                                                  PPCTR15
007900 01  RDT-TABLE-DATA.                                              PPCTR15
008000     EXEC SQL                                                     PPCTR15
008100        INCLUDE PPPVZRDT                                          PPCTR15
008200     END-EXEC.                                                    PPCTR15
008300                                                                  PPCTR15
008400     EXEC SQL                                                     PPCTR15
008500         DECLARE  RDT_ROW CURSOR FOR                              PPCTR15
008600         SELECT   RDT_REPORT_ID                                   PPCTR15
008700                 ,RDT_LINE_NUMBER                                 PPCTR15
008800                 ,RDT_START_POSITION                              PPCTR15
008900                 ,RDT_ELEMENT                                     PPCTR15
009000                 ,RDT_TRANS_EDIT                                  PPCTR15
009100                 ,RDT_TRANS_LENGTH                                PPCTR15
009200                 ,RDT_DEFAULT                                     PPCTR15
009300                 ,RDT_LAST_ACTION                                 PPCTR15
009400                 ,RDT_LAST_ACTION_DT                              PPCTR15
009500         FROM     PPPVZRDT_RDT                                    PPCTR15
009600         ORDER BY RDT_REPORT_ID                                   PPCTR15
009700                 ,RDT_LINE_NUMBER                                 PPCTR15
009800                 ,RDT_START_POSITION                              PPCTR15
009900     END-EXEC.                                                    PPCTR15
010000                                                                  PPCTR15
010100     EXEC SQL                                                     PPCTR15
010200        INCLUDE SQLCA                                             PPCTR15
010300     END-EXEC.                                                    PPCTR15
010400                                                                  PPCTR15
010500 LINKAGE SECTION.                                                 PPCTR15
010600                                                                  PPCTR15
010700 01  CTPI-CTL-TBL-PRINT-INTERFACE.              COPY CPLNCTPI.    PPCTR15
010800                                                                  PPCTR15
010900 PROCEDURE DIVISION USING CTPI-CTL-TBL-PRINT-INTERFACE.           PPCTR15
011000                                                                  PPCTR15
011100 0000-MAIN SECTION.                                               PPCTR15
011200                                                                  PPCTR15
011300     EXEC SQL                                                     PPCTR15
011400        INCLUDE CPPDXE99                                          PPCTR15
011500     END-EXEC.                                                    PPCTR15
011600                                                                  PPCTR15
011700     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTR15
011800     PERFORM 2000-CREATE-REPORT.                                  PPCTR15
011900     PERFORM 9000-PROGRAM-TERMINATION.                            PPCTR15
012000                                                                  PPCTR15
012100 0999-END.                                                        PPCTR15
012200                                                                  PPCTR15
012300     GOBACK.                                                      PPCTR15
012400                                                                  PPCTR15
012500 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTR15
012600                                                                  PPCTR15
012700     MOVE 'PPCTR15' TO XWHC-DISPLAY-MODULE.                       PPCTR15
012800     COPY CPPDXWHC.                                               PPCTR15
012900     MOVE CTPI-HEADER-1 TO CTH-RPT-HD1.                           PPCTR15
013000     MOVE CTPI-HEADER-2 TO CTH-RPT-HD2.                           PPCTR15
013100     MOVE CTPI-HEADER-3 TO CTH-RPT-HD3.                           PPCTR15
013200     MOVE XWHC-MONTH-COMPILED TO CTH-COMPILE-MM.                  PPCTR15
013300     MOVE XWHC-DAY-COMPILED TO CTH-COMPILE-DD.                    PPCTR15
013400     MOVE XWHC-YEAR-COMPILED TO CTH-COMPILE-YY.                   PPCTR15
013500     OPEN OUTPUT REPORT-PRINT-FILE.                               PPCTR15
013600                                                                  PPCTR15
013700 1999-INITIALIZATION-EXIT.                                        PPCTR15
013800     EXIT.                                                        PPCTR15
013900                                                                  PPCTR15
014000 2000-CREATE-REPORT SECTION.                                      PPCTR15
014100                                                                  PPCTR15
014200     PERFORM 7000-OPEN-RDT.                                       PPCTR15
014300     PERFORM 7100-FETCH-RDT.                                      PPCTR15
014400     PERFORM 6000-PAGE-HEADER.                                    PPCTR15
014500                                                                  PPCTR15
014600     PERFORM 3000-PRINT-RDT-TABLE                                 PPCTR15
014700       UNTIL SQLCODE NOT = ZERO.                                  PPCTR15
014800                                                                  PPCTR15
014900     PERFORM 7200-CLOSE-RDT.                                      PPCTR15
015000                                                                  PPCTR15
015100 2999-CREATE-EXIT.                                                PPCTR15
015200     EXIT.                                                        PPCTR15
015300                                                                  PPCTR15
015400 3000-PRINT-RDT-TABLE SECTION.                                    PPCTR15
015500                                                                  PPCTR15
015600     MOVE SPACES TO RPT15-D1.                                     PPCTR15
015700     IF RDT-REPORT-ID NOT = SAVE-REPORT-ID                        PPCTR15
015800        MOVE RDT-REPORT-ID TO SAVE-REPORT-ID                      PPCTR15
015900                              RPT15-REPORT-ID                     PPCTR15
016000        MOVE 3 TO NO-OF-LINES                                     PPCTR15
016100     END-IF.                                                      PPCTR15
016200     MOVE RDT-LINE-NUMBER    TO RPT15-LINE-NUMBER.                PPCTR15
016300     MOVE RDT-START-POSITION TO RPT15-START.                      PPCTR15
016400     MOVE RDT-TRANS-LENGTH   TO RPT15-TRANS-LENGTH.               PPCTR15
016500     MOVE RDT-ELEMENT        TO RPT15-ELEMENT.                    PPCTR15
016600     MOVE RDT-TRANS-EDIT     TO RPT15-TRANS-EDIT.                 PPCTR15
016700     MOVE RDT-DEFAULT        TO RPT15-DEFAULT.                    PPCTR15
016800     MOVE RPT15-D1           TO PRINT-REC.                        PPCTR15
016900     PERFORM 8000-PRINT-A-LINE.                                   PPCTR15
017000                                                                  PPCTR15
017100     PERFORM 7100-FETCH-RDT.                                      PPCTR15
017200                                                                  PPCTR15
017300 3999-PRINT-EXIT.                                                 PPCTR15
017400     EXIT.                                                        PPCTR15
017500                                                                  PPCTR15
017600                                                                  PPCTR15
017700 5000-BUILD-PRINT SECTION.                                        PPCTR15
017800                                                                  PPCTR15
017900                                                                  PPCTR15
018000 5999-BUILD-EXIT.                                                 PPCTR15
018100     EXIT.                                                        PPCTR15
018200                                                                  PPCTR15
018300 6000-PAGE-HEADER SECTION.                                        PPCTR15
018400                                                                  PPCTR15
018500     COPY CPPDXCHR.                                               PPCTR15
018700     MOVE RPT15-HD1 TO PRINT-REC.                                 PPCTR15
018800     MOVE 3 TO NO-OF-LINES.                                       PPCTR15
018900     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR15
019000     MOVE 1 TO NO-OF-LINES.                                       PPCTR15
019100     MOVE RPT15-HD2 TO PRINT-REC.                                 PPCTR15
019200     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR15
019300     ADD 4 TO LINE-COUNT.                                         PPCTR15
019400     MOVE SPACES TO PRINT-REC                                     PPCTR15
019500                    SAVE-REPORT-ID.                               PPCTR15
019600                                                                  PPCTR15
019700 6999-HEADER-EXIT.                                                PPCTR15
019800     EXIT.                                                        PPCTR15
019900                                                                  PPCTR15
020000 7000-OPEN-RDT SECTION.                                           PPCTR15
020100                                                                  PPCTR15
020200     MOVE 'OPEN RDT CURSOR' TO DB2MSG-TAG.                        PPCTR15
020300     EXEC SQL                                                     PPCTR15
020400         OPEN RDT_ROW                                             PPCTR15
020500     END-EXEC.                                                    PPCTR15
020600                                                                  PPCTR15
020700 7099-OPEN-EXIT.                                                  PPCTR15
020800     EXIT.                                                        PPCTR15
020900                                                                  PPCTR15
021000 7100-FETCH-RDT SECTION.                                          PPCTR15
021100                                                                  PPCTR15
021200     MOVE 'FETCH RDT ROW' TO DB2MSG-TAG.                          PPCTR15
021300     EXEC SQL                                                     PPCTR15
021400     FETCH RDT_ROW                                                PPCTR15
021500          INTO                                                    PPCTR15
021600               :RDT-REPORT-ID                                     PPCTR15
021700              ,:RDT-LINE-NUMBER                                   PPCTR15
021800              ,:RDT-START-POSITION                                PPCTR15
021900              ,:RDT-ELEMENT                                       PPCTR15
022000              ,:RDT-TRANS-EDIT                                    PPCTR15
022100              ,:RDT-TRANS-LENGTH                                  PPCTR15
022200              ,:RDT-DEFAULT                                       PPCTR15
022300              ,:RDT-LAST-ACTION                                   PPCTR15
022400              ,:RDT-LAST-ACTION-DT                                PPCTR15
022500     END-EXEC.                                                    PPCTR15
022600                                                                  PPCTR15
022700 7099-FETCH-RDT-EXIT.                                             PPCTR15
022800     EXIT.                                                        PPCTR15
022900                                                                  PPCTR15
023000 7200-CLOSE-RDT SECTION.                                          PPCTR15
023100                                                                  PPCTR15
023200     MOVE 'CLOSE RDT CURSOR' TO DB2MSG-TAG.                       PPCTR15
023300     EXEC SQL                                                     PPCTR15
023400         CLOSE RDT_ROW                                            PPCTR15
023500     END-EXEC.                                                    PPCTR15
023600                                                                  PPCTR15
023700 7299-CLOSE-EXIT.                                                 PPCTR15
023800     EXIT.                                                        PPCTR15
023900                                                                  PPCTR15
024000                                                                  PPCTR15
024100 8000-PRINT-A-LINE SECTION.                                       PPCTR15
024200                                                                  PPCTR15
024300     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR15
024400     MOVE SPACES TO PRINT-REC.                                    PPCTR15
024500     ADD NO-OF-LINES TO LINE-COUNT.                               PPCTR15
024600     IF LINE-COUNT > 54                                           PPCTR15
024700        PERFORM 6000-PAGE-HEADER                                  PPCTR15
024800     END-IF.                                                      PPCTR15
024900     MOVE 1 TO NO-OF-LINES.                                       PPCTR15
025000                                                                  PPCTR15
025100 8999-PRINT-EXIT.                                                 PPCTR15
025200     EXIT.                                                        PPCTR15
025300                                                                  PPCTR15
025400 9000-PROGRAM-TERMINATION SECTION.                                PPCTR15
025500                                                                  PPCTR15
025600     CLOSE REPORT-PRINT-FILE.                                     PPCTR15
025700                                                                  PPCTR15
025800 9999-TERMINATION-EXIT.                                           PPCTR15
025900     EXIT.                                                        PPCTR15
026000                                                                  PPCTR15
026100*999999-SQL-ERROR.                                                PPCTR15
026200     EXEC SQL                                                     PPCTR15
026300        INCLUDE CPPDXP99                                          PPCTR15
026400     END-EXEC.                                                    PPCTR15
026500     MOVE '99' TO CTPI-RETURN-CODE.                               PPCTR15
026600     GO TO 0999-END.                                              PPCTR15
