000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEG283                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    DCP PLAN DEDUCTION MAINTENANCE                          *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEG283
001200 IDENTIFICATION DIVISION.                                         PPEG283
001300 PROGRAM-ID. PPEG283.                                             PPEG283
001400 AUTHOR. UCOP.                                                    PPEG283
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEG283
001600                                                                  PPEG283
001700 DATE-WRITTEN.  AUGUST 06,1992.                                   PPEG283
001800 DATE-COMPILED.                                                   PPEG283
001900                                                                  PPEG283
002500 ENVIRONMENT DIVISION.                                            PPEG283
002600 CONFIGURATION SECTION.                                           PPEG283
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEG283
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEG283
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEG283
003000                                                                  PPEG283
003100 INPUT-OUTPUT SECTION.                                            PPEG283
003200 FILE-CONTROL.                                                    PPEG283
003300                                                                  PPEG283
003400 DATA DIVISION.                                                   PPEG283
003500 FILE SECTION.                                                    PPEG283
003600     EJECT                                                        PPEG283
003700*----------------------------------------------------------------*PPEG283
003800 WORKING-STORAGE SECTION.                                         PPEG283
003900*----------------------------------------------------------------*PPEG283
004000                                                                  PPEG283
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEG283
004200     'PPEG283 /111992'.                                           PPEG283
004300                                                                  PPEG283
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEG283
004500     A-STND-PROG-ID.                                              PPEG283
004600     05  FILLER                       PIC X(03).                  PPEG283
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEG283
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEG283
004900     05  FILLER                       PIC X(08).                  PPEG283
005000                                                                  PPEG283
005100 01  MESSAGES-USED.                                               PPEG283
005200     05  M12223               PIC X(05)  VALUE '12223'.           PPEG283
005300                                                                  PPEG283
005400 01  ELEMENTS-USED.                                               PPEG283
005500     05  E6283                PIC 9(04)  VALUE  6283.             PPEG283
005600                                                                  PPEG283
005700 01  GTN-ELEMENTS-USED.                                           PPEG283
005800     05  G283                 PIC 9(04)  VALUE   283.             PPEG283
005900                                                                  PPEG283
006000     EJECT                                                        PPEG283
006100 01  MISC-WORK-AREAS.                                             PPEG283
006200     05  FIRST-TIME-SW            PIC X(01)      VALUE LOW-VALUES.PPEG283
006300         88  THIS-IS-THE-FIRST-TIME              VALUE LOW-VALUES.PPEG283
006400         88  NOT-THE-FIRST-TIME                  VALUE 'Y'.       PPEG283
006500     05  DECL-BAL-P-UNPACK        PIC 9(5)V99    VALUE ZERO.      PPEG283
006600     05  DISP-DECL-BAL-P-UNPACK   REDEFINES                       PPEG283
006700         DECL-BAL-P-UNPACK        PIC  X(7).                      PPEG283
006800     05  DSA-GTN-P                PIC S9(4) COMP VALUE +1.        PPEG283
006900                                                                  PPEG283
007000     EJECT                                                        PPEG283
007100 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEG283
007200                                                                  PPEG283
007300     EJECT                                                        PPEG283
007400 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPEG283
007500                                                                  PPEG283
007600     EJECT                                                        PPEG283
007700 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEG283
007800                                                                  PPEG283
007900     EJECT                                                        PPEG283
008000 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEG283
008100                                                                  PPEG283
008200     EJECT                                                        PPEG283
008300 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPEG283
008400                                                                  PPEG283
008500     EJECT                                                        PPEG283
008600 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEG283
008700                                                                  PPEG283
008800     EJECT                                                        PPEG283
008900 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEG283
009000                                                                  PPEG283
009100     EJECT                                                        PPEG283
009200******************************************************************PPEG283
009300**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEG283
009400**--------------------------------------------------------------**PPEG283
009500** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEG283
009600******************************************************************PPEG283
009700                                                                  PPEG283
009800 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEG283
009900                                                                  PPEG283
010000     EJECT                                                        PPEG283
010100 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEG283
010200                                                                  PPEG283
010300     EJECT                                                        PPEG283
010400 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEG283
010500                                                                  PPEG283
010600     EJECT                                                        PPEG283
010700 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEG283
010800                                                                  PPEG283
010900 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEG283
011000                                                                  PPEG283
011100 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEG283
011200                                                                  PPEG283
011300 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEG283
011400                                                                  PPEG283
011500     EJECT                                                        PPEG283
011600 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEG283
011700                                                                  PPEG283
011800     EJECT                                                        PPEG283
011900 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEG283
012000                                                                  PPEG283
012100     EJECT                                                        PPEG283
012200 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEG283
012300                                                                  PPEG283
012400     EJECT                                                        PPEG283
012500 01  DEDUCTION-SEGMENT-ARRAY             EXTERNAL. COPY CPWSEDSA. PPEG283
012600                                                                  PPEG283
012700     EJECT                                                        PPEG283
012800******************************************************************PPEG283
012900**  DATA BASE AREAS                                             **PPEG283
013000******************************************************************PPEG283
013100                                                                  PPEG283
013200 01  SCR-ROW                             EXTERNAL.                PPEG283
013300     EXEC SQL INCLUDE PPPV120B END-EXEC.                          PPEG283
013400                                                                  PPEG283
013500     EJECT                                                        PPEG283
013600 01  PER-ROW                             EXTERNAL.                PPEG283
013700     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEG283
013800                                                                  PPEG283
013900     EJECT                                                        PPEG283
014000 01  BEL-ROW                             EXTERNAL.                PPEG283
014100     EXEC SQL INCLUDE PPPVBEL1 END-EXEC.                          PPEG283
014200                                                                  PPEG283
014300     EJECT                                                        PPEG283
014400 01  BEN-ROW                             EXTERNAL.                PPEG283
014500     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPEG283
014600                                                                  PPEG283
014700     EJECT                                                        PPEG283
014800******************************************************************PPEG283
014900**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEG283
015000******************************************************************PPEG283
015100                                                                  PPEG283
015200*----------------------------------------------------------------*PPEG283
015300 LINKAGE SECTION.                                                 PPEG283
015400*----------------------------------------------------------------*PPEG283
015500                                                                  PPEG283
015600******************************************************************PPEG283
015700**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEG283
015800**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEG283
015900******************************************************************PPEG283
016000                                                                  PPEG283
016100 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEG283
016200                                                                  PPEG283
016300     EJECT                                                        PPEG283
016400******************************************************************PPEG283
016500**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEG283
016600**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEG283
016700******************************************************************PPEG283
016800                                                                  PPEG283
016900 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEG283
017000                                                                  PPEG283
017100     EJECT                                                        PPEG283
017200******************************************************************PPEG283
017300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEG283
017400******************************************************************PPEG283
017500                                                                  PPEG283
017600 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEG283
017700                          KMTA-MESSAGE-TABLE-ARRAY.               PPEG283
017800                                                                  PPEG283
017900                                                                  PPEG283
018000*----------------------------------------------------------------*PPEG283
018100 0000-DRIVER.                                                     PPEG283
018200*----------------------------------------------------------------*PPEG283
018300                                                                  PPEG283
018400******************************************************************PPEG283
018500**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEG283
018600******************************************************************PPEG283
018700                                                                  PPEG283
018800     IF  THIS-IS-THE-FIRST-TIME                                   PPEG283
018900         PERFORM 0100-INITIALIZE                                  PPEG283
019000     END-IF.                                                      PPEG283
019100                                                                  PPEG283
019200******************************************************************PPEG283
019300**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEG283
019400**  TO THE CALLING PROGRAM                                      **PPEG283
019500******************************************************************PPEG283
019600                                                                  PPEG283
019700     PERFORM 1000-MAINLINE-ROUTINE.                               PPEG283
019800                                                                  PPEG283
019900     EXIT PROGRAM.                                                PPEG283
020000                                                                  PPEG283
020100                                                                  PPEG283
020200     EJECT                                                        PPEG283
020300*----------------------------------------------------------------*PPEG283
020400 0100-INITIALIZE    SECTION.                                      PPEG283
020500*----------------------------------------------------------------*PPEG283
020600                                                                  PPEG283
020700******************************************************************PPEG283
020800**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEG283
020900******************************************************************PPEG283
021000                                                                  PPEG283
021100     MOVE A-STND-PROG-ID          TO XWHC-DISPLAY-MODULE.         PPEG283
021200                                                                  PPEG283
021300*                                               *-------------*   PPEG283
021400                                                 COPY CPPDXWHC.   PPEG283
021500*                                               *-------------*   PPEG283
021600                                                                  PPEG283
021700*                                               *-------------*   PPEG283
021800                                                 COPY CPPDDATE.   PPEG283
021900*                                               *-------------*   PPEG283
022000                                                                  PPEG283
022100     MOVE PRE-EDIT-DATE          TO DATE-WO-CC.                   PPEG283
022200     SET NOT-THE-FIRST-TIME      TO TRUE.                         PPEG283
022300                                                                  PPEG283
022400                                                                  PPEG283
022500     EJECT                                                        PPEG283
022600*----------------------------------------------------------------*PPEG283
022700 1000-MAINLINE-ROUTINE SECTION.                                   PPEG283
022800*----------------------------------------------------------------*PPEG283
022900                                                                  PPEG283
023000     MOVE RET-ELIG-CODE           TO W88-RET-ELIG-CODE.           PPEG283
023100                                                                  PPEG283
023200     IF      EDSA-BALAMT (G283, DSA-GTN-P) NOT = ZERO             PPEG283
023300         AND NOT RET-ELIG-CODE-UCRS-RETR-ND                       PPEG283
023400         AND NOT RET-ELIG-CODE-UCRS-RETR-FY-ND                    PPEG283
023500                                                                  PPEG283
023600         MOVE ZEROES TO EDSA-BALAMT (G283, DSA-GTN-P)             PPEG283
023700         MOVE E6283               TO DL-FIELD                     PPEG283
023800         PERFORM 9050-AUDITING-RESPONSIBILITIES                   PPEG283
023900                                                                  PPEG283
024000         ADD 1                    TO KMTA-CTR                     PPEG283
024100         MOVE M12223              TO KMTA-MSG-NUMBER   (KMTA-CTR) PPEG283
024200         MOVE WS-ROUTINE-TYPE     TO KMTA-ROUTINE-TYPE (KMTA-CTR) PPEG283
024300         MOVE WS-ROUTINE-NUM      TO KMTA-ROUTINE-NUM  (KMTA-CTR) PPEG283
024400     END-IF.                                                      PPEG283
024500                                                                  PPEG283
024600     EJECT                                                        PPEG283
024700*----------------------------------------------------------------*PPEG283
024800 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEG283
024900*----------------------------------------------------------------*PPEG283
025000                                                                  PPEG283
025100*                                                 *-------------* PPEG283
025200                                                   COPY CPPDXDEC. PPEG283
025300*                                                 *-------------* PPEG283
025400                                                                  PPEG283
025500*----------------------------------------------------------------*PPEG283
025600 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEG283
025700*----------------------------------------------------------------*PPEG283
025800                                                                  PPEG283
025900*                                                 *-------------* PPEG283
026000                                                   COPY CPPDADLC. PPEG283
026100*                                                 *-------------* PPEG283
026200                                                                  PPEG283
026300     EJECT                                                        PPEG283
026400*----------------------------------------------------------------*PPEG283
026500 9300-DATE-CONVERSION-DB2  SECTION.                               PPEG283
026600*----------------------------------------------------------------*PPEG283
026700                                                                  PPEG283
026800*                                                 *-------------* PPEG283
026900                                                   COPY CPPDXDC2. PPEG283
027000*                                                 *-------------* PPEG283
027100                                                                  PPEG283
027200******************************************************************PPEG283
027300**   E N D  S O U R C E   ----  PPEG283 ----                    **PPEG283
027400******************************************************************PPEG283
