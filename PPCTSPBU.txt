000100**************************************************************/   30871465
000200*  PROGRAM: PPCTSPBU                                         */   30871465
000300*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000400*  NAME:___SRS___________ CREATION DATE:      ___02/11/03__  */   30871465
000500*  DESCRIPTION:                                              */   30871465
000600*  SUREPAY BANK TABLE UPDATE MODULE                          */   30871465
000700**************************************************************/   30871465
000800 IDENTIFICATION DIVISION.                                         PPCTSPBU
000900 PROGRAM-ID. PPCTSPBU.                                            PPCTSPBU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTSPBU
001100 DATE-COMPILED.                                                   PPCTSPBU
001200 ENVIRONMENT DIVISION.                                            PPCTSPBU
001300 DATA DIVISION.                                                   PPCTSPBU
001400 WORKING-STORAGE SECTION.                                         PPCTSPBU
001500                                                                  PPCTSPBU
001600 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTSPBU
001700 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTSPBU
001800                                                                  PPCTSPBU
001900     EXEC SQL                                                     PPCTSPBU
002000        INCLUDE SQLCA                                             PPCTSPBU
002100     END-EXEC.                                                    PPCTSPBU
002200                                                                  PPCTSPBU
002300 LINKAGE SECTION.                                                 PPCTSPBU
002400                                                                  PPCTSPBU
002500 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTSPBU
002600 01  SUREPAY-BANK-SPB-TABLE-DATA.                                 PPCTSPBU
002700     EXEC SQL                                                     PPCTSPBU
002800        INCLUDE PPPVZSPB                                          PPCTSPBU
002900     END-EXEC.                                                    PPCTSPBU
003000                                                                  PPCTSPBU
003100 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE            PPCTSPBU
003200                          SUREPAY-BANK-SPB-TABLE-DATA.            PPCTSPBU
003300                                                                  PPCTSPBU
003400 0000-MAIN.                                                       PPCTSPBU
003500                                                                  PPCTSPBU
003600     EXEC SQL                                                     PPCTSPBU
003700        INCLUDE CPPDXE99                                          PPCTSPBU
003800     END-EXEC.                                                    PPCTSPBU
003900     MOVE 'PPCTSPBU' TO XWHC-DISPLAY-MODULE.                      PPCTSPBU
004000     COPY CPPDXWHC.                                               PPCTSPBU
004100     MOVE SPACE TO KCTU-STATUS.                                   PPCTSPBU
004200     IF KCTU-ACTION-ADD                                           PPCTSPBU
004300        PERFORM 2000-INSERT-SPB                                   PPCTSPBU
004400     ELSE                                                         PPCTSPBU
004500       IF KCTU-ACTION-CHANGE                                      PPCTSPBU
004600          PERFORM 2100-UPDATE-SPB                                 PPCTSPBU
004700       ELSE                                                       PPCTSPBU
004800         IF KCTU-ACTION-DELETE                                    PPCTSPBU
004900            PERFORM 2200-DELETE-SPB                               PPCTSPBU
005000         ELSE                                                     PPCTSPBU
005100            SET KCTU-INVALID-ACTION TO TRUE                       PPCTSPBU
005200         END-IF                                                   PPCTSPBU
005300       END-IF                                                     PPCTSPBU
005400     END-IF.                                                      PPCTSPBU
005500                                                                  PPCTSPBU
005600 0100-END.                                                        PPCTSPBU
005700                                                                  PPCTSPBU
005800     GOBACK.                                                      PPCTSPBU
005900                                                                  PPCTSPBU
006000 2000-INSERT-SPB.                                                 PPCTSPBU
006100                                                                  PPCTSPBU
006200     MOVE '2000-INSRT-SPB' TO DB2MSG-TAG.                         PPCTSPBU
006300     EXEC SQL                                                     PPCTSPBU
006400         INSERT INTO PPPVZSPB_SPB                                 PPCTSPBU
006500              VALUES (:SPB-BANK-NUMBER                            PPCTSPBU
006600                     ,:SPB-TRANSIT-RTE-NO                         PPCTSPBU
006700                     ,:SPB-ACCT-FMT-CHECK                         PPCTSPBU
006800                     ,:SPB-ACCT-FMT-SAVE                          PPCTSPBU
006900                     ,:SPB-BANK-NAME                              PPCTSPBU
007000                     ,:SPB-COMMENT-1                              PPCTSPBU
007100                     ,:SPB-COMMENT-2                              PPCTSPBU
007200                     ,:SPB-LAST-ACTION                            PPCTSPBU
007300                     ,:SPB-LAST-ACTION-DT                         PPCTSPBU
007400                     )                                            PPCTSPBU
007500     END-EXEC.                                                    PPCTSPBU
007600     IF SQLCODE NOT = ZERO                                        PPCTSPBU
007700        SET KCTU-INSERT-ERROR TO TRUE                             PPCTSPBU
007800     END-IF.                                                      PPCTSPBU
007900                                                                  PPCTSPBU
008000 2100-UPDATE-SPB.                                                 PPCTSPBU
008100                                                                  PPCTSPBU
008200     MOVE '2100-UPD-SPB ' TO DB2MSG-TAG.                          PPCTSPBU
008300     EXEC SQL                                                     PPCTSPBU
008400         UPDATE PPPVZSPB_SPB                                      PPCTSPBU
008500            SET SPB_BANK_NUMBER       = :SPB-BANK-NUMBER          PPCTSPBU
008600               ,SPB_TRANSIT_RTE_NO    = :SPB-TRANSIT-RTE-NO       PPCTSPBU
008700               ,SPB_ACCT_FMT_CHECK    = :SPB-ACCT-FMT-CHECK       PPCTSPBU
008800               ,SPB_ACCT_FMT_SAVE     = :SPB-ACCT-FMT-SAVE        PPCTSPBU
008900               ,SPB_BANK_NAME         = :SPB-BANK-NAME            PPCTSPBU
009000               ,SPB_COMMENT_1         = :SPB-COMMENT-1            PPCTSPBU
009100               ,SPB_COMMENT_2         = :SPB-COMMENT-2            PPCTSPBU
009200               ,SPB_LAST_ACTION       = :SPB-LAST-ACTION          PPCTSPBU
009300               ,SPB_LAST_ACTION_DT    = :SPB-LAST-ACTION-DT       PPCTSPBU
009400          WHERE SPB_BANK_NUMBER       = :SPB-BANK-NUMBER          PPCTSPBU
009500     END-EXEC.                                                    PPCTSPBU
009600     IF SQLCODE NOT = ZERO                                        PPCTSPBU
009700        SET KCTU-UPDATE-ERROR TO TRUE                             PPCTSPBU
009800     END-IF.                                                      PPCTSPBU
009900                                                                  PPCTSPBU
010000 2200-DELETE-SPB.                                                 PPCTSPBU
010100                                                                  PPCTSPBU
010200     MOVE '2200-DLTE-SPB ' TO DB2MSG-TAG.                         PPCTSPBU
010300     EXEC SQL                                                     PPCTSPBU
010400         DELETE FROM PPPVZSPB_SPB                                 PPCTSPBU
010500          WHERE SPB_BANK_NUMBER       = :SPB-BANK-NUMBER          PPCTSPBU
010600     END-EXEC.                                                    PPCTSPBU
010700     IF SQLCODE NOT = ZERO                                        PPCTSPBU
010800        SET KCTU-DELETE-ERROR TO TRUE                             PPCTSPBU
010900     END-IF.                                                      PPCTSPBU
011000                                                                  PPCTSPBU
011100*999999-SQL-ERROR.                                                PPCTSPBU
011200     EXEC SQL                                                     PPCTSPBU
011300        INCLUDE CPPDXP99                                          PPCTSPBU
011400     END-EXEC.                                                    PPCTSPBU
011410     EVALUATE TRUE                                                PPCTSPBU
011420       WHEN (KCTU-ACTION-ADD)                                     PPCTSPBU
011430         SET KCTU-INSERT-ERROR TO TRUE                            PPCTSPBU
011440       WHEN (KCTU-ACTION-CHANGE)                                  PPCTSPBU
011450         SET KCTU-UPDATE-ERROR TO TRUE                            PPCTSPBU
011460       WHEN (KCTU-ACTION-DELETE)                                  PPCTSPBU
011470         SET KCTU-DELETE-ERROR TO TRUE                            PPCTSPBU
011480     END-EVALUATE.                                                PPCTSPBU
011500     GO TO 0100-END.                                              PPCTSPBU
