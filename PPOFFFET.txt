000000**************************************************************/   28521087
000001*  PROGRAM: PPOFFFET                                         */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/22/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  ** DATE CONVERSION II **                                  */   28521087
000006*  - REPLACED CPWSXDC2 AND CPPDXDC2 WITH CPWSXDC3 AND CPPDXDC3/   28521087
000007*  - COMMENTED PARAGRAPH 999999-EXIT. CAN NEVER BE EXECUTED  */   28521087
000008**************************************************************/   28521087
000100**************************************************************/   05490748
000200*  PROGRAM: PPOFFFET                                         */   05490748
000300*  RELEASE: ____0748____  SERVICE REQUEST(S): ____0549____   */   05490748
000400*  NAME:__J LUCAS ________CREATION DATE:      __03/05/93__   */   05490748
000500*  DESCRIPTION:                                              */   05490748
000600*  -ADDED CODE TO INCLUDE LAYOFF-NOTICE-DATE AS PART OF THE  */   05490748
000700*   KEY FOR THE LAYOFF TABLE.                                */   05490748
000800**************************************************************/   05490748
000900**************************************************************/   36290724
001000*  PROGRAM: PPOFFFET                                         */   36290724
001100*  RELEASE # ___0724___   SERVICE REQUEST NO(S)____3629______*/   36290724
001200*  NAME __J. LUCAS_____   CREATION     DATE ____12/11/92_____*/   36290724
001300*  DESCRIPTION                                               */   36290724
001400*    THE PPPOFF SELECT MODULE                                */   36290724
001500*                                                            */   36290724
001600**************************************************************/   36290724
001700 IDENTIFICATION DIVISION.                                         PPOFFFET
001800 PROGRAM-ID. PPOFFFET.                                            PPOFFFET
001900 ENVIRONMENT DIVISION.                                            PPOFFFET
002000 DATA DIVISION.                                                   PPOFFFET
002100 WORKING-STORAGE SECTION.                                         PPOFFFET
002200*                                                                 PPOFFFET
002300     COPY CPWSXBEG.                                               PPOFFFET
002400*                                                                 PPOFFFET
002500 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPOFFFET'.       PPOFFFET
002600*                                                                 PPOFFFET
002700     EXEC SQL                                                     PPOFFFET
002800         INCLUDE CPWSWRKA                                         PPOFFFET
002900     END-EXEC.                                                    PPOFFFET
003000*                                                                 PPOFFFET
003100*01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC2.                28521087
003110 01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC3.                28521087
003200 01  WS-NOTICE-DATE            PIC  X(10) VALUE SPACES.           05490748
003300*                                                                 PPOFFFET
003400 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPOFFFET
003500*                                                                 PPOFFFET
003600 01  PPPOFF-ROW EXTERNAL.                                         PPOFFFET
003700     EXEC SQL                                                     PPOFFFET
003800         INCLUDE PPPVOFF1                                         PPOFFFET
003900     END-EXEC.                                                    PPOFFFET
004000*                                                                 PPOFFFET
004100*------------------------------------------------------------*    PPOFFFET
004200*  SQL DCLGEN AREA                                           *    PPOFFFET
004300*------------------------------------------------------------*    PPOFFFET
004400     EXEC SQL                                                     PPOFFFET
004500         INCLUDE SQLCA                                            PPOFFFET
004600     END-EXEC.                                                    PPOFFFET
004700*                                                                 PPOFFFET
004800 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPOFFFET
004900*                                                                 PPOFFFET
005000     COPY CPWSXEND.                                               PPOFFFET
005100*                                                                 PPOFFFET
005200 LINKAGE SECTION.                                                 PPOFFFET
005300*                                                                 PPOFFFET
005400 01  SELECT-INTERFACE-AREA.                                       PPOFFFET
005500     COPY CPWSXHIF REPLACING ==:TAG:== BY ==XSEL==.               PPOFFFET
005600*                                                                 PPOFFFET
005700 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPOFFFET
005800*                                                                 PPOFFFET
005900     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPOFFFET
006000     COPY  CPPDXWHC.                                              PPOFFFET
006100*                                                                 PPOFFFET
006200     EXEC SQL                                                     PPOFFFET
006300         INCLUDE CPPDXE99                                         PPOFFFET
006400     END-EXEC.                                                    PPOFFFET
006500*                                                                 PPOFFFET
006600     COPY CPPDSELM.                                               PPOFFFET
006700*                                                                 PPOFFFET
006800 2000-SELECT SECTION.                                             PPOFFFET
006900*                                                                 PPOFFFET
007000*                                                                 PPOFFFET
007100*****MOVE WS-PPPOFF-NOTICE-DATE TO WSX-STD-DATE.                  28521087
007200*****PERFORM CONVERT-STD-TO-DB-FMT-DATE.                          28521087
007300*****MOVE XDC2-DB-FMT-DATE TO WS-NOTICE-DATE.                     28521087
007310     MOVE WS-PPPOFF-NOTICE-DATE TO XDC3-STD-DATE.                 28521087
007320     PERFORM XDC3-CONVERT-STD-TO-USA.                             28521087
007330     MOVE XDC3-USA-DATE TO WS-NOTICE-DATE.                        28521087
007400*                                                                 05490748
007500     MOVE 'SELECT MAX TIMESTAMP' TO DB2MSG-TAG.                   PPOFFFET
007600     STRING WS-QUERY-DATE '-' WS-QUERY-TIME                       PPOFFFET
007700       DELIMITED BY SIZE INTO WS-QUERY-TIMESTAMP.                 PPOFFFET
007800*                                                                 PPOFFFET
007900     EXEC SQL                                                     PPOFFFET
008000         SELECT VALUE(MAX(TIMESTAMP                               PPOFFFET
008100               (SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME)),            PPOFFFET
008200                TIMESTAMP('0001-01-01-00.00.00'))                 PPOFFFET
008300         INTO :WS-MAX-TIMESTAMP                                   PPOFFFET
008400         FROM PPPVOFF1_OFF                                        PPOFFFET
008500         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPOFFFET
008600         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPOFFFET
008700         AND TIMESTAMP(SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME) ^>    PPOFFFET
008800             :WS-QUERY-TIMESTAMP                                  PPOFFFET
008900         AND   LAYOFF_TITLE_CODE  = :WS-PPPOFF-TITLE-CODE         PPOFFFET
009000         AND   LAYOFF_DEPT_CODE   = :WS-PPPOFF-DEPT-CODE          PPOFFFET
009100         AND   LAYOFF_NOTICE_DATE = :WS-NOTICE-DATE               05490748
009200     END-EXEC.                                                    PPOFFFET
009300*                                                                 PPOFFFET
009400     IF SQLCODE = +100                                            PPOFFFET
009500     OR WS-MAX-TIMESTAMP = '0001-01-01-00.00.00'                  PPOFFFET
009600        INITIALIZE XSEL-RETURN-KEY                                PPOFFFET
009700                   PPPOFF-ROW                                     PPOFFFET
009800        SET XSEL-INVALID-KEY TO TRUE                              PPOFFFET
009900        GO TO 2000-EXIT                                           PPOFFFET
010000     END-IF.                                                      PPOFFFET
010100*                                                                 PPOFFFET
010200     MOVE 'SELECT PPPOFF ROW' TO DB2MSG-TAG.                      PPOFFFET
010300*                                                                 PPOFFFET
010400     EXEC SQL                                                     PPOFFFET
010500          SELECT                                                  PPOFFFET
010600             EMPLID             ,                                 PPOFFFET
010700             ITERATION_NUMBER   ,                                 PPOFFFET
010800             CHAR(SYSTEM_ENTRY_DATE, ISO),                        PPOFFFET
010900             CHAR(SYSTEM_ENTRY_TIME, ISO),                        PPOFFFET
011000             LAYOFF_TITLE_CODE    ,                               PPOFFFET
011100             LAYOFF_DEPT_CODE     ,                               PPOFFFET
011200             DELETE_FLAG          ,                               PPOFFFET
011300             ERH_INCORRECT        ,                               PPOFFFET
011400             LAYOFF_UNIT_CODE     ,                               PPOFFFET
011500             LAYOFF_UNIT_CODE_C   ,                               PPOFFFET
011600             LAYOFF_NOTICE_DATE   ,                               PPOFFFET
011700             LAYOFF_NTCE_DATE_C   ,                               PPOFFFET
011800             LAYOFF_SALARY        ,                               PPOFFFET
011900             LAYOFF_SALARY_C      ,                               PPOFFFET
012000             LAYOFF_SALARY_IND    ,                               PPOFFFET
012100             LAYOFF_SALRY_IND_C   ,                               PPOFFFET
012200             LAYOFF_PERCNT_TIME   ,                               PPOFFFET
012300             LAYOFF_PCNT_TIME_C   ,                               PPOFFFET
012400             REHIRE_BGN_DATE      ,                               PPOFFFET
012500             REHIRE_BGN_DATE_C    ,                               PPOFFFET
012600             REHIRE_EXP_DATE      ,                               PPOFFFET
012700             REHIRE_EXP_DATE_C    ,                               PPOFFFET
012800             RHRE_SUSP_BGN_TERM   ,                               PPOFFFET
012900             RHRE_SUS_BGN_TER_C   ,                               PPOFFFET
013000             REHIRE_SUSP_END      ,                               PPOFFFET
013100             REHIRE_SUSP_END_C    ,                               PPOFFFET
013200             RHRE_SUSP_TERM_RSN   ,                               PPOFFFET
013300             RHRE_SUS_TER_RSN_C   ,                               PPOFFFET
013400             RECALL_BGN_DATE      ,                               PPOFFFET
013500             RECALL_BGN_DATE_C    ,                               PPOFFFET
013600             RECALL_EXP_DATE      ,                               PPOFFFET
013700             RECALL_EXP_DATE_C    ,                               PPOFFFET
013800             RCLL_SUSP_BGN_TERM   ,                               PPOFFFET
013900             RCLL_SUS_BGN_TER_C   ,                               PPOFFFET
014000             RECALL_SUSP_END      ,                               PPOFFFET
014100             RECALL_SUSP_END_C    ,                               PPOFFFET
014200             RCLL_SUSP_TERM_RSN   ,                               PPOFFFET
014300             RCLL_SUS_TER_RSN_C                                   PPOFFFET
014400       INTO :EMPLID             ,                                 PPOFFFET
014500            :ITERATION-NUMBER   ,                                 PPOFFFET
014600            :SYSTEM-ENTRY-DATE  ,                                 PPOFFFET
014700            :SYSTEM-ENTRY-TIME  ,                                 PPOFFFET
014800            :LAYOFF-TITLE-CODE    ,                               PPOFFFET
014900            :LAYOFF-DEPT-CODE     ,                               PPOFFFET
015000            :DELETE-FLAG          ,                               PPOFFFET
015100            :ERH-INCORRECT        ,                               PPOFFFET
015200            :LAYOFF-UNIT-CODE     ,                               PPOFFFET
015300            :LAYOFF-UNIT-CODE-C   ,                               PPOFFFET
015400            :LAYOFF-NOTICE-DATE   ,                               PPOFFFET
015500            :LAYOFF-NTCE-DATE-C   ,                               PPOFFFET
015600            :LAYOFF-SALARY        ,                               PPOFFFET
015700            :LAYOFF-SALARY-C      ,                               PPOFFFET
015800            :LAYOFF-SALARY-IND    ,                               PPOFFFET
015900            :LAYOFF-SALRY-IND-C   ,                               PPOFFFET
016000            :LAYOFF-PERCNT-TIME   ,                               PPOFFFET
016100            :LAYOFF-PCNT-TIME-C   ,                               PPOFFFET
016200            :REHIRE-BGN-DATE      ,                               PPOFFFET
016300            :REHIRE-BGN-DATE-C    ,                               PPOFFFET
016400            :REHIRE-EXP-DATE      ,                               PPOFFFET
016500            :REHIRE-EXP-DATE-C    ,                               PPOFFFET
016600            :RHRE-SUSP-BGN-TERM   ,                               PPOFFFET
016700            :RHRE-SUS-BGN-TER-C   ,                               PPOFFFET
016800            :REHIRE-SUSP-END      ,                               PPOFFFET
016900            :REHIRE-SUSP-END-C    ,                               PPOFFFET
017000            :RHRE-SUSP-TERM-RSN   ,                               PPOFFFET
017100            :RHRE-SUS-TER-RSN-C   ,                               PPOFFFET
017200            :RECALL-BGN-DATE      ,                               PPOFFFET
017300            :RECALL-BGN-DATE-C    ,                               PPOFFFET
017400            :RECALL-EXP-DATE      ,                               PPOFFFET
017500            :RECALL-EXP-DATE-C    ,                               PPOFFFET
017600            :RCLL-SUSP-BGN-TERM   ,                               PPOFFFET
017700            :RCLL-SUS-BGN-TER-C   ,                               PPOFFFET
017800            :RECALL-SUSP-END      ,                               PPOFFFET
017900            :RECALL-SUSP-END-C    ,                               PPOFFFET
018000            :RCLL-SUSP-TERM-RSN   ,                               PPOFFFET
018100            :RCLL-SUS-TER-RSN-C                                   PPOFFFET
018200         FROM PPPVOFF1_OFF                                        PPOFFFET
018300         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPOFFFET
018400         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPOFFFET
018500         AND TIMESTAMP(SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME) =     PPOFFFET
018600             :WS-MAX-TIMESTAMP                                    PPOFFFET
018700         AND   LAYOFF_TITLE_CODE  = :WS-PPPOFF-TITLE-CODE         PPOFFFET
018800         AND   LAYOFF_DEPT_CODE   = :WS-PPPOFF-DEPT-CODE          PPOFFFET
018900         AND   LAYOFF_NOTICE_DATE = :WS-NOTICE-DATE               05490748
019000     END-EXEC.                                                    PPOFFFET
019100*                                                                 PPOFFFET
019200     IF SQLCODE = +100                                            PPOFFFET
019300        INITIALIZE XSEL-RETURN-KEY                                PPOFFFET
019400                   PPPOFF-ROW                                     PPOFFFET
019500        SET XSEL-INVALID-KEY TO TRUE                              PPOFFFET
019600        GO TO 2000-EXIT                                           PPOFFFET
019700     END-IF.                                                      PPOFFFET
019800*                                                                 PPOFFFET
019900     IF SQLCODE = +0                                              PPOFFFET
020000        MOVE EMPLID TO XSEL-RETURN-EMPLID                         PPOFFFET
020100        MOVE ITERATION-NUMBER TO XSEL-RETURN-ITERATION            PPOFFFET
020210********MOVE SYSTEM-ENTRY-DATE TO WSX-INQ-DATE                    28521087
020300********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
020400********MOVE DB2-DATE TO SYSTEM-ENTRY-DATE                        28521087
020500********                 XSEL-RETURN-DATE                         28521087
020600********MOVE LAYOFF-NOTICE-DATE TO WSX-INQ-DATE                   28521087
020700********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
020800********MOVE DB2-DATE TO LAYOFF-NOTICE-DATE                       28521087
020900********MOVE REHIRE-BGN-DATE    TO WSX-INQ-DATE                   28521087
021000********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
021100********MOVE DB2-DATE TO REHIRE-BGN-DATE                          28521087
021200********MOVE REHIRE-EXP-DATE    TO WSX-INQ-DATE                   28521087
021300********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
021400********MOVE DB2-DATE TO REHIRE-EXP-DATE                          28521087
021500********MOVE RHRE-SUSP-BGN-TERM TO WSX-INQ-DATE                   28521087
021600********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
021700********MOVE DB2-DATE TO RHRE-SUSP-BGN-TERM                       28521087
021800********MOVE REHIRE-SUSP-END    TO WSX-INQ-DATE                   28521087
021900********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
022000********MOVE DB2-DATE TO REHIRE-SUSP-END                          28521087
022100********MOVE RECALL-BGN-DATE    TO WSX-INQ-DATE                   28521087
022200********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
022300********MOVE DB2-DATE TO RECALL-BGN-DATE                          28521087
022400********MOVE RECALL-EXP-DATE    TO WSX-INQ-DATE                   28521087
022500********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
022600********MOVE DB2-DATE TO RECALL-EXP-DATE                          28521087
022700********MOVE RCLL-SUSP-BGN-TERM TO WSX-INQ-DATE                   28521087
022800********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
022900********MOVE DB2-DATE TO RCLL-SUSP-BGN-TERM                       28521087
023000********MOVE RECALL-SUSP-END    TO WSX-INQ-DATE                   28521087
023100********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
023200********MOVE DB2-DATE TO RECALL-SUSP-END                          28521087
023210        MOVE SYSTEM-ENTRY-DATE TO XDC3-DB2-DATE                   28521087
023220        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023230        MOVE XDC3-DB2-DATE TO SYSTEM-ENTRY-DATE                   28521087
023240                         XSEL-RETURN-DATE                         28521087
023250        MOVE LAYOFF-NOTICE-DATE TO XDC3-DB2-DATE                  28521087
023260        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023270        MOVE XDC3-ISO-DATE TO LAYOFF-NOTICE-DATE                  28521087
023280        MOVE REHIRE-BGN-DATE    TO XDC3-DB2-DATE                  28521087
023290        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023291        MOVE XDC3-ISO-DATE TO REHIRE-BGN-DATE                     28521087
023292        MOVE REHIRE-EXP-DATE    TO XDC3-DB2-DATE                  28521087
023293        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023294        MOVE XDC3-ISO-DATE TO REHIRE-EXP-DATE                     28521087
023295        MOVE RHRE-SUSP-BGN-TERM TO XDC3-DB2-DATE                  28521087
023296        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023297        MOVE XDC3-ISO-DATE TO RHRE-SUSP-BGN-TERM                  28521087
023298        MOVE REHIRE-SUSP-END    TO XDC3-DB2-DATE                  28521087
023299        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023300        MOVE XDC3-ISO-DATE TO REHIRE-SUSP-END                     28521087
023301        MOVE RECALL-BGN-DATE    TO XDC3-DB2-DATE                  28521087
023302        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023303        MOVE XDC3-ISO-DATE TO RECALL-BGN-DATE                     28521087
023304        MOVE RECALL-EXP-DATE    TO XDC3-DB2-DATE                  28521087
023305        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023306        MOVE XDC3-ISO-DATE TO RECALL-EXP-DATE                     28521087
023307        MOVE RCLL-SUSP-BGN-TERM TO XDC3-DB2-DATE                  28521087
023308        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023309        MOVE XDC3-ISO-DATE TO RCLL-SUSP-BGN-TERM                  28521087
023310        MOVE RECALL-SUSP-END    TO XDC3-DB2-DATE                  28521087
023311        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
023312        MOVE XDC3-ISO-DATE TO RECALL-SUSP-END                     28521087
023300        MOVE SYSTEM-ENTRY-TIME TO XSEL-RETURN-TIME                PPOFFFET
023400        MOVE WS-ADDITIONAL-KEY TO XSEL-RETURN-ADD-KEY             PPOFFFET
023500        SET XSEL-OK TO TRUE                                       PPOFFFET
023600        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPOFFFET
023700        GO TO 2000-EXIT                                           PPOFFFET
023800     END-IF.                                                      PPOFFFET
023900*                                                                 PPOFFFET
024000 2000-EXIT.                                                       PPOFFFET
024100     EXIT.                                                        PPOFFFET
024200*                                                                 PPOFFFET
024300*****COPY CPPDXDC2.                                               28521087
024310 9000-DATE-CONVERSION-DB2 SECTION.                                28521087
024320     COPY CPPDXDC3.                                               28521087
024400*                                                                 PPOFFFET
024500*999999-SQL-ERROR SECTION.                                        PPOFFFET
024600     COPY CPPDXP99.                                               PPOFFFET
024700     SET XSEL-DB2-ERROR TO TRUE.                                  PPOFFFET
024800     MOVE SQLCODE TO XSEL-DB2-MSG-NUMBER.                         PPOFFFET
024900     GOBACK.                                                      PPOFFFET
025000*999999-EXIT.                                                     28521087
025100*****EXIT.                                                        28521087
025200*                                                                 PPOFFFET
