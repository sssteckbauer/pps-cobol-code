000100**************************************************************/   40070508
000200*  PROGRAM: PPOT508                                          */   40070508
000300*  RELEASE: ____0508____  SERVICE REQUEST(S): ___4007_____   */   40070508
000400*  NAME:    _L_DULANEY__  MODIFICATION DATE:  __10/11/90__   */   40070508
000500*  DESCRIPTION:                                              */   40070508
000600*   - ONE TIME PROGRAM                                       */   40070508
000700*     CONVERTS ALL CETA GROSS FIELDS ON THE DB2 EDB TO ZERO  */   40070508
000800*     AND INITIALIZES AN EMPLOYEE'S DCP-PLAN-CODE TO 'S'     */   40070508
000900*     WHENEVER THE EMPLOYEE'S RET-ELIG-CODE IS 'B' OR 'U'    */   40070508
001000**************************************************************/   40070508
001100     SKIP2                                                        PPOT508
001200 IDENTIFICATION DIVISION.                                         PPOT508
001300 PROGRAM-ID. PPOT508.                                             PPOT508
001400 AUTHOR. L.DULANEY.                                               PPOT508
001500 DATE-WRITTEN. OCT 11, 1990.                                      PPOT508
001600 DATE-COMPILED.                                                   PPOT508
001700*REMARKS.                                                         PPOT508
001800******************************************************************PPOT508
001900* THIS ONE-TIME PROGRAM INITIALIZES THE NEW DEFINED CONTRIBUTION *PPOT508
002000* PLAN DATA ELEMENTS, SET THE OBSELETE CETA GROSS FIELDS TO ZERO *PPOT508
002100* AND SETS THE (DCP-PLAN-CODE) TO 'S' IF THE RETIREMENT CODE     *PPOT508
002200* (RET-ELIG-CPDE) IS EQUAL TO 'U' OR 'B'.                        *PPOT508
002300******************************************************************PPOT508
002400     SKIP2                                                        PPOT508
002500 ENVIRONMENT DIVISION.                                            PPOT508
002600 CONFIGURATION SECTION.                                           PPOT508
002700 SPECIAL-NAMES.                                                   PPOT508
002800     SKIP1                                                        PPOT508
002900     C01 IS TOP-OF-PAGE.                                          PPOT508
003000     SKIP1                                                        PPOT508
003100 INPUT-OUTPUT SECTION.                                            PPOT508
003200 FILE-CONTROL.                                                    PPOT508
003300     SKIP2                                                        PPOT508
003400     SELECT PPOT508-REPORT ASSIGN TO UT-S-PPOT508.                PPOT508
003500     EJECT                                                        PPOT508
003600******************************************************************PPOT508
003700*                                                                *PPOT508
003800*                         DATA DIVISION                          *PPOT508
003900*                                                                *PPOT508
004000******************************************************************PPOT508
004100 DATA DIVISION.                                                   PPOT508
004200 FILE SECTION.                                                    PPOT508
004300     SKIP1                                                        PPOT508
004400 FD  PPOT508-REPORT                                               PPOT508
004500     LABEL RECORDS ARE OMITTED                                    PPOT508
004600     BLOCK CONTAINS 0 RECORDS                                     PPOT508
004700     RECORDING MODE IS F                                          PPOT508
004800     RECORD CONTAINS 133 CHARACTERS                               PPOT508
004900     DATA RECORD IS RPT1.                                         PPOT508
005000     SKIP1                                                        PPOT508
005100 01  RPT1.                                                        PPOT508
005200     05  RPT1-CC                  PIC X.                          PPOT508
005300     05  FILLER                   PIC X(132).                     PPOT508
005400     SKIP3                                                        PPOT508
005500 WORKING-STORAGE SECTION.                                         PPOT508
005600     SKIP1                                                        PPOT508
005700 01  DATE-FORMAT.                COPY CPWSDATE.                   PPOT508
005800                                                                  PPOT508
005900 01  WS-HOLD-FIELDS.                                              PPOT508
006000     05  WS-EMPLOYEE-ID           PIC  X(9)    VALUE SPACES.      PPOT508
006100     05  WS-TOTAL-B-OR-U          PIC  S9(9) COMP.                PPOT508
006200     05  WS-TOTAL-CETA-GROSS      PIC  S9(9) COMP.                PPOT508
006300     05  WS-WORK-DATE.                                            PPOT508
006400         10  WS-DATE-MM PIC X(02) VALUE SPACES.                   PPOT508
006500         10  FILLER     PIC X(01) VALUE '/'.                      PPOT508
006600         10  WS-DATE-DD PIC X(02) VALUE SPACES.                   PPOT508
006700         10  FILLER     PIC X(01) VALUE '/'.                      PPOT508
006800         10  WS-DATE-YY PIC X(02) VALUE SPACES.                   PPOT508
006900                                                                  PPOT508
007000 01  WS-SWITCHES-COUNTERS.                                        PPOT508
007100     05  BEN-ROW-OPEN-SW   PIC X(1)  VALUE SPACE.                 PPOT508
007200         88  BEN-ROW-OPEN   VALUE 'Y'.                            PPOT508
007300     05  EOF-BEN-SW        PIC X(1)  VALUE SPACE.                 PPOT508
007400         88  EOF-BEN        VALUE 'Y'.                            PPOT508
007500     05  PCM-ROW-OPEN-SW   PIC X(1)  VALUE SPACE.                 PPOT508
007600         88  PCM-ROW-OPEN   VALUE 'Y'.                            PPOT508
007700     05  EOF-PCM-SW        PIC X(1)  VALUE SPACE.                 PPOT508
007800         88  EOF-PCM        VALUE 'Y'.                            PPOT508
007900     05  END-OF-PROCESS-SW PIC X(1)  VALUE SPACE.                 PPOT508
008000         88  END-OF-PROCESS VALUE 'Y'.                            PPOT508
008100     05  ERROR-RETURNED-SW        PIC X(1)  VALUE SPACE.          PPOT508
008200         88  NO-ERROR-RETURNED              VALUE SPACE.          PPOT508
008300         88  ERROR-RETURNED                 VALUE 'Y'.            PPOT508
008400     05  HERE-BEFORE-SW PIC X(01) VALUE SPACE.                    PPOT508
008500         88  HERE-BEFORE VALUE 'Y'.                               PPOT508
008600                                                                  PPOT508
008700 01  WS-TOTAL-FIELDS.                                             PPOT508
008800     05  WS-TTL-B-OR-U            PIC S9(06) VALUE +0.            PPOT508
008900     05  WS-TTL-CETA-GROSS        PIC S9(06) VALUE +0.            PPOT508
009000                                                                  PPOT508
009100 01  STND-RPT-HD1.                                                PPOT508
009200                                            COPY 'CPWSXSHR'.      PPOT508
009300     EJECT                                                        PPOT508
009400 01  PRINT-LINE1.                                                 PPOT508
009500     05  HDR1-CC                 PIC X(01).                       PPOT508
009600     05  FILLER                  PIC X(64) VALUE                  PPOT508
009700         'TOTAL EMPLOYEES WITH RET_ELIG_CODE EQUAL B OR U = '.    PPOT508
009800     05  WS-CODE-B-OR-U          PIC X(06) VALUE ZEROES.          PPOT508
009900     05  FILLER                  PIC X(62) VALUE SPACES.          PPOT508
010000                                                                  PPOT508
010100 01  PRINT-LINE2.                                                 PPOT508
010200     05  HDR1-CC                 PIC X(01).                       PPOT508
010300     05  FILLER                  PIC X(64) VALUE                  PPOT508
010400         'TOTAL EMPLOYEES WITH INITIALIZED CETA GROSS FIELDS = '. PPOT508
010500     05  WS-CETA-GROSS           PIC X(06) VALUE ZEROES.          PPOT508
010600     05  FILLER                  PIC X(62) VALUE SPACES.          PPOT508
010700                                                                  PPOT508
010800 01  PRINT-EOJ-STATUS-LINE1.                                      PPOT508
010900     05  STS-CC1                  PIC X(01).                      PPOT508
011000     05  FILLER                   PIC X(40)  VALUE SPACES.        PPOT508
011100     05  FILLER                   PIC X(40)  VALUE                PPOT508
011200            '* CONTROL TOTALS AND DB2 UPDATE STATUS *'.           PPOT508
011300     05  FILLER                   PIC X(52)  VALUE SPACES.        PPOT508
011400                                                                  PPOT508
011500 01  PRINT-EOJ-STATUS-LINE2.                                      PPOT508
011600     05  STS-CC2                  PIC X(01).                      PPOT508
011700     05  RPT-EOJ-STATUS-FLD1      PIC X(43)   VALUE SPACES.       PPOT508
011800     05  FILLER                   PIC X(01)   VALUE SPACES.       PPOT508
011900     05  RPT-EOJ-STATUS-FLD2      PIC X(43)   VALUE SPACES.       PPOT508
012000     05  FILLER                   PIC X(45)   VALUE SPACES.       PPOT508
012100                                                                  PPOT508
012200     EJECT                                                        PPOT508
012300 01  PPDB2MSG-INTERFACE. COPY CPLNKDB2.                           PPOT508
012400******************************************************************PPOT508
012500     SKIP2                                                        PPOT508
012600 01  BEN-ROW-DATA.                                                PPOT508
012700     EXEC SQL                                                     PPOT508
012800         INCLUDE PPOT508A                                         PPOT508
012900     END-EXEC.                                                    PPOT508
013000     SKIP1                                                        PPOT508
013100     EXEC SQL                                                     PPOT508
013200       DECLARE BEN_CSR CURSOR FOR                                 PPOT508
013300         SELECT * FROM PPOT508A_BEN                               PPOT508
013400          WHERE RET_ELIG_CODE IN ('B', 'U')                       PPOT508
013500     END-EXEC.                                                    PPOT508
013600     SKIP2                                                        PPOT508
013700 01  PCM-ROW-DATA.                                                PPOT508
013800     EXEC SQL                                                     PPOT508
013900         INCLUDE PPOT508B                                         PPOT508
014000     END-EXEC.                                                    PPOT508
014100     SKIP1                                                        PPOT508
014200     EXEC SQL                                                     PPOT508
014300       DECLARE PCM_CSR CURSOR FOR                                 PPOT508
014400       SELECT * FROM PPOT508B_PCM                                 PPOT508
014500     END-EXEC.                                                    PPOT508
014600     SKIP2                                                        PPOT508
014700     EXEC SQL                                                     PPOT508
014800         INCLUDE SQLCA                                            PPOT508
014900     END-EXEC.                                                    PPOT508
015000     EJECT                                                        PPOT508
015100 PROCEDURE DIVISION.                                              PPOT508
015200**************************************************************/  *PPOT508
015300*  SQL FROM PRE-COMPILER                                     */  *PPOT508
015400**************************************************************/  *PPOT508
015500******************************************************************PPOT508
015600*****THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPOT508
015700*****CAUSE A BRANCH TO 999999-SQL-ERROR                          *PPOT508
015800******************************************************************PPOT508
015900     EXEC SQL                                                     PPOT508
016000          INCLUDE CPPDXE99                                        PPOT508
016100     END-EXEC.                                                    PPOT508
016200     SKIP1                                                        PPOT508
016300 000-MAIN SECTION.                                                PPOT508
016400     PERFORM 100-INITIALIZE    THRU  100-EXIT.                    PPOT508
016500                                                                  PPOT508
016600     PERFORM 200-PROCESS       THRU  200-EXIT                     PPOT508
016700        UNTIL ERROR-RETURNED OR END-OF-PROCESS.                   PPOT508
016800                                                                  PPOT508
016900     PERFORM 300-WRITE-REPORT  THRU  300-EXIT.                    PPOT508
017000                                                                  PPOT508
017100     PERFORM 900-EOJ-PROCESS   THRU  900-EXIT.                    PPOT508
017200                                                                  PPOT508
017300     STOP RUN.                                                    PPOT508
017400 000-EXIT.                                                        PPOT508
017500     EXIT.                                                        PPOT508
017600     EJECT                                                        PPOT508
017700 100-INITIALIZE SECTION.                                          PPOT508
017800     MOVE 'PPOT508' TO DB2MSG-PGM-ID.                             PPOT508
017900                                                                  PPOT508
018000     OPEN OUTPUT PPOT508-REPORT.                                  PPOT508
018100                                                                  PPOT508
018200        COPY CPPDDATE.                                            PPOT508
018300                                                                  PPOT508
018400     MOVE GREGORIAN-DATE  TO STND-RPT-DATE.                       PPOT508
018500     MOVE GREG-MM         TO WS-DATE-MM.                          PPOT508
018600     MOVE GREG-DD         TO WS-DATE-DD.                          PPOT508
018700     MOVE GREG-YY         TO WS-DATE-YY.                          PPOT508
018800 100-EXIT.                                                        PPOT508
018900     EXIT.                                                        PPOT508
019000     EJECT                                                        PPOT508
019100 200-PROCESS SECTION.                                             PPOT508
019200     PERFORM 210-SQL-SELECT-BEN THRU 210-EXIT.                    PPOT508
019300     IF SQLCODE NOT = ZERO                                        PPOT508
019400        SET ERROR-RETURNED TO TRUE                                PPOT508
019500        DISPLAY 'SQL SELECTING BEN TABLE, SQLCODE = ' SQLCODE     PPOT508
019600        GO TO 200-EXIT                                            PPOT508
019700     END-IF.                                                      PPOT508
019800                                                                  PPOT508
019900     PERFORM 220-SQL-SELECT-PCM THRU 220-EXIT.                    PPOT508
020000     IF SQLCODE NOT = ZERO                                        PPOT508
020100        SET ERROR-RETURNED TO TRUE                                PPOT508
020200        DISPLAY 'SQL SELECTING PCM TABLE, SQLCODE = ' SQLCODE     PPOT508
020300        GO TO 200-EXIT                                            PPOT508
020400     END-IF.                                                      PPOT508
020500                                                                  PPOT508
020600     PERFORM 230-UPDATE-PLAN-CODE  THRU  230-EXIT.                PPOT508
020700     IF SQLCODE NOT = ZERO                                        PPOT508
020800      SET ERROR-RETURNED TO TRUE                                  PPOT508
020900      DISPLAY 'SQL ERR UPDATING DCP_PLN_CDE TO S, SQLCODE = '     PPOT508
021000                                            SQLCODE               PPOT508
021100      GO TO 200-EXIT                                              PPOT508
021200     END-IF.                                                      PPOT508
021300                                                                  PPOT508
021400     PERFORM 240-INITIALIZE-YTD-CETA  THRU  240-EXIT.             PPOT508
021500     IF SQLCODE NOT = ZERO                                        PPOT508
021600      SET ERROR-RETURNED TO TRUE                                  PPOT508
021700      DISPLAY 'SQL ERROR UPDATING YTD CETA GROSS, SQLCODE = '     PPOT508
021800                                            SQLCODE               PPOT508
021900      GO TO 200-EXIT                                              PPOT508
022000     END-IF.                                                      PPOT508
022100                                                                  PPOT508
022200     PERFORM 250-INITIALIZE-QTD-CETA  THRU  250-EXIT.             PPOT508
022300     IF SQLCODE NOT = ZERO                                        PPOT508
022400      SET ERROR-RETURNED TO TRUE                                  PPOT508
022500      DISPLAY 'SQL ERROR UPDATING QTD CETA GROSS, SQLCODE = '     PPOT508
022600                                            SQLCODE               PPOT508
022700      GO TO 200-EXIT                                              PPOT508
022800     END-IF.                                                      PPOT508
022900      SET END-OF-PROCESS TO TRUE.                                 PPOT508
023000 200-EXIT.                                                        PPOT508
023100     EXIT.                                                        PPOT508
023200     EJECT                                                        PPOT508
023300 210-SQL-SELECT-BEN SECTION.                                      PPOT508
023400     MOVE 'SELECT BEN ROW' TO DB2MSG-TAG.                         PPOT508
023500     EXEC SQL                                                     PPOT508
023600          SELECT INTEGER(COUNT(*))                                PPOT508
023700            INTO :WS-TOTAL-B-OR-U                                 PPOT508
023800              FROM PPOT508A_BEN                                   PPOT508
023900                WHERE RET_ELIG_CODE IN ('B', 'U')                 PPOT508
024000     END-EXEC.                                                    PPOT508
024100     MOVE WS-TOTAL-B-OR-U  TO WS-TTL-B-OR-U.                      PPOT508
024200 210-EXIT.                                                        PPOT508
024300     EXIT.                                                        PPOT508
024400     EJECT                                                        PPOT508
024500 220-SQL-SELECT-PCM SECTION.                                      PPOT508
024600     MOVE 'SELECT PCM ROW' TO DB2MSG-TAG.                         PPOT508
024700     EXEC SQL                                                     PPOT508
024800          SELECT INTEGER(COUNT(*))                                PPOT508
024900            INTO :WS-TOTAL-CETA-GROSS                             PPOT508
025000              FROM PPOT508B_PCM                                   PPOT508
025100     END-EXEC.                                                    PPOT508
025200     MOVE WS-TOTAL-CETA-GROSS  TO  WS-TTL-CETA-GROSS.             PPOT508
025300 220-EXIT.                                                        PPOT508
025400     EXIT.                                                        PPOT508
025500     EJECT                                                        PPOT508
025600 230-UPDATE-PLAN-CODE.                                            PPOT508
025700     MOVE 'UPDATE BEN' TO DB2MSG-TAG.                             PPOT508
025800     EXEC SQL                                                     PPOT508
025900          UPDATE PPOT508A_BEN                                     PPOT508
026000           SET DCP_PLAN_CODE = 'S'                                PPOT508
026100            WHERE RET_ELIG_CODE IN ('B', 'U')                     PPOT508
026200     END-EXEC.                                                    PPOT508
026300 230-EXIT.                                                        PPOT508
026400     EXIT.                                                        PPOT508
026500     EJECT                                                        PPOT508
026600 240-INITIALIZE-YTD-CETA.                                         PPOT508
026700     MOVE 'UPDATE PCM' TO DB2MSG-TAG.                             PPOT508
026800     EXEC SQL                                                     PPOT508
026900          UPDATE PPOT508B_PCM                                     PPOT508
027000           SET YTD_CETA_GROSS = 0000000.00                        PPOT508
027100     END-EXEC.                                                    PPOT508
027200 240-EXIT.                                                        PPOT508
027300     EXIT.                                                        PPOT508
027400     EJECT                                                        PPOT508
027500 250-INITIALIZE-QTD-CETA.                                         PPOT508
027600     MOVE 'UPDATE PCM' TO DB2MSG-TAG.                             PPOT508
027700     EXEC SQL                                                     PPOT508
027800          UPDATE PPOT508B_PCM                                     PPOT508
027900           SET QTD_CETA_GROSS = 0000000.00                        PPOT508
028000     END-EXEC.                                                    PPOT508
028100 250-EXIT.                                                        PPOT508
028200     EXIT.                                                        PPOT508
028300     EJECT                                                        PPOT508
028400 300-WRITE-REPORT.                                                PPOT508
028500     ADD   1                                     TO STND-PAGE-NO. PPOT508
028600     MOVE 'PPOT508'                              TO STND-RPT-ID.  PPOT508
028700     MOVE 'PPOT508/101190'                       TO STND-PROG-ID. PPOT508
028800     MOVE 'ONE-TIME DEFINED CONTIRBUTION REPORT' TO STND-RPT-TTL. PPOT508
028900     MOVE WS-WORK-DATE                           TO STND-DATE2.   PPOT508
029000     MOVE WS-TTL-B-OR-U                         TO WS-CODE-B-OR-U.PPOT508
029100     MOVE WS-TTL-CETA-GROSS                      TO WS-CETA-GROSS.PPOT508
029200                                                                  PPOT508
029300     WRITE RPT1 FROM STND-RPT-HD1 AFTER ADVANCING TOP-OF-PAGE.    PPOT508
029400     WRITE RPT1 FROM STND-RPT-HD2 AFTER ADVANCING 1.              PPOT508
029500     WRITE RPT1 FROM STND-RPT-HD3 AFTER ADVANCING 1.              PPOT508
029600     MOVE  SPACES                  TO   RPT1.                     PPOT508
029700     WRITE RPT1                   AFTER ADVANCING 2.              PPOT508
029800     WRITE RPT1 FROM PRINT-LINE1  AFTER ADVANCING 3.              PPOT508
029900     WRITE RPT1 FROM PRINT-LINE2  AFTER ADVANCING 2.              PPOT508
030000 300-EXIT.                                                        PPOT508
030100     EXIT.                                                        PPOT508
030200     EJECT                                                        PPOT508
030300 900-EOJ-PROCESS SECTION.                                         PPOT508
030400     IF  NO-ERROR-RETURNED                                        PPOT508
030500         PERFORM 930-SQL-COMMIT  THRU  930-EXIT                   PPOT508
030600         IF  SQLCODE NOT = ZERO                                   PPOT508
030700             SET ERROR-RETURNED TO TRUE                           PPOT508
030800         END-IF                                                   PPOT508
030900     END-IF.                                                      PPOT508
031000                                                                  PPOT508
031100     IF ERROR-RETURNED                                            PPOT508
031200         MOVE 5 TO RETURN-CODE                                    PPOT508
031300         MOVE 'PPOT508 PROGRAM HAS ENDED ABNORMALLY'              PPOT508
031400             TO RPT-EOJ-STATUS-FLD1                               PPOT508
031500         MOVE 'ROLLBACK HAS TAKEN PLACE - NO EDB UPDATES'         PPOT508
031600             TO RPT-EOJ-STATUS-FLD2                               PPOT508
031700     ELSE                                                         PPOT508
031800         MOVE ZERO TO RETURN-CODE                                 PPOT508
031900         MOVE 'PPOT508 PROGRAM HAS ENDED NORMALLY'                PPOT508
032000             TO RPT-EOJ-STATUS-FLD1                               PPOT508
032100         MOVE 'COMMIT HAS TAKEN PLACE - EDB UPDATED'              PPOT508
032200             TO RPT-EOJ-STATUS-FLD2                               PPOT508
032300     END-IF.                                                      PPOT508
032400                                                                  PPOT508
032500     WRITE RPT1 FROM PRINT-EOJ-STATUS-LINE1 AFTER ADVANCING 4.    PPOT508
032600     WRITE RPT1 FROM PRINT-EOJ-STATUS-LINE2 AFTER ADVANCING 2.    PPOT508
032700                                                                  PPOT508
032800     CLOSE PPOT508-REPORT.                                        PPOT508
032900 900-EXIT.                                                        PPOT508
033000     EXIT.                                                        PPOT508
033100     EJECT                                                        PPOT508
033200 930-SQL-COMMIT SECTION.                                          PPOT508
033300     SKIP1                                                        PPOT508
033400     MOVE 'COMMIT' TO DB2MSG-TAG.                                 PPOT508
033500     EXEC SQL                                                     PPOT508
033600         COMMIT                                                   PPOT508
033700     END-EXEC.                                                    PPOT508
033800 930-EXIT.                                                        PPOT508
033900     EXIT.                                                        PPOT508
034000     EJECT                                                        PPOT508
034100*999900-SQL-ERROR.                                               *PPOT508
034200     SKIP1                                                        PPOT508
034300     EXEC SQL                                                     PPOT508
034400          INCLUDE CPPDXP99                                        PPOT508
034500     END-EXEC.                                                    PPOT508
034600     SET ERROR-RETURNED TO TRUE.                                  PPOT508
034700     IF NOT HERE-BEFORE                                           PPOT508
034800         SET HERE-BEFORE TO TRUE                                  PPOT508
034900         MOVE 'ROLLBACK' TO DB2MSG-TAG                            PPOT508
035000         EXEC SQL                                                 PPOT508
035100             ROLLBACK                                             PPOT508
035200         END-EXEC                                                 PPOT508
035300     END-IF.                                                      PPOT508
035400     SKIP1                                                        PPOT508
