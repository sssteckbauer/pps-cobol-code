000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEG286                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    BOND GROSS TO NET CONSISTENCY EDIT                      *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEG286
001200 IDENTIFICATION DIVISION.                                         PPEG286
001300 PROGRAM-ID. PPEG286.                                             PPEG286
001400 AUTHOR. UCOP.                                                    PPEG286
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEG286
001600                                                                  PPEG286
001700 DATE-WRITTEN.  AUGUST 06,1992.                                   PPEG286
001800 DATE-COMPILED.                                                   PPEG286
001900                                                                  PPEG286
002000******************************************************************PPEG286
002100**    THIS ROUTINE WILL PERFORM THE BOND GROSS TO NET           **PPEG286
002200**    CONSISTENCY EDITS                                         **PPEG286
002300******************************************************************PPEG286
002400                                                                  PPEG286
002500 ENVIRONMENT DIVISION.                                            PPEG286
002600 CONFIGURATION SECTION.                                           PPEG286
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEG286
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEG286
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEG286
003000                                                                  PPEG286
003100 INPUT-OUTPUT SECTION.                                            PPEG286
003200 FILE-CONTROL.                                                    PPEG286
003300                                                                  PPEG286
003400 DATA DIVISION.                                                   PPEG286
003500 FILE SECTION.                                                    PPEG286
003600     EJECT                                                        PPEG286
003700*----------------------------------------------------------------*PPEG286
003800 WORKING-STORAGE SECTION.                                         PPEG286
003900*----------------------------------------------------------------*PPEG286
004000                                                                  PPEG286
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEG286
004200     'PPEG286 /111992'.                                           PPEG286
004300                                                                  PPEG286
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEG286
004500     A-STND-PROG-ID.                                              PPEG286
004600     05  FILLER                       PIC X(03).                  PPEG286
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEG286
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEG286
004900     05  FILLER                       PIC X(08).                  PPEG286
005000                                                                  PPEG286
005100 01  MESSAGES-USED.                                               PPEG286
005200     05  M08170                       PIC X(05) VALUE '08170'.    PPEG286
005300                                                                  PPEG286
005400 01  MISCELLANEOUS-WORK-AREAS.                                    PPEG286
005500     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEG286
005600         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEG286
005700         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEG286
005800     05  DEPCARE-GTN-NO               PIC S9(4) VALUE +225.       PPEG286
005900     05  DSA-GTN-P                    PIC S9(4) VALUE +1.         PPEG286
006000     05  BOND-GTN-ZERO-ARRAY.                                     PPEG286
006100         10  FILLER                   PIC 9(12) VALUE ZEROS.      PPEG286
006200     05  BOND-GTN-ARRAY               REDEFINES                   PPEG286
006300         BOND-GTN-ZERO-ARRAY.                                     PPEG286
006400         10  FILLER           OCCURS 4 TIMES.                     PPEG286
006500             15  BOND-GTN-VALUE       PIC 9(03).                  PPEG286
006600     05  SYS-INDX                     PIC 9(02) VALUE ZEROS.      PPEG286
006700     05  BOND-INDX                    PIC 9(02) VALUE ZEROS.      PPEG286
006800     05  BOND-GTN-INDX                PIC 9(02) VALUE ZEROS.      PPEG286
006900     05  BOND-GTN-NO                  PIC 9(03) VALUE ZEROS.      PPEG286
007000                                                                  PPEG286
007100     EJECT                                                        PPEG286
007200 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEG286
007300                                                                  PPEG286
007400     EJECT                                                        PPEG286
007500 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPEG286
007600                                                                  PPEG286
007700     EJECT                                                        PPEG286
007800 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEG286
007900                                                                  PPEG286
008000     EJECT                                                        PPEG286
008100 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEG286
008200                                                                  PPEG286
008300     EJECT                                                        PPEG286
008400 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPEG286
008500                                                                  PPEG286
008600     EJECT                                                        PPEG286
008700 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEG286
008800                                                                  PPEG286
008900     EJECT                                                        PPEG286
009000 01  INSTALLATION-DEPENDT-CONSTATNS.               COPY CPWSXIDC. PPEG286
009100                                                                  PPEG286
009200     EJECT                                                        PPEG286
009300******************************************************************PPEG286
009400**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEG286
009500**--------------------------------------------------------------**PPEG286
009600** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEG286
009700******************************************************************PPEG286
009800                                                                  PPEG286
009900 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEG286
010000                                                                  PPEG286
010100     EJECT                                                        PPEG286
010200 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEG286
010300                                                                  PPEG286
010400     EJECT                                                        PPEG286
010500 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEG286
010600                                                                  PPEG286
010700     EJECT                                                        PPEG286
010800 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEG286
010900                                                                  PPEG286
011000 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEG286
011100                                                                  PPEG286
011200 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEG286
011300                                                                  PPEG286
011400 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEG286
011500                                                                  PPEG286
011600     EJECT                                                        PPEG286
011700 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEG286
011800                                                                  PPEG286
011900     EJECT                                                        PPEG286
012000 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEG286
012100                                                                  PPEG286
012200     EJECT                                                        PPEG286
012300 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEG286
012400                                                                  PPEG286
012500     EJECT                                                        PPEG286
012600 01  DEDUCTION-SEGMENT-ARRAY             EXTERNAL. COPY CPWSEDSA. PPEG286
012700                                                                  PPEG286
012800     EJECT                                                        PPEG286
012900 01  BND-ARRAY                           EXTERNAL.                PPEG286
013000     02  BND-ARRAY-ENTRY OCCURS 4 TIMES.           COPY CPWSRBND. PPEG286
013100                                                                  PPEG286
013200     EJECT                                                        PPEG286
013300******************************************************************PPEG286
013400**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEG286
013500******************************************************************PPEG286
013600                                                                  PPEG286
013700*----------------------------------------------------------------*PPEG286
013800 LINKAGE SECTION.                                                 PPEG286
013900*----------------------------------------------------------------*PPEG286
014000                                                                  PPEG286
014100******************************************************************PPEG286
014200**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEG286
014300**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEG286
014400******************************************************************PPEG286
014500                                                                  PPEG286
014600 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEG286
014700                                                                  PPEG286
014800     EJECT                                                        PPEG286
014900******************************************************************PPEG286
015000**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEG286
015100**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEG286
015200******************************************************************PPEG286
015300                                                                  PPEG286
015400 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEG286
015500                                                                  PPEG286
015600     EJECT                                                        PPEG286
015700******************************************************************PPEG286
015800**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEG286
015900******************************************************************PPEG286
016000                                                                  PPEG286
016100 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEG286
016200                          KMTA-MESSAGE-TABLE-ARRAY.               PPEG286
016300                                                                  PPEG286
016400                                                                  PPEG286
016500*----------------------------------------------------------------*PPEG286
016600 0000-DRIVER.                                                     PPEG286
016700*----------------------------------------------------------------*PPEG286
016800                                                                  PPEG286
016900******************************************************************PPEG286
017000**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEG286
017100******************************************************************PPEG286
017200                                                                  PPEG286
017300     IF  THIS-IS-THE-FIRST-TIME                                   PPEG286
017400         PERFORM 0100-INITIALIZE                                  PPEG286
017500     END-IF.                                                      PPEG286
017600                                                                  PPEG286
017700******************************************************************PPEG286
017800**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEG286
017900**  TO THE CALLING PROGRAM                                      **PPEG286
018000******************************************************************PPEG286
018100                                                                  PPEG286
018200     PERFORM 1000-MAINLINE-ROUTINE.                               PPEG286
018300                                                                  PPEG286
018400     EXIT PROGRAM.                                                PPEG286
018500                                                                  PPEG286
018600                                                                  PPEG286
018700     EJECT                                                        PPEG286
018800*----------------------------------------------------------------*PPEG286
018900 0100-INITIALIZE    SECTION.                                      PPEG286
019000*----------------------------------------------------------------*PPEG286
019100                                                                  PPEG286
019200******************************************************************PPEG286
019300**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEG286
019400******************************************************************PPEG286
019500                                                                  PPEG286
019600     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEG286
019700                                                                  PPEG286
019800*                                               *-------------*   PPEG286
019900                                                 COPY CPPDXWHC.   PPEG286
020000*                                               *-------------*   PPEG286
020100                                                                  PPEG286
020200*                                               *-------------*   PPEG286
020300                                                 COPY CPPDDATE.   PPEG286
020400*                                               *-------------*   PPEG286
020500                                                                  PPEG286
020600     MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 PPEG286
020700                                                                  PPEG286
020800******************************************************************PPEG286
020900**   SET FIRST CALL SWITCH OFF                                  **PPEG286
021000******************************************************************PPEG286
021100                                                                  PPEG286
021200     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEG286
021300                                                                  PPEG286
021400                                                                  PPEG286
021500     EJECT                                                        PPEG286
021600*----------------------------------------------------------------*PPEG286
021700 1000-MAINLINE-ROUTINE SECTION.                                   PPEG286
021800*----------------------------------------------------------------*PPEG286
021900                                                                  PPEG286
022000     MOVE  ZEROS          TO BOND-GTN-ZERO-ARRAY.                 PPEG286
022100                                                                  PPEG286
022200     PERFORM 1828-BOND-DED-GTN.                                   PPEG286
022300                                                                  PPEG286
022400     PERFORM WITH TEST BEFORE                                     PPEG286
022500         VARYING BOND-GTN-INDX FROM 1 BY 1                        PPEG286
022600         UNTIL BOND-GTN-INDX > IDC-MAX-BOND-SEGS                  PPEG286
022700         MOVE IDC-BOND-GTN (BOND-GTN-INDX)                        PPEG286
022800                          TO BOND-GTN-NO                          PPEG286
022900         IF  BOND-GTN-INDX = 1                                    PPEG286
023000             AND ECES-CON-EDIT-SW (116) = SW-ON                   PPEG286
023100             MOVE BOND-GTN-VALUE (BOND-GTN-INDX)                  PPEG286
023200                          TO EDSA-BALAMT (BOND-GTN-NO, DSA-GTN-P) PPEG286
023300         ELSE                                                     PPEG286
023400             IF  BOND-GTN-INDX = 2                                PPEG286
023500                 AND ECES-CON-EDIT-SW (117) = SW-ON               PPEG286
023600                 MOVE BOND-GTN-VALUE (BOND-GTN-INDX)              PPEG286
023700                          TO EDSA-BALAMT (BOND-GTN-NO, DSA-GTN-P) PPEG286
023800             ELSE                                                 PPEG286
023900                 IF  BOND-GTN-INDX = 3                            PPEG286
024000                     AND ECES-CON-EDIT-SW (118) = SW-ON           PPEG286
024100                     MOVE BOND-GTN-VALUE (BOND-GTN-INDX)          PPEG286
024200                          TO EDSA-BALAMT (BOND-GTN-NO, DSA-GTN-P) PPEG286
024300                 ELSE                                             PPEG286
024400                     IF  BOND-GTN-INDX = 4                        PPEG286
024500                         AND ECES-CON-EDIT-SW (119) = SW-ON       PPEG286
024600                         MOVE BOND-GTN-VALUE (BOND-GTN-INDX)      PPEG286
024700                          TO EDSA-BALAMT (BOND-GTN-NO, DSA-GTN-P) PPEG286
024800                     END-IF                                       PPEG286
024900                 END-IF                                           PPEG286
025000             END-IF                                               PPEG286
025100         END-IF                                                   PPEG286
025200         IF  EDSA-BALAMT (BOND-GTN-NO, DSA-GTN-P)                 PPEG286
025300             NOT = BOND-GTN-VALUE (BOND-GTN-INDX)                 PPEG286
025400             ADD 1        TO KMTA-CTR                             PPEG286
025500             MOVE M08170  TO KMTA-MSG-NUMBER            (KMTA-CTR)PPEG286
025600             MOVE WS-ROUTINE-TYPE                                 PPEG286
025700                          TO KMTA-ROUTINE-TYPE          (KMTA-CTR)PPEG286
025800             MOVE WS-ROUTINE-NUM                                  PPEG286
025900                          TO KMTA-ROUTINE-NUM           (KMTA-CTR)PPEG286
026000         END-IF                                                   PPEG286
026100     END-PERFORM.                                                 PPEG286
026200                                                                  PPEG286
026300     EJECT                                                        PPEG286
026400*----------------------------------------------------------------*PPEG286
026500 1828-BOND-DED-GTN SECTION.                                       PPEG286
026600*----------------------------------------------------------------*PPEG286
026700                                                                  PPEG286
026800     PERFORM WITH TEST BEFORE                                     PPEG286
026900         VARYING BOND-INDX FROM 1 BY 1                            PPEG286
027000         UNTIL BOND-INDX > IDC-MAX-BOND-SEGS                      PPEG286
027100         MOVE IDC-BOND-GTN (BOND-INDX)                            PPEG286
027200                          TO BOND-GTN-NO                          PPEG286
027300         MOVE BOND-DENOMCODE (BOND-INDX)                          PPEG286
027400                          TO W88-BOND-DENOMCODE                   PPEG286
027500         IF  BOND-DENOMCODE-DENOM-CD-VALID                        PPEG286
027600             PERFORM 1829-BOND-DED-RTN-SET-BAL                    PPEG286
027700         ELSE                                                     PPEG286
027800             MOVE ZEROS   TO BOND-GTN-VALUE (BOND-INDX)           PPEG286
027900         END-IF                                                   PPEG286
028000     END-PERFORM.                                                 PPEG286
028100                                                                  PPEG286
028200     EJECT                                                        PPEG286
028300*----------------------------------------------------------------*PPEG286
028400 1829-BOND-DED-RTN-SET-BAL SECTION.                               PPEG286
028500*----------------------------------------------------------------*PPEG286
028600                                                                  PPEG286
028700     PERFORM WITH TEST BEFORE                                     PPEG286
028800         VARYING SYS-INDX FROM 1 BY 1                             PPEG286
028900         UNTIL SYS-INDX > IDC-MAX-BOND-SEGS                       PPEG286
029000         IF  BOND-DENOMCODE (BOND-INDX) =                         PPEG286
029100                                       IDC-BOND-DENOM (SYS-INDX)  PPEG286
029200             MOVE IDC-BOND-SYS-PARM9 (SYS-INDX)                   PPEG286
029300                          TO BOND-GTN-VALUE (BOND-INDX)           PPEG286
029400             MOVE IDC-MAX-BOND-SEGS                               PPEG286
029500                          TO SYS-INDX                             PPEG286
029600         END-IF                                                   PPEG286
029700     END-PERFORM.                                                 PPEG286
029800                                                                  PPEG286
029900     EJECT                                                        PPEG286
030000*----------------------------------------------------------------*PPEG286
030100 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEG286
030200*----------------------------------------------------------------*PPEG286
030300                                                                  PPEG286
030400*                                                 *-------------* PPEG286
030500                                                   COPY CPPDXDEC. PPEG286
030600*                                                 *-------------* PPEG286
030700                                                                  PPEG286
030800*----------------------------------------------------------------*PPEG286
030900 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEG286
031000*----------------------------------------------------------------*PPEG286
031100                                                                  PPEG286
031200*                                                 *-------------* PPEG286
031300                                                   COPY CPPDADLC. PPEG286
031400*                                                 *-------------* PPEG286
031500                                                                  PPEG286
031600     EJECT                                                        PPEG286
031700*----------------------------------------------------------------*PPEG286
031800 9300-DATE-CONVERSION-DB2  SECTION.                               PPEG286
031900*----------------------------------------------------------------*PPEG286
032000                                                                  PPEG286
032100*                                                 *-------------* PPEG286
032200                                                   COPY CPPDXDC2. PPEG286
032300*                                                 *-------------* PPEG286
032400                                                                  PPEG286
032500******************************************************************PPEG286
032600**   E N D  S O U R C E   ----  PPEG286 ----                    **PPEG286
032700******************************************************************PPEG286
