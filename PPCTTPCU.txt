000001**************************************************************/   30871424
000002*  PROGRAM: PPCTTPCU                                         */   30871424
000003*  RELEASE: ___1424______ SERVICE REQUEST(S): _____3087____  */   30871424
000004*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___07/30/02__  */   30871424
000005*  ERROR REPORT 1808:                                        */   30871424
000006*  SET ERROR FLAGS FOR NEGATIVE SQL ERRORS TO TRIGGER DB2    */   30871424
000007*  ERROR REPORTING IN TRANSACTION HANDLER PROGRAM.           */   30871424
000008**************************************************************/   30871424
000000**************************************************************/   EFIX1332
000001*  PROGRAM: PPCTTPCU                                         */   EFIX1332
000002*  RELEASE: ___1332______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1332
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___02/12/01__  */   EFIX1332
000004*  DESCRIPTION:                                              */   EFIX1332
000005*  ERROR REPORT 9999:                                        */   EFIX1332
000006*    CORRECT CHECK FOR DELETE ACTION.                        */   EFIX1332
000007**************************************************************/   EFIX1332
000100**************************************************************/   73151304
000200*  PROGRAM: PPCTTPCU                                         */   7315SRRR
000300*  RELEASE: ___1304______ SERVICE REQUEST(S): ____17315____  */   73151304
000400*  NAME:_______WJG_______ MODIFICATION DATE:  ___06/16/00__  */   73151304
000500*  DESCRIPTION:                                              */   73151304
000600*  TITLE CODE (TPC) TABLE UPDATE MODULE                      */   73151304
000700**************************************************************/   73151304
000800 IDENTIFICATION DIVISION.                                         PPCTTPCU
000900 PROGRAM-ID. PPCTTPCU.                                            PPCTTPCU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTTPCU
001100 DATE-COMPILED.                                                   PPCTTPCU
001200 ENVIRONMENT DIVISION.                                            PPCTTPCU
001300 DATA DIVISION.                                                   PPCTTPCU
001400 WORKING-STORAGE SECTION.                                         PPCTTPCU
001500                                                                  PPCTTPCU
001600 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTTPCU
001700 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTTPCU
001800                                                                  PPCTTPCU
001900     EXEC SQL                                                     PPCTTPCU
002000        INCLUDE SQLCA                                             PPCTTPCU
002100     END-EXEC.                                                    PPCTTPCU
002110 01  TITLE-CODE-TPC-TABLE-DATA EXTERNAL.                          PPCTTPCU
002120     EXEC SQL                                                     PPCTTPCU
002130        INCLUDE PPPVZTPC                                          PPCTTPCU
002140     END-EXEC.                                                    PPCTTPCU
002200                                                                  PPCTTPCU
002300 LINKAGE SECTION.                                                 PPCTTPCU
002400                                                                  PPCTTPCU
002500 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTTPCU
003000                                                                  PPCTTPCU
003100 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE.           PPCTTPCU
003300                                                                  PPCTTPCU
003400 0000-MAIN.                                                       PPCTTPCU
003500                                                                  PPCTTPCU
003900     MOVE 'PPCTTPCU' TO XWHC-DISPLAY-MODULE, DB2MSG-PGM-ID.       PPCTTPCU
004000     COPY CPPDXWHC.                                               PPCTTPCU
004100     MOVE SPACE TO KCTU-STATUS.                                   PPCTTPCU
004200     IF  KCTU-ACTION-ADD                                          PPCTTPCU
004300         PERFORM 2000-INSERT-TPC                                  PPCTTPCU
004400     ELSE                                                         PPCTTPCU
004500         IF  KCTU-ACTION-CHANGE                                   PPCTTPCU
004600             PERFORM 2100-UPDATE-TPC                              PPCTTPCU
004700         ELSE                                                     PPCTTPCU
004710*****        IF  KCTU-ACTION-CHANGE                               EFIX1332
004711             IF  KCTU-ACTION-DELETE                               EFIX1332
004720                 PERFORM 2200-DELETE-TPC                          PPCTTPCU
004800             ELSE                                                 PPCTTPCU
005100                 SET KCTU-INVALID-ACTION TO TRUE                  PPCTTPCU
005300             END-IF                                               PPCTTPCU
005310         END-IF                                                   PPCTTPCU
005400     END-IF.                                                      PPCTTPCU
005500                                                                  PPCTTPCU
005600 0100-END.                                                        PPCTTPCU
005700                                                                  PPCTTPCU
005800     GOBACK.                                                      PPCTTPCU
005900                                                                  PPCTTPCU
006000 2000-INSERT-TPC.                                                 PPCTTPCU
006100                                                                  PPCTTPCU
006200     MOVE '2000-INSRT-TPC' TO DB2MSG-TAG.                         PPCTTPCU
006300     EXEC SQL                                                     PPCTTPCU
006400          INSERT INTO PPPVZTPC_TPC                                PPCTTPCU
006500               VALUES (:TPC-TITLE-CODE                            PPCTTPCU
006700                      ,:TPC-ACAD-RS-EFF-DT                        PPCTTPCU
006800                      ,:TPC-ACAD-RS-END-DT                        PPCTTPCU
006900                      ,:TPC-PAY-REP-CODE                          PPCTTPCU
007000                      ,:TPC-RANGE-ADJ-ID                          PPCTTPCU
007100                      ,:TPC-PAY-INTERVAL                          PPCTTPCU
007201                      ,:TPC-MO-RATE                               PPCTTPCU
007202                      ,:TPC-HR-RATE                               PPCTTPCU
007210                      ,:TPC-UPDT-SOURCE                           PPCTTPCU
007220                      ,CURRENT TIMESTAMP                          PPCTTPCU
007300                      )                                           PPCTTPCU
007400     END-EXEC.                                                    PPCTTPCU
007500     IF  SQLCODE NOT EQUAL ZERO                                   PPCTTPCU
007600         SET KCTU-INSERT-ERROR TO TRUE                            PPCTTPCU
007610         PERFORM 999999-SQL-ERROR                                 PPCTTPCU
007700     END-IF.                                                      PPCTTPCU
007800                                                                  PPCTTPCU
007900 2100-UPDATE-TPC.                                                 PPCTTPCU
008000                                                                  PPCTTPCU
008100     MOVE '2100-UPD-TPC' TO DB2MSG-TAG.                           PPCTTPCU
008200     IF  TPC-PAY-INTERVAL EQUAL ZERO                              PPCTTPCU
008201         EXEC SQL                                                 PPCTTPCU
008202              UPDATE PPPVZTPC_TPC                                 PPCTTPCU
008203                 SET TPC_ACAD_RS_END_DT = :TPC-ACAD-RS-END-DT     PPCTTPCU
008204                    ,TPC_RANGE_ADJ_ID   = :TPC-RANGE-ADJ-ID       PPCTTPCU
008207                    ,TPC_UPDT_SOURCE    = :TPC-UPDT-SOURCE        PPCTTPCU
008208                    ,TPC_UPDT_TIMESTAMP = CURRENT TIMESTAMP       PPCTTPCU
008209               WHERE TPC_TITLE_CODE     = :TPC-TITLE-CODE         PPCTTPCU
008210                 AND TPC_ACAD_RS_EFF_DT = :TPC-ACAD-RS-EFF-DT     PPCTTPCU
008211                 AND TPC_PAY_REP_CODE   = :TPC-PAY-REP-CODE       PPCTTPCU
008213         END-EXEC                                                 PPCTTPCU
008214     ELSE                                                         PPCTTPCU
008220         EXEC SQL                                                 PPCTTPCU
008300              UPDATE PPPVZTPC_TPC                                 PPCTTPCU
008700                 SET TPC_ACAD_RS_END_DT = :TPC-ACAD-RS-END-DT     PPCTTPCU
008900                    ,TPC_RANGE_ADJ_ID   = :TPC-RANGE-ADJ-ID       PPCTTPCU
009100                    ,TPC_MO_RATE        = :TPC-MO-RATE            PPCTTPCU
009110                    ,TPC_HR_RATE        = :TPC-HR-RATE            PPCTTPCU
009120                    ,TPC_UPDT_SOURCE    = :TPC-UPDT-SOURCE        PPCTTPCU
009130                    ,TPC_UPDT_TIMESTAMP = CURRENT TIMESTAMP       PPCTTPCU
009200               WHERE TPC_TITLE_CODE     = :TPC-TITLE-CODE         PPCTTPCU
009300                 AND TPC_ACAD_RS_EFF_DT = :TPC-ACAD-RS-EFF-DT     PPCTTPCU
009400                 AND TPC_PAY_REP_CODE   = :TPC-PAY-REP-CODE       PPCTTPCU
009500                 AND TPC_PAY_INTERVAL   = :TPC-PAY-INTERVAL       PPCTTPCU
009600         END-EXEC                                                 PPCTTPCU
009610     END-IF.                                                      PPCTTPCU
009700     IF  SQLCODE NOT EQUAL ZERO                                   PPCTTPCU
009800         SET KCTU-UPDATE-ERROR TO TRUE                            PPCTTPCU
009810         PERFORM 999999-SQL-ERROR                                 PPCTTPCU
009900     END-IF.                                                      PPCTTPCU
009910                                                                  PPCTTPCU
009920 2200-DELETE-TPC.                                                 PPCTTPCU
009930                                                                  PPCTTPCU
009940     MOVE '2200-DEL-TPC' TO DB2MSG-TAG.                           PPCTTPCU
009960     EXEC SQL                                                     PPCTTPCU
009970          DELETE FROM PPPVZTPC_TPC                                PPCTTPCU
010060           WHERE TPC_TITLE_CODE     = :TPC-TITLE-CODE             PPCTTPCU
010070             AND TPC_ACAD_RS_EFF_DT = :TPC-ACAD-RS-EFF-DT         PPCTTPCU
010080             AND TPC_PAY_REP_CODE   = :TPC-PAY-REP-CODE           PPCTTPCU
010090             AND TPC_PAY_INTERVAL   = :TPC-PAY-INTERVAL           PPCTTPCU
010091     END-EXEC.                                                    PPCTTPCU
010093     IF  SQLCODE NOT EQUAL ZERO                                   PPCTTPCU
010094         SET KCTU-DELETE-ERROR TO TRUE                            PPCTTPCU
010095         PERFORM 999999-SQL-ERROR                                 PPCTTPCU
010096     END-IF.                                                      PPCTTPCU
010100                                                                  PPCTTPCU
011500*999999-SQL-ERROR.                                                PPCTTPCU
011600     EXEC SQL                                                     PPCTTPCU
011700        INCLUDE CPPDXP99                                          PPCTTPCU
011800     END-EXEC.                                                    PPCTTPCU
011810     EVALUATE TRUE                                                30871424
011820       WHEN (KCTU-ACTION-ADD)                                     30871424
011830         SET KCTU-INSERT-ERROR TO TRUE                            30871424
011840       WHEN (KCTU-ACTION-CHANGE)                                  30871424
011850         SET KCTU-UPDATE-ERROR TO TRUE                            30871424
011860       WHEN (KCTU-ACTION-DELETE)                                  30871424
011870         SET KCTU-DELETE-ERROR TO TRUE                            30871424
011880     END-EVALUATE.                                                30871424
011900     GO TO 0100-END.                                              PPCTTPCU
