000100**************************************************************/  *36090574
000200*  PROGRAM: PPBRSUTW                                         */  *36090574
000300*  RELEASE: ___0651_____  SERVICE REQUEST(S): ____3622____   */  *36220651
000400*  NAME: __________KXK__  CREATION DATE:      __04/13/92__   */  *36220651
000500*  DESCRIPTION:                                              */  *36220651
000600*  - MODIFIED PROGRAM TO PROCESS NEW ELEMENT DED-EFF-DATE    */  *36220651
000700*   TO REFLECT NEW COLUMN DED_EFF_DATE ON BRS TABLE.         */  *36220651
000800**************************************************************/  *36220651
000100**************************************************************/  *36090574
000200*  PROGRAM: PPBRSUTW                                         */  *36090574
000300*  RELEASE: ____0574____  SERVICE REQUEST(S): ____3609____   */  *36090574
000400*  NAME: ______PXP______  CREATION DATE:      __06/05/91__   */  *36090574
000500*  DESCRIPTION:                                              */  *36090574
000600*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT BRS ROWS.     */  *36090574
000700**************************************************************/  *36090574
000800 IDENTIFICATION DIVISION.                                         PPBRSUTW
000900 PROGRAM-ID. PPBRSUTW                                             PPBRSUTW
001000 AUTHOR. UCOP.                                                    PPBRSUTW
001100 DATE-WRITTEN.  NOVEMBER  1990.                                   PPBRSUTW
001200 DATE-COMPILED.                                                   PPBRSUTW
001300*REMARKS.                                                         PPBRSUTW
001400*            CALL 'PPBRSUTW' USING                                PPBRSUTW
001500*                            PPXXXUTW-INTERFACE                   PPBRSUTW
001600*                            BRS-ROW.                             PPBRSUTW
001700*                                                                 PPBRSUTW
001800*                                                                 PPBRSUTW
001900     EJECT                                                        PPBRSUTW
002000 ENVIRONMENT DIVISION.                                            PPBRSUTW
002100 CONFIGURATION SECTION.                                           PPBRSUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPBRSUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPBRSUTW
002400      EJECT                                                       PPBRSUTW
002500******************************************************************PPBRSUTW
002600*                                                                 PPBRSUTW
002700*                D A T A  D I V I S I O N                         PPBRSUTW
002800*                                                                 PPBRSUTW
002900******************************************************************PPBRSUTW
003000 DATA DIVISION.                                                   PPBRSUTW
003100 WORKING-STORAGE SECTION.                                         PPBRSUTW
003200 01  MISCELLANEOUS-WS.                                            PPBRSUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPBRSUTW'.         PPBRSUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPBRSUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPBRSUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPBRSUTW
003700     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPBRSUTW
003800         88  FIRST-TIME                VALUE 'Y'.                 PPBRSUTW
003900         88  NOT-FIRST-TIME            VALUE 'N'.                 PPBRSUTW
004000     EJECT                                                        PPBRSUTW
004100 01  XWHC-COMPILE-WORK-AREA.                                      PPBRSUTW
004200                                    COPY CPWSXWHC.                PPBRSUTW
004300     EJECT                                                        PPBRSUTW
004400 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPBRSUTW
004500******************************************************************PPBRSUTW
004600*          SQL - WORKING STORAGE                                 *PPBRSUTW
004700******************************************************************PPBRSUTW
004800 01  BRS-ROW-DATA.                                                PPBRSUTW
004900     EXEC SQL                                                     PPBRSUTW
005000          INCLUDE PPPVBRS2                                        PPBRSUTW
005100     END-EXEC.                                                    PPBRSUTW
005200     EJECT                                                        PPBRSUTW
005300     EXEC SQL                                                     PPBRSUTW
005400          INCLUDE SQLCA                                           PPBRSUTW
005500     END-EXEC.                                                    PPBRSUTW
005600******************************************************************PPBRSUTW
005700*                                                                *PPBRSUTW
005800*                SQL- SELECTS                                    *PPBRSUTW
005900*                                                                *PPBRSUTW
006000******************************************************************PPBRSUTW
006100*                                                                *PPBRSUTW
006200*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPBRSUTW
006300*                                                                *PPBRSUTW
006400*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPBRSUTW
006500*                        MEMBER NAMES                            *PPBRSUTW
006600*                                                                *PPBRSUTW
006700*  PPPVBRS2_BRS          PPPVBRS2                                *PPBRSUTW
006800*                                                                *PPBRSUTW
006900******************************************************************PPBRSUTW
007000*                                                                 PPBRSUTW
007100 LINKAGE SECTION.                                                 PPBRSUTW
007200*                                                                 PPBRSUTW
007300******************************************************************PPBRSUTW
007400*                L I N K A G E   S E C T I O N                   *PPBRSUTW
007500******************************************************************PPBRSUTW
007600 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPBRSUTW
007700 01  BRS-ROW.                       COPY CPWSRBRS.                PPBRSUTW
007800*                                                                 PPBRSUTW
007900 PROCEDURE DIVISION USING                                         PPBRSUTW
008000                          PPXXXUTW-INTERFACE                      PPBRSUTW
008100                          BRS-ROW.                                PPBRSUTW
008200******************************************************************PPBRSUTW
008300*            P R O C E D U R E  D I V I S I O N                  *PPBRSUTW
008400******************************************************************PPBRSUTW
008500******************************************************************PPBRSUTW
008600*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPBRSUTW
008700*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPBRSUTW
008800******************************************************************PPBRSUTW
008900     EXEC SQL                                                     PPBRSUTW
009000          INCLUDE CPPDXE99                                        PPBRSUTW
009100     END-EXEC.                                                    PPBRSUTW
009200     SKIP3                                                        PPBRSUTW
009300 0100-MAINLINE SECTION.                                           PPBRSUTW
009400     SKIP1                                                        PPBRSUTW
009500     IF FIRST-TIME                                                PPBRSUTW
009600         SET NOT-FIRST-TIME TO TRUE                               PPBRSUTW
009700         PERFORM 7900-FIRST-TIME                                  PPBRSUTW
009800     END-IF.                                                      PPBRSUTW
009900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPBRSUTW
010000     MOVE EMPLOYEE-ID OF BRS-ROW TO WS-EMPLOYEE-ID.               PPBRSUTW
010100     SKIP1                                                        PPBRSUTW
010200     MOVE BRS-ROW TO BRS-ROW-DATA                                 PPBRSUTW
010300     SKIP1                                                        PPBRSUTW
010400     EVALUATE PPXXXUTW-FUNCTION                                   PPBRSUTW
010500         WHEN 'D'                                                 PPBRSUTW
010600             PERFORM 8100-DELETE-ROW                              PPBRSUTW
010700         WHEN 'U'                                                 PPBRSUTW
010800             PERFORM 8200-UPDATE-ROW                              PPBRSUTW
010900         WHEN 'I'                                                 PPBRSUTW
011000             PERFORM 8300-INSERT-ROW                              PPBRSUTW
011100         WHEN OTHER                                               PPBRSUTW
011200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPBRSUTW
011300             SET PPXXXUTW-ERROR            TO TRUE                PPBRSUTW
011400     END-EVALUATE.                                                PPBRSUTW
011500     SKIP1                                                        PPBRSUTW
011600     GOBACK.                                                      PPBRSUTW
011700     EJECT                                                        PPBRSUTW
011800******************************************************************PPBRSUTW
011900*  PROCEDURE SQL                     FOR BRS VIEW                *PPBRSUTW
012000******************************************************************PPBRSUTW
012100 8100-DELETE-ROW SECTION.                                         PPBRSUTW
012200     SKIP1                                                        PPBRSUTW
012300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPBRSUTW
012400     SKIP1                                                        PPBRSUTW
012500     EXEC SQL                                                     PPBRSUTW
012600         DELETE FROM PPPVBRS2_BRS                                 PPBRSUTW
012700          WHERE   EMPLOYEE_ID        = :EMPLOYEE-ID               PPBRSUTW
012800            AND   BENRATE_GTN        = :BENRATE-GTN               PPBRSUTW
012900     END-EXEC.                                                    PPBRSUTW
013000     SKIP1                                                        PPBRSUTW
013100     IF  SQLCODE    NOT = 0                                       PPBRSUTW
013200         SET PPXXXUTW-ERROR TO TRUE                               PPBRSUTW
013300     END-IF.                                                      PPBRSUTW
013400     EJECT                                                        PPBRSUTW
013500 8200-UPDATE-ROW SECTION.                                         PPBRSUTW
013600     SKIP1                                                        PPBRSUTW
013700     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPBRSUTW
013800     SKIP1                                                        PPBRSUTW
013900     EXEC SQL                                                     PPBRSUTW
014000     UPDATE PPPVBRS2_BRS                                          PPBRSUTW
014100              SET BENRATE_TUC         = :BENRATE-TUC        ,     PPBRSUTW
014200                  BENRATE_REP_CODE    = :BENRATE-REP-CODE   ,     PPBRSUTW
014300                  BENRATE_SPCL_HNDLG  = :BENRATE-SPCL-HNDLG ,     PPBRSUTW
015200*                 BENRATE_DUC         = :BENRATE-DUC              36220651
015300                  BENRATE_DUC         = :BENRATE-DUC,             36220651
015400                  DED_EFF_DATE        = :DED-EFF-DATE             36220651
014500      WHERE   EMPLOYEE_ID        = :EMPLOYEE-ID                   PPBRSUTW
014600        AND   BENRATE_GTN        = :BENRATE-GTN                   PPBRSUTW
014700     END-EXEC.                                                    PPBRSUTW
014800     SKIP1                                                        PPBRSUTW
014900     IF  SQLCODE    NOT = 0                                       PPBRSUTW
015000         SET PPXXXUTW-ERROR TO TRUE                               PPBRSUTW
015100     END-IF.                                                      PPBRSUTW
015200     EJECT                                                        PPBRSUTW
015300 8300-INSERT-ROW SECTION.                                         PPBRSUTW
015400     SKIP1                                                        PPBRSUTW
015500     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPBRSUTW
015600     SKIP1                                                        PPBRSUTW
015700     EXEC SQL                                                     PPBRSUTW
015800         INSERT INTO PPPVBRS2_BRS                                 PPBRSUTW
015900                VALUES (:BRS-ROW-DATA)                            PPBRSUTW
016000     END-EXEC.                                                    PPBRSUTW
016100     SKIP1                                                        PPBRSUTW
016200     IF  SQLCODE    NOT = 0                                       PPBRSUTW
016300         SET PPXXXUTW-ERROR TO TRUE                               PPBRSUTW
016400     END-IF.                                                      PPBRSUTW
016500     EJECT                                                        PPBRSUTW
016600 7900-FIRST-TIME SECTION.                                         PPBRSUTW
016700     SKIP1                                                        PPBRSUTW
016800     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPBRSUTW
016900     SKIP1                                                        PPBRSUTW
017000                                    COPY CPPDXWHC.                PPBRSUTW
017100     EJECT                                                        PPBRSUTW
017200*999999-SQL-ERROR.                                               *PPBRSUTW
017300     SKIP1                                                        PPBRSUTW
017400     EXEC SQL                                                     PPBRSUTW
017500          INCLUDE CPPDXP99                                        PPBRSUTW
017600     END-EXEC.                                                    PPBRSUTW
017700     SKIP1                                                        PPBRSUTW
017800     SET PPXXXUTW-ERROR TO TRUE.                                  PPBRSUTW
017900     IF NOT HERE-BEFORE                                           PPBRSUTW
018000         SET HERE-BEFORE TO TRUE                                  PPBRSUTW
018100     GOBACK.                                                      PPBRSUTW
