000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEA021                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    PERSONNEL ACTION 21, 35, 36, 51 - TRANSFERS             *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100 IDENTIFICATION DIVISION.                                         PPEA021
001200 PROGRAM-ID. PPEA021.                                             PPEA021
001300 AUTHOR. UCOP.                                                    PPEA021
001400 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEA021
001500                                                                  PPEA021
001600 DATE-WRITTEN.  JULY 27,1992.                                     PPEA021
001700 DATE-COMPILED.                                                   PPEA021
001800                                                                  PPEA021
001900******************************************************************PPEA021
002000**    THIS ROUTINE WILL PERFORM THE PERSONNEL ACTION 21, 35,    **PPEA021
002100**    36, 52 EDIT FOR TRANSFERS                                 **PPEA021
002200******************************************************************PPEA021
002300                                                                  PPEA021
002400 ENVIRONMENT DIVISION.                                            PPEA021
002500 CONFIGURATION SECTION.                                           PPEA021
002600 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEA021
002700 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEA021
002800 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEA021
002900                                                                  PPEA021
003000 INPUT-OUTPUT SECTION.                                            PPEA021
003100 FILE-CONTROL.                                                    PPEA021
003200                                                                  PPEA021
003300 DATA DIVISION.                                                   PPEA021
003400 FILE SECTION.                                                    PPEA021
003500     EJECT                                                        PPEA021
003600*----------------------------------------------------------------*PPEA021
003700 WORKING-STORAGE SECTION.                                         PPEA021
003800*----------------------------------------------------------------*PPEA021
003900                                                                  PPEA021
004000 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEA021
004100     'PPEA021 /111992'.                                           PPEA021
004200                                                                  PPEA021
004300 01  A-STND-MSG-PARMS                 REDEFINES                   PPEA021
004400     A-STND-PROG-ID.                                              PPEA021
004500     05  FILLER                       PIC X(03).                  PPEA021
004600     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEA021
004700     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEA021
004800     05  FILLER                       PIC X(08).                  PPEA021
004900                                                                  PPEA021
005000 01  MESSAGES-USED.                                               PPEA021
005100     05  M08016                       PIC X(05) VALUE '08016'.    PPEA021
005200     05  M08912                       PIC X(05) VALUE '08912'.    PPEA021
005300     05  M08913                       PIC X(05) VALUE '08913'.    PPEA021
005400     05  M08919                       PIC X(05) VALUE '08919'.    PPEA021
005500                                                                  PPEA021
005600 01  MISC-WORK-AREAS.                                             PPEA021
005700     05  ACTION-ON                    PIC X(01) VALUE 'Y'.        PPEA021
005800     05  FIND-TRANSFER                PIC X(01) VALUE SPACES.     PPEA021
005900     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEA021
006000         88  THIS-IS-THE-FIRST-TIME     VALUE LOW-VALUES.         PPEA021
006100         88  NOT-THE-FIRST-TIME         VALUE 'Y'.                PPEA021
006200                                                                  PPEA021
006300     EJECT                                                        PPEA021
006400 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEA021
006500                                                                  PPEA021
006600     EJECT                                                        PPEA021
006700 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPEA021
006800                                                                  PPEA021
006900     EJECT                                                        PPEA021
007000 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEA021
007100                                                                  PPEA021
007200     EJECT                                                        PPEA021
007300 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEA021
007400                                                                  PPEA021
007500     EJECT                                                        PPEA021
007600 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPEA021
007700                                                                  PPEA021
007800     EJECT                                                        PPEA021
007900 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEA021
008000                                                                  PPEA021
008100     EJECT                                                        PPEA021
008200 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEA021
008300                                                                  PPEA021
008400     EJECT                                                        PPEA021
008500******************************************************************PPEA021
008600**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEA021
008700**--------------------------------------------------------------**PPEA021
008800** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEA021
008900******************************************************************PPEA021
009000                                                                  PPEA021
009100 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEA021
009200                                                                  PPEA021
009300     EJECT                                                        PPEA021
009400 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEA021
009500                                                                  PPEA021
009600     EJECT                                                        PPEA021
009700 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEA021
009800                                                                  PPEA021
009900     EJECT                                                        PPEA021
010000 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEA021
010100                                                                  PPEA021
010200 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEA021
010300                                                                  PPEA021
010400 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEA021
010500                                                                  PPEA021
010600 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEA021
010700                                                                  PPEA021
010800     EJECT                                                        PPEA021
010900 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEA021
011000                                                                  PPEA021
011100     EJECT                                                        PPEA021
011200 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEA021
011300                                                                  PPEA021
011400     EJECT                                                        PPEA021
011500 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEA021
011600                                                                  PPEA021
011700     EJECT                                                        PPEA021
011800 01  COMM-TAB                            EXTERNAL. COPY CPOTXCMF. PPEA021
011900                                                                  PPEA021
012000     EJECT                                                        PPEA021
012100 01  XACN-ACTION-ARRAY                   EXTERNAL. COPY CPWSXACN. PPEA021
012200                                                                  PPEA021
012300     EJECT                                                        PPEA021
012400******************************************************************PPEA021
012500**  DATA BASE AREAS                                             **PPEA021
012600******************************************************************PPEA021
012700                                                                  PPEA021
012800 01  SCR-ROW                             EXTERNAL.                PPEA021
012900     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEA021
013000                                                                  PPEA021
013100     EJECT                                                        PPEA021
013200 01  PER-ROW                             EXTERNAL.                PPEA021
013300     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEA021
013400                                                                  PPEA021
013500     EJECT                                                        PPEA021
013600******************************************************************PPEA021
013700**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEA021
013800******************************************************************PPEA021
013900                                                                  PPEA021
014000*----------------------------------------------------------------*PPEA021
014100 LINKAGE SECTION.                                                 PPEA021
014200*----------------------------------------------------------------*PPEA021
014300                                                                  PPEA021
014400******************************************************************PPEA021
014500**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEA021
014600**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEA021
014700******************************************************************PPEA021
014800                                                                  PPEA021
014900 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEA021
015000                                                                  PPEA021
015100     EJECT                                                        PPEA021
015200******************************************************************PPEA021
015300**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEA021
015400**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEA021
015500******************************************************************PPEA021
015600                                                                  PPEA021
015700 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEA021
015800                                                                  PPEA021
015900     EJECT                                                        PPEA021
016000******************************************************************PPEA021
016100**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEA021
016200******************************************************************PPEA021
016300                                                                  PPEA021
016400 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEA021
016500                          KMTA-MESSAGE-TABLE-ARRAY.               PPEA021
016600                                                                  PPEA021
016700                                                                  PPEA021
016800*----------------------------------------------------------------*PPEA021
016900 0000-DRIVER.                                                     PPEA021
017000*----------------------------------------------------------------*PPEA021
017100                                                                  PPEA021
017200******************************************************************PPEA021
017300**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEA021
017400******************************************************************PPEA021
017500                                                                  PPEA021
017600     IF  THIS-IS-THE-FIRST-TIME                                   PPEA021
017700         PERFORM 0100-INITIALIZE                                  PPEA021
017800     END-IF.                                                      PPEA021
017900                                                                  PPEA021
018000******************************************************************PPEA021
018100**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEA021
018200**  TO THE CALLING PROGRAM                                      **PPEA021
018300******************************************************************PPEA021
018400                                                                  PPEA021
018500     PERFORM 1000-MAINLINE-ROUTINE.                               PPEA021
018600                                                                  PPEA021
018700     EXIT PROGRAM.                                                PPEA021
018800                                                                  PPEA021
018900                                                                  PPEA021
019000     EJECT                                                        PPEA021
019100*----------------------------------------------------------------*PPEA021
019200 0100-INITIALIZE    SECTION.                                      PPEA021
019300*----------------------------------------------------------------*PPEA021
019400                                                                  PPEA021
019500******************************************************************PPEA021
019600**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEA021
019700******************************************************************PPEA021
019800                                                                  PPEA021
019900     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEA021
020000                                                                  PPEA021
020100*                                               *-------------*   PPEA021
020200                                                 COPY CPPDXWHC.   PPEA021
020300*                                               *-------------*   PPEA021
020400                                                                  PPEA021
020500*                                               *-------------*   PPEA021
020600                                                 COPY CPPDDATE.   PPEA021
020700*                                               *-------------*   PPEA021
020800                                                                  PPEA021
020900     MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 PPEA021
021000                                                                  PPEA021
021100******************************************************************PPEA021
021200**   SET FIRST CALL SWITCH OFF                                  **PPEA021
021300******************************************************************PPEA021
021400                                                                  PPEA021
021500     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEA021
021600                                                                  PPEA021
021700                                                                  PPEA021
021800     EJECT                                                        PPEA021
021900*----------------------------------------------------------------*PPEA021
022000 1000-MAINLINE-ROUTINE SECTION.                                   PPEA021
022100*----------------------------------------------------------------*PPEA021
022200                                                                  PPEA021
022300     IF  KRMI-REQUESTED-BY-08                                     PPEA021
022400         PERFORM 1100-CONEDITS-MAINLINE                           PPEA021
022500     ELSE                                                         PPEA021
022600         PERFORM 5100-IMPLIED-MAINT-MAINLINE                      PPEA021
022700     END-IF.                                                      PPEA021
022800                                                                  PPEA021
022900                                                                  PPEA021
023000     EJECT                                                        PPEA021
023100******************************************************************PPEA021
023200**     C O N S I S T E N C Y    E D I T S   M A I N L I N E     **PPEA021
023300******************************************************************PPEA021
023400*----------------------------------------------------------------*PPEA021
023500 1100-CONEDITS-MAINLINE SECTION.                                  PPEA021
023600*----------------------------------------------------------------*PPEA021
023700                                                                  PPEA021
023800     PERFORM 1100-FIND-TRANSFER-ACTION.                           PPEA021
023900                                                                  PPEA021
024000     IF  FIND-TRANSFER NOT = ACTION-ON                            PPEA021
024100                                                                  PPEA021
024200******************************************************************PPEA021
024300**   INTER CAMPUS ACTION (ACTION 21)                            **PPEA021
024400******************************************************************PPEA021
024500                                                                  PPEA021
024600         IF  XACN-ACTION-FLAG (01, 21) NOT = SPACES               PPEA021
024700             ADD 1                 TO KMTA-CTR                    PPEA021
024800             MOVE M08016           TO KMTA-MSG-NUMBER   (KMTA-CTR)PPEA021
024900             MOVE WS-ROUTINE-TYPE  TO KMTA-ROUTINE-TYPE (KMTA-CTR)PPEA021
025000             MOVE WS-ROUTINE-NUM   TO KMTA-ROUTINE-NUM  (KMTA-CTR)PPEA021
025100         END-IF                                                   PPEA021
025200                                                                  PPEA021
025300******************************************************************PPEA021
025400*  SYSTEM WIDE TRANSFER (ACTION 35)                               PPEA021
025500******************************************************************PPEA021
025600                                                                  PPEA021
025700         IF  XACN-ACTION-FLAG (01, 35) NOT = SPACES               PPEA021
025800             ADD 1                 TO KMTA-CTR                    PPEA021
025900             MOVE M08912           TO KMTA-MSG-NUMBER   (KMTA-CTR)PPEA021
026000             MOVE WS-ROUTINE-TYPE  TO KMTA-ROUTINE-TYPE (KMTA-CTR)PPEA021
026100             MOVE WS-ROUTINE-NUM   TO KMTA-ROUTINE-NUM  (KMTA-CTR)PPEA021
026200         END-IF                                                   PPEA021
026300                                                                  PPEA021
026400******************************************************************PPEA021
026500*  LABOR OR STATE TRANSFER (ACTION 36)                            PPEA021
026600******************************************************************PPEA021
026700                                                                  PPEA021
026800         IF  XACN-ACTION-FLAG (01, 36) NOT = SPACES               PPEA021
026900             ADD 1                 TO KMTA-CTR                    PPEA021
027000             MOVE M08913           TO KMTA-MSG-NUMBER   (KMTA-CTR)PPEA021
027100             MOVE WS-ROUTINE-TYPE  TO KMTA-ROUTINE-TYPE (KMTA-CTR)PPEA021
027200             MOVE WS-ROUTINE-NUM   TO KMTA-ROUTINE-NUM  (KMTA-CTR)PPEA021
027300         END-IF                                                   PPEA021
027400                                                                  PPEA021
027500******************************************************************PPEA021
027600*  TRANSFER ACTION (ACTION 51)                                    PPEA021
027700******************************************************************PPEA021
027800                                                                  PPEA021
027900         IF  XACN-ACTION-FLAG (01, 51) NOT = SPACES               PPEA021
028000             ADD 1                 TO KMTA-CTR                    PPEA021
028100             MOVE M08919           TO KMTA-MSG-NUMBER   (KMTA-CTR)PPEA021
028200             MOVE WS-ROUTINE-TYPE  TO KMTA-ROUTINE-TYPE (KMTA-CTR)PPEA021
028300             MOVE WS-ROUTINE-NUM   TO KMTA-ROUTINE-NUM  (KMTA-CTR)PPEA021
028400         END-IF                                                   PPEA021
028500     END-IF.                                                      PPEA021
028600                                                                  PPEA021
028700     EJECT                                                        PPEA021
028800*----------------------------------------------------------------*PPEA021
028900 1100-FIND-TRANSFER-ACTION SECTION.                               PPEA021
029000*----------------------------------------------------------------*PPEA021
029100                                                                  PPEA021
029200     MOVE SPACES                   TO FIND-TRANSFER.              PPEA021
029300                                                                  PPEA021
029400     IF     XACN-ACTION-FLAG (01, 01) = ACTION-ON                 PPEA021
029500         OR XACN-ACTION-FLAG (01, 02) = ACTION-ON                 PPEA021
029600         OR XACN-ACTION-FLAG (01, 06) = ACTION-ON                 PPEA021
029700         MOVE ACTION-ON            TO FIND-TRANSFER               PPEA021
029800     ELSE                                                         PPEA021
029900         MOVE SPACES               TO FIND-TRANSFER               PPEA021
030000         IF     APPT-ACTION-FLAG (10) = ACTION-ON                 PPEA021
030100             OR APPT-ACTION-FLAG (11) = ACTION-ON                 PPEA021
030200             OR APPT-ACTION-FLAG (12) = ACTION-ON                 PPEA021
030300             OR APPT-ACTION-FLAG (13) = ACTION-ON                 PPEA021
030400             OR APPT-ACTION-FLAG (14) = ACTION-ON                 PPEA021
030500             MOVE ACTION-ON        TO FIND-TRANSFER               PPEA021
030600         END-IF                                                   PPEA021
030700     END-IF.                                                      PPEA021
030800                                                                  PPEA021
030900     EJECT                                                        PPEA021
031000******************************************************************PPEA021
031100**   I M P L I E D   M A I N T E N A N C E   M A I N L I N E    **PPEA021
031200******************************************************************PPEA021
031300*----------------------------------------------------------------*PPEA021
031400 5100-IMPLIED-MAINT-MAINLINE SECTION.                             PPEA021
031500*----------------------------------------------------------------*PPEA021
031600******************************************************************PPEA021
031700**        PLACE ANY ACTION 021 IMPLIED MAINTENANCE HERE         **PPEA021
031800******************************************************************PPEA021
031900                                                                  PPEA021
032000                                                                  PPEA021
032100     EJECT                                                        PPEA021
032200*----------------------------------------------------------------*PPEA021
032300 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEA021
032400*----------------------------------------------------------------*PPEA021
032500                                                                  PPEA021
032600*                                                 *-------------* PPEA021
032700                                                   COPY CPPDXDEC. PPEA021
032800*                                                 *-------------* PPEA021
032900                                                                  PPEA021
033000*----------------------------------------------------------------*PPEA021
033100 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEA021
033200*----------------------------------------------------------------*PPEA021
033300                                                                  PPEA021
033400*                                                 *-------------* PPEA021
033500                                                   COPY CPPDADLC. PPEA021
033600*                                                 *-------------* PPEA021
033700                                                                  PPEA021
033800     EJECT                                                        PPEA021
033900*----------------------------------------------------------------*PPEA021
034000 9300-DATE-CONVERSION-DB2  SECTION.                               PPEA021
034100*----------------------------------------------------------------*PPEA021
034200                                                                  PPEA021
034300*                                                 *-------------* PPEA021
034400                                                   COPY CPPDXDC2. PPEA021
034500*                                                 *-------------* PPEA021
034600                                                                  PPEA021
034700******************************************************************PPEA021
034800**   E N D  S O U R C E   ----  PPEA021 ----                    **PPEA021
034900******************************************************************PPEA021
