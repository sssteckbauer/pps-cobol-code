000100**************************************************************/   30871465
000200*  PROGRAM: PPCTDUDE                                         */   30871465
000300*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000400*  NAME:___SRS___________ CREATION DATE:      ___02/11/03__  */   30871465
000500*  DESCRIPTION:                                              */   30871465
000600*  RANGE ADJUSTMENT DUD TABLE EDIT MODULE                    */   30871465
000700**************************************************************/   30871465
000800 IDENTIFICATION DIVISION.                                         PPCTDUDE
000900 PROGRAM-ID. PPCTDUDE.                                            PPCTDUDE
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTDUDE
001100 DATE-COMPILED.                                                   PPCTDUDE
001200 ENVIRONMENT DIVISION.                                            PPCTDUDE
001300 DATA DIVISION.                                                   PPCTDUDE
001400 WORKING-STORAGE SECTION.                                         PPCTDUDE
001500 01  MESSAGES-AREA.                                               PPCTDUDE
001600     05  M01027                   PIC X(05)     VALUE '01027'.    PPCTDUDE
001700     05  M01028                   PIC X(05)     VALUE '01028'.    PPCTDUDE
001800     05  M01029                   PIC X(05)     VALUE '01029'.    PPCTDUDE
001900     05  M01793                   PIC X(05)     VALUE '01793'.    PPCTDUDE
002000     05  M01794                   PIC X(05)     VALUE '01794'.    PPCTDUDE
002100     05  MCT005                   PIC X(05)     VALUE 'CT005'.    PPCTDUDE
002200                                                                  PPCTDUDE
002300 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTDUDE
002400 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTDUDE
002500                                                                  PPCTDUDE
002600     EXEC SQL                                                     PPCTDUDE
002700        INCLUDE SQLCA                                             PPCTDUDE
002800     END-EXEC.                                                    PPCTDUDE
002900                                                                  PPCTDUDE
003000 LINKAGE SECTION.                                                 PPCTDUDE
003100                                                                  PPCTDUDE
003200 01  CONTROL-TABLE-EDIT-INTERFACE.              COPY CPLNKCTE.    PPCTDUDE
003300 01  RANGE-ADJUST-DUD-TABLE-INPUT.              COPY CPCTDUDI.    PPCTDUDE
003400 01  RANGE-ADJUST-DUD-TABLE-DATA.                                 PPCTDUDE
003500     EXEC SQL                                                     PPCTDUDE
003600        INCLUDE PPPVZDUD                                          PPCTDUDE
003700     END-EXEC.                                                    PPCTDUDE
003800                                                                  PPCTDUDE
003900 PROCEDURE DIVISION USING CONTROL-TABLE-EDIT-INTERFACE            PPCTDUDE
004000                          RANGE-ADJUST-DUD-TABLE-INPUT            PPCTDUDE
004100                          RANGE-ADJUST-DUD-TABLE-DATA.            PPCTDUDE
004200                                                                  PPCTDUDE
004300 0000-MAIN SECTION.                                               PPCTDUDE
004400                                                                  PPCTDUDE
004500     EXEC SQL                                                     PPCTDUDE
004600        INCLUDE CPPDXE99                                          PPCTDUDE
004700     END-EXEC.                                                    PPCTDUDE
004800     MOVE 'PPCTDUDE' TO XWHC-DISPLAY-MODULE.                      PPCTDUDE
004900     COPY CPPDXWHC.                                               PPCTDUDE
005000     INITIALIZE RANGE-ADJUST-DUD-TABLE-DATA.                      PPCTDUDE
005100     MOVE ZERO TO KCTE-ERR.                                       PPCTDUDE
005200     MOVE SPACES TO KCTE-ERRORS.                                  PPCTDUDE
005300     SET KCTE-STATUS-COMPLETED TO TRUE.                           PPCTDUDE
005400     PERFORM 1000-EDIT-KEY.                                       PPCTDUDE
005500     IF KCTE-STATUS-NOT-COMPLETED                                 PPCTDUDE
005600        GO TO 0100-END                                            PPCTDUDE
005700     END-IF.                                                      PPCTDUDE
005800     PERFORM 9000-SELECT-DUD.                                     PPCTDUDE
005900     IF KCTE-ACTION-ADD                                           PPCTDUDE
006000        PERFORM 2000-ADD-DUD                                      PPCTDUDE
006100     ELSE                                                         PPCTDUDE
006200       IF KCTE-ACTION-CHANGE                                      PPCTDUDE
006300          PERFORM 2100-CHANGE-DUD                                 PPCTDUDE
006400       ELSE                                                       PPCTDUDE
006500         IF KCTE-ACTION-DELETE                                    PPCTDUDE
006600            PERFORM 2200-DELETE-DUD                               PPCTDUDE
006700         END-IF                                                   PPCTDUDE
006800       END-IF                                                     PPCTDUDE
006900     END-IF.                                                      PPCTDUDE
007000                                                                  PPCTDUDE
007100 0100-END.                                                        PPCTDUDE
007200                                                                  PPCTDUDE
007300     GOBACK.                                                      PPCTDUDE
007400                                                                  PPCTDUDE
007500 1000-EDIT-KEY SECTION.                                           PPCTDUDE
007600                                                                  PPCTDUDE
007910     IF DUDI-TUC(1:1) = SPACE                                     PPCTDUDE
007920     OR  (DUDI-TUC(1:1) NOT NUMERIC                               PPCTDUDE
007930      AND DUDI-TUC(1:1) NOT ALPHABETIC)                           PPCTDUDE
007940     OR  (DUDI-TUC(2:1) NOT NUMERIC                               PPCTDUDE
007950      AND DUDI-TUC(2:1) NOT ALPHABETIC)                           PPCTDUDE
008000        ADD 1                  TO KCTE-ERR                        PPCTDUDE
008100        MOVE M01793            TO KCTE-ERR-NO (KCTE-ERR)          PPCTDUDE
008200        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTDUDE
008300        GO TO 1099-EDIT-KEY-EXIT                                  PPCTDUDE
008400     ELSE                                                         PPCTDUDE
008500        MOVE DUDI-TUC TO DUD-TUC                                  PPCTDUDE
008600     END-IF.                                                      PPCTDUDE
008700     IF DUDI-VALID-RDUC                                           PPCTDUDE
008800        MOVE DUDI-RDUC         TO DUD-RDUC                        PPCTDUDE
008900     ELSE                                                         PPCTDUDE
009000        ADD 1                  TO KCTE-ERR                        PPCTDUDE
009100        MOVE M01794            TO KCTE-ERR-NO (KCTE-ERR)          PPCTDUDE
009200        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTDUDE
009300        GO TO 1099-EDIT-KEY-EXIT                                  PPCTDUDE
009400     END-IF.                                                      PPCTDUDE
009500     IF DUDI-TUC = '00'                                           PPCTDUDE
009600        MOVE '<<' TO DUD-TUC                                      PPCTDUDE
009700     END-IF.                                                      PPCTDUDE
009800                                                                  PPCTDUDE
009900 1099-EDIT-KEY-EXIT.                                              PPCTDUDE
010000     EXIT.                                                        PPCTDUDE
010100                                                                  PPCTDUDE
010200 2000-ADD-DUD SECTION.                                            PPCTDUDE
010300                                                                  PPCTDUDE
010400     IF SQLCODE = ZERO                                            PPCTDUDE
010500        ADD 1                  TO KCTE-ERR                        PPCTDUDE
010600        MOVE M01027            TO KCTE-ERR-NO (KCTE-ERR)          PPCTDUDE
010700        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTDUDE
010800        GO TO 2099-ADD-EXIT                                       PPCTDUDE
010900     END-IF.                                                      PPCTDUDE
011000     MOVE DUDI-DESCRIPTION     TO DUD-DESCRIPTION.                PPCTDUDE
011100     MOVE 'A'                  TO DUD-LAST-ACTION.                PPCTDUDE
011200     MOVE KCTE-LAST-ACTION-DT  TO DUD-LAST-ACTION-DT.             PPCTDUDE
011300                                                                  PPCTDUDE
011400 2099-ADD-EXIT.                                                   PPCTDUDE
011500     EXIT.                                                        PPCTDUDE
011600                                                                  PPCTDUDE
011700 2100-CHANGE-DUD SECTION.                                         PPCTDUDE
011800                                                                  PPCTDUDE
011900     IF SQLCODE NOT = ZERO                                        PPCTDUDE
012000        ADD 1                         TO KCTE-ERR                 PPCTDUDE
012100        MOVE M01028                   TO KCTE-ERR-NO (KCTE-ERR)   PPCTDUDE
012200        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTDUDE
012300        GO TO 2199-CHANGE-EXIT                                    PPCTDUDE
012400     END-IF.                                                      PPCTDUDE
012500     IF DUDI-DESCRIPTION = '*'                                    PPCTDUDE
012600        MOVE SPACE TO DUD-DESCRIPTION                             PPCTDUDE
012700     ELSE                                                         PPCTDUDE
012800       IF DUDI-DESCRIPTION NOT = SPACE                            PPCTDUDE
012900          MOVE DUDI-DESCRIPTION TO DUD-DESCRIPTION                PPCTDUDE
013000       END-IF                                                     PPCTDUDE
013100     END-IF.                                                      PPCTDUDE
013200     MOVE 'C'                   TO  DUD-LAST-ACTION.              PPCTDUDE
013300     MOVE KCTE-LAST-ACTION-DT   TO  DUD-LAST-ACTION-DT.           PPCTDUDE
013400                                                                  PPCTDUDE
013500 2199-CHANGE-EXIT.                                                PPCTDUDE
013600     EXIT.                                                        PPCTDUDE
013700                                                                  PPCTDUDE
013800 2200-DELETE-DUD SECTION.                                         PPCTDUDE
013900                                                                  PPCTDUDE
014000     IF SQLCODE NOT = ZERO                                        PPCTDUDE
014100        ADD 1                   TO KCTE-ERR                       PPCTDUDE
014200        MOVE M01029             TO KCTE-ERR-NO (KCTE-ERR)         PPCTDUDE
014300        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTDUDE
014400        GO TO 2299-DELETE-EXIT                                    PPCTDUDE
014500     END-IF.                                                      PPCTDUDE
014600                                                                  PPCTDUDE
014700 2299-DELETE-EXIT.                                                PPCTDUDE
014800     EXIT.                                                        PPCTDUDE
014900                                                                  PPCTDUDE
015000 9000-SELECT-DUD SECTION.                                         PPCTDUDE
015100                                                                  PPCTDUDE
015200     MOVE '9000-SEL-DUD ' TO DB2MSG-TAG                           PPCTDUDE
015300     EXEC SQL                                                     PPCTDUDE
015400         SELECT   DUD_TUC                                         PPCTDUDE
015500                 ,DUD_RDUC                                        PPCTDUDE
015600                 ,DUD_DESCRIPTION                                 PPCTDUDE
015700                 ,DUD_LAST_ACTION                                 PPCTDUDE
015800                 ,DUD_LAST_ACTION_DT                              PPCTDUDE
015900           INTO                                                   PPCTDUDE
016000                 :DUD-TUC                                         PPCTDUDE
016100                ,:DUD-RDUC                                        PPCTDUDE
016200                ,:DUD-DESCRIPTION                                 PPCTDUDE
016300                ,:DUD-LAST-ACTION                                 PPCTDUDE
016400                ,:DUD-LAST-ACTION-DT                              PPCTDUDE
016500           FROM   PPPVZDUD_DUD                                    PPCTDUDE
016600          WHERE   DUD_TUC           = :DUD-TUC                    PPCTDUDE
016700            AND   DUD_RDUC          = :DUD-RDUC                   PPCTDUDE
016800     END-EXEC.                                                    PPCTDUDE
016900                                                                  PPCTDUDE
017000 9099-SELECT-DUD-EXIT.                                            PPCTDUDE
017100     EXIT.                                                        PPCTDUDE
017200                                                                  PPCTDUDE
017300*999999-SQL-ERROR.                                                PPCTDUDE
017400     EXEC SQL                                                     PPCTDUDE
017500        INCLUDE CPPDXP99                                          PPCTDUDE
017600     END-EXEC.                                                    PPCTDUDE
017700     ADD 1                         TO KCTE-ERR.                   PPCTDUDE
017800     MOVE MCT005                   TO KCTE-ERR-NO (KCTE-ERR).     PPCTDUDE
017900     SET KCTE-STATUS-NOT-COMPLETED TO TRUE.                       PPCTDUDE
018000     GO TO 0100-END.                                              PPCTDUDE
