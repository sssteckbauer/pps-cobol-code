       IDENTIFICATION DIVISION.                                         00020000
       PROGRAM-ID.           PPLWS101.
       AUTHOR.               NELSON PENALOSA.                           00040000
       DATE-WRITTEN.         APRIL, 2010.                               00050000
       DATE-COMPILED.                                                   00060000
      *REMARKS.                                                         00070000
      ******************************************************************00080000
      *                                                                 00100000
      *           PPLWS101 - LOCAL PAYROLL BATCH PROCESS                00100000
      *                                                                 00100000
      *    THIS PROGRAM WILL PROCESS THE DOS EXT. AND GSRTF TRANS.      00100000
      *    CREATED BY PPLWS100 AND UPDATE ITS RESPECTIVE DATASET/       00110000
      *    PARMLIB MEMBER.                                              00110000
      *                                                                 00130000
      *    INPUTS:                                                      00140000
      *          DOSTRANS      :  PPST.PPLWS100.DOS.TRANS               00150000
      *          GSRTRANS      :  PPST.PPLWS100.GSR.TRANS               00150000
      *    UPDATES:                                                     00140000
      *          DATABASE      :  NONE                                  00150000
      *    OUTPUT:                                                      00170000
      *          DOSEXTEN      :  PPSP.DOSEXT - MOD                     00190000
      *          PARM3284      :  PPSP.PARMLIB(PPL714C1)                00190000
      *          PARM3285      :  PPSP.PARMLIB(PPL714C2)                00190000
      *          PARM3286      :  PPSP.PARMLIB(PPL714C3)                00190000
      *          PARM3287      :  PPSP.PARMLIB(PPL714C4)                00190000
      *          PARM3262      :  PPSP.PARMLIB(PPL714C5)                00190000
      *          PARM3263      :  PPSP.PARMLIB(PPL714C6)                00190000
      *          PARM3264      :  PPSP.PARMLIB(PPL714C7)                00190000
      *                                                                 00210000
      ******************************************************************00080000
      *                                                                 00270100
      * MODIFICATIONS:                                                  00271000
      * AUTHOR    DATE      FP   DESCRIPTION
      * ------  --------  -----  -------------------------------------
      *
      ***************************************************************** 00280000

      **-------------------------------------------------------------- *
      **
      **  *** TABLE VIEWS USED IN THIS PROGRAM
      **      NONE
      **
      **-------------------------------------------------------------- *

       ENVIRONMENT DIVISION.                                            00300000
                                                                        00310000
       CONFIGURATION SECTION.                                           00320000
                                                                        00330000
       INPUT-OUTPUT SECTION.                                            00370000
       FILE-CONTROL.                                                    00380000
                                                                        00390000
           SELECT DOSTRANS-FILE    ASSIGN TO DOSTRANS.                  00400000
                                                                        00420000
           SELECT GSRTRANS-FILE    ASSIGN TO GSRTRANS.                  00400000
                                                                        00420000
           SELECT DOSEXTEN-FILE    ASSIGN TO DOSEXTEN.                  00400000
                                                                        00420000
           SELECT PARM3284-FILE    ASSIGN TO PARM3284.                  00400000
                                                                        00420000
           SELECT PARM3285-FILE    ASSIGN TO PARM3285.                  00400000
                                                                        00420000
           SELECT PARM3286-FILE    ASSIGN TO PARM3286.                  00400000
                                                                        00420000
           SELECT PARM3287-FILE    ASSIGN TO PARM3287.                  00400000
                                                                        00420000
           SELECT PARM3262-FILE    ASSIGN TO PARM3262.                  00400000
                                                                        00420000
           SELECT PARM3263-FILE    ASSIGN TO PARM3263.                  00400000
                                                                        00420000
           SELECT PARM3264-FILE    ASSIGN TO PARM3264.                  00400000
                                                                        00420000
                                                                        00420000
                                                                        00420000
       DATA DIVISION.                                                   00425000
                                                                        00426000
       FILE SECTION.                                                    00490000
                                                                        00500000
       FD  DOSTRANS-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  DOSTRANS-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  GSRTRANS-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  GSRTRANS-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  DOSEXTEN-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  DOSEXTEN-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  PARM3284-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  PARM3284-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  PARM3285-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  PARM3285-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  PARM3286-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  PARM3286-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  PARM3287-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  PARM3287-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  PARM3262-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  PARM3262-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  PARM3263-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  PARM3263-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
       FD  PARM3264-FILE                                                00510000
           RECORDING MODE IS F                                          00520000
           BLOCK CONTAINS 0 RECORDS.                                    00540000
                                                                        00570000
       01  PARM3264-FILE-REC               PIC X(080).                  00580000
                                                                        00590000
                                                                        00590000
      /                                                                 00690000
       WORKING-STORAGE SECTION.                                         00700000

       01  FILLER                      PIC X(40)       VALUE
               'PPLWS101 WORKING-STORAGE BEGINS HERE    '.

       01  WS-MISCELLANEOUS.
           05  WS-DOSTRANS-COUNT       PIC  9(5)       VALUE ZEROES.
           05  WS-GSRTRANS-COUNT       PIC  9(5)       VALUE ZEROES.
           05  EOF-DOS                 PIC X(01)       VALUE 'N'.
           05  EOF-GSRTF               PIC X(01)       VALUE 'N'.
           05  WS-CURR-PARAGRAPH       PIC X(30)       VALUE ' '.
           05  WS-ERR-MSG1             PIC X(40)       VALUE ' '.
           05  WS-ERR-MSG2             PIC X(40)       VALUE ' '.
           05  WS-ERR-MSG3             PIC X(40)       VALUE ' '.

       01  DOSTRANS-FILE-RECORD.
           05  PPST-DOSEXT.
               10  DOS-EXT-CODE                PIC X(03).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-A                   PIC X(01).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-O                   PIC X(01).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-OPT1                PIC X(03).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-OPT2                PIC X(03).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-OPT3                PIC X(03).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-OPT4                PIC X(03).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-OPT5                PIC X(03).
               10  FILLER                      PIC X(01).
               10  DOS-EXT-OPT6                PIC X(03).
               10  FILLER                      PIC X(49).

       01  GSRTRANS-FILE-RECORD.
           05  GSRTF-RATES.
               10  GSRTF-NEW-RATE              PIC X(06).
               10  GSRTF-YEAR-EFF              PIC X(04).
               10  GSRTF-MONTH                 PIC X(02).
               10  FILLER                      PIC X(02).
               10  GSRTF-OLD-RATE              PIC X(06).
               10  FILLER                      PIC X(02).
               10  GSRTF-TITLE-CODE            PIC X(04).
               10  FILLER                      PIC X(54).


       EJECT                                                            03160000
       PROCEDURE DIVISION.                                              03180000

      ***************************************************************** 03190000
       0000-MAINLINE-ROUTINE.                                           03200000
      ***************************************************************** 03210000
                                                                        03220000
           PERFORM 1000-INITIALIZATION      THRU 1000-EXIT.             03230000
                                                                        03240000
           PERFORM 2300-PROCESS-DOSTRANS    THRU 2300-EXIT              03250000
             UNTIL EOF-DOS   = 'Y'.
                                                                        03250000
           PERFORM 2400-PROCESS-GSRTRANS    THRU 2400-EXIT              03250000
             UNTIL EOF-GSRTF = 'Y'.
                                                                        03270000
           PERFORM 9000-END-OF-JOB          THRU 9000-EXIT.             03300000

           GOBACK.


       1000-INITIALIZATION.                                             03350000
      ***************************************************************** 03360000
                                                                        03370000
           OPEN INPUT  DOSTRANS-FILE                                    03410000
                       GSRTRANS-FILE.                                   03410000
                                                                        03370000
           OPEN OUTPUT DOSEXTEN-FILE.                                   03410000
                                                                        03370000
       1000-EXIT.                                                       03350000
           EXIT.
                                                                        03370000
                                                                        03370000
       2300-PROCESS-DOSTRANS.
      *********************************
      ** DOS EXTENSION TRANSACTION   **
      *********************************

           READ DOSTRANS-FILE INTO DOSTRANS-FILE-RECORD
             AT END
                MOVE 'Y' TO EOF-DOS
                GO TO 2300-EXIT.

           ADD 1 TO WS-DOSTRANS-COUNT.
           WRITE DOSEXTEN-FILE-REC FROM DOSTRANS-FILE-RECORD.

       2300-EXIT.
           EXIT.


       2400-PROCESS-GSRTRANS.
      *********************************
      ** GSRTF RATES TRANSACTION     **
      *********************************

           READ GSRTRANS-FILE INTO GSRTRANS-FILE-RECORD
             AT END
                MOVE 'Y' TO EOF-GSRTF
                GO TO 2400-EXIT.


           ADD 1 TO WS-GSRTRANS-COUNT.

           EVALUATE GSRTF-TITLE-CODE
               WHEN '3284'
                  PERFORM 2421-UPDATE-PARM3284 THRU 2421-EXIT
               WHEN '3285'
                  PERFORM 2422-UPDATE-PARM3285 THRU 2422-EXIT
               WHEN '3286'
                  PERFORM 2423-UPDATE-PARM3286 THRU 2423-EXIT
               WHEN '3287'
                  PERFORM 2424-UPDATE-PARM3287 THRU 2424-EXIT
               WHEN '3262'
                  PERFORM 2425-UPDATE-PARM3262 THRU 2425-EXIT
               WHEN '3263'
                  PERFORM 2426-UPDATE-PARM3263 THRU 2426-EXIT
               WHEN '3264'
                  PERFORM 2427-UPDATE-PARM3264 THRU 2427-EXIT
               WHEN OTHER
                  MOVE 'INVALID GSRTF TITLE CODE' TO WS-ERR-MSG1
                  MOVE GSRTF-TITLE-CODE           TO WS-ERR-MSG2
                  MOVE 'VERIFY PPPTRN ENTRY     ' TO WS-ERR-MSG3
                  PERFORM 9900-ABORT THRU 9900-ABORT-EXIT
           END-EVALUATE.

       2400-EXIT.
           EXIT.


       2421-UPDATE-PARM3284.
           OPEN OUTPUT PARM3284-FILE.

           WRITE PARM3284-FILE-REC FROM GSRTRANS-FILE-RECORD.

           CLOSE PARM3284-FILE.
       2421-EXIT.
           EXIT.


       2422-UPDATE-PARM3285.
           OPEN OUTPUT PARM3285-FILE.

           WRITE PARM3285-FILE-REC FROM GSRTRANS-FILE-RECORD.

           CLOSE PARM3285-FILE.
       2422-EXIT.
           EXIT.


       2423-UPDATE-PARM3286.
           OPEN OUTPUT PARM3286-FILE.

           WRITE PARM3286-FILE-REC FROM GSRTRANS-FILE-RECORD.

           CLOSE PARM3286-FILE.
       2423-EXIT.
           EXIT.


       2424-UPDATE-PARM3287.
           OPEN OUTPUT PARM3287-FILE.

           WRITE PARM3287-FILE-REC FROM GSRTRANS-FILE-RECORD.

           CLOSE PARM3287-FILE.
       2424-EXIT.
           EXIT.


       2425-UPDATE-PARM3262.
           OPEN OUTPUT PARM3262-FILE.

           WRITE PARM3262-FILE-REC FROM GSRTRANS-FILE-RECORD.

           CLOSE PARM3262-FILE.
       2425-EXIT.
           EXIT.


       2426-UPDATE-PARM3263.
           OPEN OUTPUT PARM3263-FILE.

           WRITE PARM3263-FILE-REC FROM GSRTRANS-FILE-RECORD.

           CLOSE PARM3263-FILE.
       2426-EXIT.
           EXIT.


       2427-UPDATE-PARM3264.
           OPEN OUTPUT PARM3264-FILE.

           WRITE PARM3264-FILE-REC FROM GSRTRANS-FILE-RECORD.

           CLOSE PARM3264-FILE.
       2427-EXIT.
           EXIT.


       EJECT                                                            04800000
       9000-END-OF-JOB.                                                 04850000

           CLOSE DOSTRANS-FILE                                          05360000
                 GSRTRANS-FILE                                          05360000
                 DOSEXTEN-FILE.                                         05360000
                                                                        05380000
                                                                        05380000
           DISPLAY '*=========================================*'.
           DISPLAY '                                           '.
           DISPLAY '     **PPLWS101 CONTROL TOTALS**           '.
           DISPLAY '                                           '.
           DISPLAY '    DOS TRANS. READ  = ' WS-DOSTRANS-COUNT.
           DISPLAY '    GSRTF TRANS READ = ' WS-GSRTRANS-COUNT.
           DISPLAY '                                           '.
           DISPLAY '*=========================================*'.
                                                                        05340000
       9000-EXIT.                                                       04850000
           EXIT.
                                                                        05490000
      ****************************************************************  05500000
       9900-ABORT.                                                      05510000
      ****************************************************************  05520000
                                                                        05530000
           DISPLAY ' '.                                                 05540000
           DISPLAY '** -------------------------------------------**'.
           DISPLAY ' '.                                                 05550000
           DISPLAY 'ABENDING PROGRAM ---> ' 'PPLWS101'.
           DISPLAY 'CURRENT PARAGRAPH --> ' WS-CURR-PARAGRAPH.
           DISPLAY 'ERROR MESSAGES -----> ' WS-ERR-MSG1.
           DISPLAY '               -----> ' WS-ERR-MSG2.
           DISPLAY '               -----> ' WS-ERR-MSG3.
           DISPLAY '** -------------------------------------------**'.
                                                                        05590000
           MOVE 0016 TO RETURN-CODE.
           STOP RUN.                                                    05610000
                                                                        05620000
       9900-ABORT-EXIT.                                                 04850000
           EXIT.
                                                                        05590000
************************ END OF PPLWS101  ***********************
