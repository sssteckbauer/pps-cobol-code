000100**************************************************************/   30871401
000200*  PROGRAM: PPCTR39                                          */   30871401
000300*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000400*  NAME:___SRS___________ CREATION DATE:      ___03/20/02__  */   30871401
000500*  DESCRIPTION:                                              */   30871401
000600*  PPPDES TABLE REPORT MODULE                                */   30871401
000700**************************************************************/   30871401
000800 IDENTIFICATION DIVISION.                                         PPCTR39
000900 PROGRAM-ID. PPCTR39.                                             PPCTR39
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTR39
001100 DATE-COMPILED.                                                   PPCTR39
001200 ENVIRONMENT DIVISION.                                            PPCTR39
001300 CONFIGURATION SECTION.                                           PPCTR39
001400 SPECIAL-NAMES.                                                   PPCTR39
001500     C01 IS NEW-PAGE,                                             PPCTR39
001600     CSP IS NO-LINES.                                             PPCTR39
001700 INPUT-OUTPUT SECTION.                                            PPCTR39
001800 FILE-CONTROL.                                                    PPCTR39
001900     SELECT REPORT-PRINT-FILE     ASSIGN TO UT-S-PPP0439.         PPCTR39
002000 DATA DIVISION.                                                   PPCTR39
002100 FILE SECTION.                                                    PPCTR39
002200 FD  REPORT-PRINT-FILE            COPY CPFDXPRT.                  PPCTR39
002300 WORKING-STORAGE SECTION.                                         PPCTR39
002400 01  MISC.                                                        PPCTR39
002500     05  LINE-COUNT               PIC S9(4) COMP SYNC VALUE +99.  PPCTR39
002600     05  NO-OF-LINES              PIC S9(4) COMP SYNC VALUE +1.   PPCTR39
002700     05  PAGE-COUNT               PIC S9(6) COMP SYNC VALUE ZERO. PPCTR39
002800 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTR39
002900 01  CTH-CTL-TABLE-MAINT-HEADERS.               COPY CPWSXCHR.    PPCTR39
003000 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTR39
003100 01  RPT39-HD1.                                                   PPCTR39
003200     05  FILLER                   PIC X(133) VALUE SPACES.        PPCTR39
003300 01  RPT39-HD2.                                                   PPCTR39
003400     05  FILLER                   PIC X(01) VALUE SPACES.         PPCTR39
003500     05  FILLER                   PIC X(44) VALUE SPACES.         PPCTR39
003600     05  FILLER                   PIC X(10) VALUE ' DATA BASE'.   PPCTR39
003700     05  FILLER                   PIC X(10) VALUE '/   DATA  '.   PPCTR39
003800     05  FILLER                   PIC X(10) VALUE '   SCREEN '.   PPCTR39
003900     05  FILLER                   PIC X(10) VALUE '  PROTECT '.   PPCTR39
004000     05  FILLER                   PIC X(08) VALUE SPACES.         PPCTR39
004100 01  RPT39-HD3.                                                   PPCTR39
004200     05  FILLER                   PIC X(01) VALUE SPACES.         PPCTR39
004300     05  FILLER                   PIC X(44) VALUE SPACES.         PPCTR39
004400     05  FILLER                   PIC X(10) VALUE '   FILE ID'.   PPCTR39
004500     05  FILLER                   PIC X(10) VALUE '   ELEMENT'.   PPCTR39
004600     05  FILLER                   PIC X(10) VALUE '     ID   '.   PPCTR39
004700     05  FILLER                   PIC X(10) VALUE '    IND   '.   PPCTR39
004800     05  FILLER                   PIC X(08) VALUE SPACES.         PPCTR39
004900 01  RPT39-D1.                                                    PPCTR39
005000     05  FILLER                   PIC X(01).                      PPCTR39
005100     05  FILLER                   PIC X(49).                      PPCTR39
005200     05  RPT39-DATABASE-ID        PIC X(03).                      PPCTR39
005300     05  FILLER                   PIC X(06).                      PPCTR39
005400     05  RPT39-ELEM-NO            PIC 9(04).                      PPCTR39
005500     05  FILLER                   PIC X(06).                      PPCTR39
005600     05  RPT39-SCREEN-ID          PIC X(04).                      PPCTR39
005700     05  FILLER                   PIC X(06).                      PPCTR39
005800     05  RPT39-PROTECT-IND        PIC X(03).                      PPCTR39
005900     05  FILLER                   PIC X(06).                      PPCTR39
006000                                                                  PPCTR39
006100 01  DES-TABLE-DATA.                                              PPCTR39
006200     EXEC SQL                                                     PPCTR39
006300        INCLUDE PPPVZDES                                          PPCTR39
006400     END-EXEC.                                                    PPCTR39
006500                                                                  PPCTR39
006600     EXEC SQL                                                     PPCTR39
006700         DECLARE  DES_ROW CURSOR FOR                              PPCTR39
006800         SELECT   DES_DATABASE_ID                                 PPCTR39
006900                 ,DES_ELEM_NO                                     PPCTR39
007000                 ,DES_SCREEN_ID                                   PPCTR39
007100                 ,DES_PROTECT_IND                                 PPCTR39
007200         FROM     PPPVZDES_DES                                    PPCTR39
007300         ORDER BY DES_DATABASE_ID                                 PPCTR39
007400                 ,DES_ELEM_NO                                     PPCTR39
007500                 ,DES_SCREEN_ID                                   PPCTR39
007600     END-EXEC.                                                    PPCTR39
007700                                                                  PPCTR39
007800     EXEC SQL                                                     PPCTR39
007900        INCLUDE SQLCA                                             PPCTR39
008000     END-EXEC.                                                    PPCTR39
008100                                                                  PPCTR39
008200 LINKAGE SECTION.                                                 PPCTR39
008300                                                                  PPCTR39
008400 01  CTPI-CTL-TBL-PRINT-INTERFACE.              COPY CPLNCTPI.    PPCTR39
008500                                                                  PPCTR39
008600 PROCEDURE DIVISION USING CTPI-CTL-TBL-PRINT-INTERFACE.           PPCTR39
008700                                                                  PPCTR39
008800 0000-MAIN SECTION.                                               PPCTR39
008900                                                                  PPCTR39
009000     EXEC SQL                                                     PPCTR39
009100        INCLUDE CPPDXE99                                          PPCTR39
009200     END-EXEC.                                                    PPCTR39
009300                                                                  PPCTR39
009400     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTR39
009500     PERFORM 2000-CREATE-REPORT.                                  PPCTR39
009600     PERFORM 9000-PROGRAM-TERMINATION.                            PPCTR39
009700                                                                  PPCTR39
009800 0999-END.                                                        PPCTR39
009900                                                                  PPCTR39
010000     GOBACK.                                                      PPCTR39
010100                                                                  PPCTR39
010200 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTR39
010300                                                                  PPCTR39
010400     MOVE 'PPCTR39' TO XWHC-DISPLAY-MODULE.                       PPCTR39
010500     COPY CPPDXWHC.                                               PPCTR39
010600     MOVE CTPI-HEADER-1 TO CTH-RPT-HD1.                           PPCTR39
010700     MOVE CTPI-HEADER-2 TO CTH-RPT-HD2.                           PPCTR39
010800     MOVE CTPI-HEADER-3 TO CTH-RPT-HD3.                           PPCTR39
010900     MOVE XWHC-MONTH-COMPILED TO CTH-COMPILE-MM.                  PPCTR39
011000     MOVE XWHC-DAY-COMPILED TO CTH-COMPILE-DD.                    PPCTR39
011100     MOVE XWHC-YEAR-COMPILED TO CTH-COMPILE-YY.                   PPCTR39
011200     OPEN OUTPUT REPORT-PRINT-FILE.                               PPCTR39
011300                                                                  PPCTR39
011400 1999-INITIALIZATION-EXIT.                                        PPCTR39
011500     EXIT.                                                        PPCTR39
011600                                                                  PPCTR39
011700 2000-CREATE-REPORT SECTION.                                      PPCTR39
011800                                                                  PPCTR39
011900     PERFORM 7000-OPEN-DES.                                       PPCTR39
012000     PERFORM 7100-FETCH-DES.                                      PPCTR39
012100     PERFORM 6000-PAGE-HEADER.                                    PPCTR39
012200                                                                  PPCTR39
012300     PERFORM 3000-PRINT-DES-TABLE                                 PPCTR39
012400       UNTIL SQLCODE NOT = ZERO.                                  PPCTR39
012500                                                                  PPCTR39
012600     PERFORM 7200-CLOSE-DES.                                      PPCTR39
012700                                                                  PPCTR39
012800 2999-CREATE-EXIT.                                                PPCTR39
012900     EXIT.                                                        PPCTR39
013000                                                                  PPCTR39
013100 3000-PRINT-DES-TABLE SECTION.                                    PPCTR39
013200                                                                  PPCTR39
013300     MOVE SPACES TO RPT39-D1.                                     PPCTR39
013400     MOVE DES-DATABASE-ID TO RPT39-DATABASE-ID.                   PPCTR39
013500     MOVE DES-ELEM-NO     TO RPT39-ELEM-NO.                       PPCTR39
013600     MOVE DES-SCREEN-ID   TO RPT39-SCREEN-ID.                     PPCTR39
013700     IF DES-PROTECT-IND = 'Y'                                     PPCTR39
013800        MOVE 'YES'        TO  RPT39-PROTECT-IND                   PPCTR39
013900     ELSE                                                         PPCTR39
014000        MOVE 'NO '        TO RPT39-PROTECT-IND                    PPCTR39
014100     END-IF.                                                      PPCTR39
014200     MOVE RPT39-D1        TO PRINT-REC.                           PPCTR39
014300     MOVE 2               TO NO-OF-LINES.                         PPCTR39
014400     PERFORM 8000-PRINT-A-LINE.                                   PPCTR39
014500                                                                  PPCTR39
014600     PERFORM 7100-FETCH-DES.                                      PPCTR39
014700                                                                  PPCTR39
014800 3999-PRINT-EXIT.                                                 PPCTR39
014900     EXIT.                                                        PPCTR39
015000                                                                  PPCTR39
015100 6000-PAGE-HEADER SECTION.                                        PPCTR39
015200                                                                  PPCTR39
015300     COPY CPPDXCHR.                                               PPCTR39
015500     MOVE RPT39-HD1 TO PRINT-REC.                                 PPCTR39
015600     MOVE 2 TO NO-OF-LINES.                                       PPCTR39
015700     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR39
015800     MOVE RPT39-HD2 TO PRINT-REC.                                 PPCTR39
015900     MOVE 3 TO NO-OF-LINES.                                       PPCTR39
016000     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR39
016100     MOVE RPT39-HD3 TO PRINT-REC.                                 PPCTR39
016200     MOVE 1 TO NO-OF-LINES.                                       PPCTR39
016300     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR39
016400     ADD 6 TO LINE-COUNT.                                         PPCTR39
016500     MOVE SPACES TO PRINT-REC.                                    PPCTR39
016600                                                                  PPCTR39
016700 6999-HEADER-EXIT.                                                PPCTR39
016800     EXIT.                                                        PPCTR39
016900                                                                  PPCTR39
017000 7000-OPEN-DES SECTION.                                           PPCTR39
017100                                                                  PPCTR39
017200     MOVE 'OPEN DES CURSOR' TO DB2MSG-TAG.                        PPCTR39
017300     EXEC SQL                                                     PPCTR39
017400         OPEN DES_ROW                                             PPCTR39
017500     END-EXEC.                                                    PPCTR39
017600                                                                  PPCTR39
017700 7099-OPEN-EXIT.                                                  PPCTR39
017800     EXIT.                                                        PPCTR39
017900                                                                  PPCTR39
018000 7100-FETCH-DES SECTION.                                          PPCTR39
018100                                                                  PPCTR39
018200     MOVE 'FETCH DES ROW' TO DB2MSG-TAG.                          PPCTR39
018300     EXEC SQL                                                     PPCTR39
018400     FETCH DES_ROW                                                PPCTR39
018500          INTO                                                    PPCTR39
018600          :DES-DATABASE-ID                                        PPCTR39
018700         ,:DES-ELEM-NO                                            PPCTR39
018800         ,:DES-SCREEN-ID                                          PPCTR39
018900         ,:DES-PROTECT-IND                                        PPCTR39
019000     END-EXEC.                                                    PPCTR39
019100                                                                  PPCTR39
019200 7099-FETCH-DES-EXIT.                                             PPCTR39
019300     EXIT.                                                        PPCTR39
019400                                                                  PPCTR39
019500 7200-CLOSE-DES SECTION.                                          PPCTR39
019600                                                                  PPCTR39
019700     MOVE 'CLOSE DES CURSOR' TO DB2MSG-TAG.                       PPCTR39
019800     EXEC SQL                                                     PPCTR39
019900         CLOSE DES_ROW                                            PPCTR39
020000     END-EXEC.                                                    PPCTR39
020100                                                                  PPCTR39
020200 7299-CLOSE-EXIT.                                                 PPCTR39
020300     EXIT.                                                        PPCTR39
020400                                                                  PPCTR39
020500                                                                  PPCTR39
020600 8000-PRINT-A-LINE SECTION.                                       PPCTR39
020700                                                                  PPCTR39
020800     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR39
020900     MOVE SPACES TO PRINT-REC.                                    PPCTR39
021000     ADD NO-OF-LINES TO LINE-COUNT.                               PPCTR39
021100     IF LINE-COUNT > 54                                           PPCTR39
021200        PERFORM 6000-PAGE-HEADER                                  PPCTR39
021300     END-IF.                                                      PPCTR39
021400     MOVE 1 TO NO-OF-LINES.                                       PPCTR39
021500                                                                  PPCTR39
021600 8999-PRINT-EXIT.                                                 PPCTR39
021700     EXIT.                                                        PPCTR39
021800                                                                  PPCTR39
021900 9000-PROGRAM-TERMINATION SECTION.                                PPCTR39
022000                                                                  PPCTR39
022100     CLOSE REPORT-PRINT-FILE.                                     PPCTR39
022200                                                                  PPCTR39
022300 9999-TERMINATION-EXIT.                                           PPCTR39
022400     EXIT.                                                        PPCTR39
022500                                                                  PPCTR39
022600*999999-SQL-ERROR.                                                PPCTR39
022700     EXEC SQL                                                     PPCTR39
022800        INCLUDE CPPDXP99                                          PPCTR39
022900     END-EXEC.                                                    PPCTR39
023000     MOVE '99' TO CTPI-RETURN-CODE.                               PPCTR39
023100     GO TO 0999-END.                                              PPCTR39
