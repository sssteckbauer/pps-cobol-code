000100**************************************************************/   36330704
000200*  PROGRAM: PPBUDHUP                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPBUDH UPDATE MODULE                               */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPBUDHUP
001000 PROGRAM-ID. PPBUDHUP.                                            PPBUDHUP
001100 ENVIRONMENT DIVISION.                                            PPBUDHUP
001200 DATA DIVISION.                                                   PPBUDHUP
001300 WORKING-STORAGE SECTION.                                         PPBUDHUP
001400*                                                                 PPBUDHUP
001500     COPY CPWSXBEG.                                               PPBUDHUP
001600*                                                                 PPBUDHUP
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPBUDHUP'.       PPBUDHUP
001800*                                                                 PPBUDHUP
001900     EXEC SQL                                                     PPBUDHUP
002000         INCLUDE CPWSWRKC                                         PPBUDHUP
002100     END-EXEC.                                                    PPBUDHUP
002200*                                                                 PPBUDHUP
002300 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPBUDHUP
002400*                                                                 PPBUDHUP
002500 01  PPPBUDH-ROW EXTERNAL.                                        PPBUDHUP
002600     EXEC SQL                                                     PPBUDHUP
002700         INCLUDE PPPVBUDH                                         PPBUDHUP
002800     END-EXEC.                                                    PPBUDHUP
002900*                                                                 PPBUDHUP
003000*------------------------------------------------------------*    PPBUDHUP
003100*  SQL DCLGEN AREA                                           *    PPBUDHUP
003200*------------------------------------------------------------*    PPBUDHUP
003300     EXEC SQL                                                     PPBUDHUP
003400         INCLUDE SQLCA                                            PPBUDHUP
003500     END-EXEC.                                                    PPBUDHUP
003600*                                                                 PPBUDHUP
003700 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPBUDHUP
003800*                                                                 PPBUDHUP
003900     COPY CPWSXEND.                                               PPBUDHUP
004000*                                                                 PPBUDHUP
004100 LINKAGE SECTION.                                                 PPBUDHUP
004200*                                                                 PPBUDHUP
004300 01  SELECT-INTERFACE-AREA.                                       PPBUDHUP
004400     COPY CPWSXTIF REPLACING ==:TAG:== BY ==XSEL==.               PPBUDHUP
004500*                                                                 PPBUDHUP
004600 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPBUDHUP
004700*                                                                 PPBUDHUP
004800     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPBUDHUP
004900     COPY  CPPDXWHC.                                              PPBUDHUP
005000*                                                                 PPBUDHUP
005100     EXEC SQL                                                     PPBUDHUP
005200         INCLUDE CPPDXE99                                         PPBUDHUP
005300     END-EXEC.                                                    PPBUDHUP
005400*                                                                 PPBUDHUP
005500     COPY CPPDUPDC.                                               PPBUDHUP
005600*                                                                 PPBUDHUP
005700 2000-UPDATE SECTION.                                             PPBUDHUP
005800*                                                                 PPBUDHUP
005900     MOVE 'UPDATE PPPBUDH ROW' TO DB2MSG-TAG.                     PPBUDHUP
006000*                                                                 PPBUDHUP
006100     EXEC SQL                                                     PPBUDHUP
006200         UPDATE PPPVBUDH_BUDH                                     PPBUDHUP
006300         SET BUD_LAST_ACTION    = :BUD-LAST-ACTION    ,           PPBUDHUP
006400             BUD_LAST_ACTION_DT = :BUD-LAST-ACTION-DT             PPBUDHUP
006500         WHERE BUD_BUC            = :WS-PPPBUD-BUC                PPBUDHUP
006600         AND   BUD_DISTRIBUTION   = :WS-PPPBUD-DIST               PPBUDHUP
006700         AND   BUD_ACCT_OR_FUND_1 = :WS-PPPBUD-ACCT-1             PPBUDHUP
006800         AND   BUD_ACCT_OR_FUND_2 = :WS-PPPBUD-ACCT-2             PPBUDHUP
006900         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPBUDHUP
007000         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPBUDHUP
007100     END-EXEC.                                                    PPBUDHUP
007200*                                                                 PPBUDHUP
007300     IF SQLCODE = +100                                            PPBUDHUP
007400         INITIALIZE XSEL-RETURN-KEY                               PPBUDHUP
007500         SET XSEL-INVALID-KEY TO TRUE                             PPBUDHUP
007600     END-IF.                                                      PPBUDHUP
007700*                                                                 PPBUDHUP
007800 2000-EXIT.                                                       PPBUDHUP
007900     EXIT.                                                        PPBUDHUP
008000*                                                                 PPBUDHUP
008100 3000-INSERT SECTION.                                             PPBUDHUP
008200*                                                                 PPBUDHUP
008300     MOVE 'INSERT PPPBUDH ROW' TO DB2MSG-TAG.                     PPBUDHUP
008400*                                                                 PPBUDHUP
008500     EXEC SQL                                                     PPBUDHUP
008600         INSERT                                                   PPBUDHUP
008700         INTO PPPVBUDH_BUDH                                       PPBUDHUP
008800         VALUES (:PPPBUDH-ROW)                                    PPBUDHUP
008900     END-EXEC.                                                    PPBUDHUP
009000*                                                                 PPBUDHUP
009100 3000-EXIT.                                                       PPBUDHUP
009200     EXIT.                                                        PPBUDHUP
009300*                                                                 PPBUDHUP
009400 4000-DELETE SECTION.                                             PPBUDHUP
009500*                                                                 PPBUDHUP
009600     MOVE 'DELETE PPPBUDH ROW' TO DB2MSG-TAG.                     PPBUDHUP
009700*                                                                 PPBUDHUP
009800     EXEC SQL                                                     PPBUDHUP
009900         DELETE                                                   PPBUDHUP
010000         FROM PPPVBUDH_BUDH                                       PPBUDHUP
010100         WHERE BUD_BUC            = :WS-PPPBUD-BUC                PPBUDHUP
010200         AND   BUD_DISTRIBUTION   = :WS-PPPBUD-DIST               PPBUDHUP
010300         AND   BUD_ACCT_OR_FUND_1 = :WS-PPPBUD-ACCT-1             PPBUDHUP
010400         AND   BUD_ACCT_OR_FUND_2 = :WS-PPPBUD-ACCT-2             PPBUDHUP
010500         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPBUDHUP
010600         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPBUDHUP
010700     END-EXEC.                                                    PPBUDHUP
010800*                                                                 PPBUDHUP
010900     IF SQLCODE = +100                                            PPBUDHUP
011000         INITIALIZE XSEL-RETURN-KEY                               PPBUDHUP
011100         SET XSEL-INVALID-KEY TO TRUE                             PPBUDHUP
011200     END-IF.                                                      PPBUDHUP
011300*                                                                 PPBUDHUP
011400 4000-EXIT.                                                       PPBUDHUP
011500     EXIT.                                                        PPBUDHUP
011600*                                                                 PPBUDHUP
011700*999999-SQL-ERROR SECTION.                                        PPBUDHUP
011800     COPY CPPDXP99.                                               PPBUDHUP
011900     SET XSEL-DB2-ERROR TO TRUE.                                  PPBUDHUP
012000     GOBACK.                                                      PPBUDHUP
012100 999999-EXIT.                                                     PPBUDHUP
012200     EXIT.                                                        PPBUDHUP
012300*                                                                 PPBUDHUP
