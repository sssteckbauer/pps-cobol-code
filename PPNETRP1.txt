000100******************************************************************36300699
000200*  PROGRAM: PPNETRP1                                             *36300699
000300*  RELEASE: ____0699____  SERVICE REQUEST(S): ____3630____       *36300699
000400*  NAME:_PXP______________MODIFICATION DATE:___09/17/92___       *36300699
000500*  DESCRIPTION:                                                  *36300699
000600*  - CHANGED FOR THF CONVERSION TO DB2                           *36300699
000700******************************************************************36300699
000800**************************************************************/   36090564
000900*  PROGRAM: PPNETRP1                                         */   36090564
000030*  RELEASE: ___0564______ SERVICE REQUEST(S): _____3609____  */   36090564
000040*  NAME:_________KXK_____ CREATION DATE:      ___04/00/91__  */   36090564
000050*  DESCRIPTION:                                              */   36090564
000060*  - MODIFIED PROGRAM TO FACILITATE PERFORMANCE.             */   36090564
000070**************************************************************/   36090564
000100**************************************************************/   36030532
000200*  PROGRAM: PPNETRP1                                         */   36030532
000300*  RELEASE: __0532______  SERVICE REQUEST(S): ____3726____   */   36030532
000400*  NAME:__SRS__           MODIFICATION DATE:  __09/06/90__   */   36030532
000500*  DESCRIPTION:                                              */   36030532
000600*  - MODIFIED FOR RUSH CHECKS PROCESSING                     */   36030532
000700**************************************************************/   36030532
000100**************************************************************/  *37550526
000200*  PROGRAM: PPNETRP1                                         */  *37550526
000300*  RELEASE: __0526______  SERVICE REQUEST(S): ____3755____   */  *37550526
000400*  NAME:__KXK__           MODIFICATION DATE:  __12/10/90__   */  *37550526
000500*  DESCRIPTION:                                              */  *37550526
000600*   - CREATED TO CALL NEW MODULES WHICH REPLACE EXISTING CODE*/  *37550526
000700**************************************************************/  *37550526
000100**************************************************************/  *36030513
000200*  PROGRAM: PPNETRP1                                         */  *36030513
000300*  RELEASE: __0513______  SERVICE REQUEST(S): ____3603____   */  *36030513
000400*  NAME:__SRS__           MODIFICATION DATE:  __11/05/90__   */  *36030513
000500*  DESCRIPTION:                                              */  *36030513
000600*   - CREATED TO CALL NEW MODULES WHICH REPLACE EXISTING CODE*/  *36030513
000700**************************************************************/  *36030513
000800 IDENTIFICATION DIVISION.                                         PPNETRP1
000900 PROGRAM-ID. PPNETRP1                                             PPNETRP1
001000 DATE-COMPILED.                                                   PPNETRP1
001100 ENVIRONMENT DIVISION.                                            PPNETRP1
001200 CONFIGURATION SECTION.                                           PPNETRP1
001300 SOURCE-COMPUTER.            COPY CPOTXISI.                       PPNETRP1
001400 OBJECT-COMPUTER.            COPY CPOTXOBJ.                       PPNETRP1
001500 SPECIAL-NAMES.                                                   PPNETRP1
001600     C01 IS NEW-PAGE,                                             PPNETRP1
001700     CSP IS NO-LINES-PRINTED.                                     PPNETRP1
001800 INPUT-OUTPUT SECTION.                                            PPNETRP1
001900 FILE-CONTROL.                                                    PPNETRP1
002000     SELECT  PRINT-FILE                      COPY CPSLXPRT.       PPNETRP1
002100******************************************************************PPNETRP1
002200*                D A T A  D I V I S I O N                        *PPNETRP1
002300******************************************************************PPNETRP1
002400*                                                                 PPNETRP1
002500 DATA DIVISION.                                                   PPNETRP1
002600 FILE SECTION.                                                    PPNETRP1
002700 FD  PRINT-FILE                          COPY CPFDXPRT.           PPNETRP1
002800     EJECT                                                        PPNETRP1
002900     EJECT                                                        PPNETRP1
003000 WORKING-STORAGE SECTION.                                         PPNETRP1
005900     SKIP3                                                        36300699
006000 01  PAYROLL-PROCESS-ID EXTERNAL. COPY CPWSXPPI.                  36300699
003100 01  MISC-REPORT-ITEMS.                                           PPNETRP1
003200     05  A-STND-PROG-ID          PIC X(15)       VALUE            PPNETRP1
003300         'PPP400  /122085'.                                       PPNETRP1
003400     05  LINE-CNT                PIC S9(3)       COMP-3 VALUE +0. PPNETRP1
003500     SKIP2                                                        PPNETRP1
003600 01  UC-REPORT-LINES.                                             PPNETRP1
003700     05  UC-REPT-LINE-1.                                          PPNETRP1
003800         10  FILLER          PIC  X         VALUE SPACES.         PPNETRP1
003900         10  FILLER          PIC  X(69)     VALUE '---------------PPNETRP1
004000-        '-------------INPUT------------------------------'.      PPNETRP1
004100         10  FILLER          PIC  X(63)     VALUE '---------------PPNETRP1
004200-        '-------------OUTPUT-----------------------------'.      PPNETRP1
004300     05  UC-REPT-LINE-2.                                          PPNETRP1
004400         10  FILLER          PIC  X         VALUE SPACES.         PPNETRP1
004500         10  FILLER          PIC  X(41)     VALUE 'TRANSACTION'.  PPNETRP1
004600         10  FILLER          PIC  X(28)     VALUE 'RECORD'.       PPNETRP1
004700         10  FILLER          PIC  X(41)     VALUE 'TRANSACTION'.  PPNETRP1
004800         10  FILLER          PIC  X(22)     VALUE 'RECORD'.       PPNETRP1
004900     05  UC-REPT-LINE-3.                                          PPNETRP1
005000         10  FILLER          PIC  X         VALUE SPACES.         PPNETRP1
005100         10  FILLER          PIC  X(41)     VALUE 'CODE  TYPE'.   PPNETRP1
005200         10  FILLER          PIC  X(28)                           PPNETRP1
005300             VALUE ' COUNT        DOLLARS'.                       PPNETRP1
005400         10  FILLER          PIC  X(41)     VALUE 'CODE  TYPE'.   PPNETRP1
005500         10  FILLER          PIC  X(22)                           PPNETRP1
005600             VALUE ' COUNT        DOLLARS'.                       PPNETRP1
005700     05  UC-REPT-LINE-4.                                          PPNETRP1
005800         10  FILLER          PIC  X(07)     VALUE SPACES.         PPNETRP1
005900         10  UCRL-TTL-4L     PIC  X(34)     VALUE SPACES.         PPNETRP1
006000         10  UCRL-AMT-4L     PIC  ZZZ,ZZ9.                        PPNETRP1
006100         10  FILLER          PIC  X(28)     VALUE SPACES.         PPNETRP1
006200         10  UCRL-TTL-4R     PIC  X(34)     VALUE SPACES.         PPNETRP1
006300         10  UCRL-AMT-4R     PIC  ZZZ,ZZ9.                        PPNETRP1
006400         10  FILLER          PIC  X(16)     VALUE SPACES.         PPNETRP1
006500     05  UC-REPT-LINE-5.                                          PPNETRP1
006600         10  FILLER          PIC  X(76)     VALUE SPACES.         PPNETRP1
006700         10  UCRL-TTL-5R     PIC  X(34).                          PPNETRP1
006800         10  UCRL-AMT-5R     PIC  ZZZ,ZZ9.                        PPNETRP1
006900         10  FILLER          PIC  X(16)     VALUE SPACES.         PPNETRP1
007000     05  UC-REPT-LINE-6.                                          PPNETRP1
007100         10  FILLER          PIC  X(07)     VALUE SPACES.         PPNETRP1
007200         10  FILLER          PIC  X(69)                           PPNETRP1
007300             VALUE 'SUMMARY OF GROSSES'.                          PPNETRP1
007400         10  FILLER          PIC  X(57)                           PPNETRP1
007500             VALUE 'SUMMARY OF GROSSES'.                          PPNETRP1
007600     05  UC-REPT-LINE-7.                                          PPNETRP1
007700         10  FILLER          PIC  X(12)     VALUE SPACES.         PPNETRP1
007800         10  UCRL-TTL-7IN    PIC  X(29)     VALUE SPACES.         PPNETRP1
007900         10  FILLER          PIC X(7) VALUE SPACES.               PPNETRP1
008000         10  UCRL-DOL-7IN    PIC  ZZZZ,ZZZ,ZZ9.99-.               PPNETRP1
008100         10  FILLER          PIC  X(17)     VALUE SPACES.         PPNETRP1
008200         10  UCRL-TTL-7OUT   PIC  X(29)     VALUE SPACES.         PPNETRP1
008300         10  FILLER          PIC X(7) VALUE SPACES.               PPNETRP1
008400         10  UCRL-DOL-7OUT   PIC  ZZZZ,ZZZ,ZZ9.99-.               PPNETRP1
008500     05  UC-REPT-LINE-7-LV.                                       PPNETRP1
008600         10  FILLER           PIC  X(12)     VALUE SPACES.        PPNETRP1
008700         10  UCRL-TTL-7IN-LV  PIC  X(29)     VALUE SPACES.        PPNETRP1
008800         10  FILLER           PIC X(3) VALUE SPACES.              PPNETRP1
008900         10  UCRL-DOL-7IN-LV  PIC  ZZZZ,ZZZ,ZZ9.999999-.          PPNETRP1
009000         10  FILLER           PIC  X(17)     VALUE SPACES.        PPNETRP1
009100         10  UCRL-TTL-7OUT-LV PIC  X(29)     VALUE SPACES.        PPNETRP1
009200         10  FILLER           PIC X(3) VALUE SPACES.              PPNETRP1
009300         10  UCRL-DOL-7OUT-LV PIC  ZZZZ,ZZZ,ZZ9.999999-.          PPNETRP1
009400     05  UC-REPT-LINE-8.                                          PPNETRP1
009500         10  FILLER          PIC  X(02)     VALUE SPACES.         PPNETRP1
009600         10  UCRL-TYPE-8IN   PIC  X(02)     VALUE SPACES.         PPNETRP1
009700         10  FILLER          PIC  X(03)     VALUE SPACES.         PPNETRP1
009800         10  UCRL-TTL-8IN    PIC  X(34)     VALUE SPACES.         PPNETRP1
009900         10  UCRL-REC-8IN    PIC  ZZZ,ZZ9.                        PPNETRP1
010000         10  UCRL-DOL-8IN    PIC  ZZZZ,ZZZ,ZZ9.99-.               PPNETRP1
010100         10  FILLER          PIC  X(07)     VALUE SPACES.         PPNETRP1
010200         10  UCRL-TYPE-8OUT  PIC  X(02).                          PPNETRP1
010300         10  FILLER          PIC  X(03)     VALUE SPACES.         PPNETRP1
010400         10  UCRL-TTL-8OUT   PIC  X(34)     VALUE SPACES.         PPNETRP1
010500         10  UCRL-REC-8OUT   PIC  ZZZ,ZZ9.                        PPNETRP1
010600         10  UCRL-DOL-8OUT   PIC  ZZZZ,ZZZ,ZZ9.99-.               PPNETRP1
010700     05  UC-REPT-LINE-9.                                          PPNETRP1
010800         10  FILLER          PIC  X(12)     VALUE SPACES.         PPNETRP1
010900         10  UCRL-TYPE-9IN   PIC  X(29)     VALUE SPACES.         PPNETRP1
011000         10  UCRL-REC-9IN    PIC  ZZZ,ZZ9.                        PPNETRP1
011100         10  FILLER          PIC  X(16)     VALUE SPACES.         PPNETRP1
011200         10  UCRL-TYPE-9OUT  PIC  X(29)     VALUE SPACES.         PPNETRP1
011300         10  UCRL-REC-9OUT   PIC  ZZZ,ZZ9.                        PPNETRP1
011400         10  FILLER          PIC  X(16)     VALUE SPACES.         PPNETRP1
011500      EJECT                                                       PPNETRP1
011600 01  STND-RPT-HD.                  COPY CPWSPSHR.                 PPNETRP1
011700     EJECT                                                        PPNETRP1
011800 01  XPTT-TRANSACTION-TITLES.     COPY CPRPXPTT.                  PPNETRP1
011900     EJECT                                                        PPNETRP1
012000 01  XP71-RPT-TITLE.              COPY CPRPXP71.                  PPNETRP1
012100       EJECT                                                      PPNETRP1
012200 01  REPORT-LINE1.                                                PPNETRP1
012300     05  CARRIAGE-CTL-CHAR       PIC X.                           PPNETRP1
012400     05  XRL-TYPE-DESC.                                           PPNETRP1
012500         10  FILLER              PIC X.                           PPNETRP1
012600         10  XRL-ELMNT-NO        PIC 9(3).                        PPNETRP1
012700         10  FILLER              PIC X.                           PPNETRP1
012800         10  XRL-ELMNT-DESC      PIC X(13).                       PPNETRP1
012900     05  XRL-GTN-GROSS REDEFINES XRL-TYPE-DESC.                   PPNETRP1
013000         10  FILLER              PIC X(3).                        PPNETRP1
013100         10  XRL-GROSS-AMT       PIC ZZZ,ZZZ,ZZZ.99-.             PPNETRP1
013200     05  XRL-TYPE-SPREAD.                                         PPNETRP1
013300         10  XRL-TYPE-AMT        PIC ZZ,ZZZ,ZZZ.ZZ- OCCURS 8.     PPNETRP1
013400     05  XRL-TYPE-SPREADB REDEFINES XRL-TYPE-SPREAD.              PPNETRP1
013500         10  XRL-TYPE-AMTB       PIC ZZ,ZZZ,ZZZ.99- OCCURS 8.     PPNETRP1
013600 01  REPORT-LINE3 REDEFINES REPORT-LINE1.                         PPNETRP1
013700     05  CARRIAGE-CTL-CHAR       PIC X.                           PPNETRP1
013800     05  FILLER                  PIC X(50).                       PPNETRP1
013900     05  XRL-RPT-TITLE           PIC X(32).                       PPNETRP1
014000     EJECT                                                        PPNETRP1
014800*01  XPCR-RECORD.                COPY CPWSXPCR.                   36090564
014810 01  XPCR-RECORD EXTERNAL.       COPY CPWSXPC2.                   36090564
014200     EJECT                                                        PPNETRP1
014300 01  DATE-FORMAT.                COPY CPWSDATE.                   PPNETRP1
014400     EJECT                                                        PPNETRP1
014500 01  PAYROLL-CONSTANTS.          COPY CPWSXIC2.                   PPNETRP1
014600 LINKAGE SECTION.                                                 PPNETRP1
014700     SKIP2                                                        PPNETRP1
015500*01  XPCR-RECORD-LINK            PIC X(224).                      36090564
014900     SKIP2                                                        PPNETRP1
015000 01  RECORD-COUNTS.                                               PPNETRP1
015100     05  XRC-TRANSACTION-TYPES.                                   PPNETRP1
015200         10  XRC-PAY-AUDIT-IN    PIC S9(7)       COMP-3.          PPNETRP1
015300         10  XRC-PAY-AUDIT-OUT   PIC S9(7)       COMP-3.          PPNETRP1
015400         10  XRC-COM3940-IN      PIC S9(7)       COMP-3.          PPNETRP1
015500         10  XRC-EDBCHG-OUT      PIC S9(7)       COMP-3.          PPNETRP1
015600         10  XRC-EXPENSE-TRANSFR PIC S9(7)       COMP-3.          PPNETRP1
015700         10  XRC-CANCELLATIONS   PIC S9(7)       COMP-3.          PPNETRP1
015800         10  XRC-OVERPAYMENTS    PIC S9(7)       COMP-3.          PPNETRP1
015900         10  XRC-HAND-DRAWN      PIC S9(7)       COMP-3.          PPNETRP1
016000         10  XRC-CURRENT-ACTIVTY PIC S9(7)       COMP-3.          PPNETRP1
016100         10  XRC-EXPENSE-TRANSFR-IN PIC S9(7)   COMP-3.           PPNETRP1
016200         10  XRC-CANCELLATIONS-IN PIC S9(7)      COMP-3.          PPNETRP1
016300         10  XRC-OVERPAYMENTS-IN PIC S9(7)       COMP-3.          PPNETRP1
016400         10  XRC-HAND-DRAWN-IN   PIC S9(7)       COMP-3.          PPNETRP1
016500         10  XRC-CURRENT-ACTIVTY-IN PIC S9(7)  COMP-3.            PPNETRP1
016600         10  XRC-PAY-AUDIT-REM   PIC S9(7)       COMP-3.          PPNETRP1
016700         10  XRC-R-HAND-DRAWN      PIC S9(7)     COMP-3.          PPNETRP1
016800         10  XRC-R-HAND-DRAWN-IN   PIC S9(7)     COMP-3.          PPNETRP1
016900     05  XRC-TRAN-OCCURS REDEFINES XRC-TRANSACTION-TYPES COMP-3.  PPNETRP1
017000         10  XRC-TRAN-CNT        PIC S9(7)       OCCURS 17 TIMES. PPNETRP1
017100     SKIP2                                                        PPNETRP1
017200 01  DOLLAR-TOTALS.                                               PPNETRP1
017300     05  XDT-GROSS-TOTAL-IN      PIC S9(9)V99    COMP-3.          PPNETRP1
017400     05  XDT-GROSS-FWT-IN        PIC S9(9)V99    COMP-3.          PPNETRP1
017510*****05  XDT-GROSS-FICA-IN       PIC S9(9)V99    COMP-3.          37550526
017520     05  XDT-GROSS-OASDI-IN      PIC S9(9)V99    COMP-3.          37550526
017530     05  XDT-GROSS-MEDCR-IN      PIC S9(9)V99    COMP-3.          37550526
017600     05  XDT-GROSS-STATE-IN      PIC S9(9)V99    COMP-3.          PPNETRP1
017700     05  XDT-NET-PAY-IN          PIC S9(9)V99    COMP-3.          PPNETRP1
017800     05  XDT-EXPENSE-TRANSFER-IN PIC S9(9)V99    COMP-3.          PPNETRP1
017900     05  XDT-CANCELLATIONS-IN    PIC S9(9)V99    COMP-3.          PPNETRP1
018000     05  XDT-OVERPAYMENTS-IN     PIC S9(9)V99    COMP-3.          PPNETRP1
018100     05  XDT-HANDDRAWN-IN        PIC S9(9)V99    COMP-3.          PPNETRP1
018200     05  XDT-DOLLAR-ADJ-TOT-IN   PIC S9(9)V99    COMP-3.          PPNETRP1
018300     05  XDT-HOUR-ADJS-IN        PIC S9(9)V99    COMP-3.          PPNETRP1
018400     05  XDT-LEAVE-ADJS-IN       PIC S9(9)V9(6)  COMP-3.          PPNETRP1
018500     05  XDT-CURRENT-GROSS-IN    PIC S9(9)V99    COMP-3.          PPNETRP1
018600     05  XDT-ADVANCE-IN          PIC S9(9)V99    COMP-3.          PPNETRP1
018700     05  XDT-GROSS-TOTAL         PIC S9(9)V99    COMP-3.          PPNETRP1
018800     05  XDT-GROSS-FWT           PIC S9(9)V99    COMP-3.          PPNETRP1
018900*****05  XDT-GROSS-FICA          PIC S9(9)V99    COMP-3.          37550526
018910     05  XDT-GROSS-OASDI         PIC S9(9)V99    COMP-3.          37550526
018920     05  XDT-GROSS-MEDCR         PIC S9(9)V99    COMP-3.          37550526
019000     05  XDT-GROSS-STATE         PIC S9(9)V99    COMP-3.          PPNETRP1
019100     05  XDT-NET-PAY             PIC S9(9)V99    COMP-3.          PPNETRP1
019200     05  XDT-EXPENSE-TRANSFER    PIC S9(9)V99    COMP-3.          PPNETRP1
019300     05  XDT-CANCELLATIONS-1     PIC S9(9)V99    COMP-3.          PPNETRP1
019400     05  XDT-OVERPAYMENTS-1      PIC S9(9)V99    COMP-3.          PPNETRP1
019500     05  XDT-HANDDRAWN-1         PIC S9(9)V99    COMP-3.          PPNETRP1
019600     05  XDT-DOLLAR-ADJ-TOT      PIC S9(9)V99    COMP-3.          PPNETRP1
019700     05  XDT-GENERATED-DOLR-ADJ  PIC S9(9)V99    COMP-3.          PPNETRP1
019800     05  XDT-WORK-AMT            PIC S9(9)V99    COMP-3.          PPNETRP1
019900     05  XDT-HOUR-ADJS           PIC S9(9)V99    COMP-3.          PPNETRP1
020000     05  XDT-LEAVE-ADJS          PIC S9(9)V9(6)  COMP-3.          PPNETRP1
020100     05  XDT-CURRENT-GROSS       PIC S9(9)V99    COMP-3.          PPNETRP1
020200     05  XDT-ADVANCE-1           PIC S9(9)V99    COMP-3.          PPNETRP1
020300     05  XDT-R-HANDDRAWN-IN      PIC S9(9)V99    COMP-3.          PPNETRP1
020400     05  XDT-R-HANDDRAWN-1       PIC S9(9)V99    COMP-3.          PPNETRP1
020500     EJECT                                                        PPNETRP1
020600******************************************************************PPNETRP1
020700*                P R O C E D U R E  D I V I S I O N              *PPNETRP1
020800******************************************************************PPNETRP1
020900     SKIP2                                                        PPNETRP1
021700*PROCEDURE DIVISION USING XPCR-RECORD-LINK                        36090564
021710 PROCEDURE DIVISION USING RECORD-COUNTS                           36090564
021800**************************RECORD-COUNTS                           36090564
021200                          DOLLAR-TOTALS.                          PPNETRP1
021300     SKIP1                                                        PPNETRP1
022100*****MOVE XPCR-RECORD-LINK TO XPCR-RECORD.                        36090564
021500     SKIP1                                                        PPNETRP1
021600     COPY CPPDDATE.                                               PPNETRP1
021700     SKIP1                                                        PPNETRP1
021800     OPEN OUTPUT PRINT-FILE.                                      PPNETRP1
021900     MOVE XPCR-CYCLE-TYPE (1) TO STND-RPT-CYCLE (1).              PPNETRP1
022000     MOVE XPCR-CYCLE-TYPE (2) TO STND-RPT-CYCLE (2).              PPNETRP1
022100     MOVE XPCR-CYCLE-TYPE (3) TO STND-RPT-CYCLE (3).              PPNETRP1
022200     MOVE XPCR-CYCLE-TYPE (4) TO STND-RPT-CYCLE (4).              PPNETRP1
022300     MOVE SPACES TO REPORT-LINE1.                                 PPNETRP1
022400     MOVE 'PPP4001' TO STND-RPT-ID.                               PPNETRP1
022500     MOVE A-STND-PROG-ID TO STND-PROG-ID.                         PPNETRP1
022600     MOVE ZEROS TO STND-PAGE-NO.                                  PPNETRP1
022700     MOVE GREGORIAN-DATE TO STND-RPT-DATE.                        PPNETRP1
022800     MOVE XPCR-END-YEAR  TO STND-RPT-END-YR                       PPNETRP1
022900     MOVE XPCR-END-MONTH TO STND-RPT-END-MONTH.                   PPNETRP1
023000     MOVE XPCR-END-DAY   TO STND-RPT-END-DAY.                     PPNETRP1
023100     MOVE XPCR-CHECK-YEAR  TO STND-DATE2-YEAR.                    PPNETRP1
023200     MOVE XPCR-CHECK-MONTH TO STND-DATE2-MONTH.                   PPNETRP1
023300     MOVE XPCR-CHECK-DAY   TO STND-DATE2-DAY.                     PPNETRP1
023400     MOVE '      -- GROSS-TO-NET PROCESS --' TO STND-RPT-TTL.     PPNETRP1
024110     SKIP1                                                        36030532
024120     IF XPCR-TRUNC-RCD (1:5) = 'RCNET'                            36030532
024130        STRING ' PPRCNET1/PPRCNET/090690'                         36030532
024140           DELIMITED BY SIZE INTO STND-RPT-HD1                    36030532
024150        MOVE SPACES TO STND-RPT-COMMENT                           36030532
024160        MOVE 'PAYROLL RUSH CHECK PROCESSING' TO STND-FUNC-AREA    36030532
024170        MOVE GREGORIAN-DATE TO STND-DATE2                         36030532
027800     ELSE                                                         36300699
027900         MOVE 'PPI: ' TO STND-PPI-TTL                             36300699
028000         MOVE XPPI-PPI TO STND-PPI                                36300699
024180     END-IF                                                       36030532
024190     SKIP1                                                        36030532
023500     COPY CPPDXP01.                                               PPNETRP1
023600     MOVE SPACES TO REPORT-LINE3.                                 PPNETRP1
023700     MOVE XP71-STND-RPT-TTL TO XRL-RPT-TITLE.                     PPNETRP1
023800     MOVE REPORT-LINE3 TO PRINT-REC.                              PPNETRP1
023900     WRITE PRINT-REC AFTER ADVANCING 3 LINES.                     PPNETRP1
024000     MOVE UC-REPT-LINE-1 TO PRINT-REC.                            PPNETRP1
024100     WRITE PRINT-REC AFTER ADVANCING 3 LINES.                     PPNETRP1
024200     MOVE UC-REPT-LINE-2 TO PRINT-REC.                            PPNETRP1
024300     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
024400     MOVE UC-REPT-LINE-3 TO PRINT-REC.                            PPNETRP1
024500     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
024600     MOVE 'COM3940  FILE RCDS READ      ' TO UCRL-TTL-4L.         PPNETRP1
024700     MOVE XRC-COM3940-IN                       TO UCRL-AMT-4L.    PPNETRP1
024800     MOVE 'EDBCHG   FILE RCDS WRITTEN     ' TO UCRL-TTL-4R.       PPNETRP1
024900     MOVE XRC-EDBCHG-OUT                   TO UCRL-AMT-4R.        PPNETRP1
025000     MOVE UC-REPT-LINE-4 TO PRINT-REC.                            PPNETRP1
025100     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
025200     MOVE 'PRLMN PAYROLL AUDIT FILE RCDS READ' TO UCRL-TTL-4L.    PPNETRP1
025300     MOVE XRC-PAY-AUDIT-IN                     TO UCRL-AMT-4L.    PPNETRP1
025400     MOVE 'PAYROLL AUDIT FILE RCDS DELETED' TO UCRL-TTL-4R.       PPNETRP1
025500     MOVE XRC-PAY-AUDIT-REM                 TO UCRL-AMT-4R.       PPNETRP1
025600     MOVE UC-REPT-LINE-4 TO PRINT-REC.                            PPNETRP1
025700     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
025800     MOVE 'PAYROLL AUDIT FILE RCDS WRITTEN' TO UCRL-TTL-5R.       PPNETRP1
025900     MOVE XRC-PAY-AUDIT-OUT                 TO UCRL-AMT-5R.       PPNETRP1
026000     MOVE UC-REPT-LINE-5 TO PRINT-REC.                            PPNETRP1
026100     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
026200     MOVE UC-REPT-LINE-6 TO PRINT-REC.                            PPNETRP1
026300     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
026400     MOVE 'TOTAL GROSS'   TO UCRL-TTL-7IN UCRL-TTL-7OUT.          PPNETRP1
026500     MOVE XDT-GROSS-TOTAL-IN TO UCRL-DOL-7IN.                     PPNETRP1
026600     MOVE XDT-GROSS-TOTAL TO UCRL-DOL-7OUT.                       PPNETRP1
026700     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
026800     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
026900     MOVE 'FWT GROSS'     TO UCRL-TTL-7IN UCRL-TTL-7OUT.          PPNETRP1
027000     MOVE XDT-GROSS-FWT-IN   TO UCRL-DOL-7IN.                     PPNETRP1
027100     MOVE XDT-GROSS-FWT   TO UCRL-DOL-7OUT.                       PPNETRP1
027200     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
027300     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
027400     MOVE 'SWT GROSS'     TO UCRL-TTL-7IN UCRL-TTL-7OUT.          PPNETRP1
027500     MOVE XDT-GROSS-STATE-IN TO UCRL-DOL-7IN.                     PPNETRP1
027600     MOVE XDT-GROSS-STATE TO UCRL-DOL-7OUT.                       PPNETRP1
027700     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
027800     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
027900*****MOVE 'FICA GROSS'    TO UCRL-TTL-7IN UCRL-TTL-7OUT.          37550526
028000*****MOVE XDT-GROSS-FICA-IN  TO UCRL-DOL-7IN.                     37550526
028100*****MOVE XDT-GROSS-FICA  TO UCRL-DOL-7OUT.                       37550526
028200*****MOVE UC-REPT-LINE-7 TO PRINT-REC.                            37550526
028300*****WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     37550526
028310     MOVE 'OASDI GROSS'   TO UCRL-TTL-7IN UCRL-TTL-7OUT.          37550526
028320     MOVE XDT-GROSS-OASDI-IN  TO UCRL-DOL-7IN.                    37550526
028330     MOVE XDT-GROSS-OASDI  TO UCRL-DOL-7OUT.                      37550526
028340     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            37550526
028350     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     37550526
028360     MOVE 'MEDICARE GROSS'   TO UCRL-TTL-7IN UCRL-TTL-7OUT.       37550526
028370     MOVE XDT-GROSS-MEDCR-IN  TO UCRL-DOL-7IN.                    37550526
028380     MOVE XDT-GROSS-MEDCR  TO UCRL-DOL-7OUT.                      37550526
028390     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            37550526
028391     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     37550526
028400     MOVE 'NET PAY'       TO UCRL-TTL-7IN UCRL-TTL-7OUT.          PPNETRP1
028500     MOVE XDT-NET-PAY-IN  TO UCRL-DOL-7IN.                        PPNETRP1
028600     MOVE XDT-NET-PAY     TO UCRL-DOL-7OUT.                       PPNETRP1
028700     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
028800     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
028900*                                                                 PPNETRP1
029000*     EXPENSE TRANSFERS                                           PPNETRP1
029100*                                                                 PPNETRP1
029200     MOVE EXPENSE-TRANSFER-PAR                                    PPNETRP1
029300         TO UCRL-TYPE-8IN UCRL-TYPE-8OUT.                         PPNETRP1
029400     MOVE XPTT-EXPENSE-TRANSFERS-1                                PPNETRP1
029500         TO UCRL-TTL-8IN UCRL-TTL-8OUT.                           PPNETRP1
029600     MOVE XRC-EXPENSE-TRANSFR-IN     TO UCRL-REC-8IN.             PPNETRP1
029700     MOVE XDT-EXPENSE-TRANSFER-IN    TO UCRL-DOL-8IN.             PPNETRP1
029800     MOVE XRC-EXPENSE-TRANSFR    TO UCRL-REC-8OUT.                PPNETRP1
029900     MOVE XDT-EXPENSE-TRANSFER   TO UCRL-DOL-8OUT.                PPNETRP1
030000     MOVE UC-REPT-LINE-8 TO PRINT-REC.                            PPNETRP1
030100     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
030200*                                                                 PPNETRP1
030300*     CANCELLATIONS                                               PPNETRP1
030400*                                                                 PPNETRP1
030500     MOVE CANCELLATION-PAR                                        PPNETRP1
030600         TO UCRL-TYPE-8IN UCRL-TYPE-8OUT.                         PPNETRP1
030700     MOVE XPTT-CANCELLATIONS-1                                    PPNETRP1
030800         TO UCRL-TTL-8IN UCRL-TTL-8OUT.                           PPNETRP1
030900     MOVE XRC-CANCELLATIONS-IN    TO UCRL-REC-8IN.                PPNETRP1
031000     MOVE XDT-CANCELLATIONS-IN    TO UCRL-DOL-8IN.                PPNETRP1
031100     MOVE XRC-CANCELLATIONS       TO UCRL-REC-8OUT.               PPNETRP1
031200     MOVE XDT-CANCELLATIONS-1     TO UCRL-DOL-8OUT.               PPNETRP1
031300     MOVE UC-REPT-LINE-8 TO PRINT-REC.                            PPNETRP1
031400     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
031500*                                                                 PPNETRP1
031600*     OVERPAYMENTS                                                PPNETRP1
031700*                                                                 PPNETRP1
031800     MOVE OVERPAYMENT-PAR                                         PPNETRP1
031900         TO UCRL-TYPE-8IN UCRL-TYPE-8OUT.                         PPNETRP1
032000     MOVE XPTT-OVERPAYMENTS-1                                     PPNETRP1
032100         TO UCRL-TTL-8IN UCRL-TTL-8OUT.                           PPNETRP1
032200     MOVE XRC-OVERPAYMENTS-IN     TO UCRL-REC-8IN.                PPNETRP1
032300     MOVE XDT-OVERPAYMENTS-IN     TO UCRL-DOL-8IN.                PPNETRP1
032400     MOVE XRC-OVERPAYMENTS        TO UCRL-REC-8OUT.               PPNETRP1
032500     MOVE XDT-OVERPAYMENTS-1      TO UCRL-DOL-8OUT.               PPNETRP1
032600     MOVE UC-REPT-LINE-8 TO PRINT-REC.                            PPNETRP1
032700     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
032800*                                                                 PPNETRP1
032900*    HAND-DRAWN CHECKS                                            PPNETRP1
033000*                                                                 PPNETRP1
033100     MOVE HAND-DRAWN-PAR                                          PPNETRP1
033200         TO UCRL-TYPE-8IN UCRL-TYPE-8OUT.                         PPNETRP1
033300     MOVE XPTT-HAND-DRAWNS-1                                      PPNETRP1
033400         TO UCRL-TTL-8IN UCRL-TTL-8OUT.                           PPNETRP1
033500     MOVE XRC-HAND-DRAWN-IN       TO UCRL-REC-8IN.                PPNETRP1
033600     MOVE XDT-HANDDRAWN-IN        TO UCRL-DOL-8IN.                PPNETRP1
033700     MOVE XRC-HAND-DRAWN          TO UCRL-REC-8OUT.               PPNETRP1
033800     MOVE XDT-HANDDRAWN-1         TO UCRL-DOL-8OUT.               PPNETRP1
033900     MOVE UC-REPT-LINE-8 TO PRINT-REC.                            PPNETRP1
034000     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
034710*                                                                 36030532
034720*    RUSH-HAND-DRAWN CHECKS                                       36030532
034730*                                                                 36030532
034740     MOVE R-HAND-DRAWN-PAR                                        36030532
034750         TO UCRL-TYPE-8IN UCRL-TYPE-8OUT.                         36030532
034760     MOVE XPTT-R-HAND-DRAWNS-1                                    36030532
034770         TO UCRL-TTL-8IN UCRL-TTL-8OUT.                           36030532
034780     MOVE XRC-R-HAND-DRAWN-IN       TO UCRL-REC-8IN.              36030532
034790     MOVE XDT-R-HANDDRAWN-IN        TO UCRL-DOL-8IN.              36030532
034791     MOVE XRC-R-HAND-DRAWN          TO UCRL-REC-8OUT.             36030532
034792     MOVE XDT-R-HANDDRAWN-1         TO UCRL-DOL-8OUT.             36030532
034793     MOVE UC-REPT-LINE-8 TO PRINT-REC.                            36030532
034794     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     36030532
034100*                                                                 PPNETRP1
034200*    CURRENT ACTIVITY                                             PPNETRP1
034300*                                                                 PPNETRP1
034400     MOVE CURRENT-ACTIVITY-PAR                                    PPNETRP1
034500         TO UCRL-TYPE-8IN UCRL-TYPE-8OUT.                         PPNETRP1
034600     MOVE XPTT-CURRENT-ACTIVITY                                   PPNETRP1
034700         TO UCRL-TTL-8IN UCRL-TTL-8OUT.                           PPNETRP1
034800     MOVE XRC-CURRENT-ACTIVTY-IN  TO UCRL-REC-8IN.                PPNETRP1
034900     MOVE XDT-CURRENT-GROSS-IN   TO UCRL-DOL-8IN.                 PPNETRP1
035000     MOVE XRC-CURRENT-ACTIVTY    TO UCRL-REC-8OUT.                PPNETRP1
035100     MOVE XDT-CURRENT-GROSS       TO UCRL-DOL-8OUT.               PPNETRP1
035200     MOVE UC-REPT-LINE-8 TO PRINT-REC.                            PPNETRP1
035300     WRITE PRINT-REC AFTER ADVANCING 2 LINES.                     PPNETRP1
035400*                                                                 PPNETRP1
035500*    CURRENT ACTIVITY - HOURS ADJUSTMENTS                         PPNETRP1
035600*                                                                 PPNETRP1
035700     MOVE 'HOURS ADJS'    TO UCRL-TTL-7IN UCRL-TTL-7OUT.          PPNETRP1
035800     MOVE XDT-HOUR-ADJS-IN   TO UCRL-DOL-7IN.                     PPNETRP1
035900     MOVE XDT-HOUR-ADJS   TO UCRL-DOL-7OUT.                       PPNETRP1
036000     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
036100     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
036200     MOVE 'LEAVE ADJS'    TO UCRL-TTL-7IN-LV UCRL-TTL-7OUT-LV.    PPNETRP1
036300     MOVE XDT-LEAVE-ADJS-IN   TO UCRL-DOL-7IN-LV.                 PPNETRP1
036400     MOVE XDT-LEAVE-ADJS   TO UCRL-DOL-7OUT-LV.                   PPNETRP1
036500     MOVE UC-REPT-LINE-7-LV TO PRINT-REC.                         PPNETRP1
036600     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
036700     MOVE 'DOLLAR ADJS'   TO UCRL-TTL-7IN UCRL-TTL-7OUT.          PPNETRP1
036800     MOVE XDT-DOLLAR-ADJ-TOT-IN TO  UCRL-DOL-7IN.                 PPNETRP1
036900     COMPUTE XDT-WORK-AMT =                                       PPNETRP1
037000         XDT-DOLLAR-ADJ-TOT  +  XDT-GENERATED-DOLR-ADJ.           PPNETRP1
037100     MOVE XDT-WORK-AMT       TO  UCRL-DOL-7OUT.                   PPNETRP1
037200     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
037300     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
037400     MOVE SPACES TO UC-REPT-LINE-7.                               PPNETRP1
037500     MOVE '   INPUT ADJ'  TO UCRL-TTL-7OUT.                       PPNETRP1
037600     MOVE XDT-DOLLAR-ADJ-TOT TO  UCRL-DOL-7OUT.                   PPNETRP1
037700     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
037800     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
037900     MOVE SPACES TO UC-REPT-LINE-7.                               PPNETRP1
038000     MOVE '   AUTO ADJS'  TO UCRL-TTL-7OUT.                       PPNETRP1
038100     MOVE XDT-GENERATED-DOLR-ADJ TO UCRL-DOL-7OUT.                PPNETRP1
038200     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
038300     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
038400     MOVE 'ADVANCES'      TO UCRL-TTL-7IN UCRL-TTL-7OUT.          PPNETRP1
038500     MOVE XDT-ADVANCE-IN     TO UCRL-DOL-7IN.                     PPNETRP1
038600     MOVE XDT-ADVANCE-1   TO UCRL-DOL-7OUT.                       PPNETRP1
038700     MOVE UC-REPT-LINE-7 TO PRINT-REC.                            PPNETRP1
038800     WRITE PRINT-REC AFTER ADVANCING 1 LINES.                     PPNETRP1
038900     SKIP2                                                        PPNETRP1
039000     CLOSE PRINT-FILE                                             PPNETRP1
039100     GOBACK.                                                      PPNETRP1
