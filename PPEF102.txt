000000**************************************************************/   28521025
000001*  PROGRAM: PPEF102                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/28/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE NAMES REFERENCED FROM COPYMEMBER CPWSXDTS HAVE      */   28521025
000007*    BEEN CHANGED TO THE NEW NAMES IN CPWSXDTS.              */   28521025
000010**************************************************************/   28521025
000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEF102                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*  - THIS NEW PROCESS WILL SET RETIREMENT ELIGIBILITY        *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEF102
001200 IDENTIFICATION DIVISION.                                         PPEF102
001300 PROGRAM-ID. PPEF102.                                             PPEF102
001400 AUTHOR. UCOP.                                                    PPEF102
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEF102
001600                                                                  PPEF102
001700 DATE-WRITTEN.  JULY 2,1992.                                      PPEF102
001800 DATE-COMPILED. XX/XX/XX.                                         PPEF102
001900     EJECT                                                        PPEF102
002000 ENVIRONMENT DIVISION.                                            PPEF102
002100 CONFIGURATION SECTION.                                           PPEF102
002200 SOURCE-COMPUTER.                       COPY CPOTXUCS.            PPEF102
002300 OBJECT-COMPUTER.                       COPY CPOTXOBJ.            PPEF102
002400 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEF102
002500     EJECT                                                        PPEF102
002600 INPUT-OUTPUT SECTION.                                            PPEF102
002700 FILE-CONTROL.                                                    PPEF102
002800                                                                  PPEF102
002900 DATA DIVISION.                                                   PPEF102
003000 FILE SECTION.                                                    PPEF102
003100*----------------------------------------------------------------*PPEF102
003200 WORKING-STORAGE SECTION.                                         PPEF102
003300*----------------------------------------------------------------*PPEF102
003400                                                                  PPEF102
003500 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEF102
003600*****'PPEF102 /111992'.                                           28521025
003610     'PPEF102 /080195'.                                           28521025
003700                                                                  PPEF102
003800 01  A-STND-MSG-PARMS                 REDEFINES                   PPEF102
003900     A-STND-PROG-ID.                                              PPEF102
004000     05  FILLER                       PIC X(03).                  PPEF102
004100     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEF102
004200     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEF102
004300     05  FILLER                       PIC X(08).                  PPEF102
004400                                                                  PPEF102
004500 01  MISC-WORK-AREAS.                                             PPEF102
004600     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEF102
004700         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEF102
004800         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEF102
004900                                                                  PPEF102
005000 01  MESSAGES-USED.                                               PPEF102
005100     05  XXX001                       PIC X(05) VALUE 'XX001'.    PPEF102
005200                                                                  PPEF102
005300 01  ELEMENTS-USED.                                               PPEF102
005400     05  E0122                        PIC 9(04) VALUE  0122.      PPEF102
005500                                                                  PPEF102
005600                                                                  PPEF102
005700     EJECT                                                        PPEF102
005800 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEF102
005900                                                                  PPEF102
006000     EJECT                                                        PPEF102
006100*01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. 28521025
006200*****                                                             28521025
006300*****EJECT                                                        28521025
006400 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEF102
006200                                                                  PPEF102
006300     EJECT                                                        PPEF102
006700 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEF102
006500                                                                  PPEF102
006600     EJECT                                                        PPEF102
007000*01  DATE-WORK-AREA.                               COPY CPWSDATE. 28521025
007100*****                                                             28521025
007200*****EJECT                                                        28521025
007300 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEF102
007400                                                                  PPEF102
007500     EJECT                                                        PPEF102
007600 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEF102
007700                                                                  PPEF102
007800     EJECT                                                        PPEF102
007900 01  FY-NON-DEDUCT-RETIRE-CODES.                   COPY CPWSXRET. PPEF102
008000                                                                  PPEF102
008100     EJECT                                                        PPEF102
008200******************************************************************PPEF102
008300**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEF102
008400**--------------------------------------------------------------**PPEF102
008500** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEF102
008600******************************************************************PPEF102
008700                                                                  PPEF102
008800 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEF102
008900                                                                  PPEF102
009000     EJECT                                                        PPEF102
009100 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEF102
009200                                                                  PPEF102
009300     EJECT                                                        PPEF102
009400 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEF102
009500                                                                  PPEF102
009600     EJECT                                                        PPEF102
009700 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEF102
009800                                                                  PPEF102
009900 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEF102
010000                                                                  PPEF102
010100 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEF102
010200                                                                  PPEF102
010300 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEF102
010400                                                                  PPEF102
010500     EJECT                                                        PPEF102
010600 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEF102
010700                                                                  PPEF102
010800     EJECT                                                        PPEF102
010900 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEF102
011000                                                                  PPEF102
011100     EJECT                                                        PPEF102
011200******************************************************************PPEF102
011300**  DATA BASE AREAS                                             **PPEF102
011400******************************************************************PPEF102
011500                                                                  PPEF102
011600 01  BEN-ROW                             EXTERNAL.                PPEF102
011700     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPEF102
011800                                                                  PPEF102
011900                                                                  PPEF102
012000     EJECT                                                        PPEF102
012100******************************************************************PPEF102
012200**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEF102
012300******************************************************************PPEF102
012400                                                                  PPEF102
012500*----------------------------------------------------------------*PPEF102
012600 LINKAGE SECTION.                                                 PPEF102
012700*----------------------------------------------------------------*PPEF102
012800                                                                  PPEF102
012900******************************************************************PPEF102
013000**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEF102
013100**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEF102
013200******************************************************************PPEF102
013300                                                                  PPEF102
013400 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEF102
013500                                                                  PPEF102
013600     EJECT                                                        PPEF102
013700******************************************************************PPEF102
013800**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEF102
013900**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEF102
014000******************************************************************PPEF102
014100                                                                  PPEF102
014200 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEF102
014300                                                                  PPEF102
014400     EJECT                                                        PPEF102
014500******************************************************************PPEF102
014600**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEF102
014700******************************************************************PPEF102
014800                                                                  PPEF102
014900 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEF102
015000                          KMTA-MESSAGE-TABLE-ARRAY.               PPEF102
015100                                                                  PPEF102
015200                                                                  PPEF102
015300*----------------------------------------------------------------*PPEF102
015400 0000-DRIVER.                                                     PPEF102
015500*----------------------------------------------------------------*PPEF102
015600                                                                  PPEF102
015700******************************************************************PPEF102
015800**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEF102
015900******************************************************************PPEF102
016000                                                                  PPEF102
016100     IF  THIS-IS-THE-FIRST-TIME                                   PPEF102
016200         PERFORM 0100-INITIALIZE                                  PPEF102
016300     END-IF.                                                      PPEF102
016400                                                                  PPEF102
016500******************************************************************PPEF102
016600**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEF102
016700**  TO THE CALLING PROGRAM                                      **PPEF102
016800******************************************************************PPEF102
016900                                                                  PPEF102
017000     PERFORM 1000-MAINLINE-ROUTINE.                               PPEF102
017100                                                                  PPEF102
017200     EXIT PROGRAM.                                                PPEF102
017300                                                                  PPEF102
017400                                                                  PPEF102
017500     EJECT                                                        PPEF102
017600*----------------------------------------------------------------*PPEF102
017700 0100-INITIALIZE    SECTION.                                      PPEF102
017800*----------------------------------------------------------------*PPEF102
017900                                                                  PPEF102
018000******************************************************************PPEF102
018100**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEF102
018200******************************************************************PPEF102
018300                                                                  PPEF102
018400     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEF102
018500                                                                  PPEF102
018600*                                               *-------------*   PPEF102
018700                                                 COPY CPPDXWHC.   PPEF102
018800*                                               *-------------*   PPEF102
018900                                                                  PPEF102
019000*****                                           *-------------*   28521025
019100*****                                            COPY CPPDDATE.   28521025
019200*****                                           *-------------*   28521025
019300*****                                                             28521025
019400*****MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 28521025
019300                                                                  PPEF102
019600******************************************************************PPEF102
019700**   SET FIRST CALL SWITCH OFF                                  **PPEF102
019800******************************************************************PPEF102
019900                                                                  PPEF102
020000     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEF102
020100                                                                  PPEF102
020200     EJECT                                                        PPEF102
020300*----------------------------------------------------------------*PPEF102
020400 1000-MAINLINE-ROUTINE    SECTION.                                PPEF102
020500*----------------------------------------------------------------*PPEF102
020600                                                                  PPEF102
020700**************************************************                PPEF102
020800*    CHANGE RETIREMENT PLAN CODE BACK TO ORIGINAL*                PPEF102
020900*         ON PERSONEL DATA SEGMENT (100)         *                PPEF102
021000**************************************************                PPEF102
021100                                                                  PPEF102
021200     IF  RET-ELIG-CODE NUMERIC                                    PPEF102
021300         SET XRET-INDX            TO 1                            PPEF102
021400         SEARCH XRET-PLAN-TABLE                                   PPEF102
021500             WHEN RET-ELIG-CODE =                                 PPEF102
021600                         XRET-NO-DEDUCT-PLAN (XRET-INDX)          PPEF102
021700             MOVE XRET-PLAN (XRET-INDX)                           PPEF102
021800                                  TO RET-ELIG-CODE                PPEF102
021900         END-SEARCH                                               PPEF102
022000         MOVE E0122               TO DL-FIELD                     PPEF102
022100         PERFORM 9050-AUDITING-RESPONSIBILITIES                   PPEF102
022200     END-IF.                                                      PPEF102
022300                                                                  PPEF102
022400     EJECT                                                        PPEF102
022500*----------------------------------------------------------------*PPEF102
022600 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEF102
022700*----------------------------------------------------------------*PPEF102
022800                                                                  PPEF102
022900*                                                 *-------------* PPEF102
023000                                                   COPY CPPDXDEC. PPEF102
023100*                                                 *-------------* PPEF102
023200                                                                  PPEF102
023300*----------------------------------------------------------------*PPEF102
023400 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEF102
023500*----------------------------------------------------------------*PPEF102
023600                                                                  PPEF102
023700*                                                 *-------------* PPEF102
023800                                                   COPY CPPDADLC. PPEF102
023900*                                                 *-------------* PPEF102
024000                                                                  PPEF102
024100*****EJECT                                                        28521025
024200*****------------------------------------------------------------*28521025
024300*9300-DATE-CONVERSION-DB2  SECTION.                               28521025
024400*****------------------------------------------------------------*28521025
024500*****                                                             28521025
024600*****                                             *-------------* 28521025
024700*****                                              COPY CPPDXDC2. 28521025
024800*****                                             *-------------* 28521025
024900*****                                                             28521025
025000******************************************************************PPEF102
025100**   E N D  S O U R C E   ----  PPEF102 ----                    **PPEF102
025200******************************************************************PPEF102
