000100 IDENTIFICATION DIVISION.                                         PPL751
000200 PROGRAM-ID.  PPL751 .                                            PPL751
000300 ENVIRONMENT DIVISION.                                            PPL751
000400 CONFIGURATION SECTION.                                           PPL751
000500 SOURCE-COMPUTER.  COPY  CPOTXUCS .                               PPL751
000600 OBJECT-COMPUTER.  COPY  CPOTXOBJ .                               PPL751
000700/                                                                 PPL751
000800 INPUT-OUTPUT SECTION.                                            PPL751
000900 FILE-CONTROL.                                                    PPL751
001000*                                                                 PPL751
001100     SELECT MULTIIN          ASSIGN TO UT-S-MULTIIN.              PPL751
001200*                                                                 PPL751
001300     SELECT MULTIOUT         ASSIGN TO UT-S-MULTIOUT.             PPL751
001400/                                                                 PPL751
001500 DATA DIVISION.                                                   PPL751
001600 FILE SECTION.                                                    PPL751
001700     SKIP2                                                        PPL751
001800 FD  MULTIIN                                                      PPL751
001900     RECORD CONTAINS   0 CHARACTERS                               PPL751
002000     BLOCK  CONTAINS   0 RECORDS                                  PPL751
002100     RECORDING MODE IS F                                          PPL751
002200     LABEL RECORDS ARE STANDARD                                   PPL751
002300     DATA RECORD    IS MULTIINREC.                                PPL751
002400     SKIP1                                                        PPL751
002500 01  MULTIINREC.                                                  PPL751
002600     05  MULTIDATA     PIC X(558) .                               PPL751
002700     SKIP2                                                        PPL751
002800 FD  MULTIOUT                                                     PPL751
002900     RECORD CONTAINS 558 CHARACTERS                               PPL751
003000     BLOCK  CONTAINS   0 RECORDS                                  PPL751
003100     RECORDING MODE IS F                                          PPL751
003200     LABEL RECORDS ARE STANDARD                                   PPL751
003300     DATA RECORD    IS MULTIOUTREC.                               PPL751
003400     SKIP1                                                        PPL751
003500 01  MULTIOUTREC   PICTURE  X(558) .                              PPL751
003600/                                                                 PPL751
003700 WORKING-STORAGE SECTION.                                         PPL751
003800     SKIP1                                                        PPL751
003900 77  MULTI-RECS-INPUT      PIC 9(6)  VALUE  ZEROES .              PPL751
004000 77  MULTI-RECS-OUTPUT     PIC 9(6)  VALUE  ZEROES .              PPL751
004100     SKIP2                                                        PPL751
004200 77  ERROR-RETURN-CODE     PIC S9(4) COMP   SYNC  VALUE  0 .      PPL751
004300     SKIP1                                                        PPL751
004400 77  SET-FLAG              PIC 9(1)  VALUE  1 .                   PPL751
004500 77  RESET-FLAG            PIC 9(1)  VALUE  0 .                   PPL751
004600     SKIP1                                                        PPL751
004700 77  INPUT-FINISHED-FLAG   PIC 9(1)  VALUE  0 .                   PPL751
004800     88 INPUT-FINISHED        VALUE  1 .                          PPL751
004900 77  ERROR-CONDITION-FLAG  PIC 9(1)  VALUE  0 .                   PPL751
005000     88 ERROR-CONDITION       VALUE  1 .                          PPL751
005100     SKIP1                                                        PPL751
005200 77  I                     PIC 9(9)  VALUE  ZEROES .              PPL751
005300 77  J                     PIC 9(9)  VALUE  ZEROES .              PPL751
005400 77  K                     PIC 9(9)  VALUE  ZEROES .              PPL751
005500/                                                                 PPL751
005600 01  MULTI-OUTPUT-REC .                                           PPL751
005700     05  REC-FIELDS.                                              PPL751
005800         10  MULTI-FIELD1    PIC 9(5)V9(4) .                      PPL751
005900         10  MULTI-SPACER1   PIC X(1) .                           PPL751
006000         10  MULTI-FIELD2    PIC 9(5)V9(4) .                      PPL751
006100         10  MULTI-SPACER2   PIC X(1) .                           PPL751
006200         10  MULTI-FIELD3    PIC 9(10)V9(4) .                     PPL751
006300         10  MULTI-SPACER3   PIC X(1) .                           PPL751
006400         10  MULTI-FIELD4    PIC 9(10)V9(2) .                     PPL751
006500         10  MULTI-SPACER4   PIC X(1) .                           PPL751
006600         10  MULTI-FIELD5    PIC 9(10) .                          PPL751
006700     05  REC-OTHER-INFO  PIC X(500) .                             PPL751
006800/                                                                 PPL751
006900 01  WORK-INFO .                                                  PPL751
007000     05 W-REC-KEY-CURRENT  PIC X(30) .                            PPL751
007100     05 W-REC-KEY-COUNT    PIC 9(6) .                             PPL751
007200/                                                                 PPL751
007300 PROCEDURE DIVISION.                                              PPL751
007400     SKIP1                                                        PPL751
007500 MAIN SECTION.                                                    PPL751
007600     SKIP1                                                        PPL751
007700 MAIN-START.                                                      PPL751
007800     MOVE LOW-VALUES  TO  W-REC-KEY-CURRENT .                     PPL751
007900     MOVE 0           TO  W-REC-KEY-COUNT .                       PPL751
008000     MOVE RESET-FLAG TO ERROR-CONDITION-FLAG .                    PPL751
008100     PERFORM PROCESS-MULTI-INPUT .                                PPL751
008200     PERFORM DISPLAY-STATISTICS .                                 PPL751
008300     STOP RUN .                                                   PPL751
008400     SKIP3                                                        PPL751
008500 DISPLAY-STATISTICS.                                              PPL751
008600     DISPLAY ' --- PROCESSING STATISTICS ----------------- ' .    PPL751
008700     DISPLAY ' MULTIPLIC INPT READ  ... ', MULTI-RECS-INPUT .     PPL751
008800     DISPLAY ' MULTIPLIC OUTP WRITTEN . ', MULTI-RECS-OUTPUT      PPL751
008900     DISPLAY ' --- END PROCESSING STATISTICS ------------- ' .    PPL751
009000     SKIP3                                                        PPL751
009100 ERROR-ABORT-PROGRAM.                                             PPL751
009200     MOVE ERROR-RETURN-CODE  TO  RETURN-CODE .                    PPL751
009300     COMPUTE ERROR-RETURN-CODE  =                                 PPL751
009400             ERROR-RETURN-CODE / 0 .                              PPL751
009500     STOP RUN .                                                   PPL751
009600/                                                                 PPL751
009700 PROCESS-MULTI-INPUT.                                             PPL751
009800     OPEN INPUT  MULTIIN .                                        PPL751
009900     OPEN OUTPUT MULTIOUT .                                       PPL751
010000     MOVE RESET-FLAG  TO  INPUT-FINISHED-FLAG .                   PPL751
010100     PERFORM PROCESS-MULTI-RECORD THRU                            PPL751
010200             PROCESS-MULTI-RECORD-EXIT .                          PPL751
010300     CLOSE MULTIIN .                                              PPL751
010400     CLOSE MULTIOUT .                                             PPL751
010500     SKIP2                                                        PPL751
010600 PROCESS-MULTI-RECORD.                                            PPL751
010700     READ MULTIIN   AT END                                        PPL751
010800          MOVE SET-FLAG  TO  INPUT-FINISHED-FLAG                  PPL751
010900          GO TO  PROCESS-MULTI-RECORD-EXIT .                      PPL751
011000     ADD  1  TO  MULTI-RECS-INPUT .                               PPL751
011100     MOVE MULTIINREC  TO  MULTI-OUTPUT-REC .                      PPL751
011200     COMPUTE MULTI-FIELD3  ROUNDED  =                             PPL751
011300             MULTI-FIELD1 * MULTI-FIELD2 .                        PPL751
011400     COMPUTE MULTI-FIELD4  ROUNDED  =  MULTI-FIELD3 .             PPL751
011500     COMPUTE MULTI-FIELD5  ROUNDED  =  MULTI-FIELD3 .             PPL751
011600     PERFORM OUTPUT-MULTI-REC .                                   PPL751
011700     GO TO  PROCESS-MULTI-RECORD .                                PPL751
011800     SKIP1                                                        PPL751
011900 PROCESS-MULTI-RECORD-EXIT.                                       PPL751
012000     EXIT .                                                       PPL751
012100     SKIP2                                                        PPL751
012200 OUTPUT-MULTI-REC.                                                PPL751
012300     MOVE MULTI-OUTPUT-REC TO                                     PPL751
012400          MULTIOUTREC .                                           PPL751
012500     WRITE MULTIOUTREC     INVALID KEY                            PPL751
012600          DISPLAY '*** LVL CAL WRITE ERROR '                      PPL751
012700          MOVE 102 TO ERROR-RETURN-CODE                           PPL751
012800          PERFORM ERROR-ABORT-PROGRAM .                           PPL751
012900     ADD 1 TO MULTI-RECS-OUTPUT .                                 PPL751
