000010*==========================================================%      UCSD0102
000020*=    PROGRAM: PPFAU001                                   =%      UCSD0102
000030*=    CHANGE   #UCSD0102    PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000040*=    NAME: R WOLBERG       MODIFICATION DATE: 07/13/00   =%      UCSD0102
000060*=                                                        =%      UCSD0102
000090*=    DESCRIPTION:                                        =%      UCSD0102
000091*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA.          =%      UCSD0102
000092*=                                                        =%      UCSD0102
000093*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  PROGRAM: PPFAU012                                         */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:__P. THOMPSON____ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    Initial Release of program.                             */   32021138
000700*                                                            */   32021138
000800**************************************************************/   32021138
000900*                                                             *   PPFAU012
001000*  This program is one of the Full Accounting Unit modules    *   PPFAU012
001100*  which campuses may need to modify to accomodate their own  *   PPFAU012
001200*  Chart of Accounts structure.                               *   PPFAU012
001300*                                                             *   PPFAU012
001400*  In particular, depending upon the need for validation at   *   PPFAU012
001500*  various levels, campuses may need fewer or more levels of  *   PPFAU012
001600*  validation failure.                                        *   PPFAU012
001700*                                                             *   PPFAU012
002060*  The Base version calls PPBUTUT2 to derive the DUC.         *   PPFAU012
002100***************************************************************   PPFAU012
002200 IDENTIFICATION DIVISION.                                         PPFAU012
002300 PROGRAM-ID. PPFAU012.                                            PPFAU012
002400 AUTHOR. Phillip Thompson.                                        PPFAU012
002500 INSTALLATION. University of California, Base Payroll System.     PPFAU012
002600 DATE-WRITTEN. 29 January 1997.                                   PPFAU012
002700 DATE-COMPILED.                                                   PPFAU012
002800     SKIP2                                                        PPFAU012
002900******************************************************************PPFAU012
003000**                                                              **PPFAU012
003100** PPFAU012 - FAU DUC Derivation routine.                       **PPFAU012
003200**                                                              **PPFAU012
003300** This program accepts a Full Accounting Unit (FAU) and DUC    **PPFAU012
003400** as input.                                                    **PPFAU012
003500**                                                              **PPFAU012
003600** The base version of this program extracts two fields from    **PPFAU012
003700** the FAU and passes them and the DUC to PPBUTUT2. It passes   **PPFAU012
003800** the derived DUC back to the calling program.                 **PPFAU012
004600**                                                              **PPFAU012
004700******************************************************************PPFAU012
004800     SKIP2                                                        PPFAU012
004900 ENVIRONMENT DIVISION.                                            PPFAU012
005000 CONFIGURATION SECTION.                                           PPFAU012
005100     SKIP2                                                        PPFAU012
005200 SOURCE-COMPUTER.            COPY CPOTXUCS.                       PPFAU012
005300 OBJECT-COMPUTER.            COPY CPOTXOBJ.                       PPFAU012
005400     EJECT                                                        PPFAU012
005500 DATA DIVISION.                                                   PPFAU012
005600 WORKING-STORAGE SECTION.                                         PPFAU012
005700******************************************************************PPFAU012
005800*                Working Storage                                 *PPFAU012
005900******************************************************************PPFAU012
006000*                                                                 PPFAU012
006100 01  WB-PROGRAM-CONSTANTS.                                        PPFAU012
006200     05  A-STND-PROG-ID               PIC X(15) VALUE             PPFAU012
006300                                      'PPFAU012/080497'.          PPFAU012
006400     05  PGM-PPBUTUT2                 PIC X(08) VALUE 'PPBUTUT2'. PPFAU012
006500     SKIP3                                                        PPFAU012
006600 01  WF-PROGRAM-FLAGS.                                            PPFAU012
006700     05  FIRST-TIME-SW                PIC X(1) VALUE 'Y'.         PPFAU012
006800         88  THIS-IS-THE-FIRST-TIME            VALUE 'Y'.         PPFAU012
006900         88  NOT-THE-FIRST-TIME                VALUE 'N'.         PPFAU012
007010     EJECT                                                        PPFAU012
007100******************************************************************PPFAU012
007200*                Copylib Members                                 *PPFAU012
007300******************************************************************PPFAU012
007400 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPFAU012
007500     SKIP3                                                        PPFAU012
007600 01  FAUR-FAU-REDEFINITION.              COPY CPWSFAUR.           PPFAU012
007700     SKIP3                                                        PPFAU012
007820 01  XUTF-UTILITY-FUNCTIONS.             COPY 'CPWSXUTF'.         PPFAU012
007830     SKIP3                                                        PPFAU012
007840 01  PPBUTUTL-INTERFACE.                 COPY 'CPWSBUTI'.         PPFAU012
007900     EJECT                                                        PPFAU012
008000 LINKAGE SECTION.                                                 PPFAU012
008100******************************************************************PPFAU012
008200*                Program Linkage                                 *PPFAU012
008300******************************************************************PPFAU012
008400 01  PPFAU012-INTERFACE.               COPY CPLNF012.             PPFAU012
008500     EJECT                                                        PPFAU012
008600 PROCEDURE DIVISION USING PPFAU012-INTERFACE.                     PPFAU012
008700******************************************************************PPFAU012
008800*                Procedure Division                              *PPFAU012
008900******************************************************************PPFAU012
009000 00000-MAINLINE SECTION.                                          PPFAU012
009100 00000-ENTRY.                                                     PPFAU012
009200*                                                                 PPFAU012
009300     IF THIS-IS-THE-FIRST-TIME                                    PPFAU012
009400         PERFORM 10000-PROGRAM-INITIALIZATION                     PPFAU012
009500     END-IF.                                                      PPFAU012
009600*                                                                 PPFAU012
009700     PERFORM 20000-DERIVE-DUC.                                    PPFAU012
009800*                                                                 PPFAU012
010000     EXIT PROGRAM.                                                PPFAU012
010100     EJECT                                                        PPFAU012
010200******************************************************************PPFAU012
010300*                Program Initialization                          *PPFAU012
010400******************************************************************PPFAU012
010500 10000-PROGRAM-INITIALIZATION SECTION.                            PPFAU012
010600 10000-ENTRY.                                                     PPFAU012
010700*                                                                 PPFAU012
010800     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPFAU012
010900     COPY CPPDXWHC.                                               PPFAU012
011000     SET NOT-THE-FIRST-TIME TO TRUE.                              PPFAU012
011100     SKIP3                                                        PPFAU012
011200 19999-EXIT.                                                      PPFAU012
011300     EXIT.                                                        PPFAU012
011400     EJECT                                                        PPFAU012
011500******************************************************************PPFAU012
011600*                DERIVE THE DUC                                  *PPFAU012
011700******************************************************************PPFAU012
011800 20000-DERIVE-DUC SECTION.                                        PPFAU012
011900 20000-ENTRY.                                                     PPFAU012
012000*                                                                 PPFAU012
012100     MOVE SPACES TO F012-OUTPUTS.                                 PPFAU012
012200*                                                                 PPFAU012
012210     MOVE PPBUTUTL-DERIVE-DUC TO BUTI-IN-ACTION.                  PPFAU012
012212     MOVE F012-IN-DUC TO BUTI-IN-DUC.                             PPFAU012
012300     MOVE F012-FAU TO FAUR-FAU.                                   PPFAU012
012370*    MOVE FAUR-ACCOUNT TO BUTI-IN-ACCOUNT.                        UCSD0102
012371     MOVE FAUR-FUND TO BUTI-IN-FUND.                              PPFAU012
012372     MOVE F012-IN-BUC TO BUTI-IN-BUC.                             PPFAU012
012373*                                                                 PPFAU012
012374*    CALL PGM-PPBUTUT2 USING PPBUTUTL-INTERFACE.                  UCSD0102
012375*                                                                 PPFAU012
012376*    MOVE BUTI-OUT-DUC TO F012-OUT-DUC.                           UCSD0102
012377     MOVE SPACES       TO F012-OUT-DUC.                           UCSD0102
014400     SKIP3                                                        PPFAU012
014500 29999-EXIT.                                                      PPFAU012
014600     EXIT.                                                        PPFAU012
014700******************************************************************PPFAU012
014800* End of source code                                             *PPFAU012
014900******************************************************************PPFAU012
