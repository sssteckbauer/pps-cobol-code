000100**************************************************************/   30871460
000200*  PROGRAM: PPCTCALU                                         */   30871460
000300*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000400*  NAME:___SRS___________ CREATION DATE:      ___MM/MM/YY__  */   30871460
000500*  DESCRIPTION:                                              */   30871460
000600*  CALENDAR TABLE UPDATE MODULE.                             */   30871460
000700**************************************************************/   30871460
000800 IDENTIFICATION DIVISION.                                         PPCTCALU
000900 PROGRAM-ID. PPCTCALU.                                            PPCTCALU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTCALU
001100 DATE-COMPILED.                                                   PPCTCALU
001200 ENVIRONMENT DIVISION.                                            PPCTCALU
001300 DATA DIVISION.                                                   PPCTCALU
001400 WORKING-STORAGE SECTION.                                         PPCTCALU
001410                                                                  PPCTCALU
001420 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTCALU
001500 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTCALU
001501                                                                  PPCTCALU
001510     EXEC SQL                                                     PPCTCALU
001520        INCLUDE SQLCA                                             PPCTCALU
001530     END-EXEC.                                                    PPCTCALU
001540                                                                  PPCTCALU
001541 LINKAGE SECTION.                                                 PPCTCALU
001550                                                                  PPCTCALU
001600 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTCALU
001700 01  CAL-TABLE-DATA.                                              PPCTCALU
001900     EXEC SQL                                                     PPCTCALU
002000        INCLUDE PPPVZCAL                                          PPCTCALU
002100     END-EXEC.                                                    PPCTCALU
002200 01  CAD-TABLE-DATA.                                              PPCTCALU
002300     EXEC SQL                                                     PPCTCALU
002400        INCLUDE PPPVZCAD                                          PPCTCALU
002410     END-EXEC.                                                    PPCTCALU
002500                                                                  PPCTCALU
002600 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE            PPCTCALU
002610                          CAL-TABLE-DATA                          PPCTCALU
002620                          CAD-TABLE-DATA.                         PPCTCALU
002700                                                                  PPCTCALU
002800 0000-MAIN.                                                       PPCTCALU
002900                                                                  PPCTCALU
003000     EXEC SQL                                                     PPCTCALU
003100        INCLUDE CPPDXE99                                          PPCTCALU
003200     END-EXEC.                                                    PPCTCALU
003201     MOVE 'PPCTCALU' TO XWHC-DISPLAY-MODULE.                      PPCTCALU
003202     COPY CPPDXWHC.                                               PPCTCALU
003210     MOVE SPACE TO KCTU-STATUS.                                   PPCTCALU
003300     IF KCTU-ACTION-ADD                                           PPCTCALU
003310       IF CAD-DAY = ZERO                                          PPCTCALU
003400          PERFORM 2000-INSERT-CAL                                 PPCTCALU
003401       ELSE                                                       PPCTCALU
003410          PERFORM 2300-INSERT-CAD                                 PPCTCALU
003420       END-IF                                                     PPCTCALU
003500     ELSE                                                         PPCTCALU
003600       IF KCTU-ACTION-CHANGE                                      PPCTCALU
003610         IF CAD-DAY = ZERO                                        PPCTCALU
003700            PERFORM 2100-UPDATE-CAL                               PPCTCALU
003701         ELSE                                                     PPCTCALU
003710            PERFORM 2400-UPDATE-CAD                               PPCTCALU
003720         END-IF                                                   PPCTCALU
003800       ELSE                                                       PPCTCALU
003900         IF KCTU-ACTION-DELETE                                    PPCTCALU
004000            PERFORM 2200-DELETE-CAL                               PPCTCALU
004100         ELSE                                                     PPCTCALU
004200            SET KCTU-INVALID-ACTION TO TRUE                       PPCTCALU
004300         END-IF                                                   PPCTCALU
004400       END-IF                                                     PPCTCALU
004500     END-IF.                                                      PPCTCALU
004600                                                                  PPCTCALU
004700 0100-END.                                                        PPCTCALU
004800                                                                  PPCTCALU
004900     GOBACK.                                                      PPCTCALU
004910                                                                  PPCTCALU
005005 2000-INSERT-CAL.                                                 PPCTCALU
005006                                                                  PPCTCALU
005007     MOVE '2000-INSRT-CAL' TO DB2MSG-TAG.                         PPCTCALU
005008     EXEC SQL                                                     PPCTCALU
005009         INSERT INTO PPPVZCAL_CAL                                 PPCTCALU
005010              VALUES (:CAL-YEAR                                   PPCTCALU
005011                     ,:CAL-MONTH                                  PPCTCALU
005012                     ,:CAL-FISCAL-DAYS                            PPCTCALU
005013                     ,:CAL-DAYS-IN-MONTH                          PPCTCALU
005014                     ,:CAL-FIRST-DAY-NAME                         PPCTCALU
005015                     ,:CAL-END-DATE                               PPCTCALU
005016                     ,:CAL-END-DATE-TYPE                          PPCTCALU
005017                     ,:CAL-FISCAL-END-DT                          PPCTCALU
005018                     ,:CAL-LAST-ACTION                            PPCTCALU
005019                     ,:CAL-LAST-ACTION-DT                         PPCTCALU
005020                     )                                            PPCTCALU
005021     END-EXEC.                                                    PPCTCALU
005022     IF SQLCODE NOT = ZERO                                        PPCTCALU
005023        SET KCTU-INSERT-ERROR TO TRUE                             PPCTCALU
005024     END-IF.                                                      PPCTCALU
005025                                                                  PPCTCALU
005026 2100-UPDATE-CAL.                                                 PPCTCALU
005027                                                                  PPCTCALU
005028     MOVE '2100-UPD-CAL ' TO DB2MSG-TAG.                          PPCTCALU
005029     EXEC SQL                                                     PPCTCALU
005030         UPDATE PPPVZCAL_CAL                                      PPCTCALU
005031            SET CAL_YEAR              = :CAL-YEAR                 PPCTCALU
005032               ,CAL_MONTH             = :CAL-MONTH                PPCTCALU
005033               ,CAL_FISCAL_DAYS       = :CAL-FISCAL-DAYS          PPCTCALU
005034               ,CAL_DAYS_IN_MONTH     = :CAL-DAYS-IN-MONTH        PPCTCALU
005035               ,CAL_FIRST_DAY_NAME    = :CAL-FIRST-DAY-NAME       PPCTCALU
005036               ,CAL_END_DATE          = :CAL-END-DATE             PPCTCALU
005037               ,CAL_END_DATE_TYPE     = :CAL-END-DATE-TYPE        PPCTCALU
005038               ,CAL_FISCAL_END_DT     = :CAL-FISCAL-END-DT        PPCTCALU
005039               ,CAL_LAST_ACTION       = :CAL-LAST-ACTION          PPCTCALU
005040               ,CAL_LAST_ACTION_DT    = :CAL-LAST-ACTION-DT       PPCTCALU
005041          WHERE CAL_YEAR              = :CAL-YEAR                 PPCTCALU
005042            AND CAL_MONTH             = :CAL-MONTH                PPCTCALU
005043     END-EXEC.                                                    PPCTCALU
005044     IF SQLCODE NOT = ZERO                                        PPCTCALU
005045        SET KCTU-UPDATE-ERROR TO TRUE                             PPCTCALU
005046     END-IF.                                                      PPCTCALU
005048                                                                  PPCTCALU
005049 2200-DELETE-CAL.                                                 PPCTCALU
005050                                                                  PPCTCALU
005051     MOVE '2200-DLTE-CAL ' TO DB2MSG-TAG.                         PPCTCALU
005052     EXEC SQL                                                     PPCTCALU
005053         DELETE FROM PPPVZCAL_CAL                                 PPCTCALU
005054          WHERE CAL_YEAR              = :CAL-YEAR                 PPCTCALU
005055            AND CAL_MONTH             = :CAL-MONTH                PPCTCALU
005056     END-EXEC.                                                    PPCTCALU
005057     IF SQLCODE NOT = ZERO                                        PPCTCALU
005058        SET KCTU-DELETE-ERROR TO TRUE                             PPCTCALU
005059     END-IF.                                                      PPCTCALU
005060                                                                  PPCTCALU
011900 2300-INSERT-CAD.                                                 PPCTCALU
012000                                                                  PPCTCALU
012100     MOVE '2300-INSRT-CAD' TO DB2MSG-TAG.                         PPCTCALU
012300     EXEC SQL                                                     PPCTCALU
012400         INSERT INTO PPPVZCAD_CAD                                 PPCTCALU
012500              VALUES (:CAD-YEAR                                   PPCTCALU
012600                     ,:CAD-MONTH                                  PPCTCALU
012700                     ,:CAD-DAY                                    PPCTCALU
012800                     ,:CAD-DAY-TYPE                               PPCTCALU
012901                     ,:CAD-B1-PAY-END                             PPCTCALU
012902                     ,:CAD-B1-PAY-DAY                             PPCTCALU
012910                     ,:CAD-B2-PAY-END                             PPCTCALU
012920                     ,:CAD-B2-PAY-DAY                             PPCTCALU
012930                     ,:CAD-SM-PAY-END                             PPCTCALU
012940                     ,:CAD-SM-PAY-DAY                             PPCTCALU
012950                     ,:CAD-MO-PAY-END                             PPCTCALU
012960                     ,:CAD-MO-PAY-DAY                             PPCTCALU
012970                     ,:CAD-MA-PAY-END                             PPCTCALU
012980                     ,:CAD-MA-PAY-DAY                             PPCTCALU
013000                     ,:CAD-LAST-ACTION                            PPCTCALU
013100                     ,:CAD-LAST-ACTION-DT                         PPCTCALU
018100                     )                                            PPCTCALU
018200     END-EXEC.                                                    PPCTCALU
018300     IF SQLCODE NOT = ZERO                                        PPCTCALU
018400        SET KCTU-INSERT-ERROR TO TRUE                             PPCTCALU
018500     END-IF.                                                      PPCTCALU
018600                                                                  PPCTCALU
018700 2400-UPDATE-CAD.                                                 PPCTCALU
018800                                                                  PPCTCALU
018900     MOVE '2400-UPD-CAD ' TO DB2MSG-TAG.                          PPCTCALU
019000     EXEC SQL                                                     PPCTCALU
019100         UPDATE PPPVZCAD_CAD                                      PPCTCALU
019200            SET CAD_YEAR              = :CAD-YEAR                 PPCTCALU
019300               ,CAD_MONTH             = :CAD-MONTH                PPCTCALU
019400               ,CAD_DAY               = :CAD-DAY                  PPCTCALU
019500               ,CAD_DAY_TYPE          = :CAD-DAY-TYPE             PPCTCALU
019600               ,CAD_B1_PAY_END        = :CAD-B1-PAY-END           PPCTCALU
019700               ,CAD_B1_PAY_DAY        = :CAD-B1-PAY-DAY           PPCTCALU
019800               ,CAD_B2_PAY_END        = :CAD-B2-PAY-END           PPCTCALU
019900               ,CAD_B2_PAY_DAY        = :CAD-B2-PAY-DAY           PPCTCALU
020000               ,CAD_SM_PAY_END        = :CAD-SM-PAY-END           PPCTCALU
020100               ,CAD_SM_PAY_DAY        = :CAD-SM-PAY-DAY           PPCTCALU
020200               ,CAD_MO_PAY_END        = :CAD-MO-PAY-END           PPCTCALU
020300               ,CAD_MO_PAY_DAY        = :CAD-MO-PAY-DAY           PPCTCALU
020400               ,CAD_MA_PAY_END        = :CAD-MA-PAY-END           PPCTCALU
020500               ,CAD_MA_PAY_DAY        = :CAD-MA-PAY-DAY           PPCTCALU
020600               ,CAD_LAST_ACTION       = :CAD-LAST-ACTION          PPCTCALU
020700               ,CAD_LAST_ACTION_DT    = :CAD-LAST-ACTION-DT       PPCTCALU
020800          WHERE CAD_YEAR              = :CAD-YEAR                 PPCTCALU
020900            AND CAD_MONTH             = :CAD-MONTH                PPCTCALU
021000            AND CAD_DAY               = :CAD-DAY                  PPCTCALU
021100     END-EXEC.                                                    PPCTCALU
021200     IF SQLCODE NOT = ZERO                                        PPCTCALU
021300        SET KCTU-UPDATE-ERROR TO TRUE                             PPCTCALU
021400     END-IF.                                                      PPCTCALU
021500                                                                  PPCTCALU
025700*999999-SQL-ERROR.                                                PPCTCALU
025800     EXEC SQL                                                     PPCTCALU
025900        INCLUDE CPPDXP99                                          PPCTCALU
026000     END-EXEC.                                                    PPCTCALU
026010     EVALUATE TRUE                                                PPCTCALU
026020       WHEN (KCTU-ACTION-ADD)                                     PPCTCALU
026030         SET KCTU-INSERT-ERROR TO TRUE                            PPCTCALU
026040       WHEN (KCTU-ACTION-CHANGE)                                  PPCTCALU
026050         SET KCTU-UPDATE-ERROR TO TRUE                            PPCTCALU
026060       WHEN (KCTU-ACTION-DELETE)                                  PPCTCALU
026070         SET KCTU-DELETE-ERROR TO TRUE                            PPCTCALU
026080     END-EVALUATE.                                                PPCTCALU
026100     GO TO 0100-END.                                              PPCTCALU
