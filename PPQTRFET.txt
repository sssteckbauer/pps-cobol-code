000000**************************************************************/   28521087
000001*  PROGRAM: PPQTRFET                                         */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/22/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  ** DATE CONVERSION II **                                  */   28521087
000006*  - REPLACE CPWSXDC2 AND CPPDXDC2 WITH CPWSXDC3 AND CPPDXDC3*/   28521087
000007*  - COMMENTED PARAGRAPH 999999-EXIT. CAN NEVER BE EXECUTED  */   28521087
000008**************************************************************/   28521087
000100**************************************************************/   36330704
000200*  PROGRAM: PPQTRFET                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPQTR SELECT MODULE                                */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPQTRFET
001000 PROGRAM-ID. PPQTRFET.                                            PPQTRFET
001100 ENVIRONMENT DIVISION.                                            PPQTRFET
001200 DATA DIVISION.                                                   PPQTRFET
001300 WORKING-STORAGE SECTION.                                         PPQTRFET
001400*                                                                 PPQTRFET
001500     COPY CPWSXBEG.                                               PPQTRFET
001600*                                                                 PPQTRFET
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPQTRFET'.       PPQTRFET
001800*                                                                 PPQTRFET
001900     EXEC SQL                                                     PPQTRFET
002000         INCLUDE CPWSWRKA                                         PPQTRFET
002100     END-EXEC.                                                    PPQTRFET
002200*                                                                 PPQTRFET
002300*01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC2.                28521087
002310 01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC3.                28521087
002400*                                                                 PPQTRFET
002500 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPQTRFET
002600*                                                                 PPQTRFET
002700 01  PPPQTR-ROW EXTERNAL.                                         PPQTRFET
002800     EXEC SQL                                                     PPQTRFET
002900         INCLUDE PPPVQTR1                                         PPQTRFET
003000     END-EXEC.                                                    PPQTRFET
003100*                                                                 PPQTRFET
003200*------------------------------------------------------------*    PPQTRFET
003300*  SQL DCLGEN AREA                                           *    PPQTRFET
003400*------------------------------------------------------------*    PPQTRFET
003500     EXEC SQL                                                     PPQTRFET
003600         INCLUDE SQLCA                                            PPQTRFET
003700     END-EXEC.                                                    PPQTRFET
003800*                                                                 PPQTRFET
003900 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPQTRFET
004000*                                                                 PPQTRFET
004100     COPY CPWSXEND.                                               PPQTRFET
004200*                                                                 PPQTRFET
004300 LINKAGE SECTION.                                                 PPQTRFET
004400*                                                                 PPQTRFET
004500 01  SELECT-INTERFACE-AREA.                                       PPQTRFET
004600     COPY CPWSXHIF REPLACING ==:TAG:== BY ==XSEL==.               PPQTRFET
004700*                                                                 PPQTRFET
004800 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPQTRFET
004900*                                                                 PPQTRFET
005000     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPQTRFET
005100     COPY  CPPDXWHC.                                              PPQTRFET
005200*                                                                 PPQTRFET
005300     EXEC SQL                                                     PPQTRFET
005400         INCLUDE CPPDXE99                                         PPQTRFET
005500     END-EXEC.                                                    PPQTRFET
005600*                                                                 PPQTRFET
005700     COPY CPPDSELM.                                               PPQTRFET
005800*                                                                 PPQTRFET
005900 2000-SELECT SECTION.                                             PPQTRFET
006000*                                                                 PPQTRFET
006100*                                                                 PPQTRFET
006200     MOVE 'SELECT MAX TIMESTAMP' TO DB2MSG-TAG.                   PPQTRFET
006300     STRING WS-QUERY-DATE '-' WS-QUERY-TIME                       PPQTRFET
006400       DELIMITED BY SIZE INTO WS-QUERY-TIMESTAMP.                 PPQTRFET
006500*                                                                 PPQTRFET
006600     EXEC SQL                                                     PPQTRFET
006700         SELECT VALUE(MAX(TIMESTAMP                               PPQTRFET
006800               (SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME)),            PPQTRFET
006900                TIMESTAMP('0001-01-01-00.00.00'))                 PPQTRFET
007000         INTO :WS-MAX-TIMESTAMP                                   PPQTRFET
007100         FROM PPPVQTR1_QTR                                        PPQTRFET
007200         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPQTRFET
007300         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPQTRFET
007400         AND TIMESTAMP(SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME) ^>    PPQTRFET
007500             :WS-QUERY-TIMESTAMP                                  PPQTRFET
007600     END-EXEC.                                                    PPQTRFET
007700*                                                                 PPQTRFET
007800     IF SQLCODE = +100                                            PPQTRFET
007900     OR WS-MAX-TIMESTAMP = '0001-01-01-00.00.00'                  PPQTRFET
008000        INITIALIZE XSEL-RETURN-KEY                                PPQTRFET
008100                   PPPQTR-ROW                                     PPQTRFET
008200        SET XSEL-INVALID-KEY TO TRUE                              PPQTRFET
008300        GO TO 2000-EXIT                                           PPQTRFET
008400     END-IF.                                                      PPQTRFET
008500*                                                                 PPQTRFET
008600     MOVE 'SELECT PPPQTR ROW' TO DB2MSG-TAG.                      PPQTRFET
008700*                                                                 PPQTRFET
008800     EXEC SQL                                                     PPQTRFET
008900          SELECT                                                  PPQTRFET
009000             EMPLID             ,                                 PPQTRFET
009100             ITERATION_NUMBER   ,                                 PPQTRFET
009200             CHAR(SYSTEM_ENTRY_DATE, ISO),                        PPQTRFET
009300             CHAR(SYSTEM_ENTRY_TIME, ISO),                        PPQTRFET
009400             DELETE_FLAG        ,                                 PPQTRFET
009500             ERH_INCORRECT                                        PPQTRFET
009600       INTO :EMPLID             ,                                 PPQTRFET
009700            :ITERATION-NUMBER   ,                                 PPQTRFET
009800            :SYSTEM-ENTRY-DATE  ,                                 PPQTRFET
009900            :SYSTEM-ENTRY-TIME  ,                                 PPQTRFET
010000            :DELETE-FLAG        ,                                 PPQTRFET
010100            :ERH-INCORRECT                                        PPQTRFET
010200         FROM PPPVQTR1_QTR                                        PPQTRFET
010300         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPQTRFET
010400         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPQTRFET
010500         AND TIMESTAMP(SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME) =     PPQTRFET
010600             :WS-MAX-TIMESTAMP                                    PPQTRFET
010700     END-EXEC.                                                    PPQTRFET
010800*                                                                 PPQTRFET
010900     IF SQLCODE = +100                                            PPQTRFET
011000        INITIALIZE XSEL-RETURN-KEY                                PPQTRFET
011100                   PPPQTR-ROW                                     PPQTRFET
011200        SET XSEL-INVALID-KEY TO TRUE                              PPQTRFET
011300        GO TO 2000-EXIT                                           PPQTRFET
011400     END-IF.                                                      PPQTRFET
011500*                                                                 PPQTRFET
011600     IF SQLCODE = +0                                              PPQTRFET
011700        MOVE EMPLID TO XSEL-RETURN-EMPLID                         PPQTRFET
011800        MOVE ITERATION-NUMBER TO XSEL-RETURN-ITERATION            PPQTRFET
011910********MOVE SYSTEM-ENTRY-DATE TO WSX-INQ-DATE                    28521087
012000********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
012100********MOVE DB2-DATE TO SYSTEM-ENTRY-DATE                        28521087
012200********                 XSEL-RETURN-DATE                         28521087
012210        MOVE SYSTEM-ENTRY-DATE TO XDC3-DB2-DATE                   28521087
012220        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
012230        MOVE XDC3-ISO-DATE TO SYSTEM-ENTRY-DATE                   28521087
012240                              XSEL-RETURN-DATE                    28521087
012300        MOVE SYSTEM-ENTRY-TIME TO XSEL-RETURN-TIME                PPQTRFET
012400        SET XSEL-OK TO TRUE                                       PPQTRFET
012500        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPQTRFET
012600        GO TO 2000-EXIT                                           PPQTRFET
012700     END-IF.                                                      PPQTRFET
012800*                                                                 PPQTRFET
012900 2000-EXIT.                                                       PPQTRFET
013000     EXIT.                                                        PPQTRFET
013100*                                                                 PPQTRFET
013200*****COPY CPPDXDC2.                                               28521087
013210 9000-DATE-CONVERSION-DB2 SECTION.                                28521087
013220     COPY CPPDXDC3.                                               28521087
013300*                                                                 PPQTRFET
013400*999999-SQL-ERROR SECTION.                                        PPQTRFET
013500     COPY CPPDXP99.                                               PPQTRFET
013600     SET XSEL-DB2-ERROR TO TRUE.                                  PPQTRFET
013700     MOVE SQLCODE TO XSEL-DB2-MSG-NUMBER.                         PPQTRFET
013800     GOBACK.                                                      PPQTRFET
013900*999999-EXIT.                                                     28521087
014000*****EXIT.                                                        28521087
014100*                                                                 PPQTRFET
