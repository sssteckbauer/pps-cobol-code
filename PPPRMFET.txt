000100**************************************************************/   36330704
000200*  PROGRAM: PPPRMFET                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPPRMH SELECT MODULE                               */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPPRMFET
001000 PROGRAM-ID. PPPRMFET.                                            PPPRMFET
001100 ENVIRONMENT DIVISION.                                            PPPRMFET
001200 DATA DIVISION.                                                   PPPRMFET
001300 WORKING-STORAGE SECTION.                                         PPPRMFET
001400*                                                                 PPPRMFET
001500     COPY CPWSXBEG.                                               PPPRMFET
001600*                                                                 PPPRMFET
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPPRMFET'.       PPPRMFET
001800*                                                                 PPPRMFET
001900     EXEC SQL                                                     PPPRMFET
002000         INCLUDE CPWSWRKP                                         PPPRMFET
002100     END-EXEC.                                                    PPPRMFET
002200*                                                                 PPPRMFET
002300 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPPRMFET
002400*                                                                 PPPRMFET
002500 01  PPPPRM-ROW EXTERNAL.                                         PPPRMFET
002600     EXEC SQL                                                     PPPRMFET
002700         INCLUDE PPPVZPRM                                         PPPRMFET
002800     END-EXEC.                                                    PPPRMFET
002900*                                                                 PPPRMFET
003000*------------------------------------------------------------*    PPPRMFET
003100*  SQL CURSOR DECLARE AREA                                   *    PPPRMFET
003200*------------------------------------------------------------*    PPPRMFET
003300     EXEC SQL                                                     PPPRMFET
003400         DECLARE PPPPRM-CURSOR                                    PPPRMFET
003500         CURSOR FOR                                               PPPRMFET
003600         SELECT *                                                 PPPRMFET
003700         FROM PPPVZPRM_PRM                                        PPPRMFET
003800         ORDER BY PRM_NUMBER                                      PPPRMFET
003900     END-EXEC.                                                    PPPRMFET
004000*                                                                 PPPRMFET
004100*------------------------------------------------------------*    PPPRMFET
004200*  SQL DCLGEN AREA                                           *    PPPRMFET
004300*------------------------------------------------------------*    PPPRMFET
004400     EXEC SQL                                                     PPPRMFET
004500         INCLUDE SQLCA                                            PPPRMFET
004600     END-EXEC.                                                    PPPRMFET
004700*                                                                 PPPRMFET
004800 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPPRMFET
004900*                                                                 PPPRMFET
005000     COPY CPWSXEND.                                               PPPRMFET
005100*                                                                 PPPRMFET
005200 LINKAGE SECTION.                                                 PPPRMFET
005300*                                                                 PPPRMFET
005400 01  SELECT-INTERFACE-AREA.                                       PPPRMFET
005500     COPY CPWSXPIF REPLACING ==:TAG:== BY ==XSEL==.               PPPRMFET
005600*                                                                 PPPRMFET
005700 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPPRMFET
005800*                                                                 PPPRMFET
005900     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPPRMFET
006000     COPY  CPPDXWHC.                                              PPPRMFET
006100*                                                                 PPPRMFET
006200     EXEC SQL                                                     PPPRMFET
006300         INCLUDE CPPDXE99                                         PPPRMFET
006400     END-EXEC.                                                    PPPRMFET
006500*                                                                 PPPRMFET
006600     COPY CPPDSELP.                                               PPPRMFET
006700*                                                                 PPPRMFET
006800 2000-OPEN-CURSOR SECTION.                                        PPPRMFET
006900     IF WS-CURSOR-OPEN                                            PPPRMFET
007000        SET XSEL-INVALID-REQUEST TO TRUE                          PPPRMFET
007100     ELSE                                                         PPPRMFET
007200        EXEC SQL                                                  PPPRMFET
007300            OPEN PPPPRM-CURSOR                                    PPPRMFET
007400        END-EXEC                                                  PPPRMFET
007500        SET WS-CURSOR-OPEN TO TRUE                                PPPRMFET
007600     END-IF.                                                      PPPRMFET
007700 2000-EXIT.                                                       PPPRMFET
007800     EXIT.                                                        PPPRMFET
007900*                                                                 PPPRMFET
008000 3000-FETCH-ROW SECTION.                                          PPPRMFET
008100*                                                                 PPPRMFET
008200     IF NOT WS-CURSOR-OPEN                                        PPPRMFET
008300        PERFORM 2000-OPEN-CURSOR                                  PPPRMFET
008400     END-IF.                                                      PPPRMFET
008500*                                                                 PPPRMFET
008600     IF NOT XSEL-OK                                               PPPRMFET
008700        GO TO 3000-EXIT                                           PPPRMFET
008800     END-IF.                                                      PPPRMFET
008900*                                                                 PPPRMFET
009000     EXEC SQL                                                     PPPRMFET
009100        FETCH PPPPRM-CURSOR                                       PPPRMFET
009200        INTO :PPPPRM-ROW                                          PPPRMFET
009300     END-EXEC.                                                    PPPRMFET
009400*                                                                 PPPRMFET
009500     IF SQLCODE = +100                                            PPPRMFET
009600        INITIALIZE XSEL-RETURN-KEY                                PPPRMFET
009700                   PPPPRM-ROW                                     PPPRMFET
009800        SET XSEL-INVALID-KEY TO TRUE                              PPPRMFET
009900        GO TO 3000-EXIT                                           PPPRMFET
010000     END-IF.                                                      PPPRMFET
010100*                                                                 PPPRMFET
010200     IF SQLCODE = +0                                              PPPRMFET
010300        SET XSEL-OK TO TRUE                                       PPPRMFET
010400        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPPRMFET
010500        GO TO 3000-EXIT                                           PPPRMFET
010600     END-IF.                                                      PPPRMFET
010700 3000-EXIT.                                                       PPPRMFET
010800     EXIT.                                                        PPPRMFET
010900*                                                                 PPPRMFET
011000 4000-SELECT SECTION.                                             PPPRMFET
011100*                                                                 PPPRMFET
011200     MOVE 'SELECT PPPPRM ROW' TO DB2MSG-TAG.                      PPPRMFET
011300*                                                                 PPPRMFET
011400     EXEC SQL                                                     PPPRMFET
011500         SELECT *                                                 PPPRMFET
011600         INTO :PPPPRM-ROW                                         PPPRMFET
011700         FROM PPPVZPRM_PRM                                        PPPRMFET
011800         WHERE PRM_NUMBER         = :WS-PPPPRM-PRM-NO             PPPRMFET
011900     END-EXEC.                                                    PPPRMFET
012000*                                                                 PPPRMFET
012100     IF SQLCODE = +100                                            PPPRMFET
012200        INITIALIZE XSEL-RETURN-KEY                                PPPRMFET
012300                   PPPPRM-ROW                                     PPPRMFET
012400        SET XSEL-INVALID-KEY TO TRUE                              PPPRMFET
012500        GO TO 4000-EXIT                                           PPPRMFET
012600     END-IF.                                                      PPPRMFET
012700*                                                                 PPPRMFET
012800     IF SQLCODE = +0                                              PPPRMFET
012900        MOVE WS-CONTROL-KEY    TO XSEL-RETURN-CTL-KEY             PPPRMFET
013000        SET XSEL-OK TO TRUE                                       PPPRMFET
013100        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPPRMFET
013200        GO TO 4000-EXIT                                           PPPRMFET
013300     END-IF.                                                      PPPRMFET
013400*                                                                 PPPRMFET
013500 4000-EXIT.                                                       PPPRMFET
013600     EXIT.                                                        PPPRMFET
013700*                                                                 PPPRMFET
013800 5000-CLOSE-CURSOR SECTION.                                       PPPRMFET
013900     IF WS-CURSOR-CLOSED                                          PPPRMFET
014000        SET XSEL-INVALID-REQUEST TO TRUE                          PPPRMFET
014100     ELSE                                                         PPPRMFET
014200        EXEC SQL                                                  PPPRMFET
014300            CLOSE PPPPRM-CURSOR                                   PPPRMFET
014400        END-EXEC                                                  PPPRMFET
014500        SET WS-CURSOR-CLOSED TO TRUE                              PPPRMFET
014600     END-IF.                                                      PPPRMFET
014700 5000-EXIT.                                                       PPPRMFET
014800     EXIT.                                                        PPPRMFET
014900*                                                                 PPPRMFET
015000*999999-SQL-ERROR SECTION.                                        PPPRMFET
015100     COPY CPPDXP99.                                               PPPRMFET
015200     SET XSEL-DB2-ERROR TO TRUE.                                  PPPRMFET
015300     MOVE SQLCODE TO XSEL-DB2-MSG-NUMBER.                         PPPRMFET
015400     GOBACK.                                                      PPPRMFET
015500 999999-EXIT.                                                     PPPRMFET
015600     EXIT.                                                        PPPRMFET
015700*                                                                 PPPRMFET
