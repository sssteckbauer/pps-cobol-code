000010**************************************************************/   05390718
000020*  PROGRAM: PPBENEPD                                         */   05390718
000030*  RELEASE: ___0718______ SERVICE REQUEST(S): ____10539____  */   05390718
000031*  REFERENCE RELEASE: ___651___                              */   05390718
000040*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___11/23/92__  */   05390718
000050*  DESCRIPTION:                                              */   05390718
000060*  - MODIFIED CODE TO ACCOMMODATE THE NEW RULE WHEN TO TAKE  */   05390718
000070*    INSURANCE DEDUCTIONS.                                   */   05390718
000080**************************************************************/   05390718
000100**************************************************************/   36220651
000200*  PROGRAM:  PPBENEPD                                        */   36220651
000300*  RELEASE # __0651____   SERVICE REQUEST NO(S) __3622______ */   36220651
000400*  NAME _______PXP_____   MODIFICATION DATE ____04/13/92_____*/   36220651
000500*  DESCRIPTION                                               */   36220651
000600*  - IMPLEMENT COVERAGE EFFECTIVE DATE SUPPRESSION OF        */   36220651
000700*    DEDUCTIONS.                                             */   36220651
000800**************************************************************/   36220651
000100******************************************************************02040641
000200*  PROGRAM: PPBENEPD                                             *02040641
000300*  RELEASE: ____0641____  SERVICE REQUEST(S): ____10204___       *02040641
000400*  NAME:    __J.QUAN____  MODIFICATION DATE:  __02/01/92__       *02040641
000500*  DESCRIPTION:                                                  *02040641
000700*    - NEW MODULE FOR RETRIEVING BENEFITS RATES FROM THE         *02040641
000800*      BENEFITS RATES TABLE. LOGIC IS MODELLED AFTER PPBENSTD    *02040641
001000******************************************************************02040641
001110 IDENTIFICATION DIVISION.                                         PPBENEPD
001200 PROGRAM-ID. PPBENEPD.                                            PPBENEPD
001300 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENEPD
001400 AUTHOR. J. QUAN.                                                 PPBENEPD
001700*                                                                 PPBENEPD
001800 DATE-WRITTEN. FEBRUARY 2, 1992.                                  PPBENEPD
001900 DATE-COMPILED.                                                   PPBENEPD
002000*REMARKS.                                                         PPBENEPD
002100******************************************************************PPBENEPD
002200*                                                                *PPBENEPD
002300*  THIS MODULE PERFORMS EMPLOYEE PAID DISABILITY RATE AND        *PPBENEPD
002400*  PREMIUM CALCULATIONS.                                         *PPBENEPD
002500*                                                                *PPBENEPD
002600******************************************************************PPBENEPD
002700 ENVIRONMENT DIVISION.                                            PPBENEPD
002800 CONFIGURATION SECTION.                                           PPBENEPD
002900 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       PPBENEPD
003000 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       PPBENEPD
003100 SPECIAL-NAMES.                                                   PPBENEPD
003200 INPUT-OUTPUT SECTION.                                            PPBENEPD
003300 FILE-CONTROL.                                                    PPBENEPD
003400     SKIP3                                                        PPBENEPD
003500******************************************************************PPBENEPD
003600*                                                                *PPBENEPD
003700*                         DATA DIVISION                          *PPBENEPD
003800*                                                                *PPBENEPD
003900******************************************************************PPBENEPD
004000 DATA DIVISION.                                                   PPBENEPD
004100 FILE SECTION.                                                    PPBENEPD
004200     EJECT                                                        PPBENEPD
004300******************************************************************PPBENEPD
004400*                                                                *PPBENEPD
004500*                    WORKING-STORAGE SECTION                     *PPBENEPD
004600*                                                                *PPBENEPD
004700******************************************************************PPBENEPD
004800 WORKING-STORAGE SECTION.                                         PPBENEPD
004900     SKIP1                                                        PPBENEPD
005000 01  WS-ID                        PIC X(37)                       PPBENEPD
005100     VALUE 'PPBENEPD WORKING-STORAGE BEGINS HERE'.                PPBENEPD
005700 01  DED-EFF-DATE-COMMAREA EXTERNAL.  COPY CPWSEFDT.              36220651
005200     SKIP3                                                        PPBENEPD
005300 01  WS-SUBSCRIPTS    COMP.                                       PPBENEPD
005400     05  IWAIT                       PIC S9(04).                  PPBENEPD
005500     05  IAGE                        PIC S9(04).                  PPBENEPD
005600     SKIP3                                                        PPBENEPD
005700 01  WS-WORK-VALUES   COMP-3.                                     PPBENEPD
005800     05  WS-SALARY-BASE              PIC S9(05).                  PPBENEPD
005900     05  WS-EPD-RATE                 PIC S9V9(4).                 PPBENEPD
006000     SKIP3                                                        PPBENEPD
006100*    *******************************************                  PPBENEPD
006200*    * USED TO GET PREMIUM AMOUNT BY PLAN CODE                    PPBENEPD
006300*    *******************************************                  PPBENEPD
006400 01  FLAGS-AND-SWITCHES.                                          PPBENEPD
006500     05  PROGRAM-STATUS-FLAG         PIC X(01)   VALUE '0'.       PPBENEPD
006600         88  PROGRAM-STATUS-NORMAL               VALUE '0'.       PPBENEPD
006700         88  PROGRAM-STATUS-EXITING              VALUE '1'.       PPBENEPD
006800     05  FOUND-RATE-SW               PIC X(01)   VALUE 'N'.       PPBENEPD
006900         88  FOUND-RATE                          VALUE 'Y'.       PPBENEPD
007000     SKIP1                                                        PPBENEPD
007100 01  EDIT-VALUES-AREA.                                            PPBENEPD
007200******************************************************************PPBENEPD
007300*                USED FOR DATE CALCS                             *PPBENEPD
007400******************************************************************PPBENEPD
007500     05  WS-PPEND-DATE-HOLD       PIC 9(6).                       PPBENEPD
007600     05  WS-PPEND-AREA  REDEFINES                                 PPBENEPD
007700         WS-PPEND-DATE-HOLD.                                      PPBENEPD
007800         10  WS-PPEND-YYMM        PIC 9(4).                       PPBENEPD
007900         10  FILLER               PIC XX.                         PPBENEPD
008000*                                                                *PPBENEPD
008100     05  WS-EFFECT-DATE-HOLD      PIC 9(6).                       PPBENEPD
008200     05  WS-EFFECT-AREA REDEFINES                                 PPBENEPD
008300         WS-EFFECT-DATE-HOLD.                                     PPBENEPD
008400         10  WS-EFFECT-YYMM       PIC 9(4).                       PPBENEPD
008500         10  FILLER               PIC XX.                         PPBENEPD
008600*                                                                *PPBENEPD
008700     SKIP1                                                        PPBENEPD
008800 01  CONSTANT-VALUES.                                             PPBENEPD
008900*    *************************************************************PPBENEPD
009000*    * THE FOLLOWING SHOULD BE THE SAME VALUE AS FOR THE OCCURS ABPPBENEPD
009100*    *************************************************************PPBENEPD
009200     05  CONSTANT-BENEFIT-TYPE       PIC X(01)   VALUE 'Z'.       PPBENEPD
009300     05  CONSTANT-NORMAL             PIC X(01)   VALUE '0'.       PPBENEPD
009400     05  CONSTANT-ABORT              PIC X(01)   VALUE '1'.       PPBENEPD
009500     05  CONSTANT-EXIT-PROGRAM       PIC X(01)   VALUE '1'.       PPBENEPD
009600                                  COPY 'CPWSXUTF'.                PPBENEPD
009700     EJECT                                                        PPBENEPD
009800 01  XBRT-BENEFITS-RATES-RECORD.                                  PPBENEPD
009900                                  COPY 'CPWSXBRT'.                PPBENEPD
010000     SKIP3                                                        PPBENEPD
010100 01  WS-BRT-KEY.                                                  PPBENEPD
010200     05  WS-BRT-KEY-CONST         PIC X(04) VALUE ' BRT'.         PPBENEPD
010300     05  WS-BRT-KEY-VALUES.                                       PPBENEPD
010400         10  WS-BRT-BRSC          PIC X(05).                      PPBENEPD
010500         10  WS-BRT-BEN-TYPE      PIC X.                          PPBENEPD
010600         10  WS-BRT-SEQ-NO        PIC 99.                         PPBENEPD
010700 01  WS-DEFAULTS.                                                 PPBENEPD
010800     05  WS-DEFAULT-BRSC          PIC X(05) VALUE '00   '.        PPBENEPD
010900     05  WS-DEFAULT-BRT           PIC X(05) VALUE '<<   '.        PPBENEPD
011000 01  FILLER                       PIC X(37)                       PPBENEPD
011100     VALUE 'PPBENEPD WORKING-STORAGE ENDS HERE'.                  PPBENEPD
011200     EJECT                                                        PPBENEPD
011300******************************************************************PPBENEPD
011400*                                                                *PPBENEPD
011500*                        LINKAGE SECTION                         *PPBENEPD
011600*                                                                *PPBENEPD
011700******************************************************************PPBENEPD
011800 LINKAGE SECTION.                                                 PPBENEPD
011900     SKIP1                                                        PPBENEPD
012000 01  PPBENEPD-INTERFACE.          COPY 'CPLNKEPD'.                PPBENEPD
012100     EJECT                                                        PPBENEPD
012200 01  CTL-INTERFACE.                                               PPBENEPD
012300                                  COPY 'CPWSXCIF'.                PPBENEPD
012400     SKIP3                                                        PPBENEPD
012500 01  CTL-SEGMENT-TABLE.                                           PPBENEPD
012600                                  COPY 'CPWSXCST'.                PPBENEPD
012700     EJECT                                                        PPBENEPD
012800******************************************************************PPBENEPD
012900*                                                                *PPBENEPD
013000*                       PROCEDURE DIVISION                       *PPBENEPD
013100*                                                                *PPBENEPD
013200******************************************************************PPBENEPD
013300     SKIP1                                                        PPBENEPD
013400 PROCEDURE DIVISION USING PPBENEPD-INTERFACE                      PPBENEPD
013500                          CTL-INTERFACE                           PPBENEPD
013600                          CTL-SEGMENT-TABLE.                      PPBENEPD
013700     SKIP1                                                        PPBENEPD
013800 A000-MAINLINE SECTION.                                           PPBENEPD
013900     SKIP2                                                        PPBENEPD
014000     MOVE CONSTANT-NORMAL TO PROGRAM-STATUS-FLAG.                 PPBENEPD
014100     SKIP1                                                        PPBENEPD
014200     PERFORM B010-INITIALIZE                                      PPBENEPD
014300        THRU B019-INITIALIZE-EXIT.                                PPBENEPD
014400     SKIP1                                                        PPBENEPD
014500     IF PROGRAM-STATUS-NORMAL                                     PPBENEPD
014600     THEN                                                         PPBENEPD
014700        IF KEPD-ACTION-RATE-RETRIEVAL OR                          PPBENEPD
014800           KEPD-ACTION-PREMIUM-CALC                               PPBENEPD
014900        THEN                                                      PPBENEPD
015000            PERFORM C010-GET-RATE                                 PPBENEPD
015100               THRU C019-GET-RATE-EXIT                            PPBENEPD
015200            IF PROGRAM-STATUS-NORMAL    AND                       PPBENEPD
015300               KEPD-ACTION-PREMIUM-CALC AND                       PPBENEPD
015400               FOUND-RATE                                         PPBENEPD
015500            THEN                                                  PPBENEPD
015600                PERFORM D010-CALCULATE                            PPBENEPD
015700                   THRU D019-CALCULATE-EXIT                       PPBENEPD
015800            ELSE                                                  PPBENEPD
015900                NEXT SENTENCE.                                    PPBENEPD
016000     SKIP3                                                        PPBENEPD
016100 A009-GOBACK.                                                     PPBENEPD
016200     GOBACK.                                                      PPBENEPD
016300     EJECT                                                        PPBENEPD
016400 B000-INITIALIZATION SECTION.                                     PPBENEPD
016500     SKIP2                                                        PPBENEPD
016600 B010-INITIALIZE.                                                 PPBENEPD
016700     SKIP1                                                        PPBENEPD
016800     MOVE CONSTANT-NORMAL TO KEPD-RETURN-STATUS-FLAG.             PPBENEPD
016900*                                                                 PPBENEPD
017000******************************************************************PPBENEPD
017100*      SET NUMERIC RETURN FIELDS TO ZERO                         *PPBENEPD
017200******************************************************************PPBENEPD
017300*                                                                 PPBENEPD
017400     MOVE ZEROS TO KEPD-RATE                                      PPBENEPD
017500                   KEPD-BASE-SALARY                               PPBENEPD
017600                   KEPD-MAX-SALARY                                PPBENEPD
017700                   KEPD-TOTAL-PREMIUM.                            PPBENEPD
017800     SKIP1                                                        PPBENEPD
017900     IF KEPD-ACTION-RATE-RETRIEVAL OR                             PPBENEPD
018000        KEPD-ACTION-PREMIUM-CALC                                  PPBENEPD
018100         PERFORM B030-VALIDATE-LOOKUP-ARGS                        PPBENEPD
018200            THRU B039-VALIDATE-LOOKUP-ARGS-EXIT                   PPBENEPD
018300     ELSE                                                         PPBENEPD
018400         NEXT SENTENCE.                                           PPBENEPD
018500     SKIP1                                                        PPBENEPD
018600     IF KEPD-INVALID-LOOKUP-ARG                                   PPBENEPD
018700         NEXT SENTENCE                                            PPBENEPD
018800     ELSE                                                         PPBENEPD
018900         IF KEPD-ACTION-PREMIUM-CALC                              PPBENEPD
019000             PERFORM B050-VALIDATE-ENROLLMENT                     PPBENEPD
019100                THRU B059-VALIDATE-ENROLLMENT-EXIT                PPBENEPD
019200         ELSE                                                     PPBENEPD
019300             NEXT SENTENCE.                                       PPBENEPD
019400     SKIP3                                                        PPBENEPD
019500 B019-INITIALIZE-EXIT.                                            PPBENEPD
019600     EXIT.                                                        PPBENEPD
019700     SKIP3                                                        PPBENEPD
019800 B030-VALIDATE-LOOKUP-ARGS.                                       PPBENEPD
019900     SKIP1                                                        PPBENEPD
020000     MOVE '0' TO KEPD-INVALID-LOOKUP-ARG-FLAG                     PPBENEPD
020100                 KEPD-NOT-ENROLLED-IN-EPD-FLAG.                   PPBENEPD
020200     SKIP1                                                        PPBENEPD
020300     IF  KEPD-INVALID-AGE                                         PPBENEPD
020400         MOVE '1' TO KEPD-INVALID-LOOKUP-ARG-FLAG                 PPBENEPD
020500     ELSE                                                         PPBENEPD
020600         NEXT SENTENCE.                                           PPBENEPD
020700     SKIP1                                                        PPBENEPD
020800     IF  KEPD-VALID-WAIT-PERIOD                                   PPBENEPD
020900         NEXT SENTENCE                                            PPBENEPD
021000     ELSE                                                         PPBENEPD
021100         MOVE '1' TO KEPD-INVALID-LOOKUP-ARG-FLAG.                PPBENEPD
021200     SKIP1                                                        PPBENEPD
021300     IF KEPD-INVALID-LOOKUP-ARG                                   PPBENEPD
021400         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENEPD
021500     SKIP1                                                        PPBENEPD
021600     MOVE '1' TO KEPD-NOT-ENROLLED-IN-EPD-FLAG.                   PPBENEPD
021700     SKIP3                                                        PPBENEPD
021800 B039-VALIDATE-LOOKUP-ARGS-EXIT.                                  PPBENEPD
021900     EXIT.                                                        PPBENEPD
022000     SKIP3                                                        PPBENEPD
022100 B050-VALIDATE-ENROLLMENT.                                        PPBENEPD
022200     SKIP1                                                        PPBENEPD
022300     MOVE '0' TO KEPD-NOT-ENROLLED-IN-EPD-FLAG.                   PPBENEPD
023000*****MOVE KEPD-COVERAGE-EFFECT-DATE  TO WS-EFFECT-DATE-HOLD.      05390718
023100*****MOVE KEPD-PERIOD-END-DATE       TO WS-PPEND-DATE-HOLD.       05390718
023710*****                                                          CD 05390718
023800     SKIP2                                                        36220651
024500******************************************************************36220651
024600*  IF THE USER HAS FAILED TO ASSIGN AN INDICATOR IN THE GTN      *36220651
024700*  TABLE, USE 'E' AS A DEFAULT.                                  *36220651
024800******************************************************************36220651
024900     IF EFDT-INPUT-XGTN-ICED-IND = 'A' OR 'E' OR 'V' OR 'Z'       36220651
025000         CONTINUE                                                 36220651
025100     ELSE                                                         36220651
025200        MOVE 'E' TO EFDT-INPUT-XGTN-ICED-IND                      36220651
025300     END-IF.                                                      36220651
025400*  THIS CODE CAN BYPASS NORMAL DEDUCTIONS BASED ON DEDUCTION     *36220651
025500*  EFFECTIVE DATE CONSIDERATIONS.                                *36220651
025600*****MOVE WS-EFFECT-AREA(1:2) TO EFDT-INPUT-EFF-YY.               05390718
025700*****MOVE WS-EFFECT-AREA(3:2) TO EFDT-INPUT-EFF-MM.               05390718
025800*****MOVE WS-PPEND-AREA(1:2)   TO EFDT-INPUT-END-YEAR.            05390718
025900*****MOVE WS-PPEND-AREA(3:2)  TO EFDT-INPUT-END-MONTH.            05390718
025910     MOVE KEPD-COVERAGE-EFFECT-DATE(1:2) TO EFDT-INPUT-EFF-YY.    05390718
025911     MOVE KEPD-COVERAGE-EFFECT-DATE(3:2) TO EFDT-INPUT-EFF-MM.    05390718
025912     MOVE KEPD-COVERAGE-EFFECT-DATE(5:2) TO EFDT-INPUT-EFF-DD.    05390718
025920     MOVE KEPD-PERIOD-END-DATE(1:2) TO EFDT-XPCR-YY.              05390718
025930     MOVE KEPD-PERIOD-END-DATE(3:2) TO EFDT-XPCR-MM.              05390718
025940     MOVE KEPD-PERIOD-END-DATE(5:2) TO EFDT-XPCR-DD.              05390718
026000     PERFORM Z01-PROCESS-EFF-DATE.                                36220651
026100     IF EFDT-DONT-TAKE-DEDUCTION                                  36220651
026200         MOVE '1' TO KEPD-NOT-ENROLLED-IN-EPD-FLAG                36220651
026300     END-IF.                                                      36220651
023200     SKIP1                                                        PPBENEPD
023300     IF  KEPD-INVALID-SALARY-BASE                                 PPBENEPD
023400         MOVE '1' TO KEPD-INVALID-LOOKUP-ARG-FLAG                 PPBENEPD
023500     ELSE                                                         PPBENEPD
023600         NEXT SENTENCE.                                           PPBENEPD
023700     SKIP3                                                        PPBENEPD
023800     IF KEPD-NOT-ENROLLED-IN-EPD                                  PPBENEPD
023900              OR                                                  PPBENEPD
024000        KEPD-INVALID-LOOKUP-ARG                                   PPBENEPD
024100         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENEPD
024200     SKIP1                                                        PPBENEPD
024300 B059-VALIDATE-ENROLLMENT-EXIT.                                   PPBENEPD
024400     EXIT.                                                        PPBENEPD
024500     EJECT                                                        PPBENEPD
024600 C000-RATE-RETRIEVAL SECTION.                                     PPBENEPD
024700     SKIP2                                                        PPBENEPD
024800 C010-GET-RATE.                                                   PPBENEPD
024900     SKIP1                                                        PPBENEPD
025000     IF KEPD-BRSC  = WS-DEFAULT-BRSC                              PPBENEPD
025100         MOVE WS-DEFAULT-BRT TO WS-BRT-BRSC                       PPBENEPD
025200     ELSE                                                         PPBENEPD
025300         MOVE KEPD-BRSC TO WS-BRT-BRSC.                           PPBENEPD
025400     SKIP1                                                        PPBENEPD
025500     MOVE CONSTANT-BENEFIT-TYPE TO WS-BRT-BEN-TYPE.               PPBENEPD
025600     MOVE '01' TO WS-BRT-SEQ-NO.                                  PPBENEPD
025700     SKIP1                                                        PPBENEPD
025800     PERFORM P010-BRT-LOOKUP-RATE                                 PPBENEPD
025900        THRU P019-BRT-LOOKUP-RATE-EXIT.                           PPBENEPD
026000     SKIP3                                                        PPBENEPD
026100     IF FOUND-RATE                                                PPBENEPD
026200         PERFORM C030-CALC-RATE                                   PPBENEPD
026300            THRU C039-CALC-RATE-EXIT                              PPBENEPD
026400     ELSE                                                         PPBENEPD
026500         MOVE '1' TO PROGRAM-STATUS-FLAG                          PPBENEPD
026600         MOVE '1' TO KEPD-NOT-ENROLLED-IN-EPD-FLAG.               PPBENEPD
026700     SKIP3                                                        PPBENEPD
026800 C019-GET-RATE-EXIT.                                              PPBENEPD
026900     EXIT.                                                        PPBENEPD
027000     SKIP3                                                        PPBENEPD
027100 C030-CALC-RATE.                                                  PPBENEPD
027200     SKIP1                                                        PPBENEPD
027300*    *************************************************************PPBENEPD
027400*    SAVE THE MAXIMUM SALARY                                      PPBENEPD
027500*    *************************************************************PPBENEPD
027600     MOVE XBRT-EPD-MAX-MTHLY-SAL TO KEPD-MAX-SALARY.              PPBENEPD
027700     SKIP1                                                        PPBENEPD
027800*    *************************************************************PPBENEPD
027900*    SET THE SUBSCRIPT FOR THE WAITING PERIOD                     PPBENEPD
028000*    *************************************************************PPBENEPD
028100     PERFORM VARYING IWAIT FROM 1 BY 1                            PPBENEPD
028200       UNTIL KEPD-WAIT-PERIOD = XBRT-EPD-WAIT-PER (IWAIT)         PPBENEPD
028300     END-PERFORM.                                                 PPBENEPD
028400     SKIP1                                                        PPBENEPD
028500*    *************************************************************PPBENEPD
028600*    SET THE SUBSCRIPT FOR THE AGE GROUP                          PPBENEPD
028700*    *************************************************************PPBENEPD
028800     PERFORM VARYING IAGE FROM 1 BY 1                             PPBENEPD
028900       UNTIL KEPD-EMPLOYEE-AGE NOT < XBRT-EPD-MIN-AGE (IAGE) AND  PPBENEPD
029000             KEPD-EMPLOYEE-AGE NOT > XBRT-EPD-MAX-AGE (IAGE)      PPBENEPD
029100     END-PERFORM.                                                 PPBENEPD
029200     SKIP1                                                        PPBENEPD
029300*    *************************************************************PPBENEPD
029400*    AND GET THE RATE                                             PPBENEPD
029500*    *************************************************************PPBENEPD
029600     MOVE XBRT-EPD-RATE (IAGE, IWAIT) TO KEPD-RATE                PPBENEPD
029700                                         WS-EPD-RATE.             PPBENEPD
029800     SKIP3                                                        PPBENEPD
029900 C039-CALC-RATE-EXIT.                                             PPBENEPD
030000     EXIT.                                                        PPBENEPD
030100     EJECT                                                        PPBENEPD
030200 D000-EPD-CALC SECTION.                                           PPBENEPD
030300     SKIP2                                                        PPBENEPD
030400 D010-CALCULATE.                                                  PPBENEPD
030500     SKIP1                                                        PPBENEPD
030600*    *************************************************************PPBENEPD
030700*    CALCULATE THE SALARY BASE                                    PPBENEPD
030800*    *************************************************************PPBENEPD
030900     IF KEPD-SALARY-BASE > KEPD-MAX-SALARY                        PPBENEPD
031000         MOVE XBRT-EPD-MAX-MTHLY-SAL TO WS-SALARY-BASE            PPBENEPD
031100     ELSE                                                         PPBENEPD
031200         MOVE KEPD-SALARY-BASE TO WS-SALARY-BASE                  PPBENEPD
031300     END-IF.                                                      PPBENEPD
031400     SKIP1                                                        PPBENEPD
031500     MOVE WS-SALARY-BASE TO KEPD-BASE-SALARY.                     PPBENEPD
031600     SKIP1                                                        PPBENEPD
031700*    *************************************************************PPBENEPD
031800*    CALCULATE THE PREMIUM                                        PPBENEPD
031900*    *************************************************************PPBENEPD
032000     COMPUTE KEPD-TOTAL-PREMIUM ROUNDED =                         PPBENEPD
032100             WS-SALARY-BASE * WS-EPD-RATE.                        PPBENEPD
032200     SKIP3                                                        PPBENEPD
032300 D019-CALCULATE-EXIT.                                             PPBENEPD
032400     EXIT.                                                        PPBENEPD
032500     EJECT                                                        PPBENEPD
032600 P000-SET-UP-BRT-KEY SECTION.                                     PPBENEPD
032700     SKIP2                                                        PPBENEPD
032800 P010-BRT-LOOKUP-RATE.                                            PPBENEPD
032900     SKIP1                                                        PPBENEPD
033000******************************************************************PPBENEPD
033100*      TO AVOID UNNECESSARY DISK ACCESS, CHECK PREVIOUS BEFORE   *PPBENEPD
033200*      DOING LOOKUP PHYSICALLY IN TABLE                          *PPBENEPD
033300******************************************************************PPBENEPD
033400     SKIP1                                                        PPBENEPD
033500     IF XBRT-KEY = WS-BRT-KEY                                     PPBENEPD
033600         MOVE 'Y' TO FOUND-RATE-SW                                PPBENEPD
033700     ELSE                                                         PPBENEPD
033800         MOVE 'N' TO FOUND-RATE-SW                                PPBENEPD
033900*                                                                 PPBENEPD
034000*        ************************************                     PPBENEPD
034100*        * RETRIEVE RATE FROM TABLE USING KEY                     PPBENEPD
034200*        ************************************                     PPBENEPD
034300*                                                                 PPBENEPD
034400         PERFORM P050-ACCESS-CTL                                  PPBENEPD
034500            THRU P059-ACCESS-CTL-EXIT                             PPBENEPD
034600                 UNTIL FOUND-RATE OR PROGRAM-STATUS-EXITING.      PPBENEPD
034700     SKIP3                                                        PPBENEPD
034800 P019-BRT-LOOKUP-RATE-EXIT.                                       PPBENEPD
034900     EXIT.                                                        PPBENEPD
035000     SKIP3                                                        PPBENEPD
035100 P050-ACCESS-CTL.                                                 PPBENEPD
035200     SKIP1                                                        PPBENEPD
035300     MOVE WS-BRT-KEY       TO IO-CTL-NOM-KEY.                     PPBENEPD
035400     PERFORM R020-CALL-PPIOCTL                                    PPBENEPD
035500        THRU R029-CALL-PPIOCTL-EXIT.                              PPBENEPD
035600     SKIP1                                                        PPBENEPD
035700     IF IO-CTL-ERROR-CODE NOT = '00' AND                          PPBENEPD
035800                          NOT = '05' AND                          PPBENEPD
035900                          NOT = '06' AND                          PPBENEPD
036000                          NOT = '13' AND                          PPBENEPD
036100                          NOT = '21'                              PPBENEPD
036200*                                                                 PPBENEPD
036300*        ********************************************             PPBENEPD
036400*        * ABORT PROGRAM DUE TO ERROR READING TABLE *             PPBENEPD
036500*        ********************************************             PPBENEPD
036600*                                                                 PPBENEPD
036700         MOVE CONSTANT-ABORT TO KEPD-RETURN-STATUS-FLAG           PPBENEPD
036800         MOVE CONSTANT-EXIT-PROGRAM TO PROGRAM-STATUS-FLAG        PPBENEPD
036900     ELSE                                                         PPBENEPD
037000         IF IO-CTL-ERROR-CODE = '00' AND                          PPBENEPD
037100            CTL-SEG-TAB-DEL NOT = HIGH-VALUE                      PPBENEPD
037200*                                                                 PPBENEPD
037300*        *****************************************************    PPBENEPD
037400*        * HIGH-VALUE WOULD INDICATE THAT RECORD IS DELETED, *    PPBENEPD
037500*        * WHICH WOULD NOT BE A TRUE "FIND"                  *    PPBENEPD
037600*        *****************************************************    PPBENEPD
037700*                                                                 PPBENEPD
037800             MOVE 'Y' TO FOUND-RATE-SW                            PPBENEPD
037900         ELSE                                                     PPBENEPD
038000             IF IO-CTL-ERROR-CODE  = '05' OR '06'                 PPBENEPD
038100                                  OR '13' OR '21'                 PPBENEPD
038200*                                                                 PPBENEPD
038300*        *********************************************************PPBENEPD
038400*        * RATE NOT FOUND, SO GET KEY FOR NEXT ITERATION OF LOOP *PPBENEPD
038500*        *********************************************************PPBENEPD
038600*                                                                 PPBENEPD
038700                IF WS-BRT-BRSC NOT = WS-DEFAULT-BRT               PPBENEPD
038800                   MOVE '2' TO KEPD-RETURN-STATUS-FLAG            PPBENEPD
038900                   MOVE WS-DEFAULT-BRT  TO WS-BRT-BRSC            PPBENEPD
039000                ELSE                                              PPBENEPD
039100                   MOVE CONSTANT-ABORT TO KEPD-RETURN-STATUS-FLAG PPBENEPD
039200                   MOVE CONSTANT-EXIT-PROGRAM TO                  PPBENEPD
039300                                       PROGRAM-STATUS-FLAG.       PPBENEPD
039400     SKIP1                                                        PPBENEPD
039500*    ************************************************************ PPBENEPD
039600*    * SAVE PREVIOUS, TO AVOID UNECESSARY DISK ACCESS IN FUTURE * PPBENEPD
039700*    ************************************************************ PPBENEPD
039800     SKIP1                                                        PPBENEPD
039900     IF FOUND-RATE                                                PPBENEPD
040000         MOVE CTL-SEGMENT-TABLE TO XBRT-BENEFITS-RATES-RECORD     PPBENEPD
040100     ELSE                                                         PPBENEPD
040200         NEXT SENTENCE.                                           PPBENEPD
040300     SKIP3                                                        PPBENEPD
040400 P059-ACCESS-CTL-EXIT.                                            PPBENEPD
040500     EXIT.                                                        PPBENEPD
040600     EJECT                                                        PPBENEPD
040700 R000-SUB-PROGRAM-CALLS SECTION.                                  PPBENEPD
040800     SKIP2                                                        PPBENEPD
040900     SKIP3                                                        PPBENEPD
041000 R020-CALL-PPIOCTL.                                               PPBENEPD
041100     SKIP1                                                        PPBENEPD
041200     CALL 'PPIOCTL' USING CTL-INTERFACE                           PPBENEPD
041300                          CTL-SEGMENT-TABLE.                      PPBENEPD
041400     SKIP3                                                        PPBENEPD
041500 R029-CALL-PPIOCTL-EXIT.                                          PPBENEPD
041600     EXIT.                                                        PPBENEPD
044900 Z01-PROCESS-EFF-DATE SECTION.                                    36220651
045000     COPY CPPDEFDT.                                               36220651
