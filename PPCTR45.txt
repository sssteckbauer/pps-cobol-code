000000**************************************************************/   69321592
000001*  PROGRAM: PPCTR45                                          */   69321592
000002*  RELEASE: ___1592______ SERVICE REQUEST(S): ____16932____  */   69321592
000003*  NAME:_______QUAN______ CREATION DATE:      ___06/28/04__  */   69321592
000004*  DESCRIPTION:                                              */   69321592
000005*  - NACHA GTN ORGANIZATION REPORT MODULE                    */   69321592
000008**************************************************************/   69321592
000800 IDENTIFICATION DIVISION.                                         PPCTR45
000900 PROGRAM-ID. PPCTR45.                                             PPCTR45
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTR45
001100 DATE-COMPILED.                                                   PPCTR45
001200 ENVIRONMENT DIVISION.                                            PPCTR45
001300 CONFIGURATION SECTION.                                           PPCTR45
001400 SPECIAL-NAMES.                                                   PPCTR45
001500     C01 IS NEW-PAGE,                                             PPCTR45
001600     CSP IS NO-LINES.                                             PPCTR45
001700 INPUT-OUTPUT SECTION.                                            PPCTR45
001800 FILE-CONTROL.                                                    PPCTR45
001900     SELECT REPORT-PRINT-FILE     ASSIGN TO UT-S-PPP0445.         PPCTR45
002000 DATA DIVISION.                                                   PPCTR45
002100 FILE SECTION.                                                    PPCTR45
002200 FD  REPORT-PRINT-FILE            COPY CPFDXPRT.                  PPCTR45
002300 WORKING-STORAGE SECTION.                                         PPCTR45
002400 01  MISC.                                                        PPCTR45
002500     05  LINE-COUNT               PIC S9(4) COMP SYNC VALUE +99.  PPCTR45
002600     05  NO-OF-LINES              PIC S9(4) COMP SYNC VALUE +1.   PPCTR45
002610     05  LINK-ENTRY               PIC S9(4) COMP SYNC VALUE +0.   PPCTR45
002700     05  PAGE-COUNT               PIC S9(6) COMP SYNC VALUE ZERO. PPCTR45
002740     05  EOF-CURSOR-O-SW          PIC  X(1) VALUE 'N'.            PPCTR45
002750         88 EOF-CURSOR-O                    VALUE 'Y'.            PPCTR45
002760         88 NOT-EOF-CURSOR-O                VALUE 'N'.            PPCTR45
002761     05  EOF-CURSOR-L-SW          PIC  X(1) VALUE 'N'.            PPCTR45
002770         88 EOF-CURSOR-L                    VALUE 'Y'.            PPCTR45
002780         88 NOT-EOF-CURSOR-L                VALUE 'N'.            PPCTR45
002800 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTR45
002900 01  CTH-CTL-TABLE-MAINT-HEADERS.               COPY CPWSXCHR.    PPCTR45
002910 01  XDC3-DATE-CONVERSION-WORK.                 COPY CPWSXDC3.    PPCTR45
003000 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTR45
003100 01  RPT45-HD1.                                                   PPCTR45
004400     03  FILLER                   PIC X(1)  VALUE SPACES.         PPCTR45
004500     03  FILLER                   PIC X(14) VALUE                 PPCTR45
004600         'LINK ORG NAME/'.                                        PPCTR45
004700     03  FILLER                   PIC X(09) VALUE SPACES.         PPCTR45
004710     03  FILLER                   PIC X(15) VALUE                 PPCTR45
004720         'PRENOTE/DEPOSIT'.                                       PPCTR45
004730     03  FILLER                   PIC X(04) VALUE SPACES.         PPCTR45
004740     03  FILLER                   PIC X(15) VALUE                 PPCTR45
004750         'PRENOTE/DEPOSIT'.                                       PPCTR45
004760     03  FILLER                   PIC X(02) VALUE SPACES.         PPCTR45
004770     03  FILLER                   PIC X(11) VALUE                 PPCTR45
004780         'PRENOTE PAY'.                                           PPCTR45
004790     03  FILLER                   PIC X(03) VALUE SPACES.         PPCTR45
004791     03  FILLER                   PIC X(15) VALUE                 PPCTR45
004792         'DEPOSIT PAY END'.                                       PPCTR45
004793     03  FILLER                   PIC X(04) VALUE SPACES.         PPCTR45
004797     03  FILLER                   PIC X(40) VALUE                 PPCTR45
004798         'PRNOTE CYCL/ ORG GRP NEG ADDENDA PRENOTE'.              PPCTR45
004799 01  RPT45-HD2.                                                   PPCTR45
004800     03  FILLER                   PIC X(1)  VALUE SPACES.         PPCTR45
004801     03  FILLER                   PIC X(05) VALUE SPACES.         PPCTR45
004802     03  FILLER                   PIC X(36) VALUE                 PPCTR45
004803         'PRNOTE INDV ID/ TRANSMIT RTE CHK DGT'.                  PPCTR45
004804     03  FILLER                   PIC X(02) VALUE SPACES.         PPCTR45
004805     03  FILLER                   PIC X(12) VALUE                 PPCTR45
004806         'BANK ACCOUNT'.                                          PPCTR45
004807     03  FILLER                   PIC X(04) VALUE SPACES.         PPCTR45
004811     03  FILLER                   PIC X(32) VALUE                 PPCTR45
004812         ' END DATE   DATE FIRST DATE LAST'.                      PPCTR45
004813     03  FILLER                   PIC X(01) VALUE SPACES.         PPCTR45
004814     03  FILLER                   PIC X(40) VALUE                 PPCTR45
004815         'FORMAT RTE     SORT  IND   IND      IND '.              PPCTR45
004816 01  RPT45-HD3.                                                   PPCTR45
004817     03  FILLER                   PIC X(1)  VALUE SPACES.         PPCTR45
004818     03  FILLER                   PIC X(08) VALUE                 PPCTR45
004819         '     GTN'.                                              PPCTR45
004832     03  FILLER                   PIC X(124) VALUE SPACES.        PPCTR45
004840 01  RPT45-D1.                                                    PPCTR45
004900     03  FILLER                   PIC X(01).                      PPCTR45
005000     03  FILLER                   PIC X(01).                      PPCTR45
005200     03  RPT45-LINK               PIC 9(02).                      PPCTR45
005300     03  FILLER                   PIC X(02).                      PPCTR45
005400     03  RPT45-ORG-NAME           PIC X(13).                      PPCTR45
005500     03  FILLER                   PIC X(05).                      PPCTR45
005600     03  RPT45-PRE-TRANS-RTE      PIC X(08).                      PPCTR45
005800     03  FILLER                   PIC X(06).                      PPCTR45
005900     03  RPT45-PRE-CHECK-DGT      PIC X(01).                      PPCTR45
005910     03  FILLER                   PIC X(04).                      PPCTR45
005920     03  RPT45-PRE-BANK-ACCT      PIC X(17).                      PPCTR45
005930     03  FILLER                   PIC X(01).                      PPCTR45
005940     03  RPT45-PRE-PAY-END-DT     PIC X(08).                      PPCTR45
005950     03  FILLER                   PIC X(03).                      PPCTR45
005960     03  RPT45-DEP-PAY-END-FRST   PIC X(08).                      PPCTR45
005970     03  FILLER                   PIC X(04).                      PPCTR45
005980     03  RPT45-DEP-PAY-END-LAST   PIC X(08).                      PPCTR45
005990     03  FILLER                   PIC X(06).                      PPCTR45
006000     03  RPT45-PRE-CYCLE          PIC X(02).                      PPCTR45
006100     03  FILLER                   PIC X(09).                      PPCTR45
006200     03  RPT45-ORG-SORT           PIC X(01).                      PPCTR45
006300     03  FILLER                   PIC X(05).                      PPCTR45
006400     03  RPT45-NEG-IND            PIC X(01).                      PPCTR45
006500     03  FILLER                   PIC X(05).                      PPCTR45
006600     03  RPT45-ADDENDA-IND        PIC X(01).                      PPCTR45
006700     03  FILLER                   PIC X(08).                      PPCTR45
006800     03  RPT45-PRENOTE-IND        PIC X(01).                      PPCTR45
006900     03  FILLER                   PIC X(02).                      PPCTR45
008010                                                                  PPCTR45
008011 01  RPT45-D2.                                                    PPCTR45
008012     03  FILLER                   PIC X(01).                      PPCTR45
008013     03  FILLER                   PIC X(05).                      PPCTR45
008014     03  RPT45-INDV-ID            PIC X(15).                      PPCTR45
008015     03  FILLER                   PIC X(03).                      PPCTR45
008016     03  RPT45-DEP-TRANS-RTE      PIC X(08).                      PPCTR45
008017     03  FILLER                   PIC X(06).                      PPCTR45
008018     03  RPT45-DEP-CHECK-DGT      PIC X(01).                      PPCTR45
008019     03  FILLER                   PIC X(04).                      PPCTR45
008020     03  RPT45-DEP-BANK-ACCT      PIC X(17).                      PPCTR45
008021     03  FILLER                   PIC X(38).                      PPCTR45
008022     03  RPT45-FORMAT-RTE         PIC X(02).                      PPCTR45
008023     03  FILLER                   PIC X(33).                      PPCTR45
008024                                                                  PPCTR45
008025 01  RPT45-D3.                                                    PPCTR45
008026     03  FILLER                   PIC X(01).                      PPCTR45
008027     03  FILLER                   PIC X(05).                      PPCTR45
008028     03  RPT45-GTN-ARRAY.                                         PPCTR45
008029         05  RPT45-LINK-ENTRY  OCCURS 4.                          PPCTR45
008030         07  RPT45-GTN                PIC X(03).                  PPCTR45
008031         07  FILLER                   PIC X(01).                  PPCTR45
008040     03  FILLER                       PIC X(111).                 PPCTR45
008041                                                                  PPCTR45
008042 01  NGO-TABLE-DATA.                                              PPCTR45
008043     EXEC SQL                                                     PPCTR45
008044        INCLUDE PPPVZNGO                                          PPCTR45
008050     END-EXEC.                                                    PPCTR45
008060                                                                  PPCTR45
008070 01  NGL-TABLE-DATA.                                              PPCTR45
008080     EXEC SQL                                                     PPCTR45
008090        INCLUDE PPPVZNGL                                          PPCTR45
008091     END-EXEC.                                                    PPCTR45
008100                                                                  PPCTR45
008300     EXEC SQL                                                     PPCTR45
008400          DECLARE NGO_ROW CURSOR FOR                              PPCTR45
008500          SELECT                                                  PPCTR45
008600              NGO_LINK                                            PPCTR45
008700             ,NGO_PRENOTE_IND                                     PPCTR45
008800             ,NGO_PRENOTE_CYC_TP                                  PPCTR45
008900             ,NGO_PRENOTE_RTE_NB                                  PPCTR45
009000             ,NGO_PRENOTE_CHK_DG                                  PPCTR45
009100             ,NGO_PRENOTE_BNK_AC                                  PPCTR45
009200             ,CHAR(NGO_PRENOTE_END_DT,ISO)                        PPCTR45
009300             ,NGO_PRENOTE_IDV_ID                                  PPCTR45
009400             ,NGO_ORG_NAME                                        PPCTR45
009500             ,NGO_ORG_GROUP_SORT                                  PPCTR45
009600             ,NGO_DEPOSIT_RTE_NB                                  PPCTR45
009610             ,NGO_DEPOSIT_CHK_DG                                  PPCTR45
009700             ,NGO_DEPOSIT_BNK_AC                                  PPCTR45
009800             ,CHAR(NGO_DEP_END_DT_FST,ISO)                        PPCTR45
009900             ,CHAR(NGO_DEP_END_DT_LST,ISO)                        PPCTR45
010000             ,NGO_PROC_NEG_IND                                    PPCTR45
010100             ,NGO_PROC_ADDN_IND                                   PPCTR45
010200             ,NGO_PROC_FRMT_RTE                                   PPCTR45
010300             ,NGO_LAST_ACTION                                     PPCTR45
010310             ,NGO_LAST_ACTION_DT                                  PPCTR45
010320              FROM PPPVZNGO_NGO                                   PPCTR45
010330          ORDER BY NGO_LINK                                       PPCTR45
010340     END-EXEC.                                                    PPCTR45
010350                                                                  PPCTR45
010360     EXEC SQL                                                     PPCTR45
010370          DECLARE NGL_ROW CURSOR FOR                              PPCTR45
010371          SELECT                                                  PPCTR45
010372              NGL_LINK                                            PPCTR45
010373             ,NGL_GTN                                             PPCTR45
010374             ,NGL_LAST_ACTION                                     PPCTR45
010375             ,NGL_LAST_ACTION_DT                                  PPCTR45
010376              FROM PPPVZNGL_NGL                                   PPCTR45
010377          WHERE NGL_LINK = :NGO-LINK                              PPCTR45
010378          ORDER BY NGL_GTN                                        PPCTR45
010379     END-EXEC.                                                    PPCTR45
010380                                                                  PPCTR45
010400     EXEC SQL                                                     PPCTR45
010500        INCLUDE SQLCA                                             PPCTR45
010600     END-EXEC.                                                    PPCTR45
010700                                                                  PPCTR45
010800 LINKAGE SECTION.                                                 PPCTR45
010900                                                                  PPCTR45
011000 01  CTPI-CTL-TBL-PRINT-INTERFACE.              COPY CPLNCTPI.    PPCTR45
011100                                                                  PPCTR45
011200 PROCEDURE DIVISION USING CTPI-CTL-TBL-PRINT-INTERFACE.           PPCTR45
011300                                                                  PPCTR45
011400 0000-MAIN SECTION.                                               PPCTR45
011500                                                                  PPCTR45
011600     EXEC SQL                                                     PPCTR45
011700        INCLUDE CPPDXE99                                          PPCTR45
011800     END-EXEC.                                                    PPCTR45
011900                                                                  PPCTR45
012000     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTR45
012100     PERFORM 2000-CREATE-REPORT.                                  PPCTR45
012200     PERFORM 9100-PROGRAM-TERMINATION.                            PPCTR45
012300                                                                  PPCTR45
012400 0999-END.                                                        PPCTR45
012500                                                                  PPCTR45
012600     GOBACK.                                                      PPCTR45
012700                                                                  PPCTR45
012800 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTR45
012900                                                                  PPCTR45
013000     MOVE 'PPCTR45' TO XWHC-DISPLAY-MODULE.                       PPCTR45
013100     COPY CPPDXWHC.                                               PPCTR45
013200     MOVE CTPI-HEADER-1 TO CTH-RPT-HD1.                           PPCTR45
013300     MOVE CTPI-HEADER-2 TO CTH-RPT-HD2.                           PPCTR45
013400     MOVE CTPI-HEADER-3 TO CTH-RPT-HD3.                           PPCTR45
013500     MOVE XWHC-MONTH-COMPILED TO CTH-COMPILE-MM.                  PPCTR45
013600     MOVE XWHC-DAY-COMPILED TO CTH-COMPILE-DD.                    PPCTR45
013700     MOVE XWHC-YEAR-COMPILED TO CTH-COMPILE-YY.                   PPCTR45
013730                                                                  PPCTR45
013740     MOVE SPACES TO RPT45-D1                                      PPCTR45
013750                    RPT45-D2                                      PPCTR45
013760                    RPT45-D3.                                     PPCTR45
013800     OPEN OUTPUT REPORT-PRINT-FILE.                               PPCTR45
014200                                                                  PPCTR45
014300 2000-CREATE-REPORT          SECTION.                             PPCTR45
014400                                                                  PPCTR45
014500     PERFORM 7000-OPEN-NGO.                                       PPCTR45
014600     PERFORM 7100-FETCH-NGO.                                      PPCTR45
014700     PERFORM 6000-PAGE-HEADER.                                    PPCTR45
014800                                                                  PPCTR45
014900     PERFORM 3000-PRINT-NGO-TABLE                                 PPCTR45
015000       UNTIL EOF-CURSOR-O.                                        PPCTR45
015100                                                                  PPCTR45
015200     PERFORM 7200-CLOSE-NGO.                                      PPCTR45
015600                                                                  PPCTR45
015700 3000-PRINT-NGO-TABLE        SECTION.                             PPCTR45
015800                                                                  PPCTR45
015900     MOVE SPACES TO RPT45-D1                                      PPCTR45
016000                    RPT45-D2.                                     PPCTR45
016100                                                                  PPCTR45
016200     MOVE NGO-LINK    TO  RPT45-LINK.                             PPCTR45
016300     MOVE NGO-PRENOTE-IND                                         PPCTR45
016400                      TO RPT45-PRENOTE-IND.                       PPCTR45
016500     MOVE NGO-PRENOTE-CYC-TP                                      PPCTR45
016600                      TO  RPT45-PRE-CYCLE.                        PPCTR45
016610     MOVE NGO-PRENOTE-RTE-NB                                      PPCTR45
016620                      TO  RPT45-PRE-TRANS-RTE.                    PPCTR45
016621     MOVE NGO-PRENOTE-CHK-DG                                      PPCTR45
016622                      TO  RPT45-PRE-CHECK-DGT.                    PPCTR45
016623     MOVE NGO-PRENOTE-BNK-AC                                      PPCTR45
016624                      TO  RPT45-PRE-BANK-ACCT.                    PPCTR45
016625     MOVE NGO-PRENOTE-END-DT  TO XDC3-ISO-DATE.                   PPCTR45
016626     PERFORM XDC3-CONVERT-ISO-TO-FMT.                             PPCTR45
016627     MOVE XDC3-FMT-DATE TO RPT45-PRE-PAY-END-DT.                  PPCTR45
016630     MOVE NGO-ORG-NAME TO  RPT45-ORG-NAME.                        PPCTR45
016631     MOVE NGO-ORG-GROUP-SORT                                      PPCTR45
016632                      TO  RPT45-ORG-SORT.                         PPCTR45
016646                                                                  PPCTR45
016647     MOVE NGO-PROC-NEG-IND   TO RPT45-NEG-IND.                    PPCTR45
016648     MOVE NGO-PROC-ADDN-IND  TO RPT45-ADDENDA-IND.                PPCTR45
016650                                                                  PPCTR45
016660     MOVE NGO-PRENOTE-IDV-ID                                      PPCTR45
016670                      TO  RPT45-INDV-ID.                          PPCTR45
016671     MOVE NGO-DEPOSIT-RTE-NB                                      PPCTR45
016672                      TO  RPT45-DEP-TRANS-RTE.                    PPCTR45
016673     MOVE NGO-DEPOSIT-CHK-DG                                      PPCTR45
016674                      TO  RPT45-DEP-CHECK-DGT.                    PPCTR45
016675     MOVE NGO-DEPOSIT-BNK-AC                                      PPCTR45
016676                      TO  RPT45-DEP-BANK-ACCT.                    PPCTR45
016677                                                                  PPCTR45
016678     MOVE NGO-DEP-END-DT-FST TO XDC3-ISO-DATE.                    PPCTR45
016679     PERFORM XDC3-CONVERT-ISO-TO-FMT.                             PPCTR45
016680     MOVE XDC3-FMT-DATE TO RPT45-DEP-PAY-END-FRST.                PPCTR45
016681     MOVE NGO-DEP-END-DT-LST TO XDC3-ISO-DATE.                    PPCTR45
016682     PERFORM XDC3-CONVERT-ISO-TO-FMT.                             PPCTR45
016683     MOVE XDC3-FMT-DATE TO RPT45-DEP-PAY-END-LAST.                PPCTR45
016684     MOVE NGO-PROC-FRMT-RTE  TO RPT45-FORMAT-RTE.                 PPCTR45
016691                                                                  PPCTR45
016692     MOVE 2 TO NO-OF-LINES.                                       PPCTR45
016693     MOVE RPT45-D1 TO PRINT-REC.                                  PPCTR45
016694     PERFORM 8000-PRINT-A-LINE.                                   PPCTR45
016695                                                                  PPCTR45
016696     MOVE 1 TO NO-OF-LINES.                                       PPCTR45
016697     MOVE RPT45-D2 TO PRINT-REC.                                  PPCTR45
016698     PERFORM 8000-PRINT-A-LINE.                                   PPCTR45
016699                                                                  PPCTR45
016700     MOVE ZERO TO LINK-ENTRY.                                     PPCTR45
016701     PERFORM 7300-OPEN-NGL.                                       PPCTR45
016710     PERFORM 7400-FETCH-NGL.                                      PPCTR45
016800     PERFORM 4000-NGL-DATA                                        PPCTR45
016801        UNTIL SQLCODE NOT = ZERO                                  PPCTR45
016802     PERFORM 7500-CLOSE-NGL.                                      PPCTR45
016803                                                                  PPCTR45
016804     IF  LINK-ENTRY > ZERO                                        PPCTR45
016805         MOVE RPT45-D3 TO PRINT-REC                               PPCTR45
016806         PERFORM 8000-PRINT-A-LINE                                PPCTR45
016807     END-IF.                                                      PPCTR45
016808                                                                  PPCTR45
017900     MOVE SPACES TO RPT45-D1                                      PPCTR45
018000                    RPT45-D2                                      PPCTR45
018100                    RPT45-D3.                                     PPCTR45
019500                                                                  PPCTR45
019600     PERFORM 7100-FETCH-NGO.                                      PPCTR45
019700                                                                  PPCTR45
019800 4000-NGL-DATA      SECTION.                                      PPCTR45
019900                                                                  PPCTR45
019901     ADD 1 TO LINK-ENTRY.                                         PPCTR45
019910     IF  LINK-ENTRY > 4                                           PPCTR45
019920         MOVE RPT45-D3 TO PRINT-REC                               PPCTR45
019930         PERFORM 8000-PRINT-A-LINE                                PPCTR45
019931         INITIALIZE RPT45-GTN-ARRAY                               PPCTR45
019932         MOVE 1 TO LINK-ENTRY                                     PPCTR45
019950     END-IF.                                                      PPCTR45
019960     MOVE NGL-GTN TO RPT45-GTN (LINK-ENTRY).                      PPCTR45
020110                                                                  PPCTR45
020200     PERFORM 7400-FETCH-NGL.                                      PPCTR45
020700                                                                  PPCTR45
020800 6000-PAGE-HEADER            SECTION.                             PPCTR45
020810**************************************************************/   PPCTR45
020820*    PRINT THE REPORT HEADER                                 */   PPCTR45
020821*    CPPDXCHR ADDS TO LINE-COUNT FOR ITS OWN ACTIONS         */   PPCTR45
020830**************************************************************/   PPCTR45
020900                                                                  PPCTR45
021000     COPY CPPDXCHR.                                               PPCTR45
021100     MOVE 2 TO NO-OF-LINES.                                       PPCTR45
021200     MOVE RPT45-HD1 TO PRINT-REC.                                 PPCTR45
021300     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR45
021310     MOVE 1 TO NO-OF-LINES.                                       PPCTR45
021400     MOVE RPT45-HD2 TO PRINT-REC.                                 PPCTR45
021500     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR45
021600     MOVE 1 TO NO-OF-LINES.                                       PPCTR45
021601     MOVE RPT45-HD3 TO PRINT-REC.                                 PPCTR45
021602     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR45
021700     ADD 6 TO LINE-COUNT.                                         PPCTR45
022200                                                                  PPCTR45
022300 7000-OPEN-NGO               SECTION.                             PPCTR45
022400                                                                  PPCTR45
022500     MOVE 'OPEN NGO CURSOR' TO DB2MSG-TAG.                        PPCTR45
022815     EXEC SQL                                                     PPCTR45
022816          OPEN NGO_ROW                                            PPCTR45
022817     END-EXEC.                                                    PPCTR45
023200                                                                  PPCTR45
023300 7100-FETCH-NGO              SECTION.                             PPCTR45
023400                                                                  PPCTR45
023510     MOVE 'FETCH NGO ROW' TO DB2MSG-TAG.                          PPCTR45
023520     EXEC SQL                                                     PPCTR45
023530         FETCH NGO_ROW                                            PPCTR45
023531         INTO                                                     PPCTR45
023532             :NGO-LINK                                            PPCTR45
023533            ,:NGO-PRENOTE-IND                                     PPCTR45
023534            ,:NGO-PRENOTE-CYC-TP                                  PPCTR45
023535            ,:NGO-PRENOTE-RTE-NB                                  PPCTR45
023536            ,:NGO-PRENOTE-CHK-DG                                  PPCTR45
023537            ,:NGO-PRENOTE-BNK-AC                                  PPCTR45
023538            ,:NGO-PRENOTE-END-DT                                  PPCTR45
023539            ,:NGO-PRENOTE-IDV-ID                                  PPCTR45
023540            ,:NGO-ORG-NAME                                        PPCTR45
023541            ,:NGO-ORG-GROUP-SORT                                  PPCTR45
023542            ,:NGO-DEPOSIT-RTE-NB                                  PPCTR45
023543            ,:NGO-DEPOSIT-CHK-DG                                  PPCTR45
023544            ,:NGO-DEPOSIT-BNK-AC                                  PPCTR45
023545            ,:NGO-DEP-END-DT-FST                                  PPCTR45
023546            ,:NGO-DEP-END-DT-LST                                  PPCTR45
023547            ,:NGO-PROC-NEG-IND                                    PPCTR45
023548            ,:NGO-PROC-ADDN-IND                                   PPCTR45
023549            ,:NGO-PROC-FRMT-RTE                                   PPCTR45
023550            ,:NGO-LAST-ACTION                                     PPCTR45
023551            ,:NGO-LAST-ACTION-DT                                  PPCTR45
023552     END-EXEC.                                                    PPCTR45
023553                                                                  PPCTR45
023556     IF SQLCODE NOT = ZERO                                        PPCTR45
023557        SET EOF-CURSOR-O TO TRUE                                  PPCTR45
023570     END-IF.                                                      PPCTR45
025800                                                                  PPCTR45
025900 7200-CLOSE-NGO              SECTION.                             PPCTR45
026000                                                                  PPCTR45
026420     MOVE 'CLOSE NGO CURSOR' TO DB2MSG-TAG.                       PPCTR45
026427     EXEC SQL                                                     PPCTR45
026428          CLOSE NGO_ROW                                           PPCTR45
026429     END-EXEC.                                                    PPCTR45
026793                                                                  PPCTR45
026794 7300-OPEN-NGL               SECTION.                             PPCTR45
026795                                                                  PPCTR45
026796     MOVE 'OPEN NGL CURSOR' TO DB2MSG-TAG.                        PPCTR45
026797     EXEC SQL                                                     PPCTR45
026798          OPEN NGL_ROW                                            PPCTR45
026799     END-EXEC.                                                    PPCTR45
026800                                                                  PPCTR45
026900 7400-FETCH-NGL              SECTION.                             PPCTR45
026910                                                                  PPCTR45
026920     MOVE 'FETCH NGL ROW' TO DB2MSG-TAG.                          PPCTR45
026930     EXEC SQL                                                     PPCTR45
026940         FETCH NGL_ROW                                            PPCTR45
026950         INTO                                                     PPCTR45
026960             :NGL-LINK                                            PPCTR45
026970            ,:NGL-GTN                                             PPCTR45
027007     END-EXEC.                                                    PPCTR45
027008                                                                  PPCTR45
027009     IF SQLCODE NOT = ZERO                                        PPCTR45
027010        SET EOF-CURSOR-L TO TRUE                                  PPCTR45
027011     END-IF.                                                      PPCTR45
027012                                                                  PPCTR45
027013 7500-CLOSE-NGL              SECTION.                             PPCTR45
027014                                                                  PPCTR45
027015     MOVE 'CLOSE NGL CURSOR' TO DB2MSG-TAG.                       PPCTR45
027016     EXEC SQL                                                     PPCTR45
027017          CLOSE NGL_ROW                                           PPCTR45
027018     END-EXEC.                                                    PPCTR45
027019                                                                  PPCTR45
027020 8000-PRINT-A-LINE           SECTION.                             PPCTR45
027100**************************************************************/   PPCTR45
027110*    PRINT A DETAIL LINE OF THE REPORT                       */   PPCTR45
027120**************************************************************/   PPCTR45
027150                                                                  PPCTR45
027200     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR45
027300     MOVE SPACES TO PRINT-REC.                                    PPCTR45
027400     ADD NO-OF-LINES TO LINE-COUNT.                               PPCTR45
027500     IF LINE-COUNT > 55                                           PPCTR45
027600        PERFORM 6000-PAGE-HEADER                                  PPCTR45
027700     END-IF.                                                      PPCTR45
027800     MOVE 1 TO NO-OF-LINES.                                       PPCTR45
027900                                                                  PPCTR45
028000 8999-PRINT-EXIT.                                                 PPCTR45
028100     EXIT.                                                        PPCTR45
028210                                                                  PPCTR45
028220 90000-DATE-CONVERSION    SECTION.                                PPCTR45
028230                                                                  PPCTR45
028240     COPY CPPDXDC3.                                               PPCTR45
028300 9100-PROGRAM-TERMINATION    SECTION.                             PPCTR45
028400                                                                  PPCTR45
028500     CLOSE REPORT-PRINT-FILE.                                     PPCTR45
028900                                                                  PPCTR45
029000*999999-SQL-ERROR.                                                PPCTR45
029010**************************************************************/   PPCTR45
029020*    NEGATIVE SQL ERRORS                                     */   PPCTR45
029030**************************************************************/   PPCTR45
029100     EXEC SQL                                                     PPCTR45
029200        INCLUDE CPPDXP99                                          PPCTR45
029300     END-EXEC.                                                    PPCTR45
029400     MOVE '99' TO CTPI-RETURN-CODE.                               PPCTR45
029500     GO TO 0999-END.                                              PPCTR45
