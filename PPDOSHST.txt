000010**************************************************************/   30871230
000020*  PROGRAM: PPDOSHST                                         */   30871230
000030*  RELEASE: ___1230______ SERVICE REQUEST(S): _____3087____  */   30871230
000040*  NAME:___SRS___________ MODIFICATION DATE:  ___03/08/99__  */   30871230
000050*  DESCRIPTION:                                              */   30871230
000060*  REPLACE MESSAGE UTILITY CALLS WITH DB2 VERSION.           */   30871230
000070**************************************************************/   30871230
000100**************************************************************/   36330704
000200*  PROGRAM: PPDOSHST                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME __B.I.T._______   CREATION DATE     ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*                                                            */   36330704
000700*      PPDOSHST IS A NEW PROGRAM FOR RELEASE   # 0704        */   36330704
000800*                                                            */   36330704
000900**************************************************************/   36330704
001000 IDENTIFICATION DIVISION.                                         PPDOSHST
001100 PROGRAM-ID. PPDOSHST.                                            PPDOSHST
001200 AUTHOR. BUSINESS INFORMATION TECH, INC. DWH/BIT.                 PPDOSHST
001300 INSTALLATION. UNIVERSITY OF CALIFORNIA, STANDARD SYSTEM          PPDOSHST
001400*REMARKS.                                                         PPDOSHST
001500*-----------------------------------------------------------------PPDOSHST
001600*  THIS PROGRAM RECEIVES A CALL FROM PPP741 TO CAPTURE CHANGES    PPDOSHST
001700*  MADE ON THE PCD TABLE TO THE CDB DATABASE.                     PPDOSHST
001800*                                                                 PPDOSHST
001900*  THE FOLLOWING CODES ARE RETURN CODES TO PASS BACK THROUGH TO   PPDOSHST
002000*  THE CALLING PROGRAM IN IHDB-ERROR:                             PPDOSHST
002100*                         00  PROCESSING SUCCESSFUL               PPDOSHST
002200*                         02  WARNING MESSAGE SENT                PPDOSHST
002300*                         03  ERROR ON PCD READ                   PPDOSHST
002400*                         04  SYSTEMS MESSAGE SENT                PPDOSHST
002500*  AN ERROR CODE > 2 SHOULD ABEND THE PROCESS                     PPDOSHST
002600*-----------------------------------------------------------------PPDOSHST
002700 DATE-WRITTEN.  04/10/92.  VERSION 1.0.                           PPDOSHST
002800 DATE-COMPILED.                                                   PPDOSHST
002900     SKIP3                                                        PPDOSHST
003000 ENVIRONMENT DIVISION.                                            PPDOSHST
003100 CONFIGURATION SECTION.                                           PPDOSHST
003200 SOURCE-COMPUTER.  COPY CPOTXUCS.                                 PPDOSHST
003300 OBJECT-COMPUTER. COPY CPOTXOBJ.                                  PPDOSHST
003400 SPECIAL-NAMES.                                                   PPDOSHST
003500     EJECT                                                        PPDOSHST
003600 INPUT-OUTPUT SECTION.                                            PPDOSHST
003700 FILE-CONTROL.                                                    PPDOSHST
003800*                                                                 PPDOSHST
003900 DATA DIVISION.                                                   PPDOSHST
004000 FILE SECTION.                                                    PPDOSHST
004100*                                                                 PPDOSHST
004200     EJECT                                                        PPDOSHST
004300******************************************************************PPDOSHST
004400 WORKING-STORAGE SECTION.                                         PPDOSHST
004500******************************************************************PPDOSHST
004600 77  I                           PICTURE S9(5)   COMP-3.          PPDOSHST
004700*                                                                 PPDOSHST
004800*                                                                 PPDOSHST
004900 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPDOSHST'.       PPDOSHST
005000*                                                                 PPDOSHST
005100*-----------------------------------------------------------------PPDOSHST
005200*    MISC WORK DEFINITIONS                                        PPDOSHST
005300*-----------------------------------------------------------------PPDOSHST
005400 01  WORK-SWITCHES.                                               PPDOSHST
005500     03  CDB-EOF-SW              PIC X     VALUE 'N'.             PPDOSHST
005600         88  CDB-EOF                       VALUE 'Y'.             PPDOSHST
005700         88  CDB-NOT-EOF                   VALUE 'N'.             PPDOSHST
005800     03  PCD-EOF-SW              PIC X     VALUE 'N'.             PPDOSHST
005900         88  PCD-EOF                       VALUE 'Y'.             PPDOSHST
006000         88  PCD-NOT-EOF                   VALUE 'N'.             PPDOSHST
006100     03  CDB-ROW-OPEN-SW         PIC X     VALUE 'N'.             PPDOSHST
006200         88  CDB-ROW-OPEN                  VALUE 'Y'.             PPDOSHST
006300         88  CDB-ROW-NOTOPEN               VALUE 'N'.             PPDOSHST
006400*                                                                 PPDOSHST
006500 01  WORK-AREA.                                                   PPDOSHST
006600     05  RECORD-COUNTS.                                           PPDOSHST
006700         10  TABLE-ADDS           PIC S9(6) COMP-3 VALUE ZEROS.   PPDOSHST
006800         10  TABLE-DELS           PIC S9(6) COMP-3 VALUE ZEROS.   PPDOSHST
006900         10  TABLE-CHGS           PIC S9(6) COMP-3 VALUE ZEROS.   PPDOSHST
007000     05  RPT-LINE.                                                PPDOSHST
007100         10  FILLER               PIC X(17) VALUE                 PPDOSHST
007200             'TABLE DOS - ADDS:'.                                 PPDOSHST
007300         10  RPT-AMOUNT1          PIC ZZ,ZZ9.                     PPDOSHST
007400         10  FILLER               PIC X(8)  VALUE                 PPDOSHST
007500             '   DELS:'.                                          PPDOSHST
007600         10  RPT-AMOUNT2          PIC ZZ,ZZ9.                     PPDOSHST
007700         10  FILLER               PIC X(8)  VALUE                 PPDOSHST
007800             '   CHGS:'.                                          PPDOSHST
007900         10  RPT-AMOUNT3          PIC ZZ,ZZ9.                     PPDOSHST
008000*                                                                 PPDOSHST
008100     EJECT                                                        PPDOSHST
008200*-----------------------------------------------------------------PPDOSHST
008300*    INDIVIDUAL CDB TABLE AREA DEFINITIONS                        PPDOSHST
008400*-----------------------------------------------------------------PPDOSHST
008500 01  COMPARE-AREA.                                                PPDOSHST
008600     05  CDB-REC.                                                 PPDOSHST
008700         10  CDB-KEY.                                             PPDOSHST
008800             15 DOS-SEQUENCE      PIC X(3).                       PPDOSHST
008900             15 DOS-EARNINGS-TYPE PIC X(3).                       PPDOSHST
009000         10  CDB-KEY2             PIC X(18).                      PPDOSHST
009100         10  CDB-DATA             PIC X(500).                     PPDOSHST
009200     05  HOLD-CDB-REC.                                            PPDOSHST
009300         10  HOLD-CDB-KEY.                                        PPDOSHST
009400             15 DOS-SEQUENCE      PIC X(3).                       PPDOSHST
009500             15 DOS-EARNINGS-TYPE PIC X(3).                       PPDOSHST
009600         10  HOLD-CDB-KEY2        PIC X(18).                      PPDOSHST
009700         10  HOLD-CDB-DATA        PIC X(500).                     PPDOSHST
009800     05  PCD-REC.                                                 PPDOSHST
009900         10  PCD-KEY.                                             PPDOSHST
010000             15 DOS-SEQUENCE      PIC X(3).                       PPDOSHST
010100             15 DOS-EARNINGS-TYPE PIC X(3).                       PPDOSHST
010200         10  PCD-DATA             PIC X(500).                     PPDOSHST
010300     EJECT                                                        PPDOSHST
010400*-----------------------------------------------------------------PPDOSHST
010500*    CDB TABLE AREA                                               PPDOSHST
010600*-----------------------------------------------------------------PPDOSHST
010700 01  PPPCDB-TABLE.                                                PPDOSHST
010800     05  PPPCDB-ROW              OCCURS 1000 INDEXED BY ICDB.     PPDOSHST
010900                                 COPY CPWSDOSH.                   PPDOSHST
011000 01  PPPCDB-TBL-MAX              PIC 9(4)    VALUE 1000.          PPDOSHST
011100 01  PPPCDB-TBL-LOADED           PIC 9(4)    VALUE 0000.          PPDOSHST
011200*                                                                 PPDOSHST
011300     EJECT                                                        PPDOSHST
011400*-----------------------------------------------------------------PPDOSHST
011500*    MESSAGE WORKING STORAGE AREA                                 PPDOSHST
011600*-----------------------------------------------------------------PPDOSHST
011700     EJECT                                                        PPDOSHST
011800 01  MSSG-INTERFACE.             COPY CPWSXMIF.                   PPDOSHST
011810 01  CALLED-ROUTINES.            COPY CPWSXRTN.                   30871230
011900*                                                                 PPDOSHST
012000     EJECT                                                        PPDOSHST
012100 01  MSSG-LITERALS.                                               PPDOSHST
012200     03  M74109                  PIC 9(5) VALUE 74109.            PPDOSHST
012300     03  M74116                  PIC 9(5) VALUE 74116.            PPDOSHST
012400     03  M74117                  PIC 9(5) VALUE 74117.            PPDOSHST
012500     03  M74118                  PIC 9(5) VALUE 74118.            PPDOSHST
012600     03  M74120                  PIC 9(5) VALUE 74120.            PPDOSHST
012700*                                                                 PPDOSHST
012800     EJECT                                                        PPDOSHST
012900*-----------------------------------------------------------------PPDOSHST
013000*    INTERFACE AREA                                               PPDOSHST
013100*-----------------------------------------------------------------PPDOSHST
013200 01  XTIF-INTERFACE.             COPY CPWSXTIF                    PPDOSHST
013300                                 REPLACING ==:TAG:== BY ==XTIF==. PPDOSHST
013400     EJECT                                                        PPDOSHST
013500 01  XPIF-INTERFACE.             COPY CPWSXPIF                    PPDOSHST
013600                                 REPLACING ==:TAG:== BY ==XPIF==. PPDOSHST
013700     EJECT                                                        PPDOSHST
013800*------------------------------------------------------------*    PPDOSHST
013900*          SQL - WORKING STORAGE                             *    PPDOSHST
014000*------------------------------------------------------------*    PPDOSHST
014100 01  PPDB2MSG-INTERFACE.         COPY CPLNKDB2.                   PPDOSHST
014200     EJECT                                                        PPDOSHST
014300 01  PPPDOSH-ROW  EXTERNAL.                                       PPDOSHST
014400     EXEC SQL                                                     PPDOSHST
014500         INCLUDE PPPVDOSH                                         PPDOSHST
014600     END-EXEC.                                                    PPDOSHST
014700     EXEC SQL                                                     PPDOSHST
014800         DECLARE PPPCDB_ROW CURSOR FOR                            PPDOSHST
014900         SELECT * FROM PPPVDOSH_DOSH                              PPDOSHST
015000         ORDER BY DOS_EARNINGS_TYPE,                              PPDOSHST
015100                  DOS_SEQUENCE,                                   PPDOSHST
015200                  SYSTEM_ENTRY_DATE DESC,                         PPDOSHST
015300                  SYSTEM_ENTRY_TIME DESC                          PPDOSHST
015400     END-EXEC.                                                    PPDOSHST
015500 01  PPPDOS-ROW   EXTERNAL.                                       PPDOSHST
015600     EXEC SQL                                                     PPDOSHST
015700         INCLUDE PPPVZDOS                                         PPDOSHST
015800     END-EXEC.                                                    PPDOSHST
015900     EJECT                                                        PPDOSHST
016000     EXEC SQL                                                     PPDOSHST
016100         INCLUDE SQLCA                                            PPDOSHST
016200     END-EXEC.                                                    PPDOSHST
016300*                                                                 PPDOSHST
016400 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPDOSHST
016500*                                                                 PPDOSHST
016600     EJECT                                                        PPDOSHST
016700 LINKAGE SECTION.                                                 PPDOSHST
016800*----------------------------------------------------------------*PPDOSHST
016900*    HDB INTERFACE AREA FROM CALLING PROGRAM                      PPDOSHST
017000*----------------------------------------------------------------*PPDOSHST
017100 01  IHDB-INTERFACE.             COPY CPWSIHDB.                   PPDOSHST
017200     EJECT                                                        PPDOSHST
017300*----------------------------------------------------------------*PPDOSHST
017400 PROCEDURE DIVISION USING IHDB-INTERFACE.                         PPDOSHST
017500*----------------------------------------------------------------*PPDOSHST
017600******************************************************************PPDOSHST
017700*----------------------------------------------------------------*PPDOSHST
017800*    MAIN LOOP                                                    PPDOSHST
017900*----------------------------------------------------------------*PPDOSHST
018000*                                                                 PPDOSHST
018100     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPDOSHST
018200     COPY  CPPDXWHC.                                              PPDOSHST
018300*                                                                 PPDOSHST
018400     EXEC SQL                                                     PPDOSHST
018500        INCLUDE CPPDXE99                                          PPDOSHST
018600     END-EXEC.                                                    PPDOSHST
018700*                                                                 PPDOSHST
018800     PERFORM 0100-INITIALIZATION.                                 PPDOSHST
018900*----------------------------------------------------------------*PPDOSHST
019000*    GET FIRST RECORD FROM BOTH TABLES                            PPDOSHST
019100*----------------------------------------------------------------*PPDOSHST
019200     MOVE ZEROS                  TO I.                            PPDOSHST
019300     SET CDB-NOT-EOF             TO TRUE.                         PPDOSHST
019400     SET PCD-NOT-EOF             TO TRUE.                         PPDOSHST
019500     PERFORM 1000-GET-NEXT-CDB.                                   PPDOSHST
019600     PERFORM 2000-GET-NEXT-PCD.                                   PPDOSHST
019700*----------------------------------------------------------------*PPDOSHST
019800*    PERFORM COMPARE UNTIL AT THE END OF BOTH TABLES              PPDOSHST
019900*----------------------------------------------------------------*PPDOSHST
020000     PERFORM 3000-COMPARE-ROW                                     PPDOSHST
020100             UNTIL PCD-EOF                                        PPDOSHST
020200             AND   CDB-EOF.                                       PPDOSHST
020300*                                                                 PPDOSHST
020400*----------------------------------------------------------------*PPDOSHST
020500*    WRITE STATISTICS TO MESSAGE FILE                             PPDOSHST
020600*----------------------------------------------------------------*PPDOSHST
020700     PERFORM 4000-WRITE-STATS.                                    PPDOSHST
020800*----------------------------------------------------------------*PPDOSHST
020900     GOBACK.                                                      PPDOSHST
021000*----------------------------------------------------------------*PPDOSHST
021100*                                                                 PPDOSHST
021200     EJECT                                                        PPDOSHST
021300*----------------------------------------------------------------*PPDOSHST
021400 0100-INITIALIZATION  SECTION.                                    PPDOSHST
021500*----------------------------------------------------------------*PPDOSHST
021600******************************************************************PPDOSHST
021700*----------------------------------------------------------------*PPDOSHST
021800*    IF PROCESSING AS INITIALIZATION THE CDB INTERNAL TABLE IS    PPDOSHST
021900*    NOT LOADED, BUT THE CDB-EOF FLAG IS SET TO ON SO THAT ALL    PPDOSHST
022000*    REMAINING (MEANING ALL) PCD ROWS WILL BE WRITTEN OUT TO THE  PPDOSHST
022100*    CDB TABLE.                                                   PPDOSHST
022200*----------------------------------------------------------------*PPDOSHST
022300     MOVE ZEROS                    TO TABLE-ADDS                  PPDOSHST
022400                                      TABLE-DELS                  PPDOSHST
022500                                      TABLE-CHGS.                 PPDOSHST
022600*                                                                 PPDOSHST
022700     MOVE ZEROS                    TO PPPCDB-TBL-LOADED.          PPDOSHST
022800     MOVE ZEROS                    TO I.                          PPDOSHST
022900     MOVE SPACES                   TO HOLD-CDB-KEY.               PPDOSHST
023000     SET CDB-NOT-EOF               TO TRUE.                       PPDOSHST
023100     PERFORM 0200-LOAD-CDB-TABLE                                  PPDOSHST
023200             UNTIL CDB-EOF.                                       PPDOSHST
023300*                                                                 PPDOSHST
023400 0100-EXIT.    EXIT.                                              PPDOSHST
023500*                                                                 PPDOSHST
023600*                                                                 PPDOSHST
023700     EJECT                                                        PPDOSHST
023800*----------------------------------------------------------------*PPDOSHST
023900 0200-LOAD-CDB-TABLE  SECTION.                                    PPDOSHST
024000*----------------------------------------------------------------*PPDOSHST
024100******************************************************************PPDOSHST
024200     IF CDB-ROW-NOTOPEN                                           PPDOSHST
024300        MOVE 'OPEN PPPCDB CURSOR'  TO DB2MSG-TAG                  PPDOSHST
024400        EXEC SQL                                                  PPDOSHST
024500           OPEN PPPCDB_ROW                                        PPDOSHST
024600        END-EXEC                                                  PPDOSHST
024700        SET CDB-ROW-OPEN           TO TRUE                        PPDOSHST
024800     END-IF.                                                      PPDOSHST
024900     MOVE 'FETCH PPPCDB ROW'       TO DB2MSG-TAG.                 PPDOSHST
025000     EXEC SQL                                                     PPDOSHST
025100        FETCH PPPCDB_ROW INTO :PPPDOSH-ROW                        PPDOSHST
025200     END-EXEC.                                                    PPDOSHST
025300     IF SQLCODE = +100                                            PPDOSHST
025400        SET CDB-EOF                TO TRUE                        PPDOSHST
025500        MOVE 'CLOSE PPPCDB CURSOR' TO DB2MSG-TAG                  PPDOSHST
025600        EXEC SQL                                                  PPDOSHST
025700           CLOSE PPPCDB_ROW                                       PPDOSHST
025800        END-EXEC                                                  PPDOSHST
025900        SET CDB-ROW-NOTOPEN        TO TRUE                        PPDOSHST
026000     ELSE                                                         PPDOSHST
026100        MOVE PPPDOSH-ROW           TO CDB-REC                     PPDOSHST
026200        IF HOLD-CDB-KEY = CDB-KEY                                 PPDOSHST
026300           NEXT SENTENCE                                          PPDOSHST
026400        ELSE                                                      PPDOSHST
026500           IF DOS-LAST-ACTION OF PPPDOSH-ROW NOT = '*'            PPDOSHST
026600              COMPUTE I = I + 1                                   PPDOSHST
026700              IF I > PPPCDB-TBL-MAX                               PPDOSHST
026800                 MOVE M74120       TO XMSG-NUMBER                 PPDOSHST
026900*****************PERFORM 8100-CALL-PPMSGUTL                       30871230
026910                 PERFORM 8100-CALL-MESSAGE-UTILITY                30871230
027000                 SET IHDB-SYSTEMS  TO TRUE                        PPDOSHST
027100                 GOBACK                                           PPDOSHST
027200              END-IF                                              PPDOSHST
027300              MOVE CDB-REC         TO PPPCDB-ROW(I)               PPDOSHST
027400              COMPUTE PPPCDB-TBL-LOADED = I                       PPDOSHST
027500           END-IF                                                 PPDOSHST
027600           MOVE CDB-KEY            TO HOLD-CDB-KEY                PPDOSHST
027700        END-IF                                                    PPDOSHST
027800     END-IF.                                                      PPDOSHST
027900*                                                                 PPDOSHST
028000 0200-EXIT.    EXIT.                                              PPDOSHST
028100*                                                                 PPDOSHST
028200*                                                                 PPDOSHST
028300     EJECT                                                        PPDOSHST
028400*----------------------------------------------------------------*PPDOSHST
028500 1000-GET-NEXT-CDB  SECTION.                                      PPDOSHST
028600*----------------------------------------------------------------*PPDOSHST
028700******************************************************************PPDOSHST
028800     COMPUTE I = I + 1.                                           PPDOSHST
028900     IF I > PPPCDB-TBL-LOADED                                     PPDOSHST
029000        SET CDB-EOF              TO TRUE                          PPDOSHST
029100        MOVE HIGH-VALUES         TO CDB-KEY                       PPDOSHST
029200        MOVE SPACES              TO CDB-DATA                      PPDOSHST
029300     ELSE                                                         PPDOSHST
029400        MOVE PPPCDB-ROW(I)       TO CDB-REC                       PPDOSHST
029500     END-IF.                                                      PPDOSHST
029600*                                                                 PPDOSHST
029700 1000-EXIT.    EXIT.                                              PPDOSHST
029800*                                                                 PPDOSHST
029900*                                                                 PPDOSHST
030000     EJECT                                                        PPDOSHST
030100*----------------------------------------------------------------*PPDOSHST
030200 2000-GET-NEXT-PCD  SECTION.                                      PPDOSHST
030300*----------------------------------------------------------------*PPDOSHST
030400******************************************************************PPDOSHST
030500     SET XPIF-SEQ-REQUEST        TO TRUE.                         PPDOSHST
030600     MOVE 'DOS'                  TO XPIF-TABLE-REQUEST.           PPDOSHST
030700     CALL 'PPPCDFET' USING XPIF-INTERFACE.                        PPDOSHST
030800     IF XPIF-OK                                                   PPDOSHST
030900        MOVE PPPDOS-ROW          TO PCD-REC                       PPDOSHST
031000     ELSE                                                         PPDOSHST
031100        IF XPIF-INVALID-KEY                                       PPDOSHST
031200           SET PCD-EOF           TO TRUE                          PPDOSHST
031300           MOVE HIGH-VALUES      TO PCD-KEY                       PPDOSHST
031400           MOVE SPACES           TO PCD-DATA                      PPDOSHST
031500        ELSE                                                      PPDOSHST
031600           MOVE XPIF-RETURN-STATUS  TO XMSG-DATA                  PPDOSHST
031700           MOVE M74117           TO XMSG-NUMBER                   PPDOSHST
031800***********PERFORM 8100-CALL-PPMSGUTL                             30871230
031810           PERFORM 8100-CALL-MESSAGE-UTILITY                      30871230
031900           SET IHDB-BAD-RETURN-PCD TO TRUE                        PPDOSHST
032000           GOBACK                                                 PPDOSHST
032100        END-IF                                                    PPDOSHST
032200     END-IF.                                                      PPDOSHST
032300*                                                                 PPDOSHST
032400 2000-EXIT.    EXIT.                                              PPDOSHST
032500*                                                                 PPDOSHST
032600*                                                                 PPDOSHST
032700*                                                                 PPDOSHST
032800     EJECT                                                        PPDOSHST
032900*----------------------------------------------------------------*PPDOSHST
033000 3000-COMPARE-ROW  SECTION.                                       PPDOSHST
033100*----------------------------------------------------------------*PPDOSHST
033200******************************************************************PPDOSHST
033300     IF DOS-EARNINGS-TYPE OF CDB-KEY >                            PPDOSHST
033400                             DOS-EARNINGS-TYPE OF PCD-KEY         PPDOSHST
033500        MOVE PCD-KEY             TO HOLD-CDB-KEY                  PPDOSHST
033600        MOVE PCD-DATA            TO HOLD-CDB-DATA                 PPDOSHST
033700        MOVE HOLD-CDB-REC        TO PPPDOSH-ROW                   PPDOSHST
033800        PERFORM 3100-ADD-CDB-ROW                                  PPDOSHST
033900        PERFORM 2000-GET-NEXT-PCD                                 PPDOSHST
034000        COMPUTE TABLE-ADDS = TABLE-ADDS + 1                       PPDOSHST
034100     ELSE                                                         PPDOSHST
034200        IF DOS-EARNINGS-TYPE OF CDB-KEY <                         PPDOSHST
034300                                DOS-EARNINGS-TYPE OF PCD-KEY      PPDOSHST
034400           PERFORM 3200-INIT-CDB-ROW                              PPDOSHST
034500           MOVE CDB-KEY          TO HOLD-CDB-KEY                  PPDOSHST
034600           MOVE HOLD-CDB-REC     TO PPPDOSH-ROW                   PPDOSHST
034700           PERFORM 3100-ADD-CDB-ROW                               PPDOSHST
034800           PERFORM 1000-GET-NEXT-CDB                              PPDOSHST
034900           COMPUTE TABLE-DELS = TABLE-DELS + 1                    PPDOSHST
035000        ELSE                                                      PPDOSHST
035100           IF CDB-DATA NOT = PCD-DATA                             PPDOSHST
035200              MOVE PCD-KEY       TO HOLD-CDB-KEY                  PPDOSHST
035300              MOVE PCD-DATA      TO HOLD-CDB-DATA                 PPDOSHST
035400              MOVE HOLD-CDB-REC  TO PPPDOSH-ROW                   PPDOSHST
035500              PERFORM 3100-ADD-CDB-ROW                            PPDOSHST
035600              COMPUTE TABLE-CHGS = TABLE-CHGS + 1                 PPDOSHST
035700           END-IF                                                 PPDOSHST
035800           PERFORM 1000-GET-NEXT-CDB                              PPDOSHST
035900           PERFORM 2000-GET-NEXT-PCD                              PPDOSHST
036000        END-IF                                                    PPDOSHST
036100     END-IF.                                                      PPDOSHST
036200*                                                                 PPDOSHST
036300 3000-EXIT.    EXIT.                                              PPDOSHST
036400*                                                                 PPDOSHST
036500*                                                                 PPDOSHST
036600     EJECT                                                        PPDOSHST
036700*----------------------------------------------------------------*PPDOSHST
036800 3100-ADD-CDB-ROW  SECTION.                                       PPDOSHST
036900*----------------------------------------------------------------*PPDOSHST
037000******************************************************************PPDOSHST
037100*----------------------------------------------------------------*PPDOSHST
037200*    SETUP AND CALL CDBFET TO ADD THE NEW CDB RECORD              PPDOSHST
037300*----------------------------------------------------------------*PPDOSHST
037400     SET XTIF-INSERT-REQUEST     TO TRUE.                         PPDOSHST
037500     MOVE 'DOS'                  TO XTIF-TABLE-REQUEST.           PPDOSHST
037600     MOVE IHDB-PROCESS-DATE      TO XTIF-DATE                     PPDOSHST
037700                                 SYSTEM-ENTRY-DATE OF PPPDOSH-ROW.PPDOSHST
037800     MOVE IHDB-PROCESS-TIME      TO XTIF-TIME                     PPDOSHST
037900                                 SYSTEM-ENTRY-TIME OF PPPDOSH-ROW.PPDOSHST
038000     MOVE HOLD-CDB-KEY           TO XTIF-PPPDOS-KEY.              PPDOSHST
038100     CALL 'PPCDBFET' USING XTIF-INTERFACE.                        PPDOSHST
038200     IF XTIF-OK                                                   PPDOSHST
038300        NEXT SENTENCE                                             PPDOSHST
038400     ELSE                                                         PPDOSHST
038500        MOVE XTIF-RETURN-STATUS  TO XMSG-DATA                     PPDOSHST
038600        MOVE M74116              TO XMSG-NUMBER                   PPDOSHST
038700********PERFORM 8100-CALL-PPMSGUTL                                30871230
038710        PERFORM 8100-CALL-MESSAGE-UTILITY                         30871230
038800        SET IHDB-SYSTEMS         TO TRUE                          PPDOSHST
038900        GOBACK                                                    PPDOSHST
039000     END-IF.                                                      PPDOSHST
039100*                                                                 PPDOSHST
039200 3100-EXIT.    EXIT.                                              PPDOSHST
039300*                                                                 PPDOSHST
039400*                                                                 PPDOSHST
039500     EJECT                                                        PPDOSHST
039600*----------------------------------------------------------------*PPDOSHST
039700 3200-INIT-CDB-ROW  SECTION.                                      PPDOSHST
039800*----------------------------------------------------------------*PPDOSHST
039900******************************************************************PPDOSHST
040000     INITIALIZE PPPDOSH-ROW.                                      PPDOSHST
040100     MOVE '0001-01-01' TO DOS-LAST-ACTION-DT OF PPPDOSH-ROW.      PPDOSHST
040200     MOVE '*'          TO DOS-LAST-ACTION    OF PPPDOSH-ROW.      PPDOSHST
040300*                                                                 PPDOSHST
040400     MOVE PPPDOSH-ROW  TO HOLD-CDB-REC.                           PPDOSHST
040500*                                                                 PPDOSHST
040600 3200-EXIT.    EXIT.                                              PPDOSHST
040700*                                                                 PPDOSHST
040800*                                                                 PPDOSHST
040900     EJECT                                                        PPDOSHST
041000*----------------------------------------------------------------*PPDOSHST
041100 4000-WRITE-STATS  SECTION.                                       PPDOSHST
041200*----------------------------------------------------------------*PPDOSHST
041300******************************************************************PPDOSHST
041400     COMPUTE IHDB-LOADED = TABLE-ADDS + TABLE-DELS + TABLE-CHGS.  PPDOSHST
041500     MOVE TABLE-ADDS             TO RPT-AMOUNT1.                  PPDOSHST
041600     MOVE TABLE-DELS             TO RPT-AMOUNT2.                  PPDOSHST
041700     MOVE TABLE-CHGS             TO RPT-AMOUNT3.                  PPDOSHST
041800     MOVE RPT-LINE               TO XMSG-DATA.                    PPDOSHST
041900     MOVE M74118                 TO XMSG-NUMBER.                  PPDOSHST
042000*****PERFORM 8100-CALL-PPMSGUTL.                                  30871230
042010     PERFORM 8100-CALL-MESSAGE-UTILITY.                           30871230
042100*                                                                 PPDOSHST
042200 4000-EXIT.    EXIT.                                              PPDOSHST
042300*                                                                 PPDOSHST
042400*                                                                 PPDOSHST
042500     EJECT                                                        PPDOSHST
042600*----------------------------------------------------------------*PPDOSHST
042700 8100-CALL-MESSAGE-UTILITY SECTION.                               30871230
042710*8100-CALL-PPMSGUTL    SECTION.                                   30871230
042800*----------------------------------------------------------------*PPDOSHST
042900******************************************************************PPDOSHST
043000*----------------------------------------------------------------*PPDOSHST
043100*    THIS SECTION CALLS THE MESSAGE UTILITY PROGRAM TO PROCESS    PPDOSHST
043200*    MESSAGES AND PRODUCE THE OUTPUT REPORT.                      PPDOSHST
043300*----------------------------------------------------------------*PPDOSHST
043400*****CALL 'PPMSGUTL' USING MSSG-INTERFACE.                        30871230
043410     CALL PGM-PPMSGUTL USING MSSG-INTERFACE.                      30871230
043500*                                                                 PPDOSHST
043600     MOVE SPACES                 TO XMSG-TEXT                     PPDOSHST
043700                                    XMSG-DATA                     PPDOSHST
043800                                    XMSG-USER.                    PPDOSHST
043900*                                                                 PPDOSHST
044000 8100-EXIT.    EXIT.                                              PPDOSHST
044100*                                                                 PPDOSHST
044200*                                                                 PPDOSHST
044300*                                                                 PPDOSHST
044400     EJECT                                                        PPDOSHST
044500*----------------------------------------------------------------*PPDOSHST
044600*999999-SQL-ERROR.   SECTION HEADING IS IN CPPDXP99              *PPDOSHST
044700*----------------------------------------------------------------*PPDOSHST
044800******************************************************************PPDOSHST
044900     EXEC SQL                                                     PPDOSHST
045000          INCLUDE CPPDXP99                                        PPDOSHST
045100     END-EXEC.                                                    PPDOSHST
045200     SET IHDB-SYSTEMS            TO TRUE.                         PPDOSHST
045300     MOVE M74109                 TO XMSG-NUMBER                   PPDOSHST
045400     MOVE PPDB2MSG-INTERFACE     TO XMSG-DATA                     PPDOSHST
045500*****PERFORM 8100-CALL-PPMSGUTL                                   30871230
045510     PERFORM 8100-CALL-MESSAGE-UTILITY                            30871230
045600     GOBACK.                                                      PPDOSHST
045700*DUMMY                                                            PPDOSHST
