000008**************************************************************/   48561396
000009*  PROGRAM: PPP565                                           */   48561396
000010*  RELEASE: ___1396______ SERVICE REQUEST(S): ____14856____  */   48561396
000020*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___02/26/02__  */   48561396
000030*  DESCRIPTION:                                              */   48561396
000031*  - Consolidate different Self Billing Statement formats    */   48561396
000032*    into a single common record for FTP purposes.           */   48561396
000080**************************************************************/   48561396
000090 IDENTIFICATION DIVISION.                                         PPP565
000100 PROGRAM-ID. PPP565.                                              PPP565
000200 AUTHOR. PHILLIP THOMPSON, UCOP.                                  PPP565
000300 INSTALLATION. UNIVERSITY OF CALIFORNIA, BASE PAYROLL SYSTEM.     PPP565
000400 DATE-WRITTEN. NOVEMBER 2001.                                     PPP565
00024  DATE-COMPILED.                                                   PPP565
000600******************************************************************PPP565
000700**                                                              **PPP565
000800******************************************************************PPP565
000900     EJECT                                                        PPP565
00026  ENVIRONMENT DIVISION.                                            PPP565
00028  CONFIGURATION SECTION.                                           PPP565
001200                                                                  PPP565
001300 SOURCE-COMPUTER.            COPY CPOTXUCS.                       PPP565
001400 OBJECT-COMPUTER.            COPY CPOTXOBJ.                       PPP565
001500                                                                  PPP565
001600 SPECIAL-NAMES.                                                   PPP565
001700     C01 IS NEW-PAGE.                                             PPP565
001800                                                                  PPP565
00034  INPUT-OUTPUT SECTION.                                            PPP565
00035  FILE-CONTROL.                                                    PPP565
002200     SELECT SBS-FILE                 ASSIGN TO   UT-S-SBSFILE.    PPP565
002210     SELECT SBS-LIFE-FILE            ASSIGN TO   UT-S-SBSLIFE.    PPP565
002220     SELECT SBS-DIS-FILE             ASSIGN TO   UT-S-SBSDIS.     PPP565
002230     SELECT SBS-ADD-FILE             ASSIGN TO   UT-S-SBSADD.     PPP565
002240     SELECT SBS-FTP-FILE             ASSIGN TO   UT-S-SBSFTP.     PPP565
002300                                                                  PPP565
002400 I-O-CONTROL.                                                     PPP565
002500                                                                  PPP565
00059  DATA DIVISION.                                                   PPP565
00061  FILE SECTION.                                                    PPP565
003700                                                                  PPP565
003800 FD  SBS-FILE                                                     PPP565
003900     RECORDING MODE IS F                                          PPP565
004000     LABEL RECORDS ARE STANDARD                                   PPP565
004100     DATA RECORD IS SBS-RECORD.                                   PPP565
004200 01  SBS-RECORD                  PIC X(71).                       PPP565
004210                                                                  PPP565
004220 FD  SBS-LIFE-FILE                                                PPP565
004230     RECORDING MODE IS F                                          PPP565
004240     LABEL RECORDS ARE STANDARD                                   PPP565
004250     DATA RECORD IS SBS-LIFE-RECORD.                              PPP565
004260 01  SBS-LIFE-RECORD             PIC X(120).                      PPP565
004270                                                                  PPP565
004280 FD  SBS-DIS-FILE                                                 PPP565
004290     RECORDING MODE IS F                                          PPP565
004291     LABEL RECORDS ARE STANDARD                                   PPP565
004292     DATA RECORD IS SBS-DIS-RECORD.                               PPP565
004293 01  SBS-DIS-RECORD              PIC X(117).                      PPP565
004294                                                                  PPP565
004295 FD  SBS-ADD-FILE                                                 PPP565
004296     RECORDING MODE IS F                                          PPP565
004297     LABEL RECORDS ARE STANDARD                                   PPP565
004298     DATA RECORD IS SBS-ADD-RECORD.                               PPP565
004299 01  SBS-ADD-RECORD              PIC X(114).                      PPP565
004300                                                                  PPP565
004301 FD  SBS-FTP-FILE                                                 PPP565
004302     RECORDING MODE IS F                                          PPP565
004303     LABEL RECORDS ARE STANDARD                                   PPP565
004304     DATA RECORD IS SBS-FTP-RECORD.                               PPP565
004305 01  SBS-FTP-RECORD              PIC X(153).                      PPP565
004400     EJECT                                                        PPP565
00160  WORKING-STORAGE SECTION.                                         PPP565
004600******************************************************************PPP565
004700*                Working Storage                                 *PPP565
004800******************************************************************PPP565
004900 01  WS-PROGRAM-CONSTANTS.                                        PPP565
005000     05  WS-STND-PGM-ID           PIC X(15)  VALUE                PPP565
005100                                            'PPP565  /030102'.    PPP565
005400 01  WS-PROGRAM-VARIABLES.                                        PPP565
005500     05  WS-SBS-READ              PIC S9(08) VALUE ZERO.          PPP565
005510     05  WS-SBS-LIFE-READ         PIC S9(08) VALUE ZERO.          PPP565
005520     05  WS-SBS-DIS-READ          PIC S9(08) VALUE ZERO.          PPP565
005530     05  WS-SBS-ADD-READ          PIC S9(08) VALUE ZERO.          PPP565
005540     05  WS-SBS-FTP-WRITTEN       PIC S9(08) VALUE ZERO.          PPP565
005600     05  WS-RECORD-COUNT          PIC 99999999.                   PPP565
005700     05  WS-RECORD-COUNT-Z        PIC ZZZZZZZ9.                   PPP565
005800 01  WS-PROGRAM-SWITCHES.                                         PPP565
005900     05  WS-ERROR-SW                PIC X(1) VALUE 'N'.           PPP565
006000         88  FATAL-ERROR                     VALUE 'Y'.           PPP565
006100     05  EOF-SBS-SW                 PIC X(1) VALUE 'N'.           PPP565
006200         88  EOF-SBS                         VALUE 'Y'.           PPP565
006300     05  EOF-SBS-LIFE-SW            PIC X(1) VALUE 'N'.           PPP565
006310         88  EOF-SBS-LIFE                    VALUE 'Y'.           PPP565
006320     05  EOF-SBS-DIS-SW             PIC X(1) VALUE 'N'.           PPP565
006330         88  EOF-SBS-DIS                     VALUE 'Y'.           PPP565
006340     05  EOF-SBS-ADD-SW             PIC X(1) VALUE 'N'.           PPP565
006350         88  EOF-SBS-ADD                     VALUE 'Y'.           PPP565
006400 01  WS-MESSAGES.                                                 PPP565
006500     05  M56501                     PIC X(05) VALUE '56501'.      PPP565
006600     05  M56502                     PIC X(05) VALUE '56502'.      PPP565
006700     05  M56503                     PIC X(05) VALUE '56503'.      PPP565
006800     05  M56504                     PIC X(05) VALUE '56504'.      PPP565
006900     05  M56505                     PIC X(05) VALUE '56505'.      PPP565
007000     05  M56506                     PIC X(05) VALUE '56506'.      PPP565
007100     05  M56507                     PIC X(05) VALUE '56507'.      PPP565
007200     05  M56508                     PIC X(05) VALUE '56508'.      PPP565
007300     05  M56509                     PIC X(05) VALUE '56509'.      PPP565
008300******************************************************************PPP565
008400*    SBS Record Formats.                                         *PPP565
008500******************************************************************PPP565
008600 01  CPWSXSBS.                               COPY CPWSXSBS.       PPP565
009300******************************************************************PPP565
009700*    Message Utlity Interface                                    *PPP565
009800******************************************************************PPP565
009900 01  MSSG-INTERFACE.                         COPY CPWSXMIF.       PPP565
010400******************************************************************PPP565
010500*    Working Storage for common XDC3 date routines.              *PPP565
010600******************************************************************PPP565
010700 01  CPWSXDC3.                               COPY CPWSXDC3.       PPP565
010800******************************************************************PPP565
010900*    Compile timestamp display working storage.                  *PPP565
011000******************************************************************PPP565
011100 01  XWHC-COMPILE-WORK-AREA.                 COPY CPWSXWHC.       PPP565
011200******************************************************************PPP565
011300*  Report Headings.                                              *PPP565
011400******************************************************************PPP565
011500 01  STND-RPT-HD1.                           COPY CPWSXSHR.       PPP565
011600******************************************************************PPP565
011700*    PPP5651 Messages Report                                     *PPP565
011800******************************************************************PPP565
011900 01  WS-5651-MISC-PRINT.                                          PPP565
012000     05  WS-5651-TTL             PIC X(40)       VALUE            PPP565
012100         'SELF BILLING STATEMENT FTP CONSOLIDATION'.              PPP565
012200     EJECT                                                        PPP565
012300******************************************************************PPP565
012400*                Procedure Division                              *PPP565
012500******************************************************************PPP565
00221  PROCEDURE DIVISION.                                              PPP565
012700******************************************************************PPP565
012800 00000-MAINLINE               SECTION.                            PPP565
012900                                                                  PPP565
013000******************************************************************PPP565
013100*    Perform program initialization.                             *PPP565
013200******************************************************************PPP565
013300     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPP565
013400                                                                  PPP565
013500******************************************************************PPP565
013600*    Read each of the four input SBS files, move the records to  *PPP565
013700*    the common format, and write out the common FTP record.     *PPP565
013800******************************************************************PPP565
013900     PERFORM 2100-READ-SBS.                                       PPP565
014000     PERFORM 2200-READ-SBS-LIFE.                                  PPP565
014100     PERFORM 2300-READ-SBS-DIS.                                   PPP565
014200     PERFORM 2400-READ-SBS-ADD.                                   PPP565
014300     PERFORM 2500-WRITE-TRAILER.                                  PPP565
015200                                                                  PPP565
015800     PERFORM 7000-PRINT-TOTALS.                                   PPP565
015900                                                                  PPP565
016000 0000-EOJ.                                                        PPP565
016100     MOVE XMSG-HIGHEST-SEVERITY TO RETURN-CODE.                   PPP565
00232      STOP RUN.                                                    PPP565
016300                                                                  PPP565
016400******************************************************************PPP565
016500*    Program Initialization                                      *PPP565
016600******************************************************************PPP565
016700 1000-PROGRAM-INITIALIZATION  SECTION.                            PPP565
016800                                                                  PPP565
016900******************************************************************PPP565
017000*    Display module compile date and time                        *PPP565
017100******************************************************************PPP565
017200     MOVE 'PPP565' TO XWHC-DISPLAY-MODULE.                        PPP565
017300*                                               *-------*         PPP565
017400                                             COPY CPPDXWHC.       PPP565
017500*                                               *-------*         PPP565
017600                                                                  PPP565
017700******************************************************************PPP565
017800*    Set up report headings                                      *PPP565
017900******************************************************************PPP565
018000     PERFORM XDC3-GET-CURRENT-DATE.                               PPP565
018100     MOVE  XDC3-FMT-DATE               TO  STND-RPT-DATE.         PPP565
018200     MOVE  '     PAYROLL PROCESSING'   TO  STND-FUNC-AREA.        PPP565
018300     MOVE  WS-STND-PGM-ID              TO  STND-PROG-ID.          PPP565
018400                                                                  PPP565
018500     MOVE SPACES         TO  MSSG-INTERFACE.                      PPP565
018600     MOVE XDC3-FMT-DATE  TO  XMSG-RUN-DATE.                       PPP565
018700     MOVE 'PPP5651'      TO  XMSG-RPT-ID.                         PPP565
018800     MOVE STND-PROG-ID   TO  XMSG-PROG-ID.                        PPP565
018900     MOVE WS-5651-TTL    TO  XMSG-RPT-TTL.                        PPP565
019000     MOVE SPACE          TO  XMSG-REFERENCE.                      PPP565
019100     MOVE ZERO           TO  XMSG-SEVERITY.                       PPP565
019200     MOVE ZEROES         TO  XMSG-NUMBER.                         PPP565
019300                                                                  PPP565
019400     PERFORM 9000-CALL-PPMSGUT2                                   PPP565
020600                                                                  PPP565
020610     OPEN OUTPUT SBS-FTP-FILE.                                    PPP565
020620                                                                  PPP565
024400******************************************************************PPP565
024500* Read the SBS file for Medical, Dental, Vision and Legal and    *PPP565
024600* reformat into common FTP format.                               *PPP565
024610* If the file is empty it is a fatal error.                      *PPP565
024700******************************************************************PPP565
024800 2100-READ-SBS                SECTION.                            PPP565
024900                                                                  PPP565
025000     OPEN INPUT SBS-FILE.                                         PPP565
025100     READ SBS-FILE                                                PPP565
025200       AT END SET EOF-SBS TO TRUE                                 PPP565
025300              SET FATAL-ERROR TO TRUE                             PPP565
025400              MOVE M56501 TO XMSG-NUMBER                          PPP565
025500              PERFORM 9000-CALL-PPMSGUT2.                         PPP565
025600                                                                  PPP565
025700     PERFORM 2110-PROCESS-SBS                                     PPP565
025900       UNTIL EOF-SBS                                              PPP565
025910          OR FATAL-ERROR.                                         PPP565
026000                                                                  PPP565
026010 2110-PROCESS-SBS             SECTION.                            PPP565
026020                                                                  PPP565
026021     ADD +1 TO WS-SBS-READ.                                       PPP565
026022                                                                  PPP565
026030     MOVE SBS-RECORD TO XSBS-RECORD.                              PPP565
026031     MOVE '01'       TO XSBX-RECORD-TYPE.                         PPP565
026040     MOVE SBS-RECORD TO XSBX-SBS-RECORD.                          PPP565
026050                                                                  PPP565
026060     WRITE SBS-FTP-RECORD FROM XSBX-RECORD.                       PPP565
026065     ADD +1 TO WS-SBS-FTP-WRITTEN.                                PPP565
026070                                                                  PPP565
026410     READ SBS-FILE                                                PPP565
026420       AT END SET EOF-SBS TO TRUE.                                PPP565
026500                                                                  PPP565
026600******************************************************************PPP565
026700* Read the SBS file for Life Insurance and reformat into common  *PPP565
026800* FTP format.                                                    *PPP565
026900* If the file is empty it is a fatal error.                      *PPP565
027000******************************************************************PPP565
027100 2200-READ-SBS-LIFE           SECTION.                            PPP565
027200                                                                  PPP565
027300     OPEN INPUT SBS-LIFE-FILE.                                    PPP565
027400     READ SBS-LIFE-FILE                                           PPP565
027500       AT END SET EOF-SBS-LIFE TO TRUE                            PPP565
027600              SET FATAL-ERROR TO TRUE                             PPP565
027700              MOVE M56502 TO XMSG-NUMBER                          PPP565
027800              PERFORM 9000-CALL-PPMSGUT2.                         PPP565
027900                                                                  PPP565
028000     PERFORM 2210-PROCESS-SBS-LIFE                                PPP565
028100       UNTIL EOF-SBS-LIFE                                         PPP565
028200          OR FATAL-ERROR.                                         PPP565
028300                                                                  PPP565
028400 2210-PROCESS-SBS-LIFE        SECTION.                            PPP565
028410                                                                  PPP565
028420     ADD +1 TO WS-SBS-LIFE-READ.                                  PPP565
028500                                                                  PPP565
028700     MOVE '02' TO XSBX-RECORD-TYPE.                               PPP565
028800     MOVE SBS-LIFE-RECORD TO XSBX-SBS-RECORD.                     PPP565
028900                                                                  PPP565
029000     WRITE SBS-FTP-RECORD FROM XSBX-RECORD.                       PPP565
029010     ADD +1 TO WS-SBS-FTP-WRITTEN.                                PPP565
029100                                                                  PPP565
029200     READ SBS-LIFE-FILE                                           PPP565
029300       AT END SET EOF-SBS-LIFE TO TRUE.                           PPP565
029400                                                                  PPP565
029500******************************************************************PPP565
029600* Read the SBS file for Disability insurance and reformat into   *PPP565
029700* common FTP format.                                             *PPP565
029800* If the file is empty it is a fatal error.                      *PPP565
029900******************************************************************PPP565
030000 2300-READ-SBS-DIS            SECTION.                            PPP565
030100                                                                  PPP565
030200     OPEN INPUT SBS-DIS-FILE.                                     PPP565
030300     READ SBS-DIS-FILE                                            PPP565
030400       AT END SET EOF-SBS-DIS TO TRUE                             PPP565
030500              SET FATAL-ERROR TO TRUE                             PPP565
030600              MOVE M56503 TO XMSG-NUMBER                          PPP565
030700              PERFORM 9000-CALL-PPMSGUT2.                         PPP565
030800                                                                  PPP565
030900     PERFORM 2310-PROCESS-SBS-DIS                                 PPP565
031000       UNTIL EOF-SBS-DIS                                          PPP565
031100          OR FATAL-ERROR.                                         PPP565
031200                                                                  PPP565
031300 2310-PROCESS-SBS-DIS         SECTION.                            PPP565
031310                                                                  PPP565
031320     ADD +1 TO WS-SBS-DIS-READ.                                   PPP565
031400                                                                  PPP565
031600     MOVE '03' TO XSBX-RECORD-TYPE.                               PPP565
031700     MOVE SBS-DIS-RECORD TO XSBX-SBS-RECORD.                      PPP565
031800                                                                  PPP565
031900     WRITE SBS-FTP-RECORD FROM XSBX-RECORD.                       PPP565
031910     ADD +1 TO WS-SBS-FTP-WRITTEN.                                PPP565
032000                                                                  PPP565
032100     READ SBS-DIS-FILE                                            PPP565
032200       AT END SET EOF-SBS-DIS TO TRUE.                            PPP565
032300                                                                  PPP565
032400******************************************************************PPP565
032500* Read the SBS file for AD&D insurance and reformat into common  *PPP565
032600* FTP format.                                                    *PPP565
032700* If the file is empty it is a fatal error.                      *PPP565
032800******************************************************************PPP565
032900 2400-READ-SBS-ADD            SECTION.                            PPP565
033000                                                                  PPP565
033100     OPEN INPUT SBS-ADD-FILE.                                     PPP565
033200     READ SBS-ADD-FILE                                            PPP565
033300       AT END SET EOF-SBS-ADD TO TRUE                             PPP565
033400              SET FATAL-ERROR TO TRUE                             PPP565
033500              MOVE M56504 TO XMSG-NUMBER                          PPP565
033600              PERFORM 9000-CALL-PPMSGUT2.                         PPP565
033700                                                                  PPP565
033800     PERFORM 2410-PROCESS-SBS-ADD                                 PPP565
033900       UNTIL EOF-SBS-ADD                                          PPP565
034000          OR FATAL-ERROR.                                         PPP565
034100                                                                  PPP565
034200 2410-PROCESS-SBS-ADD         SECTION.                            PPP565
034210                                                                  PPP565
034220     ADD +1 TO WS-SBS-ADD-READ.                                   PPP565
034300                                                                  PPP565
034320     MOVE '04' TO XSBX-RECORD-TYPE.                               PPP565
034330     MOVE SBS-ADD-RECORD TO XSBX-SBS-RECORD.                      PPP565
034340                                                                  PPP565
034350     WRITE SBS-FTP-RECORD FROM XSBX-RECORD.                       PPP565
034351     ADD +1 TO WS-SBS-FTP-WRITTEN.                                PPP565
034360                                                                  PPP565
034370     READ SBS-ADD-FILE                                            PPP565
034380       AT END SET EOF-SBS-ADD TO TRUE.                            PPP565
034390                                                                  PPP565
034391******************************************************************PPP565
034392* After all the files have been read and written, add +1 to the  *PPP565
034393* written record count, move it to the trailer record and write  *PPP565
034394* it.                                                            *PPP565
034395******************************************************************PPP565
034396 2500-WRITE-TRAILER           SECTION.                            PPP565
034397                                                                  PPP565
034398     MOVE HIGH-VALUES TO XSBX-TRL-RECORD-TYPE.                    PPP565
034399     ADD +1 TO WS-SBS-FTP-WRITTEN.                                PPP565
034400     MOVE WS-SBS-FTP-WRITTEN TO WS-RECORD-COUNT.                  PPP565
034401     MOVE WS-RECORD-COUNT TO XSBX-TRL-TOTAL-COUNT.                PPP565
034402     MOVE WS-SBS-READ TO WS-RECORD-COUNT.                         PPP565
034403     MOVE WS-RECORD-COUNT TO XSBX-TRL-MED-COUNT.                  PPP565
034404     MOVE WS-SBS-LIFE-READ TO WS-RECORD-COUNT.                    PPP565
034405     MOVE WS-RECORD-COUNT TO XSBX-TRL-LIFE-COUNT.                 PPP565
034406     MOVE WS-SBS-DIS-READ TO WS-RECORD-COUNT.                     PPP565
034407     MOVE WS-RECORD-COUNT TO XSBX-TRL-DIS-COUNT.                  PPP565
034408     MOVE WS-SBS-ADD-READ TO WS-RECORD-COUNT.                     PPP565
034409     MOVE WS-RECORD-COUNT TO XSBX-TRL-ADD-COUNT.                  PPP565
034410                                                                  PPP565
034411     WRITE SBS-FTP-RECORD FROM XSBX-TRAILER.                      PPP565
034420******************************************************************PPP565
034500*     Print premium totals for each plan.                        *PPP565
034600******************************************************************PPP565
034700 7000-PRINT-TOTALS            SECTION.                            PPP565
034800                                                                  PPP565
035600******************************************************************PPP565
035700*    Medical, Dental, Vision, Legal Self Billing Statement count *PPP565
035800******************************************************************PPP565
035900     MOVE M56505 TO XMSG-NUMBER.                                  PPP565
036000     MOVE WS-SBS-READ TO WS-RECORD-COUNT-Z.                       PPP565
036100     MOVE WS-RECORD-COUNT-Z TO XMSG-DATA.                         PPP565
036200     PERFORM 9000-CALL-PPMSGUT2.                                  PPP565
036300******************************************************************PPP565
036400*    Life Self Billing Statement count                           *PPP565
036500******************************************************************PPP565
036600     MOVE M56506 TO XMSG-NUMBER.                                  PPP565
036700     MOVE WS-SBS-LIFE-READ TO WS-RECORD-COUNT-Z.                  PPP565
036800     MOVE WS-RECORD-COUNT-Z TO XMSG-DATA.                         PPP565
036900     PERFORM 9000-CALL-PPMSGUT2.                                  PPP565
037000******************************************************************PPP565
037100*    Disability Self Billing Statement count                     *PPP565
037200******************************************************************PPP565
037300     MOVE M56507 TO XMSG-NUMBER.                                  PPP565
037400     MOVE WS-SBS-DIS-READ TO WS-RECORD-COUNT-Z.                   PPP565
037500     MOVE WS-RECORD-COUNT-Z TO XMSG-DATA.                         PPP565
037600     PERFORM 9000-CALL-PPMSGUT2.                                  PPP565
037610******************************************************************PPP565
037620*    AD&D Self Billing Statement count                           *PPP565
037630******************************************************************PPP565
037640     MOVE M56508 TO XMSG-NUMBER.                                  PPP565
037650     MOVE WS-SBS-ADD-READ TO WS-RECORD-COUNT-Z.                   PPP565
037660     MOVE WS-RECORD-COUNT-Z TO XMSG-DATA.                         PPP565
037670     PERFORM 9000-CALL-PPMSGUT2.                                  PPP565
037680******************************************************************PPP565
037690*    SBS FTP count                                               *PPP565
037691******************************************************************PPP565
037692     MOVE M56509 TO XMSG-NUMBER.                                  PPP565
037694     MOVE WS-SBS-FTP-WRITTEN TO WS-RECORD-COUNT-Z.                PPP565
037695     MOVE WS-RECORD-COUNT-Z TO XMSG-DATA.                         PPP565
037696     PERFORM 9000-CALL-PPMSGUT2.                                  PPP565
037700                                                                  PPP565
039400******************************************************************PPP565
039500*     Message Utility Program                                    *PPP565
039600******************************************************************PPP565
039700 9000-CALL-PPMSGUT2           SECTION.                            PPP565
039800                                                                  PPP565
039900     CALL 'PPMSGUT2' USING MSSG-INTERFACE.                        PPP565
040000     IF XMSG-SEVERITY   > '7'                                     PPP565
040100        SET FATAL-ERROR TO TRUE                                   PPP565
040110        GO TO 0000-EOJ                                            PPP565
040200     END-IF.                                                      PPP565
040300                                                                  PPP565
040400******************************************************************PPP565
040500*     XDC3 Date Functions                                        *PPP565
040600******************************************************************PPP565
040700 9100-XDC3-FUNCTIONS          SECTION.                            PPP565
040800                                             COPY CPPDXDC3.       PPP565
040900                                                                  PPP565
041000**************************  END OF PPP565  ***********************PPP565
