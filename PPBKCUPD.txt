000100**************************************************************/   36450775
000200*  PROGRAM: PPBKCUPD                                         */   36450775
000300*  RELEASE: ___0775______ SERVICE REQUEST(S): _____3645____  */   36450775
000400*  NAME:_______DDM_______ MODIFICATION DATE:  __06/01/93____ */   36450775
000500*  DESCRIPTION: UPDATE COMPLEX DRIVER FOR BKC TABLE.         */   36450775
000600*                                                            */   36450775
000700**************************************************************/   36450775
000800 IDENTIFICATION DIVISION.                                         PPBKCUPD
000900 PROGRAM-ID. PPBKCUPD.                                            PPBKCUPD
001000*AUTHOR  DDM                                                      PPBKCUPD
001100*DATE-WRITTEN. FEB 1993                                           PPBKCUPD
001200*DATE-COMPILED.                                                   PPBKCUPD
001300*REMARKS.                                                         PPBKCUPD
001400***************************************************************** PPBKCUPD
001500*                                                                 PPBKCUPD
001600* THIS PROGRAM DRIVES THE EDB UPDATE PROCESS FOR THE BACKGROUND   PPBKCUPD
001700* TABLE. THIS PROGRAM IS CALLED BY PPEDBUPD.                      PPBKCUPD
001800*                                                                 PPBKCUPD
001900*                                                                 PPBKCUPD
002000***************************************************************** PPBKCUPD
002100     EJECT                                                        PPBKCUPD
002200 ENVIRONMENT DIVISION.                                            PPBKCUPD
002300 CONFIGURATION SECTION.                                           PPBKCUPD
002400 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPBKCUPD
002500 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPBKCUPD
002600     EJECT                                                        PPBKCUPD
002700***************************************************************** PPBKCUPD
002800*                                                                 PPBKCUPD
002900*                D A T A  D I V I S I O N                         PPBKCUPD
003000*                                                                 PPBKCUPD
003100***************************************************************** PPBKCUPD
003200 DATA DIVISION.                                                   PPBKCUPD
003300 WORKING-STORAGE SECTION.                                         PPBKCUPD
003400 01  MISCELLANEOUS-WS.                                            PPBKCUPD
003500     05  A-STND-PROG-ID      PIC X(08) VALUE 'PPBKCUPD'.          PPBKCUPD
003600     05  PPXXXUTL-NAME       PIC X(08) VALUE 'PPBKCUTL'.          PPBKCUPD
003700     05  PPXXXUTW-NAME       PIC X(08) VALUE 'PPBKCUTW'.          PPBKCUPD
003800     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPBKCUPD
003900         88  FIRST-TIME                VALUE 'Y'.                 PPBKCUPD
004000         88  NOT-FIRST-TIME            VALUE 'N'.                 PPBKCUPD
004100     05  WS-EMPLOYEE-ID      PIC X(9).                            PPBKCUPD
004200     05  WS-IX1              PIC S9(4) COMP VALUE ZERO.           PPBKCUPD
004300     05  WS-IX2              PIC S9(4) COMP VALUE ZERO.           PPBKCUPD
004400     05  ISO-LOW-DATE          PIC X(10)  VALUE '0001-01-01'.     PPBKCUPD
004500     05  INIT-KEY-FOR-BKC-ARRAY.                                  PPBKCUPD
004600         10  INIT-BKC-CHECK-CD PIC S9(04)  COMP VALUE ZERO.       PPBKCUPD
004700     05  WS-HOLD-KEY.                                             PPBKCUPD
004800         15  WS-BKC-CHECK-CD       PIC  9(02).                    PPBKCUPD
004900     05  WS-HOLD-KEY-COMP.                                        PPBKCUPD
005000         15  WS-BKC-CHECK-CD-COMP  PIC S9(04) COMP.               PPBKCUPD
005100     EJECT                                                        PPBKCUPD
005200***************************************************************** PPBKCUPD
005300***   DATE/DE FORMAT CONVERSION AREA                              PPBKCUPD
005400***************************************************************** PPBKCUPD
005500 01  TEMP-DATA-AREA.                                              PPBKCUPD
005600                                 COPY CPWSXMMM.                   PPBKCUPD
005700     EJECT                                                        PPBKCUPD
005800***************************************************************** PPBKCUPD
005900***   EXTERNAL ARRAY - MULTIPLE VERSIONS -                        PPBKCUPD
006000***************************************************************** PPBKCUPD
006100*                                                                 PPBKCUPD
006200*  BKGRND-ARRAY IS THE DATA AREA THAT WILL BE PASSED              PPBKCUPD
006300* AROUND FOR USE BY HIGHER LEVEL PROGRAMS. IT IS DEFINED AS AN    PPBKCUPD
006400* EXTERNAL DATA AREA TO ALLOW HIGHER LEVEL PROGRAMS TO AVOID      PPBKCUPD
006500* PASSING THE ARRAY THROUGH UNNECESSARY CALLING CHAINS.           PPBKCUPD
006600*                                                                 PPBKCUPD
006700*  BKGRND-ARRAY IS THE UNMODIFIED ARRAY AS FETCHED BY PPBKCUTL.   PPBKCUPD
006800* THIS ARRAY ALLOWS THE REFRESH FUNCTION TO BE PERFORMED WITHOUT  PPBKCUPD
006900* ADDITIONAL DATA BASE ACCESS.                                    PPBKCUPD
007000*  THIS ARRAY IS ALSO THE REFERENCE FOR MATCHING LOGIC            PPBKCUPD
007100* WHICH DETERMINES WHICH BKC-ROWS ARE TO BE UPDATED, DELETED, OR  PPBKCUPD
007200* INSERTED.                                                       PPBKCUPD
007300*                                                                 PPBKCUPD
007400*  INIT-BKC-ARRAY IS AN ARRAY CONSISTING ENTIRELY OF ROWS         PPBKCUPD
007500* CONTAINING "INITIAL VALUES". THESE ROWS SERVE A SPECIAL         PPBKCUPD
007600* PURPOSE IN THE DATA BASE UPDATE (POST) LOGIC OF CPPDEUPD.       PPBKCUPD
007700* FOR EXAMPLE, SETTING A ROW TO INITIAL VALUES IN THE             PPBKCUPD
007800* BKGRND-ARRAY SIGNALS THAT IT IS TO BE DELETED WHEN POSTED.      PPBKCUPD
007900*                                                                 PPBKCUPD
008000***************************************************************** PPBKCUPD
008100 01  BKGRND-ARRAY EXTERNAL.                                       PPBKCUPD
008200                                 COPY CPWSEBKC.                   PPBKCUPD
008300     EJECT                                                        PPBKCUPD
008400 01  BKGRND-ARRAY-HOLD EXTERNAL.                                  PPBKCUPD
008500                                 COPY CPWSEBKC.                   PPBKCUPD
008600     EJECT                                                        PPBKCUPD
008700 01  BKGRND-ARRAY-HOLD1 EXTERNAL.                                 PPBKCUPD
008800                                 COPY CPWSEBKC.                   PPBKCUPD
008900     EJECT                                                        PPBKCUPD
009000 01  INIT-BKC-ARRAY    EXTERNAL.                                  PPBKCUPD
009100                                 COPY CPWSEBKC.                   PPBKCUPD
009200*                                                                 PPBKCUPD
009300***************************************************************** PPBKCUPD
009400     EJECT                                                        PPBKCUPD
009500***************************************************************** PPBKCUPD
009600***   OTHER EXTERNALS                                             PPBKCUPD
009700***************************************************************** PPBKCUPD
009800 01  PPP080-PPP120-SET-TRAN EXTERNAL.                             PPBKCUPD
009900     05  TRAN-APPLIED-SWITCH PIC X(01).                           PPBKCUPD
010000         88  TRAN-APPLIED              VALUE 'Y'.                 PPBKCUPD
010100         88  NOT-TRAN-APPLIED          VALUE 'N'.                 PPBKCUPD
010200*                                                                 PPBKCUPD
010300*                                                                 PPBKCUPD
010400*                                                                 PPBKCUPD
010500 01  ONLINE-SIGNALS EXTERNAL.                                     PPBKCUPD
010600                                    COPY CPWSONLI.                PPBKCUPD
010700     EJECT                                                        PPBKCUPD
010800***************************************************************** PPBKCUPD
010900***   OTHER INTERNAL COPIES OF THE ARRAY                          PPBKCUPD
011000***************************************************************** PPBKCUPD
011100 01  BKC-ROW.                                                     PPBKCUPD
011200     EXEC SQL                                                     PPBKCUPD
011300         INCLUDE PPPVBKC1                                         PPBKCUPD
011400     END-EXEC.                                                    PPBKCUPD
011500     EJECT                                                        PPBKCUPD
011600***************************************************************** PPBKCUPD
011700*** INTERFACES                                                    PPBKCUPD
011800***************************************************************** PPBKCUPD
011900 01  PPBKCUTL-INTERFACE.                                          PPBKCUPD
012000                                 COPY CPLNKBKC.                   PPBKCUPD
012100     EJECT                                                        PPBKCUPD
012200 01  PPXXXUTW-INTERFACE.                                          PPBKCUPD
012300                                 COPY CPLNWXXX.                   PPBKCUPD
012400     EJECT                                                        PPBKCUPD
012500***************************************************************** PPBKCUPD
012600*** COMPILER TIMESTAMP                                            PPBKCUPD
012700***************************************************************** PPBKCUPD
012800 01  XWHC-COMPILE-WORK-AREA.                                      PPBKCUPD
012900                                 COPY CPWSXWHC.                   PPBKCUPD
013000     EJECT                                                        PPBKCUPD
013100***************************************************************** PPBKCUPD
013200*** COMMON CONTROL DATA                                           PPBKCUPD
013300***************************************************************** PPBKCUPD
013400 01  PAYROLL-CONSTANTS.                                           PPBKCUPD
013500                                 COPY CPWSXIC2.                   PPBKCUPD
013600 LINKAGE SECTION.                                                 PPBKCUPD
013700     EJECT                                                        PPBKCUPD
013800***************************************************************** PPBKCUPD
013900*              L I N K A G E    S E C T I O N                     PPBKCUPD
014000***************************************************************** PPBKCUPD
014100*                                                                 PPBKCUPD
014200 01  PPXXXUPD-INTERFACE.            COPY CPLNUXXX.                PPBKCUPD
014300     EJECT                                                        PPBKCUPD
014400 01  EDB-CHANGE-RECORD.             COPY CPWSXCHG.                PPBKCUPD
014500     EJECT                                                        PPBKCUPD
014600 PROCEDURE DIVISION USING                                         PPBKCUPD
014700                          PPXXXUPD-INTERFACE                      PPBKCUPD
014800                          EDB-CHANGE-RECORD.                      PPBKCUPD
014900 0000-PARA-FOR-PRECOMP-PAGING  SECTION.                           PPBKCUPD
015000****************************************************************  PPBKCUPD
015100****************************************************************  PPBKCUPD
015200     EJECT                                                        PPBKCUPD
015300***************************************************************** PPBKCUPD
015400*            P R O C E D U R E  D I V I S I O N                   PPBKCUPD
015500***************************************************************** PPBKCUPD
015600 0100-MAINLINE SECTION.                                           PPBKCUPD
015700****************************************************************  PPBKCUPD
015800****************************************************************  PPBKCUPD
015900     MOVE PPXXXUPD-EMPLOYEE-ID TO PPBKCUTL-EMPLOYEE-ID.           PPBKCUPD
016000     MOVE PPBKCUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                 PPBKCUPD
016100     INITIALIZE PPXXXUPD-ERROR-SWITCHES.                          PPBKCUPD
016200     IF FIRST-TIME                                                PPBKCUPD
016300         SET NOT-FIRST-TIME TO TRUE                               PPBKCUPD
016400         IF NOT ENVIRON-IS-CICS                                   PPBKCUPD
016500             PERFORM 0112-DISPLAY-COMPILE-STAMP                   PPBKCUPD
016600         END-IF                                                   PPBKCUPD
016700     END-IF.                                                      PPBKCUPD
016800*                                                                 PPBKCUPD
016900     EVALUATE PPXXXUPD-FUNCTION                                   PPBKCUPD
017000         WHEN 'A'                                                 PPBKCUPD
017100             PERFORM 1000-APPLY-DATA-ELEMENT-CHANGE               PPBKCUPD
017200         WHEN 'C'                                                 PPBKCUPD
017300             PERFORM 0114-INIT-CURRENT-ARRAY                      PPBKCUPD
017400         WHEN 'F'                                                 PPBKCUPD
017500             PERFORM 0110-INITIALIZATION                          PPBKCUPD
017600             MOVE PPXXXUPD-EMPLOYEE-ID TO PPBKCUTL-EMPLOYEE-ID    PPBKCUPD
017700             PERFORM 8100-CALL-PPXXXUTL                           PPBKCUPD
017800         WHEN 'H'                                                 PPBKCUPD
017900             MOVE BKGRND-ARRAY TO BKGRND-ARRAY-HOLD1              PPBKCUPD
018000         WHEN 'P'                                                 PPBKCUPD
018100             PERFORM 2000-POST-ROWS                               PPBKCUPD
018200         WHEN 'R'                                                 PPBKCUPD
018300             MOVE BKGRND-ARRAY-HOLD   TO  BKGRND-ARRAY            PPBKCUPD
018400         WHEN OTHER                                               PPBKCUPD
018500             SET PPXXXUPD-INVALID-FUNCTION TO TRUE                PPBKCUPD
018600     END-EVALUATE.                                                PPBKCUPD
018700*                                                                 PPBKCUPD
018800     IF PPXXXUPD-ERROR-SWITCHES NOT = SPACES                      PPBKCUPD
018900         SET PPXXXUPD-ERROR TO TRUE                               PPBKCUPD
019000*                                                                 PPBKCUPD
019100     END-IF.                                                      PPBKCUPD
019200     GOBACK.                                                      PPBKCUPD
019300     EJECT                                                        PPBKCUPD
019400 0110-INITIALIZATION SECTION.                                     PPBKCUPD
019500****************************************************************  PPBKCUPD
019600* THIS SECTION SETS THE "INIT" COPY OF THE ARRAY TO "INITIAL      PPBKCUPD
019700* VALUES" INCLUDING DEFAULT DATES OF 0001-01-01. THIS ARRAY       PPBKCUPD
019800* IS THEN USED FOR REFERENCE AND NEVER MODIFIED.                  PPBKCUPD
019900*                                                                 PPBKCUPD
020000* OTHER COPIES OF THE ARRAY REPRESENTING ORIGINAL FETCHED         PPBKCUPD
020100* VALUES ("HOLD") AND NEW VALUES, RESPECTIVELY, ARE ALSO          PPBKCUPD
020200* INITIALIZED HERE.                                               PPBKCUPD
020300****************************************************************  PPBKCUPD
020400     PERFORM 0114-INIT-CURRENT-ARRAY.                             PPBKCUPD
020500     MOVE BKGRND-ARRAY   TO  INIT-BKC-ARRAY.                      PPBKCUPD
020600     MOVE BKGRND-ARRAY   TO  BKGRND-ARRAY-HOLD.                   PPBKCUPD
020700*                                                                 PPBKCUPD
020800*                                                                 PPBKCUPD
020900*                                                                 PPBKCUPD
021000*                                                                 PPBKCUPD
021100 0112-DISPLAY-COMPILE-STAMP  SECTION.                             PPBKCUPD
021200****************************************************************  PPBKCUPD
021300* EXECUTE CODE IN COPYBOOK TO DISPLAY COMPILE TIMESTAMP.          PPBKCUPD
021400****************************************************************  PPBKCUPD
021500     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPBKCUPD
021600                                    COPY CPPDXWHC.                PPBKCUPD
021700*                                                                 PPBKCUPD
021800*                                                                 PPBKCUPD
021900*                                                                 PPBKCUPD
022000 0114-INIT-CURRENT-ARRAY SECTION.                                 PPBKCUPD
022100****************************************************************  PPBKCUPD
022200* THIS SECTION REINITIALIZES A COPY OF THE ARRAY WHICH            PPBKCUPD
022300* REPRESENTS NEW VALUES IN THE UPDATE PROCESSING. DATES (IF       PPBKCUPD
022400* ANY) ARE INITIALIZED TO 0001-01-01.                             PPBKCUPD
022500***************************************************************** PPBKCUPD
022600     INITIALIZE BKGRND-ARRAY.                                     PPBKCUPD
022700     PERFORM VARYING WS-IX1  FROM 1 BY 1                          PPBKCUPD
022800     UNTIL   WS-IX1  > IDC-MAX-NO-BKC                             PPBKCUPD
022900         MOVE ISO-LOW-DATE   TO BKGRND-CHECK-DATE                 PPBKCUPD
023000                                OF BKGRND-ARRAY (WS-IX1)          PPBKCUPD
023100     END-PERFORM.                                                 PPBKCUPD
023200     EJECT                                                        PPBKCUPD
023300 1000-APPLY-DATA-ELEMENT-CHANGE SECTION.                          PPBKCUPD
023400****************************************************************  PPBKCUPD
023500*** "APPLY" XCHG FILE DATA TO EDB EXTERNAL DATA.                  PPBKCUPD
023600*** SELECT THE APPROPRIATE NEW VALUE AND SAVE AS TEMP-DATA-VALUE. PPBKCUPD
023700*** MATCH THE XCHG OCCURRENCE KEY TO THE EDB KEY IN THE ARRAY.    PPBKCUPD
023800*** MAP THE NEW VALUE TO THE CORRECT DB2 COLUMN USING THE DE #.   PPBKCUPD
023900****************************************************************  PPBKCUPD
024000     IF XCHG-EMPLOYEE-ID NOT = SPACES                             PPBKCUPD
024100         IF XCHG-EMPLOYEE-ID NOT = PPBKCUTL-EMPLOYEE-ID           PPBKCUPD
024200             MOVE XCHG-EMPLOYEE-ID TO PPBKCUTL-EMPLOYEE-ID        PPBKCUPD
024230         IF NOT ENVIRON-IS-CICS                                   PPBKCUPD
024300             PERFORM 8100-CALL-PPXXXUTL                           PPBKCUPD
024310         END-IF                                                   PPBKCUPD
024400         END-IF                                                   PPBKCUPD
024500*                                                                 PPBKCUPD
024600*** SELECT THE APPROPRIATE NEW VALUE AND SAVE AS TEMP-DATA-VALUE. PPBKCUPD
024700         IF XCHG-NEW-VAL2-LNGTH NOT = 0                           PPBKCUPD
024800             MOVE XCHG-NEW-VALUE2 TO TEMP-DATA-VALUE              PPBKCUPD
024900         ELSE                                                     PPBKCUPD
025000             IF XCHG-NEW-VAL1-LNGTH NOT = 0                       PPBKCUPD
025100                 MOVE XCHG-NEW-VALUE1 TO TEMP-DATA-VALUE          PPBKCUPD
025200             ELSE                                                 PPBKCUPD
025300                 SET PPXXXUPD-APPLY-CHANGE-ERROR TO TRUE          PPBKCUPD
025400             END-IF                                               PPBKCUPD
025500         END-IF                                                   PPBKCUPD
025600*                                                                 PPBKCUPD
025700*** MATCH THE XCHG OCCURRENCE KEY TO THE EDB KEY IN THE ARRAY.    PPBKCUPD
025800         IF NOT PPXXXUPD-APPLY-CHANGE-ERROR                       PPBKCUPD
025900             PERFORM 9100-MATCH-OCC-KEY                           PPBKCUPD
026000         END-IF                                                   PPBKCUPD
026100*                                                                 PPBKCUPD
026200*** MAP THE NEW VALUE TO THE CORRECT DB2 COLUMN USING THE DE #.   PPBKCUPD
026300         IF  TRAN-APPLIED                                         PPBKCUPD
026400         AND  NOT  PPXXXUPD-DATA-ELEM-ERROR                       PPBKCUPD
026500             PERFORM 9200-MAP-DE-TO-EDB-COL                       PPBKCUPD
026600         END-IF                                                   PPBKCUPD
026700*                                                                 PPBKCUPD
026800     ELSE                                                         PPBKCUPD
026900***      EMPLOYEE ID IS BLANK                                     PPBKCUPD
027000         SET PPXXXUPD-APPLY-CHANGE-ERROR TO TRUE                  PPBKCUPD
027100     END-IF.                                                      PPBKCUPD
027200     EJECT                                                        PPBKCUPD
027300 2000-POST-ROWS SECTION.                                          PPBKCUPD
027400****************************************************************  PPBKCUPD
027500*                                                                 PPBKCUPD
027600****************************************************************  PPBKCUPD
027700*** MARCH THROUGH THE HOLD (ORIG VALUES) ARRAY AND DELETE         PPBKCUPD
027800*** ALL CORRESPONDING DB2 ROWS, ONE AT A TIME...                  PPBKCUPD
027900*                                                                 PPBKCUPD
028000     PERFORM VARYING WS-IX1 FROM 1 BY 1                           PPBKCUPD
028100             UNTIL  WS-IX1 > IDC-MAX-NO-BKC                       PPBKCUPD
028200         OR  BKGRND-CHECK-CD OF BKGRND-ARRAY-HOLD (WS-IX1)        PPBKCUPD
028300             = INIT-BKC-CHECK-CD                                  PPBKCUPD
028400             MOVE BKGRND-CHECK-CD                                 PPBKCUPD
028500                  OF BKGRND-ARRAY-HOLD (WS-IX1)                   PPBKCUPD
028600                  TO BKC-BKGRND-CHECK-CD OF BKC-ROW               PPBKCUPD
028700             MOVE WS-EMPLOYEE-ID TO BKC-EMPLOYEE-ID OF BKC-ROW    PPBKCUPD
028800             MOVE 'D' TO PPXXXUTW-FUNCTION                        PPBKCUPD
028900             PERFORM 8200-CALL-PPXXXUTW                           PPBKCUPD
029000     END-PERFORM.                                                 PPBKCUPD
029100*                                                                 PPBKCUPD
029200*    SAVE POINTER TO FIRST "NEW" ARRAY ELEMENT IF ANY             PPBKCUPD
029300*                                                                 PPBKCUPD
029400     MOVE WS-IX1 TO WS-IX2.                                       PPBKCUPD
029500*                                                                 PPBKCUPD
029600*    INSERT ALL ROWS FROM THE "CURRENT" ARRAY UNTIL THE END       PPBKCUPD
029700*    OF THE REVISED AND NEW ROWS. THE END IS INDICATED BY THE     PPBKCUPD
029800*    FIRST ROW CONTAINING "INITIAL VALUES" IN THE KEY, AFTER      PPBKCUPD
029900*    PROCESSING ALL OF THE ARRAY ROWS REPRESENTING PRE-EXISTING   PPBKCUPD
030000*    DATA BASE ROWS.                                              PPBKCUPD
030100*                                                                 PPBKCUPD
030200     PERFORM VARYING WS-IX1 FROM 1 BY 1                           PPBKCUPD
030300             UNTIL  WS-IX1 > IDC-MAX-NO-BKC                       PPBKCUPD
030400         OR ( BKGRND-CHECK-CD OF BKGRND-ARRAY (WS-IX1)            PPBKCUPD
030500             = INIT-BKC-CHECK-CD                                  PPBKCUPD
030600                AND WS-IX1 NOT < WS-IX2 )                         PPBKCUPD
030700         IF  BKGRND-CHECK-CD OF BKGRND-ARRAY (WS-IX1)             PPBKCUPD
030800             NOT  = INIT-BKC-CHECK-CD                             PPBKCUPD
030900             MOVE BKGRND-CHECK-CD                                 PPBKCUPD
031000                  OF BKGRND-ARRAY (WS-IX1)                        PPBKCUPD
031100                  TO BKC-BKGRND-CHECK-CD    OF BKC-ROW            PPBKCUPD
031200             MOVE BKGRND-CHECK-DATE                               PPBKCUPD
031300                  OF BKGRND-ARRAY (WS-IX1)                        PPBKCUPD
031400                  TO BKC-BKGRND-CHECK-DATE OF BKC-ROW             PPBKCUPD
031500             MOVE BKGRND-ADC-CD                                   PPBKCUPD
031600                  OF BKGRND-ARRAY (WS-IX1)                        PPBKCUPD
031700                  TO BKC-BKGRND-ADC-CD      OF BKC-ROW            PPBKCUPD
031800             MOVE WS-EMPLOYEE-ID TO BKC-EMPLOYEE-ID OF BKC-ROW    PPBKCUPD
031900             MOVE 'I' TO PPXXXUTW-FUNCTION                        PPBKCUPD
032000             PERFORM 8200-CALL-PPXXXUTW                           PPBKCUPD
032100         END-IF                                                   PPBKCUPD
032200     END-PERFORM.                                                 PPBKCUPD
032300     EJECT                                                        PPBKCUPD
032400 8100-CALL-PPXXXUTL SECTION.                                      PPBKCUPD
032500****************************************************************  PPBKCUPD
032600*** THE CALLED PROGRAM FETCHES ALL ROWS FOR THE EMPLOYEE ID       PPBKCUPD
032700*** AND FILLS THE ARRAY. EXTRA ROWS IN THE ARRAY ARE RETURNED     PPBKCUPD
032800*** WITH "INITIAL VALUES" IN THEM.                                PPBKCUPD
032900****************************************************************  PPBKCUPD
033000*                                                                 PPBKCUPD
033100     CALL PPXXXUTL-NAME USING PPBKCUTL-INTERFACE,                 PPBKCUPD
033200                              BKGRND-ARRAY                        PPBKCUPD
033300*                                                                 PPBKCUPD
033400     IF PPBKCUTL-ERROR                                            PPBKCUPD
033500         PERFORM 0114-INIT-CURRENT-ARRAY                          PPBKCUPD
033600         SET PPXXXUPD-ERROR TO TRUE                               PPBKCUPD
033700     END-IF.                                                      PPBKCUPD
033800*                                                                 PPBKCUPD
033900     IF PPXXXUPD-DIAGNOSTICS-ON                                   PPBKCUPD
034000         IF  NOT ENVIRON-IS-CICS                                  PPBKCUPD
034100             DISPLAY A-STND-PROG-ID ': ' PPXXXUTL-NAME,           PPBKCUPD
034200                     '-INTERFACE "' PPBKCUTL-INTERFACE '"'        PPBKCUPD
034300         END-IF                                                   PPBKCUPD
034400     END-IF.                                                      PPBKCUPD
034500*                                                                 PPBKCUPD
034600     MOVE BKGRND-ARRAY   TO  BKGRND-ARRAY-HOLD.                   PPBKCUPD
034700*                                                                 PPBKCUPD
034800     MOVE PPBKCUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                 PPBKCUPD
034900     EJECT                                                        PPBKCUPD
035000 8200-CALL-PPXXXUTW SECTION.                                      PPBKCUPD
035100****************************************************************  PPBKCUPD
035200*** DB2 UPDATE PROCESSING - ONE ROW AT A TIME.                    PPBKCUPD
035300****************************************************************  PPBKCUPD
035400     INITIALIZE PPXXXUTW-ERROR-SWITCHES.                          PPBKCUPD
035500*                                                                 PPBKCUPD
035600     CALL PPXXXUTW-NAME USING PPXXXUTW-INTERFACE,                 PPBKCUPD
035700                           BKC-ROW.                               PPBKCUPD
035800*                                                                 PPBKCUPD
035900     IF PPXXXUTW-ERROR                                            PPBKCUPD
036000         SET PPXXXUPD-ERROR TO TRUE                               PPBKCUPD
036100     END-IF.                                                      PPBKCUPD
036200*                                                                 PPBKCUPD
036300     IF PPXXXUPD-DIAGNOSTICS-ON                                   PPBKCUPD
036400         IF  NOT ENVIRON-IS-CICS                                  PPBKCUPD
036500             DISPLAY A-STND-PROG-ID ': PPXXXUTW-INTERFACE "',     PPBKCUPD
036600                     PPXXXUTW-INTERFACE '"',                      PPBKCUPD
036700                     'BKC-ROW="' BKC-ROW '"'                      PPBKCUPD
036800         END-IF                                                   PPBKCUPD
036900     END-IF.                                                      PPBKCUPD
037000     EJECT                                                        PPBKCUPD
037100 9100-MATCH-OCC-KEY SECTION.                                      PPBKCUPD
037200***************************************************************** PPBKCUPD
037300* MATCH THE XCHG OCCURRENCE KEY TO THE EDB ROW KEY TO IDENTIFY    PPBKCUPD
037400* THE ROW IN THE EXTERNAL TABLE TO BE UPDATED.                    PPBKCUPD
037500* A MATCH ON THE INITIAL VALUE ROW INDICATES NO MATCH. THE        PPBKCUPD
037600* INDEX POINTS TO THE NEXT AVAILABLE (NEW) ROW.                   PPBKCUPD
037700***************************************************************** PPBKCUPD
037800***  REFORMAT THE XCHG KEY AS NECESSARY FOR THE SEARCH.           PPBKCUPD
037900     MOVE XCHG-OCCURENCE-KEY TO WS-HOLD-KEY.                      PPBKCUPD
038000     MOVE WS-BKC-CHECK-CD    TO WS-BKC-CHECK-CD-COMP.             PPBKCUPD
038100*                                                                 PPBKCUPD
038200*    LOCATE THE PROPER ARRAY ELEMENT OR THE                       PPBKCUPD
038300*    NEXT AVAILABLE                                               PPBKCUPD
038400*                                                                 PPBKCUPD
038500     PERFORM VARYING WS-IX1 FROM 1 BY 1                           PPBKCUPD
038600             UNTIL WS-IX1 > IDC-MAX-NO-BKC                        PPBKCUPD
038700         OR  BKGRND-CHECK-CD OF BKGRND-ARRAY (WS-IX1)             PPBKCUPD
038800             = WS-BKC-CHECK-CD-COMP                               PPBKCUPD
038900         OR  BKGRND-CHECK-CD OF BKGRND-ARRAY (WS-IX1)             PPBKCUPD
039000             = INIT-BKC-CHECK-CD                                  PPBKCUPD
039100     END-PERFORM.                                                 PPBKCUPD
039200*                                                                 PPBKCUPD
039300*    CHECK FOR ARRAY OVERFLOW                                     PPBKCUPD
039400     SET TRAN-APPLIED TO TRUE.                                    PPBKCUPD
039500     IF WS-IX1 > IDC-MAX-NO-BKC                                   PPBKCUPD
039600          IF PPXXXUPD-FUNCTION = 'A'                              PPBKCUPD
039700              SET NOT-TRAN-APPLIED TO TRUE                        PPBKCUPD
039800          ELSE                                                    PPBKCUPD
039900              SET PPXXXUPD-DATA-ELEM-ERROR TO TRUE                PPBKCUPD
040000          END-IF                                                  PPBKCUPD
040100     END-IF.                                                      PPBKCUPD
040200     EJECT                                                        PPBKCUPD
040300 9200-MAP-DE-TO-EDB-COL  SECTION.                                 PPBKCUPD
040400***************************************************************** PPBKCUPD
040500* THIS SECTION MAPS THE DATA FROM THE EMPLOYEE CHANGE FILE(ECF)   PPBKCUPD
040600* TO THE EDB DB2 COLUMN USING HARDCODED DATA ELEMENT NUMBERS      PPBKCUPD
040700* AND COLUMN NAMES. THIS MAPPING IS NOT PERFORMED DIRECTLY TO     PPBKCUPD
040800* DB2.                                                            PPBKCUPD
040900***************************************************************** PPBKCUPD
041000*                                                                 PPBKCUPD
041100* COPY THE XCHG KEY TO THE ROW IN THE EDB ARRAY IF THIS IS A NEW  PPBKCUPD
041200* ROW. (A ROW IS NEW IF THE OCC KEY FOR THE ROW IS STILL SET TO   PPBKCUPD
041300* INITIAL VALUES).                                                PPBKCUPD
041400     IF  BKGRND-CHECK-CD OF BKGRND-ARRAY (WS-IX1)                 PPBKCUPD
041500         = INIT-BKC-CHECK-CD                                      PPBKCUPD
041600        MOVE WS-BKC-CHECK-CD-COMP                                 PPBKCUPD
041700          TO BKGRND-CHECK-CD OF BKGRND-ARRAY (WS-IX1)             PPBKCUPD
041800     END-IF.                                                      PPBKCUPD
041900*                                                                 PPBKCUPD
042000     EVALUATE XCHG-DATA-ELEM-NO                                   PPBKCUPD
042100         WHEN '0740'                                              PPBKCUPD
042200              MOVE WS-BKC-CHECK-CD-COMP                           PPBKCUPD
042300                TO BKGRND-CHECK-CD  OF BKGRND-ARRAY (WS-IX1)      PPBKCUPD
042400         WHEN '0741'                                              PPBKCUPD
042500              MOVE TEMP-DATA-VALUE TO                             PPBKCUPD
042600                      BKGRND-CHECK-DATE                           PPBKCUPD
042700                          OF BKGRND-ARRAY (WS-IX1)                PPBKCUPD
042800         WHEN '0716'                                              PPBKCUPD
042900              MOVE TEMP-DATA-VALUE TO                             PPBKCUPD
043000                      BKGRND-ADC-CD                               PPBKCUPD
043100                          OF BKGRND-ARRAY (WS-IX1)                PPBKCUPD
043200*                                                                 PPBKCUPD
043300         WHEN OTHER                                               PPBKCUPD
043400              IF PPXXXUPD-DIAGNOSTICS-ON                          PPBKCUPD
043500                  IF  NOT ENVIRON-IS-CICS                         PPBKCUPD
043600                      DISPLAY A-STND-PROG-ID                      PPBKCUPD
043700                          ': UNRECOGNIZED DATA ELEMENT NUMBER ('  PPBKCUPD
043800                           XCHG-DATA-ELEM-NO                      PPBKCUPD
043900                          ')'                                     PPBKCUPD
044000                  END-IF                                          PPBKCUPD
044100              END-IF                                              PPBKCUPD
044200              SET PPXXXUPD-DATA-ELEM-ERROR TO TRUE                PPBKCUPD
044300     END-EVALUATE.                                                PPBKCUPD
