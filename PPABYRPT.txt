000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPABYRPT                                          *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    ABEYANCE FILE I/O MODULE                                *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100 IDENTIFICATION DIVISION.                                         PPABYRPT
001200 PROGRAM-ID. PPABYRPT.                                            PPABYRPT
001300 AUTHOR. UCOP.                                                    PPABYRPT
001400 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPABYRPT
001500*                                                                 PPABYRPT
001600 DATE-WRITTEN.  SEPT 9,1992.                                      PPABYRPT
001700 DATE-COMPILED. XX/XX/XX.                                         PPABYRPT
001800     EJECT                                                        PPABYRPT
001900 ENVIRONMENT DIVISION.                                            PPABYRPT
002000 CONFIGURATION SECTION.                                           PPABYRPT
002100 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPABYRPT
002200 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPABYRPT
002300 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPABYRPT
002400 INPUT-OUTPUT SECTION.                                            PPABYRPT
002500 FILE-CONTROL.                                                    PPABYRPT
002600                                                                  PPABYRPT
002700     SELECT  IMPUTED-INCOME-ABEYANCE                              PPABYRPT
002800             ASSIGN TO UT-S-EXECABEY.                             PPABYRPT
002900                                                                  PPABYRPT
003000                                                                  PPABYRPT
003100 DATA DIVISION.                                                   PPABYRPT
003200 FILE SECTION.                                                    PPABYRPT
003300                                                                  PPABYRPT
003400 FD  IMPUTED-INCOME-ABEYANCE                                      PPABYRPT
003500     BLOCK CONTAINS 0 RECORDS                                     PPABYRPT
003600     LABEL RECORDS ARE STANDARD                                   PPABYRPT
003700     RECORDING MODE IS F                                          PPABYRPT
003800     RECORD CONTAINS 24 CHARACTERS                                PPABYRPT
003900     DATA RECORD IS IMP-INC-ABEYANCE-RECORD.                      PPABYRPT
004000 01  IMP-INC-ABEYANCE-RECORD.                                     PPABYRPT
004100     05  IMP-INC-ABEY-ID             PIC X(09).                   PPABYRPT
004200     05  IMP-INC-ABEY-NOV            PIC S9(05)V99.               PPABYRPT
004300     05  IMP-INC-ABEY-DEC            PIC S9(05)V99.               PPABYRPT
004400     05  FILLER                      PIC X(01).                   PPABYRPT
004500                                                                  PPABYRPT
004600                                                                  PPABYRPT
004700     EJECT                                                        PPABYRPT
004800*----------------------------------------------------------------*PPABYRPT
004900 WORKING-STORAGE SECTION.                                         PPABYRPT
005000*----------------------------------------------------------------*PPABYRPT
005100 77  A-STND-PROG-ID                  PIC X(15) VALUE              PPABYRPT
005200     'PPABYRPT/111992'.                                           PPABYRPT
005300                                                                  PPABYRPT
005400 01  MISC-WORK-AREAS.                                             PPABYRPT
005500     05  FIRST-TIME-SW               PIC X(01) VALUE LOW-VALUES.  PPABYRPT
005600         88  THIS-IS-THE-FIRST-TIME            VALUE LOW-VALUES.  PPABYRPT
005700         88  NOT-THE-FIRST-TIME                VALUE 'Y'.         PPABYRPT
005800                                                                  PPABYRPT
005900     EJECT                                                        PPABYRPT
006000 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPABYRPT
006100                                                                  PPABYRPT
006200     EJECT                                                        PPABYRPT
006300 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPABYRPT
006400                                                                  PPABYRPT
006500     EJECT                                                        PPABYRPT
006600 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPABYRPT
006700                                                                  PPABYRPT
006800     EJECT                                                        PPABYRPT
006900******************************************************************PPABYRPT
007000**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPABYRPT
007100******************************************************************PPABYRPT
007200                                                                  PPABYRPT
007300*----------------------------------------------------------------*PPABYRPT
007400 LINKAGE SECTION.                                                 PPABYRPT
007500*----------------------------------------------------------------*PPABYRPT
007600                                                                  PPABYRPT
007700******************************************************************PPABYRPT
007800**  INTERFACE RECORD IS PASSED FROM PPEM101                     **PPABYRPT
007900******************************************************************PPABYRPT
008000                                                                  PPABYRPT
008100 01  PPABYRPT-INTERFACE.                            COPY CPLNKABY.PPABYRPT
008200                                                                  PPABYRPT
008300     EJECT                                                        PPABYRPT
008400******************************************************************PPABYRPT
008500**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPABYRPT
008600******************************************************************PPABYRPT
008700                                                                  PPABYRPT
008800 PROCEDURE DIVISION USING PPABYRPT-INTERFACE.                     PPABYRPT
008900                                                                  PPABYRPT
009000                                                                  PPABYRPT
009100*----------------------------------------------------------------*PPABYRPT
009200 0000-DRIVER.                                                     PPABYRPT
009300*----------------------------------------------------------------*PPABYRPT
009400******************************************************************PPABYRPT
009500**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPABYRPT
009600******************************************************************PPABYRPT
009700                                                                  PPABYRPT
009800     IF  THIS-IS-THE-FIRST-TIME                                   PPABYRPT
009900         PERFORM 0100-INITIALIZE                                  PPABYRPT
010000     END-IF.                                                      PPABYRPT
010100                                                                  PPABYRPT
010200******************************************************************PPABYRPT
010300**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPABYRPT
010400**  TO THE CALLING PROGRAM                                      **PPABYRPT
010500******************************************************************PPABYRPT
010600                                                                  PPABYRPT
010700     PERFORM 1000-MAINLINE-ROUTINE.                               PPABYRPT
010800                                                                  PPABYRPT
010900                                                                  PPABYRPT
011000     EXIT PROGRAM.                                                PPABYRPT
011100                                                                  PPABYRPT
011200                                                                  PPABYRPT
011300     EJECT                                                        PPABYRPT
011400*----------------------------------------------------------------*PPABYRPT
011500 0100-INITIALIZE    SECTION.                                      PPABYRPT
011600*----------------------------------------------------------------*PPABYRPT
011700                                                                  PPABYRPT
011800******************************************************************PPABYRPT
011900**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPABYRPT
012000******************************************************************PPABYRPT
012100                                                                  PPABYRPT
012200     MOVE A-STND-PROG-ID          TO XWHC-DISPLAY-MODULE.         PPABYRPT
012300                                                                  PPABYRPT
012400*                                               *-------------*   PPABYRPT
012500                                                 COPY CPPDXWHC.   PPABYRPT
012600*                                               *-------------*   PPABYRPT
012700                                                                  PPABYRPT
012800     SET NOT-THE-FIRST-TIME       TO TRUE.                        PPABYRPT
012900                                                                  PPABYRPT
013000                                                                  PPABYRPT
013100     EJECT                                                        PPABYRPT
013200*----------------------------------------------------------------*PPABYRPT
013300 1000-MAINLINE-ROUTINE    SECTION.                                PPABYRPT
013400*----------------------------------------------------------------*PPABYRPT
013500                                                                  PPABYRPT
013600     EVALUATE PPABYRPT-ACTION                                     PPABYRPT
013700                                                                  PPABYRPT
013800         WHEN 'INPUT'                                             PPABYRPT
013900               OPEN INPUT IMPUTED-INCOME-ABEYANCE                 PPABYRPT
014000                                                                  PPABYRPT
014100         WHEN 'OUTPUT'                                            PPABYRPT
014200               OPEN OUTPUT IMPUTED-INCOME-ABEYANCE                PPABYRPT
014300                                                                  PPABYRPT
014400         WHEN 'CLOSE'                                             PPABYRPT
014500               CLOSE       IMPUTED-INCOME-ABEYANCE                PPABYRPT
014600                                                                  PPABYRPT
014700         WHEN 'READ'                                              PPABYRPT
014800               PERFORM 1100-IMPUTED-INCOME-READ                   PPABYRPT
014900                                                                  PPABYRPT
015000         WHEN 'WRITE'                                             PPABYRPT
015100               PERFORM 1200-IMPUTED-INCOME-WRITE                  PPABYRPT
015200                                                                  PPABYRPT
015300     END-EVALUATE.                                                PPABYRPT
015400                                                                  PPABYRPT
015500*----------------------------------------------------------------*PPABYRPT
015600 1100-IMPUTED-INCOME-READ  SECTION.                               PPABYRPT
015700*----------------------------------------------------------------*PPABYRPT
015800                                                                  PPABYRPT
015900     READ IMPUTED-INCOME-ABEYANCE AT END                          PPABYRPT
016000         MOVE ALL '9'                TO IMP-INC-ABEY-ID           PPABYRPT
016100         MOVE ZEROS                  TO IMP-INC-ABEY-NOV,         PPABYRPT
016200                                        IMP-INC-ABEY-DEC          PPABYRPT
016300     END-READ.                                                    PPABYRPT
016400                                                                  PPABYRPT
016500     MOVE IMP-INC-ABEY-ID            TO PPABYRPT-ID.              PPABYRPT
016600     MOVE IMP-INC-ABEY-NOV           TO PPABYRPT-NOV.             PPABYRPT
016700     MOVE IMP-INC-ABEY-DEC           TO PPABYRPT-DEC.             PPABYRPT
016800                                                                  PPABYRPT
016900                                                                  PPABYRPT
017000*----------------------------------------------------------------*PPABYRPT
017100 1200-IMPUTED-INCOME-WRITE  SECTION.                              PPABYRPT
017200*----------------------------------------------------------------*PPABYRPT
017300                                                                  PPABYRPT
017400     MOVE SPACES                     TO IMP-INC-ABEYANCE-RECORD.  PPABYRPT
017500     MOVE PPABYRPT-ID                TO IMP-INC-ABEY-ID.          PPABYRPT
017600     MOVE PPABYRPT-NOV               TO IMP-INC-ABEY-NOV.         PPABYRPT
017700     MOVE PPABYRPT-DEC               TO IMP-INC-ABEY-DEC.         PPABYRPT
017800                                                                  PPABYRPT
017900     WRITE IMP-INC-ABEYANCE-RECORD.                               PPABYRPT
018000                                                                  PPABYRPT
018100                                                                  PPABYRPT
018200     EJECT                                                        PPABYRPT
018300*----------------------------------------------------------------*PPABYRPT
018400 9300-DATE-CONVERSION-DB2  SECTION.                               PPABYRPT
018500*----------------------------------------------------------------*PPABYRPT
018600                                                                  PPABYRPT
018700*                                                 *-------------* PPABYRPT
018800                                                   COPY CPPDXDC2. PPABYRPT
018900*                                                 *-------------* PPABYRPT
019000                                                                  PPABYRPT
019100******************************************************************PPABYRPT
019200*    E N D  S O U R C E   ----  PPABYRPT ----                    *PPABYRPT
019300******************************************************************PPABYRPT
