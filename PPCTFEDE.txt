000100**************************************************************/   30871460
000200*  PROGRAM: PPCTFEDE                                         */   30871460
000300*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000400*  NAME:___SRS___________ CREATION DATE:      ___01/22/03__  */   30871460
000500*  DESCRIPTION:                                              */   30871460
000600*  FEDERAL TAX  TABLE EDIT MODULE                            */   30871460
000700**************************************************************/   30871460
000800 IDENTIFICATION DIVISION.                                         PPCTFEDE
000900 PROGRAM-ID. PPCTFEDE.                                            PPCTFEDE
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTFEDE
001100 DATE-COMPILED.                                                   PPCTFEDE
001200 ENVIRONMENT DIVISION.                                            PPCTFEDE
001300 DATA DIVISION.                                                   PPCTFEDE
001400 WORKING-STORAGE SECTION.                                         PPCTFEDE
001500 01  MISC.                                                        PPCTFEDE
001600     05  WS-COUNT                 PIC S9(8) COMP SYNC VALUE ZERO. PPCTFEDE
001700     05  FED-IX                   PIC S9(4) COMP SYNC VALUE ZERO. PPCTFEDE
001800 01  MESSAGES-AREA.                                               PPCTFEDE
001900     05  M01027                   PIC X(05)     VALUE '01027'.    PPCTFEDE
002000     05  M01028                   PIC X(05)     VALUE '01028'.    PPCTFEDE
002100     05  M01029                   PIC X(05)     VALUE '01029'.    PPCTFEDE
002200     05  M01180                   PIC X(05)     VALUE '01180'.    PPCTFEDE
002300     05  M01181                   PIC X(05)     VALUE '01181'.    PPCTFEDE
002400     05  M01182                   PIC X(05)     VALUE '01182'.    PPCTFEDE
002500     05  M01183                   PIC X(05)     VALUE '01183'.    PPCTFEDE
002600     05  M01184                   PIC X(05)     VALUE '01184'.    PPCTFEDE
002700     05  M01185                   PIC X(05)     VALUE '01185'.    PPCTFEDE
002800     05  M01186                   PIC X(05)     VALUE '01186'.    PPCTFEDE
002900     05  M01187                   PIC X(05)     VALUE '01187'.    PPCTFEDE
003000     05  MCT005                   PIC X(05)     VALUE 'CT005'.    PPCTFEDE
003100                                                                  PPCTFEDE
003200 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTFEDE
003300 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTFEDE
003400                                                                  PPCTFEDE
003500     EXEC SQL                                                     PPCTFEDE
003600        INCLUDE SQLCA                                             PPCTFEDE
003700     END-EXEC.                                                    PPCTFEDE
003800                                                                  PPCTFEDE
003900 LINKAGE SECTION.                                                 PPCTFEDE
004000                                                                  PPCTFEDE
004100 01  CONTROL-TABLE-EDIT-INTERFACE.              COPY CPLNKCTE.    PPCTFEDE
004200 01  FEDERAL-TAX-TABLE-INPUT.                   COPY CPCTFEDI.    PPCTFEDE
004300 01  FEDERAL-TAX-TABLE-DATA.                                      PPCTFEDE
004400     EXEC SQL                                                     PPCTFEDE
004500        INCLUDE PPPVZFED                                          PPCTFEDE
004600     END-EXEC.                                                    PPCTFEDE
004700                                                                  PPCTFEDE
004800 PROCEDURE DIVISION USING CONTROL-TABLE-EDIT-INTERFACE            PPCTFEDE
004900                          FEDERAL-TAX-TABLE-INPUT                 PPCTFEDE
005000                          FEDERAL-TAX-TABLE-DATA.                 PPCTFEDE
005100                                                                  PPCTFEDE
005200 0000-MAIN SECTION.                                               PPCTFEDE
005300                                                                  PPCTFEDE
005400     EXEC SQL                                                     PPCTFEDE
005500        INCLUDE CPPDXE99                                          PPCTFEDE
005600     END-EXEC.                                                    PPCTFEDE
005700     MOVE 'PPCTFEDE' TO XWHC-DISPLAY-MODULE.                      PPCTFEDE
005800     COPY CPPDXWHC.                                               PPCTFEDE
005900     INITIALIZE FEDERAL-TAX-TABLE-DATA.                           PPCTFEDE
006000     MOVE ZERO TO KCTE-ERR.                                       PPCTFEDE
006100     MOVE SPACES TO KCTE-ERRORS.                                  PPCTFEDE
006200     SET KCTE-STATUS-COMPLETED TO TRUE.                           PPCTFEDE
006300     PERFORM 1000-EDIT-KEY.                                       PPCTFEDE
006400     IF KCTE-STATUS-NOT-COMPLETED                                 PPCTFEDE
006500        GO TO 0100-END                                            PPCTFEDE
006600     END-IF.                                                      PPCTFEDE
006700     IF KCTE-ACTION-ADD                                           PPCTFEDE
006800        PERFORM 2000-ADD-FED                                      PPCTFEDE
006900     ELSE                                                         PPCTFEDE
007000       IF KCTE-ACTION-DELETE                                      PPCTFEDE
007100          PERFORM 2200-DELETE-FED                                 PPCTFEDE
007200       END-IF                                                     PPCTFEDE
007300     END-IF.                                                      PPCTFEDE
007400                                                                  PPCTFEDE
007500 0100-END.                                                        PPCTFEDE
007600                                                                  PPCTFEDE
007700     GOBACK.                                                      PPCTFEDE
007800                                                                  PPCTFEDE
007900 1000-EDIT-KEY SECTION.                                           PPCTFEDE
008000                                                                  PPCTFEDE
008100     IF FEDI-PERIOD = 'M' OR 'B' OR 'S'                           PPCTFEDE
008200        MOVE FEDI-PERIOD TO FED-PERIOD                            PPCTFEDE
008300     ELSE                                                         PPCTFEDE
008400        ADD 1                   TO KCTE-ERR                       PPCTFEDE
008500        MOVE M01184             TO KCTE-ERR-NO (KCTE-ERR)         PPCTFEDE
008600        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTFEDE
008700     END-IF.                                                      PPCTFEDE
008800     IF KCTE-ACTION-DELETE                                        PPCTFEDE
008900        GO TO 1099-EDIT-KEY-EXIT                                  PPCTFEDE
009000     END-IF.                                                      PPCTFEDE
009100     IF FEDI-MARITAL-STATUS = 'M' OR 'S' OR 'E'                   PPCTFEDE
009200        MOVE FEDI-MARITAL-STATUS TO FED-MARITAL-STATUS            PPCTFEDE
009300     ELSE                                                         PPCTFEDE
009400        ADD 1                   TO KCTE-ERR                       PPCTFEDE
009500        MOVE M01186             TO KCTE-ERR-NO (KCTE-ERR)         PPCTFEDE
009600        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTFEDE
009700     END-IF.                                                      PPCTFEDE
009800     INSPECT FEDI-AMOUNTS REPLACING ALL SPACES BY ZEROS.          PPCTFEDE
009900     IF FEDI-MIN-GROSS IS NUMERIC                                 PPCTFEDE
010000        MOVE FEDI-MIN-GROSS-N TO FED-MIN-GROSS                    PPCTFEDE
010100     ELSE                                                         PPCTFEDE
010200        ADD 1                   TO KCTE-ERR                       PPCTFEDE
010300        MOVE M01187             TO KCTE-ERR-NO (KCTE-ERR)         PPCTFEDE
010400        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTFEDE
010500     END-IF.                                                      PPCTFEDE
010600                                                                  PPCTFEDE
010700 1099-EDIT-KEY-EXIT.                                              PPCTFEDE
010800     EXIT.                                                        PPCTFEDE
010900                                                                  PPCTFEDE
011000 2000-ADD-FED SECTION.                                            PPCTFEDE
011100                                                                  PPCTFEDE
011200     PERFORM 9000-SELECT-FED.                                     PPCTFEDE
011300     IF SQLCODE = ZERO                                            PPCTFEDE
011400        ADD 1                   TO KCTE-ERR                       PPCTFEDE
011500        MOVE M01027             TO KCTE-ERR-NO (KCTE-ERR)         PPCTFEDE
011600        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTFEDE
011700        GO TO 2099-ADD-EXIT                                       PPCTFEDE
011800     END-IF.                                                      PPCTFEDE
011900     MOVE 'A'                   TO  FED-LAST-ACTION.              PPCTFEDE
012000     MOVE KCTE-LAST-ACTION-DT   TO  FED-LAST-ACTION-DT.           PPCTFEDE
012100     PERFORM 4000-FIELD-EDIT-FED.                                 PPCTFEDE
012200     IF KCTE-ERR NOT > ZERO                                       PPCTFEDE
012300        MOVE FEDI-BASE-TAX-N       TO FED-BASE-TAX                PPCTFEDE
012400        MOVE FEDI-ADDITIONAL-PCT-N TO FED-ADDITIONAL-PCT          PPCTFEDE
012500     END-IF.                                                      PPCTFEDE
012600                                                                  PPCTFEDE
012700 2099-ADD-EXIT.                                                   PPCTFEDE
012800     EXIT.                                                        PPCTFEDE
012900                                                                  PPCTFEDE
013000                                                                  PPCTFEDE
013100 2200-DELETE-FED SECTION.                                         PPCTFEDE
013200                                                                  PPCTFEDE
013300     PERFORM 9100-COUNT                                           PPCTFEDE
013400     IF WS-COUNT = ZERO                                           PPCTFEDE
013500        ADD 1                   TO KCTE-ERR                       PPCTFEDE
013600        MOVE M01029             TO KCTE-ERR-NO (KCTE-ERR)         PPCTFEDE
013700        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTFEDE
013800     END-IF.                                                      PPCTFEDE
013900                                                                  PPCTFEDE
014000 2299-DELETE-EXIT.                                                PPCTFEDE
014100     EXIT.                                                        PPCTFEDE
014200                                                                  PPCTFEDE
014300 4000-FIELD-EDIT-FED SECTION.                                     PPCTFEDE
014400                                                                  PPCTFEDE
014500     IF FEDI-MARITAL-STATUS = 'E'                                 PPCTFEDE
014600       IF FEDI-LINE NOT NUMERIC                                   PPCTFEDE
014700       OR FEDI-LINE NOT = ZERO                                    PPCTFEDE
014800          ADD 1 TO KCTE-ERR                                       PPCTFEDE
014900          MOVE M01185 TO KCTE-ERR-NO (KCTE-ERR)                   PPCTFEDE
015000       END-IF                                                     PPCTFEDE
015100       IF FEDI-MIN-GROSS      NOT = ZERO                          PPCTFEDE
015200       OR FEDI-ADDITIONAL-PCT NOT = ZERO                          PPCTFEDE
015300          ADD 1 TO KCTE-ERR                                       PPCTFEDE
015400          MOVE M01180 TO KCTE-ERR-NO (KCTE-ERR)                   PPCTFEDE
015500       END-IF                                                     PPCTFEDE
015600       IF FEDI-BASE-TAX       NOT NUMERIC                         PPCTFEDE
015700          ADD 1 TO KCTE-ERR                                       PPCTFEDE
015800          MOVE M01181 TO KCTE-ERR-NO (KCTE-ERR)                   PPCTFEDE
015900       END-IF                                                     PPCTFEDE
016000       GO TO 4999-FIELD-EDIT-FED-EXIT                             PPCTFEDE
016100     END-IF.                                                      PPCTFEDE
016200     IF FEDI-LINE NOT NUMERIC                                     PPCTFEDE
016300     OR FEDI-LINE = ZERO                                          PPCTFEDE
016400     OR FEDI-LINE-N > 11                                          PPCTFEDE
016500        ADD 1 TO KCTE-ERR                                         PPCTFEDE
016600        MOVE M01185 TO KCTE-ERR-NO (KCTE-ERR)                     PPCTFEDE
016700     END-IF.                                                      PPCTFEDE
016800     IF FEDI-BASE-TAX       NOT NUMERIC                           PPCTFEDE
016900        ADD 1 TO KCTE-ERR                                         PPCTFEDE
017000        MOVE M01183 TO KCTE-ERR-NO (KCTE-ERR)                     PPCTFEDE
017100     END-IF.                                                      PPCTFEDE
017200     IF FEDI-ADDITIONAL-PCT NOT NUMERIC                           PPCTFEDE
017300        ADD 1 TO KCTE-ERR                                         PPCTFEDE
017400        MOVE M01182 TO KCTE-ERR-NO (KCTE-ERR)                     PPCTFEDE
017500     END-IF.                                                      PPCTFEDE
017600                                                                  PPCTFEDE
017700 4999-FIELD-EDIT-FED-EXIT.                                        PPCTFEDE
017800     EXIT.                                                        PPCTFEDE
017900                                                                  PPCTFEDE
018000 9000-SELECT-FED SECTION.                                         PPCTFEDE
018100                                                                  PPCTFEDE
018200     MOVE '9000-SEL-FED ' TO DB2MSG-TAG                           PPCTFEDE
018300     EXEC SQL                                                     PPCTFEDE
018400         SELECT   FED_PERIOD                                      PPCTFEDE
018500                 ,FED_MARITAL_STATUS                              PPCTFEDE
018600                 ,FED_MIN_GROSS                                   PPCTFEDE
018700                 ,FED_BASE_TAX                                    PPCTFEDE
018800                 ,FED_ADDITIONAL_PCT                              PPCTFEDE
018900                 ,FED_LAST_ACTION                                 PPCTFEDE
019000                 ,FED_LAST_ACTION_DT                              PPCTFEDE
019100           INTO                                                   PPCTFEDE
019200                 :FED-PERIOD                                      PPCTFEDE
019300                ,:FED-MARITAL-STATUS                              PPCTFEDE
019400                ,:FED-MIN-GROSS                                   PPCTFEDE
019500                ,:FED-BASE-TAX                                    PPCTFEDE
019600                ,:FED-ADDITIONAL-PCT                              PPCTFEDE
019700                ,:FED-LAST-ACTION                                 PPCTFEDE
019800                ,:FED-LAST-ACTION-DT                              PPCTFEDE
019900           FROM   PPPVZFED_FED                                    PPCTFEDE
020000          WHERE   FED_PERIOD         = :FED-PERIOD                PPCTFEDE
020100            AND   FED_MARITAL_STATUS = :FED-MARITAL-STATUS        PPCTFEDE
020200            AND   FED_MIN_GROSS      = :FED-MIN-GROSS             PPCTFEDE
020300     END-EXEC.                                                    PPCTFEDE
020400                                                                  PPCTFEDE
020500 9099-SELECT-FED-EXIT.                                            PPCTFEDE
020600     EXIT.                                                        PPCTFEDE
020700                                                                  PPCTFEDE
020800 9100-COUNT SECTION.                                              PPCTFEDE
020900                                                                  PPCTFEDE
021000     MOVE '9100-COUNT'    TO DB2MSG-TAG.                          PPCTFEDE
021100     EXEC SQL                                                     PPCTFEDE
021200       SELECT  COUNT(*)                                           PPCTFEDE
021300       INTO    :WS-COUNT                                          PPCTFEDE
021400       FROM    PPPVZFED_FED                                       PPCTFEDE
021500       WHERE   FED_PERIOD         = :FED-PERIOD                   PPCTFEDE
021600     END-EXEC.                                                    PPCTFEDE
021700                                                                  PPCTFEDE
021800 9199-COUNT-EXIT.                                                 PPCTFEDE
021900     EXIT.                                                        PPCTFEDE
022000                                                                  PPCTFEDE
022100*999999-SQL-ERROR.                                                PPCTFEDE
022200     EXEC SQL                                                     PPCTFEDE
022300        INCLUDE CPPDXP99                                          PPCTFEDE
022400     END-EXEC.                                                    PPCTFEDE
022500     ADD 1                         TO KCTE-ERR.                   PPCTFEDE
022600     MOVE MCT005                   TO KCTE-ERR-NO (KCTE-ERR).     PPCTFEDE
022700     SET KCTE-STATUS-NOT-COMPLETED TO TRUE.                       PPCTFEDE
022800     GO TO 0100-END.                                              PPCTFEDE
