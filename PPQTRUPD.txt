000100**************************************************************/   36330704
000200*  PROGRAM: PPQTRUPD                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPQTR UPDATE MODULE                                */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPQTRUPD
001000 PROGRAM-ID. PPQTRUPD.                                            PPQTRUPD
001100 ENVIRONMENT DIVISION.                                            PPQTRUPD
001200 DATA DIVISION.                                                   PPQTRUPD
001300 WORKING-STORAGE SECTION.                                         PPQTRUPD
001400*                                                                 PPQTRUPD
001500     COPY CPWSXBEG.                                               PPQTRUPD
001600*                                                                 PPQTRUPD
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPQTRUPD'.       PPQTRUPD
001800*                                                                 PPQTRUPD
001900     EXEC SQL                                                     PPQTRUPD
002000         INCLUDE CPWSWRKA                                         PPQTRUPD
002100     END-EXEC.                                                    PPQTRUPD
002200*                                                                 PPQTRUPD
002300 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPQTRUPD
002400*                                                                 PPQTRUPD
002500 01  PPPQTR-ROW EXTERNAL.                                         PPQTRUPD
002600     EXEC SQL                                                     PPQTRUPD
002700         INCLUDE PPPVQTR1                                         PPQTRUPD
002800     END-EXEC.                                                    PPQTRUPD
002900*                                                                 PPQTRUPD
003000*------------------------------------------------------------*    PPQTRUPD
003100*  SQL DCLGEN AREA                                           *    PPQTRUPD
003200*------------------------------------------------------------*    PPQTRUPD
003300     EXEC SQL                                                     PPQTRUPD
003400         INCLUDE SQLCA                                            PPQTRUPD
003500     END-EXEC.                                                    PPQTRUPD
003600*                                                                 PPQTRUPD
003700 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPQTRUPD
003800*                                                                 PPQTRUPD
003900     COPY CPWSXEND.                                               PPQTRUPD
004000*                                                                 PPQTRUPD
004100 LINKAGE SECTION.                                                 PPQTRUPD
004200*                                                                 PPQTRUPD
004300 01  SELECT-INTERFACE-AREA.                                       PPQTRUPD
004400     COPY CPWSXHIF REPLACING ==:TAG:== BY ==XSEL==.               PPQTRUPD
004500*                                                                 PPQTRUPD
004600 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPQTRUPD
004700*                                                                 PPQTRUPD
004800     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPQTRUPD
004900     COPY  CPPDXWHC.                                              PPQTRUPD
005000*                                                                 PPQTRUPD
005100     EXEC SQL                                                     PPQTRUPD
005200         INCLUDE CPPDXE99                                         PPQTRUPD
005300     END-EXEC.                                                    PPQTRUPD
005400*                                                                 PPQTRUPD
005500     COPY CPPDUPDM.                                               PPQTRUPD
005600*                                                                 PPQTRUPD
005700 2000-UPDATE SECTION.                                             PPQTRUPD
005800*                                                                 PPQTRUPD
005900     MOVE 'UPDATE PPPQTR ROW' TO DB2MSG-TAG.                      PPQTRUPD
006000*                                                                 PPQTRUPD
006100     EXEC SQL                                                     PPQTRUPD
006200         UPDATE PPPVQTR1_QTR                                      PPQTRUPD
006300         SET DELETE_FLAG        = :DELETE-FLAG        ,           PPQTRUPD
006400             ERH_INCORRECT      = :ERH-INCORRECT                  PPQTRUPD
006500         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPQTRUPD
006600         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPQTRUPD
006700         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPQTRUPD
006800         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPQTRUPD
006900     END-EXEC.                                                    PPQTRUPD
007000*                                                                 PPQTRUPD
007100     IF SQLCODE = +100                                            PPQTRUPD
007200         INITIALIZE XSEL-RETURN-KEY                               PPQTRUPD
007300         SET XSEL-INVALID-KEY TO TRUE                             PPQTRUPD
007400     END-IF.                                                      PPQTRUPD
007500*                                                                 PPQTRUPD
007600 2000-EXIT.                                                       PPQTRUPD
007700     EXIT.                                                        PPQTRUPD
007800*                                                                 PPQTRUPD
007900 3000-INSERT SECTION.                                             PPQTRUPD
008000*                                                                 PPQTRUPD
008100     MOVE 'INSERT PPPQTR ROW' TO DB2MSG-TAG.                      PPQTRUPD
008200*                                                                 PPQTRUPD
008300     EXEC SQL                                                     PPQTRUPD
008400         INSERT                                                   PPQTRUPD
008500         INTO PPPVQTR1_QTR                                        PPQTRUPD
008600         VALUES (:PPPQTR-ROW)                                     PPQTRUPD
008700     END-EXEC.                                                    PPQTRUPD
008800*                                                                 PPQTRUPD
008900 3000-EXIT.                                                       PPQTRUPD
009000     EXIT.                                                        PPQTRUPD
009100*                                                                 PPQTRUPD
009200 4000-DELETE SECTION.                                             PPQTRUPD
009300*                                                                 PPQTRUPD
009400     MOVE 'DELETE PPPQTR ROW' TO DB2MSG-TAG.                      PPQTRUPD
009500*                                                                 PPQTRUPD
009600     EXEC SQL                                                     PPQTRUPD
009700         DELETE                                                   PPQTRUPD
009800         FROM PPPVQTR1_QTR                                        PPQTRUPD
009900         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPQTRUPD
010000         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPQTRUPD
010100         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPQTRUPD
010200         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPQTRUPD
010300     END-EXEC.                                                    PPQTRUPD
010400*                                                                 PPQTRUPD
010500     IF SQLCODE = +100                                            PPQTRUPD
010600         INITIALIZE XSEL-RETURN-KEY                               PPQTRUPD
010700         SET XSEL-INVALID-KEY TO TRUE                             PPQTRUPD
010800     END-IF.                                                      PPQTRUPD
010900*                                                                 PPQTRUPD
011000 4000-EXIT.                                                       PPQTRUPD
011100     EXIT.                                                        PPQTRUPD
011200*                                                                 PPQTRUPD
011300*999999-SQL-ERROR SECTION.                                        PPQTRUPD
011400     COPY CPPDXP99.                                               PPQTRUPD
011500     SET XSEL-DB2-ERROR TO TRUE.                                  PPQTRUPD
011600     GOBACK.                                                      PPQTRUPD
011700 999999-EXIT.                                                     PPQTRUPD
011800     EXIT.                                                        PPQTRUPD
011900*                                                                 PPQTRUPD
