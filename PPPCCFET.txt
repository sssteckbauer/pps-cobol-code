000100**************************************************************/   36090553
000200*  PROGRAM:  PPPCCFET                                        */   36090553
000300*  RELEASE # ____0553____ SERVICE REQUEST NO(S)___3609_______*/   36090553
000400*  NAME ______JAG______   MODIFICATION DATE ____04/08/91_____*/   36090553
000500*  DESCRIPTION                                               */   36090553
000600*    EDB PCC TABLE DATA ELEMENT FETCH MODULE.                */   36090553
000700**************************************************************/   36090553
000800 IDENTIFICATION DIVISION.                                         PPPCCFET
000900 PROGRAM-ID. PPPCCFET.                                            PPPCCFET
001000 AUTHOR. UCOP.                                                    PPPCCFET
001100 DATE-WRITTEN. DECEMBER 1990.                                     PPPCCFET
001200 DATE-COMPILED.                                                   PPPCCFET
001300*REMARKS.                                                         PPPCCFET
001400******************************************************************PPPCCFET
001500*                                                                *PPPCCFET
001600*  THIS PROGRAM DRIVES THE RETRIEVAL OF EMPLOYEE DATA BASE       *PPPCCFET
001700* DATA ELEMENTS, BY DATA ELEMENT NUMBER, FROM PCC-TABLE ROWS.    *PPPCCFET
001800* THIS PROGRAM IS CALLED BY PPEDBFET.                            *PPPCCFET
001900*                                                                *PPPCCFET
002000*  THE DETAILS OF THIS PROGRAMS FUNCTION ARE DESCRIBED IN THE    *PPPCCFET
002100* PROCEDURE DIVISION COPYMEMBER CPPDEFET. ADDITIONAL CODE IS     *PPPCCFET
002200* HARCODED IN THIS SOURCE TO PROVIDE THE NECESSARY MOVEMENT OF   *PPPCCFET
002300* PCC-TABLE ROW COLUMNS TO THE EDB FETCH COMPLEX DATA RETURN     *PPPCCFET
002400* AREA.                                                          *PPPCCFET
002500*                                                                *PPPCCFET
002600*  AS SHOULD BE OBVIOUS, THIS PROGRAM USES COPY CODE FOR THE     *PPPCCFET
002700* MAJORITY OF THE PROCEDURE DIVISION. THIS COPY CODE ADDRESSES   *PPPCCFET
002800* THE ROOT FUNCTIONS WHICH ARE COMMON TO THE PPXXXFET PROGRAM    *PPPCCFET
002900* SERIES.                                                        *PPPCCFET
003000*                                                                *PPPCCFET
003100******************************************************************PPPCCFET
003200     EJECT                                                        PPPCCFET
003300 ENVIRONMENT DIVISION.                                            PPPCCFET
003400 CONFIGURATION SECTION.                                           PPPCCFET
003500 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPPCCFET
003600 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPPCCFET
003700      EJECT                                                       PPPCCFET
003800******************************************************************PPPCCFET
003900*                                                                *PPPCCFET
004000*                D A T A  D I V I S I O N                        *PPPCCFET
004100*                                                                *PPPCCFET
004200******************************************************************PPPCCFET
004300 DATA DIVISION.                                                   PPPCCFET
004400 WORKING-STORAGE SECTION.                                         PPPCCFET
004500 01  MISCELLANEOUS-WS.                                            PPPCCFET
004600******************************************************************PPPCCFET
004700*                                                                *PPPCCFET
004800* NOTE:                                                          *PPPCCFET
004900*                                                                *PPPCCFET
005000*   DATA NAMES A-STND-PROG-ID AND PPXXXUTL-NAME                  *PPPCCFET
005100*  ARE REFERENCED IN COPY CODE CPPDEFET. A-STND-PROG-ID IS USED  *PPPCCFET
005200*  IN DIAGNOSTIC REFERENCES, WHILE PPXXXUTL-NAME                 *PPPCCFET
005300*  IS USED IN DYNAMIC CALLS TO THE APPROPRIATE TABLE READ        *PPPCCFET
005400*  UTILITY.                                                      *PPPCCFET
005500*                                                                *PPPCCFET
005600******************************************************************PPPCCFET
005700     05  A-STND-PROG-ID      PIC X(08) VALUE 'PPPCCFET'.          PPPCCFET
005800     05  PPXXXUTL-NAME       PIC X(08) VALUE 'PPPCCUTL'.          PPPCCFET
005900     05  WS-INIT-DATE        PIC X(10) VALUE '01/01/0001'.        PPPCCFET
006000     05  WS-ISO-INIT-DATE    PIC X(10) VALUE '0001-01-01'.        PPPCCFET
006100     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPPCCFET
006200         88  FIRST-TIME                VALUE 'Y'.                 PPPCCFET
006300         88  NOT-FIRST-TIME            VALUE 'N'.                 PPPCCFET
006400     05  INIT-VALUE-SWITCH   PIC X(01) VALUE 'Y'.                 PPPCCFET
006500         88  INIT-VALUE                VALUE 'Y'.                 PPPCCFET
006600         88  NON-INIT-VALUE            VALUE 'N'.                 PPPCCFET
006700     05  MULTIPLE-OCCURR-SW  PIC X(01) VALUE 'N'.                 PPPCCFET
006800         88  MULTIPLE-OCCURRENCE       VALUE 'Y'.                 PPPCCFET
006900         88  SINGLE-OCCURRENCE         VALUE 'N'.                 PPPCCFET
007000     05  DATE-VALUE-SWITCH   PIC X(01) VALUE 'N'.                 PPPCCFET
007100         88  NO-DATE-VALUE             VALUE 'N'.                 PPPCCFET
007200         88  ISO-DATE-VALUE            VALUE 'I'.                 PPPCCFET
007300         88  USA-DATE-VALUE            VALUE 'U'.                 PPPCCFET
007400         88  DATE-VALUE                VALUES 'I' 'U'.            PPPCCFET
007500     05  DATE-HOLD-SWITCH    PIC X(01) VALUE 'N'.                 PPPCCFET
007600     05  WS-DATA-TYPE        PIC X(01) VALUE 'A'.                 PPPCCFET
007700         88  WS-DATA-TYPE-AN           VALUE 'A'.                 PPPCCFET
007800         88  WS-DATA-TYPE-NUM          VALUE 'N'.                 PPPCCFET
007900         88  WS-DATA-TYPE-DATE         VALUE 'D'.                 PPPCCFET
008000     05  WS-EMPLOYEE-ID      PIC X(09) VALUE SPACES.              PPPCCFET
008100     05  WS-DATA-ELEM-NO     PIC 9(04) VALUE ZEROES.              PPPCCFET
008200     05  WS-OCCURRENCE-KEY.                                       PPPCCFET
008300         10  FILLER          PIC X(18) VALUE SPACES.              PPPCCFET
008400     05  WS-IX1              PIC S9(4) COMP VALUE ZERO.           PPPCCFET
008500     05  WS-IX2              PIC S9(4) COMP VALUE ZERO.           PPPCCFET
008600     05  WS-IX3              PIC S9(4) COMP VALUE ZERO.           PPPCCFET
008700     05  WS-IX4              PIC S9(4) COMP VALUE ZERO.           PPPCCFET
008800     05  WS-IX5              PIC S9(4) COMP VALUE ZERO.           PPPCCFET
008900     05  WS-IX9              PIC S9(4) COMP VALUE ZERO.           PPPCCFET
009000     SKIP3                                                        PPPCCFET
009100 01  DATE-CONVERSION-WORK-AREAS.                                  PPPCCFET
009200                                    COPY CPWSXDC2.                PPPCCFET
009300     EJECT                                                        PPPCCFET
009400 01  TEMP-DATA-AREA.                                              PPPCCFET
009500                                    COPY CPWSXMMM.                PPPCCFET
009600     EJECT                                                        PPPCCFET
009700******************************************************************PPPCCFET
009800*                                                                *PPPCCFET
009900*  PCC-ROW IS THE ROW THAT WILL BE PASSED AROUND FOR USE BY      *PPPCCFET
010000* HIGHER LEVEL PROGRAMS. IT IS DEFINED AS AN EXTERNAL DATA AREA  *PPPCCFET
010100* TO ALLOW HIGHER LEVEL PROGRAMS TO AVOID PASSING THE ROW        *PPPCCFET
010200* THROUGH UNNECESSARY CALLING CHAINS.                            *PPPCCFET
010300*                                                                *PPPCCFET
010400*  PCC-ROW-HOLD IS THE PURE ROW AS READ FROM THE PCC-TABLE. THIS *PPPCCFET
010500* ROW IS RETRIEVED BY THE CORRESPONDING PPXXXUPD MODULE, AND IS  *PPPCCFET
010600* REFERNCED IN THIS PPXXXFET MODULE FOR THE SOLE PURPOSE OF      *PPPCCFET
010700* FILLING THE CHANGE-RETURN-DATA ARRAY.                          *PPPCCFET
010800*                                                                *PPPCCFET
010900*  PCC-ROW-HOLD1 IS THE ROW AS SET ASIDE, AFTER SOME UPDATE      *PPPCCFET
011000* ACTIVITY, BY THE EDB UPDATE COMPLEX  TO ALLOW THE GENERATION   *PPPCCFET
011100* OF THE "FIRST" CHANGE ENTRY IN CHANGE RECORDS.                 *PPPCCFET
011200*                                                                *PPPCCFET
011300******************************************************************PPPCCFET
011400 01  PCC-ROW        EXTERNAL.                                     PPPCCFET
011500                                    COPY CPWSRPCC.                PPPCCFET
011600     EJECT                                                        PPPCCFET
011700 01  PCC-ROW-HOLD   EXTERNAL.                                     PPPCCFET
011800                                    COPY CPWSRPCC.                PPPCCFET
011900     EJECT                                                        PPPCCFET
012000 01  PCC-ROW-HOLD1  EXTERNAL.                                     PPPCCFET
012100                                    COPY CPWSRPCC.                PPPCCFET
012200     EJECT                                                        PPPCCFET
012300 01  PPEDBFET-REQUEST-ARRAY     EXTERNAL.                         PPPCCFET
012400                                    COPY CPWSEREQ.                PPPCCFET
012500     EJECT                                                        PPPCCFET
012600 01  PPEDBFET-D-E-RETURN-ARRAY  EXTERNAL.                         PPPCCFET
012700                                    COPY CPWSERET.                PPPCCFET
012800     EJECT                                                        PPPCCFET
012900 01  PPEDBFET-CHANGE-RETURN-ARRAY EXTERNAL.                       PPPCCFET
013000                                    COPY CPWSECHG.                PPPCCFET
013100     EJECT                                                        PPPCCFET
013200 01  PPEDBFET-RETURN-POINTERS     EXTERNAL.                       PPPCCFET
013300                                    COPY CPWSEPTR.                PPPCCFET
013400     EJECT                                                        PPPCCFET
013500 01  PPPCCUTL-INTERFACE.                                          PPPCCFET
013600                                    COPY CPLNKPCC.                PPPCCFET
013700     EJECT                                                        PPPCCFET
013800 01  XWHC-COMPILE-WORK-AREA.                                      PPPCCFET
013900                                    COPY CPWSXWHC.                PPPCCFET
014000     EJECT                                                        PPPCCFET
014100 01  PPXXXFET-INTERFACE  EXTERNAL.                                PPPCCFET
014200                                    COPY CPLNFXXX.                PPPCCFET
014300     EJECT                                                        PPPCCFET
014400 PROCEDURE DIVISION.                                              PPPCCFET
014500*                                                                 PPPCCFET
014600******************************************************************PPPCCFET
014700*            P R O C E D U R E  D I V I S I O N                  *PPPCCFET
014800******************************************************************PPPCCFET
014900*                                                                 PPPCCFET
015000******************************************************************PPPCCFET
015100*                                                                *PPPCCFET
015200*  THE PROCEDURE DIVISION OF THIS PROGRAM IS CONTAINED PRIMARILY *PPPCCFET
015300* IN COPY MEMBER CPPDEFET. HERE THE GENERIC DATA-NAMES IN        *PPPCCFET
015400* CPPDEFET ARE REPLACED WITH THE SPECIFIC DATA-NAMES OF THE      *PPPCCFET
015500* WORKING STORAGE TABLE ROWS AND THE PPXXXUTL MODULE INTERFACE.  *PPPCCFET
015600*                                                                *PPPCCFET
015700*                                                                *PPPCCFET
015800******************************************************************PPPCCFET
015900                                    COPY CPPDEFET                 PPPCCFET
016000        REPLACING PP000UTL-INTERFACE   BY PPPCCUTL-INTERFACE      PPPCCFET
016100                  PP000UTL-ERROR       BY PPPCCUTL-ERROR          PPPCCFET
016200                  PP000UTL-EMPLOYEE-ID BY PPPCCUTL-EMPLOYEE-ID    PPPCCFET
016300                  000-ROW              BY PCC-ROW.                PPPCCFET
016400     EJECT                                                        PPPCCFET
016500 7900-FIRST-TIME SECTION.                                         PPPCCFET
016600     SKIP1                                                        PPPCCFET
016700******************************************************************PPPCCFET
016800*  THIS SECTION IS REFERENCED IN PROCEDURE DIVISION COPYMEMBER   *PPPCCFET
016900* CPPDEFET AND PRESENTS THE MODULE NAME, COMPILE DATE/TIME       *PPPCCFET
017000* ON THE DISPLAY SYSOUT ONLY WHEN THIS MODULE IS CALLED FOR THE  *PPPCCFET
017100* FIRST TIME.                                                    *PPPCCFET
017200******************************************************************PPPCCFET
017300     SKIP1                                                        PPPCCFET
017400     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPPCCFET
017500     SKIP1                                                        PPPCCFET
017600                                    COPY CPPDXWHC.                PPPCCFET
017700     EJECT                                                        PPPCCFET
017800 9000-RESTORE-INDEXES SECTION.                                    PPPCCFET
017900     SKIP1                                                        PPPCCFET
018000******************************************************************PPPCCFET
018100*                                                                *PPPCCFET
018200* THIS SECTION IS INCLUDED IN THIS MODULE TO RESOLVE REFERENCES  *PPPCCFET
018300* IN CPPDEFET. SINCE THIS MODULE WORKS WITH A SINGLE DB2 EDB     *PPPCCFET
018400* PCC ROW, THERE ARE NO INDEXES THAT NEED TO BE RESTORED WHEN    *PPPCCFET
018500* PROCESSING THE "MORE-DATA" CONDITION. THEREFORE, THIS SECTION  *PPPCCFET
018600* PERFORMS THE "DUMMY" ACTION OF INITIALIZING WS-IX5.            *PPPCCFET
018700*                                                                *PPPCCFET
018800******************************************************************PPPCCFET
018900     SKIP1                                                        PPPCCFET
019000     INITIALIZE WS-IX5.                                           PPPCCFET
019100     EJECT                                                        PPPCCFET
019200 9100-EVALUATE-DATA-ELEMENT SECTION.                              PPPCCFET
019300     SKIP1                                                        PPPCCFET
019400******************************************************************PPPCCFET
019500* THIS SECTION TAKES THE DATA ELEMENT NUMBER FROM THE REQUEST    *PPPCCFET
019600* LIST AND PLACES THE APPROPRIATE PCC TABLE ROW COLUMN VALUE(S)  *PPPCCFET
019700* INTO A WORKING-STORAGE DATA AREA. THE VALUES WILL BE TESTED    *PPPCCFET
019800* BY LOGIC IN PROCEDURE DIVISION COPYMEMBER CPPDEFET TO          *PPPCCFET
019900* DETERMINE WHERE IN THE DATA-RETURN ARRAY THE VALUE(S) WILL     *PPPCCFET
020000* BE PLACED.                                                     *PPPCCFET
020100******************************************************************PPPCCFET
020200     SKIP1                                                        PPPCCFET
020300     EVALUATE WS-DATA-ELEM-NO                                     PPPCCFET
020400         WHEN '0133'                                              PPPCCFET
020500             MOVE    CREDIT-CARDTYPE    OF PCC-ROW       TO       PPPCCFET
020600                     TEMP-DATA-VALUE                              PPPCCFET
020700              IF PPXXXFET-CHANGE-REQUEST                          PPPCCFET
020800                  MOVE    CREDIT-CARDTYPE    OF PCC-ROW-HOLD  TO  PPPCCFET
020900                          ORIG-DATA-VALUE                         PPPCCFET
021000                  MOVE    CREDIT-CARDTYPE    OF PCC-ROW-HOLD1 TO  PPPCCFET
021100                          CHG1-DATA-VALUE                         PPPCCFET
021200              END-IF                                              PPPCCFET
021300         WHEN '0168'                                              PPPCCFET
021400             MOVE    CREDIT-CARDSTATUS  OF PCC-ROW       TO       PPPCCFET
021500                     TEMP-DATA-VALUE                              PPPCCFET
021600              IF PPXXXFET-CHANGE-REQUEST                          PPPCCFET
021700                  MOVE    CREDIT-CARDSTATUS  OF PCC-ROW-HOLD  TO  PPPCCFET
021800                          ORIG-DATA-VALUE                         PPPCCFET
021900                  MOVE    CREDIT-CARDSTATUS  OF PCC-ROW-HOLD1 TO  PPPCCFET
022000                          CHG1-DATA-VALUE                         PPPCCFET
022100              END-IF                                              PPPCCFET
022200         WHEN '0169'                                              PPPCCFET
022300             MOVE    CREDIT-CARDDATE    OF PCC-ROW       TO       PPPCCFET
022400                     TEMP-DATA-VALUE                              PPPCCFET
022500              IF PPXXXFET-CHANGE-REQUEST                          PPPCCFET
022600                  MOVE    CREDIT-CARDDATE    OF PCC-ROW-HOLD  TO  PPPCCFET
022700                          ORIG-DATA-VALUE                         PPPCCFET
022800                  MOVE    CREDIT-CARDDATE    OF PCC-ROW-HOLD1 TO  PPPCCFET
022900                          CHG1-DATA-VALUE                         PPPCCFET
023000              END-IF                                              PPPCCFET
023100         WHEN OTHER                                               PPPCCFET
023200              IF PPXXXFET-DIAGNOSTICS-ON                          PPPCCFET
023300                  DISPLAY A-STND-PROG-ID                          PPPCCFET
023400                          ': UNRECOGNIZED DATA ELEMENT NUMBER ('  PPPCCFET
023500                          WS-DATA-ELEM-NO                         PPPCCFET
023600                          ')'                                     PPPCCFET
023700              END-IF                                              PPPCCFET
023800              SET PPXXXFET-DATA-ELEM-NO-ERROR TO TRUE             PPPCCFET
023900              MOVE WS-DATA-ELEM-NO TO PPXXXFET-D-E-IN-ERROR       PPPCCFET
024000     END-EVALUATE.                                                PPPCCFET
024100     SKIP3                                                        PPPCCFET
024200************************   END OF PROGRAM ************************PPPCCFET
