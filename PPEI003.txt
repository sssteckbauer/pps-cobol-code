000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEI003                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    MISCELLANEOUS APPOINTMENT LEVEL MAINTENANCE             *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEI003
001200 IDENTIFICATION DIVISION.                                         PPEI003
001300 PROGRAM-ID. PPEI003.                                             PPEI003
001400 AUTHOR. BUSINESS INFORMATION TECHNOLOGY.                         PPEI003
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEI003
001600                                                                  PPEI003
001700 DATE-WRITTEN.  SEPTEMBER 1, 1992.                                PPEI003
001800 DATE-COMPILED.                                                   PPEI003
001900                                                                  PPEI003
002500 ENVIRONMENT DIVISION.                                            PPEI003
002600 CONFIGURATION SECTION.                                           PPEI003
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEI003
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEI003
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEI003
003000                                                                  PPEI003
003100 INPUT-OUTPUT SECTION.                                            PPEI003
003200 FILE-CONTROL.                                                    PPEI003
003300                                                                  PPEI003
003400 DATA DIVISION.                                                   PPEI003
003500 FILE SECTION.                                                    PPEI003
003600     EJECT                                                        PPEI003
003700*----------------------------------------------------------------*PPEI003
003800 WORKING-STORAGE SECTION.                                         PPEI003
003900*----------------------------------------------------------------*PPEI003
004000                                                                  PPEI003
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEI003
004200     'PPEI103 /111992'.                                           PPEI003
004300                                                                  PPEI003
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEI003
004500     A-STND-PROG-ID.                                              PPEI003
004600     05  FILLER                       PIC X(03).                  PPEI003
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEI003
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEI003
004900     05  FILLER                       PIC X(08).                  PPEI003
005000                                                                  PPEI003
005100 01  MISC-WORK-AREAS.                                             PPEI003
005200     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEI003
005300         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEI003
005400         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEI003
005500                                                                  PPEI003
005600                                                                  PPEI003
005700     EJECT                                                        PPEI003
005800 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEI003
005900                                                                  PPEI003
006000     EJECT                                                        PPEI003
006100 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPEI003
006200                                                                  PPEI003
006300     EJECT                                                        PPEI003
006400 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEI003
006500                                                                  PPEI003
006600     EJECT                                                        PPEI003
006700 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEI003
006800                                                                  PPEI003
006900     EJECT                                                        PPEI003
007000 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPEI003
007100                                                                  PPEI003
007200     EJECT                                                        PPEI003
007300 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEI003
007400                                                                  PPEI003
007500     EJECT                                                        PPEI003
007600 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEI003
007700                                                                  PPEI003
007800     EJECT                                                        PPEI003
007900 01  OFFSETS-TABLE.                                COPY CPWSXOST. PPEI003
008000                                                                  PPEI003
008100     EJECT                                                        PPEI003
008200******************************************************************PPEI003
008300**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEI003
008400**--------------------------------------------------------------**PPEI003
008500** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEI003
008600******************************************************************PPEI003
008700                                                                  PPEI003
008800 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEI003
008900                                                                  PPEI003
009000     EJECT                                                        PPEI003
009100 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEI003
009200                                                                  PPEI003
009300     EJECT                                                        PPEI003
009400 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEI003
009500                                                                  PPEI003
009600     EJECT                                                        PPEI003
009700 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEI003
009800                                                                  PPEI003
009900 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEI003
010000                                                                  PPEI003
010100 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEI003
010200                                                                  PPEI003
010300 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEI003
010400                                                                  PPEI003
010500     EJECT                                                        PPEI003
010600 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEI003
010700                                                                  PPEI003
010800     EJECT                                                        PPEI003
010900 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEI003
011000                                                                  PPEI003
011100     EJECT                                                        PPEI003
011200 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEI003
011300                                                                  PPEI003
011400                                                                  PPEI003
011500 01  WORK-APPT-DATA                      EXTERNAL. COPY CPWSRAPP  PPEI003
011600                                                                  PPEI003
011700         REPLACING  EMPLOYEE-ID        BY  WORK-APPT-EMPLOYEE-ID  PPEI003
011800                    APPT-NUM           BY WORK-APPT-NO            PPEI003
011900                    APPT-ADC-CODE      BY WORK-APPT-ADC-CODE      PPEI003
012000                    RATE-CODE          BY WORK-RATE-CODE          PPEI003
012100                    PAY-SCHEDULE       BY WORK-PAY-SCHED          PPEI003
012200                    TIME-REPT-CODE     BY WORK-TIME-CODE          PPEI003
012300                    APPT-WOS-IND       BY WORK-APPT-WOS           PPEI003
012400                    APPT-BEGIN-DATE    BY WORK-APPT-BEGN-DATE     PPEI003
012500                    APPT-END-DATE      BY WORK-APPT-END-DATE      PPEI003
012600                    APPT-DURATION      BY WORK-DUR-EMPLMT         PPEI003
012700                    TITLE-CODE         BY WORK-TITLE-CODE         PPEI003
012800                    APPT-TYPE          BY WORK-APPT-TYPE          PPEI003
012900                    ACADEMIC-BASIS     BY WORK-BASIS              PPEI003
013000                    WORK-STUDY-PGM     BY NULL-WORK-WORK-STYDY-PGMPPEI003
013100                    PERSONNEL-PGM      BY WORK-PERSNL-PROGRM      PPEI003
013200                    APPT-STEP          BY NULL-WORK-APPT-STEP     PPEI003
013300                    APPT-OFF-SCALE     BY NULL-WORK-APPT-OFF-SCALEPPEI003
013400                    APPT-PAID-OVER     BY WORK-PAID-OVER          PPEI003
013500                    PAY-RATE           BY WORK-ANNL-HRLY-RATE     PPEI003
013600                    LEAVE-ACRUCODE     BY WORK-LEAVE              PPEI003
013700                    RETIREMENT-CODE    BY WORK-RETIRE             PPEI003
013800                    PERCENT-FULLTIME   BY WORK-PCT-FULL-TIME      PPEI003
013900                    FIXED-VAR-CODE     BY WORK-FXD-VAR            PPEI003
014000                    TITLE-UNIT-CODE    BY WORK-PERB-CODE          PPEI003
014100                    APPT-SPCL-HNDLG    BY WORK-APPT-SHC           PPEI003
014200                    APPT-REP-CODE      BY WORK-APPT-AREP          PPEI003
014300                    APPT-DEPT          BY WORK-APPT-DEPT-CODE     PPEI003
014400                    GRADE              BY WORK-GRADE.             PPEI003
014500                                                                  PPEI003
014600     EJECT                                                        PPEI003
014700 01  AXD-ARRAY                           EXTERNAL.                PPEI003
014800                                                                  PPEI003
014900     03  FILLER           OCCURS 27.                              PPEI003
015000         07  APP-ROW-DELETE-BYTE  PIC X(1).                       PPEI003
015100             88  APP-ROW-DELETED                VALUE HIGH-VALUES.PPEI003
015200         07  APP-ROW-SEG-ID       PIC X(2).                       PPEI003
015300         07  FILLER       OCCURS 3.                               PPEI003
015400             08  APP-ROW.                          COPY CPWSRAPP  PPEI003
015500                 REPLACING WORK-STUDY-PGM                         PPEI003
015600                        BY NULL-WORK-STUDY-PGM.                   PPEI003
015700                                                                  PPEI003
015800     03  FILLER           OCCURS 27.                              PPEI003
015900         07  DIS-ROW-DELETE-BYTE  PIC X(1).                       PPEI003
016000             88  DIS-ROW-DELETED                VALUE HIGH-VALUES.PPEI003
016100         07  DIS-ROW-SEG-ID       PIC X(2).                       PPEI003
016200         07  FILLER       OCCURS 3.                               PPEI003
016300             08  DIS-ROW.                          COPY CPWSRDIS. PPEI003
016400                                                                  PPEI003
016500     EJECT                                                        PPEI003
016600******************************************************************PPEI003
016700**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEI003
016800******************************************************************PPEI003
016900                                                                  PPEI003
017000*----------------------------------------------------------------*PPEI003
017100 LINKAGE SECTION.                                                 PPEI003
017200*----------------------------------------------------------------*PPEI003
017300                                                                  PPEI003
017400******************************************************************PPEI003
017500**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEI003
017600**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEI003
017700******************************************************************PPEI003
017800                                                                  PPEI003
017900 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEI003
018000                                                                  PPEI003
018100     EJECT                                                        PPEI003
018200******************************************************************PPEI003
018300**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEI003
018400**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEI003
018500******************************************************************PPEI003
018600                                                                  PPEI003
018700 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEI003
018800                                                                  PPEI003
018900     EJECT                                                        PPEI003
019000******************************************************************PPEI003
019100**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEI003
019200******************************************************************PPEI003
019300                                                                  PPEI003
019400 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEI003
019500                          KMTA-MESSAGE-TABLE-ARRAY.               PPEI003
019600                                                                  PPEI003
019700                                                                  PPEI003
019800*----------------------------------------------------------------*PPEI003
019900 0000-DRIVER.                                                     PPEI003
020000*----------------------------------------------------------------*PPEI003
020100                                                                  PPEI003
020200******************************************************************PPEI003
020300**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEI003
020400******************************************************************PPEI003
020500                                                                  PPEI003
020600     IF  THIS-IS-THE-FIRST-TIME                                   PPEI003
020700         PERFORM 0100-INITIALIZE                                  PPEI003
020800     END-IF.                                                      PPEI003
020900                                                                  PPEI003
021000******************************************************************PPEI003
021100**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEI003
021200**  TO THE CALLING PROGRAM                                      **PPEI003
021300******************************************************************PPEI003
021400                                                                  PPEI003
021500     PERFORM 1000-MAINLINE-ROUTINE.                               PPEI003
021600                                                                  PPEI003
021700     EXIT PROGRAM.                                                PPEI003
021800                                                                  PPEI003
021900                                                                  PPEI003
022000     EJECT                                                        PPEI003
022100*----------------------------------------------------------------*PPEI003
022200 0100-INITIALIZE    SECTION.                                      PPEI003
022300*----------------------------------------------------------------*PPEI003
022400                                                                  PPEI003
022500******************************************************************PPEI003
022600**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEI003
022700******************************************************************PPEI003
022800                                                                  PPEI003
022900     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEI003
023000                                                                  PPEI003
023100*                                               *-------------*   PPEI003
023200                                                 COPY CPPDXWHC.   PPEI003
023300*                                               *-------------*   PPEI003
023400                                                                  PPEI003
023500*                                               *-------------*   PPEI003
023600                                                 COPY CPPDDATE.   PPEI003
023700*                                               *-------------*   PPEI003
023800                                                                  PPEI003
023900     MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 PPEI003
024000                                                                  PPEI003
024100******************************************************************PPEI003
024200**   SET FIRST CALL SWITCH OFF                                  **PPEI003
024300******************************************************************PPEI003
024400                                                                  PPEI003
024500     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEI003
024600                                                                  PPEI003
024700                                                                  PPEI003
024800     EJECT                                                        PPEI003
024900*----------------------------------------------------------------*PPEI003
025000 1000-MAINLINE-ROUTINE SECTION.                                   PPEI003
025100*----------------------------------------------------------------*PPEI003
025200******************************************************************PPEI003
025300** CONSISTENCY EDIT 03 - MISCELLANEOUS APPOINTMENT LEVEL        **PPEI003
025400**                       MAINTENANCE                            **PPEI003
025500******************************************************************PPEI003
025600******************************************************************PPEI003
025700**  SINCE THERE IS A CHANGE TO THE EMPLOYEE'S APPOINTMENTS, WE  **PPEI003
025800**  SET THE FLAG SO THAT WE WILL CHECK FOR EXECUTIVE LIFE ELIG- **PPEI003
025900**  IBILITY IN 6500-MAINT-ALWAYS-PERFORMED.                     **PPEI003
026000******************************************************************PPEI003
026100                                                                  PPEI003
026200     MOVE  'Y'                    TO CHECK-EXEC-LIFE-ELIG-FLAG.   PPEI003
026300                                                                  PPEI003
026400******************************************************************PPEI003
026500** FIRST MOVE APPOINTMENT DATA AND APPOINTMENT'S BASE DATA      **PPEI003
026600** DATA ELEMENT NUMBER TO WORK AREAS TO MINIMIZE USE OF DOUBLE  **PPEI003
026700** SUBSCRIPTS.                                                  **PPEI003
026800******************************************************************PPEI003
026900                                                                  PPEI003
027000     MOVE APP-ROW      (APPT-PNTR, 1)                             PPEI003
027100                                  TO WORK-APPT-DATA.              PPEI003
027200                                                                  PPEI003
027300******************************************************************PPEI003
027400**                CHECK APPOINTMENT END DATE                    **PPEI003
027500**--------------------------------------------------------------**PPEI003
027600** IF  THE DURATION OF EMPLOYMENT IS INDEFINITE OR TENURED, THE **PPEI003
027700** APPOINTMENT END DATE IS SET TO 999999.  ALSO IF  THE DURATION *PPEI003
027800** OF EMPLOYMENT CODE IS 'E' FOR 'NO SPECIFIC END DATE'.        **PPEI003
027900******************************************************************PPEI003
028000                                                                  PPEI003
028100     IF          WORK-APPT-END-DATE NOT = '9999-12-31'            PPEI003
028200         AND (   WORK-DUR-EMPLMT        = 'I'                     PPEI003
028300              OR WORK-DUR-EMPLMT        = 'T'                     PPEI003
028400              OR WORK-DUR-EMPLMT        = 'E' )                   PPEI003
028500                                                                  PPEI003
028600         MOVE '9999-12-31'        TO APPT-END-DATE (APPT-PNTR, 1) PPEI003
028700                                                                  PPEI003
028800         COMPUTE DL-FIELD-R = ADA-SEG-BASE (APPT-PNTR, 1)         PPEI003
028900                            + OFF-2003-APPT-END-DATE              PPEI003
029000                                                                  PPEI003
029100         PERFORM 9050-AUDITING-RESPONSIBILITIES                   PPEI003
029200                                                                  PPEI003
029300     END-IF.                                                      PPEI003
029400                                                                  PPEI003
029500     EJECT                                                        PPEI003
029600*----------------------------------------------------------------*PPEI003
029700 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEI003
029800*----------------------------------------------------------------*PPEI003
029900                                                                  PPEI003
030000*                                                 *-------------* PPEI003
030100                                                   COPY CPPDXDEC. PPEI003
030200*                                                 *-------------* PPEI003
030300                                                                  PPEI003
030400*----------------------------------------------------------------*PPEI003
030500 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEI003
030600*----------------------------------------------------------------*PPEI003
030700                                                                  PPEI003
030800*                                                 *-------------* PPEI003
030900                                                   COPY CPPDADLC. PPEI003
031000*                                                 *-------------* PPEI003
031100                                                                  PPEI003
031200     EJECT                                                        PPEI003
031300*----------------------------------------------------------------*PPEI003
031400 9300-DATE-CONVERSION-DB2  SECTION.                               PPEI003
031500*----------------------------------------------------------------*PPEI003
031600                                                                  PPEI003
031700*                                                 *-------------* PPEI003
031800                                                   COPY CPPDXDC2. PPEI003
031900*                                                 *-------------* PPEI003
032000                                                                  PPEI003
032100******************************************************************PPEI003
032200**   E N D  S O U R C E   ----  PPEI003 ----                    **PPEI003
032300******************************************************************PPEI003
