000100**************************************************************/   30871465
000200*  PROGRAM: PPCTT24                                          */   30871465
000300*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000400*  NAME:___SRS___________ CREATION DATE:      ___02/11/03__  */   30871465
000500*  DESCRIPTION:                                              */   30871465
000600*  PPPWSP/WSF TABLE TRANSACTION ASSEMBLY MODULE              */   30871465
000700**************************************************************/   30871465
000800 IDENTIFICATION DIVISION.                                         PPCTT24
000900 PROGRAM-ID. PPCTT24.                                             PPCTT24
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT24
001100 DATE-COMPILED.                                                   PPCTT24
001200 DATA DIVISION.                                                   PPCTT24
001300 FILE SECTION.                                                    PPCTT24
001400 WORKING-STORAGE SECTION.                                         PPCTT24
001500                                                                  PPCTT24
001600 01  WA-WORK-VARIABLES.                                           PPCTT24
001700     05  WA-READ-ERROR.                                           PPCTT24
001800         10 FILLER                     PIC X(37) VALUE            PPCTT24
001900                 'PPCTLTRD ERROR ON TRANS CALL: RETURN='.         PPCTT24
002000         10 WA-RTN-CODE                PIC XX.                    PPCTT24
002100         10 FILLER                     PIC X(07) VALUE            PPCTT24
002200                 ' COUNT='.                                       PPCTT24
002300         10 WA-WORK-Z5                 PIC ZZ,ZZ9.                PPCTT24
002400 01  WB-PROGRAM-CONSTANTS.                                        PPCTT24
002500     05  WB-TABLE-NO                   PIC X(2) VALUE '24'.       PPCTT24
002600 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT24
002700     05  WC-ERR-IX                     PIC S9(04) VALUE ZERO.     PPCTT24
002800     05  WC-SET-CNT                    PIC S9(04) VALUE ZERO.     PPCTT24
002900     05  WC-TALLY                      PIC S9(04) VALUE ZERO.     PPCTT24
003000 01  WF-PROGRAM-FLAGS.                                            PPCTT24
003100     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT24
003200         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT24
003300         88  WF-ACCEPT-SET                      VALUES 00 THRU 04.PPCTT24
003400         88  WF-REJECT-SET                      VALUES 05 THRU 99.PPCTT24
003500 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT24
003600     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT24
003700     05  MCT020                     PIC X(05) VALUE 'CT020'.      PPCTT24
003800     05  MCT021                     PIC X(05) VALUE 'CT021'.      PPCTT24
003900     05  MCT022                     PIC X(05) VALUE 'CT022'.      PPCTT24
004000     05  MCT023                     PIC X(05) VALUE 'CT023'.      PPCTT24
004100 01  WS-SET-WORK-AREA.                                            PPCTT24
004200     05  WS-VALID-NEXT-SEQ-NOS      PIC X(03) VALUE LOW-VALUES.   PPCTT24
004300     05  WS-SET-END-FLAG            PIC X(01) VALUE 'N'.          PPCTT24
004400         88  WS-SET-END-NOT-OK                VALUE 'N'.          PPCTT24
004500         88  WS-SET-END-OK                    VALUE 'Y'.          PPCTT24
004600     05  WS-SET-STATUS-FLAG         PIC X(01) VALUE 'N'.          PPCTT24
004700         88  WS-SET-OK                        VALUE 'Y'.          PPCTT24
004800         88  WS-SET-NOT-OK                    VALUE 'N'.          PPCTT24
004900     05  WS-SET-KEY.                                              PPCTT24
005000         10  WS-SET-DB2-TBL         PIC X(03).                    PPCTT24
005100         10  WS-SET-ACTION          PIC X(01).                    PPCTT24
005200             88  WS-ADD-SET                   VALUE 'A'.          PPCTT24
005300             88  WS-CHANGE-SET                VALUE 'C'.          PPCTT24
005400             88  WS-DELETE-SET                VALUE 'D'.          PPCTT24
005500         10  WS-SET-PROGRAM-CODE    PIC X(01).                    PPCTT24
005600         10  WS-SET-FUNDING-FAU     PIC X(30).                    PPCTT24
005700         10  WS-SET-BEGIN-DATE      PIC X(06).                    PPCTT24
005800     05  WS-HOLD-KEY VALUE LOW-VALUES.                            PPCTT24
005900         10 WS-HOLD-KEY-DB2-TBL     PIC X(03).                    PPCTT24
006000         10 WS-HOLD-KEY-DATA        PIC X(38).                    PPCTT24
006100 01  WORK-STUDY-TABLE-TRANS.                                      PPCTT24
006200     05  WSPT-ACTION-CODE           PIC X(01).                    PPCTT24
006300         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT24
006400         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT24
006500         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT24
006600         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT24
006700     05  FILLER                     PIC X(02).                    PPCTT24
006800     05  WSPT-PROGRAM-CODE          PIC X(01).                    PPCTT24
006900     05  WSPT-TRAN-SEQ-NO           PIC X(01).                    PPCTT24
007000         88  WSPT-TRAN-SEQ-NO-VALID     VALUES '1' '2' '3'.       PPCTT24
007100     05  WSPT-DATA-1.                                             PPCTT24
007200         10 WSPT-CATEG-CODE         PIC X(01).                    PPCTT24
007300         10 WSPT-BENEFITS-IND       PIC X(01).                    PPCTT24
007400         10 WSPT-PGM-END-DATE       PIC X(06).                    PPCTT24
007500         10 WSPT-PROGRAM-NAME       PIC X(30).                    PPCTT24
007600         10 FILLER                  PIC X(37).                    PPCTT24
007700     05  WSPT-DATA-2 REDEFINES WSPT-DATA-1.                       PPCTT24
007800         10 WSPT-APPROP-FAU         PIC X(30).                    PPCTT24
007900         10 WSPT-GL-DESC            PIC X(18).                    PPCTT24
008000         10 FILLER                  PIC X(27).                    PPCTT24
008100     05  WSPT-DATA-3 REDEFINES WSPT-DATA-1.                       PPCTT24
008200         10 WSPT-BEGIN-DATE         PIC X(06).                    PPCTT24
008300         10 WSPT-FUNDING-FAU        PIC X(30).                    PPCTT24
008400         10 WSPT-SPLIT-PERCENT.                                   PPCTT24
008500            15 WSPT-SPLIT-PERCENT-9 PIC 9V9999.                   PPCTT24
008600         10 FILLER                  PIC X(34).                    PPCTT24
008700                                                                  PPCTT24
008800 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT24
008900 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT24
009000 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT24
009100 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT24
009200 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT24
009300 01  WORK-STUDY-WSP-TABLE-INPUT.       COPY CPCTWSPI.             PPCTT24
009400 01  WORK-STUDY-WSP-TABLE-DATA.                                   PPCTT24
009500     EXEC SQL                                                     PPCTT24
009600         INCLUDE PPPVZWSP                                         PPCTT24
009700     END-EXEC.                                                    PPCTT24
009800 01  WORK-STUDY-WSF-TABLE-INPUT.       COPY CPCTWSFI.             PPCTT24
009900 01  WORK-STUDY-WSF-TABLE-DATA.                                   PPCTT24
010000     EXEC SQL                                                     PPCTT24
010100         INCLUDE PPPVZWSF                                         PPCTT24
010200     END-EXEC.                                                    PPCTT24
010300                                                                  PPCTT24
010400 LINKAGE SECTION.                                                 PPCTT24
010500                                                                  PPCTT24
010600 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT24
010700                                                                  PPCTT24
010800 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE.           PPCTT24
010900                                                                  PPCTT24
011000 0000-MAIN SECTION.                                               PPCTT24
011500                                                                  PPCTT24
011600     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT24
011700     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT24
011800       UNTIL CSRI-TABLE-TRANS-END.                                PPCTT24
011900     PERFORM 3000-FINISH-PREVIOUS-SET.                            PPCTT24
012000                                                                  PPCTT24
012100 0099-END.                                                        PPCTT24
012200                                                                  PPCTT24
012300     GOBACK.                                                      PPCTT24
012400                                                                  PPCTT24
012500 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT24
012600                                                                  PPCTT24
012700     MOVE 'PPCTT24'             TO XWHC-DISPLAY-MODULE.           PPCTT24
012800     COPY CPPDXWHC.                                               PPCTT24
012900     SET CTTI-NORMAL            TO TRUE.                          PPCTT24
013000     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTT24
013100     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTT24
013200                                   CTRI-TRANSACTION               PPCTT24
013300                                   CTRI-TRANSACTION-DISPO         PPCTT24
013400                                   CTRI-TEXT-LINE                 PPCTT24
013500                                   CTRI-MESSAGE-NUMBER.           PPCTT24
013600     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTT24
013700                                   CTTI-ERROR-SEVERITY.           PPCTT24
013800     MOVE WB-TABLE-NO           TO CSRI-CALL-TYPE-NUM.            PPCTT24
013900     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT24
014000                                                                  PPCTT24
014100 1099-INITIALIZATION-EXIT.                                        PPCTT24
014200     EXIT.                                                        PPCTT24
014300                                                                  PPCTT24
014400 2000-PROCESS-TRANSACTIONS SECTION.                               PPCTT24
014500                                                                  PPCTT24
014600     IF WS-SET-KEY NOT = WS-HOLD-KEY                              PPCTT24
014700        PERFORM 3000-FINISH-PREVIOUS-SET                          PPCTT24
014800        PERFORM 2100-BEGIN-NEW-SET                                PPCTT24
014900     END-IF.                                                      PPCTT24
015000     MOVE WORK-STUDY-TABLE-TRANS TO CTRI-TRANSACTION.             PPCTT24
015100     MOVE WSPT-ACTION-CODE TO KCTE-ACTION.                        PPCTT24
015200     ADD 1 TO WC-SET-CNT.                                         PPCTT24
015300     IF KCTE-ACTION = 'D'                                         PPCTT24
015400        SET WS-SET-END-OK TO TRUE                                 PPCTT24
015500        PERFORM 9100-CALL-PPCTLRPT                                PPCTT24
015600        PERFORM 9300-FETCH-NEXT-TRANSACTION                       PPCTT24
015700        GO TO 2099-EXIT                                           PPCTT24
015800     END-IF.                                                      PPCTT24
015900     IF WSPT-TRAN-SEQ-NO-VALID                                    PPCTT24
016000        MOVE ZERO TO WC-TALLY                                     PPCTT24
016100        INSPECT WS-VALID-NEXT-SEQ-NOS TALLYING WC-TALLY           PPCTT24
016200            FOR ALL WSPT-TRAN-SEQ-NO                              PPCTT24
016300        IF WC-TALLY = ZERO                                        PPCTT24
016400            MOVE MCT021 TO CTRI-MESSAGE-NUMBER                    PPCTT24
016500            PERFORM 9100-CALL-PPCTLRPT                            PPCTT24
016600            SET WS-SET-NOT-OK TO TRUE                             PPCTT24
016700        ELSE                                                      PPCTT24
016800            PERFORM 2200-MOVE-TRANSACTION-DATA                    PPCTT24
016900        END-IF                                                    PPCTT24
017000        EVALUATE TRUE                                             PPCTT24
017100          WHEN WS-ADD-SET                                         PPCTT24
017200            EVALUATE WSPT-TRAN-SEQ-NO                             PPCTT24
017300              WHEN '1'                                            PPCTT24
017400                  MOVE '2  ' TO WS-VALID-NEXT-SEQ-NOS             PPCTT24
017500                  SET WS-SET-END-OK TO TRUE                       PPCTT24
017600              WHEN '2'                                            PPCTT24
017700                  MOVE '   ' TO WS-VALID-NEXT-SEQ-NOS             PPCTT24
017800                  SET WS-SET-END-OK TO TRUE                       PPCTT24
017900              WHEN '3'                                            PPCTT24
018000                  MOVE '   ' TO WS-VALID-NEXT-SEQ-NOS             PPCTT24
018100                  SET WS-SET-END-OK TO TRUE                       PPCTT24
018200            END-EVALUATE                                          PPCTT24
018300          WHEN WS-CHANGE-SET                                      PPCTT24
018400            SET WS-SET-END-OK TO TRUE                             PPCTT24
018500        END-EVALUATE                                              PPCTT24
018600        IF CTRI-TRANSACTION NOT = SPACES                          PPCTT24
018700            PERFORM 9100-CALL-PPCTLRPT                            PPCTT24
018800        END-IF                                                    PPCTT24
018900     ELSE                                                         PPCTT24
019000        MOVE MCT020 TO CTRI-MESSAGE-NUMBER                        PPCTT24
019100        PERFORM 9100-CALL-PPCTLRPT                                PPCTT24
019200        SET WS-SET-NOT-OK TO TRUE                                 PPCTT24
019300     END-IF.                                                      PPCTT24
019400                                                                  PPCTT24
019500     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT24
019600                                                                  PPCTT24
019700 2099-EXIT.                                                       PPCTT24
019800     EXIT.                                                        PPCTT24
019900                                                                  PPCTT24
020000 2100-BEGIN-NEW-SET SECTION.                                      PPCTT24
020100                                                                  PPCTT24
020200     MOVE WS-SET-KEY TO WS-HOLD-KEY.                              PPCTT24
020300     SET WS-SET-OK TO TRUE.                                       PPCTT24
020400     SET WS-SET-END-NOT-OK TO TRUE.                               PPCTT24
020500     MOVE ZERO TO WC-SET-CNT.                                     PPCTT24
020600     MOVE SPACES TO WORK-STUDY-WSP-TABLE-INPUT                    PPCTT24
020700                    WORK-STUDY-WSF-TABLE-INPUT.                   PPCTT24
020800     MOVE WSPT-PROGRAM-CODE TO WSPI-PROGRAM-CODE                  PPCTT24
020900                               WSFI-PROGRAM-CODE.                 PPCTT24
021000     IF WSPT-TRAN-SEQ-NO = '3'                                    PPCTT24
021100        MOVE WSPT-BEGIN-DATE  TO WSFI-BEGIN-DATE                  PPCTT24
021200        MOVE WSPT-FUNDING-FAU TO WSFI-FUNDING-FAU                 PPCTT24
021300     END-IF.                                                      PPCTT24
021400     EVALUATE TRUE                                                PPCTT24
021500       WHEN WS-ADD-SET                                            PPCTT24
021600           MOVE '13 ' TO WS-VALID-NEXT-SEQ-NOS                    PPCTT24
021700       WHEN WS-CHANGE-SET                                         PPCTT24
021800           MOVE '123' TO WS-VALID-NEXT-SEQ-NOS                    PPCTT24
021900     END-EVALUATE.                                                PPCTT24
022000                                                                  PPCTT24
022100 2199-EXIT.                                                       PPCTT24
022200     EXIT.                                                        PPCTT24
022300                                                                  PPCTT24
022400 2200-MOVE-TRANSACTION-DATA SECTION.                              PPCTT24
022500                                                                  PPCTT24
022600     EVALUATE WSPT-TRAN-SEQ-NO                                    PPCTT24
022700      WHEN '1'                                                    PPCTT24
022800        IF WSPT-CATEG-CODE      NOT = SPACES                      PPCTT24
022900           MOVE WSPT-CATEG-CODE        TO WSPI-CATEG-CODE         PPCTT24
023000        END-IF                                                    PPCTT24
023100        IF WSPT-BENEFITS-IND    NOT = SPACES                      PPCTT24
023200           MOVE WSPT-BENEFITS-IND      TO WSPI-BENEFITS-IND       PPCTT24
023300        END-IF                                                    PPCTT24
023400        IF WSPT-PGM-END-DATE    NOT = SPACES                      PPCTT24
023500           MOVE WSPT-PGM-END-DATE      TO WSPI-PGM-END-DATE       PPCTT24
023600        END-IF                                                    PPCTT24
023700        IF WSPT-PROGRAM-NAME    NOT = SPACES                      PPCTT24
023800           MOVE WSPT-PROGRAM-NAME      TO WSPI-PROGRAM-NAME       PPCTT24
023900        END-IF                                                    PPCTT24
024000      WHEN '2'                                                    PPCTT24
024100        IF WSPT-APPROP-FAU      NOT = SPACES                      PPCTT24
024200           MOVE WSPT-APPROP-FAU        TO WSPI-APPROP-FAU         PPCTT24
024300        END-IF                                                    PPCTT24
024400        IF WSPT-GL-DESC         NOT = SPACES                      PPCTT24
024500           MOVE WSPT-GL-DESC           TO WSPI-GL-DESC            PPCTT24
024600        END-IF                                                    PPCTT24
024700      WHEN '3'                                                    PPCTT24
024800        IF WSPT-SPLIT-PERCENT   NOT = SPACES                      PPCTT24
024900           MOVE WSPT-SPLIT-PERCENT     TO WSFI-SPLIT-PERCENT      PPCTT24
025000        END-IF                                                    PPCTT24
025100     END-EVALUATE.                                                PPCTT24
025200                                                                  PPCTT24
025300 2299-EXIT.                                                       PPCTT24
025400     EXIT.                                                        PPCTT24
025500                                                                  PPCTT24
025600 3000-FINISH-PREVIOUS-SET SECTION.                                PPCTT24
025700                                                                  PPCTT24
025800     IF WS-HOLD-KEY NOT = LOW-VALUES                              PPCTT24
025900       IF WS-SET-END-NOT-OK                                       PPCTT24
026000          MOVE MCT023 TO CTRI-MESSAGE-NUMBER                      PPCTT24
026100          PERFORM 9100-CALL-PPCTLRPT                              PPCTT24
026200          SET WS-SET-NOT-OK TO TRUE                               PPCTT24
026300       END-IF                                                     PPCTT24
026400       IF WS-SET-OK                                               PPCTT24
026500          SET WF-NO-ERRORS TO TRUE                                PPCTT24
026600          MOVE CTTI-ACTION-DATE TO KCTE-LAST-ACTION-DT            PPCTT24
026700          IF WS-HOLD-KEY-DB2-TBL = 'WSP'                          PPCTT24
026800             CALL 'PPCTWSPE' USING CONTROL-TABLE-EDIT-INTERFACE   PPCTT24
026900                                   WORK-STUDY-WSP-TABLE-INPUT     PPCTT24
027000                                   WORK-STUDY-WSP-TABLE-DATA      PPCTT24
027100          ELSE                                                    PPCTT24
027200             CALL 'PPCTWSFE' USING CONTROL-TABLE-EDIT-INTERFACE   PPCTT24
027300                                   WORK-STUDY-WSF-TABLE-INPUT     PPCTT24
027400                                   WORK-STUDY-WSF-TABLE-DATA      PPCTT24
027500          END-IF                                                  PPCTT24
027600          IF KCTE-ERR > ZERO                                      PPCTT24
027700             PERFORM 3100-PRINT-ERROR                             PPCTT24
027800               VARYING WC-ERR-IX FROM 1 BY 1                      PPCTT24
027900                 UNTIL WC-ERR-IX > KCTE-ERR                       PPCTT24
028000             IF WF-REJECT-SET OR KCTE-STATUS-NOT-COMPLETED        PPCTT24
028100                MOVE MCT022 TO CTRI-MESSAGE-NUMBER                PPCTT24
028200                SET WS-SET-NOT-OK TO TRUE                         PPCTT24
028300             END-IF                                               PPCTT24
028400          END-IF                                                  PPCTT24
028500       END-IF                                                     PPCTT24
028600       IF WS-SET-OK                                               PPCTT24
028700          MOVE 'SET ACCEPTED' TO CTRI-TEXT-DISPO                  PPCTT24
028800          ADD WC-SET-CNT TO CTTI-TRANS-ACCEPTED                   PPCTT24
028900          PERFORM 3200-UPDATE-DB2-TABLE                           PPCTT24
029000       ELSE                                                       PPCTT24
029100          MOVE 'SET REJECTED' TO CTRI-TEXT-DISPO                  PPCTT24
029200          ADD WC-SET-CNT TO CTTI-TRANS-REJECTED                   PPCTT24
029300       END-IF                                                     PPCTT24
029400       PERFORM 9100-CALL-PPCTLRPT                                 PPCTT24
029500       MOVE 2 TO CTRI-SPACING-OVERRIDE                            PPCTT24
029600     END-IF.                                                      PPCTT24
029700                                                                  PPCTT24
029800 3099-EXIT.                                                       PPCTT24
029900     EXIT.                                                        PPCTT24
030000                                                                  PPCTT24
030100 3100-PRINT-ERROR SECTION.                                        PPCTT24
030200                                                                  PPCTT24
030300     MOVE KCTE-ERR-NO (WC-ERR-IX) TO CTRI-MESSAGE-NUMBER.         PPCTT24
030400     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT24
030500     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT24
030600         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT24
030700     END-IF.                                                      PPCTT24
030800                                                                  PPCTT24
030900 3199-EXIT.                                                       PPCTT24
031000     EXIT.                                                        PPCTT24
031100                                                                  PPCTT24
031200 3200-UPDATE-DB2-TABLE SECTION.                                   PPCTT24
031300                                                                  PPCTT24
031400     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT24
031500     IF WS-HOLD-KEY-DB2-TBL = 'WSP'                               PPCTT24
031600        CALL 'PPCTWSPU' USING CONTROL-TABLE-UPDT-INTERFACE        PPCTT24
031700                              WORK-STUDY-WSP-TABLE-DATA           PPCTT24
031800        IF NOT KCTU-STATUS-OK                                     PPCTT24
031900           GO TO 3300-BAD-UPDATE-CALL                             PPCTT24
032000        END-IF                                                    PPCTT24
032100     ELSE                                                         PPCTT24
032200        CALL 'PPCTWSFU' USING CONTROL-TABLE-UPDT-INTERFACE        PPCTT24
032300                              WORK-STUDY-WSF-TABLE-DATA           PPCTT24
032400        IF NOT KCTU-STATUS-OK                                     PPCTT24
032500           GO TO 3400-BAD-UPDATE-CALL                             PPCTT24
032600        END-IF                                                    PPCTT24
032700     END-IF.                                                      PPCTT24
032800     EVALUATE TRUE                                                PPCTT24
032900       WHEN KCTU-ACTION-ADD                                       PPCTT24
033000         ADD 1 TO CTTI-ENTRIES-ADDED                              PPCTT24
033100       WHEN KCTU-ACTION-CHANGE                                    PPCTT24
033200         ADD 1 TO CTTI-ENTRIES-UPDATED                            PPCTT24
033300       WHEN KCTU-ACTION-DELETE                                    PPCTT24
033400         ADD 1 TO CTTI-ENTRIES-DELETED                            PPCTT24
033500       WHEN OTHER                                                 PPCTT24
033600         GO TO 3300-BAD-UPDATE-CALL                               PPCTT24
033700     END-EVALUATE.                                                PPCTT24
033800                                                                  PPCTT24
033900 3299-EXIT.                                                       PPCTT24
034000     EXIT.                                                        PPCTT24
034100                                                                  PPCTT24
034200 3300-BAD-UPDATE-CALL SECTION.                                    PPCTT24
034300                                                                  PPCTT24
034400     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT24
034500     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT24
034600     STRING 'PPCTWSPU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT24
034700            '" STATUS WAS "'               KCTU-STATUS            PPCTT24
034800            '" COUNT='                     WA-WORK-Z5             PPCTT24
034900            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT24
035000     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT24
035100     GO TO 9999-ABEND.                                            PPCTT24
035200                                                                  PPCTT24
035300 3399-EXIT.                                                       PPCTT24
035400     EXIT.                                                        PPCTT24
035500                                                                  PPCTT24
035600 3400-BAD-UPDATE-CALL SECTION.                                    PPCTT24
035700                                                                  PPCTT24
035800     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT24
035900     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT24
036000     STRING 'PPCTWSFU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT24
036100            '" STATUS WAS "'               KCTU-STATUS            PPCTT24
036200            '" COUNT='                     WA-WORK-Z5             PPCTT24
036300            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT24
036400     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT24
036500     GO TO 9999-ABEND.                                            PPCTT24
036600                                                                  PPCTT24
036700 3499-EXIT.                                                       PPCTT24
036800     EXIT.                                                        PPCTT24
036900                                                                  PPCTT24
037000 9100-CALL-PPCTLRPT SECTION.                                      PPCTT24
037100                                                                  PPCTT24
037200     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT24
037300     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT24
037400         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT24
037500     END-IF.                                                      PPCTT24
037600     IF CTRI-RUN-ABORTED                                          PPCTT24
037700        GO TO 9999-ABEND                                          PPCTT24
037800     END-IF.                                                      PPCTT24
037900                                                                  PPCTT24
038000 9199-EXIT.                                                       PPCTT24
038100     EXIT.                                                        PPCTT24
038200                                                                  PPCTT24
038300 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT24
038400                                                                  PPCTT24
038500     PERFORM WITH TEST AFTER                                      PPCTT24
038600       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT24
038700          OR CSRI-TRANSACTION(1:1) NOT = '*'                      PPCTT24
038800      CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE        PPCTT24
038900      IF CSRI-NORMAL                                              PPCTT24
039000         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT24
039100         IF CSRI-TRANSACTION(1:1) = '*'                           PPCTT24
039200            MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION             PPCTT24
039300            MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO     PPCTT24
039400            PERFORM 9100-CALL-PPCTLRPT                            PPCTT24
039500            ADD 1 TO CTTI-TRANS-ACCEPTED                          PPCTT24
039600         ELSE                                                     PPCTT24
039700            MOVE CSRI-TRANSACTION    TO WORK-STUDY-TABLE-TRANS    PPCTT24
039800            MOVE WSPT-ACTION-CODE    TO WS-SET-ACTION             PPCTT24
039900            MOVE WSPT-PROGRAM-CODE   TO WS-SET-PROGRAM-CODE       PPCTT24
040000            IF WSPT-TRAN-SEQ-NO = '3'                             PPCTT24
040100               MOVE 'WSF'            TO WS-SET-DB2-TBL            PPCTT24
040200               MOVE WSPT-FUNDING-FAU TO WS-SET-FUNDING-FAU        PPCTT24
040300               MOVE WSPT-BEGIN-DATE  TO WS-SET-BEGIN-DATE         PPCTT24
040400            ELSE                                                  PPCTT24
040500               MOVE 'WSP'            TO WS-SET-DB2-TBL            PPCTT24
040600               MOVE SPACES           TO WS-SET-FUNDING-FAU        PPCTT24
040700                                        WS-SET-BEGIN-DATE         PPCTT24
040800            END-IF                                                PPCTT24
040900         END-IF                                                   PPCTT24
041000      ELSE                                                        PPCTT24
041100        IF NOT CSRI-TABLE-TRANS-END                               PPCTT24
041200           MOVE MCT001              TO CTRI-MESSAGE-NUMBER        PPCTT24
041300           MOVE CSRI-RETURN-CODE    TO WA-RTN-CODE                PPCTT24
041400           MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                PPCTT24
041500           MOVE WA-READ-ERROR       TO CTRI-TEXT-LINE             PPCTT24
041600           PERFORM 9100-CALL-PPCTLRPT                             PPCTT24
041700           GO TO 9999-ABEND                                       PPCTT24
041800        END-IF                                                    PPCTT24
041900      END-IF                                                      PPCTT24
042000     END-PERFORM.                                                 PPCTT24
042100                                                                  PPCTT24
042200 9399-EXIT.                                                       PPCTT24
042300     EXIT.                                                        PPCTT24
042400                                                                  PPCTT24
042500 9999-ABEND SECTION.                                              PPCTT24
042600                                                                  PPCTT24
042700     SET CTTI-ABORT TO TRUE.                                      PPCTT24
042800     GOBACK.                                                      PPCTT24
042900*************************END OF SOURCE CODE***********************PPCTT24
