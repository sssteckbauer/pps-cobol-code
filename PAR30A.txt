000100 IDENTIFICATION DIVISION.                                         PAR30A
000200 PROGRAM-ID. PAR30A.                                              PAR30A
000300 DATE-WRITTEN. MAY 08, 1980.                                      PAR30A
000400 DATE-COMPILED.                                                   PAR30A
000500*REMARKS.                                                         PAR30A
000600**************************************************************/   DU039
000700*  PROGRAM:  PAR30A                                          */   DU039
000800*  CHANGE #DU039            PROJECT REQUEST:   DU-039        */   DU039
000900*  NAME: BOB MERRYMAN       MODIFICATION DATE: 02/24/92      */   DU039
001000*                                                            */   DU039
001100*  DESCRIPTION:                                              */   DU039
001200*    CONVERSION TO IFIS CHART-OF-ACCOUNTS CONVENTIONS.       */   DU039
001300*                                                            */   DU039
001400**************************************************************/   DU039
001500******************************************************************PAR30A
001600*                                                                *PAR30A
001700* FUNCTION:                                                      *PAR30A
001800*    THIS PROGRAM CONTROLS THE PRODUCTION OF THE EXTRACT CONTROL *PAR30A
001900*    REPORT, PAR0301R, AND THE EXTRACT EXCEPTION EXCEPTION       *PAR30A
002000*    REPORT, PAR0302R.  THE REPORT FILE FROM PAR0200A IS READ    *PAR30A
002100*    AND BASED ON THE TYPE OF REPORT RECORD, THE PROPER REPORT   *PAR30A
002200*    MODULE IS CALLED.                                           *PAR30A
002300*                                                                *PAR30A
002400* INPUT FILES:                                                   *PAR30A
002500*    PARI03EX - EXTRACT REPORTING FILE                           *PAR30A
002600*                                                                *PAR30A
002700* OUTPUT FILES:                                                  *PAR30A
002800*    NONE                                                        *PAR30A
002900*                                                                *PAR30A
003000* PARM:                                                          *PAR30A
003100*    XCCMM/DD/YY                                                 *PAR30A
003200*        X   TEST SWITCH                                         *PAR30A
003300*          Y = TEST RUN.  DEBUGGING DISPLAYS.                    *PAR30A
003400*          N = PRODUCTION RUN.                                   *PAR30A
003500*        CC  LOCATION SELECTION                                  *PAR30A
003600*          XX = PROCESS ALL LOCATIONS                            *PAR30A
003700*          01-09 = PRODUCE EXCEPTION REPORT FOR ONLY THE         *PAR30A
003800*            LOCATION IDENTIFIED.                                *PAR30A
003900*        MM/DD/YY  EXTRACT DATE                                  *PAR30A
004000*            THIS DATE WILL BE PRINTED ON BOTH REPORTS           *PAR30A
004100*                                                                *PAR30A
004200* CALLED MODULES:                                                *PAR30A
004300*    PAR0301A - PRODUCE EXTRACT CONTROL REPORT                   *PAR30A
004400*    PAR0302A - PRODUCE EXTRACT EXCEPTION REPORT                 *PAR30A
004500*                                                                *PAR30A
004600* CALLING MODULES:                                               *PAR30A
004700*    NONE                                                        *PAR30A
004800*                                                                *PAR30A
004900* RETURN CODES:                                                  *PAR30A
005000*    0000 = GOOD EOJ                                             *PAR30A
005100*    0016 = ABNORMAL CONDITION ENCOUNTERED.  SEE DISPLAYS.       *PAR30A
005200*                                                                *PAR30A
005300* MODIFICATIONS:                                                 *PAR30A
005400*    DATE     WHO        MODIFICATION DESCRIPTION                *PAR30A
005500*  XX/XX/XX   XXX    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *PAR30A
005600*                    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *PAR30A
005700*                                                                *PAR30A
005800******************************************************************PAR30A
005900 EJECT                                                            PAR30A
006000 ENVIRONMENT DIVISION.                                            PAR30A
006100 INPUT-OUTPUT SECTION.                                            PAR30A
006200 FILE-CONTROL.                                                    PAR30A
006300     SELECT REPORT-RECORDS-IN    ASSIGN TO UT-S-PARI03EX.         PAR30A
006400     SELECT SPEC-CARD                                             PAR30A
006500         COPY CPSLXCRD.                                           PAR30A
006600*    SELECT REPORT-RECORDS-IN    ASSIGN TO PFMS.                  PAR30A
006700 EJECT                                                            PAR30A
006800 DATA DIVISION.                                                   PAR30A
006900 FILE SECTION.                                                    PAR30A
007000 FD  REPORT-RECORDS-IN           RECORDING MODE IS F              PAR30A
007100*                                RECORD CONTAINS 2616 CHARACTERS  DU039
007200                                 RECORD CONTAINS 3336 CHARACTERS  DU039
007300                                 BLOCK CONTAINS 0 RECORDS         PAR30A
007400                                 LABEL RECORDS ARE STANDARD       PAR30A
007500                                 DATA RECORD IS                   PAR30A
007600                                         REPORT-RECORD.           PAR30A
007700*FD  REPORT-RECORDS-IN   UNCOMPRESSED                             PAR30A
007800*    LABEL RECORDS ARE STANDARD                                   PAR30A
007900*    VALUE OF FILE-ID 'F_EXTRPT'.                                 PAR30A
008000 01  REPORT-RECORD.                                               PAR30A
008100     05  REPORT-TYPE             PIC X.                           PAR30A
008200         88  CNTL-RPT            VALUE 'A'.                       PAR30A
008300         88  EXTRACT-RPT         VALUE 'B'.                       PAR30A
008400     05  REPORT-DATA.                                             PAR30A
008500         10  CAMPUS-RR           PIC 99.                          PAR30A
008600*        10  FILLER              PIC X(2613).                     DU039
008700         10  FILLER              PIC X(3333).                     DU039
008800 FD  SPEC-CARD                                                    PAR30A
008900*                                                                 PAR30A
009000         COPY CPFDXCRD.                                           PAR30A
009100*                                                                 PAR30A
009200 01  SPEC-CARD-REC               PIC X(80).                       PAR30A
009300 EJECT                                                            PAR30A
009400 WORKING-STORAGE SECTION.                                         PAR30A
009500 01  CONSTANTS-C.                                                 PAR30A
009600     05  MOD-NAME-C              PIC X(8) VALUE 'PAR0300A'.       PAR30A
009700 01  REPORT-ACCUMULATORS-W.                                       PAR30A
009800     05  NUM-CNTL-RCDS-W         PIC S9(9) COMP-3 VALUE +0.       PAR30A
009900     05  NUM-EXT-RCDS-W          PIC S9(9) COMP-3 VALUE +0.       PAR30A
010000     05  NUM-RPT-RCDS-W          PIC S9(9) COMP-3 VALUE +0.       PAR30A
010100 01  LOCATION-DESC-TABLE.        COPY 'PARYDESC'.                 PAR30A
010200 01  MODULE-COMMON-AREA.         COPY 'PARYMODC'.                 PAR30A
010300 EJECT                                                            PAR30A
010400*                                                                 PAR30A
010500*01  TEMP-REPORT-DATA         PIC X(2615).                        DU039
010600 01  TEMP-REPORT-DATA         PIC X(3335).                        DU039
010700 01  SWITCH-AREA-SW.                                              PAR30A
010800     05  PROCESS-COMPLETE-SW     PIC X VALUE '0'.                 PAR30A
010900         88  PROCESS-COMPLETE    VALUE '1'.                       PAR30A
011000     05  RPT-FILE-EOF-SW         PIC X VALUE '0'.                 PAR30A
011100         88  RPT-FILE-EOF        VALUE '1'.                       PAR30A
011200     05  EOF-SPEC                PIC 9 VALUE 0.                   PAR30A
011300*                                                                 PAR30A
011400 EJECT                                                            PAR30A
011500 01  INPUT-PARM-L.                                                PAR30A
011600*    05  NUM-CHAR-L              PIC S9(4) COMP-3.                PAR30A
011700     05  TEST-SWITCH-L           PIC X.                           PAR30A
011800         88  TEST-RUN            VALUE 'Y'.                       PAR30A
011900     05  LOCATION-L              PIC XX.                          PAR30A
012000     05  LOCATION-R-L            REDEFINES LOCATION-L             PAR30A
012100                                 PIC 99.                          PAR30A
012200     05  MONTH-L                 PIC 99.                          PAR30A
012300     05  FILLER                  PIC X.                           PAR30A
012400     05  DAY-L                   PIC 99.                          PAR30A
012500     05  FILLER                  PIC X.                           PAR30A
012600     05  YEAR-L                  PIC 99.                          PAR30A
012700 EJECT                                                            PAR30A
012800******************************************************************PAR30A
012900*    THE PROCEDURE DIVISION IS ORGANIZED INTO FOUR MAIN SECTIONS.*PAR30A
013000*    0000-MAINLINE                                               *PAR30A
013100*        ENTRY POINT OF PROGRAM                                  *PAR30A
013200*        PERFORMS EACH OTHER MAJOR SECTION                       *PAR30A
013300*    1000-INITIALIZATION                                         *PAR30A
013400*        OPENS INPUT FILE                                        *PAR30A
013500*        PRIMES INPUT FILE                                       *PAR30A
013600*        EDITS PARM                                              *PAR30A
013700*        INITIALIZES WORK AREA                                   *PAR30A
013800*        INITIALIZES CALLED MODULES                              *PAR30A
013900*    3000-PROCESS                                                *PAR30A
014000*        DETERMINES TYPE OF INPUT RECORD                         *PAR30A
014100*        CALLS PROPER MODULE BASED ON TYPE OF INPUT RECORD       *PAR30A
014200*        READS NEXT INPUT RECORD                                 *PAR30A
014300*    9000-TERMINATION.                                           *PAR30A
014400*        CLOSES INPUT FILE                                       *PAR30A
014500*        FINALIZES CALLED MODULES                                *PAR30A
014600*        DISPLAYS PROCESSING TOTALS                              *PAR30A
014700******************************************************************PAR30A
014800*PROCEDURE DIVISION USING INPUT-PARM-L.                           PAR30A
014900 PROCEDURE DIVISION.                                              PAR30A
015000 0000-MAINLINE SECTION.                                           PAR30A
015100 0000-MAIN-PARA.                                                  PAR30A
015200*    DISPLAY 'ENTER PARM'.                                        PAR30A
015300*                                                                 PAR30A
015400     OPEN INPUT SPEC-CARD                                         PAR30A
015500*                                                                 PAR30A
015600     READ SPEC-CARD INTO INPUT-PARM-L                             PAR30A
015700         AT END                                                   PAR30A
015800         MOVE 1 TO EOF-SPEC.                                      PAR30A
015900*                                                                 PAR30A
016000     CLOSE SPEC-CARD.                                             PAR30A
016100*    ACCEPT INPUT-PARM-L.                                         PAR30A
016200     DISPLAY 'INPUT PARM = ' INPUT-PARM-L.                        PAR30A
016300     DISPLAY MOD-NAME-C ' BEGIN EXECUTION'.                       PAR30A
016400     PERFORM 1000-INITIALIZATION THRU                             PAR30A
016500         1001-EXIT-INITIALIZATION.                                PAR30A
016600     PERFORM 3000-PROCESS  THRU                                   PAR30A
016700         3001-EXIT-PROCESS                                        PAR30A
016800         UNTIL PROCESS-COMPLETE.                                  PAR30A
016900     PERFORM 9000-TERMINATION  THRU                               PAR30A
017000         9001-EXIT-TERMINATION.                                   PAR30A
017100     GO TO 0020-SET-GOOD-RETURN.                                  PAR30A
017200 0010-SET-BAD-RETURN.                                             PAR30A
017300     MOVE +16 TO RETURN-CODE.                                     PAR30A
017400     GO TO 0030-RETURN.                                           PAR30A
017500 0020-SET-GOOD-RETURN.                                            PAR30A
017600     MOVE +0 TO RETURN-CODE.                                      PAR30A
017700 0030-RETURN.                                                     PAR30A
017800*    GOBACK.                                                      PAR30A
017900     STOP RUN.                                                    PAR30A
018000 EJECT                                                            PAR30A
018100 1000-INITIALIZATION SECTION.                                     PAR30A
018200 1000-INIT-PARA.                                                  PAR30A
018300     OPEN INPUT REPORT-RECORDS-IN.                                PAR30A
018400     PERFORM 4000-READ-RPT-FILE.                                  PAR30A
018500     IF RPT-FILE-EOF                                              PAR30A
018600         DISPLAY MOD-NAME-C ' REPORT RECORD INPUT FILE HAS'       PAR30A
018700                 ' NO RECORDS.  PROCESSING TERMINATED'            PAR30A
018800         GO TO 0010-SET-BAD-RETURN.                               PAR30A
018900     PERFORM 1100-EDIT-PARM.                                      PAR30A
019000     IF LOCATION-L = 'XX'                                         PAR30A
019100         PERFORM 1200-INIT-LOCATIONS                              PAR30A
019200             VARYING LOC-IX FROM +1 BY +1                         PAR30A
019300             UNTIL LOC-IX > +9                                    PAR30A
019400     ELSE                                                         PAR30A
019500         SET LOC-IX TO LOCATION-R-L                               PAR30A
019600         PERFORM 1200-INIT-LOCATIONS.                             PAR30A
019700     MOVE MONTH-L TO RUN-DATE-MM-LS.                              PAR30A
019800     MOVE DAY-L TO RUN-DATE-DD-LS.                                PAR30A
019900     MOVE YEAR-L TO RUN-DATE-YY-LS.                               PAR30A
020000     MOVE TEST-SWITCH-L TO TEST-SWITCH-LS.                        PAR30A
020100     MOVE 00 TO LOCATION-CODE-LS.                                 PAR30A
020200     MOVE 'I' TO TYPE-OF-CALL-LS.                                 PAR30A
020300     PERFORM 5000-CALL-PAR0301A THRU                              PAR30A
020400         5001-EXIT-CALL-PAR0301A.                                 PAR30A
020500     PERFORM 6000-CALL-PAR0302A  THRU                             PAR30A
020600         6001-EXIT-CALL-PAR0302A.                                 PAR30A
020700     MOVE 'P' TO TYPE-OF-CALL-LS.                                 PAR30A
020800 1001-EXIT-INITIALIZATION.                                        PAR30A
020900     EXIT.                                                        PAR30A
021000 1100-EDIT-PARM SECTION.                                          PAR30A
021100 1100-EDIT-PARM-PARA.                                             PAR30A
021200*    IF NUM-CHAR-L = +0                                           PAR30A
021300*        DISPLAY MOD-NAME-C ' INPUT PARM IS NOT PRESENT'          PAR30A
021400*                'RUN TERMINATED'                                 PAR30A
021500*        GO TO 0010-SET-BAD-RETURN.                               PAR30A
021600*    IF MONTH-L > 00 AND                                          PAR30A
021700*      MONTH-L < 13 AND                                           PAR30A
021800     IF MONTH-L = 03 OR 06 OR 09 OR 12                            PAR30A
021900        AND   DAY-L > 00 AND                                      PAR30A
022000              DAY-L < 32                                          PAR30A
022100         NEXT SENTENCE                                            PAR30A
022200     ELSE                                                         PAR30A
022300         DISPLAY MOD-NAME-C ' PARM DATE IS INVALID '              PAR30A
022400                 'MONTH = ' MONTH-L ' DAY = ' DAY-L               PAR30A
022500                 ' YEAR = ' YEAR-L                                PAR30A
022600         GO TO 0010-SET-BAD-RETURN.                               PAR30A
022700     DISPLAY MOD-NAME-C ' PARM VALID ' INPUT-PARM-L.              PAR30A
022800 1101-EXIT-EDIT-PARM.                                             PAR30A
022900     EXIT.                                                        PAR30A
023000 1200-INIT-LOCATIONS SECTION.                                     PAR30A
023100 1200-INIT-PARA.                                                  PAR30A
023200     MOVE '1' TO LOC-SEL (LOC-IX).                                PAR30A
023300 1201-EXIT-INIT-LOCATIONS.                                        PAR30A
023400     EXIT.                                                        PAR30A
023500 EJECT                                                            PAR30A
023600 3000-PROCESS SECTION.                                            PAR30A
023700 3000-PROC-PARA.                                                  PAR30A
023800     IF RPT-FILE-EOF                                              PAR30A
023900         MOVE '1' TO PROCESS-COMPLETE-SW                          PAR30A
024000         GO TO 3001-EXIT-PROCESS.                                 PAR30A
024100     IF CNTL-RPT                                                  PAR30A
024200         ADD +1 TO NUM-CNTL-RCDS-W                                PAR30A
024300         PERFORM 5000-CALL-PAR0301A   THRU                        PAR30A
024400         5001-EXIT-CALL-PAR0301A                                  PAR30A
024500     ELSE                                                         PAR30A
024600         SET LOC-IX TO CAMPUS-RR                                  PAR30A
024700         IF LOC-SEL (LOC-IX) = '1'                                PAR30A
024800             IF EXTRACT-RPT                                       PAR30A
024900                 ADD +1 TO NUM-EXT-RCDS-W                         PAR30A
025000                 PERFORM 6000-CALL-PAR0302A   THRU                PAR30A
025100                  6001-EXIT-CALL-PAR0302A                         PAR30A
025200             ELSE                                                 PAR30A
025300                 DISPLAY MOD-NAME-C ' INVALID RECORD TYPE OF '    PAR30A
025400                         REPORT-TYPE                              PAR30A
025500             GO TO 0010-SET-BAD-RETURN.                           PAR30A
025600     PERFORM 4000-READ-RPT-FILE.                                  PAR30A
025700 3001-EXIT-PROCESS.                                               PAR30A
025800     EXIT.                                                        PAR30A
025900 4000-READ-RPT-FILE SECTION.                                      PAR30A
026000 4000-READ-PARA.                                                  PAR30A
026100     READ REPORT-RECORDS-IN                                       PAR30A
026200         AT END                                                   PAR30A
026300             MOVE '1' TO RPT-FILE-EOF-SW                          PAR30A
026400             GO TO 4001-EXIT-READ-RPT-FILE.                       PAR30A
026500     ADD +1 TO NUM-RPT-RCDS-W.                                    PAR30A
026600 4001-EXIT-READ-RPT-FILE.                                         PAR30A
026700     EXIT.                                                        PAR30A
026800 5000-CALL-PAR0301A SECTION.                                      PAR30A
026900 5000-CALL-PARA.                                                  PAR30A
027000     MOVE REPORT-DATA TO TEMP-REPORT-DATA.                        PAR30A
027100     CALL 'PAR31A' USING MODULE-COMMON-AREA                       PAR30A
027200                      TEMP-REPORT-DATA.                           PAR30A
027300     IF RETURN-CODE = +0                                          PAR30A
027400         NEXT SENTENCE                                            PAR30A
027500     ELSE                                                         PAR30A
027600         DISPLAY MOD-NAME-C ' BAD RETURN FROM PAR0301A '          PAR30A
027700                 'REQUESTED FUNCTION ' TYPE-OF-CALL-LS            PAR30A
027800         PERFORM 0010-SET-BAD-RETURN.                             PAR30A
027900 5001-EXIT-CALL-PAR0301A.                                         PAR30A
028000     EXIT.                                                        PAR30A
028100 6000-CALL-PAR0302A SECTION.                                      PAR30A
028200 6000-CALL-PARA.                                                  PAR30A
028300     MOVE REPORT-DATA TO TEMP-REPORT-DATA.                        PAR30A
028400     CALL 'PAR32A' USING MODULE-COMMON-AREA                       PAR30A
028500                      TEMP-REPORT-DATA.                           PAR30A
028600     IF RETURN-CODE = +0                                          PAR30A
028700         NEXT SENTENCE                                            PAR30A
028800     ELSE                                                         PAR30A
028900         DISPLAY MOD-NAME-C ' BAD RETURN FROM PAR0302A '          PAR30A
029000                 'REQUESTED FUNCTION ' TYPE-OF-CALL-LS            PAR30A
029100         PERFORM   0010-SET-BAD-RETURN.                           PAR30A
029200 6001-EXIT-CALL-PAR0302A.                                         PAR30A
029300     EXIT.                                                        PAR30A
029400 EJECT                                                            PAR30A
029500 9000-TERMINATION SECTION.                                        PAR30A
029600 9000-TERM-PARA.                                                  PAR30A
029700     MOVE 'T' TO TYPE-OF-CALL-LS.                                 PAR30A
029800     PERFORM 5000-CALL-PAR0301A THRU                              PAR30A
029900         5001-EXIT-CALL-PAR0301A.                                 PAR30A
030000     PERFORM 6000-CALL-PAR0302A.                                  PAR30A
030100     CLOSE REPORT-RECORDS-IN.                                     PAR30A
030200     DISPLAY MOD-NAME-C ' PROCESSING COMPLETED'.                  PAR30A
030300     DISPLAY '         NUMBER CONTROL RECORDS   = '               PAR30A
030400                                     NUM-CNTL-RCDS-W.             PAR30A
030500     DISPLAY '         NUMBER EXCEPTION RECORDS = '               PAR30A
030600                                     NUM-EXT-RCDS-W.              PAR30A
030700         DISPLAY '         TOTAL RECORDS READ       = '           PAR30A
030800                                     NUM-RPT-RCDS-W.              PAR30A
030900 9001-EXIT-TERMINATION.                                           PAR30A
031000     EXIT.                                                        PAR30A
