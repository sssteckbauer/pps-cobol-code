       IDENTIFICATION DIVISION.
       PROGRAM-ID.    PPLEVRFY.
       AUTHOR.        UCSD ACT.
       DATE-WRITTEN.  FEB 2010.
       DATE-COMPILED.
      *****************************************************************
      *
      * THIS PROGRAM READS A FILE (CONCATENATED FILES) OF CONTROL TABLE
      * UPDATE TRANSACTION RECORDS AND COUNTS THE TOTAL NUMBER OF
      * UPDATES FOR EACH TABLE AND THE TOTAL NUMBER OF EACH UPDATE TYPE
      * (ADD, CHG OR DEL) FOR EACH TABLE. THIS INFO IS NEEDED SO IT
      * CAN BE SENT TO UCOP ALONG WITH THE CTL TBL UPDATE TRANSACTIONS.
      *
      * INPUT:  CONTROL TABLE UPDATE TRANSACTION RECORDS.
      * OUTPUT: DESCRIPTION OF THE NUMBER & TYPE OF CTL TBL UPDATES.
      *
      * UPDATES TO THE FOLLOWING CTL TBLS ARE ACCOMMODATED BY THIS PGM:
      *
      * DESCRIPTION        TBL UPAY FORM NOTES
      * ------------------ --- --------- ---------------------------
      * DOS CODE (DOS)     10  UPAY650
      * HOME DEPT (HME)    11  UPAY552
      * SUREPAY BANK (SPB) 22  UPAY703
      * FUND GROUP (FND)   41  UPAY866   EVERIFY, EXCLFUNDS, ETC.
      * MERIT CTL (MCP)    44  UPAY910   REQUIRED W/SOME HME UPDATES
      * WORK STUDY PGM(WSP)24  UPAY721
      * WORKERS' COMP. RATE 23 UPAY707
      *
      * MAINTENANCE NOTES - WHEN MODIFYING THIS PROGRAM TO ACCOMMODATE
      * AN ADDITIONAL TABLE TYPE:
      *
      * 1) ADD NEW TBL NBR TO WS-TBLNOS W/NEXT HIGHER SUBSCRIPT VALUE
      * 2) UPDATE THE NUMERIC VALUE OF NBR-TBL-ENTRIES
      * 3) MODIFY WS-CTL-TABLES TO ADD NEW TBL & TBLNO
      * 4) ADD PROCESS TO THE EVALUATE OF IN-TBL-NO IN 2000-PROCESS
      * 5) ADD A REDEFINE OF IN-PPP004-RECORD IF NEEDED
      *
      *****************************************************************
      *                      MAINTENANCE HISTORY
      *****************************************************************
      * DATE     AUTH DESCRIPTION
      * -------- ---- -------------------------------------------------
      * 02/23/10 JRW  CREATED INITIAL VER OF PGM TO PROCESS 5 TABLES:
      *               DOS, HME, SPB, FND, MCP.
      *
      *               FOR TBL UPDATES THAT REQUIRE MULTIPLE TRANSACTION
      *               RECS (SPB, DOS, HME), THE PROCESS CURRENTLY
      *               ASSUMES THERE WILL ALWAYS BE 1 RECORD WITH TRN NO
      *               '1' PER TABLE UPDATE. IF THIS TURNS OUT TO NOT
      *               ALWAYS BE THE CASE, THE PROCESS WILL NEED TO BE
      *               UPDATED, PERHAPS TO TAKE A SORTED INPUT FILE AND
      *               INCREMENT ON KEY CHANGE.
      *
      *               NOTE THAT TBL KEY IS OF VARIABLE LENGTH DEPENDING
      *               ON TBL (E.G., 8 BYTES FOR SPB, 9 FOR DOS).
      * 02/16/11 NDP  ADDED WORK STUDY PROGRAM PROCESS (TABLE 24)
      * 04/07/11 NDP  ADDED WORKERS' COMPENSATION RATE (TABLE 23)
      ******************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT IN-PPP004-FILE ASSIGN TO INPPP004.
           SELECT OT-PPP004-FILE ASSIGN TO OTPPP004.

       DATA DIVISION.
       FILE SECTION.
       FD  IN-PPP004-FILE
           RECORDING MODE IS F
           DATA RECORD IS IN-PPP004-REC.
       01  IN-PPP004-REC.
           03  IN-PPP004-RECORD.
               05  IN-ADD-CHG-DEL           PIC X(01).
               05  IN-TBL-NO                PIC 9(02).
               05  IN-VARIABLE-DATA             PIC X(77).
               05  IN-DOS-UPAY650 REDEFINES IN-VARIABLE-DATA.
                   07  IN-DOS-SEQ-NBR              PIC 9(03).
                   07  IN-DOS-CODE                 PIC X(03).
                   07  IN-DOS-TRN-NO               PIC 9(01).
                   07  IN-DOS-FILLER               PIC X(70).
               05  IN-HME-UPAY552 REDEFINES IN-VARIABLE-DATA.
                   07  IN-HME-DEPT-NO              PIC X(06).
                   07  IN-HME-BLANKS               PIC X(03).
                   07  IN-HME-TRN-NO               PIC 9(01).
                   07  IN-HME-FILLER               PIC X(67).
               05  IN-SPB-UPAY703 REDEFINES IN-VARIABLE-DATA.
                   07  IN-SPB-BANK-TBL-KEY         PIC X(05).
                   07  IN-SPB-TRN-NO               PIC 9(01).
                   07  IN-SPB-FILLER               PIC X(71).
               05  IN-FND-UPAY866 REDEFINES IN-VARIABLE-DATA.
                   07  IN-FND-GRP-DEF-CODE         PIC X(08).
                   07  IN-FND-FILLER               PIC X(69).
               05  IN-MCP-UPAY910 REDEFINES IN-VARIABLE-DATA.
                   07  IN-MCP-FILLER               PIC X(77).

       FD  OT-PPP004-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           DATA RECORD IS OT-PPP004-REC.
       01  OT-PPP004-REC                   PIC X(80).

       WORKING-STORAGE SECTION.
       01  WORK-AREAS.
           03  WS-PPP004-RECS-READ   PIC 9(06) VALUE ZEROES.
           03  WS-ADD                PIC 9 VALUE 1.
           03  WS-CHG                PIC 9 VALUE 2.
           03  WS-DEL                PIC 9 VALUE 3.
           03  UPDTYPE               PIC 9 VALUE ZEROES.
           03  WS-TBLNOS.
               05  TBL10                 PIC 9 VALUE 1.
               05  TBL11                 PIC 9 VALUE 2.
               05  TBL22                 PIC 9 VALUE 3.
               05  TBL41                 PIC 9 VALUE 4.
               05  TBL44                 PIC 9 VALUE 5.
               05  TBL24                 PIC 9 VALUE 6.
               05  TBL23                 PIC 9 VALUE 7.
           03  NBR-TBL-ENTRIES       PIC 9 VALUE 7.
       01  WS-TABLE-LITERALS.
           03  WS-TYPS PIC X(09) VALUE 'ADDCHGDEL'.
           03  WS-TYP REDEFINES WS-TYPS    OCCURS 3 TIMES PIC XXX.
           03  WS-CTL-TABLES PIC X(35)
               VALUE 'DOS10HME11SPB22FND41MCP44WSP24WCR23'.
           03  WS-TABLES REDEFINES WS-CTL-TABLES OCCURS 7 TIMES.
               05  TBL    PIC X(3).
               05  TBLNO  PIC X(2).
       01  WS-TOTAL-RECS                 PIC 9(06) VALUE ZEROES.
       01  WS-SWITCHES.
           03  WS-END-OF-FILE                   PIC X(01) VALUE ' '.
               88  END-OF-FILE                            VALUE 'Y'.
       01  WS-RECORD-TYPES-ARRAY.
           03  TABLE-NO OCCURS 7 TIMES VALUE ZEROES.
               05  UPD-TYPE OCCURS 3 TIMES.
                   07  REC-CNT     PIC 9(5).
       01  WS-SUBSCRIPTS.
           03  S1                    PIC 99.
           03  S2                    PIC 99.

       01 WS-HEADING-1.
          05                         PIC X(01) VALUE SPACES.
          05                         PIC X(20) VALUE
              'TOTAL RECORD COUNT: '.
          05  WS-H1-TOTAL            PIC ZZZZZ9 VALUE ZEROES.

       01 WS-HEADING-2.
          05                         PIC X(01) VALUE SPACES.
          05                         PIC X(44) VALUE
              'CONTROL TABLES TO BE UPDATED ARE: '.
          05                         PIC X(35) VALUE SPACES.

       01 WS-PPP004-RECORD.
          05 FILLER                  PIC X(02) VALUE SPACES.
          05 OUT-PPP004-TBL-TYPE     PIC X(03) VALUE SPACES.
          05                         PIC X(01) VALUE ' '.
          05                         PIC X(01) VALUE '('.
          05 OUT-PPP004-TBL-NBR      PIC X(02) VALUE SPACES.
          05                         PIC X(04) VALUE ') - '.
          05 OUT-PPP004-ACD          PIC X(03) VALUE SPACES.
          05                         PIC X(01) VALUE ' '.
          05 OUT-PPP004-NBR-RECS     PIC ZZZ9  VALUE ZEROES.
          05 FILLER                  PIC X(01) VALUE SPACES.

       PROCEDURE DIVISION.

       0000-MAINLINE SECTION.
      ****************************************************************
      * MAIN PROCESSING
      ****************************************************************
           PERFORM 8000-INITIALIZE
           PERFORM UNTIL END-OF-FILE
               READ IN-PPP004-FILE
                   AT END
                       MOVE 'Y' TO WS-END-OF-FILE
                   NOT AT END
                       ADD 1 TO WS-PPP004-RECS-READ
                       PERFORM 2000-PROCESS
               END-READ
           END-PERFORM
           PERFORM 9000-END

           GOBACK.

       1000-READ-PPP004-FILE.
      ****************************************************************
      * READ INPUT
      ****************************************************************
           READ IN-PPP004-FILE
              AT END
                 MOVE 'Y' TO WS-END-OF-FILE
              NOT AT END
                 ADD 1 TO WS-PPP004-RECS-READ
           END-READ
           .

       2000-PROCESS.
      ****************************************************************
      * PROCESS RECORDS
      ****************************************************************
           EVALUATE IN-ADD-CHG-DEL
              WHEN 'A' MOVE WS-ADD TO UPDTYPE
              WHEN 'C' MOVE WS-CHG TO UPDTYPE
              WHEN 'D' MOVE WS-DEL TO UPDTYPE
              WHEN OTHER DISPLAY 'OTHER'
           END-EVALUATE
           EVALUATE IN-TBL-NO
              WHEN 10
                 IF IN-DOS-TRN-NO = 1
                     ADD 1 TO REC-CNT (TBL10, UPDTYPE)
                 END-IF
              WHEN 11
                 IF IN-HME-TRN-NO = 1
                     ADD 1 TO REC-CNT (TBL11, UPDTYPE)
                 END-IF
              WHEN 22
                 IF IN-SPB-TRN-NO = 1
                     ADD 1 TO REC-CNT (TBL22, UPDTYPE)
                 END-IF
              WHEN 41
                 ADD 1 TO REC-CNT (TBL41, UPDTYPE)
              WHEN 44
                 ADD 1 TO REC-CNT (TBL44, UPDTYPE)
              WHEN 24
                 ADD 1 TO REC-CNT (TBL24, UPDTYPE)
              WHEN 23
                 ADD 1 TO REC-CNT (TBL23, UPDTYPE)
              WHEN OTHER
                 DISPLAY 'ERROR: A REC EXISTS FOR AN UNDEFINED TABLE'
           END-EVALUATE
           .

       8000-INITIALIZE.
      ****************************************************************
      * INITIALIZE
      ****************************************************************
           MOVE 'N'  TO WS-END-OF-FILE
           OPEN INPUT  IN-PPP004-FILE
                OUTPUT OT-PPP004-FILE
           .

       9000-END.
      ****************************************************************
      * END PROGRAM & WRITE OUTPUT
      ****************************************************************
           DISPLAY ' '
           DISPLAY 'TOTAL PPP004 RECS READ: ' WS-PPP004-RECS-READ
           DISPLAY ' '
           MOVE WS-PPP004-RECS-READ TO WS-H1-TOTAL
           WRITE OT-PPP004-REC FROM WS-HEADING-1
           WRITE OT-PPP004-REC FROM WS-HEADING-2
           PERFORM VARYING S1 FROM 1 BY 1 UNTIL S1 > NBR-TBL-ENTRIES
               MOVE TBL (S1)    TO OUT-PPP004-TBL-TYPE
               MOVE TBLNO (S1)  TO OUT-PPP004-TBL-NBR
               PERFORM VARYING S2 FROM 1 BY 1 UNTIL S2 > 3
                   IF REC-CNT (S1, S2) > 0
                       MOVE WS-TYP (S2) TO OUT-PPP004-ACD
                       MOVE REC-CNT (S1, S2) TO OUT-PPP004-NBR-RECS
                       WRITE OT-PPP004-REC FROM WS-PPP004-RECORD
                   END-IF
               END-PERFORM
           END-PERFORM

           CLOSE IN-PPP004-FILE
           CLOSE OT-PPP004-FILE
           .

