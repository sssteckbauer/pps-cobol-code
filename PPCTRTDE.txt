000100**************************************************************/   30871424
000200*  PROGRAM: PPCTRTDE                                         */   30871424
000300*  RELEASE: ___1424______ SERVICE REQUEST(S): _____3087____  */   30871424
000400*  NAME:___SRS___________ CREATION DATE:      ___07/30/02__  */   30871424
000500*  DESCRIPTION:                                              */   30871424
000600*  ROUTINE DEFINITIION TABLE EDIT MODULE                     */   30871424
000700**************************************************************/   30871424
000800 IDENTIFICATION DIVISION.                                         PPCTRTDE
000900 PROGRAM-ID. PPCTRTDE.                                            PPCTRTDE
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTRTDE
001100 DATE-COMPILED.                                                   PPCTRTDE
001200 ENVIRONMENT DIVISION.                                            PPCTRTDE
001300 DATA DIVISION.                                                   PPCTRTDE
001400 WORKING-STORAGE SECTION.                                         PPCTRTDE
001500 01  MESSAGES-AREA.                                               PPCTRTDE
001600     05  M01027                   PIC X(05)     VALUE '01027'.    PPCTRTDE
001700     05  M01028                   PIC X(05)     VALUE '01028'.    PPCTRTDE
001800     05  M01029                   PIC X(05)     VALUE '01029'.    PPCTRTDE
001900     05  M01420                   PIC X(05)     VALUE '01420'.    PPCTRTDE
002000     05  M01421                   PIC X(05)     VALUE '01421'.    PPCTRTDE
002100     05  M01422                   PIC X(05)     VALUE '01422'.    PPCTRTDE
002200     05  M01423                   PIC X(05)     VALUE '01423'.    PPCTRTDE
002300     05  M01424                   PIC X(05)     VALUE '01424'.    PPCTRTDE
002400     05  M01425                   PIC X(05)     VALUE '01425'.    PPCTRTDE
002500     05  M01426                   PIC X(05)     VALUE '01426'.    PPCTRTDE
002600     05  M01427                   PIC X(05)     VALUE '01427'.    PPCTRTDE
002700     05  MCT005                   PIC X(05)     VALUE 'CT005'.    PPCTRTDE
002800                                                                  PPCTRTDE
002900 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTRTDE
003000 01  XDC3-DATE-CONVERSION-WORK.                 COPY CPWSXDC3.    PPCTRTDE
003100 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTRTDE
003200                                                                  PPCTRTDE
003300     EXEC SQL                                                     PPCTRTDE
003400        INCLUDE SQLCA                                             PPCTRTDE
003500     END-EXEC.                                                    PPCTRTDE
003600                                                                  PPCTRTDE
003700 LINKAGE SECTION.                                                 PPCTRTDE
003800                                                                  PPCTRTDE
003900 01  CONTROL-TABLE-EDIT-INTERFACE.              COPY CPLNKCTE.    PPCTRTDE
004000 01  ROUTINE-DEF-RTD-TABLE-INPUT.               COPY CPCTRTDI.    PPCTRTDE
004100 01  ROUTINE-DEF-RTD-TABLE-DATA.                                  PPCTRTDE
004200     EXEC SQL                                                     PPCTRTDE
004300        INCLUDE PPPVZRTD                                          PPCTRTDE
004400     END-EXEC.                                                    PPCTRTDE
004500                                                                  PPCTRTDE
004600 PROCEDURE DIVISION USING CONTROL-TABLE-EDIT-INTERFACE            PPCTRTDE
004700                          ROUTINE-DEF-RTD-TABLE-INPUT             PPCTRTDE
004800                          ROUTINE-DEF-RTD-TABLE-DATA.             PPCTRTDE
004900                                                                  PPCTRTDE
005000 0000-MAIN SECTION.                                               PPCTRTDE
005100                                                                  PPCTRTDE
005200     EXEC SQL                                                     PPCTRTDE
005300        INCLUDE CPPDXE99                                          PPCTRTDE
005400     END-EXEC.                                                    PPCTRTDE
005500     MOVE 'PPCTRTDE' TO XWHC-DISPLAY-MODULE.                      PPCTRTDE
005600     COPY CPPDXWHC.                                               PPCTRTDE
005700     INITIALIZE ROUTINE-DEF-RTD-TABLE-DATA.                       PPCTRTDE
005800     MOVE ZERO TO KCTE-ERR.                                       PPCTRTDE
005900     MOVE SPACES TO KCTE-ERRORS.                                  PPCTRTDE
006000     SET KCTE-STATUS-COMPLETED TO TRUE.                           PPCTRTDE
006100     PERFORM 1000-EDIT-KEY.                                       PPCTRTDE
006200     IF KCTE-STATUS-NOT-COMPLETED                                 PPCTRTDE
006300        GO TO 0100-END                                            PPCTRTDE
006400     END-IF.                                                      PPCTRTDE
006500     PERFORM 9000-SELECT-RTD.                                     PPCTRTDE
006600     IF KCTE-ACTION-ADD                                           PPCTRTDE
006700        PERFORM 2000-ADD-RTD                                      PPCTRTDE
006800     ELSE                                                         PPCTRTDE
006900       IF KCTE-ACTION-CHANGE                                      PPCTRTDE
007000          PERFORM 2100-CHANGE-RTD                                 PPCTRTDE
007100       ELSE                                                       PPCTRTDE
007200         IF KCTE-ACTION-DELETE                                    PPCTRTDE
007300            PERFORM 2200-DELETE-RTD                               PPCTRTDE
007400         END-IF                                                   PPCTRTDE
007500       END-IF                                                     PPCTRTDE
007600     END-IF.                                                      PPCTRTDE
007700                                                                  PPCTRTDE
007800 0100-END.                                                        PPCTRTDE
007900                                                                  PPCTRTDE
008000     GOBACK.                                                      PPCTRTDE
008100                                                                  PPCTRTDE
008200 1000-EDIT-KEY SECTION.                                           PPCTRTDE
008300                                                                  PPCTRTDE
008400     IF RTDI-VALID-RTN-TYPE                                       PPCTRTDE
008500        MOVE RTDI-RTN-TYPE     TO RTD-RTN-TYPE                    PPCTRTDE
008600     ELSE                                                         PPCTRTDE
008700        ADD 1                  TO KCTE-ERR                        PPCTRTDE
008800        MOVE M01420            TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
008900        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTRTDE
009000     END-IF.                                                      PPCTRTDE
009100     INSPECT RTDI-RTN-NUM REPLACING ALL SPACES BY ZEROS           PPCTRTDE
009200     IF RTDI-RTN-NUM NOT NUMERIC                                  PPCTRTDE
009300     OR RTDI-RTN-NUM = ZERO                                       PPCTRTDE
009400        ADD 1                  TO KCTE-ERR                        PPCTRTDE
009500        MOVE M01421            TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
009600        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTRTDE
009700     ELSE                                                         PPCTRTDE
009800        MOVE RTDI-RTN-NUM-9 TO RTD-RTN-NUM                        PPCTRTDE
009900     END-IF.                                                      PPCTRTDE
010000                                                                  PPCTRTDE
010100 1099-EDIT-KEY-EXIT.                                              PPCTRTDE
010200     EXIT.                                                        PPCTRTDE
010300                                                                  PPCTRTDE
010400 2000-ADD-RTD SECTION.                                            PPCTRTDE
010500                                                                  PPCTRTDE
010600     IF SQLCODE = ZERO                                            PPCTRTDE
010700        ADD 1                  TO KCTE-ERR                        PPCTRTDE
010800        MOVE M01027            TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
010900        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTRTDE
011000        GO TO 2099-ADD-EXIT                                       PPCTRTDE
011100     END-IF.                                                      PPCTRTDE
011200     IF RTDI-CALL-MODULE = SPACES                                 PPCTRTDE
011300        ADD 1                  TO KCTE-ERR                        PPCTRTDE
011400        MOVE M01422            TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
011500     ELSE                                                         PPCTRTDE
011600        MOVE RTDI-CALL-MODULE  TO RTD-CALL-MODULE                 PPCTRTDE
011700     END-IF.                                                      PPCTRTDE
011800     IF RTDI-MODULE-DESC = SPACES                                 PPCTRTDE
011900        ADD 1                  TO KCTE-ERR                        PPCTRTDE
012000        MOVE M01423            TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
012100     ELSE                                                         PPCTRTDE
012200        MOVE RTDI-MODULE-DESC  TO RTD-MODULE-DESC                 PPCTRTDE
012300     END-IF.                                                      PPCTRTDE
012400     IF RTDI-STATUS = 'A' OR 'I'                                  PPCTRTDE
012500        MOVE RTDI-STATUS       TO RTD-STATUS                      PPCTRTDE
012600     ELSE                                                         PPCTRTDE
012700        ADD 1                  TO KCTE-ERR                        PPCTRTDE
012800        MOVE M01424            TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
012900     END-IF.                                                      PPCTRTDE
013000     PERFORM 3000-EDIT-DATES.                                     PPCTRTDE
013100     MOVE 'A'                  TO RTD-LAST-ACTION.                PPCTRTDE
013200     MOVE KCTE-LAST-ACTION-DT  TO RTD-LAST-ACTION-DT.             PPCTRTDE
013300                                                                  PPCTRTDE
013400 2099-ADD-EXIT.                                                   PPCTRTDE
013500     EXIT.                                                        PPCTRTDE
013600                                                                  PPCTRTDE
013700 2100-CHANGE-RTD SECTION.                                         PPCTRTDE
013800                                                                  PPCTRTDE
013900     IF SQLCODE NOT = ZERO                                        PPCTRTDE
014000        ADD 1                  TO KCTE-ERR                        PPCTRTDE
014100        MOVE M01028            TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
014200        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTRTDE
014300        GO TO 2199-CHANGE-EXIT                                    PPCTRTDE
014400     END-IF.                                                      PPCTRTDE
014500     IF RTDI-CALL-MODULE(1:1) = '*'                               PPCTRTDE
014600        MOVE SPACES            TO RTD-CALL-MODULE                 PPCTRTDE
014700     ELSE                                                         PPCTRTDE
014800       IF RTDI-CALL-MODULE NOT = SPACES                           PPCTRTDE
014900          MOVE RTDI-CALL-MODULE TO RTD-CALL-MODULE                PPCTRTDE
015000       END-IF                                                     PPCTRTDE
015100     END-IF.                                                      PPCTRTDE
015200     IF RTDI-MODULE-DESC(1:1) = '*'                               PPCTRTDE
015300        MOVE SPACES            TO RTD-MODULE-DESC                 PPCTRTDE
015400     ELSE                                                         PPCTRTDE
015500       IF RTDI-MODULE-DESC NOT = SPACES                           PPCTRTDE
015600          MOVE RTDI-MODULE-DESC TO RTD-MODULE-DESC                PPCTRTDE
015700       END-IF                                                     PPCTRTDE
015800     END-IF.                                                      PPCTRTDE
015900     IF RTDI-STATUS = '*'                                         PPCTRTDE
016000        MOVE 'A'               TO RTD-STATUS                      PPCTRTDE
016100     ELSE                                                         PPCTRTDE
016200       IF RTDI-STATUS NOT = SPACES                                PPCTRTDE
016300         IF RTDI-STATUS = 'A' OR 'I'                              PPCTRTDE
016400            MOVE RTDI-STATUS   TO RTD-STATUS                      PPCTRTDE
016500         ELSE                                                     PPCTRTDE
016600            ADD 1              TO KCTE-ERR                        PPCTRTDE
016700            MOVE M01424        TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
016800         END-IF                                                   PPCTRTDE
016900       END-IF                                                     PPCTRTDE
017000     END-IF.                                                      PPCTRTDE
017100     PERFORM 3000-EDIT-DATES.                                     PPCTRTDE
017200     MOVE 'C'                  TO RTD-LAST-ACTION.                PPCTRTDE
017300     MOVE KCTE-LAST-ACTION-DT  TO RTD-LAST-ACTION-DT.             PPCTRTDE
017400                                                                  PPCTRTDE
017500 2199-CHANGE-EXIT.                                                PPCTRTDE
017600     EXIT.                                                        PPCTRTDE
017700                                                                  PPCTRTDE
017800 2200-DELETE-RTD SECTION.                                         PPCTRTDE
017900                                                                  PPCTRTDE
018000     IF SQLCODE NOT = ZERO                                        PPCTRTDE
018100        ADD 1                   TO KCTE-ERR                       PPCTRTDE
018200        MOVE M01029             TO KCTE-ERR-NO (KCTE-ERR)         PPCTRTDE
018300        SET KCTE-STATUS-NOT-COMPLETED TO TRUE                     PPCTRTDE
018400        GO TO 2299-DELETE-EXIT                                    PPCTRTDE
018500     END-IF.                                                      PPCTRTDE
018600                                                                  PPCTRTDE
018700 2299-DELETE-EXIT.                                                PPCTRTDE
018800     EXIT.                                                        PPCTRTDE
018900                                                                  PPCTRTDE
019000 3000-EDIT-DATES SECTION.                                         PPCTRTDE
019100                                                                  PPCTRTDE
019200     IF  KCTE-ACTION-CHANGE                                       PPCTRTDE
019300     AND RTDI-EFF-DATE = SPACES                                   PPCTRTDE
019400         CONTINUE                                                 PPCTRTDE
019500     ELSE                                                         PPCTRTDE
019600       IF RTDI-EFF-DATE NOT NUMERIC                               PPCTRTDE
019700          ADD 1                TO KCTE-ERR                        PPCTRTDE
019800          MOVE M01425          TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
019900       ELSE                                                       PPCTRTDE
020000         IF RTDI-EFF-DATE = ALL '0'                               PPCTRTDE
020100         OR RTDI-EFF-DATE(5:4) = '0001'                           PPCTRTDE
020200            MOVE XDC3-LOW-ISO-DATE TO RTD-EFF-DATE                PPCTRTDE
020300         ELSE                                                     PPCTRTDE
020400           IF RTDI-EFF-DATE(5:4) = '9999'                         PPCTRTDE
020500              MOVE XDC3-HIGH-ISO-DATE TO RTD-EFF-DATE             PPCTRTDE
020600           ELSE                                                   PPCTRTDE
020700              MOVE RTDI-EFF-DATE TO XDC3-INPUT8-DATE              PPCTRTDE
020800              PERFORM XDC3-VALIDATE-INPUT8-TO-ISO                 PPCTRTDE
020900              IF XDC3-VALID-DATE                                  PPCTRTDE
021000                 MOVE XDC3-ISO-DATE TO RTD-EFF-DATE               PPCTRTDE
021100              ELSE                                                PPCTRTDE
021200                 ADD 1         TO KCTE-ERR                        PPCTRTDE
021300                 MOVE M01425   TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
021400              END-IF                                              PPCTRTDE
021500           END-IF                                                 PPCTRTDE
021600         END-IF                                                   PPCTRTDE
021700       END-IF                                                     PPCTRTDE
021800     END-IF.                                                      PPCTRTDE
021900     IF  KCTE-ACTION-CHANGE                                       PPCTRTDE
022000     AND RTDI-STRT-DATE = SPACES                                  PPCTRTDE
022100         CONTINUE                                                 PPCTRTDE
022200     ELSE                                                         PPCTRTDE
022300       IF RTDI-STRT-DATE NOT NUMERIC                              PPCTRTDE
022400          ADD 1                TO KCTE-ERR                        PPCTRTDE
022500          MOVE M01426          TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
022600       ELSE                                                       PPCTRTDE
022700         IF RTDI-STRT-DATE = ALL '0'                              PPCTRTDE
022800         OR RTDI-STRT-DATE(5:4) = '0001'                          PPCTRTDE
022900            MOVE XDC3-LOW-ISO-DATE TO RTD-STRT-DATE               PPCTRTDE
023000         ELSE                                                     PPCTRTDE
023100           IF RTDI-STRT-DATE(5:4) = '9999'                        PPCTRTDE
023200              MOVE XDC3-HIGH-ISO-DATE TO RTD-STRT-DATE            PPCTRTDE
023300           ELSE                                                   PPCTRTDE
023400              MOVE RTDI-STRT-DATE TO XDC3-INPUT8-DATE             PPCTRTDE
023500              PERFORM XDC3-VALIDATE-INPUT8-TO-ISO                 PPCTRTDE
023600              IF XDC3-VALID-DATE                                  PPCTRTDE
023700                 MOVE XDC3-ISO-DATE TO RTD-STRT-DATE              PPCTRTDE
023800              ELSE                                                PPCTRTDE
023900                 ADD 1         TO KCTE-ERR                        PPCTRTDE
024000                 MOVE M01426   TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
024100              END-IF                                              PPCTRTDE
024200           END-IF                                                 PPCTRTDE
024300         END-IF                                                   PPCTRTDE
024400       END-IF                                                     PPCTRTDE
024500     END-IF.                                                      PPCTRTDE
024600     IF  KCTE-ACTION-CHANGE                                       PPCTRTDE
024700     AND RTDI-STOP-DATE = SPACES                                  PPCTRTDE
024800         CONTINUE                                                 PPCTRTDE
024900     ELSE                                                         PPCTRTDE
025000       IF RTDI-STOP-DATE NOT NUMERIC                              PPCTRTDE
025100          ADD 1                TO KCTE-ERR                        PPCTRTDE
025200          MOVE M01427          TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
025300       ELSE                                                       PPCTRTDE
025400         IF RTDI-STOP-DATE = ALL '0'                              PPCTRTDE
025500         OR RTDI-STOP-DATE(5:4) = '0001'                          PPCTRTDE
025600            MOVE XDC3-LOW-ISO-DATE TO RTD-STOP-DATE               PPCTRTDE
025700         ELSE                                                     PPCTRTDE
025800           IF RTDI-STOP-DATE(5:4) = '9999'                        PPCTRTDE
025900              MOVE XDC3-HIGH-ISO-DATE TO RTD-STOP-DATE            PPCTRTDE
026000           ELSE                                                   PPCTRTDE
026100              MOVE RTDI-STOP-DATE TO XDC3-INPUT8-DATE             PPCTRTDE
026200              PERFORM XDC3-VALIDATE-INPUT8-TO-ISO                 PPCTRTDE
026300              IF XDC3-VALID-DATE                                  PPCTRTDE
026400                 MOVE XDC3-ISO-DATE TO RTD-STOP-DATE              PPCTRTDE
026500              ELSE                                                PPCTRTDE
026600                 ADD 1         TO KCTE-ERR                        PPCTRTDE
026700                 MOVE M01427   TO KCTE-ERR-NO (KCTE-ERR)          PPCTRTDE
026800              END-IF                                              PPCTRTDE
026900           END-IF                                                 PPCTRTDE
027000         END-IF                                                   PPCTRTDE
027100       END-IF                                                     PPCTRTDE
027200     END-IF.                                                      PPCTRTDE
027300                                                                  PPCTRTDE
027400 3099-EXIT.                                                       PPCTRTDE
027500     EXIT.                                                        PPCTRTDE
027600                                                                  PPCTRTDE
027700 9000-SELECT-RTD SECTION.                                         PPCTRTDE
027800                                                                  PPCTRTDE
027900     MOVE '9000-SEL-RTD ' TO DB2MSG-TAG                           PPCTRTDE
028000     EXEC SQL                                                     PPCTRTDE
028100         SELECT   RTD_RTN_TYPE                                    PPCTRTDE
028200                 ,RTD_RTN_NUM                                     PPCTRTDE
028300                 ,RTD_CALL_MODULE                                 PPCTRTDE
028400                 ,RTD_MODULE_DESC                                 PPCTRTDE
028500                 ,CHAR(RTD_EFF_DATE,ISO)                          PPCTRTDE
028600                 ,CHAR(RTD_STRT_DATE,ISO)                         PPCTRTDE
028700                 ,CHAR(RTD_STOP_DATE,ISO)                         PPCTRTDE
028800                 ,RTD_STATUS                                      PPCTRTDE
028900                 ,RTD_LAST_ACTION                                 PPCTRTDE
029000                 ,RTD_LAST_ACTION_DT                              PPCTRTDE
029100           INTO                                                   PPCTRTDE
029200                 :RTD-RTN-TYPE                                    PPCTRTDE
029300                ,:RTD-RTN-NUM                                     PPCTRTDE
029400                ,:RTD-CALL-MODULE                                 PPCTRTDE
029500                ,:RTD-MODULE-DESC                                 PPCTRTDE
029600                ,:RTD-EFF-DATE                                    PPCTRTDE
029700                ,:RTD-STRT-DATE                                   PPCTRTDE
029800                ,:RTD-STOP-DATE                                   PPCTRTDE
029900                ,:RTD-STATUS                                      PPCTRTDE
030000                ,:RTD-LAST-ACTION                                 PPCTRTDE
030100                ,:RTD-LAST-ACTION-DT                              PPCTRTDE
030200           FROM   PPPVZRTD_RTD                                    PPCTRTDE
030300          WHERE   RTD_RTN_TYPE        = :RTD-RTN-TYPE             PPCTRTDE
030400            AND   RTD_RTN_NUM         = :RTD-RTN-NUM              PPCTRTDE
030500     END-EXEC.                                                    PPCTRTDE
030600                                                                  PPCTRTDE
030700 9099-SELECT-RTD-EXIT.                                            PPCTRTDE
030800     EXIT.                                                        PPCTRTDE
030900                                                                  PPCTRTDE
031000 10000-DATE-CONVERSION.                                           PPCTRTDE
031100                                                                  PPCTRTDE
031200     COPY CPPDXDC3.                                               PPCTRTDE
031300                                                                  PPCTRTDE
031400*999999-SQL-ERROR.                                                PPCTRTDE
031500     EXEC SQL                                                     PPCTRTDE
031600        INCLUDE CPPDXP99                                          PPCTRTDE
031700     END-EXEC.                                                    PPCTRTDE
031800     ADD 1                         TO KCTE-ERR.                   PPCTRTDE
031900     MOVE MCT005                   TO KCTE-ERR-NO (KCTE-ERR).     PPCTRTDE
032000     SET KCTE-STATUS-NOT-COMPLETED TO TRUE.                       PPCTRTDE
032100     GO TO 0100-END.                                              PPCTRTDE
