000010**************************************************************/   30871424
000020*  PROGRAM: PPCTBRAU                                         */   30871424
000030*  RELEASE: ___1424______ SERVICE REQUEST(S): _____3087____  */   30871424
000040*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___07/30/02__  */   30871424
000050*  ERROR REPORT 1808:                                        */   30871424
000060*  SET ERROR FLAGS FOR NEGATIVE SQL ERRORS TO TRIGGER DB2    */   30871424
000070*  ERROR REPORTING IN TRANSACTION HANDLER PROGRAM.           */   30871424
000080**************************************************************/   30871424
000100**************************************************************/   30871413
000200*  PROGRAM: PPCTBRAU                                         */   30871413
000300*  RELEASE: ___1413______ SERVICE REQUEST(S): _____3087____  */   30871413
000400*  NAME:___SRS___________ CREATION DATE:      ___05/21/02__  */   30871413
000500*  DESCRIPTION:                                              */   30871413
000600*  BENEFITS RATES ADD TABLE UPDATE MODULE                    */   30871413
000700**************************************************************/   30871413
000800 IDENTIFICATION DIVISION.                                         PPCTBRAU
000900 PROGRAM-ID. PPCTBRAU.                                            PPCTBRAU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTBRAU
001100 DATE-COMPILED.                                                   PPCTBRAU
001200 ENVIRONMENT DIVISION.                                            PPCTBRAU
001300 DATA DIVISION.                                                   PPCTBRAU
001400 WORKING-STORAGE SECTION.                                         PPCTBRAU
001500                                                                  PPCTBRAU
001600 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTBRAU
001700 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTBRAU
001800                                                                  PPCTBRAU
001900     EXEC SQL                                                     PPCTBRAU
002000        INCLUDE SQLCA                                             PPCTBRAU
002100     END-EXEC.                                                    PPCTBRAU
002200                                                                  PPCTBRAU
002300 LINKAGE SECTION.                                                 PPCTBRAU
002400                                                                  PPCTBRAU
002500 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTBRAU
002600 01  BRA-ADD-TABLE-DATA.                                          PPCTBRAU
002700     EXEC SQL                                                     PPCTBRAU
002800        INCLUDE PPPVZBRA                                          PPCTBRAU
002900     END-EXEC.                                                    PPCTBRAU
003000                                                                  PPCTBRAU
003100 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE            PPCTBRAU
003200                          BRA-ADD-TABLE-DATA.                     PPCTBRAU
003300                                                                  PPCTBRAU
003400 0000-MAIN.                                                       PPCTBRAU
003500                                                                  PPCTBRAU
003600     EXEC SQL                                                     PPCTBRAU
003700        INCLUDE CPPDXE99                                          PPCTBRAU
003800     END-EXEC.                                                    PPCTBRAU
003900     MOVE 'PPCTBRAU' TO XWHC-DISPLAY-MODULE.                      PPCTBRAU
004000     COPY CPPDXWHC.                                               PPCTBRAU
004100     MOVE SPACE TO KCTU-STATUS.                                   PPCTBRAU
004200     IF KCTU-ACTION-ADD                                           PPCTBRAU
004300        PERFORM 2000-INSERT-BRA                                   PPCTBRAU
004400     ELSE                                                         PPCTBRAU
004500       IF KCTU-ACTION-CHANGE                                      PPCTBRAU
004600          PERFORM 2100-UPDATE-BRA                                 PPCTBRAU
004700       ELSE                                                       PPCTBRAU
004800         IF KCTU-ACTION-DELETE                                    PPCTBRAU
004900            PERFORM 2200-DELETE-BRA                               PPCTBRAU
005000         ELSE                                                     PPCTBRAU
005100            SET KCTU-INVALID-ACTION TO TRUE                       PPCTBRAU
005200         END-IF                                                   PPCTBRAU
005300       END-IF                                                     PPCTBRAU
005400     END-IF.                                                      PPCTBRAU
005500                                                                  PPCTBRAU
005600 0100-END.                                                        PPCTBRAU
005700                                                                  PPCTBRAU
005800     GOBACK.                                                      PPCTBRAU
005900                                                                  PPCTBRAU
006000 2000-INSERT-BRA.                                                 PPCTBRAU
006100                                                                  PPCTBRAU
006200     MOVE '2000-INSRT-BRA' TO DB2MSG-TAG.                         PPCTBRAU
006300     EXEC SQL                                                     PPCTBRAU
006400         INSERT INTO PPPVZBRA_BRA                                 PPCTBRAU
006500              VALUES (:BRA-CBUC                                   PPCTBRAU
006600                     ,:BRA-REP                                    PPCTBRAU
006700                     ,:BRA-SHC                                    PPCTBRAU
006800                     ,:BRA-DUC                                    PPCTBRAU
006900                     ,:BRA-PRINCIPAL-SUM                          PPCTBRAU
007000                     ,:BRA-SINGLE-RATE                            PPCTBRAU
007100                     ,:BRA-FAMILY-RATE                            PPCTBRAU
007200                     ,:BRA-MODIFIED-RATE                          PPCTBRAU
007300                     ,:BRA-STATUS                                 PPCTBRAU
007700                     ,:BRA-EFFECTIVE-DATE                         PPCTBRAU
007800                     ,:BRA-LAST-ACTION                            PPCTBRAU
007900                     ,:BRA-LAST-ACTION-DT                         PPCTBRAU
008000                     )                                            PPCTBRAU
008100     END-EXEC.                                                    PPCTBRAU
008200     IF SQLCODE NOT = ZERO                                        PPCTBRAU
008300        SET KCTU-INSERT-ERROR TO TRUE                             PPCTBRAU
008400     END-IF.                                                      PPCTBRAU
008500                                                                  PPCTBRAU
008600 2100-UPDATE-BRA.                                                 PPCTBRAU
008700                                                                  PPCTBRAU
008800     MOVE '2100-UPD-BRA ' TO DB2MSG-TAG.                          PPCTBRAU
008900     EXEC SQL                                                     PPCTBRAU
009000         UPDATE PPPVZBRA_BRA                                      PPCTBRAU
009100            SET BRA_CBUC              = :BRA-CBUC                 PPCTBRAU
009200               ,BRA_REP               = :BRA-REP                  PPCTBRAU
009300               ,BRA_SHC               = :BRA-SHC                  PPCTBRAU
009400               ,BRA_DUC               = :BRA-DUC                  PPCTBRAU
009500               ,BRA_PRINCIPAL_SUM     = :BRA-PRINCIPAL-SUM        PPCTBRAU
009600               ,BRA_SINGLE_RATE       = :BRA-SINGLE-RATE          PPCTBRAU
009700               ,BRA_FAMILY_RATE       = :BRA-FAMILY-RATE          PPCTBRAU
009800               ,BRA_MODIFIED_RATE     = :BRA-MODIFIED-RATE        PPCTBRAU
009900               ,BRA_STATUS            = :BRA-STATUS               PPCTBRAU
010300               ,BRA_EFFECTIVE_DATE    = :BRA-EFFECTIVE-DATE       PPCTBRAU
010400               ,BRA_LAST_ACTION       = :BRA-LAST-ACTION          PPCTBRAU
010410               ,BRA_LAST_ACTION_DT    = :BRA-LAST-ACTION-DT       PPCTBRAU
010500          WHERE BRA_CBUC              = :BRA-CBUC                 PPCTBRAU
010600            AND BRA_REP               = :BRA-REP                  PPCTBRAU
010610            AND BRA_SHC               = :BRA-SHC                  PPCTBRAU
010620            AND BRA_DUC               = :BRA-DUC                  PPCTBRAU
010630            AND BRA_PRINCIPAL_SUM     = :BRA-PRINCIPAL-SUM        PPCTBRAU
010700     END-EXEC.                                                    PPCTBRAU
010800     IF SQLCODE NOT = ZERO                                        PPCTBRAU
010900        SET KCTU-UPDATE-ERROR TO TRUE                             PPCTBRAU
011000     END-IF.                                                      PPCTBRAU
011100                                                                  PPCTBRAU
011200 2200-DELETE-BRA.                                                 PPCTBRAU
011300                                                                  PPCTBRAU
011400     MOVE '2200-DLTE-BRA ' TO DB2MSG-TAG.                         PPCTBRAU
011500     EXEC SQL                                                     PPCTBRAU
011600         DELETE FROM PPPVZBRA_BRA                                 PPCTBRAU
011810          WHERE BRA_CBUC              = :BRA-CBUC                 PPCTBRAU
011820            AND BRA_REP               = :BRA-REP                  PPCTBRAU
011830            AND BRA_SHC               = :BRA-SHC                  PPCTBRAU
011840            AND BRA_DUC               = :BRA-DUC                  PPCTBRAU
011841            AND BRA_PRINCIPAL_SUM     = :BRA-PRINCIPAL-SUM        PPCTBRAU
011900     END-EXEC.                                                    PPCTBRAU
012000     IF SQLCODE NOT = ZERO                                        PPCTBRAU
012100        SET KCTU-DELETE-ERROR TO TRUE                             PPCTBRAU
012200     END-IF.                                                      PPCTBRAU
012300                                                                  PPCTBRAU
012400*999999-SQL-ERROR.                                                PPCTBRAU
012500     EXEC SQL                                                     PPCTBRAU
012600        INCLUDE CPPDXP99                                          PPCTBRAU
012700     END-EXEC.                                                    PPCTBRAU
012710     EVALUATE TRUE                                                30871424
012720       WHEN (KCTU-ACTION-ADD)                                     30871424
012730         SET KCTU-INSERT-ERROR TO TRUE                            30871424
012740       WHEN (KCTU-ACTION-CHANGE)                                  30871424
012750         SET KCTU-UPDATE-ERROR TO TRUE                            30871424
012760       WHEN (KCTU-ACTION-DELETE)                                  30871424
012770         SET KCTU-DELETE-ERROR TO TRUE                            30871424
012780     END-EVALUATE.                                                30871424
012800     GO TO 0100-END.                                              PPCTBRAU
