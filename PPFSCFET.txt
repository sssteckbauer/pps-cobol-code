000000**************************************************************/   28521087
000001*  PROGRAM: PPFSCFET                                         */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/22/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  ** DATE CONVERSION II **                                  */   28521087
000006*  - REPLACED CPWSXDC2 AND CPPDXDC2 WITH CPWSXDC3 AND CPPDXDC3/   28521087
000007*  - COMMENTED PARAGRAPH 999999-EXIT. CAN NEVER BE EXECUTED  */   28521087
000009**************************************************************/   28521087
000100**************************************************************/   36330704
000200*  PROGRAM: PPFSCFET                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPFSC SELECT MODULE                                */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPFSCFET
001000 PROGRAM-ID. PPFSCFET.                                            PPFSCFET
001100 ENVIRONMENT DIVISION.                                            PPFSCFET
001200 DATA DIVISION.                                                   PPFSCFET
001300 WORKING-STORAGE SECTION.                                         PPFSCFET
001400*                                                                 PPFSCFET
001500     COPY CPWSXBEG.                                               PPFSCFET
001600*                                                                 PPFSCFET
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPFSCFET'.       PPFSCFET
001800*                                                                 PPFSCFET
001900     EXEC SQL                                                     PPFSCFET
002000         INCLUDE CPWSWRKA                                         PPFSCFET
002100     END-EXEC.                                                    PPFSCFET
002200*                                                                 PPFSCFET
002300*01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC2.                28521087
002310 01  DATE-CONVERSION-WORK-AREAS.    COPY CPWSXDC3.                28521087
002400*                                                                 PPFSCFET
002500 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPFSCFET
002600*                                                                 PPFSCFET
002700 01  PPPFSC-ROW EXTERNAL.                                         PPFSCFET
002800     EXEC SQL                                                     PPFSCFET
002900         INCLUDE PPPVFSC1                                         PPFSCFET
003000     END-EXEC.                                                    PPFSCFET
003100*                                                                 PPFSCFET
003200*------------------------------------------------------------*    PPFSCFET
003300*  SQL DCLGEN AREA                                           *    PPFSCFET
003400*------------------------------------------------------------*    PPFSCFET
003500     EXEC SQL                                                     PPFSCFET
003600         INCLUDE SQLCA                                            PPFSCFET
003700     END-EXEC.                                                    PPFSCFET
003800*                                                                 PPFSCFET
003900 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPFSCFET
004000*                                                                 PPFSCFET
004100     COPY CPWSXEND.                                               PPFSCFET
004200*                                                                 PPFSCFET
004300 LINKAGE SECTION.                                                 PPFSCFET
004400*                                                                 PPFSCFET
004500 01  SELECT-INTERFACE-AREA.                                       PPFSCFET
004600     COPY CPWSXHIF REPLACING ==:TAG:== BY ==XSEL==.               PPFSCFET
004700*                                                                 PPFSCFET
004800 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPFSCFET
004900*                                                                 PPFSCFET
005000     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPFSCFET
005100     COPY  CPPDXWHC.                                              PPFSCFET
005200*                                                                 PPFSCFET
005300     EXEC SQL                                                     PPFSCFET
005400         INCLUDE CPPDXE99                                         PPFSCFET
005500     END-EXEC.                                                    PPFSCFET
005600*                                                                 PPFSCFET
005700     COPY CPPDSELM.                                               PPFSCFET
005800*                                                                 PPFSCFET
005900 2000-SELECT SECTION.                                             PPFSCFET
006000*                                                                 PPFSCFET
006100*                                                                 PPFSCFET
006200     MOVE 'SELECT MAX TIMESTAMP' TO DB2MSG-TAG.                   PPFSCFET
006300     STRING WS-QUERY-DATE '-' WS-QUERY-TIME                       PPFSCFET
006400       DELIMITED BY SIZE INTO WS-QUERY-TIMESTAMP.                 PPFSCFET
006500*                                                                 PPFSCFET
006600     EXEC SQL                                                     PPFSCFET
006700         SELECT VALUE(MAX(TIMESTAMP                               PPFSCFET
006800               (SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME)),            PPFSCFET
006900                TIMESTAMP('0001-01-01-00.00.00'))                 PPFSCFET
007000         INTO :WS-MAX-TIMESTAMP                                   PPFSCFET
007100         FROM PPPVFSC1_FSC                                        PPFSCFET
007200         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPFSCFET
007300         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPFSCFET
007400         AND TIMESTAMP(SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME) ^>    PPFSCFET
007500             :WS-QUERY-TIMESTAMP                                  PPFSCFET
007600     END-EXEC.                                                    PPFSCFET
007700*                                                                 PPFSCFET
007800     IF SQLCODE = +100                                            PPFSCFET
007900     OR WS-MAX-TIMESTAMP = '0001-01-01-00.00.00'                  PPFSCFET
008000        INITIALIZE XSEL-RETURN-KEY                                PPFSCFET
008100                   PPPFSC-ROW                                     PPFSCFET
008200        SET XSEL-INVALID-KEY TO TRUE                              PPFSCFET
008300        GO TO 2000-EXIT                                           PPFSCFET
008400     END-IF.                                                      PPFSCFET
008500*                                                                 PPFSCFET
008600     MOVE 'SELECT PPPFSC ROW' TO DB2MSG-TAG.                      PPFSCFET
008700*                                                                 PPFSCFET
008800     EXEC SQL                                                     PPFSCFET
008900          SELECT                                                  PPFSCFET
009000             EMPLID             ,                                 PPFSCFET
009100             ITERATION_NUMBER   ,                                 PPFSCFET
009200             CHAR(SYSTEM_ENTRY_DATE, ISO),                        PPFSCFET
009300             CHAR(SYSTEM_ENTRY_TIME, ISO),                        PPFSCFET
009400             DELETE_FLAG        ,                                 PPFSCFET
009500             ERH_INCORRECT      ,                                 PPFSCFET
009600             FTD_ADDTNL_UNEX    ,                                 PPFSCFET
009700             FTD_ADDTNL_UNEX_C                                    PPFSCFET
009800       INTO :EMPLID             ,                                 PPFSCFET
009900            :ITERATION-NUMBER   ,                                 PPFSCFET
010000            :SYSTEM-ENTRY-DATE  ,                                 PPFSCFET
010100            :SYSTEM-ENTRY-TIME  ,                                 PPFSCFET
010200            :DELETE-FLAG        ,                                 PPFSCFET
010300            :ERH-INCORRECT      ,                                 PPFSCFET
010400            :FTD-ADDTNL-UNEX    ,                                 PPFSCFET
010500            :FTD-ADDTNL-UNEX-C                                    PPFSCFET
010600         FROM PPPVFSC1_FSC                                        PPFSCFET
010700         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPFSCFET
010800         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPFSCFET
010900         AND TIMESTAMP(SYSTEM_ENTRY_DATE,SYSTEM_ENTRY_TIME) =     PPFSCFET
011000             :WS-MAX-TIMESTAMP                                    PPFSCFET
011100     END-EXEC.                                                    PPFSCFET
011200*                                                                 PPFSCFET
011300     IF SQLCODE = +100                                            PPFSCFET
011400        INITIALIZE XSEL-RETURN-KEY                                PPFSCFET
011500                   PPPFSC-ROW                                     PPFSCFET
011600        SET XSEL-INVALID-KEY TO TRUE                              PPFSCFET
011700        GO TO 2000-EXIT                                           PPFSCFET
011800     END-IF.                                                      PPFSCFET
011900*                                                                 PPFSCFET
012000     IF SQLCODE = +0                                              PPFSCFET
012100        MOVE EMPLID TO XSEL-RETURN-EMPLID                         PPFSCFET
012200        MOVE ITERATION-NUMBER TO XSEL-RETURN-ITERATION            PPFSCFET
012300********MOVE SYSTEM-ENTRY-DATE TO WSX-INQ-DATE                    28521087
012400********PERFORM CONVERT-DATE8-TO-ISO-DATE                         28521087
012500********MOVE DB2-DATE TO SYSTEM-ENTRY-DATE                        28521087
012600********                 XSEL-RETURN-DATE                         28521087
012610        MOVE SYSTEM-ENTRY-DATE TO XDC3-DB2-DATE                   28521087
012620        PERFORM XDC3-CONVERT-DB2-TO-ISO                           28521087
012630        MOVE XDC3-ISO-DATE TO SYSTEM-ENTRY-DATE                   28521087
012640                         XSEL-RETURN-DATE                         28521087
012700        MOVE SYSTEM-ENTRY-TIME TO XSEL-RETURN-TIME                PPFSCFET
012800        SET XSEL-OK TO TRUE                                       PPFSCFET
012900        MOVE +0 TO XSEL-DB2-MSG-NUMBER                            PPFSCFET
013000        GO TO 2000-EXIT                                           PPFSCFET
013100     END-IF.                                                      PPFSCFET
013200*                                                                 PPFSCFET
013300 2000-EXIT.                                                       PPFSCFET
013400     EXIT.                                                        PPFSCFET
013500*                                                                 PPFSCFET
013600*****COPY CPPDXDC2.                                               28521087
013610 9000-DATE-CONVERSION-DB2 SECTION.                                28521087
013620     COPY CPPDXDC3.                                               28521087
013700*                                                                 PPFSCFET
013800*999999-SQL-ERROR SECTION.                                        PPFSCFET
013900     COPY CPPDXP99.                                               PPFSCFET
014000     SET XSEL-DB2-ERROR TO TRUE.                                  PPFSCFET
014100     MOVE SQLCODE TO XSEL-DB2-MSG-NUMBER.                         PPFSCFET
014200     GOBACK.                                                      PPFSCFET
014300*999999-EXIT.                                                     28521087
014400*****EXIT.                                                        28521087
014500*                                                                 PPFSCFET
