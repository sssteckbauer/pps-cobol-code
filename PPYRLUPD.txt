000010**************************************************************/   32041155
000020*  PROGRAM: PPYRLUPD                                         */   32041155
000030*  RELEASE # ___1155___   SERVICE REQUEST NO(S)____3204______*/   32041155
000040*  NAME ________JLT____   MODIFICATION DATE ____12/01/97_____*/   32041155
000050*  DESCRIPTION                                               */   32041155
000060*    ADDED YTD-ESL-GROSS AND YTD-WCR-REFUND AND RELATED      */   32041155
000070*    SWITCH CODE PROCESSING.                                 */   32041155
000080**************************************************************/   32041155
000100**************************************************************/   36330704
000200*  PROGRAM: PPYRLUPD                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE PPPYRL UPDATE MODULE                                */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900 IDENTIFICATION DIVISION.                                         PPYRLUPD
001000 PROGRAM-ID. PPYRLUPD.                                            PPYRLUPD
001100 ENVIRONMENT DIVISION.                                            PPYRLUPD
001200 DATA DIVISION.                                                   PPYRLUPD
001300 WORKING-STORAGE SECTION.                                         PPYRLUPD
001400*                                                                 PPYRLUPD
001500     COPY CPWSXBEG.                                               PPYRLUPD
001600*                                                                 PPYRLUPD
001700 01  A-STND-PROG-ID            PIC  X(8)  VALUE 'PPYRLUPD'.       PPYRLUPD
001800*                                                                 PPYRLUPD
001900     EXEC SQL                                                     PPYRLUPD
002000         INCLUDE CPWSWRKA                                         PPYRLUPD
002100     END-EXEC.                                                    PPYRLUPD
002200*                                                                 PPYRLUPD
002300 01  PPDB2MSG-INTERFACE.  COPY CPLNKDB2.                          PPYRLUPD
002400*                                                                 PPYRLUPD
002500 01  PPPYRL-ROW EXTERNAL.                                         PPYRLUPD
002600     EXEC SQL                                                     PPYRLUPD
002700         INCLUDE PPPVYRL1                                         PPYRLUPD
002800     END-EXEC.                                                    PPYRLUPD
002900*                                                                 PPYRLUPD
003000*------------------------------------------------------------*    PPYRLUPD
003100*  SQL DCLGEN AREA                                           *    PPYRLUPD
003200*------------------------------------------------------------*    PPYRLUPD
003300     EXEC SQL                                                     PPYRLUPD
003400         INCLUDE SQLCA                                            PPYRLUPD
003500     END-EXEC.                                                    PPYRLUPD
003600*                                                                 PPYRLUPD
003700 01  XWHC-COMPILE-WORK-AREA.             COPY CPWSXWHC.           PPYRLUPD
003800*                                                                 PPYRLUPD
003900     COPY CPWSXEND.                                               PPYRLUPD
004000*                                                                 PPYRLUPD
004100 LINKAGE SECTION.                                                 PPYRLUPD
004200*                                                                 PPYRLUPD
004300 01  SELECT-INTERFACE-AREA.                                       PPYRLUPD
004400     COPY CPWSXHIF REPLACING ==:TAG:== BY ==XSEL==.               PPYRLUPD
004500*                                                                 PPYRLUPD
004600 PROCEDURE DIVISION USING SELECT-INTERFACE-AREA.                  PPYRLUPD
004700*                                                                 PPYRLUPD
004800     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPYRLUPD
004900     COPY  CPPDXWHC.                                              PPYRLUPD
005000*                                                                 PPYRLUPD
005100     EXEC SQL                                                     PPYRLUPD
005200         INCLUDE CPPDXE99                                         PPYRLUPD
005300     END-EXEC.                                                    PPYRLUPD
005400*                                                                 PPYRLUPD
005500     COPY CPPDUPDM.                                               PPYRLUPD
005600*                                                                 PPYRLUPD
005700 2000-UPDATE SECTION.                                             PPYRLUPD
005800*                                                                 PPYRLUPD
005900     MOVE 'UPDATE PPPYRL ROW' TO DB2MSG-TAG.                      PPYRLUPD
006000*                                                                 PPYRLUPD
006100     EXEC SQL                                                     PPYRLUPD
006200         UPDATE PPPVYRL1_YRL                                      PPYRLUPD
006300         SET DELETE_FLAG        = :DELETE-FLAG        ,           PPYRLUPD
006400             ERH_INCORRECT      = :ERH-INCORRECT      ,           PPYRLUPD
006500             YTD_TOTAL_GROSS    = :YTD-TOTAL-GROSS    ,           PPYRLUPD
006600             YTD_TOTAL_GROSS_C  = :YTD-TOTAL-GROSS-C  ,           PPYRLUPD
006700             YTD_REGULAR_PAY    = :YTD-REGULAR-PAY    ,           PPYRLUPD
006800             YTD_REGULAR_PAY_C  = :YTD-REGULAR-PAY-C  ,           PPYRLUPD
006900             YTD_ADDTNL_COMP    = :YTD-ADDTNL-COMP    ,           PPYRLUPD
007000             YTD_ADDTNL_COMP_C  = :YTD-ADDTNL-COMP-C              PPYRLUPD
007010           , YTD_ESL_GROSS      = :YTD-ESL-GROSS                  32041155
007020           , YTD_ESL_GROSS_C    = :YTD-ESL-GROSS-C                32041155
007030           , YTD_WCR_REFUND     = :YTD-WCR-REFUND                 32041155
007040           , YTD_WCR_REFUND_C   = :YTD-WCR-REFUND-C               32041155
007100         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPYRLUPD
007200         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPYRLUPD
007300         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPYRLUPD
007400         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPYRLUPD
007500     END-EXEC.                                                    PPYRLUPD
007600*                                                                 PPYRLUPD
007700     IF SQLCODE = +100                                            PPYRLUPD
007800         INITIALIZE XSEL-RETURN-KEY                               PPYRLUPD
007900         SET XSEL-INVALID-KEY TO TRUE                             PPYRLUPD
008000     END-IF.                                                      PPYRLUPD
008100*                                                                 PPYRLUPD
008200 2000-EXIT.                                                       PPYRLUPD
008300     EXIT.                                                        PPYRLUPD
008400*                                                                 PPYRLUPD
008500 3000-INSERT SECTION.                                             PPYRLUPD
008600*                                                                 PPYRLUPD
008700     MOVE 'INSERT PPPYRL ROW' TO DB2MSG-TAG.                      PPYRLUPD
008800*                                                                 PPYRLUPD
008900     EXEC SQL                                                     PPYRLUPD
009000         INSERT                                                   PPYRLUPD
009100         INTO PPPVYRL1_YRL                                        PPYRLUPD
009200         VALUES (:PPPYRL-ROW)                                     PPYRLUPD
009300     END-EXEC.                                                    PPYRLUPD
009400*                                                                 PPYRLUPD
009500 3000-EXIT.                                                       PPYRLUPD
009600     EXIT.                                                        PPYRLUPD
009700*                                                                 PPYRLUPD
009800 4000-DELETE SECTION.                                             PPYRLUPD
009900*                                                                 PPYRLUPD
010000     MOVE 'DELETE PPPYRL ROW' TO DB2MSG-TAG.                      PPYRLUPD
010100*                                                                 PPYRLUPD
010200     EXEC SQL                                                     PPYRLUPD
010300         DELETE                                                   PPYRLUPD
010400         FROM PPPVYRL1_YRL                                        PPYRLUPD
010500         WHERE EMPLID             = :WS-EMPLOYEE-ID               PPYRLUPD
010600         AND   ITERATION_NUMBER   = :WS-ITERATION                 PPYRLUPD
010700         AND   SYSTEM_ENTRY_DATE  = :WS-QUERY-DATE                PPYRLUPD
010800         AND   SYSTEM_ENTRY_TIME  = :WS-QUERY-TIME                PPYRLUPD
010900     END-EXEC.                                                    PPYRLUPD
011000*                                                                 PPYRLUPD
011100     IF SQLCODE = +100                                            PPYRLUPD
011200         INITIALIZE XSEL-RETURN-KEY                               PPYRLUPD
011300         SET XSEL-INVALID-KEY TO TRUE                             PPYRLUPD
011400     END-IF.                                                      PPYRLUPD
011500*                                                                 PPYRLUPD
011600 4000-EXIT.                                                       PPYRLUPD
011700     EXIT.                                                        PPYRLUPD
011800*                                                                 PPYRLUPD
011900*999999-SQL-ERROR SECTION.                                        PPYRLUPD
012000     COPY CPPDXP99.                                               PPYRLUPD
012100     SET XSEL-DB2-ERROR TO TRUE.                                  PPYRLUPD
012200     GOBACK.                                                      PPYRLUPD
012300 999999-EXIT.                                                     PPYRLUPD
012400     EXIT.                                                        PPYRLUPD
012500*                                                                 PPYRLUPD
