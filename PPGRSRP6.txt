000100**************************************************************/   48761492
000200*  PROGRAM: PPGRSRP6                                         */   48761492
000300*  RELEASE: ___1492______ SERVICE REQUEST(S): ____14876____  */   48761492
000400*  NAME:_______JLT_______ MODIFICATION DATE:  ___02/24/03__  */   48761492
000500*  DESCRIPTION:                                              */   48761492
000600*  - ADDED PROCESSING FOR FINAL PAY "YX" COMPUTE.            */   48761492
000700**************************************************************/   48761492
000001**************************************************************/   30871230
000002*  PROGRAM: PPGRSRP6                                         */   30871230
000003*  RELEASE: ___1230______ SERVICE REQUEST(S): _____3087____  */   30871230
000004*  NAME:___SRS___________ MODIFICATION DATE:  ___03/08/99__  */   30871230
000005*  DESCRIPTION:                                              */   30871230
000006*  REMOVE UNUSED MESSAGE UTILITY REFERENCE.                  */   30871230
000007*  CHANGE VSAM CTL ACCESS TO DB2.                            */   30871230
000008**************************************************************/   30871230
000000**************************************************************/   28521087
000001*  PROGRAM: PPGRSRP6                                         */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  REF RLSE:___0930______                                    */   28521087
000004*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/12/96__  */   28521087
000005*  DESCRIPTION:                                              */   28521087
000006*  - REPORT NAME PPP3906 ALREADY EXISTS (PRODUCED FROM       */   28521087
000007*    PPLVHRS). REPORT NAME HAS BEEN CHANGED TO PPP3909.      */   28521087
000008**************************************************************/   28521087
000100**************************************************************/   17040930
000200*  PROGRAM: PPGRSRP6                                         */   17040930
000300*  RELEASE: ___0930______ SERVICE REQUEST(S): ____11704____  */   17040930
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___09/15/94__  */   17040930
000500*  DESCRIPTION:                                              */   17040930
000600*  -  PRODUCES THE HOURS ADJUSTMENT REPORT PPP3906 FROM THE       17040930
000700*     COMPUTE PROCESS. CALLED BY PPGRSPAR.                   */   17040930
000800**************************************************************/   17040930
000900     SKIP2                                                        PPGRSRP6
001000 IDENTIFICATION DIVISION.                                         PPGRSRP6
001100 PROGRAM-ID. PPGRSRP6.                                            PPGRSRP6
001200 INSTALLATION.  UNIVERSITY OF CALIFORNIA.                         PPGRSRP6
001300 AUTHOR.                                                          PPGRSRP6
001400 DATE-WRITTEN.  SEPTEMBER 15, 1994.                               PPGRSRP6
001500 DATE-COMPILED.                                                   PPGRSRP6
001600     SKIP3                                                        PPGRSRP6
001700 ENVIRONMENT DIVISION.                                            PPGRSRP6
001800     SKIP2                                                        PPGRSRP6
001900 CONFIGURATION SECTION.                                           PPGRSRP6
002000     SKIP1                                                        PPGRSRP6
002100 SPECIAL-NAMES.                                                   PPGRSRP6
002200     C01 IS NEW-PAGE.                                             PPGRSRP6
002300     SKIP2                                                        PPGRSRP6
002400 INPUT-OUTPUT SECTION.                                            PPGRSRP6
002500     SKIP1                                                        PPGRSRP6
002600 FILE-CONTROL.                                                    PPGRSRP6
002700     SKIP1                                                        PPGRSRP6
002800*                                                              CD 30871230
002810     SELECT REPORT9-FILE ASSIGN TO PRNTFIL9.                      28521087
002900     EJECT                                                        PPGRSRP6
003000 DATA DIVISION.                                                   PPGRSRP6
003100     SKIP2                                                        PPGRSRP6
003200 FILE SECTION.                                                    PPGRSRP6
003300     SKIP1                                                        PPGRSRP6
003310*                                                              CD 30871230
003410 FD  REPORT9-FILE                                                 28521087
003500     RECORDING MODE IS F                                          PPGRSRP6
003600     LABEL RECORDS ARE OMITTED                                    PPGRSRP6
003610*                                                              CD 30871230
003710     DATA RECORD IS REPORT9-REC.                                  28521087
003800     SKIP1                                                        PPGRSRP6
003810*                                                              CD 30871230
003910 01  REPORT9-REC.                                                 28521087
004000     05  CARRIAGE-CTL-CHAR       PIC  X(01).                      PPGRSRP6
004100     05  FILLER                  PIC  X(132).                     PPGRSRP6
004200     SKIP2                                                        PPGRSRP6
004300 WORKING-STORAGE  SECTION.                                        PPGRSRP6
004400     SKIP1                                                        PPGRSRP6
004500*                                                                 PPGRSRP6
004600 01  WA-COMPILE-WORK-AREA.       COPY CPWSXWHC.                   PPGRSRP6
004700*                                                                 PPGRSRP6
004800     SKIP3                                                        PPGRSRP6
004900 01  PAYROLL-PROCESS-ID EXTERNAL. COPY CPWSXPPI.                  PPGRSRP6
005000 01  FILLER.                                                      PPGRSRP6
005100     05  LINE-CNT                PIC S9(03) COMP-3   VALUE +99.   PPGRSRP6
005200     05  PRT-SPACING             PIC S9(01) COMP-3   VALUE ZERO.  PPGRSRP6
005210     05  WS-DEPT-CODE            PIC  X(06).                      30871230
005300************SSG                  PIC  X(08)          VALUE        30871230
005400*********************************************************SSG'.    30871230
005500*****05  PPIOCTL                 PIC  X(08)          VALUE        30871230
005600*****                                                'PPIOCTL'.   30871230
005700*****05  PPINAFP                 PIC  X(08)          VALUE        30871230
005800*****                                                'PPINAFP'.   30871230
005900     05  FIRST-TIME-SW           PIC  X(01)          VALUE 'Y'.   PPGRSRP6
006000         88  FIRST-TIME                              VALUE 'Y'.   PPGRSRP6
006100         88  NOT-FIRST-TIME                          VALUE 'N'.   PPGRSRP6
006200     05  WS-PGM-ID.                                               PPGRSRP6
006300         10  FILLER              PIC X(08) VALUE 'PPGRSRP6'.      PPGRSRP6
006310*                                                              CD 30871230
006410*********10  DATE-CMPLD          PIC X(07) VALUE '/080196'.       30871230
006420         10  DATE-CMPLD          PIC X(07) VALUE '/111698'.       30871230
006500     SKIP1                                                        PPGRSRP6
006600 01  STND-RPT-HD.                COPY CPWSPSHR.                   PPGRSRP6
006700*****SKIP1                                                        30871230
006800*****XCFK-CONTROL-FILE-KEYS.     COPY CPWSXCFK.                   30871230
006900*****SKIP1                                                        30871230
007000*****CTL-INTERFACE.              COPY CPWSXCIF.                   30871230
007100*****SKIP1                                                        30871230
007200*****CTL-SEGMENT-TABLE.          COPY CPWSXCST.                   30871230
007300*****SKIP1                                                        30871230
007400*****XHME-DEPARTMENT-TABLE REDEFINES CTL-SEGMENT-TABLE.           30871230
007500*****                            COPY CPWSXHME.                   30871230
007600*****SKIP1                                                        30871230
007700*****IO-FUNCTIONS.               COPY CPWSXIOF.                   30871230
007800*****SKIP1                                                        30871230
007900 01  PRT-LINE                    PIC  X(133)     VALUE SPACE.     PPGRSRP6
008000     SKIP1                                                        PPGRSRP6
008100 01  MSG-LINE1 REDEFINES PRT-LINE.                                PPGRSRP6
008200     05  FILLER                  PIC  X(01).                      PPGRSRP6
008300     05  ML1-EMPL-ID             PIC  X(09).                      PPGRSRP6
008400     05  FILLER                  PIC  X(03).                      PPGRSRP6
008500     05  ML1-EMPL-NAME           PIC  X(30).                      PPGRSRP6
008600     05  FILLER                  PIC  X(03).                      PPGRSRP6
008700     05  ML1-DEPT-NO             PIC  X(06).                      PPGRSRP6
008800     05  FILLER                  PIC  X(02).                      PPGRSRP6
008900     05  ML1-DEPT-NAME           PIC  X(27).                      PPGRSRP6
009000     05  FILLER                  PIC  X(01).                      PPGRSRP6
009100     05  ML1-HOURS-DATE.                                          PPGRSRP6
009200         10 ML1-MM               PIC X(02).                       PPGRSRP6
009300         10 ML1-SEP              PIC X(01).                       PPGRSRP6
009400         10 ML1-YY               PIC X(02).                       PPGRSRP6
009500     05  FILLER                  PIC X(52).                       PPGRSRP6
009600     SKIP1                                                        PPGRSRP6
009700 01  PRT-HD4.                                                     PPGRSRP6
009800     05  FILLER                  PIC  X(01) VALUE SPACES.         PPGRSRP6
009900     05  HD1-EMPL-ID             PIC  X(09) VALUE 'EMP ID. '.     PPGRSRP6
010000     05  FILLER                  PIC  X(03) VALUE SPACES.         PPGRSRP6
010100     05  HD1-EMPL-NAME           PIC  X(05) VALUE 'NAME '.        PPGRSRP6
010200     05  FILLER                  PIC  X(28) VALUE SPACES.         PPGRSRP6
010300     05  HD1-DEPT                PIC  X(10) VALUE 'DEPARTMENT'.   PPGRSRP6
010400     05  FILLER                  PIC  X(25) VALUE SPACES.         PPGRSRP6
010500     05  HD1-HOURS-DATE          PIC  X(15) VALUE                 PPGRSRP6
010600                                       'ADJUSTMENT DATE'.         PPGRSRP6
010700     05  FILLER                  PIC  X(54) VALUE SPACES.         PPGRSRP6
010800     SKIP2                                                        PPGRSRP6
010810 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                30871230
010820 01  HME-DATA.                                                    30871230
010830     EXEC SQL                                                     30871230
010840          INCLUDE PPPVZHME                                        30871230
010850     END-EXEC.                                                    30871230
010860     EXEC SQL                                                     30871230
010870          INCLUDE SQLCA                                           30871230
010880     END-EXEC.                                                    30871230
010900 LINKAGE SECTION.                                                 PPGRSRP6
011000     SKIP1                                                        PPGRSRP6
011100     SKIP1                                                        PPGRSRP6
011200 01  RP6-INTERFACE.                                               PPGRSRP6
011300                                 COPY CPLNKRP6.                   PPGRSRP6
011400 01  GRS-INTERFACE.                                               PPGRSRP6
011500                                 COPY CPLNKGRS.                   PPGRSRP6
011600     SKIP1                                                        PPGRSRP6
011700     EJECT                                                        PPGRSRP6
011800***************************************************************   PPGRSRP6
011900***   P R O C E D U R E    D I V I S I O N                  ***   PPGRSRP6
012000***************************************************************   PPGRSRP6
012100*                                                                 PPGRSRP6
012200 PROCEDURE DIVISION USING RP6-INTERFACE                           PPGRSRP6
012300                          GRS-INTERFACE.                          PPGRSRP6
012310     EXEC SQL                                                     30871230
012320          INCLUDE CPPDXE99                                        30871230
012330     END-EXEC.                                                    30871230
012400     SKIP2                                                        PPGRSRP6
012500 100-MAINLINE   SECTION.                                          PPGRSRP6
012600     SKIP1                                                        PPGRSRP6
012700     IF FIRST-TIME                                                PPGRSRP6
012800         PERFORM 300-INITIALIZATION.                              PPGRSRP6
012900     SKIP1                                                        PPGRSRP6
013000     IF KRP6-PROGRAM-ACTION-CODE = 'CLOSE' AND NOT-FIRST-TIME     PPGRSRP6
013010*                                                              CD 30871230
013110         CLOSE REPORT9-FILE                                       28521087
013200     ELSE                                                         PPGRSRP6
013300         PERFORM 400-PRINT-MESSAGE THRU 499-PRINT-EXIT.           PPGRSRP6
013400     SKIP1                                                        PPGRSRP6
013500     GOBACK.                                                      PPGRSRP6
013600     SKIP2                                                        PPGRSRP6
013700***************************************************************   PPGRSRP6
013800***     I N I T I A L I Z A T I O N                         ***   PPGRSRP6
013900***************************************************************   PPGRSRP6
014000*                                                                 PPGRSRP6
014100 300-INITIALIZATION SECTION.                                      PPGRSRP6
014200     SKIP1                                                        PPGRSRP6
014300     SET NOT-FIRST-TIME TO TRUE.                                  PPGRSRP6
014400*                                                              CD 30871230
014710     OPEN OUTPUT REPORT9-FILE.                                    28521087
014800     MOVE WS-PGM-ID TO STND-PROG-ID.                              PPGRSRP6
014810*                                                              CD 30871230
014910     MOVE 'PPP3909' TO STND-RPT-ID.                               28521087
015000     MOVE KGRS-RUN-DATE TO STND-RPT-DATE.                         PPGRSRP6
019300**** MOVE KGRS-PAY-PD-MONTH TO STND-RPT-END-MONTH.                48761492
019400**** MOVE KGRS-PAY-PD-DAY TO STND-RPT-END-DAY.                    48761492
019500**** MOVE KGRS-PAY-PD-YEAR TO STND-RPT-END-YR.                    48761492
019600     MOVE KGRS-PRT-HDG-END-DT-STD (1:2)  TO STND-RPT-END-MONTH    48761492
019700     MOVE KGRS-PRT-HDG-END-DT-STD (3:2)  TO STND-RPT-END-DAY      48761492
019800     MOVE KGRS-PRT-HDG-END-DT-STD (5:2)  TO STND-RPT-END-YR       48761492
019900     MOVE KGRS-PRT-HDG-CYC-1  TO STND-RPT-CYCLE (1).              48761492
020000     MOVE KGRS-PRT-HDG-CYC-2  TO STND-RPT-CYCLE (2).              48761492
020100     MOVE KGRS-PRT-HDG-CYC-3  TO STND-RPT-CYCLE (3).              48761492
020200     MOVE SPACES              TO STND-RPT-CYCLE (4).              48761492
015400     MOVE 'HOURS ADJUSTMENT PRIOR TO LAST 12 MONTHS '             PPGRSRP6
015500                                               TO STND-RPT-TTL.   PPGRSRP6
015600     MOVE KGRS-CHECK-MONTH TO STND-DATE2-MONTH.                   PPGRSRP6
015700     MOVE KGRS-CHECK-DAY TO STND-DATE2-DAY.                       PPGRSRP6
015800     MOVE KGRS-CHECK-YEAR TO STND-DATE2-YEAR.                     PPGRSRP6
020800**** MOVE KGRS-CYCLE-TYPE (1) TO STND-RPT-CYCLE (1).              48761492
020900**** MOVE KGRS-CYCLE-TYPE (2) TO STND-RPT-CYCLE (2).              48761492
021000**** MOVE KGRS-CYCLE-TYPE (3) TO STND-RPT-CYCLE (3).              48761492
021100**** MOVE KGRS-CYCLE-TYPE (4) TO STND-RPT-CYCLE (4).              48761492
016300     SKIP2                                                        PPGRSRP6
016400***************************************************************   PPGRSRP6
016500***   PRINT MESSAGE LINES                                   ***   PPGRSRP6
016600***************************************************************   PPGRSRP6
016700*                                                                 PPGRSRP6
016800 400-PRINT-MESSAGE SECTION.                                       PPGRSRP6
016900     SKIP1                                                        PPGRSRP6
017000     SKIP1                                                        PPGRSRP6
017100     MOVE KGRS-EMPL-ID TO ML1-EMPL-ID.                            PPGRSRP6
017200     MOVE KGRS-EMPL-NAME TO ML1-EMPL-NAME.                        PPGRSRP6
017300     MOVE KGRS-EMPL-HOME-DEPT TO ML1-DEPT-NO.                     PPGRSRP6
017400*****PERFORM 500-GET-DEPT-NAME.                                   30871230
017500*****MOVE XHME-DEPT-NAME TO ML1-DEPT-NAME.                        30871230
017501     MOVE 'UNKNOWN DEPARTMENT'       TO ML1-DEPT-NAME.            30871230
017510     IF KGRS-EMPL-HOME-DEPT NOT = SPACES                          30871230
017540        MOVE KGRS-EMPL-HOME-DEPT     TO WS-DEPT-CODE              30871230
017550        PERFORM 900-SELECT-HME                                    30871230
017560        IF SQLCODE = ZERO                                         30871230
017570           MOVE HME-DEPT-NAME        TO ML1-DEPT-NAME             30871230
017591        END-IF                                                    30871230
017592     END-IF.                                                      30871230
017600     MOVE KRP6-PRIOR-MM TO ML1-MM.                                PPGRSRP6
017610     MOVE '/' TO ML1-SEP.                                         PPGRSRP6
017700     MOVE KRP6-PRIOR-YY TO ML1-YY.                                PPGRSRP6
017800     SKIP1                                                        PPGRSRP6
017900     PERFORM 700-WRITE-PRINT.                                     PPGRSRP6
018000     SKIP1                                                        PPGRSRP6
018100 499-PRINT-EXIT.                                                  PPGRSRP6
018200     EXIT.                                                        PPGRSRP6
018300     SKIP2                                                        PPGRSRP6
018400*                                                                 PPGRSRP6
018500*****GET-DEPT-NAME SECTION.                                       30871230
018600*****IF KGRS-EMPL-HOME-DEPT = SPACES                              30871230
018700*****    MOVE 'UNKNOWN DEPARTMENT'    TO                          30871230
018800*****            XHME-DEPT-NAME                                   30871230
018900*****ELSE                                                         30871230
019000*****    MOVE KGRS-EMPL-HOME-DEPT TO XCFK-DEPT-NO                 30871230
019100*****    MOVE XCFK-HOME-DPT TO IO-CTL-NOM-KEY                     30871230
019200*****    MOVE SEQ-READ TO IO-CTL-ACTION                           30871230
019300*****    CALL PPIOCTL USING CTL-INTERFACE                         30871230
019400*****            CTL-SEGMENT-TABLE                                30871230
019500*****    IF IO-CTL-ERROR-CODE = ZERO                              30871230
019600*****        CONTINUE                                             30871230
019700*****    ELSE                                                     30871230
019800*****        MOVE 'UNKNOWN DEPARTMENT'    TO XHME-DEPT-NAME       30871230
019900*****    END-IF                                                   30871230
020000*****END-IF.                                                      30871230
020100*****EJECT                                                        30871230
020200***************************************************************   PPGRSRP6
020300***   PRINT LINES AND HEADINGS AS NEEDED                    ***   PPGRSRP6
020400***************************************************************   PPGRSRP6
020500*                                                                 PPGRSRP6
020600 700-WRITE-PRINT SECTION.                                         PPGRSRP6
020700     SKIP1                                                        PPGRSRP6
020800     IF LINE-CNT > 55                                             PPGRSRP6
020900         PERFORM 800-PRINT-HEADINGS.                              PPGRSRP6
021000     SKIP1                                                        PPGRSRP6
021010*                                                              CD 30871230
021110     WRITE REPORT9-REC FROM PRT-LINE                              28521087
021200             AFTER ADVANCING PRT-SPACING.                         PPGRSRP6
021300     SKIP1                                                        PPGRSRP6
021400     ADD PRT-SPACING TO LINE-CNT.                                 PPGRSRP6
021500     MOVE 1 TO PRT-SPACING.                                       PPGRSRP6
021600     MOVE SPACES TO PRT-LINE.                                     PPGRSRP6
021700     SKIP2                                                        PPGRSRP6
021800 800-PRINT-HEADINGS SECTION.                                      PPGRSRP6
021900     SKIP1                                                        PPGRSRP6
022000     ADD 1 TO STND-PAGE-NO.                                       PPGRSRP6
022100     SKIP1                                                        PPGRSRP6
022110*                                                              CD 30871230
022510     WRITE REPORT9-REC FROM STND-RPT-HD1 AFTER ADVANCING NEW-PAGE.28521087
022520     WRITE REPORT9-REC FROM STND-RPT-HD2 AFTER ADVANCING 1.       28521087
022530     WRITE REPORT9-REC FROM STND-RPT-HD3 AFTER ADVANCING 1.       28521087
022540     WRITE REPORT9-REC FROM PRT-HD4 AFTER ADVANCING 2.            28521087
022600     SKIP1                                                        PPGRSRP6
022700     MOVE 6 TO LINE-CNT.                                          PPGRSRP6
022800     MOVE 2 TO PRT-SPACING.                                       PPGRSRP6
022900     SKIP1                                                        PPGRSRP6
023000 900-SELECT-HME SECTION.                                          30871230
023100     MOVE 'SELECT HME' TO DB2MSG-TAG.                             30871230
023200     EXEC SQL                                                     30871230
023300     SELECT HME_DEPT_NO                                           30871230
023400           ,HME_DEPT_NAME                                         30871230
023500     INTO  :HME-DEPT-NO                                           30871230
023600          ,:HME-DEPT-NAME                                         30871230
023700     FROM   PPPVZHME_HME                                          30871230
023800     WHERE  HME_DEPT_NO = :WS-DEPT-CODE                           30871230
023900     END-EXEC.                                                    30871230
024200*999999-SQL-ERROR.                                               *30871230
024300     COPY CPPDXP99.                                               30871230
024600     SET KGRS-PROGRAM-FATAL-ERROR TO TRUE.                        30871230
024800     GOBACK.                                                      30871230
