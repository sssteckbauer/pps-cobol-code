000100**************************************************************/   30871405
000200*  PROGRAM: PPCTC04                                          */   30871405
000300*  RELEASE: ___1405______ SERVICE REQUEST(S): _____3087____  */   30871405
000400*  NAME:___SRS___________ CREATION DATE:      ___MM/MM/YY__  */   30871405
000500*  DESCRIPTION:                                              */   30871405
000600*  CONTROL TABLE CON EDIT MODULE 04                          */   30871405
000700**************************************************************/   30871405
000800 IDENTIFICATION DIVISION.                                         PPCTC04
000900 PROGRAM-ID. PPCTC04.                                             PPCTC04
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTC04
001100 DATE-COMPILED.                                                   PPCTC04
001200 ENVIRONMENT DIVISION.                                            PPCTC04
001300 DATA DIVISION.                                                   PPCTC04
001400 WORKING-STORAGE SECTION.                                         PPCTC04
001500 01  MISC.                                                        PPCTC04
001600     05  WS-COUNT              PIC S9(08)  COMP SYNC VALUE ZERO.  PPCTC04
001700     05  M01940                PIC  X(05)  VALUE '01940'.         PPCTC04
001800     05  M01941                PIC  X(05)  VALUE '01941'.         PPCTC04
001900     05  M01945                PIC  X(05)  VALUE '01945'.         PPCTC04
002000     05  MCT005                PIC  X(05)  VALUE 'CT005'.         PPCTC04
002100     05  WS-TUC                PIC  XX.                           PPCTC04
002200     05  WS-SHC                PIC  X.                            PPCTC04
002300                                                                  PPCTC04
002400 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTC04
002500 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTC04
002600 01  PPTCTUTL-INTERFACE EXTERNAL.               COPY CPLNKTCL.    PPCTC04
002700 01  CTRI-CTL-REPORT-INTERFACE.                 COPY CPLNCTRI.    PPCTC04
002800                                                                  PPCTC04
002900 01  BUG-TABLE-DATA.                                              PPCTC04
003000     EXEC SQL                                                     PPCTC04
003100        INCLUDE PPPVZBUG                                          PPCTC04
003200     END-EXEC.                                                    PPCTC04
003300                                                                  PPCTC04
003400 01  BUB-TABLE-DATA.                                              PPCTC04
003500     EXEC SQL                                                     PPCTC04
003600        INCLUDE PPPVZBUB                                          PPCTC04
003700     END-EXEC.                                                    PPCTC04
003800                                                                  PPCTC04
003900     EXEC SQL                                                     PPCTC04
004000        INCLUDE SQLCA                                             PPCTC04
004100     END-EXEC.                                                    PPCTC04
004200                                                                  PPCTC04
004300 LINKAGE SECTION.                                                 PPCTC04
004400                                                                  PPCTC04
004500 01  CTCI-CTL-TBL-CONEDIT-INTERFACE.            COPY CPLNCTCI.    PPCTC04
004600                                                                  PPCTC04
004700 PROCEDURE DIVISION USING CTCI-CTL-TBL-CONEDIT-INTERFACE.         PPCTC04
004800                                                                  PPCTC04
004900 0000-MAIN SECTION.                                               PPCTC04
005000                                                                  PPCTC04
005100     EXEC SQL                                                     PPCTC04
005200        INCLUDE CPPDXE99                                          PPCTC04
005300     END-EXEC.                                                    PPCTC04
005400                                                                  PPCTC04
005500     MOVE 'PPCTC04' TO XWHC-DISPLAY-MODULE.                       PPCTC04
005600     COPY CPPDXWHC.                                               PPCTC04
005700                                                                  PPCTC04
005800     SET CTCI-NORMAL TO TRUE.                                     PPCTC04
005900     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTC04
006000     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTC04
006100                                   CTRI-TRANSACTION               PPCTC04
006200                                   CTRI-TRANSACTION-DISPO         PPCTC04
006300                                   CTRI-TEXT-LINE                 PPCTC04
006400                                   CTRI-MESSAGE-NUMBER.           PPCTC04
006500     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTC04
006600                                                                  PPCTC04
006700     PERFORM 1000-EDIT.                                           PPCTC04
006800                                                                  PPCTC04
006900 0100-END.                                                        PPCTC04
007000                                                                  PPCTC04
007100     GOBACK.                                                      PPCTC04
007200                                                                  PPCTC04
007300 1000-EDIT SECTION.                                               PPCTC04
007400                                                                  PPCTC04
007500     SET XTCL-READ-ALL-TITLES-FIRST TO TRUE.                      PPCTC04
007600     PERFORM UNTIL XTCL-STATUS-NORMAL-END-ALL                     PPCTC04
007700                OR XTCL-STATUS-ERROR-DB2                          PPCTC04
007800                OR XTCL-STATUS-ERROR-EVENT                        PPCTC04
007900      CALL 'PPTCTUTL'                                             PPCTC04
008000      IF XTCL-STATUS-NORMAL-END                                   PPCTC04
008100        IF  XTCL-TITLE-UNIT-CODE        NOT = SPACE               PPCTC04
008200        AND XTCL-TITLE-SPECIAL-HANDLING NOT = SPACE               PPCTC04
008300            MOVE XTCL-TITLE-UNIT-CODE        TO WS-TUC            PPCTC04
008400            MOVE XTCL-TITLE-SPECIAL-HANDLING TO WS-SHC            PPCTC04
008500            PERFORM 9000-COUNT-BUG                                PPCTC04
008600            IF WS-COUNT = ZERO                                    PPCTC04
008700               STRING 'TUC/SHC: ' XTCL-TITLE-CODE                 PPCTC04
008800               XTCL-TITLE-UNIT-CODE                               PPCTC04
008900               XTCL-TITLE-SPECIAL-HANDLING                        PPCTC04
009000               DELIMITED BY SIZE INTO CTRI-TEXT-LINE              PPCTC04
009100               MOVE M01940 TO CTRI-MESSAGE-NUMBER                 PPCTC04
009200               PERFORM 3100-PRINT-ERROR                           PPCTC04
009300            END-IF                                                PPCTC04
009400            PERFORM 9100-COUNT-BUB                                PPCTC04
009500            IF WS-COUNT = ZERO                                    PPCTC04
009600               STRING 'TUC/SHC: ' XTCL-TITLE-CODE                 PPCTC04
009700               XTCL-TITLE-UNIT-CODE                               PPCTC04
009800               XTCL-TITLE-SPECIAL-HANDLING                        PPCTC04
009900               DELIMITED BY SIZE INTO CTRI-TEXT-LINE              PPCTC04
010000               MOVE M01941 TO CTRI-MESSAGE-NUMBER                 PPCTC04
010100               PERFORM 3100-PRINT-ERROR                           PPCTC04
010200            END-IF                                                PPCTC04
010300        END-IF                                                    PPCTC04
010400      END-IF                                                      PPCTC04
010500     END-PERFORM.                                                 PPCTC04
010600     IF XTCL-STATUS-ERROR-DB2                                     PPCTC04
010700        MOVE XTCL-DB2MSG-MESSNO TO CTRI-MESSAGE-NUMBER            PPCTC04
010800        PERFORM 3100-PRINT-ERROR                                  PPCTC04
010900     ELSE                                                         PPCTC04
011000       IF XTCL-STATUS-ERROR-EVENT                                 PPCTC04
011100          MOVE M01945 TO CTRI-MESSAGE-NUMBER                      PPCTC04
011200          MOVE XTCL-STATUS-CODE TO CTRI-TEXT-LINE                 PPCTC04
011300          PERFORM 3100-PRINT-ERROR                                PPCTC04
011400     END-IF.                                                      PPCTC04
011500                                                                  PPCTC04
011600 1099-EXIT.                                                       PPCTC04
011700     EXIT.                                                        PPCTC04
011800                                                                  PPCTC04
011900 3100-PRINT-ERROR SECTION.                                        PPCTC04
012000                                                                  PPCTC04
012100     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTC04
012200     IF CTRI-RUN-ABORTED                                          PPCTC04
012300        SET CTCI-ABORT TO TRUE                                    PPCTC04
012400        GO TO 0100-END                                            PPCTC04
012500     END-IF.                                                      PPCTC04
012600                                                                  PPCTC04
012700 3199-EXIT.                                                       PPCTC04
012800     EXIT.                                                        PPCTC04
012900                                                                  PPCTC04
013000 9000-COUNT-BUG SECTION.                                          PPCTC04
013100                                                                  PPCTC04
013200     MOVE '9000 COUNT BUG ' TO DB2MSG-TAG.                        PPCTC04
013300     EXEC SQL                                                     PPCTC04
013400       SELECT  COUNT(*)                                           PPCTC04
013500       INTO    :WS-COUNT                                          PPCTC04
013600       FROM    PPPVZBUG_BUG                                       PPCTC04
013700       WHERE   BUG_BUC         = :WS-TUC                          PPCTC04
013800         AND   BUG_SHC         = :WS-SHC                          PPCTC04
013900     END-EXEC.                                                    PPCTC04
014000                                                                  PPCTC04
014100 9099-EXIT.                                                       PPCTC04
014200     EXIT.                                                        PPCTC04
014300                                                                  PPCTC04
014400 9100-COUNT-BUB SECTION.                                          PPCTC04
014500                                                                  PPCTC04
014600     MOVE '9100 COUNT BUB ' TO DB2MSG-TAG.                        PPCTC04
014700     EXEC SQL                                                     PPCTC04
014800       SELECT  COUNT(*)                                           PPCTC04
014900       INTO    :WS-COUNT                                          PPCTC04
015000       FROM    PPPVZBUB_BUB                                       PPCTC04
015100       WHERE   BUB_BUC         = :WS-TUC                          PPCTC04
015200         AND   BUB_SHC         = :WS-SHC                          PPCTC04
015300     END-EXEC.                                                    PPCTC04
015400                                                                  PPCTC04
015500 9199-EXIT.                                                       PPCTC04
015600     EXIT.                                                        PPCTC04
015700                                                                  PPCTC04
015800*999999-SQL-ERROR.                                                PPCTC04
015900     EXEC SQL                                                     PPCTC04
016000        INCLUDE CPPDXP99                                          PPCTC04
016100     END-EXEC.                                                    PPCTC04
016200     MOVE MCT005 TO CTRI-MESSAGE-NUMBER                           PPCTC04
016300     PERFORM 3100-PRINT-ERROR                                     PPCTC04
016400     SET CTCI-ABORT TO TRUE.                                      PPCTC04
016500     GO TO 0100-END.                                              PPCTC04
