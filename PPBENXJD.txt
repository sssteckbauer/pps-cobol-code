000001**************************************************************/   12570722
000002*  PROGRAM: PPBENXJD                                         */   12570722
000003*  RELEASE: ___0722______ SERVICE REQUEST(S): ____11257____  */   12570722
000004*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___12/08/92__  */   12570722
000005*  DESCRIPTION:                                              */   12570722
000006*    ONLY U AND UUU COVERAGE CODES VALID. DEFAULT UU TO UUU  */   12570722
000007*                                                            */   12570722
000008**************************************************************/   12570722
000010**************************************************************/   05390718
000020*  PROGRAM: PPBENXJD                                         */   05390718
000030*  RELEASE: ___0718______ SERVICE REQUEST(S): ____10539____  */   05390718
000031*  REFERENCE RELEASE: ___651___                              */   05390718
000040*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___11/23/92__  */   05390718
000050*  DESCRIPTION:                                              */   05390718
000060*  - MODIFIED CODE TO ACCOMMODATE THE NEW RULE WHEN TO TAKE  */   05390718
000070*    INSURANCE DEDUCTIONS.                                   */   05390718
000080**************************************************************/   05390718
000100**************************************************************/   36220651
000200*  PROGRAM:  PPBENXJD                                        */   36220651
000300*  RELEASE # __0651____   SERVICE REQUEST NO(S) __3622______ */   36220651
000400*  NAME _______PXP_____   MODIFICATION DATE ____04/13/92_____*/   36220651
000500*  DESCRIPTION                                               */   36220651
000600*  - IMPLEMENT COVERAGE EFFECTIVE DATE SUPPRESSION OF        */   36220651
000700*    DEDUCTIONS.                                             */   36220651
000800**************************************************************/   36220651
000100**************************************************************/   14420448
000200*  PROGRAM:  PPBENXJD                                        */   14420448
000300*  RELEASE # ___0448___   SERVICE REQUEST NO(S) __1442______ */   14420448
000400*  NAME ___M. SANO_____   MODIFICATION DATE ____02/07/90_____*/   14420448
000500*  DESCRIPTION                                               */   14420448
000600*      - CHANGE PROCESSING TO LOOK UP RATES IN BRT DIRECTLY, */   14420448
000700*        WITHOUT REFERENCE TO THE BUT, USING THE BRSC AS     */   14420448
000800*        PART OF THE BRT KEY                                 */   14420448
000900**************************************************************/   14420448
000100**************************************************************/   30930413
000200*  PROGRAM: PPBENXJD                                         */   30930413
000300*  RELEASE: ____0413____  SERVICE REQUEST(S): ____3093____   */   30930413
000400*  NAME:____JIM WILLIAMS__CREATION DATE:      __05/23/89__   */   30930413
000500*  DESCRIPTION:                                              */   30930413
000600*  - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II. */   30930413
000700**************************************************************/   30930413
000800**************************************************************/   14240327
000900*  PROGRAM:  PPBENXJD                                        */   14240327
001000*  REL#: 327     REF: NONE  SERVICE REQUESTS:   ____1492____ */   14240327
001100*  NAME ___S.ISAACS____   MODIFICATION DATE ____11/17/87_____*/   14240327
001200*  DESCRIPTION                                               */   14240327
001300*                                                            */   14240327
001400*  NEW PROGRAM TO RETREIVE GROUP LEGAL PLAN BENEFITS RATES   */   14240327
001500*  AND DETERMINE GROUP LEGAL PLAN PREMIUMS.                  */   14240327
001600*                                                            */   14240327
001700**************************************************************/   14240327
001800 IDENTIFICATION DIVISION.                                         PPBENXJD
001900 PROGRAM-ID. PPBENXJD.                                            PPBENXJD
003810*****                                                          CD 05390718
002200 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENXJD
002300 AUTHOR. S.ISAACS                                                 PPBENXJD
002400 DATE-WRITTEN.                                                    PPBENXJD
002500 DATE-COMPILED.                                                   PPBENXJD
002600*REMARKS.                                                         30930413
002700******************************************************************PPBENXJD
002800*                                                                *PPBENXJD
002900*  THIS MODULE PERFORMS GROUP LEGAL PLAN PREMIUM CALCULATIONS.   *PPBENXJD
003000* IN ADDITION IT RETRIEVES GROUP LEGAL PLAN RATES FROM THE       *PPBENXJD
003100* BENEFITS RATES TABLE BY CALLING PROGRAMS.                      *PPBENXJD
003200*                                                                *PPBENXJD
003300*                                                                *PPBENXJD
003400*                                                                *PPBENXJD
003500******************************************************************PPBENXJD
003600 ENVIRONMENT DIVISION.                                            PPBENXJD
003700 CONFIGURATION SECTION.                                           PPBENXJD
003800 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
003900 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
004000 SPECIAL-NAMES.                                                   PPBENXJD
004100 INPUT-OUTPUT SECTION.                                            PPBENXJD
004200 FILE-CONTROL.                                                    PPBENXJD
004300     SKIP3                                                        PPBENXJD
004400******************************************************************PPBENXJD
004500*                                                                *PPBENXJD
004600*                         DATA DIVISION                          *PPBENXJD
004700*                                                                *PPBENXJD
004800******************************************************************PPBENXJD
004900 DATA DIVISION.                                                   PPBENXJD
005000 FILE SECTION.                                                    PPBENXJD
005100     SKIP1                                                        PPBENXJD
005200     EJECT                                                        PPBENXJD
005300******************************************************************PPBENXJD
005400*                                                                *PPBENXJD
005500*                    WORKING-STORAGE SECTION                     *PPBENXJD
005600*                                                                *PPBENXJD
005700******************************************************************PPBENXJD
005800 WORKING-STORAGE SECTION.                                         PPBENXJD
005900     SKIP1                                                        PPBENXJD
006000 77  WS-ID                        PIC X(27)                       PPBENXJD
006100              VALUE 'WORKING-STORAGE BEGINS HERE'.                PPBENXJD
006200     SKIP3                                                        PPBENXJD
008100 01  DED-EFF-DATE-COMMAREA EXTERNAL.  COPY CPWSEFDT.              36220651
006300 01  COVERAGE-SUBSCRIPT              PIC 9(01).                   PPBENXJD
006400*    *******************************************                  PPBENXJD
006500*    * USED TO GET PREMIUM AMOUNT BY PLAN CODE                    PPBENXJD
006600*    *******************************************                  PPBENXJD
006700 01  FLAGS-AND-SWITCHES.                                          PPBENXJD
006800     05  PROGRAM-STATUS-FLAG         PIC X(01)   VALUE '0'.       PPBENXJD
006900         88  PROGRAM-STATUS-NORMAL               VALUE '0'.       PPBENXJD
007000         88  PROGRAM-STATUS-EXITING              VALUE '1'.       PPBENXJD
007100     05  FOUND-RATE-SW               PIC X(01)   VALUE 'N'.       PPBENXJD
007200         88  FOUND-RATE                          VALUE 'Y'.       PPBENXJD
007300                                                                  PPBENXJD
007400 01  CONSTANT-VALUES.                                             PPBENXJD
007500*    *************************************************************PPBENXJD
007600*    * THE FOLLOWING SHOULD BE THE SAME VALUE AS FOR THE OCCURS  *PPBENXJD
007700*    *************************************************************PPBENXJD
007800     05  CONSTANT-BENEFIT-TYPE       PIC X(01)   VALUE 'J'.       PPBENXJD
007900     05  CONSTANT-NORMAL             PIC X(01)   VALUE '0'.       PPBENXJD
008000     05  CONSTANT-ABORT              PIC X(01)   VALUE '1'.       PPBENXJD
008100     05  CONSTANT-EXIT-PROGRAM       PIC X(01)   VALUE '1'.       PPBENXJD
008200******************************************************************PPBENXJD
008300*                USED FOR DATE CALCS                             *PPBENXJD
008400******************************************************************PPBENXJD
008500     05  WS-PPEND-DATE-HOLD       PIC 9(6).                       PPBENXJD
008600     05  WS-PPEND-AREA  REDEFINES                                 PPBENXJD
008700         WS-PPEND-DATE-HOLD.                                      PPBENXJD
008800         10  WS-PPEND-YYMM        PIC 9(4).                       PPBENXJD
008900         10  FILLER               PIC XX.                         PPBENXJD
009000*                                                                *PPBENXJD
009100     05  WS-EFFECT-DATE-HOLD      PIC 9(6).                       PPBENXJD
009200     05  WS-EFFECT-AREA REDEFINES                                 PPBENXJD
009300         WS-EFFECT-DATE-HOLD.                                     PPBENXJD
009400         10  WS-EFFECT-YYMM       PIC 9(4).                       PPBENXJD
009500         10  FILLER               PIC XX.                         PPBENXJD
009600*                                                                *PPBENXJD
009700     EJECT                                                        PPBENXJD
009800 01  XUTF-UTILITY-FUNCTIONS.                                      30930413
009900                                  COPY 'CPWSXUTF'.                PPBENXJD
010900 01  WS-BRT-KEY.                                                  14420448
011000         10  WS-BRT-KEY-CONST    PIC X(04) VALUE ' BRT'.          14420448
011100         10  WS-BRT-KEY-VALUES.                                   14420448
011200             15  WS-BRT-BRSC      PIC X(05).                      14420448
011300             15  WS-BRT-BEN-TYPE  PIC X.                          14420448
011400             15  WS-BRT-BEN-PLAN  PIC X(02).                      14420448
011500             15  WS-BRT-SEQ-NO REDEFINES WS-BRT-BEN-PLAN PIC 99.  14420448
011600 01  WS-DEFAULT-BRSC          PIC X(05) VALUE '00   '.            14420448
011700 01  WS-DEFAULT-BRT           PIC X(05) VALUE '<<   '.            14420448
010000     EJECT                                                        PPBENXJD
010100 01  XBRT-BENEFITS-RATES-RECORD.                                  30930413
010200                                  COPY 'CPWSXBRT'.                PPBENXJD
010300     EJECT                                                        PPBENXJD
010400******************************************************************PPBENXJD
010500*                                                                *PPBENXJD
010600*                        LINKAGE SECTION                         *PPBENXJD
010700*                                                                *PPBENXJD
010800******************************************************************PPBENXJD
010900 LINKAGE SECTION.                                                 PPBENXJD
011000                                                                  PPBENXJD
011100 01  PPBENXJD-INTERFACE.          COPY 'CPLNKXJD'.                PPBENXJD
011200     EJECT                                                        PPBENXJD
014210*****                                                          CD 05390718
011500     EJECT                                                        PPBENXJD
011600 01  CTL-INTERFACE.                                               30930413
011700                                  COPY 'CPWSXCIF'.                PPBENXJD
011800     SKIP3                                                        PPBENXJD
011900 01  CTL-SEGMENT-TABLE.                                           30930413
012000                                  COPY 'CPWSXCST'.                PPBENXJD
012100     EJECT                                                        PPBENXJD
012200******************************************************************PPBENXJD
012300*                                                                *PPBENXJD
012400*                       PROCEDURE DIVISION                       *PPBENXJD
012500*                                                                *PPBENXJD
012600******************************************************************PPBENXJD
012700     SKIP1                                                        PPBENXJD
012800 PROCEDURE DIVISION USING PPBENXJD-INTERFACE                      PPBENXJD
015710*****                                                          CD 05390718
013000                          CTL-INTERFACE                           PPBENXJD
013100                          CTL-SEGMENT-TABLE.                      PPBENXJD
013200     SKIP1                                                        PPBENXJD
013300 A000-MAINLINE SECTION.                                           PPBENXJD
013400     SKIP2                                                        PPBENXJD
013500     MOVE CONSTANT-NORMAL TO PROGRAM-STATUS-FLAG.                 PPBENXJD
013600     SKIP1                                                        PPBENXJD
013700     PERFORM B010-INITIALIZE THRU B019-INITIALIZE-EXIT.           PPBENXJD
013800     SKIP1                                                        PPBENXJD
013900     IF PROGRAM-STATUS-NORMAL                                     PPBENXJD
014000        IF KXJD-ACTION-GETTING-RATE OR KXJD-ACTION-CALCULATING    PPBENXJD
014100            PERFORM C010-GET-RATE THRU C019-GET-RATE-EXIT         PPBENXJD
014200            IF PROGRAM-STATUS-NORMAL                              PPBENXJD
014300                IF KXJD-ACTION-CALCULATING AND FOUND-RATE AND     PPBENXJD
014400                   KXJD-ENROLLED-IN-JD                            PPBENXJD
014500                    PERFORM D010-CALCULATE                        PPBENXJD
014600                        THRU D019-CALCULATE-EXIT.                 PPBENXJD
014700     SKIP3                                                        PPBENXJD
014800 A009-GOBACK.                                                     PPBENXJD
014900     GOBACK.                                                      PPBENXJD
015000     EJECT                                                        PPBENXJD
015100 B000-INITIALIZATION SECTION.                                     PPBENXJD
015200     SKIP2                                                        PPBENXJD
015300 B010-INITIALIZE.                                                 PPBENXJD
015400     SKIP1                                                        PPBENXJD
015500     MOVE CONSTANT-NORMAL TO KXJD-RETURN-STATUS-FLAG.             PPBENXJD
015600*                                                                 PPBENXJD
015700******************************************************************PPBENXJD
015800*      SET NUMERIC RETURN FIELDS TO ZERO                         *PPBENXJD
015900******************************************************************PPBENXJD
016000*                                                                 PPBENXJD
016100     MOVE ZERO TO KXJD-MAX-CONTRIBUTION                           PPBENXJD
016200                  KXJD-PREMIUM-AMT                                PPBENXJD
016300                  KXJD-EMPLOYEE-DEDUCTION                         PPBENXJD
016400                  KXJD-EMPLOYER-CONTRIBUTION.                     PPBENXJD
016500                                                                  PPBENXJD
016600     IF KXJD-ACTION-GETTING-RATE OR KXJD-ACTION-CALCULATING       PPBENXJD
016700         PERFORM B030-VALIDATE-LOOKUP-ARGS                        PPBENXJD
016800             THRU B039-VALIDATE-LOOKUP-ARGS-EXIT.                 PPBENXJD
016900                                                                  PPBENXJD
017000     IF KXJD-ACTION-CALCULATING                                   PPBENXJD
017100         PERFORM B050-VALIDATE-ENROLLMENT                         PPBENXJD
017200             THRU B059-VALIDATE-ENROLLMENT-EXIT.                  PPBENXJD
017300     SKIP3                                                        PPBENXJD
017400 B019-INITIALIZE-EXIT.                                            PPBENXJD
017500     EXIT.                                                        PPBENXJD
017600     SKIP3                                                        PPBENXJD
017700                                                                  PPBENXJD
017800 B030-VALIDATE-LOOKUP-ARGS.                                       PPBENXJD
017900     SKIP1                                                        PPBENXJD
018000     MOVE '0' TO KXJD-INVALID-LOOKUP-ARG-FLAG.                    PPBENXJD
020810     IF KXJD-COVERAGE-CODE = 'UU'                                 12570722
020820        MOVE 'UUU' TO KXJD-COVERAGE-CODE                          12570722
020830     END-IF.                                                      12570722
018100                                                                  PPBENXJD
018200     IF KXJD-PLAN-CODE = SPACE                                    PPBENXJD
018300         MOVE '1' TO KXJD-INVALID-LOOKUP-ARG-FLAG                 PPBENXJD
018400     ELSE IF NOT KXJD-VALID-COVERAGE-CODE                         PPBENXJD
018500         MOVE '1' TO KXJD-INVALID-LOOKUP-ARG-FLAG.                PPBENXJD
018600     SKIP1                                                        PPBENXJD
018700     IF KXJD-INVALID-LOOKUP-ARG                                   PPBENXJD
018800         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENXJD
018900     SKIP1                                                        PPBENXJD
019000     MOVE '1' TO KXJD-NOT-ENROLLED-IN-JD-FLAG.                    PPBENXJD
019100     SKIP3                                                        PPBENXJD
019200 B039-VALIDATE-LOOKUP-ARGS-EXIT.                                  PPBENXJD
019300     EXIT.                                                        PPBENXJD
019400     SKIP3                                                        PPBENXJD
019500                                                                  PPBENXJD
019600 B050-VALIDATE-ENROLLMENT.                                        PPBENXJD
019700     SKIP1                                                        PPBENXJD
019800     MOVE '0' TO KXJD-NOT-ENROLLED-IN-JD-FLAG.                    PPBENXJD
022710*****                                                          CD 05390718
022800*****MOVE KXJD-COVERAGE-EFFECT-DATE  TO WS-EFFECT-DATE-HOLD.      05390718
022900*****MOVE KXJD-PAY-CYCLE-END-DATE    TO WS-PPEND-DATE-HOLD.       05390718
020100     IF KXJD-INVALID-LOOKUP-ARG                                   PPBENXJD
020200         MOVE '1' TO KXJD-NOT-ENROLLED-IN-JD-FLAG                 PPBENXJD
023200     END-IF                                                       36220651
023910*****                                                          CD 05390718
024000     SKIP2                                                        36220651
024700******************************************************************36220651
024800*  IF THE USER HAS FAILED TO ASSIGN AN INDICATOR IN THE GTN      *36220651
024900*  TABLE, USE 'E' AS A DEFAULT.                                  *36220651
025000******************************************************************36220651
025100     IF EFDT-INPUT-XGTN-ICED-IND = 'A' OR 'E' OR 'V' OR 'Z'       36220651
025200         CONTINUE                                                 36220651
025300     ELSE                                                         36220651
025400        MOVE 'E' TO EFDT-INPUT-XGTN-ICED-IND                      36220651
025500     END-IF.                                                      36220651
025600*  THIS CODE CAN BYPASS NORMAL DEDUCTIONS BASED ON DEDUCTION     *36220651
025700*  EFFECTIVE DATE CONSIDERATIONS.                                *36220651
025800*****MOVE WS-EFFECT-AREA(1:2) TO EFDT-INPUT-EFF-YY.               05390718
025900*****MOVE WS-EFFECT-AREA(3:2) TO EFDT-INPUT-EFF-MM.               05390718
026000*****MOVE WS-PPEND-AREA(1:2)   TO EFDT-INPUT-END-YEAR.            05390718
026100*****MOVE WS-PPEND-AREA(3:2)  TO EFDT-INPUT-END-MONTH.            05390718
026110     MOVE KXJD-COVERAGE-EFFECT-DATE(1:2) TO EFDT-INPUT-EFF-YY.    05390718
026111     MOVE KXJD-COVERAGE-EFFECT-DATE(3:2) TO EFDT-INPUT-EFF-MM.    05390718
026112     MOVE KXJD-COVERAGE-EFFECT-DATE(5:2) TO EFDT-INPUT-EFF-DD.    05390718
026120     MOVE KXJD-PAY-CYCLE-END-DATE(1:2) TO EFDT-XPCR-YY.           05390718
026130     MOVE KXJD-PAY-CYCLE-END-DATE(3:2) TO EFDT-XPCR-MM.           05390718
026140     MOVE KXJD-PAY-CYCLE-END-DATE(5:2) TO EFDT-XPCR-DD.           05390718
026200     PERFORM Z01-PROCESS-EFF-DATE.                                36220651
026300     IF EFDT-DONT-TAKE-DEDUCTION                                  36220651
026400         MOVE '1' TO KXJD-NOT-ENROLLED-IN-JD-FLAG                 36220651
026500     END-IF.                                                      36220651
021000     SKIP1                                                        PPBENXJD
021100     IF KXJD-NOT-ENROLLED-IN-JD                                   PPBENXJD
021200         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENXJD
021300     SKIP3                                                        PPBENXJD
021400 B059-VALIDATE-ENROLLMENT-EXIT.                                   PPBENXJD
021500     EXIT.                                                        PPBENXJD
021600     EJECT                                                        PPBENXJD
021700 C000-RATE-RETRIEVAL SECTION.                                     PPBENXJD
021800     SKIP2                                                        PPBENXJD
021900 C010-GET-RATE.                                                   PPBENXJD
022000     SKIP1                                                        PPBENXJD
027910*****                                                          CD 05390718
024200     IF KXJD-BRSC  = WS-DEFAULT-BRSC                              14420448
024300        MOVE WS-DEFAULT-BRT TO WS-BRT-BRSC                        14420448
024400     ELSE                                                         14420448
024500        MOVE KXJD-BRSC  TO WS-BRT-BRSC.                           14420448
024600     MOVE CONSTANT-BENEFIT-TYPE TO WS-BRT-BEN-TYPE.               14420448
024700     MOVE KXJD-PLAN-CODE        TO WS-BRT-BEN-PLAN.               14420448
022400     SKIP1                                                        PPBENXJD
022500     MOVE 'N' TO FOUND-RATE-SW.                                   PPBENXJD
022600     SKIP1                                                        PPBENXJD
022700     PERFORM P010-BRT-LOOKUP-RATE-LOOP                            PPBENXJD
022800        THRU P019-BRT-LOOKUP-RATE-LOOP-EXIT                       PPBENXJD
022900         UNTIL FOUND-RATE OR                                      PPBENXJD
023000******************************************************************PPBENXJD
023100*                BLANK KEY INDICATES NOT ELIGIBLE FOR BENEFITS   *PPBENXJD
023200******************************************************************PPBENXJD
029510*****                                                          CD 05390718
023400               NOT PROGRAM-STATUS-NORMAL.                         PPBENXJD
023500                                                                  PPBENXJD
023600     IF FOUND-RATE                                                PPBENXJD
023700         PERFORM C030-GET-PREM-AND-CONTRIB THRU                   PPBENXJD
023800                 C039-GET-PREM-AND-CONTRIB-EXIT.                  PPBENXJD
023900     SKIP3                                                        PPBENXJD
024000 C019-GET-RATE-EXIT.                                              PPBENXJD
024100     EXIT.                                                        PPBENXJD
024200     SKIP3                                                        PPBENXJD
024300 C030-GET-PREM-AND-CONTRIB.                                       PPBENXJD
024400     SKIP1                                                        PPBENXJD
024500     PERFORM C050-GET-PREMIUM-LOOP THRU                           PPBENXJD
024600             C059-GET-PREMIUM-LOOP-EXIT                           PPBENXJD
024700         VARYING COVERAGE-SUBSCRIPT FROM 1 BY 1                   PPBENXJD
031010*****        UNTIL COVERAGE-SUBSCRIPT > 3.                        12570722
031020             UNTIL COVERAGE-SUBSCRIPT > 2.                        12570722
024900                                                                  PPBENXJD
025000     PERFORM C070-GET-CONTRIBUTION THRU                           PPBENXJD
025100             C079-GET-CONTRIBUTION-EXIT.                          PPBENXJD
025200     SKIP3                                                        PPBENXJD
025300 C039-GET-PREM-AND-CONTRIB-EXIT.                                  PPBENXJD
025400     EXIT.                                                        PPBENXJD
025500     SKIP3                                                        PPBENXJD
025600 C050-GET-PREMIUM-LOOP.                                           PPBENXJD
025700     SKIP1                                                        PPBENXJD
025800     IF XBRT-JD-CODE (COVERAGE-SUBSCRIPT) = KXJD-COVERAGE-CODE    PPBENXJD
025900         MOVE XBRT-JD-RATE (COVERAGE-SUBSCRIPT) TO                PPBENXJD
026000              KXJD-PREMIUM-AMT.                                   PPBENXJD
026100     SKIP3                                                        PPBENXJD
026200 C059-GET-PREMIUM-LOOP-EXIT.                                      PPBENXJD
026300     EXIT.                                                        PPBENXJD
026400     SKIP3                                                        PPBENXJD
026500 C070-GET-CONTRIBUTION.                                           PPBENXJD
026600     SKIP1                                                        PPBENXJD
026700     IF KXJD-COVERAGE-CODE = 'U'                                  PPBENXJD
026800         MOVE XBRT-JD-UC-SINGLE TO KXJD-MAX-CONTRIBUTION          PPBENXJD
033210*****ELSE IF KXJD-COVERAGE-CODE = 'UU'                            12570722
033220*****    MOVE XBRT-JD-UC-TWO-PARTY TO KXJD-MAX-CONTRIBUTION       12570722
027100     ELSE                                                         PPBENXJD
027200         MOVE XBRT-JD-UC-FAMILY TO KXJD-MAX-CONTRIBUTION.         PPBENXJD
027300     SKIP3                                                        PPBENXJD
027400 C079-GET-CONTRIBUTION-EXIT.                                      PPBENXJD
027500     EXIT.                                                        PPBENXJD
027600     EJECT                                                        PPBENXJD
027700 D000-LEGAL-PLAN-CALC SECTION.                                    PPBENXJD
027800     SKIP2                                                        PPBENXJD
027900 D010-CALCULATE.                                                  PPBENXJD
028000     SKIP1                                                        PPBENXJD
028100     IF KXJD-PREMIUM-AMT < KXJD-MAX-CONTRIBUTION                  PPBENXJD
028200         COMPUTE KXJD-EMPLOYER-CONTRIBUTION =                     PPBENXJD
028300                 KXJD-PREMIUM-AMT                                 PPBENXJD
028400         COMPUTE KXJD-EMPLOYEE-DEDUCTION = ZERO                   PPBENXJD
028500     ELSE                                                         PPBENXJD
028600         COMPUTE KXJD-EMPLOYER-CONTRIBUTION =                     PPBENXJD
028700                 KXJD-MAX-CONTRIBUTION                            PPBENXJD
028800         COMPUTE KXJD-EMPLOYEE-DEDUCTION =                        PPBENXJD
028900                 KXJD-PREMIUM-AMT - KXJD-EMPLOYER-CONTRIBUTION.   PPBENXJD
029000     SKIP3                                                        PPBENXJD
029100 D019-CALCULATE-EXIT.                                             PPBENXJD
029200     EXIT.                                                        PPBENXJD
029300     EJECT                                                        PPBENXJD
029400 P000-SET-UP-BRT-KEY SECTION.                                     PPBENXJD
029500     SKIP2                                                        PPBENXJD
029600 P010-BRT-LOOKUP-RATE-LOOP.                                       PPBENXJD
029700     SKIP1                                                        PPBENXJD
029800     MOVE 'N' TO FOUND-RATE-SW.                                   PPBENXJD
029900                                                                  PPBENXJD
030000******************************************************************PPBENXJD
030100*      TO AVOID UNNECESSARY DISK ACCESS, CHECK PREVIOUS BEFORE   *PPBENXJD
030200*      DOING LOOKUP PHYSICALLY IN TABLE                          *PPBENXJD
030300******************************************************************PPBENXJD
030400                                                                  PPBENXJD
036710*****                                                          CD 05390718
033000     IF XBRT-KEY = WS-BRT-KEY                                     14420448
030600         MOVE 'Y' TO FOUND-RATE-SW                                PPBENXJD
030700     ELSE                                                         PPBENXJD
030800*        ************************************                     PPBENXJD
030900*        * RETRIEVE RATE FROM TABLE USING KEY                     PPBENXJD
031000*        ************************************                     PPBENXJD
031100         PERFORM P050-ACCESS-CTL THRU P059-ACCESS-CTL-EXIT.       PPBENXJD
031200     SKIP3                                                        PPBENXJD
031300 P019-BRT-LOOKUP-RATE-LOOP-EXIT.                                  PPBENXJD
031400     EXIT.                                                        PPBENXJD
031500     SKIP3                                                        PPBENXJD
039610*****                                                          CD 05390718
039700*****SKIP3                                                        05390718
033500 P050-ACCESS-CTL.                                                 PPBENXJD
033600     SKIP1                                                        PPBENXJD
040110*****                                                          CD 05390718
036400     MOVE WS-BRT-KEY       TO CTL-SEG-TAB-KEY.                    14420448
036500     MOVE WS-BRT-KEY       TO IO-CTL-NOM-KEY.                     14420448
033900     PERFORM R020-CALL-PPIOCTL THRU R029-CALL-PPIOCTL-EXIT.       PPBENXJD
034000                                                                  PPBENXJD
034100     IF IO-CTL-ERROR-CODE NOT = '00' AND                          PPBENXJD
034200                          NOT = '05' AND                          PPBENXJD
034300                          NOT = '06' AND                          PPBENXJD
034400                          NOT = '13' AND                          PPBENXJD
034500                          NOT = '21'                              PPBENXJD
034600*        ******************************************               PPBENXJD
034700*        * ABORT PROGRAM DUE TO ERROR READING TABLE               PPBENXJD
034800*        ******************************************               PPBENXJD
034900         MOVE CONSTANT-ABORT TO KXJD-RETURN-STATUS-FLAG           PPBENXJD
035000         MOVE CONSTANT-EXIT-PROGRAM TO PROGRAM-STATUS-FLAG        PPBENXJD
035100     ELSE IF IO-CTL-ERROR-CODE = '00' AND                         PPBENXJD
035200         CTL-SEG-TAB-DEL NOT = HIGH-VALUE                         PPBENXJD
035300*        ***************************************************      PPBENXJD
035400*        * HIGH-VALUE WOULD INDICATE THAT RECORD IS DELETED,      PPBENXJD
035500*        * WHICH WOULD NOT BE A TRUE "FIND"                       PPBENXJD
035600*        ***************************************************      PPBENXJD
035700         MOVE 'Y' TO FOUND-RATE-SW                                PPBENXJD
035800     ELSE IF IO-CTL-ERROR-CODE  = '05' OR '06' OR '13' OR '21'    PPBENXJD
035900*        *******************************************************  PPBENXJD
036000*        * RATE NOT FOUND, SO GET KEY FOR NEXT ITERATION OF LOOP  PPBENXJD
036100*        *******************************************************  PPBENXJD
042910*****                                                          CD 05390718
039200                IF WS-BRT-BRSC NOT = WS-DEFAULT-BRT               14420448
039300                   MOVE '2'  TO KXJD-RETURN-STATUS-FLAG           14420448
039400                   MOVE WS-DEFAULT-BRT  TO WS-BRT-BRSC            14420448
039500                   GO TO P050-ACCESS-CTL                          14420448
039600                ELSE                                              14420448
039700                   MOVE CONSTANT-ABORT TO KXJD-RETURN-STATUS-FLAG 14420448
039800                   MOVE CONSTANT-EXIT-PROGRAM TO                  14420448
039900                                       PROGRAM-STATUS-FLAG.       14420448
036500                                                                  PPBENXJD
036600*    **********************************************************   PPBENXJD
036700*    * SAVE PREVIOUS, TO AVOID UNECESSARY DISK ACCESS IN FUTURE   PPBENXJD
036800*    **********************************************************   PPBENXJD
036900                                                                  PPBENXJD
037000     IF FOUND-RATE                                                PPBENXJD
037100         MOVE CTL-SEGMENT-TABLE TO XBRT-BENEFITS-RATES-RECORD.    PPBENXJD
037200     SKIP3                                                        PPBENXJD
037300 P059-ACCESS-CTL-EXIT.                                            PPBENXJD
037400     EXIT.                                                        PPBENXJD
037500     EJECT                                                        PPBENXJD
037600 R000-SUB-PROGRAM-CALLS SECTION.                                  PPBENXJD
037700     SKIP2                                                        PPBENXJD
045610*****                                                          CD 05390718
038100     SKIP3                                                        PPBENXJD
038500 R020-CALL-PPIOCTL.                                               PPBENXJD
038600     SKIP1                                                        PPBENXJD
038700     CALL 'PPIOCTL' USING CTL-INTERFACE CTL-SEGMENT-TABLE.        PPBENXJD
038800     SKIP3                                                        PPBENXJD
038900 R029-CALL-PPIOCTL-EXIT.                                          PPBENXJD
039000     EXIT.                                                        PPBENXJD
046400 Z01-PROCESS-EFF-DATE SECTION.                                    36220651
046500     COPY CPPDEFDT.                                               36220651
