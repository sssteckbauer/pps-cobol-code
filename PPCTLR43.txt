000010**************************************************************/   42541077
000020*  PROGRAM: PPCTLR43                                         */   42541077
000030*  RELEASE: ___1077______ SERVICE REQUEST(S): ___14254_____  */   42541077
000040*  NAME:_____JLT_________ MODIFICATION DATE:  ___07/15/96__  */   42541077
000050*  DESCRIPTION:                                              */   42541077
000060*  - THIS MODULE WILL LIST THE CONTENTS OF THE INCENTIVE     */   42541077
000061*    AWARD PROGRAM TABLE.                                    */   42541077
000070**************************************************************/   42541077
000700******************************************************************PPCTLR43
000800     SKIP1                                                        PPCTLR43
000900 IDENTIFICATION DIVISION.                                         PPCTLR43
001000 PROGRAM-ID.    PPCTLR43.                                         PPCTLR43
001100 AUTHOR.        UCOP.                                             PPCTLR43
001200 INSTALLATION.  UNIVERSITY OF CALIFORNIA, BASE PAYROLL SYSTEM.    PPCTLR43
001300 DATE-WRITTEN.  MAY 20, 1996.                                     PPCTLR43
001400 DATE-COMPILED.                                                   PPCTLR43
001500     SKIP1                                                        PPCTLR43
001600******************************************************************PPCTLR43
001700*                                                                *PPCTLR43
001800*  PROGRAM NAME:       PPCTLR43, CONTROL TABLE 43 REPORT MODULE  *PPCTLR43
001900*  LANGUAGE:           IBM VS COBOL II                           *PPCTLR43
002000*  TYPE:               CALLED SUBPROGRAM                         *PPCTLR43
002100*                                                                *PPCTLR43
002200*  REPORTS:            PPP0061, UPDATE MESSAGES (VIA PPCTLMSG)   *PPCTLR43
002300*                      PPP0065, INCENTIVE AWARD (IAP) TABLE      *PPCTLR43
002400*                                                                *PPCTLR43
002500*  CALLED MODULES:     PPCTLMSG                                  *PPCTLR43
002600*                                                                *PPCTLR43
002700*  DATASETS:                                                     *PPCTLR43
002800*       INPUT:         DB2 TABLES, AS LISTED IN WORKING-STORAGE  *PPCTLR43
002900*       OUTPUT:        PRINT FILE                                *PPCTLR43
003000*       WORK:          NONE                                      *PPCTLR43
003100*                                                                *PPCTLR43
003200******************************************************************PPCTLR43
003300     EJECT                                                        PPCTLR43
003400 ENVIRONMENT DIVISION.                                            PPCTLR43
003500 CONFIGURATION SECTION.                                           PPCTLR43
003600 SOURCE-COMPUTER.                                                 PPCTLR43
003700                                 COPY CPOTXUCS.                   PPCTLR43
003800 OBJECT-COMPUTER.                                                 PPCTLR43
003900                                 COPY CPOTXOBJ.                   PPCTLR43
004000     SKIP1                                                        PPCTLR43
004100 SPECIAL-NAMES.                                                   PPCTLR43
004200     C01 IS NEW-PAGE.                                             PPCTLR43
004300     SKIP1                                                        PPCTLR43
004400 INPUT-OUTPUT SECTION.                                            PPCTLR43
004500     SKIP1                                                        PPCTLR43
004600 FILE-CONTROL.                                                    PPCTLR43
004700     SELECT  PRINT-FILE          ASSIGN    TO   UT-S-PPP0065.     PPCTLR43
004800     EJECT                                                        PPCTLR43
004900 DATA DIVISION.                                                   PPCTLR43
005000 FILE SECTION.                                                    PPCTLR43
005100 FD  PRINT-FILE                  COPY CPFDXPRT.                   PPCTLR43
005200     EJECT                                                        PPCTLR43
005300 WORKING-STORAGE SECTION.                                         PPCTLR43
005400 01  WS-AGGREGATES.                                               PPCTLR43
005500     05  WSA-IDENTITY.                                            PPCTLR43
005600         10  WSA-PROGRAM-ID      PIC  X(0008) VALUE 'PPCTLR43'.   PPCTLR43
005700         10  FILLER              PIC  X(0033) VALUE               PPCTLR43
005800             ' WORKING-STORAGE SECTION (BEGINS)'.                 PPCTLR43
006400     SKIP1                                                        PPCTLR43
006500 01  WSS-STORES.                                                  PPCTLR43
006600     05  WS-END-OF-FILE-SW       PIC  X(0001) VALUE SPACES.       PPCTLR43
006700         88  WS-END-OF-FILE                   VALUE HIGH-VALUES.  PPCTLR43
006800                                                                  PPCTLR43
006900 01  WS-CALLED-ROUTINES.                                          PPCTLR43
006910     10  PGM-PPMSGUT2            PIC  X(0008) VALUE 'PPMSGUT2'.   PPCTLR43
006920                                                                  PPCTLR43
007000     EJECT                                                        PPCTLR43
007100******************************************************************PPCTLR43
007200*    REPORT HEADING AND DETAIL PRINT LINES                       *PPCTLR43
007300*----------------------------------------------------------------*PPCTLR43
007400 01  RPT43-HD1.                                                   PPCTLR43
007500     05  FILLER                  PIC X(0001) VALUE SPACES.        PPCTLR43
007600     05  FILLER                  PIC X(0050) VALUE SPACES.        PPCTLR43
007700     05  FILLER                  PIC X(0031) VALUE                PPCTLR43
007800                               '                               '. PPCTLR43
007900     05  FILLER                  PIC X(0051) VALUE SPACES.        PPCTLR43
008000 01  RPT43-HD2.                                                   PPCTLR43
008592     05  FILLER        PIC X(0001) VALUE SPACES.                  PPCTLR43
008593     05  FILLER        PIC X(0030) VALUE SPACES.                  PPCTLR43
008594     05  FILLER        PIC X(0010) VALUE ' PERSONNEL'.            PPCTLR43
008595     05  FILLER        PIC X(0010) VALUE '  APPT   T'.            PPCTLR43
008596     05  FILLER        PIC X(0010) VALUE 'ITLE    AP'.            PPCTLR43
008597     05  FILLER        PIC X(0010) VALUE 'PT   TITLE'.            PPCTLR43
008598     05  FILLER        PIC X(0010) VALUE ' RANGE    '.            PPCTLR43
008599     05  FILLER        PIC X(0010) VALUE ' EFF      '.            PPCTLR43
008600     05  FILLER        PIC X(0010) VALUE ' ASSESSMT '.            PPCTLR43
008601     05  FILLER        PIC X(0008) VALUE SPACES.                  PPCTLR43
008610 01  RPT43-HD3.                                                   PPCTLR43
008700     05  FILLER        PIC X(0001) VALUE SPACES.                  PPCTLR43
008710     05  FILLER        PIC X(0030) VALUE SPACES.                  PPCTLR43
008800     05  FILLER        PIC X(0010) VALUE '  PROGRAM '.            PPCTLR43
008810     05  FILLER        PIC X(0010) VALUE '  TYPE  UN'.            PPCTLR43
008820     05  FILLER        PIC X(0010) VALUE 'IT CD  REP'.            PPCTLR43
008830     05  FILLER        PIC X(0010) VALUE ' CD   BGN '.            PPCTLR43
008840     05  FILLER        PIC X(0010) VALUE '  END     '.            PPCTLR43
008850     05  FILLER        PIC X(0010) VALUE ' DATE     '.            PPCTLR43
008860     05  FILLER        PIC X(0010) VALUE ' RATE CD  '.            PPCTLR43
008900     05  FILLER        PIC X(0008) VALUE SPACES.                  PPCTLR43
009800 01  RPT43-DTL01                             VALUE SPACES.        PPCTLR43
009900     05  FILLER                    PIC X(0001).                   PPCTLR43
009910     05  FILLER                    PIC X(0035).                   PPCTLR43
010000     05  RPT43-DTL-PERSONNEL-PGM   PIC X(0001).                   PPCTLR43
010001     05  FILLER                    PIC X(0007).                   PPCTLR43
010010     05  RPT43-DTL-APPT-TYPE       PIC X(0001).                   PPCTLR43
010011     05  FILLER                    PIC X(0006).                   PPCTLR43
010012     05  RPT43-DTL-TITLE-UNIT-CD   PIC X(0002).                   PPCTLR43
010013     05  FILLER                    PIC X(0007).                   PPCTLR43
010014     05  RPT43-DTL-APPT-REP-CODE   PIC X(0001).                   PPCTLR43
010015     05  FILLER                    PIC X(0005).                   PPCTLR43
010016     05  RPT43-DTL-TITLE-CD-LO     PIC X(0004).                   PPCTLR43
010017     05  RPT43-DTL-TITLE-DASH      PIC X(0001).                   PPCTLR43
010018     05  RPT43-DTL-TITLE-CD-HI     PIC X(0004).                   PPCTLR43
010019     05  FILLER                    PIC X(0005).                   PPCTLR43
010020     05  RPT43-DTL-EFFECTIVE-DT    PIC X(0008).                   PPCTLR43
010021     05  FILLER                    PIC X(0005).                   PPCTLR43
010032     05  RPT43-DTL-ASSMT-RT-ASK1   PIC X(0001).                   PPCTLR43
010033     05  RPT43-DTL-ASSMT-RATE-CD   PIC X(0004).                   PPCTLR43
010034     05  RPT43-DTL-ASSMT-RT-ASK2   PIC X(0001).                   PPCTLR43
010035     05  FILLER                    PIC X(0006).                   PPCTLR43
011500     EJECT                                                        PPCTLR43
011600 01  XCNT-SUPPORT-FIELDS.        COPY CPWSXCNT.                   PPCTLR43
011700     EJECT                                                        PPCTLR43
011800 01  XWHC-COMPILE-WORK-AREA.     COPY CPWSXWHC.                   PPCTLR43
011801                                                                  PPCTLR43
011810 01  DATE-CONVERSION-WORK-AREAS. COPY CPWSXDC3.                   PPCTLR43
011820                                                                  PPCTLR43
011900     EJECT                                                        PPCTLR43
012000 01  MSSG-INTERFACE              EXTERNAL.                        PPCTLR43
012100                                 COPY CPWSXMIF.                   PPCTLR43
012200     EJECT                                                        PPCTLR43
012300 01  PPDB2MSG-INTERFACE.         COPY CPLNKDB2.                   PPCTLR43
012400******************************************************************PPCTLR43
012500*  SQL WORKING STORAGE                                           *PPCTLR43
012600*================================================================*PPCTLR43
012700*  SQL PROGRAM-VIEWS USED:                                       *PPCTLR43
012800*                                                                *PPCTLR43
012900*                 VIEW DDL/DCL                                   *PPCTLR43
013000*  VIEW NAME      INCLUDE NAME   VIEW CURSORS                    *PPCTLR43
013100*  -------------  ------------   ------------------              *PPCTLR43
013110*  PPPVZIAP_IAP   PPPVZIAP        PPPVZIAP_IAP_ROW               *PPCTLR43
013130******************************************************************PPCTLR43
013180     SKIP1                                                        PPCTLR43
013190 01  DCLPPPVZIAP-IAP.                                             PPCTLR43
013191     EXEC    SQL                                                  PPCTLR43
013192             INCLUDE PPPVZIAP                                     PPCTLR43
013193     END-EXEC.                                                    PPCTLR43
015000******************************************************************PPCTLR43
015400*  SQL CURSORS                                                   *PPCTLR43
015600******************************************************************PPCTLR43
015700     EXEC    SQL                                                  PPCTLR43
015800             DECLARE             PPPVZIAP_IAP_ROW                 PPCTLR43
015900             CURSOR                                               PPCTLR43
016000             FOR SELECT IAP_PERSONNEL_PGM                         PPCTLR43
016010                       ,IAP_APPT_TYPE                             PPCTLR43
016011                       ,IAP_TITLE_UNIT_CD                         PPCTLR43
016012                       ,IAP_APPT_REP_CODE                         PPCTLR43
016013                       ,IAP_TITLE_CD_LO                           PPCTLR43
016014                       ,IAP_TITLE_CD_HI                           PPCTLR43
016020                       ,CHAR(IAP_EFFECTIVE_DT,ISO)                PPCTLR43
016021                       ,IAP_ASSMT_RATE_CD                         PPCTLR43
016100                       FROM      PPPVZIAP_IAP                     PPCTLR43
016200                       ORDER BY 1 , 2 , 3 , 4 , 5 , 6 , 7         PPCTLR43
016400     END-EXEC.                                                    PPCTLR43
017310     EXEC    SQL                                                  PPCTLR43
017320             INCLUDE SQLCA                                        PPCTLR43
017330     END-EXEC.                                                    PPCTLR43
017340****                   ORDER BY  IAP_PERSONNEL_PGM                PPCTLR43
017350****                           , IAP_APPT_TYPE                    PPCTLR43
017360****                           , IAP_TITLE_UNIT_CD                PPCTLR43
017370****                           , IAP_APPT_REP_CODE                PPCTLR43
017380****                           , IAP_TITLE_CD_LO                  PPCTLR43
017390****                           , IAP_TITLE_CD_HI                  PPCTLR43
017391****                           , IAP_EFFECTIVE_DT                 PPCTLR43
017400     EJECT                                                        PPCTLR43
017500 01  WS-CLOSURE.                                                  PPCTLR43
017600     05  FILLER                  PIC  X(0031)   VALUE             PPCTLR43
017700         ' WORKING-STORAGE SECTION (ENDS)'.                       PPCTLR43
017800     EJECT                                                        PPCTLR43
017900 LINKAGE SECTION.                                                 PPCTLR43
018000     SKIP1                                                        PPCTLR43
018100******************************************************************PPCTLR43
018200*             L I N K A G E     S E C T I O N                    *PPCTLR43
018300******************************************************************PPCTLR43
018400     SKIP1                                                        PPCTLR43
018500 01  CTH-CTL-TABLE-MAINT-HEADERS.                                 PPCTLR43
018600     05  CTH-RPT-HD1.                                             PPCTLR43
018700         10  FILLER              PIC  X(0001).                    PPCTLR43
018800         10  CTH-RPT-ID          PIC  X(0007).                    PPCTLR43
018900         10  FILLER              PIC  X(0001).                    PPCTLR43
019000         10  CTH-PROG-ID         PIC  X(0008).                    PPCTLR43
019100         10  FILLER              PIC  X(0001).                    PPCTLR43
019200         10  CTH-COMPILE-DATE.                                    PPCTLR43
019300             15  CTH-COMPILE-MM  PIC  9(0002).                    PPCTLR43
019400             15  CTH-COMPILE-DD  PIC  9(0002).                    PPCTLR43
019500             15  CTH-COMPILE-YY  PIC  9(0002).                    PPCTLR43
019600         10  FILLER              PIC  X(0102).                    PPCTLR43
019700         10  CTH-PAGE-NO         PIC  ZZZ,ZZ9.                    PPCTLR43
019800     05  CTH-RPT-HD2             PIC  X(0133).                    PPCTLR43
019900     05  CTH-RPT-HD3.                                             PPCTLR43
020000         10  FILLER              PIC  X(0047).                    PPCTLR43
020100         10  CTH-RPT-TTL         PIC  X(0040).                    PPCTLR43
020200         10  FILLER              PIC  X(0046).                    PPCTLR43
020300     EJECT                                                        PPCTLR43
020400 PROCEDURE DIVISION              USING                            PPCTLR43
020500                                      CTH-CTL-TABLE-MAINT-HEADERS.PPCTLR43
020600******************************************************************PPCTLR43
020700*                      SQL FROM PRECOMPILER                      *PPCTLR43
020800*----------------------------------------------------------------*PPCTLR43
020900*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPCTLR43
021000*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPCTLR43
021100******************************************************************PPCTLR43
021200     EXEC    SQL                                                  PPCTLR43
021300             INCLUDE   CPPDXE99                                   PPCTLR43
021400     END-EXEC.                                                    PPCTLR43
021500     SKIP3                                                        PPCTLR43
021600 00000-MAIN SECTION.                                              PPCTLR43
021800     PERFORM 10000-BEGIN-REPORT.                                  PPCTLR43
022000     PERFORM 20000-PRINT-REPORT                                   PPCTLR43
022100             UNTIL     XCNT-REPORT-COMPLETED.                     PPCTLR43
022300     PERFORM 30000-END-REPORT.                                    PPCTLR43
022500     PERFORM 00000-GOBACK.                                        PPCTLR43
022700 00000-GOBACK SECTION.                                            PPCTLR43
022900     GOBACK.                                                      PPCTLR43
023000     EJECT                                                        PPCTLR43
023100 10000-BEGIN-REPORT SECTION.                                      PPCTLR43
023300     PERFORM 81100-DISPLAY-WHEN-COMPILED.                         PPCTLR43
023500     SET     XCNT-REPORT-PPP0065                                  PPCTLR43
023600             XCNT-REPORT-CTH-RPT-TTL-RPT43                        PPCTLR43
023700                                 TO   TRUE.                       PPCTLR43
023800     OPEN    OUTPUT              PRINT-FILE.                      PPCTLR43
023900     PERFORM 21000-PRINT-REPORT-HEADING.                          PPCTLR43
024100     PERFORM 71110-OPEN-PPPVZIAP-IAP-ROWS.                        PPCTLR43
024200     PERFORM 71120-FETCH-PPPVZIAP-IAP-ROW.                        PPCTLR43
024700     EJECT                                                        PPCTLR43
024800 20000-PRINT-REPORT SECTION.                                      PPCTLR43
025000     IF      XCNT-END-OF-PAGE                                     PPCTLR43
025100             PERFORM   21000-PRINT-REPORT-HEADING                 PPCTLR43
025200     END-IF.                                                      PPCTLR43
025400     EVALUATE          TRUE                                       PPCTLR43
025500             WHEN      WS-END-OF-FILE                             PPCTLR43
025700                       SET       XCNT-REPORT-COMPLETED            PPCTLR43
025800                                 TO   TRUE                        PPCTLR43
026000             WHEN      OTHER                                      PPCTLR43
026400                       MOVE      IAP-PERSONNEL-PGM                PPCTLR43
026500                                 TO   RPT43-DTL-PERSONNEL-PGM     PPCTLR43
026510                       MOVE      IAP-APPT-TYPE                    PPCTLR43
026520                                 TO   RPT43-DTL-APPT-TYPE         PPCTLR43
026710                       MOVE      IAP-TITLE-UNIT-CD                PPCTLR43
026720                                 TO   RPT43-DTL-TITLE-UNIT-CD     PPCTLR43
026730                       MOVE      IAP-APPT-REP-CODE                PPCTLR43
026740                                 TO   RPT43-DTL-APPT-REP-CODE     PPCTLR43
026741                       MOVE      IAP-TITLE-CD-LO                  PPCTLR43
026742                                 TO   RPT43-DTL-TITLE-CD-LO       PPCTLR43
026743                       IF  IAP-TITLE-CD-LO  = SPACES              PPCTLR43
026744                           MOVE SPACES  TO RPT43-DTL-TITLE-DASH   PPCTLR43
026746                       ELSE                                       PPCTLR43
026747                           MOVE '-'     TO RPT43-DTL-TITLE-DASH   PPCTLR43
026748                       END-IF                                     PPCTLR43
026749                       MOVE      IAP-TITLE-CD-HI                  PPCTLR43
026750                                 TO   RPT43-DTL-TITLE-CD-HI       PPCTLR43
026800                       MOVE IAP-EFFECTIVE-DT   TO XDC3-ISO-DATE   PPCTLR43
026820                       PERFORM XDC3-CONVERT-ISO-TO-FMT            PPCTLR43
026830                       MOVE XDC3-FMT-DATE                         PPCTLR43
026900                                 TO   RPT43-DTL-EFFECTIVE-DT      PPCTLR43
027000                       MOVE      IAP-ASSMT-RATE-CD                PPCTLR43
027001                                 TO   RPT43-DTL-ASSMT-RATE-CD     PPCTLR43
027002                       IF  IAP-ASSMT-RATE-CD  = 'NONE'            PPCTLR43
027003                           MOVE '*'    TO RPT43-DTL-ASSMT-RT-ASK1 PPCTLR43
027004                           MOVE '*'    TO RPT43-DTL-ASSMT-RT-ASK2 PPCTLR43
027005                       ELSE                                       PPCTLR43
027006                           MOVE SPACES TO RPT43-DTL-ASSMT-RT-ASK1 PPCTLR43
027007                           MOVE SPACES TO RPT43-DTL-ASSMT-RT-ASK2 PPCTLR43
027009                       END-IF                                     PPCTLR43
027200                       PERFORM   23000-PRINT-REPORT-DETAIL        PPCTLR43
027210                       PERFORM   71120-FETCH-PPPVZIAP-IAP-ROW     PPCTLR43
031200     END-EVALUATE.                                                PPCTLR43
031300     EJECT                                                        PPCTLR43
031420 21000-PRINT-REPORT-HEADING SECTION.                              PPCTLR43
031600     PERFORM 81200-PRINT-REPORT-TOP.                              PPCTLR43
031700     WRITE   PRINT-REC           FROM RPT43-HD1                   PPCTLR43
031800                                 AFTER ADVANCING 2 LINES.         PPCTLR43
031900     WRITE   PRINT-REC           FROM RPT43-HD2                   PPCTLR43
032000                                 AFTER ADVANCING 3 LINES.         PPCTLR43
032100     WRITE   PRINT-REC           FROM RPT43-HD3                   PPCTLR43
032200                                 AFTER ADVANCING 1 LINES.         PPCTLR43
032300     ADD     6                   TO   LINE-COUNT.                 PPCTLR43
032500     PERFORM 22000-EMPTY-REPORT-DETAIL.                           PPCTLR43
032600     EJECT                                                        PPCTLR43
032700 22000-EMPTY-REPORT-DETAIL SECTION.                               PPCTLR43
032900     MOVE    SPACES              TO   RPT43-DTL01.                PPCTLR43
033000     EJECT                                                        PPCTLR43
033100 23000-PRINT-REPORT-DETAIL SECTION.                               PPCTLR43
033300     WRITE   PRINT-REC           FROM RPT43-DTL01                 PPCTLR43
033400                                 AFTER ADVANCING 2 LINES.         PPCTLR43
033500     ADD     2                   TO   LINE-COUNT.                 PPCTLR43
033700     PERFORM 22000-EMPTY-REPORT-DETAIL.                           PPCTLR43
033800     EJECT                                                        PPCTLR43
033900 30000-END-REPORT SECTION.                                        PPCTLR43
034100     CLOSE   PRINT-FILE.                                          PPCTLR43
034200     EJECT                                                        PPCTLR43
034300 71110-OPEN-PPPVZIAP-IAP-ROWS SECTION.                            PPCTLR43
034500     MOVE    '71110-OPEN-PPPVZIAP-IAP-ROWS'                       PPCTLR43
034600                                 TO   DB2MSG-TAG.                 PPCTLR43
034700     EXEC    SQL                                                  PPCTLR43
034800             OPEN      PPPVZIAP_IAP_ROW                           PPCTLR43
034900     END-EXEC.                                                    PPCTLR43
035000     EJECT                                                        PPCTLR43
035100 71120-FETCH-PPPVZIAP-IAP-ROW SECTION.                            PPCTLR43
035300     MOVE    '71120-FETCH-PPPVZIAP-IAP-ROW'                       PPCTLR43
035400                                 TO   DB2MSG-TAG.                 PPCTLR43
035500     EXEC    SQL                                                  PPCTLR43
035600             FETCH               PPPVZIAP_IAP_ROW                 PPCTLR43
035750             INTO :IAP-PERSONNEL-PGM                              PPCTLR43
035760                , :IAP-APPT-TYPE                                  PPCTLR43
035770                , :IAP-TITLE-UNIT-CD                              PPCTLR43
035780                , :IAP-APPT-REP-CODE                              PPCTLR43
035790                , :IAP-TITLE-CD-LO                                PPCTLR43
035791                , :IAP-TITLE-CD-HI                                PPCTLR43
035792                , :IAP-EFFECTIVE-DT                               PPCTLR43
035794                , :IAP-ASSMT-RATE-CD                              PPCTLR43
035800     END-EXEC.                                                    PPCTLR43
036000     MOVE    SQLCODE             TO   XCNT-DB2-SQLCODE.           PPCTLR43
036100     EVALUATE          TRUE                                       PPCTLR43
036200             WHEN      XCNT-DB2-END-OF-CURSOR                     PPCTLR43
036300                       PERFORM   71130-CLOSE-PPPVZIAP-IAP-ROWS    PPCTLR43
036500             WHEN      OTHER                                      PPCTLR43
036600                       CONTINUE                                   PPCTLR43
036800     END-EVALUATE.                                                PPCTLR43
036900     EJECT                                                        PPCTLR43
037000 71130-CLOSE-PPPVZIAP-IAP-ROWS SECTION.                           PPCTLR43
037200     MOVE    '71130-CLOSE-PPPVZIAP-IAP-ROWS'                      PPCTLR43
037300                                 TO   DB2MSG-TAG.                 PPCTLR43
037400     EXEC    SQL                                                  PPCTLR43
037500             CLOSE     PPPVZIAP_IAP_ROW                           PPCTLR43
037600     END-EXEC.                                                    PPCTLR43
037700     SET     WS-END-OF-FILE           TO TRUE.                    PPCTLR43
037900     EJECT                                                        PPCTLR43
041700 81100-DISPLAY-WHEN-COMPILED SECTION.                             PPCTLR43
041900     MOVE    WSA-PROGRAM-ID      TO   DB2MSG-PGM-ID               PPCTLR43
042000                                      XWHC-DISPLAY-MODULE.        PPCTLR43
042100     COPY    CPPDXWHC.                                            PPCTLR43
042200     EJECT                                                        PPCTLR43
042300 81200-PRINT-REPORT-TOP SECTION.                                  PPCTLR43
042500     IF      PAGE-COUNT IS EQUAL TO ZERO                          PPCTLR43
042600             PERFORM   81210-INITIALIZE-REPORT                    PPCTLR43
042700             END-IF.                                              PPCTLR43
042900     ADD     1                   TO   PAGE-COUNT.                 PPCTLR43
043000     MOVE    PAGE-COUNT          TO   CTH-PAGE-NO.                PPCTLR43
043100     WRITE   PRINT-REC           FROM CTH-RPT-HD1                 PPCTLR43
043200                                      AFTER ADVANCING NEW-PAGE.   PPCTLR43
043300     WRITE   PRINT-REC           FROM CTH-RPT-HD2                 PPCTLR43
043400                                      AFTER ADVANCING 1 LINE.     PPCTLR43
043500     WRITE   PRINT-REC           FROM CTH-RPT-HD3                 PPCTLR43
043600                                      AFTER ADVANCING 1 LINE.     PPCTLR43
043700     MOVE    2                   TO   NO-OF-LINES.                PPCTLR43
043800     MOVE    3                   TO   LINE-COUNT.                 PPCTLR43
044000 81210-INITIALIZE-REPORT SECTION.                                 PPCTLR43
044200     MOVE    WSA-PROGRAM-ID      TO   CTH-PROG-ID.                PPCTLR43
044300     MOVE    XCNT-REPORT-ID      TO   CTH-RPT-ID.                 PPCTLR43
044400     MOVE    XWHC-MONTH-COMPILED TO   CTH-COMPILE-MM.             PPCTLR43
044500     MOVE    XWHC-DAY-COMPILED   TO   CTH-COMPILE-DD.             PPCTLR43
044600     MOVE    XWHC-YEAR-COMPILED  TO   CTH-COMPILE-YY.             PPCTLR43
044700     MOVE    XCNT-REPORT-CTH-RPT-TTL                              PPCTLR43
044800                                 TO   CTH-RPT-TTL.                PPCTLR43
045000 88110-CALL-PPMSGUTL-MODULE SECTION.                              PPCTLR43
045200     MOVE    XCNT-REPORT-PPP0061-MESSAGE                          PPCTLR43
045300                                 TO   XMSG-NUMBER.                PPCTLR43
045400     CALL PGM-PPMSGUT2           USING MSSG-INTERFACE             PPCTLR43
045600             END-CALL.                                            PPCTLR43
045610                                                                  PPCTLR43
045620 DATE-CONVERSION-DB2 SECTION.        COPY CPPDXDC3.               PPCTLR43
045630                                                                  PPCTLR43
045700     EJECT                                                        PPCTLR43
045800 89100-ABORT-ON-DB2-ERROR SECTION.                                PPCTLR43
046000     MOVE    DB2MSG-MESSNO       TO   XCNT-REPORT-PPP0061-MESSAGE.PPCTLR43
046100     PERFORM 88110-CALL-PPMSGUTL-MODULE.                          PPCTLR43
046200     EJECT                                                        PPCTLR43
046300*999999-SQL-ERROR.                                                PPCTLR43
046500     EXEC    SQL                                                  PPCTLR43
046600             INCLUDE   CPPDXP99                                   PPCTLR43
046700             END-EXEC.                                            PPCTLR43
046900     PERFORM 89100-ABORT-ON-DB2-ERROR.                            PPCTLR43
047000     EVALUATE          TRUE                                       PPCTLR43
047100             WHEN      XCNT-ABORT-RUN                             PPCTLR43
047200                       CONTINUE                                   PPCTLR43
047400             WHEN      OTHER                                      PPCTLR43
047500                       SET  XCNT-ABORT-RUN                        PPCTLR43
047600                                 TO   TRUE                        PPCTLR43
047700                       EXEC SQL  ROLLBACK  WORK                   PPCTLR43
047800                            END-EXEC                              PPCTLR43
047900     END-EVALUATE.                                                PPCTLR43
048100     PERFORM 00000-GOBACK.                                        PPCTLR43
048200* * * * * * END OF PPCTLR43  * * * * *                            PPCTLR43
