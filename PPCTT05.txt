000100**************************************************************/   30871460
000200*  PROGRAM: PPCTT05                                          */   30871460
000300*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000400*  NAME:___SRS___________ CREATION DATE:      ___01/22/03__  */   30871460
000500*  DESCRIPTION:                                              */   30871460
000600*  PPPCAL/CAD TABLE TRANSACTION ASSEMBLY MODULE              */   30871460
000700**************************************************************/   30871460
000800 IDENTIFICATION DIVISION.                                         PPCTT05
000900 PROGRAM-ID. PPCTT05.                                             PPCTT05
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTT05
001100 DATE-COMPILED.                                                   PPCTT05
001200 DATA DIVISION.                                                   PPCTT05
001300 FILE SECTION.                                                    PPCTT05
001400 WORKING-STORAGE SECTION.                                         PPCTT05
001500                                                                  PPCTT05
001600 01  WA-WORK-VARIABLES.                                           PPCTT05
001700     05  WA-READ-ERROR.                                           PPCTT05
001800         10 FILLER                     PIC X(37) VALUE            PPCTT05
001900                 'PPCTLTRD ERROR ON TRANS CALL: RETURN='.         PPCTT05
002000         10 WA-RTN-CODE                PIC XX.                    PPCTT05
002100         10 FILLER                     PIC X(07) VALUE            PPCTT05
002200                 ' COUNT='.                                       PPCTT05
002300         10 WA-WORK-Z5                 PIC ZZ,ZZ9.                PPCTT05
002400 01  WB-PROGRAM-CONSTANTS.                                        PPCTT05
002500     05  WB-TABLE-NO                   PIC X(2) VALUE '05'.       PPCTT05
002600 01  WC-PROGRAM-COUNTERS COMP SYNC.                               PPCTT05
002700     05  WC-ERR-IX                     PIC S9(04) VALUE ZERO.     PPCTT05
002800     05  IX                            PIC S9(04) VALUE ZERO.     PPCTT05
002900 01  WF-PROGRAM-FLAGS.                                            PPCTT05
003000     05  WF-ERROR-FLAG                 PIC 9(2) VALUE ZERO.       PPCTT05
003100         88  WF-NO-ERRORS                       VALUE ZERO.       PPCTT05
003200         88  WF-ACCEPT-TRANSACTION              VALUES 00 THRU 04.PPCTT05
003300         88  WF-REJECT-TRANSACTION              VALUES 05 THRU 99.PPCTT05
003400 01  WM-ERROR-MESSAGE-CODES.                                      PPCTT05
003500     05  MCT001                     PIC X(05) VALUE 'CT001'.      PPCTT05
003600     05  MCT003                     PIC X(05) VALUE 'CT003'.      PPCTT05
003700     05  MCT020                     PIC X(05) VALUE 'CT020'.      PPCTT05
003800     05  MCT021                     PIC X(05) VALUE 'CT021'.      PPCTT05
003900     05  MCT022                     PIC X(05) VALUE 'CT022'.      PPCTT05
004000     05  MCT023                     PIC X(05) VALUE 'CT023'.      PPCTT05
004100 01  CALENDAR-TABLE-TRANS.                                        PPCTT05
004200     05  CALT-ACTION-CODE           PIC X(01).                    PPCTT05
004300         88  COMMENT-TRANSACTION               VALUE '*'.         PPCTT05
004400         88  ADD-TRANSACTION                   VALUE 'A'.         PPCTT05
004500         88  CHANGE-TRANSACTION                VALUE 'C'.         PPCTT05
004600         88  DELETE-TRANSACTION                VALUE 'D'.         PPCTT05
004700     05  FILLER                     PIC X(02).                    PPCTT05
004800     05  CALT-YEAR.                                               PPCTT05
004900         10 CALT-YEAR-N             PIC 9(04).                    PPCTT05
005000     05  CALT-MONTH.                                              PPCTT05
005100         10 CALT-MONTH-N            PIC 99.                       PPCTT05
005200     05  CALT-DAY.                                                PPCTT05
005300         88 CALT-DAY-VALID   VALUES '00' THRU '31'.               PPCTT05
005400         10 CALT-DAY-N              PIC 99.                       PPCTT05
005500     05  CALT-DATA-1.                                             PPCTT05
005600         10 CALT-END-DATE           PIC XX.                       PPCTT05
005700         10 CALT-END-DATE-TYPE      PIC X.                        PPCTT05
005800         10 CALT-FISCAL-END-DT      PIC XX.                       PPCTT05
005900         10 CALT-FIRST-DAY-NAME     PIC XXX.                      PPCTT05
006000         10 FILLER                  PIC X(61).                    PPCTT05
006100     05  CALT-DATA-2 REDEFINES CALT-DATA-1.                       PPCTT05
006200         10 CALT-DAY-TYPE           PIC X.                        PPCTT05
006300         10 CALT-PAY-AREA.                                        PPCTT05
006400            15 CALT-PAY-END         PIC XX     OCCURS 8.          PPCTT05
006500            15 CALT-PAY-DAY         PIC XX     OCCURS 8.          PPCTT05
006600         10 FILLER                  PIC X(36).                    PPCTT05
006700                                                                  PPCTT05
006800 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPCTT05
006900 01  CTRI-CTL-REPORT-INTERFACE.        COPY CPLNCTRI.             PPCTT05
007000 01  CSRI-CTL-SORTED-READ-INTERFACE.   COPY CPLNCSRI.             PPCTT05
007100 01  CONTROL-TABLE-EDIT-INTERFACE.     COPY CPLNKCTE.             PPCTT05
007200 01  CONTROL-TABLE-UPDT-INTERFACE.     COPY CPLNKCTU.             PPCTT05
007300 01  CALENDAR-TABLE-INPUT.             COPY CPCTCALI.             PPCTT05
007400 01  CAL-TABLE-DATA.                                              PPCTT05
007500     EXEC SQL                                                     PPCTT05
007600         INCLUDE PPPVZCAL                                         PPCTT05
007700     END-EXEC.                                                    PPCTT05
007800 01  CAD-TABLE-DATA.                                              PPCTT05
007900     EXEC SQL                                                     PPCTT05
008000         INCLUDE PPPVZCAD                                         PPCTT05
008100     END-EXEC.                                                    PPCTT05
008200                                                                  PPCTT05
008300 LINKAGE SECTION.                                                 PPCTT05
008400                                                                  PPCTT05
008500 01  CTTI-CTL-TBL-TRANS-INTERFACE.     COPY CPLNCTTI.             PPCTT05
008600                                                                  PPCTT05
008700 PROCEDURE DIVISION USING CTTI-CTL-TBL-TRANS-INTERFACE.           PPCTT05
008800                                                                  PPCTT05
008900 0000-MAIN SECTION.                                               PPCTT05
009000                                                                  PPCTT05
009100     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTT05
009200     PERFORM 2000-PROCESS-TRANSACTIONS                            PPCTT05
009300       UNTIL CSRI-TABLE-TRANS-END.                                PPCTT05
009400                                                                  PPCTT05
009500 0099-END.                                                        PPCTT05
009600                                                                  PPCTT05
009700     GOBACK.                                                      PPCTT05
009800                                                                  PPCTT05
009900 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTT05
010000                                                                  PPCTT05
010100     MOVE 'PPCTT05'             TO XWHC-DISPLAY-MODULE.           PPCTT05
010200     COPY CPPDXWHC.                                               PPCTT05
010300     SET CTTI-NORMAL            TO TRUE.                          PPCTT05
010400     SET CTRI-PRINT-CALL        TO TRUE.                          PPCTT05
010500     MOVE SPACES                TO CTRI-SPACING-OVERRIDE-FLAG     PPCTT05
010600                                   CTRI-TRANSACTION               PPCTT05
010700                                   CTRI-TRANSACTION-DISPO         PPCTT05
010800                                   CTRI-TEXT-LINE                 PPCTT05
010900                                   CTRI-MESSAGE-NUMBER.           PPCTT05
011000     MOVE ZERO                  TO CTRI-MESSAGE-SEVERITY          PPCTT05
011100                                   CTTI-ERROR-SEVERITY.           PPCTT05
011200     MOVE WB-TABLE-NO           TO CSRI-CALL-TYPE-NUM.            PPCTT05
011300     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT05
011400                                                                  PPCTT05
011500 1099-INITIALIZATION-EXIT.                                        PPCTT05
011600     EXIT.                                                        PPCTT05
011700                                                                  PPCTT05
011800 2000-PROCESS-TRANSACTIONS SECTION.                               PPCTT05
011900                                                                  PPCTT05
012000     MOVE SPACES               TO CALENDAR-TABLE-INPUT.           PPCTT05
012100     MOVE CALENDAR-TABLE-TRANS TO CTRI-TRANSACTION.               PPCTT05
012200     MOVE CALT-YEAR            TO CALI-YEAR.                      PPCTT05
012300     MOVE CALT-MONTH           TO CALI-MONTH.                     PPCTT05
012400     MOVE CALT-DAY             TO CALI-DAY.                       PPCTT05
012500     MOVE CALT-ACTION-CODE     TO KCTE-ACTION.                    PPCTT05
012600     MOVE CTTI-ACTION-DATE     TO KCTE-LAST-ACTION-DT.            PPCTT05
012700     PERFORM 2200-MOVE-TRANSACTION-DATA                           PPCTT05
012800     PERFORM 3000-UPDATE-TRANS.                                   PPCTT05
012900     PERFORM 9300-FETCH-NEXT-TRANSACTION.                         PPCTT05
013000                                                                  PPCTT05
013100 2099-EXIT.                                                       PPCTT05
013200     EXIT.                                                        PPCTT05
013300                                                                  PPCTT05
013400 2200-MOVE-TRANSACTION-DATA SECTION.                              PPCTT05
013500                                                                  PPCTT05
013600     EVALUATE CALT-DAY                                            PPCTT05
013700      WHEN '00'                                                   PPCTT05
013800        IF CALT-END-DATE        NOT = SPACES                      PPCTT05
013900           MOVE CALT-END-DATE          TO CALI-END-DATE           PPCTT05
014000        END-IF                                                    PPCTT05
014100        IF CALT-END-DATE-TYPE   NOT = SPACES                      PPCTT05
014200           MOVE CALT-END-DATE-TYPE     TO CALI-END-DATE-TYPE      PPCTT05
014300        END-IF                                                    PPCTT05
014400        IF CALT-FISCAL-END-DT   NOT = SPACES                      PPCTT05
014500           MOVE CALT-FISCAL-END-DT     TO CALI-FISCAL-END-DT      PPCTT05
014600        END-IF                                                    PPCTT05
014700        IF CALT-FIRST-DAY-NAME  NOT = SPACES                      PPCTT05
014800           MOVE CALT-FIRST-DAY-NAME    TO CALI-FIRST-DAY-NAME     PPCTT05
014900        END-IF                                                    PPCTT05
015000      WHEN OTHER                                                  PPCTT05
015100        IF CALT-DAY-TYPE        NOT = SPACES                      PPCTT05
015200           MOVE CALT-DAY-TYPE          TO CALI-DAY-TYPE           PPCTT05
015300        END-IF                                                    PPCTT05
015400        IF CALT-PAY-AREA        NOT = SPACES                      PPCTT05
015500           PERFORM VARYING IX FROM 1 BY 1                         PPCTT05
015600             UNTIL IX > 8                                         PPCTT05
015700            IF CALT-PAY-END (IX) NOT = SPACES                     PPCTT05
015800               MOVE CALT-PAY-END (IX) TO CALI-PAY-END (IX)        PPCTT05
015900            END-IF                                                PPCTT05
016000            IF CALT-PAY-DAY (IX) NOT = SPACES                     PPCTT05
016100               MOVE CALT-PAY-DAY (IX) TO CALI-PAY-DAY (IX)        PPCTT05
016200            END-IF                                                PPCTT05
016300           END-PERFORM                                            PPCTT05
016400        END-IF                                                    PPCTT05
016500     END-EVALUATE.                                                PPCTT05
016600                                                                  PPCTT05
016700 2299-EXIT.                                                       PPCTT05
016800     EXIT.                                                        PPCTT05
016900                                                                  PPCTT05
017000 3000-UPDATE-TRANS SECTION.                                       PPCTT05
017100                                                                  PPCTT05
017200     SET WF-NO-ERRORS TO TRUE                                     PPCTT05
017300     CALL 'PPCTCALE' USING CONTROL-TABLE-EDIT-INTERFACE           PPCTT05
017400                           CALENDAR-TABLE-INPUT                   PPCTT05
017500                           CAL-TABLE-DATA                         PPCTT05
017600                           CAD-TABLE-DATA                         PPCTT05
017700     IF KCTE-ERR > ZERO                                           PPCTT05
017800        PERFORM 3100-PRINT-ERROR VARYING WC-ERR-IX FROM 1 BY 1    PPCTT05
017900          UNTIL WC-ERR-IX > KCTE-ERR                              PPCTT05
018000     END-IF.                                                      PPCTT05
018100     IF KCTE-STATUS-NOT-COMPLETED                                 PPCTT05
018200        MOVE MCT003 TO CTRI-MESSAGE-NUMBER                        PPCTT05
018300        SET WF-REJECT-TRANSACTION TO TRUE                         PPCTT05
018400     END-IF.                                                      PPCTT05
018500     IF WF-ACCEPT-TRANSACTION                                     PPCTT05
018600        MOVE 'TRANSACTION ACCEPTED' TO CTRI-TEXT-DISPO            PPCTT05
018700        ADD 1 TO CTTI-TRANS-ACCEPTED                              PPCTT05
018800        PERFORM 3200-UPDATE-DB2-TABLE                             PPCTT05
018900     ELSE                                                         PPCTT05
019000        MOVE 'TRANSACTION REJECTED' TO CTRI-TEXT-DISPO            PPCTT05
019100        ADD 1 TO CTTI-TRANS-REJECTED                              PPCTT05
019200     END-IF.                                                      PPCTT05
019300     PERFORM 9100-CALL-PPCTLRPT                                   PPCTT05
019400     MOVE 2 TO CTRI-SPACING-OVERRIDE.                             PPCTT05
019500                                                                  PPCTT05
019600 3099-EXIT.                                                       PPCTT05
019700     EXIT.                                                        PPCTT05
019800                                                                  PPCTT05
019900 3100-PRINT-ERROR SECTION.                                        PPCTT05
020000                                                                  PPCTT05
020100     MOVE KCTE-ERR-NO (WC-ERR-IX) TO CTRI-MESSAGE-NUMBER.         PPCTT05
020200     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT05
020300     IF CTRI-MESSAGE-SEVERITY > WF-ERROR-FLAG                     PPCTT05
020400         MOVE CTRI-MESSAGE-SEVERITY TO WF-ERROR-FLAG              PPCTT05
020500     END-IF.                                                      PPCTT05
020600                                                                  PPCTT05
020700 3199-EXIT.                                                       PPCTT05
020800     EXIT.                                                        PPCTT05
020900                                                                  PPCTT05
021000 3200-UPDATE-DB2-TABLE SECTION.                                   PPCTT05
021100                                                                  PPCTT05
021200     MOVE KCTE-ACTION TO KCTU-ACTION.                             PPCTT05
021300     CALL 'PPCTCALU' USING CONTROL-TABLE-UPDT-INTERFACE,          PPCTT05
021400                           CAL-TABLE-DATA                         PPCTT05
021500                           CAD-TABLE-DATA                         PPCTT05
021600     IF KCTU-STATUS-OK                                            PPCTT05
021700        EVALUATE TRUE                                             PPCTT05
021800          WHEN KCTU-ACTION-ADD                                    PPCTT05
021900            ADD 1 TO CTTI-ENTRIES-ADDED                           PPCTT05
022000          WHEN KCTU-ACTION-CHANGE                                 PPCTT05
022100            ADD 1 TO CTTI-ENTRIES-UPDATED                         PPCTT05
022200          WHEN KCTU-ACTION-DELETE                                 PPCTT05
022300            ADD 1 TO CTTI-ENTRIES-DELETED                         PPCTT05
022400          WHEN OTHER                                              PPCTT05
022500            GO TO 3300-BAD-UPDATE-CALL                            PPCTT05
022600        END-EVALUATE                                              PPCTT05
022700     ELSE                                                         PPCTT05
022800        GO TO 3300-BAD-UPDATE-CALL                                PPCTT05
022900     END-IF.                                                      PPCTT05
023000                                                                  PPCTT05
023100 3299-EXIT.                                                       PPCTT05
023200     EXIT.                                                        PPCTT05
023300                                                                  PPCTT05
023400 3300-BAD-UPDATE-CALL SECTION.                                    PPCTT05
023500                                                                  PPCTT05
023600     MOVE MCT001 TO CTRI-MESSAGE-NUMBER.                          PPCTT05
023700     MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5.                     PPCTT05
023800     STRING 'PPCTCALU ERROR: ACTION WAS "' KCTU-ACTION            PPCTT05
023900            '" STATUS WAS "'               KCTU-STATUS            PPCTT05
024000            '" COUNT='                     WA-WORK-Z5             PPCTT05
024100            DELIMITED BY SIZE INTO CTRI-TEXT-LINE.                PPCTT05
024200     PERFORM 9100-CALL-PPCTLRPT.                                  PPCTT05
024300     GO TO 9999-ABEND.                                            PPCTT05
024400                                                                  PPCTT05
024500 3399-EXIT.                                                       PPCTT05
024600     EXIT.                                                        PPCTT05
024700                                                                  PPCTT05
024800 9100-CALL-PPCTLRPT SECTION.                                      PPCTT05
024900                                                                  PPCTT05
025000     CALL 'PPCTLRPT' USING CTRI-CTL-REPORT-INTERFACE.             PPCTT05
025100     IF CTRI-MESSAGE-SEVERITY > CTTI-ERROR-SEVERITY               PPCTT05
025200         MOVE CTRI-MESSAGE-SEVERITY TO CTTI-ERROR-SEVERITY        PPCTT05
025300     END-IF.                                                      PPCTT05
025400     IF CTRI-RUN-ABORTED                                          PPCTT05
025500        GO TO 9999-ABEND                                          PPCTT05
025600     END-IF.                                                      PPCTT05
025700                                                                  PPCTT05
025800 9199-EXIT.                                                       PPCTT05
025900     EXIT.                                                        PPCTT05
026000                                                                  PPCTT05
026100 9300-FETCH-NEXT-TRANSACTION SECTION.                             PPCTT05
026200                                                                  PPCTT05
026300     PERFORM WITH TEST AFTER                                      PPCTT05
026400       UNTIL CSRI-TABLE-TRANS-END                                 PPCTT05
026500          OR CSRI-TRANSACTION(1:1) NOT = '*'                      PPCTT05
026600      CALL 'PPCTLTRD' USING CSRI-CTL-SORTED-READ-INTERFACE        PPCTT05
026700      IF CSRI-NORMAL                                              PPCTT05
026800         ADD 1 TO CTTI-TRANS-PROCESSED                            PPCTT05
026900         IF CSRI-TRANSACTION(1:1) = '*'                           PPCTT05
027000            MOVE CSRI-TRANSACTION TO CTRI-TRANSACTION             PPCTT05
027100            MOVE 'COMMENT ACCEPTED' TO CTRI-TRANSACTION-DISPO     PPCTT05
027200            PERFORM 9100-CALL-PPCTLRPT                            PPCTT05
027300            ADD 1 TO CTTI-TRANS-ACCEPTED                          PPCTT05
027400         ELSE                                                     PPCTT05
027500            MOVE CSRI-TRANSACTION TO CALENDAR-TABLE-TRANS         PPCTT05
027600         END-IF                                                   PPCTT05
027700      ELSE                                                        PPCTT05
027800        IF NOT CSRI-TABLE-TRANS-END                               PPCTT05
027900           MOVE MCT001              TO CTRI-MESSAGE-NUMBER        PPCTT05
028000           MOVE CSRI-RETURN-CODE    TO WA-RTN-CODE                PPCTT05
028100           MOVE CTTI-TRANS-PROCESSED TO WA-WORK-Z5                PPCTT05
028200           MOVE WA-READ-ERROR       TO CTRI-TEXT-LINE             PPCTT05
028300           PERFORM 9100-CALL-PPCTLRPT                             PPCTT05
028400           GO TO 9999-ABEND                                       PPCTT05
028500        END-IF                                                    PPCTT05
028600      END-IF                                                      PPCTT05
028700     END-PERFORM.                                                 PPCTT05
028800                                                                  PPCTT05
028900 9399-EXIT.                                                       PPCTT05
029000     EXIT.                                                        PPCTT05
029100                                                                  PPCTT05
029200 9999-ABEND SECTION.                                              PPCTT05
029300                                                                  PPCTT05
029400     SET CTTI-ABORT TO TRUE.                                      PPCTT05
029500     GOBACK.                                                      PPCTT05
029600*************************END OF SOURCE CODE***********************PPCTT05
