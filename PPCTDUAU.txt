000100**************************************************************/   30871465
000200*  PROGRAM: PPCTDUAU                                         */   30871465
000300*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000400*  NAME:___SRS___________ CREATION DATE:      ___02/11/03__  */   30871465
000500*  DESCRIPTION:                                              */   30871465
000600*  RANGE ADJUSTMENT DUA TABLE UPDATE MODULE                  */   30871465
000700**************************************************************/   30871465
000800 IDENTIFICATION DIVISION.                                         PPCTDUAU
000900 PROGRAM-ID. PPCTDUAU.                                            PPCTDUAU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTDUAU
001100 DATE-COMPILED.                                                   PPCTDUAU
001200 ENVIRONMENT DIVISION.                                            PPCTDUAU
001300 DATA DIVISION.                                                   PPCTDUAU
001400 WORKING-STORAGE SECTION.                                         PPCTDUAU
001500                                                                  PPCTDUAU
001600 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTDUAU
001700 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTDUAU
001800                                                                  PPCTDUAU
001900     EXEC SQL                                                     PPCTDUAU
002000        INCLUDE SQLCA                                             PPCTDUAU
002100     END-EXEC.                                                    PPCTDUAU
002200                                                                  PPCTDUAU
002300 LINKAGE SECTION.                                                 PPCTDUAU
002400                                                                  PPCTDUAU
002500 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTDUAU
002600 01  RANGE-ADJUST-DUA-TABLE-DATA.                                 PPCTDUAU
002700     EXEC SQL                                                     PPCTDUAU
002800        INCLUDE PPPVZDUA                                          PPCTDUAU
002900     END-EXEC.                                                    PPCTDUAU
003000                                                                  PPCTDUAU
003100 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE            PPCTDUAU
003200                          RANGE-ADJUST-DUA-TABLE-DATA.            PPCTDUAU
003300                                                                  PPCTDUAU
003400 0000-MAIN.                                                       PPCTDUAU
003500                                                                  PPCTDUAU
003600     EXEC SQL                                                     PPCTDUAU
003700        INCLUDE CPPDXE99                                          PPCTDUAU
003800     END-EXEC.                                                    PPCTDUAU
003900     MOVE 'PPCTDUAU' TO XWHC-DISPLAY-MODULE.                      PPCTDUAU
004000     COPY CPPDXWHC.                                               PPCTDUAU
004100     MOVE SPACE TO KCTU-STATUS.                                   PPCTDUAU
004200     IF KCTU-ACTION-ADD                                           PPCTDUAU
004300        PERFORM 2000-INSERT-DUA                                   PPCTDUAU
004400     ELSE                                                         PPCTDUAU
004500       IF KCTU-ACTION-CHANGE                                      PPCTDUAU
004600          PERFORM 2000-INSERT-DUA                                 PPCTDUAU
004700       ELSE                                                       PPCTDUAU
004800         IF KCTU-ACTION-DELETE                                    PPCTDUAU
004900            PERFORM 2200-DELETE-DUA                               PPCTDUAU
005000         ELSE                                                     PPCTDUAU
005100            SET KCTU-INVALID-ACTION TO TRUE                       PPCTDUAU
005200         END-IF                                                   PPCTDUAU
005300       END-IF                                                     PPCTDUAU
005400     END-IF.                                                      PPCTDUAU
005500                                                                  PPCTDUAU
005600 0100-END.                                                        PPCTDUAU
005700                                                                  PPCTDUAU
005800     GOBACK.                                                      PPCTDUAU
005900                                                                  PPCTDUAU
006000 2000-INSERT-DUA.                                                 PPCTDUAU
006100                                                                  PPCTDUAU
006200     MOVE '2000-INSRT-DUA' TO DB2MSG-TAG.                         PPCTDUAU
006300     EXEC SQL                                                     PPCTDUAU
006400         INSERT INTO PPPVZDUA_DUA                                 PPCTDUAU
006500              VALUES (:DUA-TUC                                    PPCTDUAU
006600                     ,:DUA-RDUC                                   PPCTDUAU
006700                     ,:DUA-REP-CODE                               PPCTDUAU
006800                     ,:DUA-LOCATION                               PPCTDUAU
006900                     ,:DUA-ACCT-OR-FUND-1                         PPCTDUAU
007000                     ,:DUA-ACCT-OR-FUND-2                         PPCTDUAU
007100                     ,:DUA-LAST-ACTION                            PPCTDUAU
007200                     ,:DUA-LAST-ACTION-DT                         PPCTDUAU
007300                     )                                            PPCTDUAU
007400     END-EXEC.                                                    PPCTDUAU
007500     IF SQLCODE NOT = ZERO                                        PPCTDUAU
007600        SET KCTU-INSERT-ERROR TO TRUE                             PPCTDUAU
007700     END-IF.                                                      PPCTDUAU
007800                                                                  PPCTDUAU
010300 2200-DELETE-DUA.                                                 PPCTDUAU
010400                                                                  PPCTDUAU
010500     MOVE '2200-DLTE-DUA ' TO DB2MSG-TAG.                         PPCTDUAU
010600     EXEC SQL                                                     PPCTDUAU
010700         DELETE FROM PPPVZDUA_DUA                                 PPCTDUAU
010800          WHERE DUA_TUC               = :DUA-TUC                  PPCTDUAU
010900            AND DUA_RDUC              = :DUA-RDUC                 PPCTDUAU
011000            AND DUA_REP_CODE          = :DUA-REP-CODE             PPCTDUAU
011100            AND DUA_LOCATION          = :DUA-LOCATION             PPCTDUAU
011200            AND DUA_ACCT_OR_FUND_1    = :DUA-ACCT-OR-FUND-1       PPCTDUAU
011300            AND DUA_ACCT_OR_FUND_2    = :DUA-ACCT-OR-FUND-2       PPCTDUAU
011400     END-EXEC.                                                    PPCTDUAU
011500     IF SQLCODE NOT = ZERO                                        PPCTDUAU
011600        SET KCTU-DELETE-ERROR TO TRUE                             PPCTDUAU
011700     END-IF.                                                      PPCTDUAU
011800                                                                  PPCTDUAU
011900*999999-SQL-ERROR.                                                PPCTDUAU
012000     EXEC SQL                                                     PPCTDUAU
012100        INCLUDE CPPDXP99                                          PPCTDUAU
012200     END-EXEC.                                                    PPCTDUAU
012210     EVALUATE TRUE                                                PPCTDUAU
012220       WHEN (KCTU-ACTION-ADD)                                     PPCTDUAU
012230         SET KCTU-INSERT-ERROR TO TRUE                            PPCTDUAU
012240       WHEN (KCTU-ACTION-CHANGE)                                  PPCTDUAU
012250         SET KCTU-UPDATE-ERROR TO TRUE                            PPCTDUAU
012260       WHEN (KCTU-ACTION-DELETE)                                  PPCTDUAU
012270         SET KCTU-DELETE-ERROR TO TRUE                            PPCTDUAU
012280     END-EVALUATE.                                                PPCTDUAU
012300     GO TO 0100-END.                                              PPCTDUAU
