000000**************************************************************/   12630761
000001*  PROGRAM: PPAPPUTL                                         */   12630761
000002*  RELEASE: ___0761______ SERVICE REQUEST(S): ____11263____  */   12630761
000003*  NAME:_____J. QUAN_____ CREATION DATE:      ___02/22/93__  */   12630761
000004*  DESCRIPTION:                                              */   12630761
000005*  -  NEW UTILITY TO ONLY LOAD EMPLOYEE APPOINTMENT DATA.    */   12630761
000007**************************************************************/   12630761
003100 IDENTIFICATION DIVISION.                                         PPAPPUTL
003200 PROGRAM-ID. PPAPPUTL.                                            PPAPPUTL
003300 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPAPPUTL
003400 AUTHOR.                                                          PPAPPUTL
003500******************************************************************PPAPPUTL
003600 DATE-WRITTEN.                                                    PPAPPUTL
003700 DATE-COMPILED.                                                   PPAPPUTL
003800*REMARKS.                                                         PPAPPUTL
003900******************************************************************PPAPPUTL
004000*                                                                *PPAPPUTL
004100*  - PPAPPUTL LOADS ALL APPOINTMENT DATA FROM THE EMPLOYEE DATA  *PPAPPUTL
004200*    BASE INTO LINKAGE INTERFACE CPLNKAPP.                       *PPAPPUTL
004800*                                                                *PPAPPUTL
004900******************************************************************PPAPPUTL
005000 ENVIRONMENT DIVISION.                                            PPAPPUTL
005100 CONFIGURATION SECTION.                                           PPAPPUTL
005200 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       PPAPPUTL
005300 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       PPAPPUTL
005400 SPECIAL-NAMES.                                                   PPAPPUTL
005500 INPUT-OUTPUT SECTION.                                            PPAPPUTL
005600 FILE-CONTROL.                                                    PPAPPUTL
005700     EJECT                                                        PPAPPUTL
005800******************************************************************PPAPPUTL
005900*                                                                *PPAPPUTL
006000*                         DATA DIVISION                          *PPAPPUTL
006100*                                                                *PPAPPUTL
006200******************************************************************PPAPPUTL
006300 DATA DIVISION.                                                   PPAPPUTL
006400 FILE SECTION.                                                    PPAPPUTL
006500     EJECT                                                        PPAPPUTL
006600******************************************************************PPAPPUTL
006700*                                                                *PPAPPUTL
006800*                    WORKING-STORAGE SECTION                     *PPAPPUTL
006900*                                                                *PPAPPUTL
007000******************************************************************PPAPPUTL
007100 WORKING-STORAGE SECTION.                                         PPAPPUTL
007200     SKIP1                                                        PPAPPUTL
007300*                                                                 PPAPPUTL
007400 01  WA-COMPILE-WORK-AREA.       COPY CPWSXWHC.                   PPAPPUTL
007500*                                                                 PPAPPUTL
007600 01  WS-APPOINT-WORKFIELDS.                                       PPAPPUTL
008300     05  WS-EMPLOYEE-ID          PIC  X(09)          VALUE SPACE. PPAPPUTL
010910*                                                                 PPAPPUTL
011000 01  WS-FLAGS.                                                    PPAPPUTL
011100     05  FIRST-TIME-SW           PIC  X(01)       VALUE 'Y'.      PPAPPUTL
011200         88  FIRST-TIME                           VALUE 'Y'.      PPAPPUTL
011300         88  NOT-FIRST-TIME                       VALUE 'N'.      PPAPPUTL
011310*                                                                 PPAPPUTL
011400     05  SQL-APPT-CURS-SW        PIC  X(01)       VALUE ' '.      PPAPPUTL
011500         88  APPT-ROW-CURS-OPENED                 VALUE 'O'.      PPAPPUTL
011600*                                                                 PPAPPUTL
011700 01  PPDB2MSG-INTERFACE.                     COPY CPLNKDB2.       PPAPPUTL
011800     EJECT                                                        PPAPPUTL
011900******************************************************************PPAPPUTL
012000*            WORKING-STORAGE SQL:                                *PPAPPUTL
012100*            DB2 STRUCTURES                                      *PPAPPUTL
012200******************************************************************PPAPPUTL
012300 01  APPOINT-DATA.                                                PPAPPUTL
012400     EXEC SQL                                                     PPAPPUTL
012600         INCLUDE PPPVAPP1                                         PPAPPUTL
012700     END-EXEC.                                                    PPAPPUTL
012800     SKIP1                                                        PPAPPUTL
013500     EXEC SQL                                                     PPAPPUTL
013600         INCLUDE SQLCA                                            PPAPPUTL
013700     END-EXEC.                                                    PPAPPUTL
013710     SKIP1                                                        PPAPPUTL
013800     EJECT                                                        PPAPPUTL
013900******************************************************************PPAPPUTL
014000*                                                                *PPAPPUTL
014100*            CURSOR DECLARATIONS                                 *PPAPPUTL
014110*                                                                *PPAPPUTL
014200******************************************************************PPAPPUTL
014300     EXEC SQL                                                     PPAPPUTL
014310         DECLARE APPT_ROW CURSOR FOR                              PPAPPUTL
014320             SELECT * FROM PPPVAPP1_APP                           PPAPPUTL
014330             WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                  PPAPPUTL
014340             ORDER BY APPT_NUM                                    PPAPPUTL
014350     END-EXEC.                                                    PPAPPUTL
014360     SKIP1                                                        PPAPPUTL
014420     EJECT                                                        PPAPPUTL
016400 LINKAGE SECTION.                                                 PPAPPUTL
016410******************************************************************PPAPPUTL
016420*                                                                *PPAPPUTL
016430*                        LINKAGE SECTION                         *PPAPPUTL
016440*                                                                *PPAPPUTL
016450******************************************************************PPAPPUTL
016500     SKIP1                                                        PPAPPUTL
016600 01  KAPP-INTERFACE.              COPY CPLNKAPP.                  PPAPPUTL
017700*                                                                *PPAPPUTL
017701     EJECT                                                        PPAPPUTL
017710******************************************************************PPAPPUTL
017720*                                                                *PPAPPUTL
017730*                       PROCEDURE DIVISION                       *PPAPPUTL
017740*                                                                *PPAPPUTL
017750******************************************************************PPAPPUTL
017800 PROCEDURE DIVISION USING KAPP-INTERFACE.                         PPAPPUTL
018100*                                                                *PPAPPUTL
018200******************************************************************PPAPPUTL
018300*                SQL FROM PRE-COMPILER                           *PPAPPUTL
018400******************************************************************PPAPPUTL
018500     SKIP2                                                        PPAPPUTL
018600******************************************************************PPAPPUTL
018700*   THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO       *PPAPPUTL
018800*         CAUSE A BRANCH TO 999999-ERROR.                        *PPAPPUTL
018900******************************************************************PPAPPUTL
019000     EXEC SQL                                                     PPAPPUTL
019100         WHENEVER SQLERROR GO TO 999999-SQL-ERROR                 PPAPPUTL
019200     END-EXEC.                                                    PPAPPUTL
019300     EJECT                                                        PPAPPUTL
019400******************************************************************PPAPPUTL
019500 1000-MAINLINE SECTION.                                           PPAPPUTL
019600     SKIP2                                                        PPAPPUTL
019710     MOVE SPACE TO KAPP-STATUS-FLAG.                              PPAPPUTL
019730     INITIALIZE KAPP-APPOINTMENT-GROUP.                           PPAPPUTL
019740     MOVE ZERO TO KAPP-APPT-LOADED.                               PPAPPUTL
019750     MOVE KAPP-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                     PPAPPUTL
019760     SKIP1                                                        PPAPPUTL
019800     MOVE 'PPAPPUTL' TO DB2MSG-PGM-ID.                            PPAPPUTL
019900     SKIP1                                                        PPAPPUTL
020000     IF FIRST-TIME                                                PPAPPUTL
020100        PERFORM 3000-INITIALIZATION                               PPAPPUTL
020110     END-IF.                                                      PPAPPUTL
020200     SKIP1                                                        PPAPPUTL
020510     IF  KAPP-LOAD-APPOINT                                        PPAPPUTL
020600         PERFORM 5000-LOAD-APPTS THRU 5000-LOAD-EXIT              PPAPPUTL
020610     ELSE                                                         PPAPPUTL
020630        IF  NOT KAPP-VALID-ACTIONS                                PPAPPUTL
020640            SET KAPP-ACTION-NOT-VALID TO TRUE                     PPAPPUTL
020700        END-IF                                                    PPAPPUTL
020800     END-IF.                                                      PPAPPUTL
021400     SKIP2                                                        PPAPPUTL
021700     GOBACK.                                                      PPAPPUTL
021800     EJECT                                                        PPAPPUTL
021900 3000-INITIALIZATION SECTION.                                     PPAPPUTL
022000     SKIP1                                                        PPAPPUTL
022200     MOVE 'PPAPPUTL' TO XWHC-DISPLAY-MODULE.                      PPAPPUTL
022300     COPY CPPDXWHC.                                               PPAPPUTL
023000     SET NOT-FIRST-TIME TO TRUE.                                  PPAPPUTL
024100     EJECT                                                        PPAPPUTL
024200 5000-LOAD-APPTS SECTION.                                         PPAPPUTL
024411     SKIP1                                                        PPAPPUTL
024412*===> INITIALIZE APPOINTMENT ROW CURSOR SWITCH.                   PPAPPUTL
024413     MOVE SPACE TO SQL-APPT-CURS-SW.                              PPAPPUTL
024414     SKIP1                                                        PPAPPUTL
024420     PERFORM SQL-OPEN-APPT-ROW-9810.                              PPAPPUTL
024500     PERFORM SQL-FETCH-APPT-ROW-9820.                             PPAPPUTL
025200     SKIP1                                                        PPAPPUTL
025300*===> CHECK FOR APPOINTMENTS FOUND.                               PPAPPUTL
025410     IF SQLCODE  = ZERO                                           PPAPPUTL
025411        SET KAPP-APPOINT-FOUND TO TRUE                            PPAPPUTL
025412     ELSE                                                         PPAPPUTL
025413         SET KAPP-APPOINT-NONE TO TRUE                            PPAPPUTL
025420         IF APPT-ROW-CURS-OPENED                                  PPAPPUTL
025430            PERFORM SQL-CLOSE-APPT-ROW-9830                       PPAPPUTL
025440         END-IF                                                   PPAPPUTL
025800         GO TO 5000-LOAD-EXIT                                     PPAPPUTL
025830     END-IF.                                                      PPAPPUTL
025900     SKIP1                                                        PPAPPUTL
026000*===> LOAD EDB APPOINTMENT DATA TO APPOINTMENT ARRAY              PPAPPUTL
026100     PERFORM VARYING KAPP-APPT-INDX FROM 1 BY 1                   PPAPPUTL
026110         UNTIL SQLCODE = +100 OR KAPP-APPT-INDX > 9               PPAPPUTL
026300         MOVE APPOINT-DATA TO KAPP-APPT-ARRAY (KAPP-APPT-INDX)    PPAPPUTL
029310         PERFORM SQL-FETCH-APPT-ROW-9820                          PPAPPUTL
029400     END-PERFORM.                                                 PPAPPUTL
029700*                                                                 PPAPPUTL
029720     IF APPT-ROW-CURS-OPENED                                      PPAPPUTL
029730        PERFORM SQL-CLOSE-APPT-ROW-9830                           PPAPPUTL
029740     END-IF.                                                      PPAPPUTL
029800     SKIP1                                                        PPAPPUTL
029900 5000-LOAD-EXIT.                                                  PPAPPUTL
030100     EXIT.                                                        PPAPPUTL
033300 EJECT                                                            PPAPPUTL
033400******************************************************************PPAPPUTL
033500*                                                                *PPAPPUTL
033600*                SQL- SELECTS                                    *PPAPPUTL
033700*                                                                *PPAPPUTL
033800*                                                                *PPAPPUTL
033900******************************************************************PPAPPUTL
034000*                                                                *PPAPPUTL
034100*  THE VIEWS USED IN THIS PROGRAM ARE WHOLE TABLE VIEWS:         *PPAPPUTL
034200*                                                                *PPAPPUTL
034300*          VIEW                 VIEW         DCL                 *PPAPPUTL
034400*                                DDL        INCLUDE              *PPAPPUTL
034500*       PPPVAPP1_APP           PPPVAPP1     PPPVAPP1             *PPAPPUTL
034700*                                                                *PPAPPUTL
034800******************************************************************PPAPPUTL
034900*               APPOINTMENT SQL                                  *PPAPPUTL
035000******************************************************************PPAPPUTL
035100     SKIP1                                                        PPAPPUTL
036900*                                                                 PPAPPUTL
037000 SQL-OPEN-APPT-ROW-9810 SECTION.                                  PPAPPUTL
037100     MOVE 'OPEN APPT CURSOR' TO DB2MSG-TAG.                       PPAPPUTL
037200     EXEC SQL                                                     PPAPPUTL
037300          OPEN APPT_ROW                                           PPAPPUTL
037400     END-EXEC.                                                    PPAPPUTL
037401*                                                                 PPAPPUTL
037410     IF SQLCODE = ZERO                                            PPAPPUTL
037420        SET APPT-ROW-CURS-OPENED TO TRUE                          PPAPPUTL
037430     END-IF.                                                      PPAPPUTL
037500*                                                                 PPAPPUTL
037600 SQL-FETCH-APPT-ROW-9820 SECTION.                                 PPAPPUTL
037700     MOVE 'FETCH APPT ROW' TO DB2MSG-TAG.                         PPAPPUTL
037800     EXEC SQL                                                     PPAPPUTL
037900          FETCH APPT_ROW INTO :APPOINT-DATA                       PPAPPUTL
038000     END-EXEC.                                                    PPAPPUTL
038001*                                                                 PPAPPUTL
038010     IF SQLCODE = ZERO                                            PPAPPUTL
038020        ADD +1 TO KAPP-APPT-LOADED                                PPAPPUTL
038030     END-IF.                                                      PPAPPUTL
038100*                                                                 PPAPPUTL
038200 SQL-CLOSE-APPT-ROW-9830 SECTION.                                 PPAPPUTL
038300     MOVE 'CLOSE APPT CURSOR' TO DB2MSG-TAG.                      PPAPPUTL
038400     EXEC SQL                                                     PPAPPUTL
038500          CLOSE APPT_ROW                                          PPAPPUTL
038600     END-EXEC.                                                    PPAPPUTL
038700     SKIP2                                                        PPAPPUTL
043100*                                                                 PPAPPUTL
043200********************** DB2 ABEND ROUTINE *************************PPAPPUTL
043300     COPY CPPDXP99.                                               PPAPPUTL
043400     SKIP3                                                        PPAPPUTL
043500     SET KAPP-INVALID-FUNCTION TO TRUE.                           PPAPPUTL
043501     MOVE ZERO TO KAPP-APPT-LOADED.                               PPAPPUTL
043510     GOBACK.                                                      PPAPPUTL
043700***********************END OF PPAPPUTL****************************PPAPPUTL
