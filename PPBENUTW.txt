000000**************************************************************/   05460730
000001*  PROGRAM: PPBENUTW                                         */   05460730
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____0555____  */   05550775
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/01/93__  */   05550775
000004*  DESCRIPTION:                                              */   05550775
000005*  -  ADDED MED-PROVIDER-ID    (DE 0653)                     */   05550775
000006*  -  ADDED DEN-PROVIDER-ID    (DE 0654)                     */   05550775
000008**************************************************************/   05460730
000000**************************************************************/   02040675
000001*  PROGRAM: PPBENUTW                                         */   02040675
000003*  RELEASE: ___0760______ SERVICE REQUEST(S): _____3626____  */   36260760
000004*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___04/30/93__  */   36260760
000005*  DESCRIPTION:                                              */   36260760
000006*  -  ADDED EMP-HLTH-COVEFFDT  (DE 0454)                     */   36260760
000007*  -  ADDED EMP-DENTL-COVEFFDT (DE 0455)                     */   36260760
000008*  -  ADDED EMP-VIS-COVEFFDT   (DE 0456)                     */   36260760
000009*  -  ADDED EMP-LEGAL-COVEFFDT (DE 0457)                     */   36260760
000007**************************************************************/   02040675
000000**************************************************************/   36220651
000001*  PROGRAM: PPBENUTW                                         */   36220651
000002*  RELEASE: ___0730______ SERVICE REQUEST(S): ____10546____  */   05460730
000003*  NAME:_____J. QUAN_____ MODIFICATION DATE:  ___12/17/92__  */   05460730
000004*  DESCRIPTION:                                              */   05460730
000005*  - ADDED PLN-403B-CHGD-DATE (DE 0279)                      */   05460730
000006*                                                            */   05460730
000007*  - DELETED CODE PREVIOUSLY COMMENTED-OUT; MARKED 'CD'      */   05460730
000800**************************************************************/   36220651
000000**************************************************************/   02040634
000001*  PROGRAM: PPBENUTW                                         */   02040634
000002*  RELEASE: ___0675______ SERVICE REQUEST(S): _____10204___  */   02040675
000003*  NAME:_____J. QUAN_____ MODIFICATION DATE:  ___05/11/92__  */   02040675
000004*  DESCRIPTION:                                              */   02040675
000005*  - REMOVED STD AND LTD DATA ELEMENTS                       */   02040675
000007**************************************************************/   02040634
000100**************************************************************/   40280572
000200*  PROGRAM: PPBENUTW                                         */   40280572
000300*  RELEASE: __0651_______ SERVICE REQUEST(S): _____3622____  */   36220651
000400*  NAME:__________KXK____ MODIFICATION DATE:  ___04/13/92__  */   36220651
000500*  DESCRIPTION:                                              */   36220651
000600*  - ADDED PROCESSING TO SUPPORT NEW AND CHANGED BENEFITS    */   36220651
000700*    TABLE COLUMNS FOR COVERAGE EFFECTIVE DATES.             */   36220651
000700**************************************************************/   40280572
000701**************************************************************/   45340564
000702*  PROGRAM: PPBENUTW                                         */   45340564
000002*  RELEASE: ___0634______ SERVICE REQUEST(S): _____10204___  */   02040634
000003*  NAME:_____J. QUAN_____ MODIFICATION DATE:  ___01/06/92__  */   02040634
000004*  DESCRIPTION:                                              */   02040634
000005*     ADD EPD-WAIT-PERIOD, EPD-SALARY-BASE, EPD-COVRGE-DATE, */   02040634
000006*     AND EPD-DEENROLL FIELD NAMES TO UPDATE SQL.            */   02040634
000007**************************************************************/   02040634
000100**************************************************************/   40280572
000200*  PROGRAM: PPBENUTW                                         */   40280572
000300*  RELEASE: ___0572______ SERVICE REQUEST(S): _____4028____  */   40280572
000400*  NAME:_____J.L._LOE____ MODIFICATION DATE:  ___05/29/91__  */   40280572
000500*  DESCRIPTION:                                              */   40280572
000600*  - ADDED EXEC-SPP-FLAG AND EXEC-SPP-PCT FOR SEVERANCE PAY  */   40280572
000700**************************************************************/   40280572
000701**************************************************************/   45340564
000702*  PROGRAM: PPBENUTW                                         */   45340564
000002*  RELEASE: ___0564______ SERVICE REQUEST(S): _____4534____  */   45340564
000003*  NAME:___M. BAPTISTA___ MODIFICATION DATE:  ___02/15/91__  */   45340564
000004*  DESCRIPTION:                                              */   45340564
000005*  - REMOVED DENTAL AND VISION ALTERNATIVE FIELDS TO BE      */   45340564
000006*    DELETED, UPDATED, OR INSERTED TO THE BEN TABLE AS THEY  */   45340564
000007*    NO LONGER EXIST.                                        */   45340564
000008**************************************************************/   45340564
000100**************************************************************/  *36090513
000200*  PROGRAM: PPBENUTW                                         */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE:      __11/05/90__   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT BEN ROWS.     */  *36090513
000700**************************************************************/  *36090513
000800 IDENTIFICATION DIVISION.                                         PPBENUTW
000900 PROGRAM-ID. PPBENUTW                                             PPBENUTW
001000 AUTHOR. UCOP.                                                    PPBENUTW
001100 DATE-WRITTEN.  SEPTEMBER 1990.                                   PPBENUTW
001200 DATE-COMPILED.                                                   PPBENUTW
001300*REMARKS.                                                         PPBENUTW
001400*            CALL 'PPBENUTW' USING                                PPBENUTW
001500*                            PPXXXUTW-INTERFACE                   PPBENUTW
001600*                            BEN-ROW.                             PPBENUTW
001700*                                                                 PPBENUTW
001800*                                                                 PPBENUTW
001900     EJECT                                                        PPBENUTW
002000 ENVIRONMENT DIVISION.                                            PPBENUTW
002100 CONFIGURATION SECTION.                                           PPBENUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPBENUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPBENUTW
002400      EJECT                                                       PPBENUTW
002500******************************************************************PPBENUTW
002600*                                                                 PPBENUTW
002700*                D A T A  D I V I S I O N                         PPBENUTW
002800*                                                                 PPBENUTW
002900******************************************************************PPBENUTW
003000 DATA DIVISION.                                                   PPBENUTW
003100 WORKING-STORAGE SECTION.                                         PPBENUTW
003200 01  MISCELLANEOUS-WS.                                            PPBENUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPBENUTW'.         PPBENUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPBENUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPBENUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPBENUTW
003700     EJECT                                                        PPBENUTW
003800 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPBENUTW
003900******************************************************************PPBENUTW
004000*          SQL - WORKING STORAGE                                 *PPBENUTW
004100******************************************************************PPBENUTW
004200 01  BEN-ROW-DATA.                                                PPBENUTW
004300     EXEC SQL                                                     PPBENUTW
004400          INCLUDE PPPVBEN2                                        PPBENUTW
004500     END-EXEC.                                                    PPBENUTW
004600     EJECT                                                        PPBENUTW
004700     EXEC SQL                                                     PPBENUTW
004800          INCLUDE SQLCA                                           PPBENUTW
004900     END-EXEC.                                                    PPBENUTW
005000******************************************************************PPBENUTW
005100*                                                                *PPBENUTW
005200*                SQL- SELECTS                                    *PPBENUTW
005300*                                                                *PPBENUTW
005400******************************************************************PPBENUTW
005500*                                                                *PPBENUTW
005600*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPBENUTW
005700*                                                                *PPBENUTW
005800*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPBENUTW
005900*                        MEMBER NAMES                            *PPBENUTW
006000*                                                                *PPBENUTW
006100*  PPPVBEN2_BEN          PPPVBEN2                                *PPBENUTW
006200*                                                                *PPBENUTW
006300******************************************************************PPBENUTW
006400*                                                                 PPBENUTW
006500 LINKAGE SECTION.                                                 PPBENUTW
006600*                                                                 PPBENUTW
006700******************************************************************PPBENUTW
006800*                L I N K A G E   S E C T I O N                   *PPBENUTW
006900******************************************************************PPBENUTW
007000 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPBENUTW
007100 01  BEN-ROW.                       COPY CPWSRBEN.                PPBENUTW
007200*                                                                 PPBENUTW
007300 PROCEDURE DIVISION USING                                         PPBENUTW
007400                          PPXXXUTW-INTERFACE                      PPBENUTW
007500                          BEN-ROW.                                PPBENUTW
007600******************************************************************PPBENUTW
007700*            P R O C E D U R E  D I V I S I O N                  *PPBENUTW
007800******************************************************************PPBENUTW
007900******************************************************************PPBENUTW
008000*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPBENUTW
008100*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPBENUTW
008200******************************************************************PPBENUTW
008300     EXEC SQL                                                     PPBENUTW
008400          INCLUDE CPPDXE99                                        PPBENUTW
008500     END-EXEC.                                                    PPBENUTW
008600     SKIP3                                                        PPBENUTW
008700 0100-MAINLINE SECTION.                                           PPBENUTW
008800     SKIP1                                                        PPBENUTW
008900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPBENUTW
009000     MOVE EMPLOYEE-ID OF BEN-ROW TO WS-EMPLOYEE-ID.               PPBENUTW
009100     SKIP1                                                        PPBENUTW
009200     MOVE BEN-ROW TO BEN-ROW-DATA                                 PPBENUTW
009300     SKIP1                                                        PPBENUTW
009400     EVALUATE PPXXXUTW-FUNCTION                                   PPBENUTW
009500         WHEN 'D'                                                 PPBENUTW
009600             PERFORM 8100-DELETE-ROW                              PPBENUTW
009700         WHEN 'U'                                                 PPBENUTW
009800             PERFORM 8200-UPDATE-ROW                              PPBENUTW
009900         WHEN 'I'                                                 PPBENUTW
010000             PERFORM 8300-INSERT-ROW                              PPBENUTW
010100         WHEN OTHER                                               PPBENUTW
010200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPBENUTW
010300             SET PPXXXUTW-ERROR            TO TRUE                PPBENUTW
010400     END-EVALUATE.                                                PPBENUTW
010500     SKIP1                                                        PPBENUTW
010600     GOBACK.                                                      PPBENUTW
010700     EJECT                                                        PPBENUTW
010800******************************************************************PPBENUTW
010900*  PROCEDURE SQL                     FOR BEN VIEW                *PPBENUTW
011000******************************************************************PPBENUTW
011100 8100-DELETE-ROW SECTION.                                         PPBENUTW
011200     SKIP1                                                        PPBENUTW
011300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPBENUTW
011400     SKIP1                                                        PPBENUTW
011500     EXEC SQL                                                     PPBENUTW
011600         DELETE FROM PPPVBEN2_BEN                                 PPBENUTW
011700                WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID               PPBENUTW
011800     END-EXEC.                                                    PPBENUTW
011900     SKIP1                                                        PPBENUTW
012000     IF  SQLCODE    NOT = 0                                       PPBENUTW
012100         SET PPXXXUTW-ERROR TO TRUE                               PPBENUTW
012200     END-IF.                                                      PPBENUTW
012300     EJECT                                                        PPBENUTW
012400 8200-UPDATE-ROW SECTION.                                         PPBENUTW
012500     SKIP1                                                        PPBENUTW
012600     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPBENUTW
012700     SKIP1                                                        PPBENUTW
012800     EXEC SQL                                                     PPBENUTW
012900     UPDATE PPPVBEN2_BEN                                          PPBENUTW
013000              SET  AGE_JAN1           = :AGE-JAN1           ,     PPBENUTW
013100                   RET_ELIG_CODE      = :RET-ELIG-CODE      ,     PPBENUTW
013200                   UCRS_ELIGDATE      = :UCRS-ELIGDATE      ,     PPBENUTW
013300                   DENTAL_PLAN        = :DENTAL-PLAN        ,     PPBENUTW
013400                   DENTAL_COVERAGE    = :DENTAL-COVERAGE    ,     PPBENUTW
016800                   DENTAL_COVEFFDATE  = :DENTAL-COVEFFDATE  ,     36220651
013600                   DENTAL_DEENROLL    = :DENTAL-DEENROLL    ,     PPBENUTW
013700                   DENTAL_OPTOUT      = :DENTAL-OPTOUT      ,     PPBENUTW
014400                   LIFEINS_PLAN       = :LIFEINS-PLAN       ,     PPBENUTW
014500                   LIFE_SALARY_BASE   = :LIFE-SALARY-BASE   ,     PPBENUTW
014600                   LIFE_UCPAIDAMT     = :LIFE-UCPAIDAMT     ,     PPBENUTW
018000                   LIFE_UCPD_EFFDATE  = :LIFE-UCPD-EFFDATE  ,     36220651
018200                   LIFE_COVEFFDATE    = :LIFE-COVEFFDATE    ,     36220651
014800                   LIFE_DEENROLL      = :LIFE-DEENROLL      ,     PPBENUTW
014900                   DEP_LIFE_IND       = :DEP-LIFE-IND       ,     PPBENUTW
015000                   DEP_LIFE_DEENROLL  = :DEP-LIFE-DEENROLL  ,     PPBENUTW
018700                   DEPLIFE_COVEFFDATE = :DEPLIFE-COVEFFDATE ,     36220651
015200                   ADD_PRINC_SUM      = :ADD-PRINC-SUM      ,     PPBENUTW
015300                   ADD_COVERAGE       = :ADD-COVERAGE       ,     PPBENUTW
019100                   ADD_COVEFFDATE     = :ADD-COVEFFDATE     ,     36220651
015500                   ADD_DEENROLL       = :ADD-DEENROLL       ,     PPBENUTW
016300                   HLTH_PLAN          = :HLTH-PLAN          ,     PPBENUTW
016400                   HLTH_COVERCODE     = :HLTH-COVERCODE     ,     PPBENUTW
020500                   HLTH_COVEFFDATE    = :HLTH-COVEFFDATE    ,     36220651
016600                   HLTH_DEENROLL      = :HLTH-DEENROLL      ,     PPBENUTW
016700                   HLTH_OPTOUT        = :HLTH-OPTOUT        ,     PPBENUTW
016800                   SEC_HLTH_PLAN      = :SEC-HLTH-PLAN      ,     PPBENUTW
016900                   SEC_HLTH_COVERAGE  = :SEC-HLTH-COVERAGE  ,     PPBENUTW
017000                   SEC_HLTH_DATE      = :SEC-HLTH-DATE      ,     PPBENUTW
017100                   VIS_PLAN           = :VIS-PLAN           ,     PPBENUTW
017200                   VIS_COVERAGE       = :VIS-COVERAGE       ,     PPBENUTW
021400                   VIS_COVEFFDATE     = :VIS-COVEFFDATE     ,     36220651
017400                   VIS_DEENROLL       = :VIS-DEENROLL       ,     PPBENUTW
017500                   VIS_OPTOUT         = :VIS-OPTOUT         ,     PPBENUTW
018200                   LEGAL_PLAN         = :LEGAL-PLAN         ,     PPBENUTW
018300                   LEGAL_COVERAGE     = :LEGAL-COVERAGE     ,     PPBENUTW
022600                   LEGAL_COVEFFDATE   = :LEGAL-COVEFFDATE   ,     36220651
018500                   LEGAL_DEENROLL     = :LEGAL-DEENROLL     ,     PPBENUTW
018600                   EXEC_BEN_ELIG      = :EXEC-BEN-ELIG      ,     PPBENUTW
018700                   INS_REDUCT_IND     = :INS-REDUCT-IND     ,     PPBENUTW
018800                   NDI_CODE           = :NDI-CODE           ,     PPBENUTW
023100                   NDI_COVEFFDATE     = :NDI-COVEFFDATE     ,     36220651
018900                   CAL_CAS_DED_IND    = :CAL-CAS-DED-IND    ,     PPBENUTW
019000                   EXEC_LIFEFLAG      = :EXEC-LIFEFLAG      ,     PPBENUTW
019100                   EXEC_LIFESALARY    = :EXEC-LIFESALARY    ,     PPBENUTW
019200                   EXEC_LIFECHANGE    = :EXEC-LIFECHANGE    ,     PPBENUTW
019300                   EXEC_LIFECHGDATE   = :EXEC-LIFECHGDATE   ,     PPBENUTW
023700                   EXEC_LIFE_EFFDATE  = :EXEC-LIFE-EFFDATE  ,     36220651
019400                   EXEC_LIFEINCOME    = :EXEC-LIFEINCOME    ,     PPBENUTW
021200                   DCP_PLAN_CODE      = :DCP-PLAN-CODE      ,     40280572
021300                   EXEC_SPP_FLAG      = :EXEC-SPP-FLAG      ,     40280572
021410                   EXEC_SPP_PCT       = :EXEC-SPP-PCT,            02040634
021420                   EPD_SALARY_BASE    = :EPD-SALARY-BASE,         02040634
021430                   EPD_WAIT_PERIOD    = :EPD-WAIT-PERIOD,         02040634
021440                   EPD_COVRGE_DATE    = :EPD-COVRGE-DATE,         02040634
024700*************      EPD_DEENROLL       = :EPD-DEENROLL             05460730
024710                   EPD_DEENROLL       = :EPD-DEENROLL,            05460730
024720                   PLN_403B_CHGD_DATE = :PLN-403B-CHGD-DATE       05460730
024730                  ,EMP_HLTH_COVEFFDT  = :EMP-HLTH-COVEFFDT        36260760
024740                  ,EMP_DENTL_COVEFFDT = :EMP-DENTL-COVEFFDT       36260760
024750                  ,EMP_VIS_COVEFFDT   = :EMP-VIS-COVEFFDT         36260760
024760                  ,EMP_LEGAL_COVEFFDT = :EMP-LEGAL-COVEFFDT       36260760
024770                  ,MED_PROVIDER_ID    = :MED-PROVIDER-ID          05550775
024780                  ,DEN_PROVIDER_ID    = :DEN-PROVIDER-ID          05550775
019500         WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                      PPBENUTW
019600     END-EXEC.                                                    PPBENUTW
019700     SKIP1                                                        PPBENUTW
019800     IF  SQLCODE    NOT = 0                                       PPBENUTW
019900         SET PPXXXUTW-ERROR TO TRUE                               PPBENUTW
020000     END-IF.                                                      PPBENUTW
020100     EJECT                                                        PPBENUTW
020200 8300-INSERT-ROW SECTION.                                         PPBENUTW
020300     SKIP1                                                        PPBENUTW
020400     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPBENUTW
020500     SKIP1                                                        PPBENUTW
020600     EXEC SQL                                                     PPBENUTW
020700         INSERT INTO PPPVBEN2_BEN                                 PPBENUTW
020800                VALUES (:BEN-ROW-DATA)                            PPBENUTW
020900     END-EXEC.                                                    PPBENUTW
021000     SKIP1                                                        PPBENUTW
021100     IF  SQLCODE    NOT = 0                                       PPBENUTW
021200         SET PPXXXUTW-ERROR TO TRUE                               PPBENUTW
021300     END-IF.                                                      PPBENUTW
021400     EJECT                                                        PPBENUTW
021500*999999-SQL-ERROR.                                               *PPBENUTW
021600     SKIP1                                                        PPBENUTW
021700     EXEC SQL                                                     PPBENUTW
021800          INCLUDE CPPDXP99                                        PPBENUTW
021900     END-EXEC.                                                    PPBENUTW
022000     SKIP1                                                        PPBENUTW
022100     SET PPXXXUTW-ERROR TO TRUE.                                  PPBENUTW
022200     IF NOT HERE-BEFORE                                           PPBENUTW
022300         SET HERE-BEFORE TO TRUE                                  PPBENUTW
022400     GOBACK.                                                      PPBENUTW
