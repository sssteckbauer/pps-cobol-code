000010*================================================================%UCSD0038
000020*= PROGRAM:  PPPERUTW                                           =%UCSD0038
000030*= CHANGE # 0038                     PROJ. REQUEST: DT038       =%UCSD0038
000040*= NAME: R SWIDERSKI                 MODIFICATION DATE: 10/31/91=%UCSD0038
000050*=                                                              =%UCSD0038
000060*= DESCRIPTION: LOCAL DATA ELEMENTS 0198 (TIMEKEEP-DEPT),       =%UCSD0038
000070*=                                  0199 (TIMEKEEP-SUB),        =%UCSD0038
000080*=                                  0192 (EMP-LEAVE-CODE),      =%UCSD0038
000090*=                                  0591 (LAST-UPD-OPERATOR),   =%UCSD0038
000091*=                                  0592 (LAST-UPD-DATE),       =%UCSD0038
000092*=                                  0593 (LAST-UPD-TIME),       =%UCSD0038
000093*=                                  0594 (LAST-SCRN-FUNC),      =%UCSD0038
000094*=                                  0194 (VICE-CHAN-UNIT),      =%UCSD0038
000095*=                                  0195 (COLLEGE-CODE),        =%UCSD0038
000096*=                                  0196 (MAIL-DROP-CODE), AND  =%UCSD0038
000097*=                                  0197 (ACAD-REVIEW-DATE)     =%UCSD0038
000098*= ADDED TO PROCESSING.                                         =%UCSD0038
000099*=============================================================== %UCSD0038
000000**************************************************************/   05450712
000001*  PROGRAM: PPPERUTW                                         */   05450712
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____3645____  */   36450775
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __06/01/93____ */   36450775
000004*  DESCRIPTION: EDB ENTRY/UPDATE IMPLEMENTATION.             */   36450775
000005*     ADDED 34 COLUMNS.                                      */   36450775
000006*     DATA ELEMENTS 0134 AND 0150,                           */   36450775
000007*     PAY-RATE-CHG AND RECLASS-TYPE, HAVE BEEN REUSED.       */   36450775
000010*                                                            */   36450775
000011**************************************************************/   36450775
000000**************************************************************/   05450712
000001*  PROGRAM: PPPERUTW                                         */   05450712
000002*  RELEASE: ___0712______ SERVICE REQUEST(S): _____0545____  */   05450712
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/04/92__  */   05450712
000004*  DESCRIPTION:                                              */   05450712
000005*    ADDED OATH SIGNATURE DATE DATA ELEMENT 0705             */   05450712
000006*                                                            */   05450712
000007**************************************************************/   05450712
000100**************************************************************/   05680687
000200*  PROGRAM: PPPERUTW                                         */   05680687
000300*  RELEASE: ___0687______ SERVICE REQUEST(S): ____10568____  */   05680687
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___06/25/92__  */   05680687
000500*  DESCRIPTION:                                              */   05680687
000600*  -- ADDED TRIP DATA ELEMENTS 0482 0483 0484                */   05680687
000700*                                                            */   05680687
000800**************************************************************/   05680687
000100**************************************************************/  *36060628
000200*  PROGRAM: PPPERUTW                                         */  *36060628
000300*  RELEASE: ____0628____  SERVICE REQUEST(S): ____3606____   */  *36060628
000400*  NAME:   G. STEINITZ    DATE MODIFIED:      __01/10/92__   */  *36060628
000500*  DESCRIPTION:                                              */  *36060628
000600*  - ADDED PER-CURR-STUD-FLAG FOR FINANCIAL AID PROCESSING   */  *36060628
000601**************************************************************/  *36210607
000602*  PROGRAM:  PPPERUTW                                        */  *36210607
000603*  RELEASE # ____0607____ SERVICE REQUEST NO(S)___3621_______*/  *36210607
000604*  NAME ______PXP______   MODIFICATION DATE ____10/14/91_____*/  *36210607
000605*  DESCRIPTION                                               */  *36210607
000606*    ADD DATA ELEMENTS 0460 AND 0461                         */  *36210607
000700**************************************************************/  *36210607
000701**************************************************************/  *36070542
000702*  PROGRAM: PPPERUTW                                         */  *36070542
000300*  RELEASE: ____0542____  SERVICE REQUEST(S): ____3607____   */  *36070542
000400*  NAME:   G. STEINITZ    DATE MODIFIED:      __03/04/91__   */  *36070542
000500*  DESCRIPTION:                                              */  *36070542
000600*  - ADDED LAST-DAY-ON-PAY                                   */  *36070542
000100**************************************************************/  *36090513
000200*  PROGRAM: PPPERUTW                                         */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE:      __11/05/90__   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT PER ROWS.     */  *36090513
000700**************************************************************/  *36090513
000800 IDENTIFICATION DIVISION.                                         PPPERUTW
000900 PROGRAM-ID. PPPERUTW                                             PPPERUTW
001000 AUTHOR. UCOP.                                                    PPPERUTW
001100 DATE-WRITTEN.  SEPTEMBER 1990.                                   PPPERUTW
001200 DATE-COMPILED.                                                   PPPERUTW
001300*REMARKS.                                                         PPPERUTW
001400*            CALL 'PPPERUTW' USING                                PPPERUTW
001500*                            PPXXXUTW-INTERFACE                   PPPERUTW
001600*                            PER-ROW.                             PPPERUTW
001700*                                                                 PPPERUTW
001800*                                                                 PPPERUTW
001900     EJECT                                                        PPPERUTW
002000 ENVIRONMENT DIVISION.                                            PPPERUTW
002100 CONFIGURATION SECTION.                                           PPPERUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPPERUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPPERUTW
002400      EJECT                                                       PPPERUTW
002500******************************************************************PPPERUTW
002600*                                                                 PPPERUTW
002700*                D A T A  D I V I S I O N                         PPPERUTW
002800*                                                                 PPPERUTW
002900******************************************************************PPPERUTW
003000 DATA DIVISION.                                                   PPPERUTW
003100 WORKING-STORAGE SECTION.                                         PPPERUTW
003200 01  MISCELLANEOUS-WS.                                            PPPERUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPPERUTW'.         PPPERUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPPERUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPPERUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPPERUTW
003700     EJECT                                                        PPPERUTW
003800 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPPERUTW
006510     EJECT                                                        36450775
003900******************************************************************PPPERUTW
004000*          SQL - WORKING STORAGE                                 *PPPERUTW
004100******************************************************************PPPERUTW
004200 01  PER-ROW-DATA.                                                PPPERUTW
004300     EXEC SQL                                                     PPPERUTW
004400          INCLUDE PPPVPER2                                        PPPERUTW
004500     END-EXEC.                                                    PPPERUTW
004600     EJECT                                                        PPPERUTW
004700     EXEC SQL                                                     PPPERUTW
004800          INCLUDE SQLCA                                           PPPERUTW
004900     END-EXEC.                                                    PPPERUTW
007610     EJECT                                                        36450775
005000******************************************************************PPPERUTW
005100*                                                                *PPPERUTW
005200*                SQL- SELECTS                                    *PPPERUTW
005300*                                                                *PPPERUTW
005400******************************************************************PPPERUTW
005500*                                                                *PPPERUTW
005600*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPPERUTW
005700*                                                                *PPPERUTW
005800*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPPERUTW
005900*                        MEMBER NAMES                            *PPPERUTW
006000*                                                                *PPPERUTW
006100*  PPPVPER2_PER          PPPVPER2                                *PPPERUTW
006200*                                                                *PPPERUTW
006300******************************************************************PPPERUTW
006400*                                                                 PPPERUTW
006500 LINKAGE SECTION.                                                 PPPERUTW
006600*                                                                 PPPERUTW
006700******************************************************************PPPERUTW
006800*                L I N K A G E   S E C T I O N                   *PPPERUTW
006900******************************************************************PPPERUTW
007000 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPPERUTW
007100 01  PER-ROW.                       COPY CPWSRPER.                PPPERUTW
007200*                                                                 PPPERUTW
007300 PROCEDURE DIVISION USING                                         PPPERUTW
007400                          PPXXXUTW-INTERFACE                      PPPERUTW
007500                          PER-ROW.                                PPPERUTW
007600******************************************************************PPPERUTW
007700*            P R O C E D U R E  D I V I S I O N                  *PPPERUTW
007800******************************************************************PPPERUTW
007900******************************************************************PPPERUTW
008000*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPPERUTW
008100*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPPERUTW
008200******************************************************************PPPERUTW
008300     EXEC SQL                                                     PPPERUTW
008400          INCLUDE CPPDXE99                                        PPPERUTW
008500     END-EXEC.                                                    PPPERUTW
008600     SKIP3                                                        PPPERUTW
008700 0100-MAINLINE SECTION.                                           PPPERUTW
008800     SKIP1                                                        PPPERUTW
008900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPPERUTW
009000     MOVE EMPLOYEE-ID OF PER-ROW TO WS-EMPLOYEE-ID.               PPPERUTW
009100     SKIP1                                                        PPPERUTW
009200     MOVE PER-ROW TO PER-ROW-DATA                                 PPPERUTW
009300     SKIP1                                                        PPPERUTW
009400     EVALUATE PPXXXUTW-FUNCTION                                   PPPERUTW
009500         WHEN 'D'                                                 PPPERUTW
009600             PERFORM 8100-DELETE-ROW                              PPPERUTW
009700         WHEN 'U'                                                 PPPERUTW
009800             PERFORM 8200-UPDATE-ROW                              PPPERUTW
009900         WHEN 'I'                                                 PPPERUTW
010000             PERFORM 8300-INSERT-ROW                              PPPERUTW
010100         WHEN OTHER                                               PPPERUTW
010200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPPERUTW
010300             SET PPXXXUTW-ERROR            TO TRUE                PPPERUTW
010400     END-EVALUATE.                                                PPPERUTW
010500     SKIP1                                                        PPPERUTW
010600     GOBACK.                                                      PPPERUTW
010700     EJECT                                                        PPPERUTW
010800******************************************************************PPPERUTW
010900*  PROCEDURE SQL                     FOR PER VIEW                *PPPERUTW
011000******************************************************************PPPERUTW
011100 8100-DELETE-ROW SECTION.                                         PPPERUTW
011200     SKIP1                                                        PPPERUTW
011300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPPERUTW
011400     SKIP1                                                        PPPERUTW
011500     EXEC SQL                                                     PPPERUTW
011600         DELETE FROM PPPVPER2_PER                                 PPPERUTW
011700                WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID               PPPERUTW
011800     END-EXEC.                                                    PPPERUTW
011900     SKIP1                                                        PPPERUTW
012000     IF  SQLCODE    NOT = 0                                       PPPERUTW
012100         SET PPXXXUTW-ERROR TO TRUE                               PPPERUTW
012200     END-IF.                                                      PPPERUTW
012300     EJECT                                                        PPPERUTW
012400 8200-UPDATE-ROW SECTION.                                         PPPERUTW
012500     SKIP1                                                        PPPERUTW
012600     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPPERUTW
012700     SKIP1                                                        PPPERUTW
012800     EXEC SQL                                                     PPPERUTW
012900     UPDATE PPPVPER2_PER                                          PPPERUTW
013000             SET  EMP_NAME           = :EMP-NAME            ,     PPPERUTW
013100                  NAMESUFFIX         = :NAMESUFFIX          ,     PPPERUTW
013200                  BIRTH_DATE         = :BIRTH-DATE          ,     PPPERUTW
013300                  HOME_PHONE         = :HOME-PHONE          ,     PPPERUTW
013400                  EMP_PRIOR_NAME     = :EMP-PRIOR-NAME      ,     PPPERUTW
013500                  SPOUSE_NAME        = :SPOUSE-NAME         ,     PPPERUTW
013600                  PRIOR_SERVICE_CODE = :PRIOR-SERVICE-CODE  ,     PPPERUTW
013700                  PRIOR_SERVICE_MTHS = :PRIOR-SERVICE-MTHS  ,     PPPERUTW
013800                  VET_STATUS         = :VET-STATUS          ,     PPPERUTW
013900                  VET_DISAB_STAT     = :VET-DISAB-STAT      ,     PPPERUTW
014000                  HANDICAP_STAT      = :HANDICAP-STAT       ,     PPPERUTW
014100                  VISA_END_DATE      = :VISA-END-DATE       ,     PPPERUTW
014200                  ETHNIC_ID          = :ETHNIC-ID           ,     PPPERUTW
014300                  SEXCODE            = :SEXCODE             ,     PPPERUTW
014400                  ADDR_DISCLOSURE1   = :ADDR-DISCLOSURE1    ,     PPPERUTW
014500                  ADDR_DISCLOSURE2   = :ADDR-DISCLOSURE2    ,     PPPERUTW
014600                  SPOUSE_DISC_IND    = :SPOUSE-DISC-IND     ,     PPPERUTW
014700                  EMP_ORGAN_DISCLO   = :EMP-ORGAN-DISCLO    ,     PPPERUTW
014800                  PAF_GEN_NUM        = :PAF-GEN-NUM         ,     PPPERUTW
014900                  LAST_ACTION        = :LAST-ACTION         ,     PPPERUTW
015000                  LAST_ACTION_OTHER  = :LAST-ACTION-OTHER   ,     PPPERUTW
015100                  LAST_ACTION_DATE   = :LAST-ACTION-DATE    ,     PPPERUTW
015200                  LAST_CHG_DATE      = :LAST-CHG-DATE       ,     PPPERUTW
015300                  HIRE_DATE          = :HIRE-DATE           ,     PPPERUTW
015400                  HOME_DEPT          = :HOME-DEPT           ,     PPPERUTW
015500                  EMP_STATUS         = :EMP-STATUS          ,     PPPERUTW
015600                  EDU_LEVEL          = :EDU-LEVEL           ,     PPPERUTW
015700                  STUDENT_STATUS     = :STUDENT-STATUS      ,     PPPERUTW
015800                  EDU_LEVEL_YR       = :EDU-LEVEL-YR        ,     PPPERUTW
015900                  PAY_RATE_CHG       = :PAY-RATE-CHG        ,     PPPERUTW
016000                  NEXT_SALARY_REV    = :NEXT-SALARY-REV     ,     PPPERUTW
016100                  NEXT_SALREV_DATE   = :NEXT-SALREV-DATE    ,     PPPERUTW
016200                  STAFF_SALARY_REV   = :STAFF-SALARY-REV    ,     PPPERUTW
016300                  STAFF_SALREV_STAT  = :STAFF-SALREV-STAT   ,     PPPERUTW
016400                  STAFF_SALREV_DATE  = :STAFF-SALREV-DATE   ,     PPPERUTW
016500                  MERIT_PERCENT      = :MERIT-PERCENT       ,     PPPERUTW
016600                  RECLASS_TYPE       = :RECLASS-TYPE        ,     PPPERUTW
016700                  LOA_BEGIN_DATE     = :LOA-BEGIN-DATE      ,     PPPERUTW
016800                  LOA_RETURN_DATE    = :LOA-RETURN-DATE     ,     PPPERUTW
016900                  LOA_TYPE_CODE      = :LOA-TYPE-CODE       ,     PPPERUTW
017000                  LOA_STATUS_IND     = :LOA-STATUS-IND      ,     PPPERUTW
017100                  EMP_REL_CODE       = :EMP-REL-CODE        ,     PPPERUTW
017200                  EMP_CBUC           = :EMP-CBUC            ,     PPPERUTW
017300                  EMP_REL_UNIT       = :EMP-REL-UNIT        ,     PPPERUTW
017400                  EMP_SPEC_HAND      = :EMP-SPEC-HAND       ,     PPPERUTW
017500                  EMP_DIST_UNIT_CODE = :EMP-DIST-UNIT-CODE  ,     PPPERUTW
017600                  EMP_REP_CODE       = :EMP-REP-CODE        ,     PPPERUTW
017700                  SEPARATE_DATE      = :SEPARATE-DATE       ,     PPPERUTW
017800                  SEPARATE_REASON    = :SEPARATE-REASON     ,     PPPERUTW
017900                  SEPARATE_DESTIN    = :SEPARATE-DESTIN     ,     PPPERUTW
018600                  LAST_DAY_ON_PAY    = :LAST-DAY-ON-PAY     ,     36070542
019400                  PRIMARY_TITLE      = :PRIMARY-TITLE       ,     36210607
019500                  JOB_GROUP_ID       = :JOB-GROUP-ID        ,     36210607
020200                  PER_CURR_STUD_FLAG = :PER-CURR-STUD-FLAG  ,     36060628
018601*                 STRIKE_IND         = :STRIKE-IND                UCSD0038
018602                  STRIKE_IND         = :STRIKE-IND          ,     UCSD0038
018603                  TIMEKEEP_DEPT      = :TIMEKEEP-DEPT       ,     UCSD0038
018604                  TIMEKEEP_SUB       = :TIMEKEEP-SUB        ,     UCSD0038
018605                  EMP_LEAVE_CODE     = :EMP-LEAVE-CODE      ,     UCSD0038
018606                  LAST_UPD_OPERATOR  = :LAST-UPD-OPERATOR   ,     UCSD0038
018607                  LAST_UPD_DATE      = :LAST-UPD-DATE       ,     UCSD0038
018608                  LAST_UPD_TIME      = :LAST-UPD-TIME       ,     UCSD0038
018609                  LAST_SCRN_FUNC     = :LAST-SCRN-FUNC      ,     UCSD0038
018610                  VICE_CHAN_UNIT     = :VICE-CHAN-UNIT      ,     UCSD0038
018611                  COLLEGE_CODE       = :COLLEGE-CODE        ,     UCSD0038
018612                  MAIL_DROP_CODE     = :MAIL-DROP-CODE      ,     UCSD0038
018613                  ACAD_REVIEW_DATE   = :ACAD-REVIEW-DATE          UCSD0038
021200                , TRIP_PERCENT       = :TRIP-PERCENT              05680687
021300                , TRIP_DURATION      = :TRIP-DURATION             05680687
021400                , TRIP_BEGIN_DATE    = :TRIP-BEGIN-DATE           05680687
021410                , OATH_SIGN_DATE     = :OATH-SIGN-DATE            05450712
021420                , EMP_CHANGED_AT     = :EMP-CHANGED-AT            36450775
021430                , EMP_CHANGED_BY     = :EMP-CHANGED-BY            36450775
021440                , ALT_DEPT_CD        = :ALT-DEPT-CD               36450775
021450                , ORIG_HIRE_DATE     = :ORIG-HIRE-DATE            36450775
021460                , PROB_END_DATE      = :PROB-END-DATE             36450775
021470                , DEGREE_INST        = :DEGREE-INST               36450775
021471                , PRIOR_SERV_INST    = :PRIOR-SERV-INST           36450775
021472                , FUTURE_INST        = :FUTURE-INST               36450775
021473                , CUR_SPECIALTY_1    = :CUR-SPECIALTY-1           36450775
021474                , CUR_SPECIALTY_2    = :CUR-SPECIALTY-2           36450775
021475                , CUR_SPECIALTY_3    = :CUR-SPECIALTY-3           36450775
021476                , HIGH_DEGREE        = :HIGH-DEGREE               36450775
021477                , TERM_COMP_TIME     = :TERM-COMP-TIME            36450775
021478                , TERM_SICK_LEAVE    = :TERM-SICK-LEAVE           36450775
021479                , TERM_VAC_LEAVE     = :TERM-VAC-LEAVE            36450775
021480                , EMPLMT_CREDIT      = :EMPLMT-CREDIT             36450775
021490                , CREDIT_FROM_DATE   = :CREDIT-FROM-DATE          36450775
021500                , TA_TIME            = :TA-TIME                   36450775
021510                , TA_TIME_TYPE       = :TA-TIME-TYPE              36450775
021520                , UNIT18_TIME        = :UNIT18-TIME               36450775
021521                , UNIT18_TIME_TYPE   = :UNIT18-TIME-TYPE          36450775
021522                , YEAR8_TIME         = :YEAR8-TIME                36450775
021523                , YEAR8_TIME_TYPE    = :YEAR8-TIME-TYPE           36450775
021524                , OVER50_TIME        = :OVER50-TIME               36450775
021525                , OVER50_TIME_TYPE   = :OVER50-TIME-TYPE          36450775
021526                , YEAR8_EXT          = :YEAR8-EXT                 36450775
021527                , YEAR8_EXT_TYPE     = :YEAR8-EXT-TYPE            36450775
021528                , LAST_CREDIT_BAL    = :LAST-CREDIT-BAL           36450775
021529                , CREDIT_DATE        = :CREDIT-DATE               36450775
021530                , LAST_CREDIT_ACC    = :LAST-CREDIT-ACC           36450775
021531                , CREDIT_USED        = :CREDIT-USED               36450775
021532                , TOT_CREDIT_BAL     = :TOT-CREDIT-BAL            36450775
021533                , CREDIT_THRU_DATE   = :CREDIT-THRU-DATE          36450775
021534                , CUT_CAP_ELIG_IND   = :CUT-CAP-ELIG-IND          36450775
018000         WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                      PPPERUTW
018100     END-EXEC.                                                    PPPERUTW
018200     SKIP1                                                        PPPERUTW
018300     IF  SQLCODE    NOT = 0                                       PPPERUTW
018400         SET PPXXXUTW-ERROR TO TRUE                               PPPERUTW
018500     END-IF.                                                      PPPERUTW
018600     EJECT                                                        PPPERUTW
018700 8300-INSERT-ROW SECTION.                                         PPPERUTW
018800     SKIP1                                                        PPPERUTW
018900     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPPERUTW
019000     SKIP1                                                        PPPERUTW
019100     EXEC SQL                                                     PPPERUTW
019200         INSERT INTO PPPVPER2_PER                                 PPPERUTW
019300                VALUES (:PER-ROW-DATA)                            PPPERUTW
019400     END-EXEC.                                                    PPPERUTW
019500     SKIP1                                                        PPPERUTW
019600     IF  SQLCODE    NOT = 0                                       PPPERUTW
019700         SET PPXXXUTW-ERROR TO TRUE                               PPPERUTW
019800     END-IF.                                                      PPPERUTW
019900     EJECT                                                        PPPERUTW
020000*999999-SQL-ERROR.                                               *PPPERUTW
020100     SKIP1                                                        PPPERUTW
020200     EXEC SQL                                                     PPPERUTW
020300          INCLUDE CPPDXP99                                        PPPERUTW
020400     END-EXEC.                                                    PPPERUTW
020500     SKIP1                                                        PPPERUTW
020600     SET PPXXXUTW-ERROR TO TRUE.                                  PPPERUTW
020700     IF NOT HERE-BEFORE                                           PPPERUTW
020800         SET HERE-BEFORE TO TRUE                                  PPPERUTW
020900     GOBACK.                                                      PPPERUTW
