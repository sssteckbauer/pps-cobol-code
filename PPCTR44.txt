000000**************************************************************/   72751413
000001*  PROGRAM: PPCTR44                                          */   72751413
000002*  RELEASE: ___1413______ SERVICE REQUEST(S): ____17275____  */   72751413
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___05/21/02__  */   72751413
000004*  DESCRIPTION:                                              */   72751413
000005*  - WEB MERIT CONTROL POINT DEPARTMENT REPORT MODULE        */   72751413
000007**************************************************************/   72751413
000800 IDENTIFICATION DIVISION.                                         PPCTR44
000900 PROGRAM-ID. PPCTR44.                                             PPCTR44
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTR44
001100 DATE-COMPILED.                                                   PPCTR44
001200 ENVIRONMENT DIVISION.                                            PPCTR44
001300 CONFIGURATION SECTION.                                           PPCTR44
001400 SPECIAL-NAMES.                                                   PPCTR44
001500     C01 IS NEW-PAGE,                                             PPCTR44
001600     CSP IS NO-LINES.                                             PPCTR44
001700 INPUT-OUTPUT SECTION.                                            PPCTR44
001800 FILE-CONTROL.                                                    PPCTR44
001900     SELECT REPORT-PRINT-FILE     ASSIGN TO UT-S-PPP0444.         PPCTR44
002000 DATA DIVISION.                                                   PPCTR44
002100 FILE SECTION.                                                    PPCTR44
002200 FD  REPORT-PRINT-FILE            COPY CPFDXPRT.                  PPCTR44
002300 WORKING-STORAGE SECTION.                                         PPCTR44
002400 01  MISC.                                                        PPCTR44
002500     05  LINE-COUNT               PIC S9(4) COMP SYNC VALUE +99.  PPCTR44
002600     05  NO-OF-LINES              PIC S9(4) COMP SYNC VALUE +1.   PPCTR44
002700     05  PAGE-COUNT               PIC S9(6) COMP SYNC VALUE ZERO. PPCTR44
002710     05  WS-DEPT-NO               PIC  X(6) VALUE SPACES.         PPCTR44
002720     05  WS-SAVE-CONTROL-DEPT     PIC  X(6) VALUE SPACES.         PPCTR44
002740     05  EOF-CURSOR-SW            PIC  X(1) VALUE 'N'.            PPCTR44
002750         88 EOF-CURSOR                      VALUE 'Y'.            PPCTR44
002760         88 NOT-EOF-CURSOR                  VALUE 'N'.            PPCTR44
002800 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTR44
002900 01  CTH-CTL-TABLE-MAINT-HEADERS.               COPY CPWSXCHR.    PPCTR44
003000 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTR44
003100 01  RPT44-HD1.                                                   PPCTR44
004400     03  FILLER                   PIC X(1)  VALUE SPACES.         PPCTR44
004500     03  FILLER                   PIC X(40) VALUE                 PPCTR44
004600         'MERIT DEPARTMENT'.                                      PPCTR44
004700     03  FILLER                   PIC X(10) VALUE SPACES.         PPCTR44
004710     03  FILLER                   PIC X(40) VALUE                 PPCTR44
004720         'MERIT CONTROL DEPARTMENT'.                              PPCTR44
004730     03  FILLER                   PIC X(42) VALUE SPACES.         PPCTR44
004800 01  RPT44-D1.                                                    PPCTR44
004900     03  FILLER                   PIC X(01).                      PPCTR44
005200     03  RPT44-DEPT-NO1           PIC X(06).                      PPCTR44
005300     03  FILLER                   PIC X(04).                      PPCTR44
005400     03  RPT44-DEPT-NAME1         PIC X(30).                      PPCTR44
005500     03  FILLER                   PIC X(10).                      PPCTR44
005600     03  RPT44-DEPT-NO2           PIC X(06).                      PPCTR44
005800     03  FILLER                   PIC X(04).                      PPCTR44
005900     03  RPT44-DEPT-NAME2         PIC X(30).                      PPCTR44
006000     03  FILLER                   PIC X(42).                      PPCTR44
007600                                                                  PPCTR44
007700 01  HME-TABLE-DATA.                                              PPCTR44
007800     EXEC SQL                                                     PPCTR44
007900        INCLUDE PPPVZHME                                          PPCTR44
008000     END-EXEC.                                                    PPCTR44
008010                                                                  PPCTR44
008020 01  MCP-TABLE-DATA.                                              PPCTR44
008030     EXEC SQL                                                     PPCTR44
008040        INCLUDE PPPVZMCP                                          PPCTR44
008050     END-EXEC.                                                    PPCTR44
008100                                                                  PPCTR44
010220     EXEC SQL                                                     PPCTR44
010230         DECLARE  MCP_CTLD CURSOR FOR                             PPCTR44
010240         SELECT   MCP_DEPT_NO                                     PPCTR44
010250                 ,MCP_CONTROL_DEPT                                PPCTR44
010260         FROM     PPPVZMCP_MCP                                    PPCTR44
010270         ORDER BY MCP_CONTROL_DEPT                                PPCTR44
010271                 ,MCP_DEPT_NO                                     PPCTR44
010280     END-EXEC.                                                    PPCTR44
010300                                                                  PPCTR44
010310     EXEC SQL                                                     PPCTR44
010320         DECLARE  MCP_DEPT CURSOR FOR                             PPCTR44
010330         SELECT   MCP_DEPT_NO                                     PPCTR44
010340                 ,MCP_CONTROL_DEPT                                PPCTR44
010350         FROM     PPPVZMCP_MCP                                    PPCTR44
010360         ORDER BY MCP_DEPT_NO                                     PPCTR44
010370     END-EXEC.                                                    PPCTR44
010380                                                                  PPCTR44
010400     EXEC SQL                                                     PPCTR44
010500        INCLUDE SQLCA                                             PPCTR44
010600     END-EXEC.                                                    PPCTR44
010700                                                                  PPCTR44
010800 LINKAGE SECTION.                                                 PPCTR44
010900                                                                  PPCTR44
011000 01  CTPI-CTL-TBL-PRINT-INTERFACE.              COPY CPLNCTPI.    PPCTR44
011100                                                                  PPCTR44
011200 PROCEDURE DIVISION USING CTPI-CTL-TBL-PRINT-INTERFACE.           PPCTR44
011300                                                                  PPCTR44
011400 0000-MAIN SECTION.                                               PPCTR44
011500                                                                  PPCTR44
011600     EXEC SQL                                                     PPCTR44
011700        INCLUDE CPPDXE99                                          PPCTR44
011800     END-EXEC.                                                    PPCTR44
011900                                                                  PPCTR44
012000     PERFORM 1000-PROGRAM-INITIALIZATION.                         PPCTR44
012100     PERFORM 2000-CREATE-REPORT.                                  PPCTR44
012200     PERFORM 9000-PROGRAM-TERMINATION.                            PPCTR44
012300                                                                  PPCTR44
012400 0999-END.                                                        PPCTR44
012500                                                                  PPCTR44
012600     GOBACK.                                                      PPCTR44
012700                                                                  PPCTR44
012800 1000-PROGRAM-INITIALIZATION SECTION.                             PPCTR44
012900                                                                  PPCTR44
013000     MOVE 'PPCTR44' TO XWHC-DISPLAY-MODULE.                       PPCTR44
013100     COPY CPPDXWHC.                                               PPCTR44
013200     MOVE CTPI-HEADER-1 TO CTH-RPT-HD1.                           PPCTR44
013300     MOVE CTPI-HEADER-2 TO CTH-RPT-HD2.                           PPCTR44
013400     MOVE CTPI-HEADER-3 TO CTH-RPT-HD3.                           PPCTR44
013500     MOVE XWHC-MONTH-COMPILED TO CTH-COMPILE-MM.                  PPCTR44
013600     MOVE XWHC-DAY-COMPILED TO CTH-COMPILE-DD.                    PPCTR44
013700     MOVE XWHC-YEAR-COMPILED TO CTH-COMPILE-YY.                   PPCTR44
013710     IF CTPI-REPORT-OPTION = SPACE                                PPCTR44
013720        MOVE '0' TO CTPI-REPORT-OPTION                            PPCTR44
013730     END-IF.                                                      PPCTR44
013800     OPEN OUTPUT REPORT-PRINT-FILE.                               PPCTR44
014200                                                                  PPCTR44
014300 2000-CREATE-REPORT          SECTION.                             PPCTR44
014310**************************************************************/   PPCTR44
014320*    OPEN THE CURSOR IN WHICHEVER ORDER PER REPORT FORMAT.   */   PPCTR44
014330*    FETCH THE FIRST, CONTINUE UNTIL EOF ON THE CURSOR.      */   PPCTR44
014340**************************************************************/   PPCTR44
014400                                                                  PPCTR44
014500     PERFORM 7000-OPEN-MCP.                                       PPCTR44
014600     PERFORM 7100-FETCH-MCP.                                      PPCTR44
014700     PERFORM 6000-PAGE-HEADER.                                    PPCTR44
014710     MOVE MCP-CONTROL-DEPT TO WS-SAVE-CONTROL-DEPT.               PPCTR44
014800                                                                  PPCTR44
014900     PERFORM 3000-PRINT-MCP-TABLE                                 PPCTR44
015000       UNTIL EOF-CURSOR.                                          PPCTR44
015100                                                                  PPCTR44
015200     PERFORM 7200-CLOSE-MCP.                                      PPCTR44
015600                                                                  PPCTR44
015700 3000-PRINT-MCP-TABLE        SECTION.                             PPCTR44
015710**************************************************************/   PPCTR44
015720*    REPORT OPTION 0: IN CONTROL DEPARTMENT/DEPARTMENT ORDER */   PPCTR44
015721*                     NEW PAGE ON CONTROL DEPARTMENT BREAK   */   PPCTR44
015730*    REPORT OPTION 1: IN DEPARTMENT ORDER                    */   PPCTR44
015740**************************************************************/   PPCTR44
015800                                                                  PPCTR44
015900     MOVE SPACES TO RPT44-D1.                                     PPCTR44
016624                                                                  PPCTR44
016625     IF CTPI-REPORT-OPTION = '0'                                  PPCTR44
016626        IF MCP-CONTROL-DEPT NOT = WS-SAVE-CONTROL-DEPT            PPCTR44
016627           PERFORM 6000-PAGE-HEADER                               PPCTR44
016628           MOVE MCP-CONTROL-DEPT TO WS-SAVE-CONTROL-DEPT          PPCTR44
016629        END-IF                                                    PPCTR44
016630     END-IF.                                                      PPCTR44
016631                                                                  PPCTR44
016632     MOVE MCP-DEPT-NO      TO RPT44-DEPT-NO1                      PPCTR44
016633     MOVE MCP-DEPT-NO      TO WS-DEPT-NO                          PPCTR44
016634     PERFORM 7300-SELECT-HME                                      PPCTR44
016635     MOVE HME-DEPT-NAME    TO RPT44-DEPT-NAME1                    PPCTR44
016636     MOVE MCP-CONTROL-DEPT TO RPT44-DEPT-NO2                      PPCTR44
016637     MOVE MCP-CONTROL-DEPT TO WS-DEPT-NO                          PPCTR44
016640     PERFORM 7300-SELECT-HME                                      PPCTR44
016650     MOVE HME-DEPT-NAME    TO RPT44-DEPT-NAME2                    PPCTR44
016671                                                                  PPCTR44
017700     MOVE RPT44-D1 TO PRINT-REC.                                  PPCTR44
017800     PERFORM 8000-PRINT-A-LINE.                                   PPCTR44
017900     MOVE SPACES TO RPT44-D1.                                     PPCTR44
019500                                                                  PPCTR44
019600     PERFORM 7100-FETCH-MCP.                                      PPCTR44
020700                                                                  PPCTR44
020800 6000-PAGE-HEADER            SECTION.                             PPCTR44
020810**************************************************************/   PPCTR44
020820*    PRINT THE REPORT HEADER                                 */   PPCTR44
020821*    CPPDXCHR ADDS TO LINE-COUNT FOR ITS OWN ACTIONS         */   PPCTR44
020830**************************************************************/   PPCTR44
020900                                                                  PPCTR44
021000     COPY CPPDXCHR.                                               PPCTR44
021100     MOVE 2 TO NO-OF-LINES.                                       PPCTR44
021200     MOVE RPT44-HD1 TO PRINT-REC.                                 PPCTR44
021300     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR44
021600     MOVE 1 TO NO-OF-LINES.                                       PPCTR44
021610     MOVE SPACES TO PRINT-REC.                                    PPCTR44
021620     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR44
021700     ADD 3 TO LINE-COUNT.                                         PPCTR44
022200                                                                  PPCTR44
022300 7000-OPEN-MCP               SECTION.                             PPCTR44
022301**************************************************************/   PPCTR44
022311*    REPORT OPTION 0: IN CONTROL DEPARTMENT/DEPARTMENT ORDER */   PPCTR44
022312*    REPORT OPTION 1: IN DEPARTMENT ORDER                    */   PPCTR44
022320**************************************************************/   PPCTR44
022400                                                                  PPCTR44
022500     MOVE 'OPEN MCP CURSOR' TO DB2MSG-TAG.                        PPCTR44
022501     EVALUATE TRUE                                                PPCTR44
022801        WHEN (CTPI-REPORT-OPTION = '0')                           PPCTR44
022811          EXEC SQL                                                PPCTR44
022812              OPEN MCP_CTLD                                       PPCTR44
022813          END-EXEC                                                PPCTR44
022814        WHEN (CTPI-REPORT-OPTION = '1')                           PPCTR44
022815          EXEC SQL                                                PPCTR44
022816              OPEN MCP_DEPT                                       PPCTR44
022817          END-EXEC                                                PPCTR44
022820     END-EVALUATE.                                                PPCTR44
023200                                                                  PPCTR44
023300 7100-FETCH-MCP              SECTION.                             PPCTR44
023310**************************************************************/   PPCTR44
023330*    REPORT OPTION 0: IN CONTROL DEPARTMENT/DEPARTMENT ORDER */   PPCTR44
023331*    REPORT OPTION 1: IN DEPARTMENT ORDER                    */   PPCTR44
023340**************************************************************/   PPCTR44
023400                                                                  PPCTR44
023500     MOVE 'FETCH MCP ROW' TO DB2MSG-TAG.                          PPCTR44
023501     EVALUATE TRUE                                                PPCTR44
023508        WHEN (CTPI-REPORT-OPTION = '0')                           PPCTR44
023509          EXEC SQL                                                PPCTR44
023510            FETCH  MCP_CTLD                                       PPCTR44
023511             INTO :MCP-DEPT-NO                                    PPCTR44
023512                 ,:MCP-CONTROL-DEPT                               PPCTR44
023513          END-EXEC                                                PPCTR44
023514        WHEN (CTPI-REPORT-OPTION = '1')                           PPCTR44
023515          EXEC SQL                                                PPCTR44
023516            FETCH  MCP_DEPT                                       PPCTR44
023517             INTO :MCP-DEPT-NO                                    PPCTR44
023518                 ,:MCP-CONTROL-DEPT                               PPCTR44
023519          END-EXEC                                                PPCTR44
023530     END-EVALUATE.                                                PPCTR44
023531                                                                  PPCTR44
023540     IF SQLCODE NOT = ZERO                                        PPCTR44
023550        SET EOF-CURSOR TO TRUE                                    PPCTR44
023560     END-IF.                                                      PPCTR44
025800                                                                  PPCTR44
025900 7200-CLOSE-MCP              SECTION.                             PPCTR44
025910**************************************************************/   PPCTR44
025920*    REPORT OPTION 0: IN CONTROL DEPARTMENT/DEPARTMENT ORDER */   PPCTR44
025930*    REPORT OPTION 1: IN DEPARTMENT ORDER                    */   PPCTR44
025940**************************************************************/   PPCTR44
026000                                                                  PPCTR44
026420     MOVE 'CLOSE MCP CURSOR' TO DB2MSG-TAG.                       PPCTR44
026421     EVALUATE TRUE                                                PPCTR44
026426        WHEN (CTPI-REPORT-OPTION = '0')                           PPCTR44
026427          EXEC SQL                                                PPCTR44
026428              CLOSE MCP_CTLD                                      PPCTR44
026429          END-EXEC                                                PPCTR44
026430        WHEN (CTPI-REPORT-OPTION = '1')                           PPCTR44
026431          EXEC SQL                                                PPCTR44
026432              CLOSE MCP_DEPT                                      PPCTR44
026433          END-EXEC                                                PPCTR44
026434     END-EVALUATE.                                                PPCTR44
026710                                                                  PPCTR44
026720 7300-SELECT-HME             SECTION.                             PPCTR44
026721**************************************************************/   PPCTR44
026722*    SELECT THE DEPARTMENT NAME FOR A DEPARTMENT NUMBER      */   PPCTR44
026724**************************************************************/   PPCTR44
026730                                                                  PPCTR44
026740     MOVE '9000-SEL-HME ' TO DB2MSG-TAG                           PPCTR44
026750     EXEC SQL                                                     PPCTR44
026760         SELECT   HME_DEPT_NAME                                   PPCTR44
026770           INTO  :HME-DEPT-NAME                                   PPCTR44
026780           FROM   PPPVZHME_HME                                    PPCTR44
026790          WHERE   HME_DEPT_NO       = :WS-DEPT-NO                 PPCTR44
026791     END-EXEC.                                                    PPCTR44
026792                                                                  PPCTR44
027000 8000-PRINT-A-LINE           SECTION.                             PPCTR44
027100**************************************************************/   PPCTR44
027110*    PRINT A DETAIL LINE OF THE REPORT                       */   PPCTR44
027120**************************************************************/   PPCTR44
027150                                                                  PPCTR44
027200     WRITE PRINT-REC AFTER ADVANCING NO-OF-LINES.                 PPCTR44
027300     MOVE SPACES TO PRINT-REC.                                    PPCTR44
027400     ADD NO-OF-LINES TO LINE-COUNT.                               PPCTR44
027500     IF LINE-COUNT > 55                                           PPCTR44
027600        PERFORM 6000-PAGE-HEADER                                  PPCTR44
027700     END-IF.                                                      PPCTR44
027800     MOVE 1 TO NO-OF-LINES.                                       PPCTR44
027900                                                                  PPCTR44
028000 8999-PRINT-EXIT.                                                 PPCTR44
028100     EXIT.                                                        PPCTR44
028200                                                                  PPCTR44
028300 9000-PROGRAM-TERMINATION    SECTION.                             PPCTR44
028400                                                                  PPCTR44
028500     CLOSE REPORT-PRINT-FILE.                                     PPCTR44
028900                                                                  PPCTR44
029000*999999-SQL-ERROR.                                                PPCTR44
029010**************************************************************/   PPCTR44
029020*    NEGATIVE SQL ERRORS                                     */   PPCTR44
029030**************************************************************/   PPCTR44
029100     EXEC SQL                                                     PPCTR44
029200        INCLUDE CPPDXP99                                          PPCTR44
029300     END-EXEC.                                                    PPCTR44
029400     MOVE '99' TO CTPI-RETURN-CODE.                               PPCTR44
029500     GO TO 0999-END.                                              PPCTR44
