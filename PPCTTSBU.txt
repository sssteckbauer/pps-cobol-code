000010**************************************************************/   30871424
000020*  PROGRAM: PPCTTSBU                                         */   30871424
000030*  RELEASE: ___1424______ SERVICE REQUEST(S): _____3087____  */   30871424
000040*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___07/30/02__  */   30871424
000050*  ERROR REPORT 1808:                                        */   30871424
000060*  SET ERROR FLAGS FOR NEGATIVE SQL ERRORS TO TRIGGER DB2    */   30871424
000070*  ERROR REPORTING IN TRANSACTION HANDLER PROGRAM.           */   30871424
000080**************************************************************/   30871424
000100**************************************************************/   73151304
000200*  PROGRAM: PPCTTSBU                                         */   7315SRRR
000300*  RELEASE: ___1304______ SERVICE REQUEST(S): ____17315____  */   73151304
000400*  NAME:_______WJG_______ MODIFICATION DATE:  ___06/15/00__  */   73151304
000500*  DESCRIPTION:                                              */   73151304
000600*  TITLE CODE (TSB) TABLE UPDATE MODULE                      */   73151304
000700**************************************************************/   73151304
000800 IDENTIFICATION DIVISION.                                         PPCTTSBU
000900 PROGRAM-ID. PPCTTSBU.                                            PPCTTSBU
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPCTTSBU
001100 DATE-COMPILED.                                                   PPCTTSBU
001200 ENVIRONMENT DIVISION.                                            PPCTTSBU
001300 DATA DIVISION.                                                   PPCTTSBU
001400 WORKING-STORAGE SECTION.                                         PPCTTSBU
001500                                                                  PPCTTSBU
001600 01  XWHC-COMPILE-WORK-AREA.                    COPY CPWSXWHC.    PPCTTSBU
001700 01  PPDB2MSG-INTERFACE.                        COPY CPLNKDB2.    PPCTTSBU
001900     EXEC SQL                                                     PPCTTSBU
002000        INCLUDE SQLCA                                             PPCTTSBU
002100     END-EXEC.                                                    PPCTTSBU
002110 01  TITLE-CODE-TSB-TABLE-DATA  EXTERNAL.                         PPCTTSBU
002120     EXEC SQL                                                     PPCTTSBU
002130        INCLUDE PPPVZTSB                                          PPCTTSBU
002140     END-EXEC.                                                    PPCTTSBU
002200                                                                  PPCTTSBU
002300 LINKAGE SECTION.                                                 PPCTTSBU
002400                                                                  PPCTTSBU
002500 01  CONTROL-TABLE-UPDT-INTERFACE.              COPY CPLNKCTU.    PPCTTSBU
003000                                                                  PPCTTSBU
003100 PROCEDURE DIVISION USING CONTROL-TABLE-UPDT-INTERFACE.           PPCTTSBU
003300                                                                  PPCTTSBU
003400 0000-MAIN.                                                       PPCTTSBU
003500                                                                  PPCTTSBU
003900     MOVE 'PPCTTSBU' TO XWHC-DISPLAY-MODULE, DB2MSG-PGM-ID.       PPCTTSBU
004000     COPY CPPDXWHC.                                               PPCTTSBU
004100     MOVE SPACE TO KCTU-STATUS.                                   PPCTTSBU
004200     IF  KCTU-ACTION-ADD                                          PPCTTSBU
004300         PERFORM 2000-INSERT-TSB                                  PPCTTSBU
004400     ELSE                                                         PPCTTSBU
004500         IF  KCTU-ACTION-CHANGE                                   PPCTTSBU
004600             PERFORM 2100-UPDATE-TSB                              PPCTTSBU
004700         ELSE                                                     PPCTTSBU
004710             IF  KCTU-ACTION-DELETE                               PPCTTSBU
004711                 PERFORM 2200-DELETE-TSB                          PPCTTSBU
004720             ELSE                                                 PPCTTSBU
004800                 SET KCTU-INVALID-ACTION TO TRUE                  PPCTTSBU
004900             END-IF                                               PPCTTSBU
004910         END-IF                                                   PPCTTSBU
005000     END-IF.                                                      PPCTTSBU
005100                                                                  PPCTTSBU
005200 0100-END.                                                        PPCTTSBU
005300                                                                  PPCTTSBU
005400     GOBACK.                                                      PPCTTSBU
005500                                                                  PPCTTSBU
005600 2000-INSERT-TSB.                                                 PPCTTSBU
005700                                                                  PPCTTSBU
005800     MOVE '2000-INSRT-TSB' TO DB2MSG-TAG.                         PPCTTSBU
005900     EXEC SQL                                                     PPCTTSBU
006000         INSERT INTO PPPVZTSB_TSB                                 PPCTTSBU
006100              VALUES (:TSB-TITLE-CODE                             PPCTTSBU
006300                     ,:TSB-SUB-LOCATION                           PPCTTSBU
006301                     ,:TSB-PAY-REP-CODE                           PPCTTSBU
006310                     ,:TSB-EFFECTIVE-DATE                         PPCTTSBU
006320                     ,:TSB-END-DATE                               PPCTTSBU
006400                     ,:TSB-RANGE-ADJ-ID                           PPCTTSBU
006500                     ,:TSB-NO-STEPS                               PPCTTSBU
006600                     ,:TSB-NO-PAY-INTRVLS                         PPCTTSBU
006700                     ,:TSB-SSP-SS-EXCPT                           PPCTTSBU
006800                     ,:TSB-MERIT-BASED                            PPCTTSBU
006900                     ,:TSB-MEMO-GRADE                             PPCTTSBU
008910                     ,:TSB-UPDT-SOURCE                            PPCTTSBU
008920                     ,CURRENT TIMESTAMP                           PPCTTSBU
009000                     )                                            PPCTTSBU
009100     END-EXEC.                                                    PPCTTSBU
009200     IF  SQLCODE NOT EQUAL ZERO                                   PPCTTSBU
009300         SET KCTU-INSERT-ERROR TO TRUE                            PPCTTSBU
009310         PERFORM 999999-SQL-ERROR                                 PPCTTSBU
009400     END-IF.                                                      PPCTTSBU
009500                                                                  PPCTTSBU
009510 2100-UPDATE-TSB.                                                 PPCTTSBU
009520                                                                  PPCTTSBU
009530     MOVE '2100-UPDT-TSB' TO DB2MSG-TAG.                          PPCTTSBU
009540     EXEC SQL                                                     PPCTTSBU
009550          UPDATE PPPVZTSB_TSB                                     PPCTTSBU
009580              SET TSB_END_DATE        = :TSB-END-DATE             PPCTTSBU
009590                 ,TSB_RANGE_ADJ_ID    = :TSB-RANGE-ADJ-ID         PPCTTSBU
009591                 ,TSB_NO_STEPS        = :TSB-NO-STEPS             PPCTTSBU
009592                 ,TSB_NO_PAY_INTRVLS  = :TSB-NO-PAY-INTRVLS       PPCTTSBU
009593                 ,TSB_SSP_SS_EXCPT    = :TSB-SSP-SS-EXCPT         PPCTTSBU
009594                 ,TSB_MERIT_BASED     = :TSB-MERIT-BASED          PPCTTSBU
009595                 ,TSB_MEMO_GRADE      = :TSB-MEMO-GRADE           PPCTTSBU
009612                 ,TSB_UPDT_SOURCE     = :TSB-UPDT-SOURCE          PPCTTSBU
009613                 ,TSB_UPDT_TIMESTAMP  = CURRENT TIMESTAMP         PPCTTSBU
009615            WHERE TSB_TITLE_CODE      = :TSB-TITLE-CODE           PPCTTSBU
009616              AND TSB_SUB_LOCATION    = :TSB-SUB-LOCATION         PPCTTSBU
009617              AND TSB_PAY_REP_CODE    = :TSB-PAY-REP-CODE         PPCTTSBU
009618              AND TSB_EFFECTIVE_DATE  = :TSB-EFFECTIVE-DATE       PPCTTSBU
009619     END-EXEC.                                                    PPCTTSBU
009620     IF  SQLCODE NOT EQUAL ZERO                                   PPCTTSBU
009621         SET KCTU-UPDATE-ERROR TO TRUE                            PPCTTSBU
009622         PERFORM 999999-SQL-ERROR                                 PPCTTSBU
009623     END-IF.                                                      PPCTTSBU
009624                                                                  PPCTTSBU
009625 2200-DELETE-TSB.                                                 PPCTTSBU
009626                                                                  PPCTTSBU
009627     MOVE '2200-DEL-TSB' TO DB2MSG-TAG.                           PPCTTSBU
009628     EXEC SQL                                                     PPCTTSBU
009629          DELETE FROM PPPVZTSB_TSB                                PPCTTSBU
009630           WHERE  TSB_TITLE_CODE      = :TSB-TITLE-CODE           PPCTTSBU
009631              AND TSB_SUB_LOCATION    = :TSB-SUB-LOCATION         PPCTTSBU
009640              AND TSB_PAY_REP_CODE    = :TSB-PAY-REP-CODE         PPCTTSBU
009652              AND TSB_EFFECTIVE_DATE  = :TSB-EFFECTIVE-DATE       PPCTTSBU
009653     END-EXEC.                                                    PPCTTSBU
009654     IF  SQLCODE NOT EQUAL ZERO                                   PPCTTSBU
009655         SET KCTU-DELETE-ERROR TO TRUE                            PPCTTSBU
009656         PERFORM 999999-SQL-ERROR                                 PPCTTSBU
009657     END-IF.                                                      PPCTTSBU
009658                                                                  PPCTTSBU
009660*999999-SQL-ERROR.                                                PPCTTSBU
009700     EXEC SQL                                                     PPCTTSBU
009800        INCLUDE CPPDXP99                                          PPCTTSBU
009900     END-EXEC.                                                    PPCTTSBU
009910     EVALUATE TRUE                                                30871424
009920       WHEN (KCTU-ACTION-ADD)                                     30871424
009930         SET KCTU-INSERT-ERROR TO TRUE                            30871424
009940       WHEN (KCTU-ACTION-CHANGE)                                  30871424
009950         SET KCTU-UPDATE-ERROR TO TRUE                            30871424
009960       WHEN (KCTU-ACTION-DELETE)                                  30871424
009970         SET KCTU-DELETE-ERROR TO TRUE                            30871424
009980     END-EVALUATE.                                                30871424
010000     GO TO 0100-END.                                              PPCTTSBU
